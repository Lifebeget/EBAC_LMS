import * as fs from 'fs';
import { mergeObject } from '../../lib/src/helpers/objects';
import { getDefaultMultidomainConfig, MultiDomains, MultiDomainsData } from '../../lib/src/config/multidomains';
import { MultiDomainsEnum } from '../../lib/src/types/enums';
const environment = process.env.ENVIRONMENT || 'local';

interface ICronJob {
  active: boolean;
  time: string;
  command: string;
  log: string;
}

interface IConfigFile {
  language: string;
  multidomain: MultiDomainsData;
  timeZone: string;
  cronJobs: Record<string, ICronJob>;
  launchJobs: Record<string, ICronJob>;
  mails: string;
  nps: Record<string, any>;
  defaultDomainCode: string;
  authLinkExpired: Record<string, number>;
  get(key: string): any;
  setUser: (any) => any;
  user: Record<string, any>;
  defaultCourseSupportDuration: number;
  defaultSatelliteCourseSupportDuration: number;
  defaultTrialCourseSupportDuration: number;
}

export class ConfigInstance {
  static _instance: ConfigInstance;
  public config: IConfigFile;

  constructor() {
    const defaultConfigPath = `./config/config.json`;
    try {
      if (fs.existsSync(defaultConfigPath)) {
        const defaultConfigFile = fs.readFileSync(defaultConfigPath).toString();
        const defaultConfig = JSON.parse(defaultConfigFile);
        const path = `./config/config.${environment}.json`;
        try {
          if (fs.existsSync(path)) {
            const file = fs.readFileSync(path).toString();
            const localConfig = JSON.parse(file);
            this.config = mergeObject(defaultConfig, localConfig);
            this.config.multidomain = environment === 'local' ? MultiDomains.LOCAL : MultiDomains.DEFAULT;
            this.config.get = function (key) {
              if (!environment.includes('production')) {
                return key ? getDefaultMultidomainConfig()[key] || '' : getDefaultMultidomainConfig();
              }
              return key ? getDefaultMultidomainConfig(this.user?.domain)[key] || '' : getDefaultMultidomainConfig(this.user?.domain);
            };
            this.config.setUser = user => {
              const _config = { ...this.config };
              _config.user = user;
              return _config;
            };
          } else {
            console.error(`no config file for environment ${process.env.ENVIRONMENT}`);
          }
        } catch (err) {
          console.error(err);
        }
      } else {
        console.error('no default config file');
      }
    } catch (err) {
      console.error(err);
    }
  }

  static getInstance(): ConfigInstance {
    return this._instance || (this._instance = new this());
  }

  public setDomain(domain: MultiDomainsEnum | undefined) {
    if (environment.includes('production') && domain && MultiDomains[domain]) {
      Object.assign(this.config.multidomain, MultiDomains[domain]);
    }
  }

  public setDefault(domain: string) {
    let def = {
      title: 'default',
      domain: process.env.COOKIE_DOMAIN || MultiDomains.DEFAULT.domain,
      ADMIN_LMS_URL: process.env.ADMIN_LMS_URL || MultiDomains.DEFAULT.ADMIN_LMS_URL,
      API_URL: process.env.API_URL || MultiDomains.DEFAULT.API_URL,
      STATIC_URL: process.env.STATIC_URL || MultiDomains.DEFAULT.STATIC_URL,
      TUTOR_LMS_URL: process.env.TUTOR_LMS_URL || MultiDomains.DEFAULT.TUTOR_LMS_URL,
      STUDENT_LMS_URL: process.env.STUDENT_LMS_URL || MultiDomains.DEFAULT.STUDENT_LMS_URL,
      COOKIE_DOMAIN: process.env.COOKIE_DOMAIN || MultiDomains.DEFAULT.COOKIE_DOMAIN,
    };
    for (const d of Object.keys(MultiDomains)) {
      if (domain.toUpperCase().includes(MultiDomains[d as MultiDomainsEnum].domain)) {
        def = MultiDomains[d as MultiDomainsEnum];
        break;
      }
    }
    Object.assign(MultiDomains.DEFAULT, def);
    this.config.multidomain = def;
  }
}

const config: IConfigFile = ConfigInstance.getInstance().config;

export default config;
