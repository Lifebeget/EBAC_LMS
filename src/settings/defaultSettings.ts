const defaultSettings = {
  mainSettings: {
    profileShowCff: true,
    showWebinars: true,
    phoneMask: '+55 (##) #########',
    phoneLength: 17,
    allCoursesLink: 'https://ebaconline.com.br/cursos',
    weekLastDay: 0,
    currency: 'R$',
    language: 'pt',
    currencyName: '',
    onlyAdvancedSubtitles: false,
    webinars: {
      useDefaultSurvey: false,
      defaultSurvey: null,
    },
    forumEbacUserEmail: 'online@ebac.art.br',
  },
  integrationSettings: {
    kaltura: {
      partnerId: '',
      uiconfId: '',
    },
    youtube: {
      apikey: '',
    },
    amo: {
      domain: '',
      clientId: '',
      clientSecret: '',
      redirectUri: '',
      profileWidgetSecret: '',
    },
  },
};

export default defaultSettings;
