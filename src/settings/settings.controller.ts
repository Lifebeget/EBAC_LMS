import { Body, Controller, Get, Patch } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { UpdateSettingsImportDto } from './dto/update-settings-import';
import { UpdateSettingsIntegrationDto } from './dto/update-settings-integration';
import { UpdateSettingsMainDto } from './dto/update-settings-main';
import { SettingsService } from './settings.service';

@ApiTags('settings')
@ApiBearerAuth()
@Controller()
export class SettingsController {
  constructor(private settingsService: SettingsService) {}

  @Get('settings/main')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Get main setting' })
  @ApiResponse({
    status: 200,
  })
  async findMain() {
    const setting = await this.settingsService.getSettings();
    return { ...setting.mainSettings };
  }

  @Patch('settings/main')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Update settings main' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  updateMain(@Body() updateDto: UpdateSettingsMainDto) {
    return this.settingsService.updateMain(updateDto);
  }

  @Get('settings/integration')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Get integration settings' })
  @ApiResponse({
    status: 200,
  })
  async findIntegration() {
    const setting = await this.settingsService.getSettings();
    return {
      ...setting.integrationSettings,
    };
  }

  @Patch('settings/integration')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Update settings integration' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  updateIntegration(@Body() updateDto: UpdateSettingsIntegrationDto) {
    return this.settingsService.updateIntegration(updateDto);
  }

  @Get('settings/import')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Get import settings' })
  @ApiResponse({
    status: 200,
  })
  async findImport() {
    const setting = await this.settingsService.getSettings();
    return {
      ...setting.importSettings,
    };
  }

  @Patch('settings/import')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Update import settings' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  updateImport(@Body() updateDto: UpdateSettingsImportDto) {
    return this.settingsService.updateImport(updateDto);
  }
}
