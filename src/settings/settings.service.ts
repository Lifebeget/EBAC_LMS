import { Settings } from '@entities/settings.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateSettingsImportDto } from './dto/update-settings-import';
import { UpdateSettingsIntegrationDto } from './dto/update-settings-integration';
import { UpdateSettingsMainDto } from './dto/update-settings-main';
import { MergeRecursive } from 'src/helpers';
import defaultSettings from './defaultSettings';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(Settings)
    private settingsRepository: Repository<Settings>,
  ) {}

  async getSettings(): Promise<Settings> {
    const settings = await this.settingsRepository.find();
    return MergeRecursive(defaultSettings, settings[0]);
  }

  async updateMain(updateDto: UpdateSettingsMainDto) {
    const settings = await this.settingsRepository.find();
    const result = await this.settingsRepository.save({
      id: settings[0].id,
      mainSettings: { ...settings[0].mainSettings, ...updateDto },
    });
    return {
      ...result.mainSettings,
    };
  }

  async updateIntegration(updateDto: UpdateSettingsIntegrationDto) {
    const settings = await this.settingsRepository.find();
    const result = await this.settingsRepository.save({
      id: settings[0].id,
      integrationSettings: { ...settings[0].integrationSettings, ...updateDto },
    });
    return {
      ...result.integrationSettings,
    };
  }

  async updateImport(updateDto: UpdateSettingsImportDto) {
    const settings = await this.getSettings();

    const result = await this.settingsRepository.save({
      id: settings.id,
      importSettings: { ...settings.importSettings, ...updateDto },
    });
    return {
      ...result.importSettings,
    };
  }
}
