import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsInt, IsOptional, IsString, IsUUID, ValidateIf, ValidateNested } from 'class-validator';

export class UpdateSettingsWebinarDto {
  @ValidateIf(o => o.hasOwnProperty('useDefaultSurvey'))
  @IsBoolean()
  useDefaultSurvey?: boolean;

  @ValidateIf(o => o.hasOwnProperty('defaultSurvey'))
  @IsUUID()
  @IsOptional()
  defaultSurvey?: string;
}

export class UpdateSettingsMainDto {
  @ValidateIf(o => o.hasOwnProperty('profileShowCff'))
  @IsOptional()
  @ApiProperty({ description: 'profileShowCff' })
  profileShowCff?: boolean;

  @ValidateIf(o => o.hasOwnProperty('showWebinars'))
  @IsOptional()
  @ApiProperty({ description: 'showWebinars' })
  showWebinars?: boolean;

  @ValidateIf(o => o.hasOwnProperty('phoneMask'))
  @IsString()
  @ApiProperty({ description: 'phoneMask' })
  phoneMask?: string;

  @ValidateIf(o => o.hasOwnProperty('phoneLength'))
  @Type(() => Number)
  @IsInt()
  @ApiProperty({ description: 'phoneLength' })
  phoneLength?: number;

  @ValidateIf(o => o.hasOwnProperty('allCoursesLink'))
  @IsString()
  @ApiProperty({ description: 'allCoursesLink' })
  allCoursesLink?: string;

  @ValidateIf(o => o.hasOwnProperty('weekLastDay'))
  @Type(() => Number)
  @IsInt()
  @ApiProperty({ description: 'weekLastDay' })
  weekLastDay?: number;

  @ValidateIf(o => o.hasOwnProperty('currency'))
  @IsString()
  @ApiProperty({ description: 'currency' })
  currency?: string;

  @ValidateIf(o => o.hasOwnProperty('currencyName'))
  @IsString()
  @ApiProperty({ description: 'currencyName' })
  currencyName?: string;

  @ValidateIf(o => o.hasOwnProperty('language'))
  @IsString()
  @ApiProperty({ description: 'language' })
  language?: string;

  @ValidateIf(o => o.hasOwnProperty('onlyAdvancedSubtitles'))
  @IsBoolean()
  @ApiProperty({ description: 'onlyAdvancedSubtitles' })
  onlyAdvancedSubtitles?: boolean;

  @ValidateIf(o => o.hasOwnProperty('webinars'))
  @ValidateNested()
  @Type(() => UpdateSettingsWebinarDto)
  webinars: UpdateSettingsWebinarDto;
}
