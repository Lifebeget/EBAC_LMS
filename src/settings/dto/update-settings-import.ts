import { ApiProperty } from '@nestjs/swagger';
import { ValidateIf } from 'class-validator';

export class UpdateSettingsImportDto {
  @ValidateIf(o => o.hasOwnProperty('userLastUpdate'))
  @ApiProperty({ description: 'Date of last users update' })
  usersLastDownload?: string;

  @ValidateIf(o => o.hasOwnProperty('userLastUpdate'))
  @ApiProperty({ description: 'Date of last users update' })
  enrollmentsLastDownload?: string;
}
