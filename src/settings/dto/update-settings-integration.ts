import { SettingsIntegrationAmo, SettingsIntegrationKaltura, SettingsIntegrationYoutube } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { ValidateIf, ValidateNested } from 'class-validator';

export class UpdateSettingsIntegrationDto {
  @ValidateIf(o => o.hasOwnProperty('kaltura'))
  @ValidateNested()
  @ApiProperty({ description: 'kaltura' })
  kaltura?: SettingsIntegrationKaltura;

  @ValidateIf(o => o.hasOwnProperty('youtube'))
  @ValidateNested()
  @ApiProperty({ description: 'youtube' })
  youtube?: SettingsIntegrationYoutube;

  @ValidateIf(o => o.hasOwnProperty('amo'))
  @ValidateNested()
  @ApiProperty({ description: 'amo' })
  amo?: SettingsIntegrationAmo;
}
