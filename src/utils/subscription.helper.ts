function normalizeName(name: string): string {
  return name.trim();
}

function normalizePhone(phone: string): string {
  return phone.replace(/[^+\d]+/gi, '');
}

function normalizeEmail(email: string): string {
  return email
    .toLowerCase()
    .trim()
    .replace(/\.\./gi, '.')
    .replace(/\.@/gi, '@')
    .replace(/@\./gi, '@')
    .replace(/_@/gi, '@')
    .replace(/@_/gi, '@')
    .replace(/ +/g, '')
    .replace('.@', '@')
    .replace('@.', '@')
    .replace(/@gmail\.co$/gi, '@gmail.com')
    .replace(/@gmeil\.com/gi, '@gmail.com')
    .replace(/@gmai\.com/gi, '@gmail.com')
    .replace(/@gmil\.com/gi, '@gmail.com')
    .replace(/@gamail\.com/gi, '@gmail.com')
    .replace('@gmaiil.com', '@gmail.com')
    .replace('@hotnail.com', '@hotmail.com')
    .replace('@gmajl.com', '@gmail.com')
    .replace('@hitnail.com', '@hotmail.com')
    .replace('@gaiil.com', '@gmail.com')
    .replace('@gm.com', '@gmail.com')
    .replace('@gmail.comeh', '@gmail.com')
    .replace('@gomail.com', '@gmail.com')
    .replace('@gamel.com', '@gmail.com')
    .replace('@gmailgmail.com', '@gmail.com')
    .replace('@gmail.comk', '@gmail.com')
    .replace('@gmail.com.comm.com', '@gmail.com')
    .replace('@htomail.com', '@hotmail.com')
    .replace('@hotemail.com', '@hotmail.com')
    .replace('@yahoo.combr', '@yahoo.com.br')
    .replace('@gimal.com', '@gmail.com')
    .replace('@gmail.con', '@gmail.com')
    .replace('@gmail.comer', '@gmail.com')
    .replace('@gamil.com', '@gmail.com')
    .replace('@gmali.com', '@gmail.com')
    .replace('@gmail.comde', '@gmail.com')
    .replace('@emaio.com', '@email.com')
    .replace('@gamai.com', '@gmail.com')
    .replace('@gtmail.com', '@gmail.com')
    .replace('@gmail.como', '@gmail.com')
    .replace('@gmial.com', '@gmail.com')
    .replace('@gmail.com.brlo', '@gmail.com.br')
    .replace('@gmaoi.com', '@gmail.com')
    .replace('@gmail.co.com', '@gmail.com')
    .replace('@gmil.com', '@gmail.com')
    .replace('@gmail.vom', '@gmail.com')
    .replace('@gmail.cim', '@gmail.com')
    .replace('@g.mail.com', '@gmail.com')
    .replace('@gmlia.com', '@gmail.com')
    .replace('@gm.com', '@gmail.com')
    .replace('@gnail.com', '@gmail.com')
    .replace('@hotmail.con', '@hotmail.com')
    .replace('@gma.com', '@gmail.com')
    .replace('@gamel.com', '@gmail.com')
    .replace('@gmail.come', '@gmail.com')
    .replace('@gmailmail.com', '@gmail.com')
    .replace('@gmail.xom', '@gmail.com')
    .replace('@mail.vom', '@mail.com')
    .replace('@gmailmail.com', '@gmail.com')
    .replace('@gmail.combr', '@gmail.com.br')
    .replace('@gmail.comde', '@gmail.com')
    .replace('@gmail.col', '@gmail.com')
    .replace('@gmail.om', '@gmail.com')
    .replace('@gmail.comm', '@gmail.com')
    .replace('@gmal.com', '@gmail.com')
    .replace('@gmai.com', '@gmail.com')
    .replace('@gmeil.com', '@gmail.com')
    .replace('@gmaio.com', '@gmail.com')
    .replace('@hormail.com', '@hotmail.com')
    .replace('@gamail.com', '@gmail.com')
    .replace('@hotmil.com', '@hotmail.com')
    .replace('@gmaail.com', '@gmail.com')
    .replace('@gmeail.com', '@gmail.com')
    .replace('@gmaill.com', '@gmail.com')
    .replace('@gmail.cm', '@gmail.com')
    .replace('@hitmail.com', '@hotmail.com')
    .replace('@hitmail.com', '@homail.com')
    .replace('@gmail.br', '@gmail.com.br')
    .replace('@gmaul.com', '@gmail.com')
    .replace('@agmail.com', '@gmail.com')
    .replace('@gmail.com.br', '@gmail.com')
    .normalize('NFKD')
    .replace(/[^\w\s+.-_\/\-]/g, '');
}

function normalizeIpAddress(ip: string | null): string | null {
  return ip ? ip.replace('ip=', '') : null;
}

function normalizePromocode(promocode: string | null): string | null {
  return promocode
    ? promocode
        .trim()
        .toLowerCase()
        .replace(/[^a-z0-9\-\_\.]/gim, '')
    : null;
}

export { normalizeName, normalizePhone, normalizeEmail, normalizeIpAddress, normalizePromocode };
