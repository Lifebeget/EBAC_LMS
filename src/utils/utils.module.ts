import { Calendar } from '@entities/calendar.entity';
import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from 'aws-sdk';
import { CalendarsService } from 'src/directory/calendar/calendars.service';
import { DirectoryModule } from 'src/directory/directory.module';
import { DateService } from './date.service';

@Global()
@Module({
  imports: [ConfigModule, DirectoryModule, TypeOrmModule.forFeature([Calendar])],
  providers: [DateService, ConfigService, CalendarsService],
  exports: [DateService, ConfigService, CalendarsService],
})
export class UtilsModule {}
