import { Conditions } from '@entities/segment.entity';
import { User } from '@entities/user.entity';

const operators = {
  equal: '=',
  notEqual: '<>',
  equalMore: '>=',
  equalLess: '<=',
  more: '>',
  less: '<',
};

const generateFilter = (name: string, index = 0, condition: Conditions) => {
  let filters = ``,
    arrayFilter = 0,
    arrayOperator = 'AND';
  if (condition.fact === 'course') {
    if (condition.operator === 'each' || condition.operator === 'oneOf') {
      for (const [i, con] of condition.value.entries()) {
        filters += `,\n${name}__n${index}_${i} AS (
          SELECT DISTINCT user_id FROM active_student_enrollments
          WHERE course_id = '${con}' and role = 'student' and type <> 'trial'
        )`;
      }
      arrayFilter = condition.value.length;
      arrayOperator = condition.operator === 'each' ? 'AND' : 'OR';
    }
    if (condition.operator === 'equal' || condition.operator === 'notEqual') {
      filters += `,\n${name}__n${index} AS (
        SELECT DISTINCT user_id FROM active_student_enrollments
        WHERE course_id = '${condition.value}' and role = 'student' and type <> 'trial'
      )`;
    }
  }
  if (condition.fact === 'courseTags') {
    if (condition.operator === 'each' || condition.operator === 'oneOf') {
      for (const [i, con] of condition.value.entries()) {
        filters += `,\n${name}__n${index}_${i} AS (
          SELECT DISTINCT ase.user_id FROM active_student_enrollments ase
          INNER JOIN course_tags ct ON ct.course_id = ase.course_id
          WHERE ct.tag_id = '${con}' and ase.role = 'student' and ase.type <> 'trial'
        )`;
      }
      arrayFilter = condition.value.length;
      arrayOperator = condition.operator === 'each' ? 'AND' : 'OR';
    }
    if (condition.operator === 'equal' || condition.operator === 'notEqual') {
      filters += `,\n${name}__n${index} AS (
        SELECT DISTINCT user_id FROM active_student_enrollments ase
        INNER JOIN course_tags ct ON ct.course_id = ase.course_id
        WHERE ct.tag_id = '${condition.value}' and ase.role = 'student' and ase.type <> 'trial'
      )`;
    }
  }
  if (condition.fact === 'userTags') {
    if (condition.operator === 'each' || condition.operator === 'oneOf') {
      for (const [i, con] of condition.value.entries()) {
        filters += `,\n${name}__n${index}_${i} AS (
          SELECT DISTINCT user_id FROM user_tags
          WHERE tag_id = '${con}'
        )`;
      }
      arrayFilter = condition.value.length;
      arrayOperator = condition.operator === 'each' ? 'AND' : 'OR';
    }
    if (condition.operator === 'equal' || condition.operator === 'notEqual') {
      filters += `,\n${name}__n${index} AS (
        SELECT DISTINCT user_id FROM user_tags
        WHERE tag_id = '${condition.value}'
      )`;
    }
  }
  if (condition.fact === 'enrollment') {
    if (condition.operator === 'enrollmentRole') {
      filters += `,\n${name}__n${index} AS (
        SELECT DISTINCT user_id FROM active_student_enrollments
        WHERE role ${condition.value === 'student' ? '=' : '<>'} 'student'
      )`;
    }
    if (condition.operator === 'enrollmentType') {
      filters += `,\n${name}__n${index} AS (
        SELECT DISTINCT user_id FROM active_student_enrollments
        WHERE type IN ${condition.value === 'buy' ? `('buy', 'unknown', 'transfer')` : `('${condition.value}')`} ${
        condition.subValue ? `AND course_id = '${condition.subValue}'` : ''
      }
      )`;
    }
  }
  if (condition.fact === 'courseModules') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id FROM active_student_enrollments ase
      LEFT JOIN view_user_progress_count_materialized vumc ON vumc.user_id = ase.user_id AND vumc.course_id = ase.course_id
      WHERE ase.course_id = '${condition.subValue}' 
        AND COALESCE(vumc.modules_required_completed, 0) ${operators[condition.operator]} ${condition.value}
    )`;
  }
  if (condition.fact === 'modules') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id FROM active_student_enrollments ase
      LEFT JOIN view_user_progress_count_materialized vumc ON vumc.user_id = ase.user_id AND vumc.course_id = ase.course_id
      WHERE COALESCE(vumc.modules_required_completed, 0) ${operators[condition.operator]} ${condition.value}
    )`;
  }
  if (condition.fact === 'courseProgress') {
    console.log(condition);
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id FROM active_student_enrollments ase
      LEFT JOIN view_user_progress_count_materialized vumc ON vumc.user_id = ase.user_id AND vumc.course_id = ase.course_id
      WHERE ase.course_id = '${condition.subValue}' AND COALESCE(vumc.progress, 0) ${operators[condition.operator]} ${condition.value}
    )`;
  }
  if (condition.fact === 'progress') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id FROM active_student_enrollments ase
      LEFT JOIN view_user_progress_count_materialized vumc ON vumc.user_id = ase.user_id AND vumc.course_id = ase.course_id
      WHERE COALESCE(vumc.progress, 0) ${operators[condition.operator]} ${condition.value}
    )`;
  }
  if (condition.fact === 'nps') {
    let expression = '';
    if (condition.value === 'detractors') {
      expression = 'score < 7';
    } else if (condition.value === 'neutral') {
      expression = 'score >= 7 and score <= 8';
    } else {
      expression = 'score > 8';
    }
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT nps.user_id::uuid FROM nps
      INNER JOIN (SELECT user_id, MAX(created) created FROM public.nps WHERE score is not null GROUP BY user_id) st ON st.user_id = nps.user_id and nps.created = st.created
      WHERE ${expression}
    )`;
  }
  if (condition.fact === 'created') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT id user_id FROM "user"
      WHERE ${condition.value ? `created >= '${condition.value}'` : ''}
      ${condition.value && condition.subValue ? ' AND ' : ''}
      ${condition.subValue ? `created <= '${condition.subValue}'` : ''}
    )`;
  }
  if (condition.fact === 'courseHomeworks') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id
      FROM active_student_enrollments ase
      LEFT JOIN (
        SELECT DISTINCT user_id, course_id, COUNT(*) FROM submission 
        WHERE status = 'graded' AND current_attempt
        GROUP BY user_id, course_id
      ) s ON ase.user_id = s.user_id AND ase.course_id = s.course_id
      WHERE ase.course_id = '${condition.subValue}' AND COALESCE(s.count, 0) ${operators[condition.operator]} ${condition.value}
      GROUP BY ase.user_id
    )`;
  }
  if (condition.fact === 'homeworks') {
    filters += `,\n${name}__n${index} AS (
      SELECT DISTINCT ase.user_id
      FROM active_student_enrollments ase
      LEFT JOIN (
        SELECT DISTINCT user_id, COUNT(*) FROM submission 
        WHERE status = 'graded' AND current_attempt
        GROUP BY user_id
      ) s ON ase.user_id = s.user_id
      WHERE COALESCE(s.count, 0) ${operators[condition.operator]} ${condition.value}
      GROUP BY ase.user_id
    )`;
  }
  return {
    filter: filters,
    name: `${name}__n${index}`,
    arrayFilter,
    arrayOperator,
    condition,
  };
};

export const buildConditionQuery = (conditions: Conditions, first = true, name = 'filter', lvl = 0, user: User = null) => {
  const filters = [];
  let all = false;
  if (conditions.all || conditions.any) {
    for (const item of conditions.all || conditions.any) {
      if (item.all || item.any) {
        filters.push({ subFilter: buildConditionQuery(item, false, `${name}__n${filters.length}_l${lvl + 1}`, lvl + 1, user) });
      } else {
        if (item.fact !== 'all') {
          filters.push(generateFilter(name, filters.length, item));
        } else {
          all = true;
        }
      }
    }
  }
  let query =
    lvl > 0
      ? ''
      : `WITH active_student_enrollments AS (
    SELECT e.* FROM enrollment e
    LEFT JOIN "user" u ON u.id = e.user_id
    WHERE e.active = true
        AND (e.date_from IS NULL OR e.date_from < NOW())
        AND (e.date_to IS NULL OR e.date_to > NOW())
        AND u.active
        ${user ? `AND u.id = '${user.id}'` : ''}
  )`;
  let join = lvl > 0 ? '' : `\nSELECT COUNT(*) FROM "user" u`;
  let where = lvl > 0 ? '' : `\nWHERE u.active ${user ? `AND u.id = '${user.id}'` : ''}`;
  if (filters.length === 0 && !all) {
    where += ' AND 1=0';
  } else if (lvl === 0 && !all) {
    where += ` AND ( `;
  }
  for (const [index, filter] of filters.entries()) {
    if (!filter.filter && !filter.subFilter) {
      break;
    }
    query += filter.filter ? filter.filter : '';
    const operator = conditions.all ? 'AND' : 'OR';
    if (filter.arrayFilter) {
      where += where === '' ? '(' : `\n${index > 0 ? operator : ''} (`;
      for (let i = 0; i < filter.arrayFilter; i++) {
        join += `\nLEFT JOIN ${filter.name}_${i} ON u.id = ${filter.name}_${i}.user_id`;
        where += `${i > 0 ? ' ' + filter.arrayOperator : ''} ${filter.name}_${i} IS NOT NULL`;
      }
      where += ')';
    } else if (filter.subFilter) {
      query += filter.subFilter[0];
      join += filter.subFilter[1];
      where += (where === '' ? '' : `\n${index > 0 ? operator : ''}`) + ` (${filter.subFilter[2]})`;
    } else {
      join += `\nLEFT JOIN ${filter.name} ON u.id = ${filter.name}.user_id`;
      if (['course', 'courseTags', 'userTags'].includes(filter.condition.fact) && filter.condition.operator === 'notEqual') {
        where += (where === '' ? '' : `\n${index > 0 ? operator : ''}`) + ` ${filter.name}.user_id IS NULL`;
      } else {
        where += (where === '' ? '' : `\n${index > 0 ? operator : ''}`) + ` ${filter.name}.user_id IS NOT NULL`;
      }
    }
  }
  if (first) {
    query += `${join}\n${where}${filters.length === 0 || all ? '' : ')'}`;
    return query;
  } else {
    return [query, join, where];
  }
};
