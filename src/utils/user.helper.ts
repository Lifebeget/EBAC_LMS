import { systemRolesHierarchy } from '@enums/permission.enum';
import { SystemRole } from '@lib/types/enums';

export function checkRoleset(current: SystemRole[], roles: SystemRole[]) {
  if (!roles) {
    // No roles to check for, allowed
    return true;
  }
  if (!current) {
    // No current roles, not allowed
    return false;
  }
  const allowedRoles = current.reduce((acc, cur) => {
    if (systemRolesHierarchy[cur]) {
      acc = acc.concat(systemRolesHierarchy[cur]);
    }
    return acc;
  }, [] as SystemRole[]);

  return roles.every(role => allowedRoles.includes(role));
}
