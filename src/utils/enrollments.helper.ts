import { Injectable } from '@nestjs/common';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { CoursesService } from 'src/courses/courses.service';

@Injectable()
export class EnrollmentsHelper {
  constructor(private coursesService: CoursesService, private enrollmentsService: EnrollmentsService) {}

  async setGraduatedDate(userId, courseId) {
    const progress = await this.coursesService.viewUserProgressCount(userId, courseId);
    if (!progress[0]?.graduated) {
      return;
    }

    const course = await this.coursesService.findOne(courseId);
    const requiredSatelliteIds = course.toSatelliteCourses.filter(c => c.required).map(c => c.satelliteCourseId);
    if (requiredSatelliteIds.length) {
      const count = await this.enrollmentsService.countOfGraduatedEnrollmentsByStudentAndCourses(userId, requiredSatelliteIds);
      if (requiredSatelliteIds.length > count) {
        return;
      }
    }

    const enrollments = await this.enrollmentsService.findByStudentAndCourse(userId, courseId);
    const graduatedAt = new Date();
    for (const enrollment of enrollments) {
      if (!enrollment.graduatedAt) {
        this.enrollmentsService.update(enrollment.id, { graduatedAt });
      }
    }
  }
}
