import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DateCalculator } from '@lib/helpers/date/date-calculator';
import * as dayjs from 'dayjs';
import { CalendarsService } from 'src/directory/calendar/calendars.service';
import { Calendar } from '@entities/calendar.entity';

@Injectable()
export class DateService {
  private weekendDays: number[];
  private readonly urgentHours: number;
  private readonly outdatedHours: number;
  private readonly pauseHours: number;

  private readonly calculator: DateCalculator;

  public readonly MIN_DATE = '1970-01-01';
  public readonly MAX_DATE = '9999-12-31';

  public calendar: Calendar;

  constructor(configService: ConfigService, private readonly calendarService: CalendarsService) {
    this.weekendDays = configService.get<number[]>('date.weekendDays');
    this.urgentHours = configService.get<number>('submission.urgentHours');
    this.outdatedHours = configService.get<number>('submission.outdatedHours');
    this.pauseHours = configService.get<number>('submission.pauseHours');
    this.calculator = new DateCalculator(this.weekendDays, dayjs);
  }

  current(): Date {
    return dayjs().utcOffset(0, false).toDate();
  }

  createDate(date: Date | string): Date {
    return dayjs(date).utcOffset(0, false).toDate();
  }

  endOfCurrentDay(): Date {
    return this.endOfDay(this.current());
  }

  startOfCurrentDay(): Date {
    return this.startOfDay(this.current());
  }

  endOfDay(date: Date | string, offsetHours = 0): Date {
    return dayjs(date).endOf('day').add(offsetHours, 'hour').toDate();
  }

  startOfDay(date: Date | string, offsetHours = 0): Date {
    return dayjs(date).startOf('day').add(offsetHours, 'hour').toDate();
  }

  subtractDays(date: Date, days: number): Date {
    return dayjs(date).subtract(days, 'day').toDate();
  }

  yesterday(date: Date): Date {
    return dayjs(date).subtract(1, 'day').toDate();
  }

  addDays(date: Date, days: number): Date {
    return dayjs(date).add(days, 'day').toDate();
  }

  tomorrow(date: Date): Date {
    return dayjs(date).add(1, 'day').toDate();
  }

  async subtractBusinessHours(date: Date, hours: number): Promise<Date> {
    const calendars = await this.calendarService.getCalendars();
    return this.calculator.setCalendars(calendars).subtractBusinessHours(date, hours);
  }

  async addBusinessHours(date: Date, hours: number): Promise<Date> {
    const calendars = await this.calendarService.getCalendars();
    return this.calculator.setCalendars(calendars).addBusinessHours(date, hours);
  }

  subtractHours(date: Date, hours: number): Date {
    return dayjs(date).subtract(hours).toDate();
  }

  getUrgentDate(currentDate: Date) {
    return this.subtractBusinessHours(currentDate, this.urgentHours);
  }

  getOutdatedDate(currentDate: Date) {
    return this.subtractBusinessHours(currentDate, this.outdatedHours);
  }

  getPauseTime() {
    return dayjs().subtract(this.pauseHours, 'hours').toDate();
  }
}
