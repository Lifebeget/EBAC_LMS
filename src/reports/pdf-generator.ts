import { Response } from 'express';
import { ReportGenerator } from './report-generator';
import * as PDFDocument from 'pdfkit-table';
import { DataLoader } from './data-loader';

export abstract class PdfGenerator implements ReportGenerator {
  protected doc: PDFDocument;
  protected loader: DataLoader;

  async generate<Query>(query: Query, resp: Response, loader: DataLoader) {
    this.doc = new PDFDocument({ margin: 30, size: 'A4' });
    this.loader = loader;
    await this.generateReport(query);
    this.pipe(resp);
  }

  protected abstract generateReport<Query>(query: Query);

  createTable(table: any) {
    table.headers = table.headers.map(h => ({
      ...this.headerDefault,
      ...h,
    }));
    this.doc.table(table, {
      padding: 3,
      divider: {
        vertical: { disabled: false, width: 0.5, opacity: 1 },
        horizontal: { disabled: false, width: 0.5, opacity: 1 },
      },
      prepareHeader: () => this.doc.font('Helvetica-Bold').fontSize(8),
      prepareRow: () => {},
    });

    return this;
  }

  pipe(resp: Response) {
    resp.set('Content-Type', 'application/pdf');
    resp.set('Content-Disposition', 'attachment; filename=tutor-report.pdf');

    this.doc.pipe(resp);
    this.doc.end();

    return this;
  }

  get headerDefault() {
    return {
      headerColor: 'white',
    };
  }
}
