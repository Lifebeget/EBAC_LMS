export interface DataLoader {
  load<Query>(query: Query): Promise<any>;
}
