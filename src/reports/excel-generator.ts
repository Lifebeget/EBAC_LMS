import * as ExcelJS from 'exceljs';
import * as dayjs from 'dayjs';

import { DataLoader } from './data-loader';
import { ReportGenerator } from './report-generator';
import { Response } from 'express';

export abstract class ExcelGenerator implements ReportGenerator {
  protected workbook: ExcelJS.Workbook;
  protected loader: DataLoader;

  protected readonly NumberFormat = {
    Integer: '#,##0;(#,##0)',
    Money: '#,##0.00;(#,##0.00)',
  };

  protected readonly Fill: Record<string, ExcelJS.FillPattern> = {
    Negative: {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'fee7e9' },
    },
    Positive: {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'ecf5ea' },
    },
    Gray: {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'ebebeb' },
    },
  };

  protected readonly defaultBorderColor: Partial<ExcelJS.Color> = {
    argb: '9B9B9B',
  };

  protected readonly defaultBorder: Partial<ExcelJS.Borders> = {
    top: { style: 'thin', color: this.defaultBorderColor },
    left: { style: 'thin', color: this.defaultBorderColor },
    bottom: { style: 'thin', color: this.defaultBorderColor },
    right: { style: 'thin', color: this.defaultBorderColor },
  };

  protected readonly defaultFont: Partial<ExcelJS.Font> = {
    name: 'Calibri',
    family: 2,
  };

  protected readonly defaultAlignment: Partial<ExcelJS.Alignment> = {
    vertical: 'middle',
    wrapText: true,
  };

  constructor() {
    this.workbook = new ExcelJS.Workbook();
  }
  protected abstract generateReport<Query>(query: Query);

  async generate<Query>(query: Query, resp: Response, loader: DataLoader) {
    this.workbook = new ExcelJS.Workbook();
    this.loader = loader;
    await this.generateReport(query);
    this.pipe(resp);
  }

  protected pipe(resp) {
    resp.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    resp.set('Content-Disposition', 'attachment; filename=tutor-report.xlsx');

    this.workbook.xlsx.write(resp);
  }

  protected addWorksheet(name: string) {
    return this.workbook.addWorksheet(name, {
      pageSetup: {
        fitToWidth: 1,
        fitToHeight: 999999,
      },
    });
  }

  protected addRow(worksheet: ExcelJS.Worksheet, rowData: any) {
    const row = worksheet.addRow(rowData);
    this.setRowDefaults(row);
    return row;
  }

  protected setRowDefaults(row: ExcelJS.Row) {
    this.setRowStyle(row, {
      alignment: this.defaultAlignment,
      border: this.defaultBorder,
      font: this.defaultFont,
    });
  }
  protected setHeaderDefaults(row: ExcelJS.Row, bold = true, height?: number) {
    this.setRowStyle(row, {
      alignment: { vertical: 'middle', horizontal: 'center', wrapText: true },
      border: this.defaultBorder,
      font: { ...this.defaultFont, bold: bold },
    });

    if (height) {
      row.height = height;
    }
  }

  protected setRowStyle(row: ExcelJS.Row, style: Partial<ExcelJS.Style>) {
    row.eachCell(cell => {
      const origin = cell.style;
      cell.style = { ...cell.style, ...style };
      cell.style.alignment = { ...origin.alignment, ...style.alignment };
    });
  }

  protected setRowFill(row: ExcelJS.Row, fill: ExcelJS.Fill) {
    row.eachCell(cell => {
      cell.fill = fill;
    });
  }

  protected setRowBold(row: ExcelJS.Row) {
    row.font = { name: 'Calibri', bold: true };
  }

  protected setNumber(cell: ExcelJS.Cell, value: number, format: string) {
    cell.value = value ?? '―';
    cell.numFmt = format;
    cell.alignment = { horizontal: 'right' };

    return cell;
  }

  protected setInteger(cell: ExcelJS.Cell, value: number) {
    return this.setNumber(cell, value, this.NumberFormat.Integer);
  }

  protected setMoney(cell: ExcelJS.Cell, value: number) {
    return this.setNumber(cell, value, this.NumberFormat.Money);
  }

  protected setHeader(ws: ExcelJS.Worksheet, title: string, query: { year?: number; month?: number; locale?: string }) {
    const leftCell = ws.getCell(1, 1);
    leftCell.value = title;
    leftCell.font = { bold: true };

    const rightCell = ws.getCell(1, ws.columnCount);
    rightCell.value = dayjs(`${query.year}-${query.month}-01`).locale(query.locale).format('MMMM YYYY');
    rightCell.font = { bold: true };

    if (ws.columnCount > 2) {
      rightCell.alignment = { horizontal: 'right' };
    }
  }
}
