import { ReportApprove } from '@entities/report-approve.entity';
import { ReportType } from '@lib/api/reports';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export interface ApproveParams {
  period: string;
  reportType: ReportType;
  approverId: string;
}

export interface FindParams {
  period: string;
  reportType: ReportType;
}

@Injectable()
export class ReportApproveService {
  constructor(
    @InjectRepository(ReportApprove)
    private readonly repo: Repository<ReportApprove>,
  ) {}

  async approve(params: ApproveParams) {
    return this.repo.save(params);
  }

  async revoke(params: FindParams) {
    return this.repo.delete(params);
  }

  async isApproved(params: FindParams) {
    return (await this.repo.count(params)) > 0;
  }
}
