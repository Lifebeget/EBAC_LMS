import { Body, ClassSerializerInterceptor, Controller, Get, Param, Post, Query, Req, Res, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PgConstraints } from 'src/pg.decorators';
import { ReportsService } from './reports.service';
import { Permission } from '@enums/permission.enum';
import { QueryReportDto } from './dto/query-report.dto';
import { ReportFormat, ReportName } from '@lib/api/reports';
import { SaveReportDto } from './dto/save-report.dto';
import { ApproveReportDto } from './dto/approve-report.dto';
@ApiTags('report')
@ApiBearerAuth()
@Controller()
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('reports/generate/:reportName/:reportFormat')
  @ApiOperation({ summary: 'Generate report' })
  async generate(
    @Param('reportName') reportName: string,
    @Param('reportFormat') reportFormat: ReportFormat,
    @Query() query: QueryReportDto,
    @Req() req,
    @Res() res,
  ) {
    if (!req.user.hasPermission(Permission.REPORT_VIEW)) {
      query.tutorId = req.user.id;
    }
    const result = await this.reportsService.generate(query, res, reportName, reportFormat);
    return result;
  }

  @Get('reports/generate/:reportName/:reportFormat/stored')
  @ApiOperation({ summary: 'Generate report' })
  async generateStored(
    @Param('reportName') reportName: string,
    @Param('reportFormat') reportFormat: ReportFormat,
    @Query() query: QueryReportDto,
    @Req() req,
    @Res() res,
  ) {
    if (!req.user.hasPermission(Permission.REPORT_VIEW)) {
      query.tutorId = req.user.id;
    }
    const result = await this.reportsService.generate(query, res, reportName, reportFormat, true);
    return result;
  }

  @Get('reports/:reportName/stored')
  @ApiOperation({ summary: 'Fetch stored report data' })
  @UseInterceptors(ClassSerializerInterceptor)
  @PgConstraints()
  async fetchStored(@Param('reportName') reportName: string, @Query() query: QueryReportDto) {
    const data = await this.reportsService.load(query, reportName, true);
    return data;
  }

  @Get('reports/:reportName/periods')
  @ApiOperation({ summary: 'Fetch stored report data' })
  @UseInterceptors(ClassSerializerInterceptor)
  @PgConstraints()
  async fetchPeriods(@Param('reportName') reportName: string, @Query() query: QueryReportDto) {
    const data = await this.reportsService.loadPeriods(query, reportName);
    return data;
  }

  @Get('reports/:reportName')
  @ApiOperation({ summary: 'Fetch realtime report data' })
  @UseInterceptors(ClassSerializerInterceptor)
  @PgConstraints()
  async fetch(@Param('reportName') reportName: string, @Query() query: QueryReportDto | QueryReportDto) {
    const data = await this.reportsService.load(query, reportName);
    return data;
  }

  @Post('reports/:reportName')
  @ApiOperation({ summary: 'Save report data' })
  @UseInterceptors(ClassSerializerInterceptor)
  @PgConstraints()
  async save(@Param('reportName') reportName: string, @Body() dto: SaveReportDto) {
    return this.reportsService.save(dto, reportName);
  }

  @Post('reports/:reportName/approve')
  @ApiOperation({ summary: 'Approve report' })
  @PgConstraints()
  async approve(@Param('reportName') reportName: ReportName, @Body() dto: ApproveReportDto, @Req() req) {
    return this.reportsService.approve(dto, reportName, req.user.id);
  }

  @Post('reports/:reportName/revoke')
  @ApiOperation({ summary: 'Revoke report approve' })
  @PgConstraints()
  async revoke(@Param('reportName') reportName: ReportName, @Body() dto: ApproveReportDto) {
    return this.reportsService.revoke(dto, reportName);
  }
}
