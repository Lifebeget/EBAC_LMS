import { ManagerPaymentsExcel, PaymentsLoader, TutorPaymentsExcel, TutorPaymentsPdf, TutorReportLoader } from './payments';

import { BonusConfig } from '@entities/bonus-config.entity';
import { BonusConfigsService } from './bonuses/bonus-configs.service';
import { BonusExcel } from './bonuses/generators/bonus.excel';
import { BonusLoader } from './bonuses/loaders/bonus.loader';
import { BonusStore } from './bonuses/bonus-store';
import { CorrectionsLoader } from './payments/loaders/corrections.loader';
import { DataLoader } from './data-loader';
import { DirectoryModule } from 'src/directory/directory.module';
import { ForumPaymentsLoader } from './payments/loaders/forum-payments.loader';
import { ManagerPaymentsLoader } from './payments/loaders';
import { ManagerPaymentsPdf } from './payments/generators';
import { Module } from '@nestjs/common';
import { PaymentCorrection } from '@entities/payment-correction.entity';
import { PaymentCorrectionsController } from './payments/payment-corrections.controller';
import { PaymentCorrectionsService } from './payments/payment-corrections.service';
import { PaymentsStore } from './payments/payments-store';
import { ReportApprove } from '@entities/report-approve.entity';
import { ReportApproveService } from './report-approve.service';
import { ReportStore } from './report-store';
import { ReportsController } from './reports.controller';
import { ReportsService } from './reports.service';
import { StoredBonus } from '@entities/stored-bonus.entity';
import { StoredBonusLoader } from './bonuses/loaders/stored-bonus.loader';
import { StoredPayment } from '@entities/stored-payment.entity';
import { StoredPaymentsLoader } from './payments/loaders/stored-payments.loader';
import { StoredPeriodsLoader } from './payments/loaders/stored-periods.loader';
import { SubmissionPaymentsLoader } from './payments/loaders/submission-payments.loader';
import { TeamleadFeesLoader } from './payments/loaders/teamlead-fees.loader';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentCorrection, BonusConfig, StoredPayment, StoredBonus, ReportApprove]), DirectoryModule],
  controllers: [ReportsController, PaymentCorrectionsController],
  providers: [
    ReportsService,
    BonusConfigsService,
    PaymentCorrectionsService,

    PaymentsStore,
    BonusStore,

    ForumPaymentsLoader,
    SubmissionPaymentsLoader,
    TeamleadFeesLoader,
    CorrectionsLoader,

    PaymentsLoader,
    StoredPaymentsLoader,
    {
      provide: 'PAYMENT_PERIODS_LOADER',
      useFactory: (store: ReportStore) => new StoredPeriodsLoader(store),
      inject: [PaymentsStore],
    },

    {
      provide: 'BONUS_PERIODS_LOADER',
      useFactory: (store: ReportStore) => new StoredPeriodsLoader(store),
      inject: [BonusStore],
    },

    {
      provide: 'TUTOR_REPORT_LOADER',
      useFactory: (commonPaymentsLoader: DataLoader) => new TutorReportLoader(commonPaymentsLoader),
      inject: [PaymentsLoader],
    },
    {
      provide: 'TUTOR_REPORT_LOADER_STORED',
      useFactory: (commonPaymentsLoader: DataLoader) => new TutorReportLoader(commonPaymentsLoader),
      inject: [StoredPaymentsLoader],
    },

    TutorPaymentsPdf,
    TutorPaymentsExcel,

    {
      provide: 'MANAGER_PAYMENTS_LOADER',
      useFactory: (commonPaymentsLoader: DataLoader) => new ManagerPaymentsLoader(commonPaymentsLoader),
      inject: [PaymentsLoader],
    },
    {
      provide: 'MANAGER_PAYMENTS_LOADER_STORED',
      useFactory: (commonPaymentsLoader: DataLoader) => new ManagerPaymentsLoader(commonPaymentsLoader),
      inject: [StoredPaymentsLoader],
    },
    ManagerPaymentsExcel,
    ManagerPaymentsPdf,

    BonusLoader,
    StoredBonusLoader,
    BonusExcel,

    ReportApproveService,
  ],
})
export class ReportsModule {}
