import { Response } from 'express';
import { DataLoader } from './data-loader';

export interface ReportGenerator {
  generate<Query>(query: Query, resp: Response, loader: DataLoader);
}
