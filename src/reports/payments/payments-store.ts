import { ReportApprove } from '@entities/report-approve.entity';
import { StoredPayment } from '@entities/stored-payment.entity';
import { BonusAmount, PaymentCorrection, PaymentRecord, PaymentType, getPeriodName, TeamleadFee, ReportType } from '@lib/api/reports';
import { QueryReportPeriodsDto } from '@lib/api/reports/dto/query-report-periods.dto';
import { NumberFormat } from '@lib/helpers/format';
import { DatePart } from '@lib/types/enums/date-part.enum';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { filterByUserCourses } from 'src/query-helpers';
import { Repository } from 'typeorm';
import { LoadReportDto } from '../dto/load-report.dto';
import { SaveReportDto } from '../dto/save-report.dto';
import { ReportStore } from '../report-store';
import { PaymentsLoader } from './loaders';

@Injectable()
export class PaymentsStore implements ReportStore {
  constructor(
    private readonly paymentsLoader: PaymentsLoader,
    @InjectRepository(StoredPayment)
    private readonly repository: Repository<StoredPayment>,
  ) {}

  async load({ year, month, tutorId, asRole }: LoadReportDto) {
    const queryBuilder = this.repository
      .createQueryBuilder('b')
      .leftJoinAndSelect('b.user', 'u')
      .leftJoinAndSelect('b.level', 'l')
      .where('period = :period', { period: getPeriodName({ year, month }) });

    if (tutorId) {
      filterByUserCourses(queryBuilder, tutorId, asRole, 'b', 'date', 'user_id');
    }

    const result = await queryBuilder.getMany();
    return result;
  }

  async loadPeriods({ periodStart, periodEnd, tutorId }: QueryReportPeriodsDto) {
    const queryBuilder = this.repository
      .createQueryBuilder('p')
      .select(['p.period as period', 'sum(p.money) as amount', 'sum(p.value) as cnt', 'a.period is not null as approved'])
      .leftJoin(ReportApprove, 'a', 'a.period = p.period and a.reportType = :reportType', { reportType: ReportType.Payment })
      .where('"type" != :total', { total: PaymentType.Total })
      .groupBy('p.period, a.period');

    if (periodStart) {
      queryBuilder.andWhere('p.date >= :periodStart', { periodStart });
    }

    if (periodEnd) {
      queryBuilder.andWhere('p.date <= :periodEnd', { periodEnd });
    }

    if (tutorId) {
      queryBuilder.andWhere('p.user_id = :tutorId', { tutorId });
    }

    const result = await queryBuilder.getRawMany();

    return result.map(r => {
      const [year, month] = r.period.split('-');

      return {
        ...r,
        year: Number(year),
        month: Number(month),
        hasData: true,
      };
    });
  }

  async save({ year, month, tutorId }: SaveReportDto) {
    const where: any = {
      period: getPeriodName({ year, month }),
    };

    if (tutorId) {
      where.userId = tutorId;
    }

    const paymentsData = await this.paymentsLoader.load({ year, month, tutorId, groupBy: DatePart.Day });

    await this.repository.delete(where);

    let toStore = paymentsData.tutorPayments.map(payment => this.fromPayment(payment, year, month));
    toStore = toStore.concat(paymentsData.teamleadFees.map(fee => this.fromTeamleadFee(fee, year, month)));
    toStore = toStore.concat(paymentsData.bonus.courseBonuses.map(bonus => this.fromTutorBonus(bonus, year, month)));
    toStore = toStore.concat(paymentsData.corrections.map(correction => this.fromCorrection(correction, year, month)));

    await this.repository.save(toStore, { chunk: 1000 });
  }

  async getTutorIds(period: string) {
    const result = await this.repository
      .createQueryBuilder('p')
      .select('user_id')
      .distinct(true)
      .where('period = :period', { period })
      .getRawMany();

    return result.map(r => r.id);
  }

  private fromPayment(record: PaymentRecord, year: number, month: number): StoredPayment {
    return {
      courseId: record.courseId,
      levelId: record.levelId,
      money: record.amount,
      period: getPeriodName({ year, month }),
      title: record.courseTitle,
      userId: record.tutorId,
      value: record.cnt,
      type: PaymentType.Submission,
      date: record.dt,
      sources: record.sources,
    };
  }

  private fromTeamleadFee(fee: TeamleadFee, year: number, month: number): StoredPayment {
    return {
      courseId: fee.courseId,
      money: fee.amount,
      period: getPeriodName({ year, month }),
      title: fee.courseTitle,
      userId: fee.tutorId,
      value: 0,
      type: PaymentType.TeamleadBonus,
      date: this.date(year, month),
    };
  }

  private fromTutorBonus(bonus: Partial<BonusAmount>, year: number, month: number): StoredPayment {
    return {
      courseId: bonus.courseId,
      money: bonus.amount,
      period: getPeriodName({ year, month }),
      title: bonus.courseTitle,
      userId: bonus.tutorId,
      value: bonus.bonusPercent,
      type: PaymentType.TutorBonus,
      date: this.date(year, month),
    };
  }

  private fromCorrection(correction: PaymentCorrection, year: number, month: number): StoredPayment {
    return {
      money: correction.value,
      period: getPeriodName({ year, month }),
      title: correction.reason,
      userId: correction.userId,
      value: correction.value,
      type: PaymentType.Correction,
      date: correction.date as string,
    };
  }

  private date(year: number, month: number) {
    const format = new NumberFormat();
    return `${year}-${format.prependZero(month)}-01`;
  }
}
