import {
  ManagerPaymentRecord,
  PaymentCorrection,
  PaymentRecord,
  PaymentsManager,
  PaymentsReportRow,
  Rows,
  TeamleadFee,
} from '@lib/api/reports';

import { DataLoader } from '../../data-loader';
import { Injectable } from '@nestjs/common';
import { QueryReportDto } from '../../dto/query-report.dto';
import { ServiceLevel } from '@lib/api/directory/service-levels';

@Injectable()
export class ManagerPaymentsLoader implements DataLoader {
  constructor(private readonly paymentsLoader: DataLoader) {}

  private tutorPayments: PaymentRecord[];
  private levels: ServiceLevel[];
  private teamleadFees: TeamleadFee[];
  private corrections: PaymentCorrection[];
  private courseBonuses: any[];
  private tutors: Rows;

  async load(query: QueryReportDto) {
    const data = await this.paymentsLoader.load(query);

    this.tutorPayments = data.tutorPayments;
    this.levels = data.levels;
    this.teamleadFees = data.teamleadFees;
    this.corrections = data.corrections;
    this.courseBonuses = data.bonus.courseBonuses;

    const pm = new PaymentsManager(this.levels);
    this.tutors = pm.groupByTutorAndCourse(this.tutorPayments);

    const tutorPayments = [];

    this.addTutorPayments(tutorPayments);
    this.addRemainingTeamleadFees(tutorPayments);
    this.addRemainingCorrections(tutorPayments);

    tutorPayments.sort((a, b) => a.tutorName.localeCompare(b.tutorName));

    return {
      ...data,
      tutorPayments,
      isEmpty: tutorPayments.length == 0,
    };
  }

  private addTutorPayments(target: ManagerPaymentRecord[]) {
    Object.keys(this.tutors).forEach(tutorKey => {
      const tutor = this.tutors[tutorKey];

      const levels = this.tutorLevelsAsObject(tutor);
      const correctionValue = this.getCorrectionValue(tutor.id);

      const { courses, teamleadFee, tutorBonus } = this.patchCourses(tutor.childs, tutor.id);

      target.push({
        tutorName: tutor.title,
        tutorId: tutor.id,
        levels: levels,
        courses,
        amount: tutor.summary.amount,
        teamleadFee: teamleadFee,
        bonus: tutorBonus,
        correctionValue,
        total: Number(tutor.summary.amount) + Number(teamleadFee) + Number(correctionValue) + Number(tutorBonus),
      });
    });
  }

  private addRemainingTeamleadFees(target: ManagerPaymentRecord[]) {
    this.teamleadFees.forEach((fee: TeamleadFee) => {
      let record = target.find(this.forTutor(fee.tutorId));

      if (!record) {
        const correctionValue = this.getCorrectionValue(fee.tutorId);

        record = {
          tutorName: fee.tutorName,
          tutorId: fee.tutorId,
          correctionValue,
          total: Number(correctionValue),
          teamleadFee: 0,
          courses: Object.create(null),
        } as any;
        target.push(record);
      }

      // record.courses = record.courses ?? [];
      // record.courses.push({ id: fee.courseId, title: fee.courseTitle, teamleadFee: fee.amount, total: fee.amount });

      record.courses[fee.courseId] = { id: fee.courseId, title: fee.courseTitle, teamleadFee: fee.amount, total: fee.amount };

      record.teamleadFee += Number(fee.amount);
      record.total += Number(fee.amount);
    });
  }

  private addRemainingCorrections(target: ManagerPaymentRecord[]) {
    this.corrections.forEach(correction => {
      target.push({
        tutorId: correction.userId,
        tutorName: correction.userName,
        correctionValue: correction.value,
        total: correction.value,
      } as any);
    });
  }

  private patchCourses(courses: Record<string, PaymentsReportRow>, tutorId: string) {
    let teamleadFee = 0;
    let tutorBonus = 0;

    Object.keys(courses).forEach(key => {
      const course = courses[key];

      course.total = course.summary.amount;
      course.amount = course.summary.amount;

      const fee = this.getFeeAmount(tutorId, key);
      course.teamleadFee = fee;
      teamleadFee += fee;

      const bonus = this.getBonus(tutorId, key);
      course.bonus = bonus;
      tutorBonus += bonus;

      course.total += fee + bonus;
    });

    const feeIndex = this.teamleadFees.findIndex(this.forTutor(tutorId));

    while (this.teamleadFees[feeIndex]?.tutorId === tutorId) {
      const [fee] = this.teamleadFees.splice(feeIndex, 1);

      courses[fee.courseId] = {
        id: fee.courseId,
        title: fee.courseTitle,
        teamleadFee: fee.amount,
        total: fee.amount,
      } as any;

      teamleadFee += Number(fee.amount);
    }

    return {
      courses,
      teamleadFee,
      tutorBonus,
    };
  }

  private getCorrectionValue(tutorId: string) {
    const tutorCorrections = this.corrections.filter(c => c.userId == tutorId);

    if (tutorCorrections.length > 0) {
      this.corrections.splice(0, this.corrections.length, ...this.corrections.filter(c => c.userId !== tutorId));
    }

    return tutorCorrections.reduce((sumValue, c) => sumValue + c.value, 0);
  }

  private getFeeAmount(tutorId: string, courseId: string) {
    const feeIndex = this.teamleadFees.findIndex(this.forTutorAndCourse(tutorId, courseId));

    if (feeIndex >= 0) {
      const [fee] = this.teamleadFees.splice(feeIndex, 1);
      return Number(fee.amount);
    }
    return 0;
  }

  private tutorLevelsAsObject(tutorRecord: PaymentsReportRow) {
    return this.levels.reduce(
      (onlyCnt, l) => ({
        ...onlyCnt,
        [l.id]: tutorRecord.levels[l.id],
      }),
      {},
    );
  }

  private getBonus(tutorId: string, courseId: string) {
    const tutorBonusAmounts = this.courseBonuses.filter(this.forTutorAndCourse(tutorId, courseId));
    return tutorBonusAmounts.reduce((a, b) => a + Number(b.amount), 0);
  }

  forTutorAndCourse(tutorId: string, courseId: string) {
    return (item: { tutorId: string; courseId: string }) => item.tutorId === tutorId && item.courseId === courseId;
  }

  forTutor(tutorId: string) {
    return (item: { tutorId: string }) => item.tutorId === tutorId;
  }
}
