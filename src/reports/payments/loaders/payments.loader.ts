import { ServiceLevel } from '@lib/api/directory/service-levels';
import { CommonPaymentsData, PaymentRecord } from '@lib/api/reports';
import { EntireMonthPeriod } from '@lib/helpers/date';
import { Injectable } from '@nestjs/common';
import { TariffsService } from 'src/directory/service-level/tariffs.service';
import { BonusLoader } from 'src/reports/bonuses/loaders/bonus.loader';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { CorrectionsLoader } from './corrections.loader';
import { ForumPaymentsLoader } from './forum-payments.loader';
import { SubmissionPaymentsLoader } from './submission-payments.loader';
import { TeamleadFeesLoader } from './teamlead-fees.loader';

@Injectable()
export class PaymentsLoader implements DataLoader {
  private _groupByCourse = false;

  constructor(
    private readonly tariffsService: TariffsService,
    private readonly submissionPaymentsLoader: SubmissionPaymentsLoader,
    private readonly forumPaymentsLoader: ForumPaymentsLoader,
    private readonly teamleadFeesLoader: TeamleadFeesLoader,
    private readonly correctionsLoader: CorrectionsLoader,
    private readonly bonusLoader: BonusLoader,
  ) {}

  // TODO: Use Promise.all
  async load(query: QueryReportDto, groupByCourse = true) {
    this._groupByCourse = groupByCourse;

    query.locale = query.locale ?? 'en';

    const submissionPayments = await this.submissionPaymentsLoader.load(query, groupByCourse);
    const period = EntireMonthPeriod.fromYearAndMonth(query.year, query.month);

    const result: Partial<CommonPaymentsData> = {
      tutorPayments: [...submissionPayments, ...(await this.forumPaymentsLoader.load(query, groupByCourse))],
      teamleadFees: await this.teamleadFeesLoader.load(query, groupByCourse),
      corrections: await this.correctionsLoader.load(query),
      bonus: await this.bonusLoader.load(query, submissionPayments),

      periodStart: period.periodStart,
      periodEnd: period.periodEnd,
    };

    result.tutorTotal = this.calcTotal(result.tutorPayments);
    result.teamleadTotal = this.calcTotal(result.teamleadFees);
    result.correctionsTotal = result.corrections.reduce((total, c) => total + c.value, 0);
    result.total = result.tutorTotal + result.teamleadTotal + result.correctionsTotal + result.bonus.total;

    result.levels = this.extractLevels(result.tutorPayments);

    result.tariffs = await this.tariffsService.getSchedule({
      startDate: period.periodStart,
      endDate: period.periodEnd,
      locale: query.locale,
      thisLevelsOnly: result.levels,
    });

    result.isEmpty = result.total == 0;

    return result;
  }

  extractLevels(payments: PaymentRecord[]) {
    return payments
      .reduce((levels: Partial<ServiceLevel>[], p) => {
        const exists = levels.some(l => l.id == p.levelId);
        if (!exists && p.levelTitle) {
          levels.push({
            title: p.levelTitle,
            target: p.levelTarget,
            id: p.levelId,
          });
        }

        return levels;
      }, [])
      .sort((a, b) => a.title.localeCompare(b.title))
      .sort((a, b) => b.target.localeCompare(a.target));
  }

  calcTotal(payments: { amount: number }[]) {
    return payments.reduce((total, p) => Number(total) + Number(p.amount), 0);
  }
}
