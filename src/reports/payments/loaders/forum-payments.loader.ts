import { ForumMessages } from '@entities/forum-messages.entity';
import { PaymentRecord } from '@lib/api/reports';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { camelizeKeys } from 'humps';
import { courseTitle, filterByYearAndMonth } from 'src/query-helpers';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { EntityManager } from 'typeorm';
import { hasPaidEnrollment } from '../helpers';

@Injectable()
export class ForumPaymentsLoader implements DataLoader {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}
  async load(query: QueryReportDto, groupByCourse = true) {
    const queryBuilder = this.entityManager
      .createQueryBuilder(ForumMessages, 'm')
      .select(
        `u.id as tutor_id, u.name as tutor_name,
       count(m.id) as cnt, 
       sum(resolve_tariff.value) as amount,
       level.title as level_title, level.id as level_id, level.target as level_target,
       json_agg(m.id) as sources`,
      )
      .innerJoin('m.user', 'u')
      .innerJoin('m.tariff', 'tariff')
      .innerJoin('u.roles', 'r')
      .withDeleted()
      .innerJoin('tariff.resolveTariff', 'resolve_tariff')
      .innerJoin('resolve_tariff.level', 'level')
      .where('(m.skip_resolve = true or m.user_id = resolved_user_id)')
      .andWhere(`r.role = 'tutor'`)
      .groupBy('u.id');

    hasPaidEnrollment(queryBuilder, 'm.user_id', 'm.course_id', 'm.created', CourseRole.Tutor);

    if (groupByCourse) {
      queryBuilder
        .innerJoin('m.course', 'c')
        .addSelect('c.id as course_id')
        .addSelect(`${courseTitle()} as course_title`)
        .addGroupBy('c.id');
    }

    queryBuilder.addGroupBy('level.id');

    if (query.groupBy) {
      queryBuilder.addSelect(`date_trunc('${query.groupBy}', m.created) as dt`).addGroupBy('dt').addOrderBy('dt');
    }

    if (query.tutorId) {
      queryBuilder.andWhere('u.id = :tutorId', {
        tutorId: query.tutorId,
      });
    }

    filterByYearAndMonth(queryBuilder, 'm.created', query.year, query.month);

    return camelizeKeys(await queryBuilder.getRawMany()) as PaymentRecord[];
  }
}
