import { PaymentsManager } from '@lib/api/reports';
import { Injectable } from '@nestjs/common';
import * as dayjs from 'dayjs';
import { DataLoader } from '../../data-loader';
import { QueryReportDto } from '../../dto/query-report.dto';
import { CommonPaymentsData, TutorPaymentRecord, TutorPaymentsData } from '@lib/api/reports';

@Injectable()
export class TutorReportLoader implements DataLoader {
  constructor(private readonly paymentsLoader: DataLoader) {}

  async load(query: QueryReportDto) {
    const data = (await this.paymentsLoader.load(query)) as CommonPaymentsData;

    const { tutorPayments: payments, levels } = data;
    const pm = new PaymentsManager(levels);
    const days = pm.groupByDay(payments, dayjs, query.locale);

    const prepared: TutorPaymentsData = {
      ...data,
      tutorPayments: [],
    };

    Object.keys(days).forEach(dayKey => {
      const day = days[dayKey];
      Object.keys(day.childs).forEach(courseKey => {
        const course = day.childs[courseKey];

        const levels = data.levels.reduce(
          (res, l) => ({
            ...res,
            [l.id]: course.levels[l.id].cnt,
          }),
          {},
        );

        prepared.tutorPayments.push({
          day: day.title,
          course: course.title,
          ...levels,
          amount: course.summary.amount,
        } as TutorPaymentRecord);
      });
    });

    return prepared;
  }
}
