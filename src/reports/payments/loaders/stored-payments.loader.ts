import { StoredPayment } from '@entities/stored-payment.entity';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import {
  BonusAmount,
  CommonPaymentsData,
  PaymentCorrection,
  PaymentRecord,
  PaymentType,
  getPeriodName,
  ReportType,
  TeamleadFee,
} from '@lib/api/reports';
import { Injectable, Scope } from '@nestjs/common';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { PaymentsStore } from 'src/reports/payments/payments-store';
import { EntireMonthPeriod } from '@lib/helpers/date/entire-month-period';
import { TariffsService } from 'src/directory/service-level/tariffs.service';
import { ReportApproveService } from 'src/reports/report-approve.service';

@Injectable({ scope: Scope.REQUEST })
export class StoredPaymentsLoader implements DataLoader {
  private readonly paymentsData: CommonPaymentsData = {
    tutorPayments: [],
    corrections: [],
    teamleadFees: [],
    levels: [],
    tariffs: [],

    tutorTotal: 0,
    teamleadTotal: 0,
    correctionsTotal: 0,
    total: 0,
    bonus: {
      courseBonuses: [],
    },
    isEmpty: false,
    isApproved: false,
  };

  private readonly addFunctions = {
    [PaymentType.Submission]: this.addSubmissionPayment,
    [PaymentType.TeamleadBonus]: this.addTeamleadBonus,
    [PaymentType.TutorBonus]: this.addBonus,
    [PaymentType.Correction]: this.addCorrection,
  };

  constructor(
    private readonly store: PaymentsStore,
    private readonly tariffsService: TariffsService,
    private readonly approveService: ReportApproveService,
  ) {}
  async load(query: QueryReportDto) {
    const storedPayments = await this.store.load({ ...query, year: query.year || 2000, month: query.month || 0, asRole: query.asRole });

    this.paymentsData.isEmpty = storedPayments.length === 0;

    storedPayments.forEach(p => this.addPayment(p));

    const period = EntireMonthPeriod.fromYearAndMonth(query.year, query.month);

    this.paymentsData.tariffs = await this.tariffsService.getSchedule({
      startDate: period.periodStart,
      endDate: period.periodEnd,
      locale: query.locale,
      thisLevelsOnly: this.paymentsData.levels,
    });

    this.paymentsData.levels = this.paymentsData.levels
      .sort((a, b) => a.title.localeCompare(b.title))
      .sort((a, b) => b.target.localeCompare(a.target));

    this.paymentsData.isApproved = await this.approveService.isApproved({
      period: getPeriodName(query as any),
      reportType: ReportType.Payment,
    });

    return this.paymentsData;
  }

  addPayment(payment: StoredPayment) {
    const add = this.addFunctions[payment.type];
    add.bind(this)(payment);

    this.paymentsData.total += Number(payment.money);
  }

  addSubmissionPayment(stored: StoredPayment) {
    const tutorPayment: PaymentRecord = {
      courseId: stored.courseId,
      courseTitle: stored.title,
      tutorId: stored.userId,
      tutorName: stored.user?.name || 'Unknown',
      levelId: stored.levelId,
      levelTitle: stored.level?.title || 'Unknown',
      levelTarget: stored.level?.target || ServiceLevelTarget.Submission,
      dt: stored.date,
      amount: Number(stored.money),
      cnt: Number(stored.value),
      sources: stored.sources,
    };

    const levelExists = this.paymentsData.levels.some(l => l.id == stored.levelId);

    if (!levelExists) {
      this.paymentsData.levels.push(stored.level);
    }
    this.paymentsData.tutorTotal += tutorPayment.amount;
    this.paymentsData.tutorPayments.push(tutorPayment);
  }

  addTeamleadBonus(stored: StoredPayment) {
    const teamleadFee: TeamleadFee = {
      courseId: stored.courseId,
      courseTitle: stored.title,
      tutorId: stored.userId,
      tutorName: stored.user?.name || 'Unknown',
      amount: Number(stored.money),
    };

    this.paymentsData.teamleadTotal += teamleadFee.amount;
    this.paymentsData.teamleadFees.push(teamleadFee);
  }

  addCorrection(stored: StoredPayment) {
    const correction: PaymentCorrection = {
      userId: stored.userId,
      userName: stored.user.name,
      date: stored.date,
      reason: stored.title,
      value: Number(stored.money),
    };

    this.paymentsData.correctionsTotal += correction.value;
    this.paymentsData.corrections.push(correction);
  }

  addBonus(stored: StoredPayment) {
    const bonusAmount: Partial<BonusAmount> = {
      courseId: stored.courseId,
      courseTitle: stored.title,
      tutorId: stored.userId,
      bonusPercent: stored.value,
      amount: stored.money,
    };

    this.paymentsData.bonus.courseBonuses.push(bonusAmount);
  }
}
