import { Enrollment } from '@entities/enrollment.entity';
import { TeamleadFee } from '@lib/api/reports';
import { EntireMonthPeriod } from '@lib/helpers/date';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { InjectEntityManager } from '@nestjs/typeorm';
import { DateService } from '@utils/date.service';
import { camelizeKeys } from 'humps';
import { courseTitle } from 'src/query-helpers';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';

export class TeamleadFeesLoader implements DataLoader {
  constructor(
    @InjectEntityManager()
    private readonly entityManager,
    private readonly dateService: DateService,
  ) {}

  async load(query: QueryReportDto, groupByCourse = true) {
    const queryBuilder = this.entityManager
      .createQueryBuilder(Enrollment, 'e')
      .select(`u.id as tutor_id, u.name as tutor_name, sum(e.teamlead_fee) as amount`)
      .innerJoin('e.user', 'u')
      .where('e.role = :role', { role: CourseRole.Teamlead })
      .andWhere('e.teamlead_fee is not null')
      .andWhere('e.type <> :testType', { testType: EnrollmentType.Test })
      .andWhere('e.active = true')
      .andWhere('e.teamlead_fee > 0')
      .groupBy('u.id')
      .addOrderBy('u.id');

    if (groupByCourse) {
      queryBuilder
        .innerJoin('e.course', 'c')
        .addSelect('c.id as course_id')
        .addSelect(`${courseTitle()} as course_title`)
        .addGroupBy('c.id')
        .addOrderBy('c.title, c.version');
    }

    if (query.tutorId) {
      const tutorId = (query.tutorId as string).slice(0, 36);
      queryBuilder.andWhere('user_id = :tutorId', {
        tutorId,
      });
    }

    if (query.year && query.month) {
      const period = EntireMonthPeriod.fromYearAndMonth(query.year, query.month);

      queryBuilder.andWhere(`coalesce(date_from, e.created) <= :periodEnd`, {
        periodEnd: period.periodEnd,
      });

      queryBuilder.andWhere(`coalesce(date_to, :max) >= :periodStart`, {
        periodStart: period.periodStart,
        max: this.dateService.MAX_DATE,
      });
    }

    const result = camelizeKeys(await queryBuilder.getRawMany());
    return result as TeamleadFee[];
  }
}
