import { InjectEntityManager } from '@nestjs/typeorm';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { PaymentCorrection as PaymentCorrectionEntity } from '@entities/payment-correction.entity';
import { filterByYearAndMonth } from 'src/query-helpers';
import { camelizeKeys } from 'humps';
import { PaymentCorrection } from '@lib/api/reports';

export class CorrectionsLoader implements DataLoader {
  constructor(
    @InjectEntityManager()
    private readonly entityManager,
  ) {}

  async load(query: QueryReportDto) {
    const queryBuilder = this.entityManager
      .createQueryBuilder(PaymentCorrectionEntity, 'pc')
      .select('pc.id, pc.user_id, pc.date, pc.value, pc.reason, u.name as user_name')
      .innerJoin('pc.user', 'u');

    if (query.tutorId) {
      queryBuilder.andWhere('user_id = :userId', {
        userId: query.tutorId,
      });
    }

    filterByYearAndMonth(queryBuilder, 'pc.date', query.year, query.month);

    const result = camelizeKeys(await queryBuilder.getRawMany()) as PaymentCorrection[];
    return result;
  }
}
