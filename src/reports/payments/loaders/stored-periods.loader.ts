import { ReportPeriod } from '@lib/api/reports';
import { QueryReportPeriodsDto } from '@lib/api/reports/dto/query-report-periods.dto';
import { EntireMonthPeriod } from '@lib/helpers/date';
import { Injectable } from '@nestjs/common';
import { DataLoader } from 'src/reports/data-loader';
import { ReportStore } from 'src/reports/report-store';

@Injectable()
export class StoredPeriodsLoader implements DataLoader {
  constructor(private readonly store: ReportStore) {}
  async load(query: QueryReportPeriodsDto) {
    const storedPeriods = await this.store.loadPeriods(query);
    const mainPeriod = new EntireMonthPeriod(query.periodStart, query.periodEnd);
    const periods = mainPeriod.entireMonths
      .map((m: EntireMonthPeriod) => {
        const stored = storedPeriods.find(p => p.year == m.year && p.month == m.month + 1);

        if (stored) {
          return stored;
        }

        const empty: ReportPeriod = {
          year: m.year,
          month: m.month + 1,
        };

        return empty;
      })
      .sort((a, b) => b.year * 100 + b.month - (a.year * 100 + a.month))
      .slice(1);

    return periods;
  }
}
