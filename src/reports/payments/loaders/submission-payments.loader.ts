import { Submission } from '@entities/submission.entity';
import { PaymentRecord } from '@lib/api/reports';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { camelizeKeys } from 'humps';
import { courseTitle, filterByUserCourses, filterByYearAndMonth } from 'src/query-helpers';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { EntityManager } from 'typeorm';
import { hasPaidEnrollment } from '../helpers';

@Injectable()
export class SubmissionPaymentsLoader implements DataLoader {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}
  async load(query: QueryReportDto, groupByCourse = true, allCourseSubmissions = false) {
    const queryBuilder = this.entityManager
      .createQueryBuilder(Submission, 's')
      .select(
        `t.id as tutor_id, t.name as tutor_name, 
       count(s.id) as cnt, sum(grade_tariff.value) as amount,
       level.title as level_title, level.id as level_id, level.target as level_target,
       json_agg(s.id) as sources`,
      )
      .innerJoin('s.assessment', 'a')
      .innerJoin('s.tutor', 't')
      .innerJoin('s.tariff', 'tariff')
      .withDeleted()
      .innerJoin('tariff.gradeTariff', 'grade_tariff')
      .innerJoin('grade_tariff.level', 'level')
      .andWhere('graded_at is not null')

      .groupBy('t.id');

    hasPaidEnrollment(queryBuilder, 's.tutor_id', 's.course_id', 's.graded_at', CourseRole.Tutor);

    if (groupByCourse) {
      queryBuilder
        .innerJoin('s.course', 'c')
        .addSelect('c.id as course_id')
        .addSelect(`${courseTitle()} as course_title`)
        .addGroupBy('c.id');
    }

    if (query.groupBy) {
      queryBuilder.addSelect(`date_trunc('${query.groupBy}', s.graded_at) as dt`).addGroupBy('dt').addOrderBy('dt');
    }

    queryBuilder.addGroupBy('level.id');

    if (groupByCourse) {
      queryBuilder.addOrderBy('c.title');
    }

    if (query.tutorId) {
      if (!allCourseSubmissions) {
        const tutorId = (query.tutorId as string).slice(0, 36);
        queryBuilder.andWhere('s.tutor_id = :tutorId', {
          tutorId,
        });
      } else {
        filterByUserCourses(queryBuilder, query.tutorId, query.asRole);
      }
    }

    filterByYearAndMonth(queryBuilder, 'graded_at', query.year, query.month);

    const results = await queryBuilder.getRawMany();
    return camelizeKeys(results) as PaymentRecord[];
  }
}
