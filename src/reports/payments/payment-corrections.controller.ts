import { PaymentCorrection } from '@entities/payment-correction.entity';
import { Permission } from '@enums/permission.enum';
import { Body, Controller, Delete, Get, HttpStatus, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreatePaymentCorrectionDto } from './dto/create-payment-correction.dto';
import { QueryPaymentCorrectionDto } from './dto/query-payment-correction.dto';
import { PaymentCorrectionsService } from './payment-corrections.service';

@ApiTags('corrections')
@ApiBearerAuth()
@Controller('corrections/payments')
export class PaymentCorrectionsController {
  constructor(private readonly service: PaymentCorrectionsService) {}

  @Post()
  @RequirePermissions(Permission.REPORT_MANAGE)
  @ApiOperation({ summary: 'Create a new payment correction' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Created payment correction',
    type: PaymentCorrection,
  })
  @PgConstraints()
  async save(@Body() dto: CreatePaymentCorrectionDto) {
    return this.service.create(dto);
  }

  @Get()
  @RequirePermissions(Permission.REPORT_MANAGE)
  @ApiOperation({ summary: 'Get list of correction' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of correction',
    type: PaymentCorrection,
  })
  @PgConstraints()
  async find(@Query() dto: QueryPaymentCorrectionDto) {
    return this.service.find(dto);
  }

  @Get(':id')
  @RequirePermissions(Permission.REPORT_MANAGE)
  @ApiOperation({ summary: 'Get one correction by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Requested correction',
    type: PaymentCorrection,
  })
  @PgConstraints()
  async findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.service.findOne(id);
  }

  @Patch(':id')
  @RequirePermissions(Permission.REPORT_MANAGE)
  @ApiOperation({ summary: 'Update payment correction' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Updated fields',
    type: PaymentCorrection,
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() dto: CreatePaymentCorrectionDto) {
    return this.service.update(id, dto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Deletes a payment correction' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns number of affected records',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.service.remove(id);
  }
}
