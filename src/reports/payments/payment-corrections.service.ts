import { PaymentCorrection } from '@entities/payment-correction.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { CreatePaymentCorrectionDto } from './dto/create-payment-correction.dto';
import { QueryPaymentCorrectionDto } from './dto/query-payment-correction.dto';
import { UpdatePaymentCorrectionDto } from './dto/update-payment-correction.dto';

@Injectable()
export class PaymentCorrectionsService {
  constructor(
    @InjectRepository(PaymentCorrection)
    private readonly repository: Repository<PaymentCorrection>,
  ) {}

  async create(dto: CreatePaymentCorrectionDto) {
    dto.date = this.normilizeDate(dto.date);
    return this.repository.save(dto);
  }

  async find(query: QueryPaymentCorrectionDto) {
    if (query.date) {
      query.date = this.normilizeDate(query.date);
    }

    return this.repository.find({
      where: query,
      order: {
        reason: 'ASC',
      },
    });
  }

  async findOne(id: string) {
    return this.repository.findOne(id);
  }

  async remove(id: string) {
    return this.repository.delete(id);
  }

  async update(id: string, dto: UpdatePaymentCorrectionDto) {
    dto.date = this.normilizeDate(dto.date);

    return this.repository.save({ ...dto, id });
  }

  normilizeDate(date: Date) {
    return dayjs(date).startOf('month').toDate();
  }
}
