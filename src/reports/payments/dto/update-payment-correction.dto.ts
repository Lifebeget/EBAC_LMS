import { PartialType } from '@nestjs/swagger';
import { CreatePaymentCorrectionDto } from './create-payment-correction.dto';

export class UpdatePaymentCorrectionDto extends PartialType(CreatePaymentCorrectionDto) {}
