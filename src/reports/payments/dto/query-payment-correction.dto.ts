import { ApiProperty } from '@nestjs/swagger';

export class QueryPaymentCorrectionDto {
  @ApiProperty({ description: 'Id of user for correction' })
  userId: string;

  @ApiProperty({ description: 'Date of correction' })
  date: Date;
}
