import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class CreatePaymentCorrectionDto {
  @ApiProperty({ description: 'Id of user for correction' })
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'Date of correction' })
  date: Date;

  @ApiProperty({ description: 'Correction value' })
  value?: number;

  @ApiProperty({ description: 'Correction reason' })
  reason?: string;
}
