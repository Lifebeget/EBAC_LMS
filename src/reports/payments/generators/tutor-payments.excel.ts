import { TutorPaymentsData } from '@lib/api/reports';
import { SystemRole } from '@lib/types/enums/system-role.enum';
import { Injectable } from '@nestjs/common';
import * as dayjs from 'dayjs';
import * as ExcelJS from 'exceljs';
import { QueryReportDto } from '../../dto/query-report.dto';
import { ExcelGenerator } from '../../excel-generator';
import { translations } from './translations';

@Injectable()
export class TutorPaymentsExcel extends ExcelGenerator {
  async generateReport(query: QueryReportDto) {
    query.asRole = SystemRole.Tutor;
    const data: TutorPaymentsData = await this.loader.load(query);

    const FIRST_ROW = 2;
    const ws = this.addWorksheet('Report');
    const headers = translations[query.locale];

    ws.getRow(FIRST_ROW).values = [headers.day, headers.course, ...data.levels.map(l => l.title), headers.money];

    ws.columns = [
      {
        key: 'day',
        width: 15,
        alignment: { horizontal: 'center' },
      },
      { key: 'course', width: 50 },
      ...data.levels.map(
        level =>
          ({
            key: level.id,
          } as ExcelJS.Column),
      ),
      { key: 'amount' },
    ];

    this.setHeaderDefaults(ws.getRow(FIRST_ROW));

    let rowIndex = FIRST_ROW + 1;
    let mergeStart = rowIndex;

    data.tutorPayments.forEach((payment, dataIndex) => {
      let col = 0;

      ws.getCell(rowIndex, ++col).value = payment.day;
      ws.getCell(rowIndex, col).alignment = { horizontal: 'center' };

      ws.getCell(rowIndex, ++col).value = payment.course;

      data.levels.forEach(level => {
        ws.getCell(rowIndex, ++col).value = payment[level.id];
      });

      ws.getCell(rowIndex, ++col).value = payment.amount;

      if (dataIndex > 0 && payment.day != '') {
        const prevDay = data.tutorPayments[dataIndex - 1].day;

        if (prevDay !== payment.day) {
          if (rowIndex - mergeStart > 1) {
            ws.mergeCells(mergeStart, 1, rowIndex - 1, 1);
          }
          mergeStart = rowIndex;
        }
      }

      this.setRowDefaults(ws.getRow(rowIndex));

      rowIndex++;
    });

    if (data.tutorPayments.length > 0) {
      ws.mergeCells(mergeStart, 1, rowIndex - 1, 1);
    }

    data.teamleadFees.forEach(fee => {
      ws.getCell(rowIndex, 1).value = `Teamlead bonus: ${fee.courseTitle}`;
      ws.mergeCells(rowIndex, 1, rowIndex, ws.columnCount - 1);
      ws.getCell(rowIndex, ws.columnCount).value = Number(fee.amount || 0);
      this.setRowDefaults(ws.getRow(rowIndex));

      rowIndex++;
    });

    data.bonus.courseBonuses.forEach(bonus => {
      if (!bonus.amount) {
        return;
      }

      ws.getCell(rowIndex, 1).value = `Tutor bonus: ${bonus.courseTitle}`;
      ws.mergeCells(rowIndex, 1, rowIndex, ws.columnCount - 1);
      ws.getCell(rowIndex, ws.columnCount).value = Number(bonus.amount);
      this.setRowDefaults(ws.getRow(rowIndex));

      rowIndex++;
    });

    data.corrections.forEach(correction => {
      ws.getCell(rowIndex, 1).value = `${headers.correction}: ${correction.reason}`;
      ws.mergeCells(rowIndex, 1, rowIndex, ws.columnCount - 1);
      ws.getCell(rowIndex, ws.columnCount).value = Number(correction.value || 0);
      this.setRowDefaults(ws.getRow(rowIndex));

      rowIndex++;
    });

    const totalRow = ws.getRow(rowIndex);

    totalRow.getCell(1).value = 'Total';

    const letter = ws.columns[ws.columnCount - 1].letter;

    totalRow.getCell(ws.columnCount).value = {
      formula: `SUM(${letter}${FIRST_ROW}:${letter}${rowIndex - 1})`,
      date1904: false,
    };

    this.setRowDefaults(totalRow);
    this.setRowBold(totalRow);

    ws.mergeCells(rowIndex, 1, rowIndex, ws.columnCount - 1);

    let cell = ws.getCell(1, 1);

    let title = headers.tutorReport;

    if (query.tutorId) {
      title += ` - ${query.tutorName}`;
    }

    cell.value = title;
    cell.font = { bold: true };

    cell = ws.getCell(1, ws.columnCount);
    cell.value = dayjs(`${query.year}-${query.month}-01`).locale(query.locale).format('MMMM YYYY');
    cell.font = { bold: true };
    cell.alignment = { horizontal: 'right' };
  }
}
