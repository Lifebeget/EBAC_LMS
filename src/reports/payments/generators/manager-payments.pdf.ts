import { translations } from './translations';
import { QueryReportDto } from '../../dto/query-report.dto';
import { Injectable } from '@nestjs/common';
import { PdfGenerator } from '../../pdf-generator';
import { NumberFormat } from '@lib/helpers/format';

@Injectable()
export class ManagerPaymentsPdf extends PdfGenerator {
  async generateReport(query: QueryReportDto) {
    const data = await this.loader.load(query);

    const headers = translations[query.locale];
    const table = {
      headers: [
        { label: 'Tutor', property: 'tutorName', width: 120 },
        ...data.levels.map(level => ({
          label: level,
          property: level,
          width: 200 / data.levels.length,
          align: 'right',
          renderer: (val, _c, _r, row) => val ?? (row.tutorName !== 'Total' ? 0 : ''),
        })),
        {
          label: headers.all,
          property: 'amount',
          width: 50,
          align: 'right',
        },
        {
          label: 'Teamlead bonus',
          property: 'teamleadFee',
          width: 50,
          align: 'right',
        },
        {
          label: headers.correction,
          property: 'correctionValue',
          width: 50,
          align: 'right',
        },
        {
          label: headers.money,
          property: 'total',
          width: 50,
          align: 'right',
        },
      ],
      datas: [],
    };

    const format = new NumberFormat(query.locale, {
      currencyName: query.currency,
    });

    data.tutorPayments.forEach(payment => {
      table.datas.push({
        ...payment,
        options: {
          fontFamily: 'Helvetica',
        },
      });
    });

    table.datas.push({
      tutorName: 'Total',
      amount: format.money(data.tutorTotal),
      teamleadFee: format.money(data.teamleadTotal),
      correctionValue: format.money(data.correctionsTotal),
      total: format.money(data.total),
      options: {
        fontFamily: 'Helvetica-Bold',
      },
    });

    this.createTable(table);
  }
}
