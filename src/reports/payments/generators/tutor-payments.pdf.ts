import { translations } from './translations';
import { NumberFormat } from '@lib/helpers/format';
import { QueryReportDto } from '../../dto/query-report.dto';
import { Injectable } from '@nestjs/common';
import { PdfGenerator } from '../../pdf-generator';
import { SystemRole } from '@lib/types/enums/system-role.enum';

@Injectable()
export class TutorPaymentsPdf extends PdfGenerator {
  async generateReport(query: QueryReportDto) {
    query.asRole = SystemRole.Tutor;

    const data = await this.loader.load(query);

    const headers = translations[query.locale];
    const format = new NumberFormat(query.locale, {
      currencyName: query.currency,
    });

    const table = {
      headers: [
        { label: headers.day, property: 'day', width: 50 },
        { label: headers.course, property: 'course', width: 140 },
        ...data.levels.map(level => ({
          label: level.title,
          property: level.id,
          width: 300 / data.levels.length,
          align: 'right',
        })),
        {
          label: headers.money,
          property: 'amount',
          width: 50,
          align: 'right',

          renderer: val => format.money(val, false),
        },
      ],
      datas: [],
    };

    data.tutorPayments.forEach((payment, i) => {
      const prevPayment = data.tutorPayments[i - 1];
      if (payment.day === prevPayment?.day) {
        payment.day = '';
      }
      table.datas.push({
        ...payment,
        options: {
          fontFamily: 'Helvetica',
        },
      });
    });

    data.teamleadFees.forEach(fee => {
      table.datas.push({
        course: `Teamlead bonus: ${fee.courseTitle}`,
        amount: fee.amount,
      });
    });

    data.bonus.courseBonuses.forEach(bonus => {
      if (!bonus.amount) {
        return;
      }
      table.datas.push({
        course: `Tutor bonus: ${bonus.courseTitle}`,
        amount: bonus.amount,
      });
    });

    data.corrections.forEach(correction => {
      table.datas.push({
        course: `${headers.correction}: ${correction.reason}`,
        amount: correction.value,
      });
    });

    table.datas.push({
      course: 'Total',
      amount: data.total,
      options: {
        fontFamily: 'Helvetica-Bold',
      },
    });

    this.createTable(table);
  }
}
