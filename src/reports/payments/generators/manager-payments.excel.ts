import * as ExcelJS from 'exceljs';
import * as dayjs from 'dayjs';

import { ExcelGenerator } from 'src/reports/excel-generator';
import { Injectable } from '@nestjs/common';
import { ManagerPaymentRecord } from '@lib/api/reports';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { translations } from './translations';

@Injectable()
export class ManagerPaymentsExcel extends ExcelGenerator {
  headers: Record<string, string>;
  data: any;

  protected async generateReport(query: QueryReportDto) {
    this.data = await this.loader.load(query);
    this.headers = translations[query.locale];

    this.generateSheet(this.headers.report, false, query);
    this.generateSheet(this.headers.expanded, true, query);
  }

  private generateSheet(title: string, expand: boolean, query: QueryReportDto) {
    const FIRST_ROW = 2;
    const ws = this.addWorksheet(title);

    ws.getRow(FIRST_ROW).values = [
      'Tutor',
      ...this.data.levels.map(l => l.title),
      this.headers.all,
      this.headers.tutorBonus,
      this.headers.teamleadBonus,
      this.headers.correction,
      this.headers.money,
    ];

    ws.columns = [
      {
        key: 'tutorName',
        width: 30,
      },
      ...this.data.levels.map(
        level =>
          ({
            key: level.id,
          } as ExcelJS.Column),
      ),
      { key: 'amount', width: 11 },
      { key: 'bonus', width: 10 },
      { key: 'teamleadFee', width: 10 },
      { key: 'correctionValue' },
      { key: 'total', width: 11 },
    ];

    this.setHeaderDefaults(ws.getRow(FIRST_ROW), true, 25);

    const tutorRows: number[] = [];

    let rowIndex = FIRST_ROW + 1;
    this.data.tutorPayments.forEach((payment: ManagerPaymentRecord) => {
      const fill = expand ? this.Fill.Gray : undefined;

      this.addPayment(ws, rowIndex, payment, payment.tutorName, fill);
      tutorRows.push(rowIndex);

      rowIndex++;

      if (expand) {
        Object.keys(payment.courses).forEach(key => {
          const course = payment.courses[key];
          this.addPayment(ws, rowIndex, course, course.title);
          rowIndex++;
        });
      }
    });

    const totalRow = ws.getRow(rowIndex);

    totalRow.getCell(1).value = 'Total';

    for (let col = ws.columnCount - 5; col < ws.columnCount; col++) {
      const letter = ws.columns[col].letter;
      const cell = totalRow.getCell(col + 1);
      cell.value = {
        formula: `SUM(${tutorRows.map(rowIndex => letter + rowIndex).join(',')})`,
        date1904: false,
      };

      cell.numFmt = this.NumberFormat.Integer;
    }

    this.setRowDefaults(totalRow);
    this.setRowBold(totalRow);

    ws.mergeCells(rowIndex, 1, rowIndex, ws.columnCount - 5);

    let reportTitle = this.headers.managerReport;

    if (expand) {
      reportTitle += ` ${this.headers.expanded.toLowerCase()}`;
    }

    this.setHeader(ws, reportTitle, query);
  }

  private addPayment(ws: ExcelJS.Worksheet, rowIndex: number, payment: ManagerPaymentRecord, rowTitle: string, fill?: ExcelJS.FillPattern) {
    let col = 0;

    ws.getCell(rowIndex, ++col).value = rowTitle;

    this.data.levels.forEach(level => {
      this.setInteger(ws.getCell(rowIndex, ++col), payment.levels ? payment.levels[level.id]?.cnt : '―');
    });

    this.setInteger(ws.getCell(rowIndex, ++col), payment.amount);
    this.setInteger(ws.getCell(rowIndex, ++col), payment.bonus);

    this.setInteger(ws.getCell(rowIndex, ++col), payment.teamleadFee);
    this.setInteger(ws.getCell(rowIndex, ++col), payment.correctionValue);

    this.setInteger(ws.getCell(rowIndex, ++col), payment.total);

    const row = ws.getRow(rowIndex);

    if (fill) {
      this.setRowFill(row, fill);
    }

    this.setRowDefaults(row);
  }
}
