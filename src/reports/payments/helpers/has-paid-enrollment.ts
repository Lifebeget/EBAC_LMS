import { Enrollment } from '@entities/enrollment.entity';
import { MAX_DATE } from '@lib/helpers/date';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { SelectQueryBuilder } from 'typeorm';

export const hasPaidEnrollment = <Entity>(
  queryBuilder: SelectQueryBuilder<Entity>,
  tutorSelector: string,
  courseSelector: string,
  dateSelector: string,
  role = CourseRole.Tutor,
) => {
  queryBuilder.andWhere(
    qb =>
      `exists ${qb
        .subQuery()
        .select('id')
        .from(Enrollment, 'e')
        .where(`e.course_id = ${courseSelector}`)
        .andWhere(`e.user_id = ${tutorSelector}`)
        .andWhere(`e.role = '${role}'`)
        .andWhere('e.type <> :testType', {
          testType: EnrollmentType.Test,
        })
        .andWhere(
          `${dateSelector} between 
            coalesce(e.date_from, e.created) and 
            coalesce(e.date_to, :maxDate)`,
          { maxDate: MAX_DATE },
        )
        .andWhere('active = true')
        .getQuery()}`,
  );
};
