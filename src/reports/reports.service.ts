import { BadRequestException, Inject, Injectable } from '@nestjs/common';

import { EntityManager, Repository } from 'typeorm';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { getPeriodName, ReportFormat, ReportName, ReportType } from '@lib/api/reports';
import { Reports } from './report';
import { PaymentsLoader, TutorPaymentsPdf, TutorPaymentsExcel, ManagerPaymentsExcel } from './payments';
import { Response } from 'express';
import * as assert from 'assert';
import { ManagerPaymentsPdf } from './payments/generators';
import { BonusLoader } from './bonuses/loaders/bonus.loader';
import { BonusExcel } from './bonuses/generators/bonus.excel';
import { StoredPaymentsLoader } from './payments/loaders/stored-payments.loader';
import { DataLoader } from './data-loader';
import { PaymentsStore } from './payments/payments-store';
import { BonusStore } from './bonuses/bonus-store';
import { StoredBonusLoader } from './bonuses/loaders/stored-bonus.loader';
import { ApproveReportDto } from './dto/approve-report.dto';
import { ReportApprove } from '@entities/report-approve.entity';
import { ReportApproveService } from './report-approve.service';

@Injectable()
export class ReportsService {
  reports: Reports = Object.create(null);
  constructor(
    paymentsLoader: PaymentsLoader,
    paymentsLoaderStored: StoredPaymentsLoader,
    paymentsStore: PaymentsStore,

    @Inject('TUTOR_REPORT_LOADER') tutorReportLoader: DataLoader,
    @Inject('TUTOR_REPORT_LOADER_STORED') tutorReportLoaderStored: DataLoader,

    tutorPaymentsPdf: TutorPaymentsPdf,
    tutorPaymentsExcel: TutorPaymentsExcel,

    @Inject('MANAGER_PAYMENTS_LOADER') managerPaymentsLoader: DataLoader,
    @Inject('MANAGER_PAYMENTS_LOADER_STORED') managerPaymentsLoaderStored: DataLoader,

    managerPaymentsExcel: ManagerPaymentsExcel,
    managerPaymentsPdf: ManagerPaymentsPdf,

    @Inject('PAYMENT_PERIODS_LOADER')
    paymentPeriodsLoader: DataLoader,

    @Inject('BONUS_PERIODS_LOADER')
    bonusPeriodsLoader: DataLoader,

    bonusLoader: BonusLoader,
    bonusLoaderStored: StoredBonusLoader,

    bonusStore: BonusStore,
    bonusExcel: BonusExcel,

    private readonly approveService: ReportApproveService,

    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {
    this.reports = {
      [ReportName.TutorPayments]: {
        loader: paymentsLoader,
        periodsLoader: paymentPeriodsLoader,
        loaderStored: paymentsLoaderStored,
        store: paymentsStore,
        generateWith: ReportName.TutorReport,
        reportType: ReportType.Payment,
      },
      [ReportName.TutorReport]: {
        loader: tutorReportLoader,
        loaderStored: tutorReportLoaderStored,
        reportType: ReportType.Payment,
        generators: {
          pdf: tutorPaymentsPdf,
          excel: tutorPaymentsExcel,
        },
      },
      [ReportName.ManagerPayments]: {
        loader: managerPaymentsLoader,
        periodsLoader: paymentPeriodsLoader,
        loaderStored: managerPaymentsLoaderStored,
        store: paymentsStore,
        reportType: ReportType.Payment,
        generators: {
          pdf: managerPaymentsPdf,
          excel: managerPaymentsExcel,
        },
      },
      [ReportName.TutorBonus]: {
        loader: bonusLoader,
        periodsLoader: bonusPeriodsLoader,
        loaderStored: bonusLoaderStored,
        store: bonusStore,
        reportType: ReportType.Bonus,
        generators: {
          excel: bonusExcel,
        },
      },
      [ReportName.PaymentPeriods]: {
        loader: paymentPeriodsLoader,
      },
    };
  }

  async load<Query>(query: Query, reportName: string, stored = false) {
    return this.getLoader(reportName, stored).load(query);
  }

  async loadPeriods<Query>(query: Query, reportName: string) {
    return this.getPeriodsLoader(reportName).load(query);
  }

  async generate<Query>(query: Query, resp: Response, reportName: string, reportFormat: ReportFormat, stored = false) {
    reportName = this.reports[reportName]?.generateWith ?? reportName;
    const loader = this.getLoader(reportName, stored);
    return this.getGenerator(reportName, reportFormat).generate(query, resp, loader);
  }

  async save<Query>(query: Query, reportName: string) {
    const store = this.getStore(reportName);
    await store.save(query);
  }

  getLoader(reportName: string, stored = false) {
    let loaderName = 'loader';

    if (stored) {
      loaderName += 'Stored';
    }

    let loader = this.reports[reportName]?.[loaderName];

    if (!loader) {
      loader = this.reports[reportName]?.loader;
    }

    assert(loader, new BadRequestException(`There is no loader for report "${reportName}"`));

    return loader;
  }

  getPeriodsLoader(reportName: string) {
    const periodsLoader = this.reports[reportName]?.periodsLoader;
    assert(periodsLoader, new BadRequestException(`There is no loader for report "${reportName}"`));

    return periodsLoader;
  }

  getGenerator(reportName: string, reportFormat: ReportFormat) {
    let report = this.reports[reportName];

    if (report.generateWith) {
      report = this.reports[report.generateWith];
    }

    const generators = report?.generators;
    assert(
      generators && generators[reportFormat],
      new BadRequestException(`There is no generator for format "${reportFormat}" in report "${reportName}" `),
    );

    return generators[reportFormat];
  }

  getStore(reportName: string) {
    const store = this.reports[reportName]?.store;

    assert(store, new BadRequestException(`Report "${reportName}" can not be saved`));

    return store;
  }

  async approve(dto: ApproveReportDto, reportName: ReportName, approverId: string) {
    const reportType = this.reports[reportName]?.reportType;
    assert(reportType, new BadRequestException(`Report "${reportName}" can not be saved`));
    const period = getPeriodName(dto);
    return this.approveService.approve({ reportType, period, approverId });
  }

  async revoke(dto: ApproveReportDto, reportName: ReportName) {
    const reportType = this.reports[reportName]?.reportType;
    assert(reportType, new BadRequestException(`Report "${reportName}" can not be saved`));

    const period = getPeriodName(dto);
    return this.approveService.revoke({ reportType, period });
  }
}
