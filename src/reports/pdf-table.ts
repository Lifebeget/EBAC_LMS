import * as PDFDocument from 'pdfkit-table';
import { Response } from 'express';

export class PdfTable {
  private readonly __doc: PDFDocument;

  constructor() {
    this.__doc = new PDFDocument({ margin: 30, size: 'A4' });
  }

  create(table: any) {
    table.headers = table.headers.map(h => ({
      ...PdfTable.headerDefault,
      ...h,
    }));
    this.__doc.table(table, {
      padding: 3,
      divider: {
        vertical: { disabled: false, width: 0.5, opacity: 1 },
        horizontal: { disabled: false, width: 0.5, opacity: 1 },
      },
      prepareHeader: () => this.__doc.font('Helvetica-Bold').fontSize(8),
      prepareRow: () => {},
    });

    return this;
  }

  pipe(resp: Response) {
    resp.set('Content-Type', 'application/pdf');
    resp.set('Content-Disposition', 'attachment; filename=tutor-report.pdf');

    this.__doc.pipe(resp);
    this.__doc.end();

    return this;
  }

  static get headerDefault() {
    return {
      headerColor: 'white',
    };
  }
}
