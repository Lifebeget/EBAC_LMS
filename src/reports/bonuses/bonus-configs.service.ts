import { BonusConfig } from '@entities/bonus-config.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LessThan, MoreThan, Repository } from 'typeorm';
import { QueryBonusConfigDto } from './dto/query-bonus-config.dto';

@Injectable()
export class BonusConfigsService {
  constructor(
    @InjectRepository(BonusConfig)
    private readonly repository: Repository<BonusConfig>,
  ) {}

  async find(query: QueryBonusConfigDto) {
    return await this.repository.find({
      where: {
        activeFrom: LessThan(query.periodEnd),
        activeTo: MoreThan(query.periodStart),
      },
    });
  }
}
