import * as dayjs from 'dayjs';

import { Cell, Worksheet } from 'exceljs';

import { BonusData } from '@lib/api/reports';
import { DateFormat } from '@lib/helpers/format';
import { ExcelGenerator } from 'src/reports/excel-generator';
import { Injectable } from '@nestjs/common';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { translations } from 'src/reports/payments/generators/translations';

const FIRST_ROW = 2;
const CHECKPOINTS_START = 2;

@Injectable()
export class BonusExcel extends ExcelGenerator {
  private rowIndex = FIRST_ROW;
  private headers: Record<string, any>;

  protected async generateReport(query: QueryReportDto) {
    this.rowIndex = FIRST_ROW;
    this.headers = translations[query.locale];

    const bonusData = (await this.loader.load(query)) as BonusData;

    const ws = this.workbook.addWorksheet('Report', {
      pageSetup: {
        fitToWidth: 1,
        fitToHeight: 999999,
      },
    });

    if (bonusData.courseBonuses?.length) {
      this.insertTableHeader(ws, query, bonusData);
      this.insertData(ws, query, bonusData);
    }

    let title = this.headers.bonusReport;

    if (query.tutorId) {
      title += ` - ${query.tutorName}`;
    }

    this.setHeader(ws, title, query);

    ws.getColumn(1).width = 50;
    if (!bonusData.courseBonuses?.length) {
      ws.getCell(2, 1).value = this.headers.noPayments;
    }
  }

  insertTableHeader(ws: Worksheet, query: QueryReportDto, { weeks, checkpointDates }: BonusData) {
    let cell = ws.getCell(this.rowIndex, 1);
    cell.value = this.headers.course;
    cell.border = this.defaultBorder;

    ws.mergeCells(this.rowIndex, 1, this.rowIndex + 1, 1);

    const dateFormat = new DateFormat(dayjs, query.locale);

    let colIndex = CHECKPOINTS_START;

    weeks.forEach((week, weekIndex) => {
      const mergeWeek = row => ws.mergeCells(row, colIndex, row, colIndex + week.length - 1);

      cell = ws.getCell(this.rowIndex, colIndex);
      cell.value = `${this.headers.week} ${weekIndex + 1}`;

      mergeWeek(this.rowIndex);

      colIndex += week.length;
    });

    ws.getCell(this.rowIndex, colIndex).value = this.headers.bonusPercent;
    ws.mergeCells(this.rowIndex, colIndex, this.rowIndex + 1, colIndex);
    ws.getColumn(colIndex).width = 13;

    this.setHeaderDefaults(ws.getRow(this.rowIndex));

    this.rowIndex += 1;
    colIndex = CHECKPOINTS_START;

    checkpointDates.forEach(date => {
      const cell = ws.getCell(this.rowIndex, colIndex);

      const weekDay = dateFormat.getWeekdayTitle(date);
      const day = dayjs(date).format('DD');
      cell.value = `${day} ${weekDay}`;

      ws.getColumn(colIndex).width = 10;

      colIndex++;
    });

    this.setHeaderDefaults(ws.getRow(this.rowIndex), false);

    colIndex = CHECKPOINTS_START;

    weeks.forEach(week => {
      this.setWeekStartStyle(ws.getCell(FIRST_ROW, colIndex));
      this.setWeekStartStyle(ws.getCell(FIRST_ROW + 1, colIndex));

      if (week.length > 1) {
        ws.getCell(this.rowIndex, colIndex + 1).border = this.defaultBorder;
      }

      colIndex += week.length;
    });

    this.rowIndex++;
  }

  insertData(ws: Worksheet, query: QueryReportDto, { courseBonuses: bonusAmounts }: BonusData) {
    bonusAmounts.forEach(courseBonus => {
      let cell = ws.getCell(this.rowIndex, 1);
      cell.value = courseBonus.courseTitle;

      cell.border = this.defaultBorder;

      let colIndex = CHECKPOINTS_START;

      courseBonus.checkpoints.forEach(checkpoint => {
        cell = ws.getCell(this.rowIndex, colIndex);

        cell.value = checkpoint.failsCount;

        cell.style.border = this.defaultBorder;

        if (checkpoint.isFirst) {
          this.setWeekStartStyle(cell);
        }

        cell.fill = checkpoint.weekFailed ? this.Fill.Negative : this.Fill.Positive;

        colIndex++;
      });

      cell = ws.getCell(this.rowIndex, colIndex);
      cell.value = courseBonus.bonusPercent / 100;
      cell.border = this.defaultBorder;

      if (courseBonus.bonusPercent > 0) {
        cell.fill = this.Fill.Positive;
        cell.numFmt = '#,##0.00%;(#,##0.00%)';
      } else {
        cell.fill = this.Fill.Negative;
      }

      this.setWeekStartStyle(cell);

      this.rowIndex++;
    });
  }

  setWeekStartStyle(cell: Cell) {
    cell.style.border.left.color = { argb: '000000' };
  }
}
