import { Bonus, getPeriodName, ReportType } from '@lib/api/reports';
import { EntireMonthPeriod } from '@lib/helpers/date';
import { Injectable } from '@nestjs/common';
import * as dayjs from 'dayjs';
import { pick } from 'lodash';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { ReportApproveService } from 'src/reports/report-approve.service';
import { BonusConfigsService } from '../bonus-configs.service';
import { BonusStore } from '../bonus-store';

@Injectable()
export class StoredBonusLoader implements DataLoader {
  constructor(
    private readonly bonusConfigService: BonusConfigsService,
    private readonly store: BonusStore,
    private readonly approveService: ReportApproveService,
  ) {}

  async load(query: QueryReportDto) {
    const stored = await this.store.load({ ...query, year: query.year || 2000, month: query.month || 0, asRole: query.asRole });

    const period = EntireMonthPeriod.fromYearAndMonth(query.year, query.month);
    const bonusConfigs = await this.bonusConfigService.find(period);
    const isApproved = await this.approveService.isApproved({
      period: getPeriodName(query as any),
      reportType: ReportType.Bonus,
    });

    const bonus = Bonus.fromStore(period, stored, bonusConfigs, isApproved, dayjs);

    return pick(bonus, ['courseBonuses', 'weeks', 'checkpointDates', 'isEmpty', 'isApproved']);
  }
}
