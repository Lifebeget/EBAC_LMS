import { Submission } from '@entities/submission.entity';
import { Bonus, BonusAmountCalculator, Fail, PaymentRecord } from '@lib/api/reports';
import { EntireMonthPeriod } from '@lib/helpers/date';
import { AssessmentType, SubmissionStatus } from '@lib/types/enums';
import { SystemRole } from '@lib/types/enums/system-role.enum';
import { flatten, Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { camelizeKeys } from 'humps';
import { omit } from 'lodash';
import { DataLoader } from 'src/reports/data-loader';
import { QueryReportDto } from 'src/reports/dto/query-report.dto';
import { SubmissionPaymentsLoader } from 'src/reports/payments/loaders/submission-payments.loader';
import { EntityManager } from 'typeorm';
import { BonusConfigsService } from '../bonus-configs.service';
import config from 'src/../config';

@Injectable()
export class BonusLoader implements DataLoader {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
    private readonly bonusConfigService: BonusConfigsService,
    private readonly paymentsLoader: SubmissionPaymentsLoader,
  ) {}

  async load(query: QueryReportDto, payments?: PaymentRecord[]) {
    const period = EntireMonthPeriod.fromYearAndMonth(query.year, query.month);

    const bonusConfigs = await this.bonusConfigService.find(period);

    if (![SystemRole.Teamlead, SystemRole.Tutor].includes(query.asRole)) {
      query.tutorId = null;
    }

    const loadingNeeded = !!query.tutorId || !payments || payments.length == 0;

    const allCoursePayments = loadingNeeded ? await this.paymentsLoader.load(query, true, true) : payments;
    const bonus = Bonus.fromPayments(period, allCoursePayments, bonusConfigs, dayjs);
    const fails = await this.getFails(bonus.checkpointDates, bonus.courseIds);
    bonus.calculate(fails);

    const amountCalculator = new BonusAmountCalculator(bonus, payments);
    amountCalculator.calculate();

    return omit(amountCalculator, ['bonus']);
  }

  private async getFails(dates: string[], courseIds?: string[]) {
    if (!dates) {
      return [];
    }

    const result = await Promise.all(dates.map(d => this.getFailsForDate(d, courseIds)));

    return flatten(result);
  }

  private async getFailsForDate(date: string, courseIds?: string[]): Promise<Fail[]> {
    const queryBuilder = this.entityManager
      .createQueryBuilder(Submission, 's')
      .select(['s.course_id', 'count(distinct s.id) as fails_count', 'json_agg(s.id) as sources'])
      .innerJoin('s.tariff', 't')
      .innerJoin('s.assessment', 'a')
      .where('s.submitted_at < :checkpoint')
      .andWhere('(s.graded_at >= :checkpoint or s.graded_at is null)')
      .andWhere('t.outdated_date < :checkpoint')
      .andWhere('a.type = :type', { type: AssessmentType.Default })
      .andWhere('s.is_question = false')
      .andWhere('s.status in (:...statuses)', { statuses: [SubmissionStatus.Submitted, SubmissionStatus.Graded] })
      .groupBy('s.course_id');

    // Bonus reports are calculated based on the local time for the particular platform
    // San Paulo for BZ platform, Mexico for MX platform
    const dateParameter = dayjs.tz(date, config.timeZone).startOf('day').add(1, 'day').toString();

    queryBuilder.setParameter('checkpoint', dateParameter);

    if (courseIds) {
      queryBuilder.andWhere('s.course_id in (:...courseIds)', { courseIds });
    }

    const result = await queryBuilder.getRawMany();

    return (camelizeKeys(result) as Fail[]).map(c => ({
      ...c,
      date,
    }));
  }
}
