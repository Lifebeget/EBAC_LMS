import { ReportApprove } from '@entities/report-approve.entity';
import { StoredBonus } from '@entities/stored-bonus.entity';
import { getPeriodName, QueryReportPeriodsDto, ReportType } from '@lib/api/reports';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { LoadReportDto } from '../dto/load-report.dto';
import { SaveReportDto } from '../dto/save-report.dto';
import { PaymentsStore } from '../payments/payments-store';
import { ReportStore } from '../report-store';
import { BonusLoader } from './loaders/bonus.loader';

@Injectable()
export class BonusStore implements ReportStore {
  constructor(
    private readonly bonusLoader: BonusLoader,
    @InjectRepository(StoredBonus)
    private readonly repository: Repository<StoredBonus>,

    private readonly paymentsStore: PaymentsStore,
  ) {}

  async load(dto: LoadReportDto) {
    const { year, month, tutorId } = dto;

    const where: any = {
      period: getPeriodName({ year, month }),
    };

    if (tutorId) {
      const courseIds = await this.tutorCourseIds(dto);
      where.courseId = In(courseIds);
    }

    return this.repository.find({
      where,
      order: { courseTitle: 'ASC' },
    });
  }
  async loadPeriods({ periodStart, periodEnd }: QueryReportPeriodsDto) {
    const queryBuilder = this.repository
      .createQueryBuilder('p')
      .select(['p.period as period', 'a.period is not null as approved'])
      .leftJoin(ReportApprove, 'a', 'a.period = p.period and a.reportType = :reportType', { reportType: ReportType.Bonus })
      .distinct(true);

    if (periodStart) {
      queryBuilder.andWhere('p.date >= :periodStart', { periodStart });
    }

    if (periodEnd) {
      queryBuilder.andWhere('p.date <= :periodEnd', { periodEnd });
    }

    const result = await queryBuilder.getRawMany();

    return result.map(r => {
      const [year, month] = r.period.split('-');

      return {
        ...r,
        year: Number(year),
        month: Number(month),
        hasData: true,
      };
    });
  }
  async save({ year, month }: SaveReportDto) {
    const bonus = await this.bonusLoader.load({ year, month });
    const period = getPeriodName({ year, month });

    await this.repository.delete({ period });

    let toStore: StoredBonus[] = [];

    bonus.courseBonuses.forEach(courseBonus => {
      const checkpoints = [...courseBonus.checkpoints];

      const courseStoredBonuses = checkpoints.map(checkpoint => ({
        date: checkpoint.date,
        failsCount: checkpoint.failsCount,
        courseId: courseBonus.courseId,
        courseTitle: courseBonus.courseTitle,
        sources: checkpoint.sources,
        period,
      }));

      toStore = toStore.concat(courseStoredBonuses);
    });

    await this.repository.save(toStore);
  }

  async tutorCourseIds(dto: LoadReportDto) {
    const payments = await this.paymentsStore.load(dto);
    const courseIds = payments.filter(p => p.level?.target === ServiceLevelTarget.Submission).map(p => p.courseId);
    return Array.from(new Set(courseIds));
  }
}
