import { QueryReportPeriodsDto } from '@lib/api/reports';
import { LoadReportDto } from './dto/load-report.dto';
import { SaveReportDto } from './dto/save-report.dto';

export interface ReportStore {
  load(dto: LoadReportDto);
  loadPeriods(dto: QueryReportPeriodsDto);
  save(dto: SaveReportDto);
}
