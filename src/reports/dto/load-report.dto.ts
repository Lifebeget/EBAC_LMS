import { SystemRole } from '@lib/types/enums/system-role.enum';
import { ApiProperty } from '@nestjs/swagger';

export class LoadReportDto {
  @ApiProperty({ description: 'Year of the report' })
  year: number;

  @ApiProperty({ description: 'Month of the report' })
  month: number;

  @ApiProperty({ description: 'Id of tutor' })
  tutorId?: string;

  @ApiProperty({ description: 'Current role', enum: SystemRole })
  asRole: SystemRole;
}
