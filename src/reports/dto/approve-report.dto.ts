import { ReportName } from '@lib/api/reports';
import { ApiProperty } from '@nestjs/swagger/dist/decorators';

export class ApproveReportDto {
  @ApiProperty({ description: 'Year of the report' })
  year: number;

  @ApiProperty({ description: 'Month of the report' })
  month: number;
}
