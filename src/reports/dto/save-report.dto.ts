import { PartialType } from '@nestjs/swagger';
import { LoadReportDto } from './load-report.dto';

export class SaveReportDto extends PartialType(LoadReportDto) {}
