import { ApiProperty } from '@nestjs/swagger';
import { QueryReportPeriodsDto as QueryReportPeriodsDtoInterface } from '@lib/api/reports';

export class QueryReportPeriodsDto implements QueryReportPeriodsDtoInterface {
  @ApiProperty({ description: 'Filter by tutor', required: false })
  tutorId?: string;

  @ApiProperty({ description: 'Start date for filter', required: false })
  periodStart?: Date;

  @ApiProperty({ description: 'End date for filter', required: false })
  periodEnd?: Date;
}
