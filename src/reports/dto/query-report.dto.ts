import { SystemRole } from '@lib/types/enums/system-role.enum';
import { DatePart } from '@lib/types/enums/date-part.enum';
import { ApiProperty } from '@nestjs/swagger';

export class QueryReportDto {
  @ApiProperty({ description: 'Filter by tutor', required: false })
  tutorId?: string;

  @ApiProperty({ description: 'Tutor name', required: false })
  tutorName?: string;

  @ApiProperty({ description: 'Year for filter' })
  year?: number;

  @ApiProperty({ description: 'Month for filter' })
  month?: number;

  @ApiProperty({ description: 'Group results by', required: false })
  groupBy?: DatePart;

  @ApiProperty({ description: 'Locale for report', required: false })
  locale?: string;

  @ApiProperty({ description: 'Currency code', required: false })
  currency?: string;

  @ApiProperty({ description: 'Order field', required: false })
  orderBy?: string;

  @ApiProperty({ description: 'Order direction', required: false })
  orderDirection?: 'ASC' | 'DESC';

  @ApiProperty({
    description: 'Role',
    required: false,
    enum: SystemRole,
  })
  asRole?: SystemRole;
}
