import { ReportName } from 'aws-sdk/clients/cur';
import { DataLoader } from './data-loader';
import { ReportGenerator } from './report-generator';
import { ReportStore } from './report-store';
import { ReportType } from '@lib/api/reports';

export interface Report {
  loader: DataLoader;
  periodsLoader?: DataLoader;
  loaderStored?: DataLoader;
  store?: ReportStore;
  generators?: Generators;
  generateWith?: ReportName;
  reportType?: ReportType;
}

export type Generators = Record<string, ReportGenerator>;
export type Reports = Record<string, Report>;
