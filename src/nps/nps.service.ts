import { Course } from '@entities/course.entity';
import { Nps } from '@entities/nps.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Exception } from 'handlebars';
import { CoursesService } from 'src/courses/courses.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { IsNull, Repository } from 'typeorm';
import { CreateNpsDto } from './dto/create-nps.dto';
import { QueryNpsDto } from './dto/query-nps.dto';
import { UpdateNpsDto } from './dto/update-nps.dto';
import * as dayjs from 'dayjs';
import config from 'config';
import { lessThanOrNull } from 'src/lib/operators/less-than-or-null';
import { ViewUserProgressCount } from 'src/lib/views/view-user-progress-count.entity';
import { CourseStatus } from '@enums/course-status.enum';
import { User } from '@entities/user.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';

export enum NpsTriggerLogic {
  modulesCompleted = 'modulesCompleted',
  modulesLeft = 'modulesLeft',
}

export interface NpsTrigger {
  type: string;
  logic: NpsTriggerLogic;
  value: number;
}

export interface NpsConfig {
  suspendedDelayHours: number;
  minimumDelayBetweenNpsHours: number;
  triggers: NpsTrigger[];
}

@Injectable()
export class NpsService {
  private cfg: NpsConfig;
  constructor(
    @InjectRepository(Nps)
    private npsRepository: Repository<Nps>,
    private socketsGateway: SocketsGateway,
    private readonly coursesService: CoursesService,
    protected readonly enrollmentService: EnrollmentsService,
  ) {
    this.cfg = {
      suspendedDelayHours: config.nps.suspendedDelayHours || 24,
      minimumDelayBetweenNpsHours: config.nps.minimumDelayBetweenNpsHours || 336,
      triggers: config.nps.triggers || [],
    };
  }

  async create(createNpsDto: CreateNpsDto, userProgress?: ViewUserProgressCount) {
    const createFields: any = {
      ...createNpsDto,
    };
    if (createNpsDto.score !== undefined) {
      createFields.completed = new Date();
    }

    let progress = userProgress;

    if (!progress) {
      const progressArray = await this.coursesService.viewUserProgressCount(createNpsDto.userId, createNpsDto.courseId);

      progress = progressArray[0];

      if (!progress) {
        throw new Exception('Combination of course and user is not valid');
      }
    }

    createFields.modulesCompleted = progress.modulesRequiredCompleted;
    createFields.courseProgress = progress.progress;

    const entity = await this.npsRepository.save(createFields);
    return entity;
  }

  async findAll(paging: PaginationDto, query: QueryNpsDto): Promise<Paginated<Nps[]>> {
    const qb = this.npsRepository
      .createQueryBuilder('nps')
      .innerJoinAndMapOne('nps.user', User, 'user', 'uuid(nps.user_id) = "user".id')
      .leftJoin('user.tags', 'tags')
      .innerJoinAndMapOne('nps.course', Course, 'course', 'uuid(nps.course_id) = course.id');

    if (query.sortField) {
      let sortField = '';
      switch (query.sortField) {
        case 'user':
          sortField = 'user.name';
          break;
        case 'course':
          sortField = 'course.title';
          break;
        default:
          sortField = `nps.${query.sortField}`;
          break;
      }
      qb.addOrderBy(sortField, query.sortDirection);
    } else {
      qb.addOrderBy('nps.completed', 'DESC');
    }

    qb.addOrderBy('nps.id', 'ASC');

    if (query.userId) {
      qb.andWhere('nps.userId = :userId', {
        userId: query.userId,
      });
    }

    if (query.courseId) {
      qb.andWhere('nps.courseId = :courseId', {
        courseId: query.courseId,
      });
    }

    if (query.type) {
      qb.andWhere('nps.type = :type', {
        type: query.type,
      });
    }

    if (query.scoreGreaterThan) {
      qb.andWhere('nps.score >= :scoreGreaterThan', {
        scoreGreaterThan: query.scoreGreaterThan,
      });
    }

    if (query.scoreLessThan) {
      qb.andWhere('nps.score <= :scoreLessThan', {
        scoreLessThan: query.scoreLessThan,
      });
    }

    if (query.createdAfter) {
      qb.andWhere('nps.created >= :createdAfter', {
        createdAfter: query.createdAfter,
      });
    }

    if (query.createdBefore) {
      qb.andWhere('nps.created < :createdBefore', {
        createdBefore: query.createdBefore,
      });
    }

    if (query.completedAfter && query.completedBefore && query.completedBefore < query.completedAfter) {
      throw new BadRequestException(`Before date should be greater than after date`);
    }

    if (query.completedAfter) {
      qb.andWhere('nps.completed >= :completedAfter', {
        completedAfter: query.completedAfter,
      });
    }

    if (query.completedBefore) {
      qb.andWhere('nps.completed < :completedBefore', {
        completedBefore: query.completedBefore,
      });
    }

    if (query.hasComment === true) {
      qb.andWhere(
        `(
          (nps.positiveComment is not null AND nps.positiveComment != '') 
          OR 
          (nps.negativeComment is not null AND nps.negativeComment != '')
        )`,
      );
    } else if (query.hasComment === false) {
      qb.andWhere('(nps.positiveComment is null AND nps.negativeComment is null)');
    }

    if (query.isCompleted) {
      qb.andWhere('nps.completed is not null');
    }

    //Filter by courses
    if (query.coursesIds && query.coursesIds.length) {
      qb.andWhere('course_id in (:...coursesIds)', {
        coursesIds: query.coursesIds,
      });
    }

    //Filter by scores
    if (query.scores?.length) {
      qb.andWhere('score in (:...scores)', { scores: query.scores });
    }

    if (query.tagIds?.length) {
      qb.andWhere('tags.id in (:...tagIds)', {
        tagIds: query.tagIds,
      });
    }

    if (!paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    const [results, total] = await qb.getManyAndCount();

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  findOne(id: string): Promise<Nps> {
    return this.npsRepository.findOne(id);
  }

  @Transactional()
  findOnePending(userId: string, courseId: string): Promise<Nps> {
    const suspendedDelayTime = dayjs().subtract(this.cfg.suspendedDelayHours, 'hours').toDate();
    return this.npsRepository.findOne({
      userId,
      courseId,
      completed: IsNull(),
      declined: IsNull(),
      suspended: lessThanOrNull(suspendedDelayTime),
    });
  }

  async update(id: string, updateNpsDto: UpdateNpsDto) {
    const updateFields = { ...updateNpsDto, id };
    const entity = await this.npsRepository.save(updateFields);
    return entity;
  }

  remove(id: string) {
    return this.npsRepository.softDelete(id);
  }

  isSuspended(nps: Nps): boolean {
    const minSuspendDate = dayjs().subtract(this.cfg.suspendedDelayHours, 'hours');
    return nps.suspended && dayjs(nps.suspended).diff(minSuspendDate) > 0;
  }

  isCompletedRecently(nps: Nps): boolean {
    const minLastDate = dayjs().subtract(this.cfg.minimumDelayBetweenNpsHours, 'hours');
    return nps.completed && dayjs(nps.completed).diff(minLastDate) > 0;
  }

  async checkAndCreate(userId: string, courseId: string): Promise<Nps | undefined> {
    if (!userId || !courseId) {
      throw new Exception('Both userId and courseId should be provided');
    }
    if (!this.cfg.triggers) {
      return;
    }
    const allUserNps = await this.npsRepository.find({
      where: { userId, courseId },
    });

    // No new NPS if active NPS exists
    const activeNps = allUserNps.find(nps => !nps.completed && !nps.declined);
    if (activeNps) {
      if (!this.isSuspended(activeNps)) {
        this.socketsGateway.sendNpsAvailable(activeNps);
      }
      return;
    }

    // No new NPS if user recently completed NPS
    if (allUserNps.some(nps => this.isCompletedRecently(nps))) {
      return;
    }

    const progress = (await this.coursesService.viewUserProgressCount(userId, courseId))[0];

    if (!progress) {
      return;
    }

    // We work with the triggers backward
    // Coz if user is applicable for last nps, we can skip first one
    const triggers = this.cfg.triggers.reverse();

    for (let i = 0, l = triggers.length; i < l; i++) {
      const trigger = triggers[i];
      if (allUserNps.some(nps => nps.type == trigger.type)) {
        // User already has NPS of this type
        // Whatever completed or not, nothing to do here anymore
        return;
      }
      if (trigger.logic == NpsTriggerLogic.modulesCompleted) {
        if (progress.modulesRequiredCompleted < trigger.value) {
          continue;
        }
      } else if (trigger.logic == NpsTriggerLogic.modulesLeft) {
        if (
          progress.courseStatus !== CourseStatus.completed ||
          progress.modulesRequiredTotal - progress.modulesRequiredCompleted > trigger.value
        ) {
          continue;
        }
      }
      // Applicable for current trigger. Time to make one
      const res = await this.create(
        {
          userId,
          courseId,
          type: trigger.type,
        },
        progress,
      );
      this.socketsGateway.sendNpsAvailable(res);
      return res;
    }
  }
}
