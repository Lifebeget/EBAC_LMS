import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NpsController } from './nps.controller';
import { NpsService } from './nps.service';
import { SocketsModule } from 'src/sockets/sockets.module';
import { Nps } from '@entities/nps.entity';
import { CoursesModule } from 'src/courses/courses.module';

@Module({
  imports: [TypeOrmModule.forFeature([Nps]), forwardRef(() => SocketsModule), forwardRef(() => CoursesModule)],
  controllers: [NpsController],
  providers: [NpsService],
  exports: [NpsService],
})
export class NpsModule {}
