import { Nps } from '@entities/nps.entity';
import { Permission } from '@enums/permission.enum';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  Query,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, Paginated, PaginationDto } from 'src/pagination.dto';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateNpsDto } from './dto/create-nps.dto';
import { SubmitNpsDto } from './dto/submit-nps.dto';
import { QueryNpsDto } from './dto/query-nps.dto';
import { NpsService } from './nps.service';

interface extendedSubmitNpsDto extends SubmitNpsDto {
  completed?: Date;
  suspended?: Date;
  declined?: Date;
}

@ApiTags('notifications')
@ApiBearerAuth()
@Controller('nps')
export class NpsController {
  constructor(private readonly npsService: NpsService) {}

  @Get()
  @RequirePermissions(Permission.NPS_VIEW)
  @ApiOperation({ summary: 'List all NPS' })
  @ApiPaginatedResponse(Nps)
  @PgConstraints()
  findAll(
    @Query(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    query: QueryNpsDto,
    @Pagination() paging: PaginationDto,
  ): Promise<Paginated<Nps[]>> {
    return this.npsService.findAll(paging, query);
  }

  @Get(':id')
  @RequirePermissions(Permission.NPS_VIEW)
  @ApiOperation({ summary: 'Get one NPS by Id' })
  @PgConstraints()
  findOne(@Param('id', new ParseUUIDPipe()) id: string): Promise<Nps> {
    return this.npsService.findOne(id);
  }

  @Get('course/:courseId')
  @ApiOperation({ summary: 'Get pending/new NPS for user for courseId' })
  @PgConstraints()
  async findOnePendingForCourse(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Req() req: Express.Request): Promise<Nps> {
    const pending = await this.npsService.findOnePending(req.user.id, courseId);
    return pending || this.npsService.checkAndCreate(req.user.id, courseId);
  }

  @Post()
  @RequirePermissions(Permission.NPS_MANAGE)
  @ApiOperation({
    summary: 'Create new NPS (service method, NPS usually created automatically)',
  })
  @ApiPaginatedResponse(Nps)
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  )
  @PgConstraints()
  create(@Body() dto: CreateNpsDto): Promise<Nps> {
    return this.npsService.create(dto);
  }

  @Post('submit/:id')
  @ApiOperation({ summary: 'Submit user response to NPS' })
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  )
  async submit(@Param('id') id: string, @Body() dto: SubmitNpsDto, @Req() req: Express.Request): Promise<Partial<Nps>> {
    const nps = await this.getNpsAndCheckOwnership(id, req.user.id); // It Throws

    if (nps.declined) {
      throw new BadRequestException('ERROR.NPS_ALREADY_DECLINED');
    }

    const fields = { ...dto } as extendedSubmitNpsDto;
    if (!nps.completed && dto.score !== undefined) {
      fields.completed = new Date();
    }
    return this.npsService.update(id, fields);
  }

  @Post('suspend/:id')
  @ApiOperation({ summary: 'Suspend NPS until next time' })
  async suspend(@Param('id') id: string, @Req() req: Express.Request): Promise<Partial<Nps>> {
    const nps = await this.getNpsAndCheckOwnership(id, req.user.id); // It Throws

    if (nps.completed) {
      throw new BadRequestException('ERROR.NPS_ALREADY_COMPLETED');
    }

    const fields = {
      suspended: new Date(),
    } as extendedSubmitNpsDto;

    return this.npsService.update(id, fields);
  }

  @Post('decline/:id')
  @ApiOperation({ summary: 'Decline NPS' })
  async decline(@Param('id') id: string, @Req() req: Express.Request): Promise<Partial<Nps>> {
    const nps = await this.getNpsAndCheckOwnership(id, req.user.id); // It Throws

    if (nps.completed) {
      throw new BadRequestException('ERROR.NPS_ALREADY_COMPLETED');
    }

    const fields = {
      declined: new Date(),
    } as extendedSubmitNpsDto;

    return this.npsService.update(id, fields);
  }

  @Delete(':id')
  @RequirePermissions(Permission.NPS_MANAGE)
  @ApiOperation({ summary: 'Soft delete NPS' })
  async delete(@Param('id') id: string) {
    return this.npsService.remove(id);
  }

  async getNpsAndCheckOwnership(id, userId) {
    const nps = await this.npsService.findOne(id);
    if (!nps) {
      throw new NotFoundException('ERROR.NPS_NOT_FOUND');
    }
    if (nps.userId !== userId) {
      throw new ForbiddenException('ERROR.NPS_NOT_BELONGS_TO_CURRENT_USER');
    }
    return nps;
  }
}
