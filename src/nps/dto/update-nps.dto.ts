import { PartialType } from '@nestjs/swagger';
import { CreateNpsDto } from './create-nps.dto';

export class UpdateNpsDto extends PartialType(CreateNpsDto) {}
