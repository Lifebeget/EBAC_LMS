import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsDate, IsInt, IsNumber, IsOptional, IsUUID, Max, Min } from 'class-validator';
import { QuerySorted } from 'src/assessments/dto/query-sorted';

export class QueryNpsDto extends QuerySorted {
  @ApiProperty({ description: 'ID of the user', required: false })
  @IsOptional()
  @IsUUID()
  userId?: string;

  @ApiProperty({ description: 'ID of the course', required: false })
  @IsOptional()
  @IsUUID()
  courseId?: string;

  @ApiProperty({
    description: 'Filter by courses',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  coursesIds?: string[];

  @ApiProperty({
    description: 'Filter by tags',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  tagIds?: string[];

  @ApiProperty({ description: 'NPS type (first, second...)', required: false })
  @IsOptional()
  type?: string;

  @ApiProperty({ description: 'Score greater or equal', required: false })
  @IsOptional()
  @Transform(p => Number(p.value))
  @IsInt()
  @Max(10)
  @Min(0)
  scoreGreaterThan?: number;

  @ApiProperty({ description: 'Score less than', required: false })
  @IsOptional()
  @Transform(p => Number(p.value))
  @IsInt()
  @Max(10)
  @Min(0)
  scoreLessThan?: number;

  @ApiProperty({
    description: 'Date of creation greater or equal than',
    required: false,
  })
  @Transform(p => new Date(p.value))
  @IsDate()
  @IsOptional()
  createdAfter?: Date;

  @ApiProperty({ description: 'Date of creation less than', required: false })
  @Transform(p => new Date(p.value))
  @IsDate()
  @IsOptional()
  createdBefore?: Date;

  @ApiProperty({
    description: 'Date of completion greater or equal than',
    required: false,
  })
  @Transform(p => new Date(p.value))
  @IsDate()
  @IsOptional()
  completedAfter?: Date;

  @ApiProperty({ description: 'Date of completion less than', required: false })
  @Transform(p => new Date(p.value))
  @IsDate()
  @IsOptional()
  completedBefore?: Date;

  @ApiProperty({
    description: 'Has positive or negative comment',
    required: false,
  })
  @Transform(p => p.value.toLowerCase() == 'true')
  @IsBoolean()
  @IsOptional()
  hasComment?: boolean;

  @ApiProperty({ description: 'Is answer competed' })
  @Transform(p => p.value.toLowerCase() == 'true')
  @IsBoolean()
  @IsOptional()
  isCompleted?: boolean;

  @ApiProperty({
    description: 'Filter by score',
    required: false,
  })
  @Transform(p => p.value.map((val: string) => Number(val)))
  @IsOptional()
  @IsNumber({}, { each: true })
  scores?: number[];
}
