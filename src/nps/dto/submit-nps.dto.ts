import { PickType } from '@nestjs/mapped-types';
import { CreateNpsDto } from './create-nps.dto';

export class SubmitNpsDto extends PickType(CreateNpsDto, ['score', 'negativeComment', 'positiveComment']) {}
