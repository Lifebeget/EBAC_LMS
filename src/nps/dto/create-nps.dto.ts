import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsUUID, Max, Min } from 'class-validator';

export class CreateNpsDto {
  @ApiProperty({ description: 'ID of the user' })
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'ID of the course' })
  @IsUUID()
  courseId: string;

  @ApiProperty({ description: 'NPS type (first, second...)' })
  @IsNotEmpty()
  type: string;

  @ApiProperty({ description: 'Score from 0 to 10', required: false })
  @IsOptional()
  @IsInt()
  @Max(10)
  @Min(0)
  score?: number;

  @ApiProperty({ description: 'Positive comment', required: false })
  @IsOptional()
  positiveComment?: string;

  @ApiProperty({ description: 'Suggestions for improvement', required: false })
  @IsOptional()
  negativeComment?: string;
}
