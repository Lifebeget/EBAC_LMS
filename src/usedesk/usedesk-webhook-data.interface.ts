export interface UsedeskWebhookData {
  ticket: {
    id: string;
    channel_id: number;
    message: string;
    files: string[];
  };
  client: {
    name: string;
    phones: { phone: string }[];
    emails: { email: string }[];
  };
}
