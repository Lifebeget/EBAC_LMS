import { Injectable } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import IORedis = require('ioredis');

export const DEFAULT_STORE_EXPIRE = 24 * 3600 * 1000;

@Injectable()
export class StoreService {
  private readonly env: string = process.env.ENVIRONMENT || 'local';

  constructor(protected readonly redis: RedisService) {}

  private prefix(name): string {
    return `${process.env.ENVIRONMENT}-store:${name}`;
  }

  private getClient(): IORedis.Redis {
    return this.redis.getClient();
  }

  /**
   * Set a value
   * @param {string} name key name
   * @param {string} value value
   * @param {number} [expire] milliseconds, TTL for the redis key
   * @returns {boolean} true: success, false: failed
   */
  public async set(name: string, value: string, expire = DEFAULT_STORE_EXPIRE, overwrite = true) {
    const client = this.getClient();
    const result = await client.set(this.prefix(name), value, 'PX', expire, overwrite ? 'GET' : 'NX');
    // console.log(`set: ${name}, value: ${value}, result: ${result}`);
    return result !== null;
  }

  /**
   * Get a value
   * @param {string} name key name
   * @returns {string | null} value
   */
  public async get(name): Promise<string | null> {
    const client = this.getClient();
    const result = await client.get(this.prefix(name));
    // console.log(`get: ${name}, result: ${result}`);
    return result;
  }

  /**
   * Get a value
   * @param {string} name key name
   * @returns {string | null} value
   */
  public async del(name): Promise<boolean> {
    const client = this.getClient();
    const result = await client.del(this.prefix(name));
    // console.log(`del: ${name}, result: ${result}`);
    return result == 1;
  }

  /**
   * Set a mutex (value is current time)
   * @param {string} name mutex name
   * @param {number} [expire] milliseconds, TTL for the redis key
   * @returns {boolean} true: success, false: failed
   */
  public setMutex(name: string, expire = DEFAULT_STORE_EXPIRE) {
    return this.set(name, new Date().getTime().toString(), expire);
  }

  /**
   * Check if mutex is set
   * @param {string} name lock name
   * @param {number} [expire] milliseconds, TTL for the redis key
   * @returns {boolean} true: success, false: failed
   */
  public async hasMutex(name: string) {
    return (await this.get(name)) !== null;
  }

  /**
   * Clear mutex
   * @param {string} name lock name
   * @param {number} [expire] milliseconds, TTL for the redis key
   * @returns {boolean} true: success, false: failed
   */
  public clearMutex(name: string) {
    return this.del(name);
  }
}
