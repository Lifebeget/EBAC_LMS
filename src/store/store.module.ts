import { Module } from '@nestjs/common';
import { RedisModule } from 'nestjs-redis';
import { StoreService } from './store.service';

@Module({
  imports: [RedisModule],
  providers: [StoreService],
  exports: [StoreService],
})
export class StoreModule {}
