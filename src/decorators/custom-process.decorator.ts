import { Process } from '@nestjs/bull';
import { applyDecorators } from '@nestjs/common';

export function CustomProcess(options?: any) {
  if (process.env.CLIENT_MODE !== 'true') {
    return applyDecorators(Process(options));
  } else {
    return applyDecorators();
  }
}
