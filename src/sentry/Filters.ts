export const filterByTestEmail = ({ event, hint }) => {
  // DONT TRACK AUTO TEST
  if (event.user && event.user.email === 'auto.test.ebac@gmail.com') return { event: null, hint };
  return { event, hint };
};
