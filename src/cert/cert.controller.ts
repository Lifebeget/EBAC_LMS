import { Cert } from '@entities/cert.entity';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  Query,
  Req,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Interapp } from 'src/auth/interapp.decorator';
import { Public } from 'src/auth/public.decorator';
import { CertService } from './cert.service';
import { CreateAllWebinarCertDTO, CreateCertDTO } from './dto/create-cert.dto';
import * as express from 'express';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { Pagination } from 'src/pagination.decorator';
import { PaginationDto } from 'src/pagination.dto';
import { QueryCertsDto } from './dto/query-cert.dto';
import { CreateCertsUsingQueueDto } from './dto/create-certs-using-queue.dto';
import { Permissions } from 'src/permissions.decorator';
import { CertsGenerationQueue } from '@entities/certs-generation-queue.entity';
import { QueryCertsGenerationProgressDto } from './dto/query-certs-generation-progress.dto';

@ApiBearerAuth()
@Controller()
export class CertController {
  constructor(private readonly certService: CertService) {}

  @Get('certs/:id')
  @RequirePermissions(Permission.CERTIFICATE_VIEW, Permission.WEBINAR_CERTIFICATE_VIEW)
  @Interapp()
  @ApiOperation({ summary: 'Get a cert by id' })
  @ApiResponse({ status: 200, description: 'Get a cert by id', type: Cert })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Express.Request) {
    const cert = await this.checkCertificateRights(id, req.user.permissions);
    return cert;
  }

  @Get('certs/validate/:id')
  @Public()
  @ApiOperation({ summary: 'Get a cert by id' })
  @ApiResponse({ status: 200, description: 'Get a cert by id', type: Cert })
  async findOneByUId(@Param('id') id: string, @Query('url') returnUrl: boolean, @Res({ passthrough: true }) res: express.Response) {
    const printedCert = await this.certService.findOneByUIdAndPrint(id);
    if (returnUrl) return { url: printedCert.file.url };
    return res.redirect(303, printedCert.file.url);
  }

  @Get('course/:id/certs')
  @RequirePermissions(Permission.CERTIFICATE_VIEW)
  @Interapp()
  @ApiOperation({ summary: 'Get cert pdf and generate if doesnt exist' })
  @ApiResponse({ status: 200, description: 'Get a cert by id', type: [Cert] })
  async findByCourse(@Pagination() paging: PaginationDto, @Query() query: QueryCertsDto, @Param('id', new ParseUUIDPipe()) id: string) {
    return this.certService.findByCourse(id, query, paging);
  }

  @Get('webinar/:id/certs')
  @Interapp()
  @RequirePermissions(Permission.WEBINAR_CERTIFICATE_VIEW)
  @ApiOperation({ summary: 'Get certs for webinar' })
  @ApiResponse({
    status: 200,
    description: 'Get a cert by webinar id',
    type: [Cert],
  })
  async findByWebinar(@Pagination() paging: PaginationDto, @Query() query: QueryCertsDto, @Param('id', new ParseUUIDPipe()) id: string) {
    return this.certService.findByWebinar(id, query, paging);
  }

  @Post('certs')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_VIEW)
  @ApiOperation({ summary: 'Create new cert' })
  @ApiResponse({ status: 201, description: 'New cert created', type: Cert })
  async create(@Body() createCertDTO: CreateCertDTO, @Permissions() perms: Permission[]) {
    if (!perms.includes(Permission.CERTIFICATE_MANAGE) && createCertDTO.courseId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_CERTS_PERMISSION');
    }
    if (!perms.includes(Permission.WEBINAR_CERTIFICATE_MANAGE) && createCertDTO.webinarId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_CERTS_PERMISSION');
    }
    return this.certService.create(createCertDTO);
  }

  @Post('certs/webinar/:id')
  @RequirePermissions(Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Create certs for all subscribed users' })
  @ApiResponse({ status: 201, description: 'New cert created', type: Cert })
  async CreateAllWebinarCerts(@Param('id', new ParseUUIDPipe()) id: string, @Body() _createAllWebinarCertDTO: CreateAllWebinarCertDTO) {
    return await this.certService.createAll(id);
  }

  @Post('certs/using-queue')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Create certs using queues' })
  @ApiResponse({
    status: 201,
    description: 'Created queue',
    type: CertsGenerationQueue,
  })
  async generateUsingQueue(@Body() dto: CreateCertsUsingQueueDto, @Permissions() perms: Permission[]) {
    if (!perms.includes(Permission.CERTIFICATE_MANAGE) && dto.courseId) {
      throw new UnauthorizedException('REQUIRE_COURSE_CERTS_PERMISSION');
    }
    if (!perms.includes(Permission.WEBINAR_CERTIFICATE_MANAGE) && dto.webinarId) {
      throw new UnauthorizedException('REQUIRE_WEBINAR_CERTS_PERMISSION');
    }
    return this.certService.createUsingQueue(dto);
  }

  @Get('certs/queue/:queueId')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Get cert generation progress' })
  @ApiResponse({
    status: 200,
    description: 'Certs generation progress',
  })
  async generationProgressByQueueId(@Param('queueId', new ParseUUIDPipe()) queueId: string) {
    return await this.certService.generationProgressByQueueId(queueId);
  }

  @Delete('certs/queue/:queueId')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Delete cert generation queue' })
  @ApiResponse({
    status: 200,
    description: 'Delete generation queue',
  })
  async cancelQueue(@Param('queueId', new ParseUUIDPipe()) queueId: string) {
    return await this.certService.cancelQueue(queueId);
  }

  @Get('certs/queue/webinar/:webinarId')
  @RequirePermissions(Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({
    summary: 'Get cert generation progress by webinarID and status = active',
  })
  @ApiResponse({
    status: 200,
    description: 'Certs generation progress',
  })
  async generationProgressByWebinarId(
    @Param('webinarId', new ParseUUIDPipe()) webinarId: string,
    @Query() query: QueryCertsGenerationProgressDto,
  ) {
    return this.certService.generationProgressByWebinarId(webinarId, query);
  }

  @Get('certs/queue/course/:courseId')
  @RequirePermissions(Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({
    summary: 'Get cert generation progress by courseId and status = active',
  })
  @ApiResponse({
    status: 200,
    description: 'Certs generation progress',
  })
  async generationProgressByCourseId(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Query() query: QueryCertsGenerationProgressDto,
  ) {
    return this.certService.generationProgressByCourseId(courseId, query);
  }

  @Post('certs/:id/print')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Create new cert' })
  @ApiResponse({ status: 201, description: 'New cert created', type: Cert })
  async print(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    await this.checkCertificateRights(id, perms); // Throws
    return this.certService.print(id);
  }

  @Delete('certs/:id')
  @RequirePermissions(Permission.CERTIFICATE_MANAGE, Permission.WEBINAR_CERTIFICATE_MANAGE)
  @ApiOperation({ summary: 'Delete cert' })
  @ApiResponse({ status: 201, description: 'Cert deleted' })
  async del(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    await this.checkCertificateRights(id, perms); // Throws
    return this.certService.del(id);
  }

  @Get('user/:userId/certs')
  @RequirePermissions(Permission.USERS_VIEW)
  findByUser(@Param('userId') userId: string) {
    return this.certService.findByUser(userId);
  }

  async checkCertificateRights(id: string, perms: Permission[]): Promise<Cert> {
    const cert = await this.certService.findOne(id);
    if (!cert) {
      throw new NotFoundException('ERRORS.CERTIFICATE_NOT_FOUND');
    }
    if (cert.webinar && !perms.includes(Permission.WEBINAR_CERTIFICATE_MANAGE)) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_CERTS_PERMISSION');
    }
    if (!cert.webinar && !perms.includes(Permission.CERTIFICATE_MANAGE)) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_CERTS_PERMISSION');
    }
    return cert;
  }
}
