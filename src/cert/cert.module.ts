import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CertService } from './cert.service';
import { CertController } from './cert.controller';
import { Cert } from '../lib/entities/cert.entity';
import { File } from '@entities/file.entity';
import { User } from '@entities/user.entity';
import { Role } from '@entities/role.entity';
import { UserActivity } from '@entities/user-activity.entity';
import { Webinar } from '@entities/webinars.entity';
import { Trigger } from '@entities/trigger.entity';
import { SettingsModule } from 'src/settings/settings.module';
import { SocketsModule } from 'src/sockets/sockets.module';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { BullModule } from '@nestjs/bull';
import { CertsGenerationQueue } from '@entities/certs-generation-queue.entity';
import { Tag } from '@entities/tag.entity';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { TagCategory } from '@entities/tag-category.entity';
import { CoursesModule } from 'src/courses/courses.module';
import { Segment } from '@entities/segment.entity';
import { PdfModule } from 'src/pdf/pdf.module';
import { UsersModule } from 'src/users/users.module';
import { WebinarModule } from 'src/webinar/webinar.module';
import { TriggerModule } from 'src/trigger/trigger.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cert]),
    TypeOrmModule.forFeature([File]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Role]),
    TypeOrmModule.forFeature([UserActivity]),
    TypeOrmModule.forFeature([Webinar]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([CertsGenerationQueue]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([TagCategory]),
    TypeOrmModule.forFeature([FeatureFlag]),
    TypeOrmModule.forFeature([Segment]),
    BullModule.registerQueue({
      name: 'certs',
    }),
    forwardRef(() => SettingsModule),
    forwardRef(() => SocketsModule),
    forwardRef(() => AssessmentsModule),
    forwardRef(() => CoursesModule),
    forwardRef(() => PdfModule),
    forwardRef(() => UsersModule),
    forwardRef(() => WebinarModule),
    forwardRef(() => TriggerModule),
  ],
  controllers: [CertController],
  providers: [CertService],
  exports: [CertService],
})
export class CertModule {}
