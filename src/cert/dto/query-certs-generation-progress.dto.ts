import { QueueStatus } from '@lib/types/enums/queue.enum';
import { IsEnum, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QueryCertsGenerationProgressDto {
  @ApiProperty({
    description: 'Queue status (completed, failed, ...)',
    required: false,
    enum: QueueStatus,
  })
  @IsEnum(QueueStatus)
  @IsOptional()
  status: QueueStatus;
}
