import { IsArray, IsBoolean, IsOptional, IsUUID, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCertsUsingQueueDto {
  @ApiProperty({
    description: 'Array of users id',
  })
  @IsArray()
  @IsUUID(4, { each: true })
  userIds: string[];

  @ApiProperty({ description: 'Course ID' })
  @IsUUID()
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  courseId?: string;

  @ApiProperty({ description: 'Webinar ID' })
  @IsUUID()
  @ValidateIf(o => o.webinarId !== null)
  webinarId?: string;

  @ApiProperty({ description: 'Need to send an email' })
  @IsBoolean()
  @IsOptional()
  sendEmail?: boolean;

  @ApiProperty({ description: 'Create certificates even if they are already created' })
  @IsBoolean()
  @IsOptional()
  forceCreate?: boolean;
}
