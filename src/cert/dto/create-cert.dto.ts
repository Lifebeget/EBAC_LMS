export interface CreateCertDTO {
  userId?: string;
  userIds?: Array<string>;
  courseId?: string;
  webinarId?: string;
  sendEmail?: boolean;
  template?: string;
}

export interface CreateAllWebinarCertDTO {
  sendEmail?: boolean;
}
