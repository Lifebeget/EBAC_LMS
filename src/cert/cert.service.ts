import { Cert } from '@entities/cert.entity';
import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { generateNumericId } from 'src/helpers';
import { PaginationDto } from 'src/pagination.dto';
import { PdfService } from 'src/pdf/pdf.service';
import { CertTriggerService } from 'src/trigger/services/cert-trigger.service';
import { UsersService } from 'src/users/users.service';
import { WebinarService } from 'src/webinar/webinar.service';
import { Repository } from 'typeorm';
import { CreateCertDTO } from './dto/create-cert.dto';
import { QueryCertsDto } from './dto/query-cert.dto';
import { CreateCertsUsingQueueDto } from './dto/create-certs-using-queue.dto';
import { InjectQueue, OnQueueCompleted, OnQueueFailed, Process, Processor } from '@nestjs/bull';
import { Job, Queue } from 'bull';
import { CertsGenerationQueue } from '@entities/certs-generation-queue.entity';
import { QueueStatus } from '@lib/types/enums/queue.enum';
import { QueryCertsGenerationProgressDto } from './dto/query-certs-generation-progress.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CoursesService } from 'src/courses/courses.service';
import config from 'config';
import { CustomProcess } from 'src/decorators/custom-process.decorator';

interface CertsGenerationJobData extends CreateCertsUsingQueueDto {
  queueId: string;
  userId: string;
}
interface CertsGenerationJob {
  name: string;
  data: CertsGenerationJobData;
}

@Processor('certs')
@Injectable()
export class CertService {
  constructor(
    @InjectRepository(Cert)
    private certsRepository: Repository<Cert>,
    @InjectRepository(CertsGenerationQueue)
    private certsGenerationQueueRepository: Repository<CertsGenerationQueue>,
    @InjectQueue('certs')
    private certsGeneratingQueue: Queue,

    private pdfService: PdfService,
    private usersService: UsersService,
    private webinarService: WebinarService,
    private coursesServie: CoursesService,
    private certTriggerService: CertTriggerService,
  ) {}

  async checkWebinarExisting(webinarId: string) {
    const webinar = await this.webinarService.findOne(webinarId);
    if (!webinar) {
      throw new NotFoundException(`Webinar ${webinarId} not found`);
    }
    return webinar;
  }
  async checkActiveQueueForWebinar(webinarId: string) {
    const queue = await this.certsGenerationQueueRepository.findOne({
      webinarId,
      status: QueueStatus.Active,
    });
    if (queue) {
      throw new ConflictException(`There is already a queue for the webinar ${webinarId}`);
    }
  }

  async checkCourseExisting(courseId: string) {
    const course = await this.coursesServie.findOne(courseId);
    if (!course) {
      throw new NotFoundException(`Course ${courseId} not found`);
    }
    return course;
  }
  async checkActiveQueueForCourse(courseId: string) {
    const queue = await this.certsGenerationQueueRepository.findOne({
      courseId,
      status: QueueStatus.Active,
    });
    if (queue) {
      throw new ConflictException(`There is already a queue for the course ${courseId}`);
    }
  }

  // Проверить, есть ли очерередь генерации сертификатов для указанного вебинара
  async checkQueueForWebinar(webinarId) {
    await Promise.all([this.checkWebinarExisting(webinarId), this.checkActiveQueueForWebinar(webinarId)]);
  }
  async checkQueueForCourse(courseId) {
    await Promise.all([this.checkCourseExisting(courseId), this.checkActiveQueueForCourse(courseId)]);
  }

  async generationProgressByQueueId(queueId: string) {
    const queue = await this.certsGenerationQueueRepository.findOne({
      id: queueId,
    });
    if (!queue) {
      return null;
    }
    return queue;
  }

  async generationProgressByWebinarId(webinarId: string, query: QueryCertsGenerationProgressDto) {
    await this.checkWebinarExisting(webinarId);
    let queue;
    if (query.status === QueueStatus.Active) {
      queue = await this.certsGenerationQueueRepository.findOne({
        webinarId,
        status: query.status,
      });

      // если в bull активных очередей нет, а в базе по какой-то причине есть, то закрываем со статусом Completed
      const activeQueue = await this.certsGeneratingQueue.getActive();
      if (activeQueue.length == 0 && queue?.status == 'active') {
        await this.certsGenerationQueueRepository.save({
          id: queue.id,
          status: QueueStatus.Completed,
        });
      }

      if (!queue) {
        // Попытка получить последнюю запущенную очередь для вебинара
        queue = await this.certsGenerationQueueRepository.findOne({
          where: {
            webinarId,
            status: QueueStatus.Completed,
          },
          order: { created: 'DESC' },
        });
      }
    } else {
      const findOptions: any = { webinarId };
      if ('status' in query) {
        findOptions.status = query.status;
      }
      queue = await this.certsGenerationQueueRepository.find(findOptions);
    }
    if (!queue) {
      return null;
    }
    return queue;
  }

  async generationProgressByCourseId(courseId: string, query: QueryCertsGenerationProgressDto) {
    await this.checkCourseExisting(courseId);
    let queue;
    if (query.status === QueueStatus.Active) {
      queue = await this.certsGenerationQueueRepository.findOne({
        courseId,
        status: query.status,
      });

      // если в bull активных очередей нет, а в базе по какой-то причине есть, то закрываем со статусом Completed
      const activeQueue = await this.certsGeneratingQueue.getActive();
      if (activeQueue.length == 0 && queue?.status == 'active') {
        await this.certsGenerationQueueRepository.save({
          id: queue.id,
          status: QueueStatus.Completed,
        });
      }

      if (!queue) {
        // Попытка получить последнюю запущенную очередь для вебинара
        queue = await this.certsGenerationQueueRepository.findOne({
          where: {
            courseId,
            status: QueueStatus.Completed,
          },
          order: { created: 'DESC' },
        });
      }
    } else {
      const findOptions: any = { courseId };
      if ('status' in query) {
        findOptions.status = query.status;
      }
      queue = await this.certsGenerationQueueRepository.find(findOptions);
    }
    if (!queue) {
      return null;
    }
    return queue;
  }

  @CustomProcess('generation')
  async consumeCertsGenerating(job: Job<CertsGenerationJobData>) {
    console.log('Cert print job:', job.data);
    const data = job.data.courseId ? { course: { id: job.data.courseId } } : { webinar: { id: job.data.webinarId } };
    let cert = await this.certsRepository.create({
      uniqueCertId: generateNumericId(job.data.webinarId, job.data.userId, null, null),
      user: { id: job.data.userId },
      ...data,
    });
    cert = await this.certsRepository.save(cert);
    if (!cert) {
      throw new NotFoundException('Cert not created');
    }
    if (job.data.courseId) {
      await this.certTriggerService.courseCertCreated(job.data.courseId, job.data.userId, cert.uniqueCertId, job.data.sendEmail);
    } else {
      await this.certTriggerService.webinarCertCreated(job.data.webinarId, job.data.userId, cert.uniqueCertId, job.data.sendEmail);
    }
    const print = await this.print(cert.id);
    if (!print) {
      throw new NotFoundException('Not printed');
    }
  }

  @Transactional()
  async getCertsGemerationQueue(id: string) {
    const queueEntity = await this.certsGenerationQueueRepository.findOne(id);
    return queueEntity;
  }

  async onQueueProcess(job: Job<CertsGenerationJobData>, queueProcess: QueueStatus) {
    const queueEntity = await this.getCertsGemerationQueue(job.data.queueId);
    if (!queueEntity) {
      throw new NotFoundException(`Queue ${job.data.queueId} not found`);
    }
    const currentProcessedCount = (queueEntity?.processed ?? 0) + 1;
    const entity: Partial<CertsGenerationQueue> = {
      id: job.data.queueId,
      processed: currentProcessedCount,
    };
    if (queueProcess === QueueStatus.Completed) {
      entity.completedForUsers = [...queueEntity.completedForUsers, job.data.userId];
    }
    if (queueProcess === QueueStatus.Failed) {
      entity.failedForUsers = [...queueEntity.failedForUsers, job.data.userId];
    }
    // При завершении очереди
    if (queueEntity.total === currentProcessedCount) {
      entity.status = QueueStatus.Completed;
    }
    await this.certsGenerationQueueRepository.save(entity);
  }

  @OnQueueCompleted()
  async onCompleted(job: Job<CertsGenerationJobData>) {
    await this.onQueueProcess(job, QueueStatus.Completed);
  }

  @OnQueueFailed()
  async onFailed(job: Job<CertsGenerationJobData>) {
    await this.onQueueProcess(job, QueueStatus.Failed);
  }

  async cancelQueue(queueId) {
    await this.certsGeneratingQueue.obliterate({ force: true });
    return await this.certsGenerationQueueRepository.save({
      id: queueId,
      status: QueueStatus.Canceled,
    });
  }

  async createUsingQueue(dto: CreateCertsUsingQueueDto) {
    let certs = null;

    if (!dto.webinarId && !dto.courseId) {
      throw new BadRequestException('webinarId or courseId should not be empty');
    } else if (dto.webinarId) {
      await this.checkQueueForWebinar(dto.webinarId);
      certs = await this.findByWebinar(dto.webinarId, {}, { page: 1, itemsPerPage: 100, showAll: true });
    } else if (dto.courseId) {
      await this.checkQueueForCourse(dto.courseId);
      certs = await this.findByCourse(dto.courseId, {}, { page: 1, itemsPerPage: 100, showAll: true });
    }
    let certsUserIds: string[] = certs?.data.map(cert => cert.user?.id) || [];
    certsUserIds = certsUserIds.filter(id => id != null);

    const certsWithoutFile: Cert[] = certs?.data.filter(cert => !cert.file) || [];
    const userIdsWithoutPrintedCert = certsWithoutFile.map(cert => cert.user?.id).filter(id => id != null);

    // Отфильтруем только те id ользователей, у которых вообще нет сертификата в БД, либо нет распечатанного сертификата (cert.file).
    // Если есть флаг forceCreate, то сертификаты создадутся и распечатаются для всех пользователей из запроса.
    if (userIdsWithoutPrintedCert && !dto.forceCreate) {
      dto.userIds = dto.userIds.filter(userId => {
        return !certsUserIds.includes(userId) || userIdsWithoutPrintedCert.includes(userId);
      });

      if (dto.userIds.length === 0) {
        return {
          result: 'notCreated',
          message: 'Certificates have already been generated and printed for all users from the request',
          code: 'certs.alreadyGenerated',
        };
      }
    }

    const queue = await this.certsGenerationQueueRepository.save({
      webinarId: dto.webinarId || null,
      courseId: dto.courseId || null,
      total: dto.userIds.length,
      processed: 0,
    });
    const jobsData: CertsGenerationJob[] = dto.userIds.map(userId => ({
      name: 'generation',
      data: { queueId: queue.id, userId, ...dto },
      opts: {
        prefix: dto.webinarId || dto.courseId,
      },
    }));
    await this.certsGeneratingQueue.addBulk(jobsData);
    return queue;
  }

  async create(createCertDTO: CreateCertDTO) {
    let usersIds = [];
    if (createCertDTO.userId) {
      usersIds = [createCertDTO.userId];
    } else if (createCertDTO.userIds) {
      usersIds = createCertDTO.userIds;
    } else {
      throw new Error('You must send userId or userIds');
    }

    const certsCreated = [];

    for (const userId of usersIds) {
      if (createCertDTO.courseId) {
        return await this.certsRepository.save({
          uniqueCertId: generateNumericId(createCertDTO.courseId, userId, null, null),
          course: { id: createCertDTO.courseId },
          user: { id: userId },
          template: createCertDTO.template ? createCertDTO.template : 'default',
        });
      } else if (createCertDTO.webinarId) {
        let cert = await this.certsRepository.create({
          uniqueCertId: generateNumericId(createCertDTO.webinarId, userId, null, null),
          webinar: { id: createCertDTO.webinarId },
          user: { id: userId },
        });

        cert = await this.certsRepository.save(cert);
        certsCreated.push(cert);

        await this.certTriggerService.webinarCertCreated(createCertDTO.webinarId, userId, cert.uniqueCertId, createCertDTO.sendEmail);
      } else {
        throw new NotFoundException('ERRORS.CERT_MUST_HAVE_WEBINAR_OR_COURSE');
      }
    }

    return {
      count: certsCreated.length,
      certs: [...certsCreated],
    };
  }

  async createAll(id: string) {
    const webinar = await this.webinarService.findOne(id);
    const users = await this.usersService.webinarSubscribers(id, {
      takeAll: true,
      skip: 0,
    });

    const certs = users.map(u => {
      return {
        uniqueCertId: generateNumericId(webinar.id, u.id, null, null),
        webinar: { id: webinar.id },
        user: { id: u.id },
      };
    });

    return await this.certsRepository.save(certs);
  }

  async print(certId: string) {
    const cert = await this.findOne(certId);

    if (!cert) {
      console.error('Cert not found: ' + certId);
      return;
    }

    const url = new URL(`${config.setUser(cert.user).get('STUDENT_LMS_URL')}/certs/${cert.id}/`);

    // Url ngrok
    // const url = new URL(
    //   `https://fa5a-5-189-88-80.eu.ngrok.io/certs/${cert.id}/`,
    // );

    const certTitle = `${cert.user.name} - ${cert.course ? cert.course.title : cert.webinar.name}`;

    const printParams = {
      width: 1648,
      height: 1192,
    };

    const file = await this.pdfService.printPage(url, printParams, 'certs', certTitle, cert.user);

    cert.file = file;
    return await this.certsRepository.save(cert);
  }

  async del(id: string) {
    return await this.certsRepository.delete(id);
  }

  @Transactional()
  async findOne(id: string) {
    return await this.certsRepository
      .createQueryBuilder('cert')
      .leftJoinAndSelect('cert.course', 'course')
      .leftJoinAndSelect('course.modules', 'modules')
      .leftJoinAndSelect('cert.webinar', 'webinar')
      .leftJoinAndSelect('webinar.topics', 'topics')
      .leftJoinAndSelect('cert.user', 'user')
      .leftJoinAndSelect('cert.file', 'file')
      .where('cert.id = :id', { id: id })
      .getOne();
  }

  async findOneByUIdAndPrint(id: string) {
    const cert = await this.certsRepository
      .createQueryBuilder('cert')
      .leftJoinAndSelect('cert.file', 'file')
      .where('cert.uniqueCertId = :id', { id: id })
      .getOne();

    if (!cert.file) {
      return this.print(cert.id);
    }

    return cert;
  }

  @Transactional()
  async findByCourse(id: string, query: QueryCertsDto, paging: PaginationDto) {
    const queryBuilder = this.certsRepository
      .createQueryBuilder('cert')
      .leftJoinAndSelect('cert.course', 'course')
      .leftJoinAndSelect('cert.user', 'user')
      .leftJoinAndSelect('cert.file', 'file')
      .where('course.id = :id', { id })
      .orderBy('cert.created');

    if (query.userEmail) {
      queryBuilder.andWhere('user.email like :email', {
        email: `%${query.userEmail}%`,
      });
    }

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    const [results, count] = await queryBuilder.getManyAndCount();
    return {
      data: results,
      meta: {
        total: count,
      },
    };
  }

  @Transactional()
  async findByWebinar(id: string, query?: QueryCertsDto, paging?: PaginationDto) {
    const queryBuilder = this.certsRepository
      .createQueryBuilder('cert')
      .leftJoinAndSelect('cert.webinar', 'webinar')
      .leftJoinAndSelect('cert.user', 'user')
      .leftJoinAndSelect('cert.file', 'file')
      .where('webinar.id = :id', { id })
      .orderBy('cert.created');

    if (query?.userEmail) {
      queryBuilder.andWhere('user.email like :email', {
        email: `%${query.userEmail}%`,
      });
    }

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    const [results, count] = await queryBuilder.getManyAndCount();
    return {
      data: results,
      meta: {
        total: count,
      },
    };
  }

  async findByUser(userId: string) {
    const qb = this.certsRepository
      .createQueryBuilder('cert')
      .leftJoinAndSelect('cert.course', 'course')
      .leftJoinAndSelect('cert.webinar', 'webinar')
      .leftJoinAndSelect('cert.file', 'file')
      .where('user_id=:userId', { userId })
      .orderBy('cert.created');
    return qb.getMany();
  }
}
