import { Source } from '@entities/source.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class FindSourceDto {
  sourceId: string;
}

@Injectable()
export class SourcesService {
  constructor(
    @InjectRepository(Source)
    private sourcesRepo: Repository<Source>,
  ) {}
  findSource(dto: FindSourceDto) {
    return this.sourcesRepo.findOne({ where: { externalId: dto.sourceId } });
  }
}
