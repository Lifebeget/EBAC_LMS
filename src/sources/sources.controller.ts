import { Permission } from '@enums/permission.enum';
import { Controller, Get, Query } from '@nestjs/common';
import { Interapp } from 'src/auth/interapp.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { FindSourceDto, SourcesService } from './sources.service';

@Controller('sources')
export class SourcesController {
  constructor(private sourcesService: SourcesService) {}

  @Get()
  @Interapp()
  @RequirePermissions(Permission.SOURCE_VIEW)
  findSource(@Query() dto: FindSourceDto) {
    return this.sourcesService.findSource(dto);
  }
}
