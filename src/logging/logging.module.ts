import { Module } from '@nestjs/common';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { SocketsModule } from 'src/sockets/sockets.module';
import { UsersModule } from 'src/users/users.module';
import { LoggingController } from './logging.controller';
import { CoursesModule } from 'src/courses/courses.module';

@Module({
  controllers: [LoggingController],
  imports: [AssessmentsModule, UsersModule, SocketsModule, CoursesModule],
})
export class LoggingModule {}
