import { ConflictException, Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { Public } from 'src/auth/public.decorator';
import { NoLog } from 'src/logger';
import * as ffs from 'fs';
import { Request, Response } from 'express';
import * as path from 'path';
import { InterappGuard } from 'src/auth/interapp.guard';

const fs = ffs.promises;

@Controller('')
@NoLog()
export class LoggingController {
  private isImportInProgress = false;

  @Public()
  @Get('log/file')
  @UseGuards(InterappGuard)
  async file(@Req() req: Request, @Res() res: Response) {
    if (this.isImportInProgress) {
      throw new ConflictException('Other instance is reading the file');
    }

    if (!ffs.existsSync(process.env.LOG_FILE) && !ffs.existsSync(process.env.LOG_FILE_POPULATION)) {
      throw new ConflictException('No logfile');
    }

    if (!ffs.existsSync(process.env.LOG_FILE_POPULATION)) {
      await fs.rename(process.env.LOG_FILE, process.env.LOG_FILE_POPULATION);
    }

    res.sendFile(path.resolve(process.env.LOG_FILE_POPULATION));
  }
}
