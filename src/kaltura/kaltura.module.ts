import { forwardRef, Module } from '@nestjs/common';
import { KalturaController } from './kaltura.controller';
import { KalturaService } from './kaltura.service';
import { SocketsModule } from 'src/sockets/sockets.module';
import { FilesModule } from '../files/files.module';
import { CoursesModule } from '../courses/courses.module';
@Module({
  imports: [forwardRef(() => SocketsModule), FilesModule, forwardRef(() => CoursesModule)],
  controllers: [KalturaController],
  providers: [KalturaService],
  exports: [KalturaService],
})
export class KalturaModule {}
