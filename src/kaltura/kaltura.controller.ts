import { Body, Delete, ForbiddenException, Patch, Post, Query } from '@nestjs/common';
import { Controller, Get, NotFoundException, Param, Req } from '@nestjs/common';
import { Request } from 'express';
import { ApiTags, ApiOperation, ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { KalturaMediaEntity } from 'src/lib/types/KalturaMediaEntity';
import { KalturaService } from './kaltura.service';
import { Public } from 'src/auth/public.decorator';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { KalturaUserType } from 'src/lib/types/KalturaUserType';
import { KalturaContextRole } from 'src/lib/types/KalturaContextRole';
import { KalturaCaptionAsset } from 'src/lib/types/KalturaCaptionAsset';
import { CoursesService } from '../courses/courses.service';
import { SetThumbnailDto } from './dto/create-thumbnail.dto';
import { CreateCaptionDto } from './dto/create-caption.dto';
import { UpdateCaptionDto } from './dto/update-caption.dto';
import { LecturesService } from 'src/courses/lectures.service';
import { Permission, SystemRole } from '@lib/types/enums';

@ApiTags('kaltura')
@ApiBearerAuth()
@Controller('kaltura')
export class KalturaController {
  constructor(
    private readonly kalturaService: KalturaService,
    private readonly coursesService: CoursesService,
    private readonly lecturesService: LecturesService,
    private readonly socketsGateway: SocketsGateway,
  ) {}

  @ApiOperation({ summary: 'Get media meta' })
  @ApiResponse({ description: 'Kaltura metadata', type: KalturaMediaEntity })
  @Get(':id/meta')
  async getMeta(@Param('id') id: string): Promise<KalturaMediaEntity> {
    try {
      return await this.kalturaService.getVideoMeta(id);
    } catch (e) {
      throw new NotFoundException(e.toString());
    }
  }

  @ApiOperation({ summary: 'Get subtitles' })
  @ApiResponse({ description: 'Kaltura subtitles' })
  @Get(':id/subs')
  async getSubs(@Param('id') id: string, @Query('lang') lang: string): Promise<KalturaCaptionAsset[]> {
    try {
      let subtitlesList = await this.kalturaService.getVideoSubtitlesList(id);
      if (lang) subtitlesList = subtitlesList.filter(subtitle => subtitle.languageCode === lang);

      if (!subtitlesList.length) throw new Error('Not found');

      const subs = subtitlesList.map(async sub => {
        sub.subs = await this.kalturaService.getVideoSubtitles(id, sub.id);
        return sub;
      });

      return Promise.all(subs);
    } catch (e) {
      throw new NotFoundException(e.toString());
    }
  }

  @ApiOperation({ summary: 'Get media meta' })
  @ApiResponse({ description: 'Kaltura browseandembed callback uri' })
  @Public()
  @Post('/callback/:sid')
  async callback(@Req() req: Request): Promise<void> {
    if (req.params?.sid) {
      const entry = { ...req.body, subs: [], ks: '' };

      const subsList = await this.kalturaService.getVideoSubtitlesList(entry.entry_id);

      if (subsList.length) {
        const subs = subsList.map(async sub => {
          sub.subs = await this.kalturaService.getVideoSubtitles(entry.entry_id, sub.id);
          return sub;
        });

        entry.subs = await Promise.all(subs);
      }

      entry.ks = this.kalturaService.getKSforEntryId(entry.entry_id);

      this.socketsGateway.sendKalturaCallback(req.params.sid, entry);
    }
  }

  @ApiOperation({ summary: 'Get embedded url' })
  @ApiResponse({ description: 'Kaltura browseandembed url' })
  @Get('/browseandembed')
  async embed(
    @Query('courseId') courseId: string,
    @Query('socketId') socketId: string,
    @Query('embedUUID') embedUUID: string,
  ): Promise<string> {
    const course = await this.coursesService.findOne(courseId, {
      includeHidden: true,
    });
    if (!course) throw new NotFoundException('ERRORS.NOT_FOUND');
    const category = await this.kalturaService.findCategoryByCourse(course);
    return this.kalturaService.generateBrowseAndEmbed(
      `${socketId}:${embedUUID}`,
      category,
      KalturaUserType.TYPE_ADMIN,
      KalturaContextRole.CONTEXT_ROLE_MANAGER,
    );
  }

  @ApiOperation({ summary: 'Get media gallery url' })
  @ApiResponse({ description: 'Kaltura media gallery url' })
  @Get('/mediagallery')
  async embedMediaGallery(@Query('courseId') courseId: string): Promise<string> {
    const course = await this.coursesService.findOne(courseId, {
      includeHidden: true,
    });
    if (!course) throw new NotFoundException('ERRORS.NOT_FOUND');
    const category = await this.kalturaService.findCategoryByCourse(course);
    return this.kalturaService.generateMediaGallery(category, KalturaUserType.TYPE_ADMIN, KalturaContextRole.CONTEXT_ROLE_MANAGER);
  }

  @ApiOperation({ summary: 'Set thumbnails to kaltura videos' })
  @ApiResponse({ description: 'Generated thumbnails' })
  @Post('setThumbnails')
  async setThumbnails(@Body() setThumbnailsDto: SetThumbnailDto | SetThumbnailDto[]) {
    return this.kalturaService.setThumbnails(setThumbnailsDto);
  }

  @ApiOperation({ summary: 'Get caption content url' })
  @ApiResponse({ description: 'Caption content url' })
  @Get('captions/:assetId/url')
  captionAssetUrl(@Param('assetId') captionAssetId: string) {
    return this.kalturaService.captionAssetUrl(captionAssetId);
  }

  @ApiOperation({ summary: 'Upload caption to Kaltura video' })
  @ApiResponse({ description: 'Created caption object' })
  @Post('entries/:entryId/captions')
  uploadCaption(@Body() uploadCaptionDto: CreateCaptionDto, @Param('entryId') entryId: string) {
    return this.kalturaService.uploadCaption(entryId, uploadCaptionDto);
  }

  @ApiOperation({ summary: 'Update caption from Kaltura video' })
  @ApiResponse({ description: 'Updated caption object' })
  @Patch('captions/:assetId')
  async updateCaption(@Body() updateCaptionDto: UpdateCaptionDto, @Param('assetId') assetId: string) {
    return await this.kalturaService.updateCaption(assetId, updateCaptionDto);
  }

  @ApiOperation({ summary: 'Remove caption from Kaltura video' })
  @ApiResponse({ description: 'Removed caption object' })
  @Delete('captions/:assetId')
  removeCaption(@Param('assetId') assetId: string) {
    return this.kalturaService.removeCaption(assetId);
  }

  @ApiOperation({ summary: 'Set caption as default' })
  @ApiResponse({ description: 'Updated caption object' })
  @Patch('captions/:assetId/set-as-default')
  setCaptionAsDefault(@Param('assetId') assetId: string) {
    return this.kalturaService.setCaptionAsDefault(assetId);
  }

  @ApiOperation({ summary: 'Get caption file content' })
  @ApiResponse({ description: 'Caption text' })
  @Get('captions/:assetId/file-content')
  getCaptionFileContent(@Param('assetId') assetId: string) {
    return this.kalturaService.getCaptionFileContent(assetId);
  }

  @ApiOperation({ summary: 'Generate kaltura embed for video upload' })
  @Get('/student-upload')
  async embedStudentUploadVideo(
    @Query('lectureId') lectureId: string,
    @Query('socketId') socketId: string,
    @Query('embedUUID') embedUUID: string,
    @Req() req: Express.Request,
  ): Promise<string> {
    const lecture = await this.lecturesService.findOne(lectureId);
    if (!lecture) throw new NotFoundException('Lecture not found.');

    if (!(req.user.permissions.includes(Permission.COURSE_VIEW) || req.user.coursePermissions[lecture.courseId].canSubmitAssignment)) {
      throw new ForbiddenException("You can't upload the video.");
    }

    const category = `Assessments / ${lecture.course.slug} / ${lecture.module.number}.${lecture.number} ${lecture.title}`;

    return this.kalturaService.generateBrowseAndEmbed(
      `${socketId}:${embedUUID}`,
      category,
      KalturaUserType.TYPE_USER,
      KalturaContextRole.CONTEXT_ROLE_MANAGER,
      req.user.id,
    );
  }

  @ApiOperation({ summary: 'Generate kaltura session for video preview' })
  @Get('/student-preview')
  async embedStudentPreviewVideo(@Query('entryId') entryId: string, @Req() req: Express.Request): Promise<string> {
    //CHECK FOR ADMIN\MANAGER PERMISSION
    const accessRequiredByPermission =
      req.user.parsedRoles.includes(SystemRole.Admin) ||
      req.user.parsedRoles.includes(SystemRole.Superadmin) ||
      req.user.parsedRoles.includes(SystemRole.Observer) ||
      req.user.parsedRoles.includes(SystemRole.Manager) ||
      req.user.parsedRoles.includes(SystemRole.Support);

    //CHECK FOR OWNER PERMISSION
    const videoMeta = await this.kalturaService.getVideoMeta(entryId);
    const videoOwn = req.user.id === videoMeta.userId;

    //DISABLE NOT LMS USERS(LECTURE'S VIDEOS)
    const uploadByManager = videoMeta.userId === 'manager';

    if (!accessRequiredByPermission && (!videoOwn || uploadByManager)) {
      throw new ForbiddenException("You can't watch the video.");
    }

    return await this.kalturaService.getKSforEntryId(entryId);
  }
}
