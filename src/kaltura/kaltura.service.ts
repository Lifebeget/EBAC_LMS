/* eslint-disable @typescript-eslint/ban-ts-comment */
import * as querystring from 'querystring';
import * as crypto from 'crypto';
import { NodeList, parseSync } from 'subtitle';
import { Injectable, NotFoundException } from '@nestjs/common';
import axios from 'axios';
import { KalturaMediaEntity } from 'src/lib/types/KalturaMediaEntity';
import { KalturaContextRole } from 'src/lib/types/KalturaContextRole';
import { KalturaUserType } from 'src/lib/types/KalturaUserType';
import { KalturaCaptionAsset } from '../lib/types/KalturaCaptionAsset';
import { KalturaCategory } from 'src/lib/types/KalturaCategory';
import { FilesService } from '../files/files.service';
import { Course } from '../lib/entities/course.entity';
import { getFileInfo } from 'src/helpers';
import { SetThumbnailDto } from './dto/create-thumbnail.dto';
import { CreateCaptionDto } from './dto/create-caption.dto';
import * as kaltura from 'kaltura-client';
import { UpdateCaptionDto } from './dto/update-caption.dto';
import { normalizeCaptionText } from '@lib/helpers/subtitles';

const formData = require('form-data');

@Injectable()
export class KalturaService {
  private secret = process.env.KALTURA_SECRET;
  private partnerId = process.env.KALTURA_PARTNER_ID;
  private cbUrl = process.env.KALTURA_CALLBACK_URL;
  private kafUrl = process.env.KALTURA_KAF_URL;
  private readonly client: any;

  private headers = {
    'Content-Type': 'application/json',
  };

  constructor(
    private fileService: FilesService, // private lecturesService: LecturesService,
  ) {
    if (!process.env.KALTURA_SECRET) {
      throw new Error('KALTURA_SECRET is empty');
    }
    if (!process.env.KALTURA_PARTNER_ID) {
      throw new Error('KALTURA_PARTNER_ID is empty');
    }
    if (!process.env.KALTURA_CALLBACK_URL) {
      throw new Error('KALTURA_CALLBACK_URL is empty');
    }
    if (!process.env.KALTURA_KAF_URL) {
      throw new Error('KALTURA_KAF_URL is empty');
    }
    const config = new kaltura.Configuration();
    this.client = new kaltura.Client(config);
  }

  async getVideoSubtitlesList(id: string): Promise<KalturaCaptionAsset[]> {
    const url = 'https://www.kaltura.com/api_v3/service/caption_captionasset/action/list';
    const data = {
      format: 1,
      ks: this.getKSforEntryId(id),
      filter: {
        entryIdIn: id,
        objectType: 'KalturaAssetFilter',
      },
    };

    const response = await axios.post(url, data, { headers: this.headers });
    if (response.data.code && response.data.message) {
      const err = response.data.code + ': ' + response.data.message;
      throw new Error(err);
    }

    return response.data?.objects || [];
  }

  async getVideoSubtitles(entryId: string, id: string): Promise<NodeList> {
    const url = 'https://www.kaltura.com/api_v3/service/caption_captionasset/action/serve';
    const data = {
      format: 1,
      ks: this.getKSforEntryId(entryId),
      captionAssetId: id,
    };

    const response = await axios.post(url, data, { headers: this.headers });
    if (response.data.code && response.data.message) {
      const err = response.data.code + ': ' + response.data.message;
      throw new Error(err);
    }

    let subs: NodeList;
    try {
      const subtitlesText = normalizeCaptionText(response.data);
      subs = parseSync(subtitlesText);
    } catch (e) {
      console.error(`Error parsing subtitles for entry: ${entryId}`);
      subs = [];
    }
    return subs;
  }

  async getVideoMeta(id: string): Promise<KalturaMediaEntity> {
    const url = 'http://www.kaltura.com/api_v3/index.php';
    const params = {
      format: 1,
      service: 'media',
      action: 'get',
      entryId: id,
      ks: this.getKSforEntryId(id),
    };

    const response = await axios.get(url, { params });
    if (response.data.code && response.data.message) {
      const err = response.data.code + ': ' + response.data.message;
      throw new Error(err);
    }
    return response.data;
  }

  async getThumbFile(id: string, timecode: number) {
    const meta = await this.getVideoMeta(id);
    const parts = [
      `https://cdnapisec.kaltura.com`,
      `p/${meta.partnerId}`,
      `thumbnail`,
      `entry_id/${meta.id}`,
      `width/${meta.width}`,
      `quality/90`,
      `vid_sec/${timecode}`,
    ];
    const link = parts.join('/');
    const info = await getFileInfo(link);
    const file = {
      ...info,
      filename: id,
      extension: 'mp4',
      title: meta.name,
      url: link,
    };
    return file;
  }

  generateBrowseAndEmbed(socketId: string, courseId: string, type: KalturaUserType, role: KalturaContextRole, userId = 'manager') {
    const cbUrl = encodeURIComponent(`${this.cbUrl}/${socketId}`);
    const ks = this.generateKS({
      role: 'adminRole',
      actionslimit: 999,
      contextId: courseId,
      userContextualRole: role,
      returnUrl: cbUrl,
      _t: type,
      _u: userId,
    });

    //const cats = `catid/${courseId}/embedTargetCategoryId/${courseId}`;

    return `${this.kafUrl}/browseandembed/index/browseandembed/ks/${ks}`;
  }

  generateMediaGallery(courseId: string, type: KalturaUserType, role: KalturaContextRole, userId = 'manager') {
    const ks = this.generateKS({
      role: 'adminRole',
      actionslimit: 999,
      contextId: courseId,
      userContextualRole: role,
      _t: type,
      _u: userId,
    });
    return `${this.kafUrl}/hosted/index/course-gallery/ks/${ks}`;
  }

  //Method generate kaltura service session
  generateKS(data: {
    _u: string; //User id
    _t: KalturaUserType; //kaltura user type - default member/admin
    sview?: string;
    setrole?: string;
    disableentitlement?: string;
    disableentitlementforentry?: string;
    userId?: string;
    role?: string;
    actionslimit?: number;
    contextId?: string;
    userContextualRole?: KalturaContextRole;
    returnUrl?: string;
    catId?: string;
    format?: number;
  }) {
    const rnadomSize = 16;
    const expiry = 3600;
    const fields = {
      ...data,
      _e: Math.floor(new Date().getTime() / 1000) + expiry,
    };

    const fieldsStr = querystring.stringify(fields);
    let fieldBuf = Buffer.concat([crypto.randomBytes(rnadomSize), Buffer.from(fieldsStr, 'latin1')]);

    // Prepending SHA1 sum
    const shasum = crypto.createHash('sha1').update(fieldBuf).digest();
    fieldBuf = Buffer.concat([shasum, fieldBuf]);

    // SHA1 of admin_secret
    const shakey = crypto.createHash('sha1').update(this.secret).digest().slice(0, 16);

    // IV is just 16 zero bytes
    const iv = '\0'.repeat(16);

    // Kaltura uses AES-128-CBC with zero bytes padding, so we need to pad it manually
    let paddingLength = 16 - (fieldBuf.length % 16);
    if (paddingLength == 16) {
      paddingLength = 0;
    }
    const paddedFieldsBuf = Buffer.concat([fieldBuf, Buffer.from('\0'.repeat(paddingLength), 'binary')]);

    // AES-128-CBC Encryption
    const cipher = crypto.createCipheriv('aes-128-cbc', shakey, iv).setAutoPadding(false);
    // @ts-ignore TODO: need description
    let crypt = cipher.update(paddedFieldsBuf, 'latin1', 'binary');
    // @ts-ignore TODO: need description
    crypt += cipher.final('binary');

    // Prepending with version and partner_id separated by pipes
    const decodedKs = `v2|${this.partnerId}|${crypt}`;

    // Converting to Base64 with [+] and [/] replaced by [-] and [_]
    const ks = Buffer.from(decodedKs, 'latin1').toString('base64').replace(/\+/g, '-').replace(/\//g, '_');

    return ks;
  }

  getAdminKS() {
    return this.generateKS({
      role: 'adminRole',
      actionslimit: 999,
      disableentitlement: '',
      userContextualRole: KalturaContextRole.CONTEXT_ROLE_MANAGER,
      _t: KalturaUserType.TYPE_ADMIN,
      _u: 'manager',
    });
  }

  async findCategoryByCourse(course: Course) {
    const ks = this.getAdminKS();
    const url = 'https://www.kaltura.com/api_v3/service/category/action/list';
    const data = { format: 1, ks };

    const response = await axios.post(url, data, { headers: this.headers });
    if (response.data.code && response.data.message) {
      const err = response.data.code + ': ' + response.data.message;
      throw new Error(err);
    }

    const anchor = `[${course.slug}]`;
    const cats: KalturaCategory[] = response.data?.objects || [];
    const category = cats.filter(cat => !cat.name.includes('InContext')).find(cat => cat.name.includes(anchor));
    if (!category) {
      const fullName = `${course.title} [${course.slug}]`;
      if (fullName.length <= 60) return fullName;
      return anchor;
    }
    return category.name;
  }

  getKSforEntryId(id: string, userId = 'manager') {
    const fields = {
      sview: id,
      setrole: 'PLAYBACK_BASE_ROLE',
      disableentitlementforentry: id,
      actionslimit: 999,
      _t: KalturaUserType.TYPE_USER,
      _u: userId,
      role: 'viewerRole',
    };
    return this.generateKS(fields);
  }

  async setThumbnails(dto: SetThumbnailDto | SetThumbnailDto[]) {
    const thumbnails = Array.isArray(dto) ? dto : [dto];
    const ks = this.getAdminKS();

    await Promise.all(
      thumbnails.map(thumbnail => {
        return new Promise(async resolve => {
          const form = new formData();
          const responseImage = await axios.get(thumbnail.url, {
            responseType: 'stream',
          });
          form.append('fileData', responseImage.data);
          form.append('entryId', thumbnail.entryId);
          form.append('ks', ks);
          form.append('format', 1);
          form.append('clientTag', 'kmcng');

          const addThumbEndpoint = 'https://www.kaltura.com/api_v3/service/thumbasset/action/addFromImage';
          form.submit(addThumbEndpoint, async (err, res) => {
            let data = '';
            res.on('data', chunk => {
              data += chunk;
            });
            res.on('end', async () => {
              const setAsDefaultEndpoint = 'https://www.kaltura.com/api_v3/service/thumbasset/action/setAsDefault';
              const parsedData: { id: string } = JSON.parse(data);
              await axios.post(
                setAsDefaultEndpoint,
                {},
                {
                  params: {
                    ks,
                    thumbAssetId: parsedData.id,
                  },
                },
              );
            });
            res.resume();
            resolve(res);
          });
        });
      }),
    );
  }

  setAdminKS() {
    const ks = this.getAdminKS();
    this.client.setKs(ks);
  }

  async captionAssetUrl(captionAssetId: string) {
    this.setAdminKS();
    return kaltura.services.captionAsset.getUrl(captionAssetId).execute(this.client);
  }

  async uploadCaption(entryId: string, uploadCaptionDto: CreateCaptionDto) {
    this.setAdminKS();
    const uploadToken = new kaltura.objects.UploadToken();
    const buffer = Buffer.from(uploadCaptionDto.file, 'utf8');

    const uploadTokenResult = await kaltura.services.uploadToken.add(uploadToken).execute(this.client);

    const resume = false;
    const finalChunk = true;
    const resumeAt = -1;

    await kaltura.services.uploadToken
      .upload(uploadTokenResult.id, buffer, resume, finalChunk, resumeAt)
      .completion((success, entry) => {
        if (!success) {
          console.error(entry, 'UploadCaptionError');
        }
      })
      .execute(this.client);

    const captionAsset = new kaltura.objects.CaptionAsset();
    captionAsset.accuracy = uploadCaptionDto.accuracy;
    captionAsset.label = uploadCaptionDto.label;
    captionAsset.displayOnPlayer = uploadCaptionDto.displayOnPlayer;
    captionAsset.language = uploadCaptionDto.language;
    captionAsset.format = uploadCaptionDto.format;

    const addCaptionAssetResult = await kaltura.services.captionAsset.add(entryId, captionAsset).execute(this.client);

    const contentResource = new kaltura.objects.UploadedFileTokenResource();
    contentResource.token = uploadTokenResult.id;

    await kaltura.services.captionAsset.setContent(addCaptionAssetResult.id, contentResource).execute(this.client);

    if (uploadCaptionDto.isDefault) {
      await this.setCaptionAsDefault(addCaptionAssetResult.id);
    }
  }

  async updateCaption(captionAssetId: string, dto: UpdateCaptionDto) {
    this.setAdminKS();

    // Если включается отображение субтитра в плеере, то отключаем отображение
    // в плеере субитров того же языка (если у них displayOnPlayer === true)
    const promises = [];
    if (dto.displayOnPlayer) {
      // Данные субтитра, у которого включается displayOnPlayer
      const updatedCaption: KalturaCaptionAsset = await kaltura.services.captionAsset.get(captionAssetId).execute(this.client);

      if (!updatedCaption) {
        throw new NotFoundException(`Caption with ID ${captionAssetId} not found`);
      }
      const captionsList = await this.getVideoSubtitlesList(updatedCaption.entryId);
      if (captionsList) {
        // Отображаемые в плеере субтитры того же языка,
        // что и редактируемый субтитр
        const captionsWithSameLang = captionsList.filter(caption => {
          return caption.languageCode === updatedCaption.languageCode && caption.id !== updatedCaption.id && caption.displayOnPlayer;
        });
        for (const caption of captionsWithSameLang) {
          const captionDto = {
            displayOnPlayer: false,
            isDefault: false,
          };
          const promise = kaltura.services.captionAsset.update(caption.id, captionDto).execute(this.client);

          promises.push(promise);
        }
      }
    }
    const updateCurrentCaptionPromise = kaltura.services.captionAsset.update(captionAssetId, dto).execute(this.client);

    return await Promise.all([...promises, updateCurrentCaptionPromise]);
  }

  async removeCaption(captionAssetId: string) {
    this.setAdminKS();
    return kaltura.services.captionAsset.deleteAction(captionAssetId).execute(this.client);
  }

  async setCaptionAsDefault(captionAssetId: string) {
    this.setAdminKS();
    return kaltura.services.captionAsset.setAsDefault(captionAssetId).execute(this.client);
  }

  async getCaptionFileContent(captionAssetId: string) {
    const url = await this.captionAssetUrl(captionAssetId);
    const { data: file } = await axios.get(url, {
      responseType: 'blob',
    });
    return file;
  }
}
