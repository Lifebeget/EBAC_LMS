import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SetThumbnailDto {
  @ApiProperty({ description: 'Kaltura video entryId' })
  @IsNotEmpty()
  entryId: string;

  @ApiProperty({ description: 'Thumbnail image url' })
  @IsNotEmpty()
  url: string;
}
