import { PartialType, OmitType } from '@nestjs/swagger';
import { CreateCaptionDto } from './create-caption.dto';

export class UpdateCaptionDto extends PartialType(OmitType(CreateCaptionDto, ['file', 'format'] as const)) {}
