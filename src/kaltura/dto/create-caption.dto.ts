import { ApiProperty } from '@nestjs/swagger';

import { IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsString, ValidateIf } from 'class-validator';
import { SubtitlesTypesEnum } from '@lib/types/enums/subtitles-types.enum';

export class CreateCaptionDto {
  @ApiProperty({ description: 'Caption label' })
  @IsNotEmpty()
  @IsString()
  label: string;

  @ApiProperty({ description: 'Caption accuracy' })
  @IsNotEmpty()
  @IsNumber()
  accuracy: number;

  @ApiProperty({ description: 'Display caption on player' })
  @ValidateIf(o => o.hasOwnProperty('displayOnPlayer'))
  @IsBoolean()
  displayOnPlayer?: boolean;

  @ApiProperty({ description: 'Caption file format' })
  @IsNotEmpty()
  @IsEnum(SubtitlesTypesEnum)
  format: SubtitlesTypesEnum;

  @ApiProperty({ description: 'Caption language' })
  @IsNotEmpty()
  @IsString()
  language: string;

  @ApiProperty({ description: 'Caption is default' })
  @ValidateIf(o => o.hasOwnProperty('isDefault'))
  @IsBoolean()
  isDefault?: boolean;

  @ApiProperty({ description: 'Caption file content' })
  @IsNotEmpty()
  @IsString()
  file: string;
}
