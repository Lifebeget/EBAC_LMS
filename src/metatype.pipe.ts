import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

export const metatypeSymb = Symbol('metatypeInfo');

@Injectable()
export class MetatypePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (typeof value === 'object') {
      Reflect.defineMetadata(metatypeSymb, metadata.metatype, value);
    }
    return value;
  }
}
