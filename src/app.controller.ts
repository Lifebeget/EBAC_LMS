import { Controller, Get, Module, Post, Req } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiProperty, ApiBearerAuth } from '@nestjs/swagger';
import { UsersModule } from './users/users.module';
import { UsersService } from './users/users.service';
import { Public } from './auth/public.decorator';
import { RequirePermissions } from './require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { Request } from 'express';
import config from 'config';

class Status {
  @ApiProperty({ description: 'API status' })
  api?: string;

  @ApiProperty({ description: 'DB status' })
  db?: string;

  @ApiProperty({ description: 'Server time' })
  serverTime?: Date;

  @ApiProperty({ description: 'DB time' })
  dbTime?: Date;

  @ApiProperty({
    description: 'Commit version (only available in CI environment)',
  })
  commit?: string;
}

@Module({ imports: [UsersModule] })
@ApiBearerAuth()
@Controller()
export class AppController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Get('status')
  @ApiOperation({ summary: 'Show current status' })
  @ApiResponse({
    status: 200,
    type: Status,
  })
  @ApiTags('public')
  async status() {
    const status = new Status();
    status.api = 'OK';
    let dbTime;
    try {
      dbTime = await this.usersService.status();
      status.db = 'OK';
    } catch (err) {
      status.db = 'ERROR';
      console.dir(err);
    }
    status.dbTime = dbTime[0].now;
    status.serverTime = new Date();
    status.commit = process.env.COMMIT ?? '';
    return status;
  }

  @Public()
  @Get('healthcheck')
  @ApiOperation({ summary: 'Returns OK if app is running' })
  @ApiResponse({
    status: 200,
    type: Status,
  })
  @ApiTags('public')
  async healthcheck() {
    return 'OK';
  }

  @Post('panic')
  @ApiOperation({ summary: 'Exits the application. For crash restart test' })
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  panic() {
    console.error('== MANUAL PANIC CRASH INVOKED ==');
    process.exit(1);
  }

  @Public()
  @ApiTags('public')
  @Get('env')
  @ApiOperation({ summary: 'Show environment variables on server' })
  @ApiResponse({
    status: 200,
    type: Status,
  })
  async env(@Req() req: Request) {
    const defaultEnv = {
      ADMIN_LMS_URL: process.env.ADMIN_LMS_URL,
      TUTOR_LMS_URL: process.env.TUTOR_LMS_URL,
      STUDENT_LMS_URL: process.env.STUDENT_LMS_URL,
      TOKEN_NAME: process.env.TOKEN_NAME,
      COOKIE_DOMAIN: process.env.COOKIE_DOMAIN,
    };
    const domainEnv = config.setUser(req.user).get();
    return Object.assign(defaultEnv, domainEnv);
  }
}
