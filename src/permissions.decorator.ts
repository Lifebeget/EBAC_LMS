import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const Permissions = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  return request.user?.permissions || [];
});
