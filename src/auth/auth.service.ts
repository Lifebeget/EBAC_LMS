/* eslint-disable @typescript-eslint/no-unused-vars */
import { BadRequestException, forwardRef, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { generatePasswordHash } from '../helpers';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/lib/entities/user.entity';
import { UpdateUserDto } from 'src/users/dto/update-user.dto';
import { LoginLinkQueryDto } from './dto/login-link-query.dto';
import axios from 'axios';
import config from 'config';

export type Payload = {
  email: string;
  sub: string;
  backUrl?: string;
  successUrl?: string;
  linkExpiredUrl?: string;
  errorUrl?: string;
  iat: number;
  exp: number;
};

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass?: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new UnauthorizedException('ERRORS.LOGIN_CREDENTIALS_NOT_FOUND');
    }
    if (!user.active) {
      throw new UnauthorizedException('ERRORS.USER_IS_INACTIVE');
    }
    if (pass && user.password === generatePasswordHash(pass)) {
      const { password, ...result } = user;
      return result;
    }

    throw new UnauthorizedException('ERRORS.LOGIN_CREDENTIALS_NOT_FOUND');
  }

  async verifyUser(token: string, onExpired?: (payload: Payload) => void): Promise<Payload> {
    let payload: Payload;
    try {
      if (onExpired) {
        payload = this.jwtService.decode(token) as Payload;
      }
      payload = await this.jwtService.verify(token, {});
    } catch (err) {
      if (err.name == 'TokenExpiredError') {
        if (onExpired) {
          onExpired(payload);
        }
        throw new UnauthorizedException('ERRORS.TOKEN_EXPIRED');
      } else {
        throw new BadRequestException('ERRORS.INVALID_TOKEN');
      }
    }
    return payload;
  }

  tokenInfo(token: string): { payload?: Payload; verify?: Payload; error?: any } {
    let payload: Payload;
    try {
      payload = this.jwtService.decode(token) as Payload;
    } catch (err) {
      return {
        error: err,
      };
    }
    let verify: Payload;
    try {
      verify = this.jwtService.verify(token, {});
    } catch (err) {
      verify = err;
    }
    return {
      payload,
      verify,
    };
  }

  async login(user: any) {
    const payload = { email: user.email, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  expiresDateStringToSeconds(expires: Date): number {
    return Math.round((new Date(expires).getTime() - new Date().getTime()) / 1000);
  }

  async loginLink(user: any, query: LoginLinkQueryDto) {
    const { expires, ...payload } = query;

    payload['sub'] = user.id;

    let expiresIn = '1d';
    if (expires) {
      expiresIn = this.expiresDateStringToSeconds(expires) + 's';
    }

    return {
      url: config.setUser(user).get('API_URL') + '/auth/link/' + this.jwtService.sign(payload, { expiresIn }),
    };
  }

  async changePassword(id: string, password: string): Promise<Partial<User>> {
    const dto = new UpdateUserDto();
    dto.password = password;
    return await this.usersService.update(id, dto);
  }
}
