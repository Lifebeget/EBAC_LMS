import { forwardRef, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { InterappStrategy } from './interapp.strategy';
import { AuthController } from './auth.controller';
import { MailModule } from 'src/mail/mail.module';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { TriggerModule } from 'src/trigger/trigger.module';

@Module({
  imports: [
    forwardRef(() => UsersModule),
    forwardRef(() => TriggerModule),
    MailModule,
    PassportModule,
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.JWT_TOKEN_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRATION_TIME,
        },
      }),
    }),
    forwardRef(() => AssessmentsModule),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy, InterappStrategy],
  exports: [AuthService, JwtModule],
})
export class AuthModule {}
