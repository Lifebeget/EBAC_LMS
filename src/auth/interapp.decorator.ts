import { SetMetadata } from '@nestjs/common';

export const IS_INTERAPP_KEY = 'isInterApp';
export const Interapp = () => SetMetadata(IS_INTERAPP_KEY, true);
