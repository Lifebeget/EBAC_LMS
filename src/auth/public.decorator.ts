import { SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_KEY = 'isPublic';
export const IS_PUBLIC_IGNORE_TOKEN_KEY = 'isPublicIgnoreToken';
export const Public = (ignoreToken = false) => SetMetadata(ignoreToken ? IS_PUBLIC_IGNORE_TOKEN_KEY : IS_PUBLIC_KEY, true);
