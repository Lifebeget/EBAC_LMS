import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  BadRequestException,
  UseGuards,
  NotFoundException,
  InternalServerErrorException,
  Res,
  Req,
  HttpStatus,
  ValidationPipe,
  UsePipes,
  ConflictException,
  UnauthorizedException,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiOperation, ApiResponse, ApiProperty, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import * as dayjs from 'dayjs';
import { User } from 'src/lib/entities/user.entity';
import { TokensService } from 'src/users/tokens.service';
import { UsersService } from 'src/users/users.service';
import { AuthService, Payload } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { Public } from './public.decorator';
import { PlatformType } from '../lib/enums/platform-type.enum';
import { InterappGuard } from 'src/auth/interapp.guard';
import { Request, Response } from 'express';
import { OpenRegisterUserDto } from './dto/open-register-user.dto';
import * as assert from 'assert';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ProfileService } from 'src/users/profile.service';
import { CreateProfileDto } from 'src/users/dto/create-profile.dto';
import { LoginLinkQueryDto } from './dto/login-link-query.dto';
import { PayloadTokenDto } from './dto/payload-token.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { mapPermissionsForRoles, Permission } from '@enums/permission.enum';
import { AuthTriggerService } from 'src/trigger/services/auth-trigger.service';
import { mapCoursePermissions } from 'src/helpers/enrollments';
import config from 'config';
import { MultiDomainsEnum } from '@lib/types/enums';

class AuthBody {
  @ApiProperty({ required: true, description: 'email' })
  email: string;

  @ApiProperty({ required: true, description: 'password' })
  password: string;
}

class ResetPasswordBody {
  @ApiProperty({ required: true, description: 'token' })
  token: string;

  @ApiProperty({ required: true, description: 'password' })
  password: string;
}

class ChangePassBody {
  @ApiProperty({ required: true })
  currentPassword: string;

  @ApiProperty({ required: true })
  newPassword: string;
}

class ForgotPassBody {
  @ApiProperty({ required: true })
  email: string;

  @ApiProperty({
    enum: PlatformType,
    description: 'Password reset link differs for student and tutor platforms',
  })
  platform: PlatformType;
}
class ResponseToken {
  @ApiProperty({ description: 'Auth token' })
  access_token: string;
}

class ReturnedException {
  @ApiProperty({ description: 'Error code' })
  statusCode: 400;

  @ApiProperty({
    description: 'Error message',
    enum: ['ERRORS.INVALID_TOKEN', 'ERRORS.TOKEN_EXPIRED', 'ERRORS.USER_NOT_FOUND', 'ERRORS.USER_IS_INACTIVE', 'ERRORS.TOKEN_ALREADY_USED'],
  })
  message: string;

  @ApiProperty({ description: 'Generic error message' })
  error: string;
}

class ReturnedLink {
  @ApiProperty({ description: 'Link for user authorization' })
  url: string;
}

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private usersService: UsersService,
    private profileService: ProfileService,
    private tokenService: TokensService,
    private authService: AuthService,
    private authTriggerService: AuthTriggerService,
  ) {
    assert(process.env.TOKEN_NAME, 'TOKEN_NAME env is not defined');
    assert(config.get('COOKIE_DOMAIN'), 'COOKIE_DOMAIN env is not defined');
  }

  cookieDomain() {
    return config.get('COOKIE_DOMAIN') === 'localhost' ? 'localhost' : '.' + config.get('COOKIE_DOMAIN');
  }

  @Public(true)
  @ApiOperation({ summary: 'Authorize' })
  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Req() req, @Body() body: AuthBody, @Res({ passthrough: true }) res: Response) {
    const loginData = await this.authService.login(req.user);

    res.cookie(process.env.TOKEN_NAME, loginData.access_token, {
      domain: this.cookieDomain(),
      path: '/',
      httpOnly: false,
    });

    // Cancel all "course not started" triggers for the user
    // Do this in background, no "await" needed
    this.authTriggerService.userLogin(req.user);
    // TODO disable after the completion of the reactivation program or
    // decide whether we can use it somehow later
    this.authTriggerService.reactivateMoveLead(req.user);
    return loginData;
  }

  @ApiOperation({ summary: 'Unauthorize' })
  @Public()
  @Post('logout')
  async logout(@Req() req, @Res({ passthrough: true }) res: Response) {
    res.clearCookie(process.env.TOKEN_NAME, {
      domain: this.cookieDomain(),
      path: '/',
    });
    return { seeYouLater: true };
  }

  @ApiOperation({ summary: 'Impersonate' })
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE) // Currently for admin only
  @Post('impersonate')
  async impersonate(@Req() req, @Body() payloadUser: PayloadTokenDto, @Res({ passthrough: true }) res: Response) {
    const differenceGrade = await this.usersService.compareGradeUsers(req.user, await this.usersService.findOne(payloadUser.id));

    if (differenceGrade <= 0 && req.user.id != payloadUser.id) {
      throw new UnauthorizedException('ERRORS.LOW_GRADE');
    }

    const loginData = await this.authService.login(payloadUser);
    res.cookie(process.env.TOKEN_NAME, loginData.access_token, {
      domain: this.cookieDomain(),
      path: '/',
      httpOnly: false,
    });

    return loginData;
  }

  @ApiOperation({ summary: 'Current user info' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiResponse({
    status: 200,
    type: User,
  })
  @ApiQuery({
    name: 'includeExpiredEnrollments',
    enum: ['true', 'false'],
    description: 'Include expired enrollments',
    required: false,
  })
  @Get('info')
  async getProfile(
    @Req() req,
    @Query('includeExpiredEnrollments')
    includeExpiredEnrollments?: boolean,
  ) {
    const user = await this.usersService.findOne(req.user.id, includeExpiredEnrollments);
    const rolesArray = user.roles.map(el => el.role);
    const permissions = mapPermissionsForRoles(rolesArray);
    const coursePermissions = mapCoursePermissions(user.enrollments);
    return { ...user, permissions, coursePermissions };
  }

  @ApiOperation({
    summary: 'Change password of current user. Required current password.',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiResponse({ status: 201 })
  @Post('change-password')
  async changePassword(@Req() req, @Body() body: ChangePassBody) {
    const verifiedUser = await this.authService.validateUser(req.user?.email, body.currentPassword);
    if (!verifiedUser) throw new NotFoundException('ERRORS.VERIFICATION_FAILED');
    const dbUser = await this.authService.changePassword(verifiedUser.id, body.newPassword);
    if (!dbUser) throw new NotFoundException('ERRORS.CHANGE_PASSWORD_PROBLEM');
    return;
  }

  @Public()
  @ApiOperation({
    summary: 'Forgot password. Sends token to email, for password recovery.',
  })
  @ApiResponse({
    status: 201,
    description: 'OK. Password reset link is emailed to the email',
  })
  @ApiResponse({
    status: 404,
    description: 'User not found',
  })
  @Post('forgot-password')
  async forgotPassword(@Body() body: ForgotPassBody) {
    const user = await this.usersService.findOneByEmail(body.email);
    if (!user) {
      throw new NotFoundException('ERRORS.USER_NOT_FOUND');
    }

    const token = await this.tokenService.create(user.id);
    if (!token) {
      throw new InternalServerErrorException('ERRORS.TOKEN_GENERATION_PROBLEM');
    }

    await this.authTriggerService.forgotPassword(user, token, body.platform || PlatformType.tutor);

    return;
  }

  @Public()
  @ApiOperation({
    summary: 'Reset password by token. Requires valid token, generated with forgot-password.',
  })
  @ApiResponse({
    status: 201,
    description: 'OK. New password is set',
  })
  @ApiResponse({
    status: 404,
    description: 'Token not found',
  })
  @ApiResponse({
    status: 400,
    description: 'Token expired or used. See message for details',
    type: ReturnedException,
  })
  @Post('reset-password')
  async resetPassword(@Body() body: ResetPasswordBody) {
    const dbToken = await this.tokenService.findOneByToken(body.token);
    if (!dbToken) throw new NotFoundException('ERRORS.TOKEN_NOT_FOUND');
    if (dbToken.used) throw new BadRequestException('ERRORS.TOKEN_ALREADY_USED');

    const ttl = parseInt(process.env.TOKEN_TTL) || 60;

    if (dayjs().diff(dayjs(dbToken.created), 'minute') > ttl) {
      throw new BadRequestException('ERRORS.TOKEN_EXPIRED');
    }

    await this.tokenService.useToken(dbToken.id);

    const dbUser = await this.authService.changePassword(dbToken.user.id, body.password);

    if (!dbUser) throw new NotFoundException('ERRORS.CHANGE_PASSWORD_PROBLEM');

    return;
  }

  @Public()
  @ApiOperation({
    summary: 'Validation of token, generated with forgot-password.',
  })
  @ApiResponse({
    status: 200,
    description: 'Token is OK',
  })
  @ApiResponse({
    status: 404,
    description: 'Token not found',
  })
  @ApiResponse({
    status: 400,
    description: 'Token expired or used. See message for details',
    type: ReturnedException,
  })
  @Get('check-token/:token')
  async checkToken(@Param('token') token: string) {
    const dbToken = await this.tokenService.findOneByToken(token);
    if (!dbToken) throw new NotFoundException('ERRORS.TOKEN_NOT_FOUND');
    if (dbToken.used) throw new BadRequestException('ERRORS.TOKEN_ALREADY_USED');

    const ttl = parseInt(process.env.TOKEN_TTL) || 60;

    if (dayjs().diff(dayjs(dbToken.created), 'minute') > ttl) {
      throw new BadRequestException('ERRORS.TOKEN_EXPIRED');
    }

    return;
  }

  validateUser(user: User) {
    if (!user) throw new NotFoundException('ERRORS.USER_NOT_FOUND');
    if (!user.active) throw new NotFoundException('ERRORS.USER_IS_INACTIVE');
    return;
  }

  @Public()
  @ApiOperation({
    summary: 'Generates authorization link. Requires X-Custom-Auth header with INTERAPP_SECRET',
  })
  @Post('link')
  @ApiResponse({
    status: 201,
    description: 'Successfull response with authorization link',
    type: ReturnedLink,
  })
  @ApiResponse({
    status: 401,
    description: 'The link could not be generated for one of the reasons',
    type: ReturnedException,
  })
  @UseGuards(InterappGuard)
  async generateLoginLink(@Body() body: LoginLinkQueryDto): Promise<ReturnedLink> {
    const user = await this.usersService.findOneByEmail(body.email);
    this.validateUser(user);
    return this.authService.loginLink(user, body);
  }

  @Public()
  @ApiOperation({
    summary:
      'Authorizes user by link generated with POST /link/ method. ' +
      'If BackUrl was provided during generation — redirects to that Url with token added.',
  })
  @ApiResponse({
    status: 200,
    description: 'Successfull response with access_token object',
    type: ResponseToken,
  })
  @ApiResponse({
    status: 302,
    description: 'Redirect to BackUrl if it was provided, passing token in "token" query parameter',
  })
  @ApiResponse({
    status: 401,
    description: 'The link does not work for one of the reasons',
    type: ReturnedException,
  })
  @Get('link/:token')
  async authorizeByLink(@Param('token') token: string, @Res() res: Response, @Req() req: Request) {
    let payload: Payload;

    try {
      payload = await this.authService.verifyUser(token, p => (payload = p)); // can throw

      const user = await this.usersService.findOne(payload.sub);

      this.validateUser(user); // can throw

      const loginData = await this.authService.login(user);

      // TODO disable after the completion of the reactivation program or
      // decide whether we can use it somehow later
      this.authTriggerService.reactivateMoveLead(user);

      res.cookie(process.env.TOKEN_NAME, loginData.access_token, {
        domain: this.cookieDomain(),
        path: '/',
        httpOnly: false,
      });

      const redirectTo = payload.successUrl || payload.backUrl;

      if (redirectTo) {
        const query = { ...req.query, token: loginData.access_token };
        const redirectUrl = new URL(redirectTo);

        for (const key in query) {
          redirectUrl.searchParams.append(key, query[key]);
        }
        res.status(HttpStatus.TEMPORARY_REDIRECT).redirect(redirectUrl.toString());
      } else {
        res.status(HttpStatus.OK).json(loginData);
      }
    } catch (error) {
      let redirectTo = error.message === 'ERRORS.TOKEN_EXPIRED' ? payload.linkExpiredUrl || payload.errorUrl : payload.errorUrl;

      redirectTo = redirectTo || payload.backUrl;

      if (!redirectTo) {
        throw new BadRequestException('ERRORS.INVALID_TOKEN');
      }

      const redirectUrl = new URL(redirectTo);

      if (redirectTo === payload.errorUrl) {
        redirectUrl.searchParams.append('error', error.message);
      }

      res.status(HttpStatus.TEMPORARY_REDIRECT).redirect(redirectUrl.toString());
    }
  }

  @Get('token-info/:token')
  @ApiOperation({
    summary: 'Checks the token, return token info. Required admin permissions',
  })
  @RequirePermissions(Permission.USERS_MANAGE)
  tokenInfo(@Param('token') token: string) {
    return this.authService.tokenInfo(token);
  }

  generatePassword(): string {
    return Math.random().toString(36).substr(2, 8);
  }

  validatePassword(password: string): boolean {
    // TODO: specify more strict rules for passwords
    console.log(password === '****', password.length);
    return password.length >= 6;
  }

  @Public()
  @UsePipes(
    new ValidationPipe({
      transform: false,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  )
  @Post('register')
  async register(@Body() body: OpenRegisterUserDto): Promise<User> {
    // Check if user is already registered
    const dbUser = await this.usersService.findOneByEmail(body.email);
    if (dbUser) {
      throw new ConflictException('ERRORS.USER_ALREADY_EXISTS');
    }
    if (body.password) {
      assert(this.validatePassword(body.password), new BadRequestException('ERRORS.PASSWORD_IS_TOO_SIMPLE'));
    }

    const defaultDomainCode = (config.defaultDomainCode as MultiDomainsEnum) || null;

    const user = {
      email: body.email,
      name: body.name,
      active: true,
      password: body.password ? body.password : null,
      emailConfirmed: false,
      domain: body.domain || defaultDomainCode,
    } as CreateUserDto;

    if (body.generatePassword) {
      user.password = this.generatePassword();
    }

    const newUser = await this.usersService.create(user);
    const profile = {
      name: body.name,
      phone: body.phone,
    } as CreateProfileDto;

    const newProfile = await this.profileService.create(newUser.id, profile);
    return {
      ...newUser,
      profile: newProfile,
    } as User;
  }
}
