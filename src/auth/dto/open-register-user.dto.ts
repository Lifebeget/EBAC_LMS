import { IsBoolean, IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PlatformType } from 'src/lib/enums/platform-type.enum';
import { MultiDomainsEnum } from '@lib/types/enums';

export class OpenRegisterUserDto {
  @ApiProperty({ description: 'User name' })
  @IsNotEmpty()
  name: string;

  @ApiProperty({ description: 'User email. Must be unique' })
  @IsEmail()
  email: string;

  @ApiProperty({ description: 'User phone' })
  @IsString()
  phone: string;

  @ApiProperty({ description: 'User Password', required: false })
  @IsOptional()
  password?: string;

  @ApiProperty({
    description: 'Generate password for user and send by email. Must specify platform',
    enum: ['true', 'false'],
    default: 'false',
  })
  @IsOptional()
  @IsBoolean()
  generatePassword?: boolean;

  @ApiProperty({
    description: 'Send welcome message',
    enum: ['true', 'false'],
    default: 'false',
  })
  @IsOptional()
  @IsBoolean()
  sendWelcomeMessage?: boolean;

  @ApiProperty({
    description: 'Platform used for registration',
    enum: PlatformType,
  })
  @IsEnum(PlatformType)
  platform: PlatformType;

  @ApiProperty({ description: 'User domain' })
  @IsEnum(MultiDomainsEnum)
  @IsOptional()
  domain?: MultiDomainsEnum;
}
