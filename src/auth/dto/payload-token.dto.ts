import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';
export class PayloadTokenDto {
  @ApiProperty({ description: 'User id' })
  @IsNotEmpty()
  @IsUUID()
  id: string;

  @ApiProperty({ description: 'User email' })
  @IsNotEmpty()
  email: string;
}
