import { ApiProperty } from '@nestjs/swagger';

export class LoginLinkQueryDto {
  @ApiProperty({ required: true, description: 'email' })
  email: string;

  @ApiProperty({
    required: false,
    description: 'Url to redirect after authorization (deprecated)',
  })
  backUrl?: string;

  @ApiProperty({
    required: false,
    description: 'Url to redirect after authorization success',
  })
  successUrl?: string;

  @ApiProperty({
    required: false,
    description: 'Url to redirect if link expired',
  })
  linkExpiredUrl?: string;

  @ApiProperty({
    required: false,
    description: 'Url to redirect on other errors',
  })
  errorUrl?: string;

  @ApiProperty({
    required: false,
    description: 'Link expiration time',
  })
  expires?: Date;
}
