import { mapPermissionsForRoles, Permission } from '@enums/permission.enum';
import { SystemRole } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { mapCoursePermissions } from 'src/helpers/enrollments';
import { UsersService } from '../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_TOKEN_SECRET,
    });
  }

  async validate(payload: any) {
    const user = await this.usersService.findOne(payload.sub);
    if (!user) {
      throw new UnauthorizedException('User not found');
    }
    if (!user.active) {
      throw new UnauthorizedException('User is suspended');
    }
    const roles = user.roles.map(role => role.role);
    const permissions = mapPermissionsForRoles(roles);
    const coursePermissions = mapCoursePermissions(user.enrollments);

    return {
      ...user,
      parsedRoles: roles,
      permissions,
      coursePermissions,
      isSuperadmin(): boolean {
        return this.parsedRoles.includes(SystemRole.Superadmin);
      },
      isAdmin(): boolean {
        return this.parsedRoles.includes(SystemRole.Admin);
      },
      isObserver(): boolean {
        return this.parsedRoles.includes(SystemRole.Observer);
      },
      isDesigner(): boolean {
        return this.parsedRoles.includes(SystemRole.Designer);
      },
      isManager(): boolean {
        return this.parsedRoles.includes(SystemRole.Manager);
      },
      isTeamlead(): boolean {
        return this.parsedRoles.includes(SystemRole.Teamlead);
      },
      isTutor(): boolean {
        return this.parsedRoles.includes(SystemRole.Tutor);
      },
      isSupport(): boolean {
        return this.parsedRoles.includes(SystemRole.Support);
      },
      isFrontendManager(): boolean {
        // роль манеджер на frontend
        return this.isManager() || this.isSuperadmin() || this.isAdmin() || this.isObserver();
      },
      hasSomeRoles(roles: SystemRole | SystemRole[]): boolean {
        if (!Array.isArray(roles)) return this.parsedRoles.includes(roles);

        return roles.some(p => this.parsedRoles.includes(p));
      },
      hasPermission(permission: Permission | Permission[]): boolean {
        if (!Array.isArray(permission)) return this.permissions.includes(permission);

        return permission.every(p => this.permissions.includes(p));
      },
      hasSomePermission(permission: Permission | Permission[]): boolean {
        if (!Array.isArray(permission)) return this.permissions.includes(permission);

        return permission.some(p => this.permissions.includes(p));
      },
      enrolledAsStudent(courseId: string): boolean {
        return this.enrollments.filter(e => e.courseId == courseId).some(e => e.role == CourseRole.Student);
      },
      enrolledAsTutor(courseId: string): boolean {
        return this.enrollments.filter(e => e.courseId == courseId).some(e => e.role == CourseRole.Tutor);
      },
      enrolledAsTeamlead(courseId: string): boolean {
        return this.enrollments.filter(e => e.courseId == courseId).some(e => e.role == CourseRole.Teamlead);
      },
      enrolledAsModerator(courseId: string): boolean {
        return this.enrollments.filter(e => e.courseId == courseId).some(e => e.role == CourseRole.Moderator);
      },
      enrolledAsSupport(courseId: string): boolean {
        return this.enrollments.filter(e => e.courseId == courseId).some(e => e.role == CourseRole.Support);
      },
    };
  }
}
