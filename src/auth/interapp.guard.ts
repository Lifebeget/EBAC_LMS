import { Injectable, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@Injectable()
export class InterappGuard extends AuthGuard('interapp') {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest() as Request;
    const interappToken = req?.header('X-Custom-Auth');

    return interappToken ? super.canActivate(context) : true;
  }
}
