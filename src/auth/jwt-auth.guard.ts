import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { IS_PUBLIC_KEY, IS_PUBLIC_IGNORE_TOKEN_KEY } from './public.decorator';
import { GqlExecutionContext } from '@nestjs/graphql';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [context.getHandler(), context.getClass()]);
    const isPublicIgnoreToken = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_IGNORE_TOKEN_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublicIgnoreToken) {
      return true;
    }

    if (isPublic) {
      const req = context.switchToHttp().getRequest();
      // Ignore @Public() if auth headers are presented
      if (!req.headers['authorization']) {
        return true;
      }
    }

    // @ts-ignore TODO: need description
    if (context.getType() === 'graphql') {
      const req = GqlExecutionContext.create(context).getContext().req;
      return super.canActivate(new ExecutionContextHost([req]));
    }

    const isInterapp = context.switchToHttp().getRequest().isInterapp;

    if (isInterapp) {
      return true;
    }

    return super.canActivate(context);
  }
}
