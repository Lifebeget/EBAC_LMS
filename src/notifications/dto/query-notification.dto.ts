import { ApiProperty } from '@nestjs/swagger';

export class QueryNotificationDto {
  @ApiProperty({ description: 'Set filter by recipient', required: false })
  recipientId?: string;

  @ApiProperty({ description: 'Minimal date of creation', required: false })
  createdMinDate?: Date;

  @ApiProperty({ description: 'Maximal date of creation', required: false })
  createdMaxDate?: Date;

  @ApiProperty({ description: 'Show read notifications', required: false })
  showRead?: boolean;

  @ApiProperty({ description: 'Show unread notifications', required: false })
  showUnread?: boolean;

  @ApiProperty({ description: 'Show deleted notifications', required: false })
  showDeleted?: boolean;
}
