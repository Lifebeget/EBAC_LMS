import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { NotificationData } from 'src/lib/types/NotificationData';
import { NotificationType } from 'src/lib/enums/notification-type.enum';

export class CreateNotificationDto {
  @ApiProperty({ description: 'Notification title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Notification body' })
  @IsNotEmpty()
  body: string;

  @ApiProperty({ enum: NotificationType, description: 'Notification type' })
  @IsNotEmpty()
  notificationType: NotificationType;

  @ApiProperty({ description: 'Additional notification data', required: false })
  notificationData?: NotificationData;

  @ApiProperty({ description: 'Notification author', required: false })
  authorId?: string;

  @ApiProperty({ description: 'Notification recipient' })
  recipientId: string;

  @ApiProperty({ description: 'Is notification important' })
  important?: boolean;
}
