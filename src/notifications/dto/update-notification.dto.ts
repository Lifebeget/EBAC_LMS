import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateNotificationDto } from './create-notification.dto';

export class UpdateNotificationDto extends PartialType(CreateNotificationDto) {
  @ApiProperty({ description: 'Time when notification was read' })
  readAt?: Date;

  @ApiProperty({ description: 'Is notification read' })
  isRead?: boolean;
}
