import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/pagination.dto';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { Notification } from '../lib/entities/notification.entity';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { QueryNotificationDto } from './dto/query-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(Notification)
    private notificationRepository: Repository<Notification>,
    private socketsGateway: SocketsGateway,
  ) {}

  async create(createNotificationDto: CreateNotificationDto) {
    const createFields: any = {
      ...createNotificationDto,
      recipient: { id: createNotificationDto.recipientId },
    };

    if (createNotificationDto.authorId) {
      createFields.author = { id: createNotificationDto.authorId };
    }

    const entity = await this.notificationRepository.save(createFields);
    this.socketsGateway.sendNotification(await this.findOne(entity.id));
    return entity;
  }

  async createBulk(values) {
    const res = await this.notificationRepository.save(values);
    return Promise.all(res.map(e => this.socketsGateway.sendNotification(e)));
  }

  findAll(paging: PaginationDto, query: QueryNotificationDto): Promise<Notification[]> {
    const qb = this.notificationRepository.createQueryBuilder('notification').orderBy('notification.created', 'DESC');

    if (query.recipientId) {
      qb.andWhere('notification.recipient.id = :recipientId', {
        recipientId: query.recipientId,
      });
    }

    if (query.createdMinDate) {
      qb.andWhere('notification.created >= :createdMinDate', {
        createdMinDate: query.createdMinDate,
      });
    }

    if (query.createdMaxDate) {
      qb.andWhere('notification.created >= :createdMaxDate', {
        createdMaxDate: query.createdMaxDate,
      });
    }

    if (query.showRead) {
      qb.andWhere('(notification.isRead = :showRead || notification.isRead = :showUnread)', {
        showRead: query.showRead,
        showUnread: !query.showUnread,
      });
    }

    if (!query.showDeleted) {
      qb.andWhere('(notification.deleted = :deleted)', {
        deleted: false,
      });
    }

    if (!paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    return qb.getMany();
  }

  @Transactional()
  findOne(id: string): Promise<Notification> {
    return this.notificationRepository.findOne(id, {
      relations: ['recipient', 'author'],
    });
  }

  async update(id: string, updateNotificationDto: UpdateNotificationDto) {
    const updateFields = { ...updateNotificationDto, id };
    const entity = await this.notificationRepository.save(updateFields);
    this.socketsGateway.sendNotification(await this.findOne(id));
    return entity;
  }

  remove(id: string) {
    return this.notificationRepository.delete(id);
  }
}
