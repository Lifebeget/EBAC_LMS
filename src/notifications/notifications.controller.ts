import { Body, Controller, Delete, ForbiddenException, Get, Param, ParseUUIDPipe, Patch, Post, Put, Query, Req } from '@nestjs/common';
import * as assert from 'assert';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { QueryNotificationDto } from './dto/query-notification.dto';
import { NotificationsService } from './notifications.service';
import { Notification } from '../lib/entities/notification.entity';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { PgConstraints } from 'src/pg.decorators';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Permission } from 'src/lib/enums/permission.enum';
import { Request } from 'express';
import { NotFoundException } from '@nestjs/common';
import { UnauthorizedException } from '@nestjs/common';

@ApiTags('notifications')
@ApiBearerAuth()
@Controller()
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @Post('notifications')
  @RequirePermissions(Permission.NOTIFICATIONS_MANAGE)
  @ApiOperation({ summary: 'Create a new notification' })
  @ApiResponse({
    status: 201,
    description: 'Created notification',
    type: Notification,
  })
  @PgConstraints()
  create(@Body() createNotificationDto: CreateNotificationDto) {
    return this.notificationsService.create(createNotificationDto);
  }

  @Get('notifications')
  @RequirePermissions(Permission.NOTIFICATIONS_VIEW)
  @ApiOperation({ summary: 'List all notifications' })
  @ApiPaginatedResponse(Notification)
  findAll(@Query() query: QueryNotificationDto, @Pagination() paging: PaginationDto): Promise<Notification[]> {
    return this.notificationsService.findAll(paging, query);
  }

  @Get('notifications/:id')
  @RequirePermissions(Permission.NOTIFICATIONS_VIEW)
  @ApiOperation({ summary: 'Get one notification by id' })
  @ApiResponse({
    status: 200,
    description: 'Retrieved notification',
    type: Notification,
  })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.notificationsService.findOne(id);
  }

  @Patch('notifications/:id')
  @RequirePermissions(Permission.NOTIFICATIONS_MANAGE)
  @ApiOperation({ summary: 'Updates a notification' })
  @ApiResponse({
    status: 200,
    description: 'Updated notification',
  })
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateNotificationDto: UpdateNotificationDto) {
    return this.notificationsService.update(id, updateNotificationDto);
  }

  @Delete('notifications/:id')
  @RequirePermissions(Permission.NOTIFICATIONS_MANAGE)
  @ApiOperation({ summary: 'Deletes a notification' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.notificationsService.remove(id);
  }

  @Get('user/notifications')
  getNotifications(@Query() query: QueryNotificationDto, @Pagination() paging: PaginationDto, @Req() req: Request) {
    const userQuery = { ...query, recipientId: req.user.id, deleted: false };
    return this.notificationsService.findAll(paging, userQuery);
  }

  @Delete('user/notifications/:id')
  async deleteNotification(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    return this.setDeletedFlag(id, req.user?.id, true);
  }

  @Put('user/notifications/:notificationId/read')
  async readNotification(@Param('notificationId', new ParseUUIDPipe()) notificationId: string, @Req() req: Request) {
    return this.setReadFlag(notificationId, req.user.id, true);
  }

  @Put('user/notifications/:notificationId/unread')
  async unreadNotification(@Param('notificationId', new ParseUUIDPipe()) notificationId: string, @Req() req: Request) {
    return this.setReadFlag(notificationId, req.user.id, false);
  }

  async setReadFlag(notificationId: string, userId: string, isRead: boolean) {
    const notification = await this.notificationsService.findOne(notificationId);

    assert(notification, new NotFoundException('Notification is not found'));
    assert(notification.recipient.id == userId, new UnauthorizedException('This notification does not belong to this user'));

    notification.readAt = isRead ? new Date() : null;
    notification.isRead = isRead;

    return this.notificationsService.update(notificationId, notification);
  }

  async setDeletedFlag(id: string, userId: string, isDeleted: boolean) {
    const notification = await this.notificationsService.findOne(id);

    if (!notification) throw new NotFoundException('Notification not found');

    if (notification.recipient.id != userId) {
      throw new ForbiddenException('You are not recipient');
    }

    notification.deleted = isDeleted;

    return this.notificationsService.update(id, notification);
  }
}
