import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationsController } from './notifications.controller';
import { Notification } from '../lib/entities/notification.entity';
import { NotificationsService } from './notifications.service';
import { SocketsModule } from 'src/sockets/sockets.module';

@Module({
  imports: [TypeOrmModule.forFeature([Notification]), forwardRef(() => SocketsModule)],
  controllers: [NotificationsController],
  providers: [NotificationsService],
  exports: [NotificationsService],
})
export class NotificationsModule {}
