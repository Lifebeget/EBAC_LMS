import { IsNotEmpty, IsInt, Matches } from 'class-validator';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';
import { SlugRegexp } from '../../helpers';
import { CourseType } from 'src/lib/enums/course-type.enum';
import { CourseStatus } from '@enums/course-status.enum';

export class CreateCourseDto {
  @ApiProperty({ description: 'Course title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    default: true,
    description: "Activity flag. Inactive course is not available even if it's published",
  })
  active?: boolean;

  @ApiProperty({
    default: false,
    description: 'Publishing flag. Unpublished courses are not visible to students',
  })
  published: boolean;

  @ApiProperty({
    default: false,
    description: 'Show link flag. Show internal url or external',
  })
  showLink: boolean;

  @IsInt()
  @ApiProperty({ description: 'Manual sorting inside category. Ascending' })
  sort?: number;

  @ApiProperty({
    description: 'URL part, must be alphanumeric with dash or underscore',
  })
  @IsNotEmpty()
  @Matches(SlugRegexp)
  slug: string;

  @ApiProperty({ description: 'Course description. Can be HTML' })
  description?: string;

  @ApiHideProperty()
  publishedAt?: Date;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'ID in CMS', required: false })
  cmsId?: string;

  @ApiProperty({ description: 'Image File ID', required: false })
  imageId?: string;

  @ApiProperty({ description: 'Course type', enum: CourseType })
  type?: CourseType;

  @ApiProperty({ description: 'Course duration in months' })
  duration?: number;

  @ApiProperty({ description: 'Course version' })
  version?: number;

  @ApiProperty({ description: 'Course accent color in hex format' })
  color?: string;

  @ApiProperty({
    description: 'Numeration of course modules from 0',
    default: false,
  })
  numberModulesFromZero: boolean;

  @ApiProperty({
    description: 'Auto numbering of course lectures',
    default: false,
  })
  autoNumbering: boolean;

  @ApiProperty({
    description: 'Course demand for support',
    default: 0,
  })
  supportDemand?: number;

  @ApiProperty({
    description: 'Video producer of the course',
    required: false,
  })
  videoProducerId?: string;

  @ApiProperty({
    description: 'Course publishing status',
    default: CourseStatus.created,
  })
  status?: CourseStatus;

  @ApiProperty({
    description: 'Disables forum for course',
    default: true,
  })
  isForumEnabled: boolean;

  @ApiProperty({
    description: 'Course is not for sale',
    default: false,
  })
  notForSale: boolean;

  @ApiProperty({
    default: false,
    required: false,
    description: 'Is satellite course',
  })
  isSatellite: boolean;
}
