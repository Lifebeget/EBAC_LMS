import { ApiProperty } from '@nestjs/swagger';
import { NoteAnchor } from '../../lib/enums/note-anchor.enum';
import { CreateVideoThumbDto } from '../../files/dto/create-video-thumb-file.dto';

export class CreateNoteDto {
  @ApiProperty({ description: 'Lecture ID' })
  lectureId?: string;

  @ApiProperty({ description: 'Content ID' })
  contentId?: string;

  @ApiProperty({ description: 'Timecode' })
  timecode?: number;

  @ApiProperty({ description: 'Quoted text' })
  quote?: string;

  @ApiProperty({ description: 'Anchor of text' })
  anchor?: NoteAnchor;

  @ApiProperty({ description: 'Position in anchor' })
  position?: number;

  @ApiProperty({ description: 'Note' })
  html?: string;

  // FIXIT Нужна реализация сохранения файлов
  @ApiProperty({ description: 'File' })
  file?: CreateVideoThumbDto;
}
