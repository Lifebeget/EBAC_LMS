import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateModuleDto } from './create-module.dto';

export class UpdateModuleDto extends PartialType(CreateModuleDto) {
  @ApiProperty({ description: 'Send email on publish trigger', required: false })
  sendEmail?: boolean;

  @ApiProperty({ description: 'Execute publish trigger', required: false })
  publishTrigger?: boolean;
}
