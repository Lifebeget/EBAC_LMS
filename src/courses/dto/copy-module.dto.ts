import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, ValidateIf } from 'class-validator';

export class CopyModuleDto {
  @ApiProperty({ description: 'Id of the course to which it is copied' })
  toCourseId: string;
  @ApiProperty({ description: 'Id of the section in the course to which it is copied' })
  toSectionId?: string;

  @ApiProperty({ description: 'User id. Needed to check copy access.' })
  userId: string;

  @IsBoolean()
  @ValidateIf(o => o.hasOwnProperty('flatCopy'))
  @ApiProperty({
    description: 'Do need to copy the content of the module lectures',
    default: false,
  })
  flatCopy?: boolean;
}
