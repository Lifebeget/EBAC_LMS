import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class MoveModuleDto {
  @ApiProperty({ description: 'Id of the course into which the module is moved' })
  @IsNotEmpty()
  toCourseId: string;
  @ApiProperty({ description: 'Id of the section into which the module is moved' })
  toSectionId?: string;
}
