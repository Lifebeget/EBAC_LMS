import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsUUID, Matches, ValidateIf } from 'class-validator';
import { SlugRegexp } from '../../helpers';
import { File } from 'src/lib/entities/file.entity';

export class CreateLectureDto {
  @ApiProperty({ description: 'Course ID' })
  @IsUUID()
  courseId: string;

  @ApiProperty({ description: 'Module ID' })
  @IsUUID()
  moduleId: string;

  @ApiProperty({ description: 'Lecture title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    description: 'URL part, must be alphanumeric with dash or underscore',
  })
  @IsNotEmpty()
  @Matches(SlugRegexp)
  slug: string;

  @ApiProperty({
    default: true,
    description: "Activity flag. Inactive module is not available even if it's published",
  })
  active?: boolean;

  @ApiProperty({
    default: false,
    description: 'Publishing flag. Unpublished modules are not visible to students',
  })
  published?: boolean;

  @IsInt()
  @ApiProperty({ description: 'Manual sorting inside course. Ascending' })
  sort?: number;

  @ApiHideProperty()
  publishedAt?: Date;

  @ApiProperty({ description: 'Course description. Can be HTML' })
  description?: string;

  @ApiProperty({
    default: false,
    description: 'Is this lesson available without enrollment',
  })
  free?: boolean;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'Image File ID', required: false })
  imageId?: string;

  @ApiProperty({ description: 'Image File ID', required: false })
  image?: File;

  @ApiProperty({ description: 'Duration', required: false })
  duration?: number;

  @ApiProperty({
    default: false,
    description: 'Requared lecture in the course',
  })
  required?: boolean;

  @IsInt()
  @ValidateIf(o => o.hasOwnProperty('number'))
  @ApiProperty({ description: 'Lecture number' })
  number?: number;

  @ApiProperty({ description: 'Lecture custom number', required: false })
  customNumber?: string;
}
