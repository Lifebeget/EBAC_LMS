import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { MultiDomainsEnum } from '@lib/types/enums';

export class CreateCMSEnrollmentDto {
  @ApiProperty({ description: 'CMS course ID' })
  @IsNotEmpty()
  courseId: string;

  @ApiProperty({ description: 'Enrollment type' })
  type?: EnrollmentType;

  @ApiProperty({
    description: 'Enrollment starts from. Defaults to current date',
  })
  dateFrom?: Date;

  @IsNotEmpty()
  @ApiProperty({ description: 'User email for search or create' })
  email: string;

  @ApiProperty({ description: 'User phone for create' })
  phone?: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'User name for create' })
  name?: string;

  @ApiProperty({ description: 'Enrollment ends at' })
  dateTo?: Date;

  @IsNotEmpty()
  @ApiProperty({ description: 'Enroll reason' })
  reason: string;

  @ApiProperty({ description: 'Eadbox user id' })
  eadboxUserId?: string;

  @ApiProperty({ description: 'Eadbox enrollment id' })
  eadboxEnrollmentId?: string;

  @ApiProperty({ description: 'Send email' })
  sendEmail?: boolean;

  @ApiProperty({ description: 'Addition information', required: false })
  options?: Record<string, unknown>;

  @ApiProperty({ description: 'User domain' })
  @IsEnum(MultiDomainsEnum)
  @IsOptional()
  domain?: MultiDomainsEnum;
}
