import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Transform } from 'class-transformer';
import { IsOptional, IsUUID, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QueryEnrollmentDto {
  @Transform(flag => flag.value.toLowerCase() === 'true', {
    toClassOnly: true,
  })
  showInactive?: boolean;
  role?: CourseRole;

  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ApiProperty({
    description: 'Filter by enrollment external id',
    required: false,
  })
  externalId?: string;

  @ApiProperty({
    description: 'Filter by user email',
    required: false,
  })
  exactEmail?: string;

  @ApiProperty({
    description: 'Filter by enrollment external ids (array)',
    required: false,
  })
  externalIds?: string[];

  @ApiProperty({
    description: 'Sort by field',
    required: false,
  })
  sortField?: string;

  @ApiProperty({
    description: 'Sort direction',
    required: false,
  })
  sortDirection?: 'ASC' | 'DESC';

  @ApiProperty({
    description: 'From date of creation',
    required: false,
  })
  createdFrom?: string;

  @ApiProperty({
    description: 'To date of creation',
    required: false,
  })
  createdTo?: string;

  @ApiProperty({
    description: 'Author Name',
    required: false,
  })
  authorName?: string;

  @ApiProperty({
    description: 'Filter by any author existence',
    required: false,
  })
  anyAuthor?: boolean;

  @ApiProperty({
    description: 'Excluded authors filter',
    required: false,
  })
  authorsExept?: string[];

  @ApiProperty({
    description: 'Filter by enrollment type',
    required: false,
  })
  types?: string[];

  @ApiProperty({
    description: 'Filter by activity',
    required: false,
  })
  active?: string;

  @ApiProperty({
    description: 'Filter by name/email/student Id/reason',
    required: false,
  })
  search?: string;

  @ApiProperty({
    description: 'Filter by tags',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  tagIds?: string[];
}
