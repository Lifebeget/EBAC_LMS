import { ApiProperty } from '@nestjs/swagger';
import { ContentType } from '../../lib/enums/content-type.enum';

export class QueryContentDto {
  @ApiProperty({ description: 'Lecture ID', required: false })
  lectureId?: string;

  @ApiProperty({ description: 'Content type', required: false })
  type?: ContentType;

  @ApiProperty({ description: 'Active', required: false })
  active?: boolean;

  @ApiProperty({ description: 'Assessment ID', required: false })
  assessmentId?: string;

  @ApiProperty({ description: 'ID kaltura/youtube', required: false })
  entityId?: string;

  @ApiProperty({ description: 'Link', required: false })
  link?: string;

  @ApiProperty({ description: 'Owner Id', required: false })
  ownerId?: string;

  @ApiProperty({ description: 'Created date', required: false })
  dateCreated?: Date;
}
