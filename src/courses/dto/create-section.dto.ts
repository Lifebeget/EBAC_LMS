import { IsNotEmpty, IsInt } from 'class-validator';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

export class CreateSectionDto {
  @ApiProperty({ description: 'Section title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Section description', required: false })
  description?: string;

  @ApiProperty({
    default: true,
    description: "Activity flag. Inactive module is not available even if it's published",
  })
  active?: boolean;

  @ApiProperty({
    default: false,
    description: 'Publishing flag. Unpublished modules are not visible to students',
  })
  published: boolean;

  @IsInt()
  @ApiProperty({ description: 'Manual sorting inside course. Ascending' })
  sort?: number;

  @ApiHideProperty()
  publishedAt?: Date;
}
