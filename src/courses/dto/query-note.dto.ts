import { ApiProperty } from '@nestjs/swagger';

export class QueryNoteDto {
  @ApiProperty({ description: 'Lecture ID', required: false })
  lectureId?: string;

  @ApiProperty({ description: 'Content ID', required: false })
  contentId?: string;

  @ApiProperty({ description: 'Owner ID', required: false })
  ownerId?: string;

  @ApiProperty({ description: 'Course ID', required: false })
  courseId?: string;

  @ApiProperty({ description: 'Created date', required: false })
  dateCreated?: Date;

  @ApiProperty({ description: 'Deleted', required: false })
  deleted?: boolean;
}
