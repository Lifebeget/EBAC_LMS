import { ApiProperty } from '@nestjs/swagger';

export class QueryLectureEvaluationDto {
  @ApiProperty({ description: 'Filter by lecture' })
  lectureId?: string;

  @ApiProperty({ description: 'Filter by user' })
  userId?: string;
}
