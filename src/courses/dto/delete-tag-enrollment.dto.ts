import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CourseRole } from '@lib/types/enums/course-role.enum';

export class DeleteTagEnrollmentDto {
  @ApiProperty({ description: 'Tag ID' })
  @IsNotEmpty()
  tagId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'User id for enrollment deletion' })
  userId: string;

  @ApiProperty({ enum: CourseRole, description: 'User role on course' })
  @IsNotEmpty()
  role: CourseRole;
}
