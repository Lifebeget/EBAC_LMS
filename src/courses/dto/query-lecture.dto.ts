import { IsUUID, ValidateIf } from 'class-validator';

export class QueryLectureDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('moduleId'))
  @IsUUID()
  moduleId?: string;
}
