import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, ValidateIf } from 'class-validator';

export class CopyLectureDto {
  @ApiProperty({ description: 'Id of the module to which it is copied' })
  toModuleId: string;

  @ApiProperty({ description: 'Id of the course to which it is copied' })
  toCourseId: string;

  @ApiProperty({ description: 'User id. Needed to check copy access.' })
  userId: string;

  @IsBoolean()
  @ValidateIf(o => o.hasOwnProperty('flatCopy'))
  @ApiProperty({
    description: 'Do need to copy the content of the lecture',
    default: false,
  })
  flatCopy?: boolean;

  @ApiProperty({ description: 'Copy binding analogs.' })
  analogs?: boolean;
}
