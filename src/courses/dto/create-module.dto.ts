import { IsNotEmpty, IsInt, IsBoolean, ValidateIf } from 'class-validator';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

export class CreateModuleDto {
  @ApiProperty({ description: 'Module title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Module description', required: false })
  description?: string;

  @ApiProperty({
    default: true,
    description: "Activity flag. Inactive module is not available even if it's published",
  })
  active?: boolean;

  @ApiProperty({
    default: false,
    description: 'Publishing flag. Unpublished modules are not visible to students',
  })
  published: boolean;

  @IsInt()
  @ApiProperty({ description: 'Manual sorting inside course. Ascending' })
  sort?: number;

  @ApiHideProperty()
  publishedAt?: Date;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'ID of section', required: false })
  section_id?: string;

  @IsBoolean()
  @ValidateIf(o => o.hasOwnProperty('numberLecturesFromZero'))
  @ApiProperty({ description: 'Numeration of module lectures from 0', default: false })
  numberLecturesFromZero?: boolean;

  @IsInt()
  @ValidateIf(o => o.hasOwnProperty('number'))
  @ApiProperty({ description: 'Module number' })
  number?: number;

  @ApiProperty({ description: 'Module custom number', required: false })
  customNumber?: string;
}
