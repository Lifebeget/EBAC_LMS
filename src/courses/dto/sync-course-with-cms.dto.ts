import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { File } from '@entities/file.entity';

export class SyncCourseWithCmsDto {
  @ApiProperty({ description: 'CMS course id' })
  @IsNotEmpty()
  id: string;

  @ApiProperty({ description: 'Course image' })
  @IsNotEmpty()
  image: File;

  @ApiProperty({ description: 'Course duration' })
  @IsNotEmpty()
  duration: number;

  @ApiProperty({ description: 'Bg color' })
  @IsNotEmpty()
  color: string;
}
