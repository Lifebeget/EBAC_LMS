import { LectureStatus } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';

export class SetLectureStatusDto {
  @ApiProperty({ enum: LectureStatus, description: 'Lecture status' })
  status: LectureStatus;
}
