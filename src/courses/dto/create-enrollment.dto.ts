import { EnrollmentRightsType } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';

export class CreateEnrollmentDto {
  @ApiProperty({ description: 'User ID' })
  @IsNotEmpty()
  userId: string;

  @ApiProperty({ enum: CourseRole, description: 'User role on course' })
  @IsNotEmpty()
  role: CourseRole;

  @ApiProperty({ description: 'Course ID' })
  courseId?: string;

  @ApiProperty({
    required: false,
    default: true,
    description: 'Enrollment activity flag',
  })
  active?: boolean;

  @ApiProperty({ description: 'Enrollment type' })
  type?: EnrollmentType;

  @ApiProperty({
    description: 'Enrollment starts from. Defaults to current date',
  })
  dateFrom?: Date;

  @ApiProperty({ description: 'Enrollment ends at', required: false })
  dateTo?: Date;

  @ApiProperty({ description: 'Enrollment completed at', required: false })
  graduatedAt?: Date;

  @ApiProperty({ description: 'Enrollment service ends at', required: false })
  serviceEndsAt?: Date;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'Send email', required: false })
  sendEmail?: boolean;

  @ApiProperty({ description: 'Reason of enrollment', required: false })
  reason?: Record<string, unknown>;

  @ApiProperty({
    description: 'Tariff for tutor interactivity (percents of income)',
    required: false,
  })
  interactiveTariff?: number;

  @ApiProperty({
    description: 'Fee for teamlead activity',
    required: false,
  })
  teamleadFee?: number;

  @ApiProperty({
    description: 'Enrollment right',
  })
  rights?: EnrollmentRightsType;

  @ApiProperty({ description: 'Addition information', required: false })
  options?: Record<string, unknown>;
}
