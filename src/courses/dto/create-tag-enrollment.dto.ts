import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';

export class CreateTagEnrollmentDto {
  @ApiProperty({ description: 'Tag ID' })
  @IsNotEmpty()
  tagId: string;

  @ApiProperty({ description: 'Enrollment type' })
  type?: EnrollmentType;

  @ApiProperty({
    description: 'Enrollment starts from. Defaults to current date',
  })
  dateFrom?: Date;

  @IsNotEmpty()
  @ApiProperty({ description: 'User id for enrollment' })
  userId: string;

  @ApiProperty({ enum: CourseRole, description: 'User role on course' })
  @IsNotEmpty()
  role: CourseRole;

  @IsNotEmpty()
  @ApiProperty({ description: 'Enrollment ends at' })
  dateTo: Date;

  @IsNotEmpty()
  @ApiProperty({ description: 'Enroll reason' })
  reason: string;

  @ApiProperty({ description: 'Send email' })
  sendEmail?: boolean;

  @ApiProperty({ description: 'Addition information', required: false })
  options?: Record<string, unknown>;
}
