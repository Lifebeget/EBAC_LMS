import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class MoveLectureDto {
  @ApiProperty({ description: 'Id of the module into which the lecture is moved' })
  @IsNotEmpty()
  toModuleId: string;
}
