import { ApiProperty } from '@nestjs/swagger';
import { EvaluationFeature } from 'src/lib/enums/evaluation-feature.enum';
import { Evaluation } from 'src/lib/enums/evaluation.enum';

export class CreateLectureEvaluationDto {
  @ApiProperty({ description: 'Course to evaluate' })
  lectureId: string;

  @ApiProperty({
    description: 'User who evaluates a course  (works only with "EVALUATIONS_MANAGE" permissions)',
  })
  userId?: string;

  @ApiProperty({ description: 'Course evaluation', minimum: 0, maximum: 3 })
  evaluation: Evaluation;

  @ApiProperty({ description: 'Why this lecture is good' })
  likedFeature?: EvaluationFeature;

  @ApiProperty({ description: 'What can be better in lecture' })
  userWishes?: string;
}
