import { ApiProperty } from '@nestjs/swagger';

export class QueryEvaluationStatisticsDto {
  @ApiProperty({ description: 'Start date for period filter', required: false })
  periodStart?: Date;

  @ApiProperty({ description: 'End date for period filter', required: false })
  periodEnd?: Date;

  @ApiProperty({ description: 'Lecture id for filter', required: false })
  lectureId?: string;
}
