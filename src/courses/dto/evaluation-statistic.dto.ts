import { ApiProperty } from '@nestjs/swagger';

export class EvaluationStatisticDto {
  @ApiProperty({ description: 'Average evaluation' })
  average: number;

  @ApiProperty({ description: 'Number of evaluations' })
  evaluationsCount: number;

  @ApiProperty({ description: 'Number of users who creates evaluations' })
  usersCount: number;
}
