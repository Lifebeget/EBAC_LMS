import { ApiProperty } from '@nestjs/swagger';

export class AttachCourseTagsDto {
  @ApiProperty({ type: [String], description: 'List of tag id' })
  tagIds: string[];

  @ApiProperty({
    description: 'If "true" then replace all tags that are attached to course',
  })
  replace: boolean;
}
