import { File } from '@entities/file.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateAssessmentForContentDto } from 'src/assessments/dto/create-assessment.dto';
import { ContentType } from '../../lib/enums/content-type.enum';

export class CreateContentDto {
  @ApiProperty({ description: 'Content type' })
  @IsNotEmpty()
  type: ContentType;

  @ApiProperty({ description: 'UUid id for sync' })
  id?: string;

  @ApiProperty({ description: 'Sorting. Min is high priority', default: 500 })
  sort?: number;

  @ApiProperty({ description: 'Active' })
  active?: boolean;

  @ApiProperty({ description: 'Title' })
  title?: string;

  @ApiProperty({ description: 'Assessment ID' })
  assessmentId?: string;

  @ApiProperty({ description: 'Attached files' })
  files?: File[];

  @ApiProperty({ description: 'Text of content' })
  html?: string;

  @ApiProperty({ description: 'ID kaltura/youtube' })
  entityId?: string;

  @ApiProperty({ description: 'Link' })
  link?: string;

  @ApiProperty({ description: 'Data' })
  data?: Record<string, unknown>;

  @ApiProperty({ description: 'Assessment' })
  assessment?: CreateAssessmentForContentDto;

  @ApiProperty({ description: 'externalId' })
  externalId?: string;
}
