import { CreateContentDto } from './create-content.dto';
import { UpdateContentDto } from './update-content.dto';

export class VersionUpdateDTO {
  lectureId?: string;
  create: CreateContentDto[];
  update: UpdateContentDto[];
  delete: (string | number)[];
}

export class VersionUpdateDTOResult {
  create: any[];
  update: any[];
  delete: any[];
}
