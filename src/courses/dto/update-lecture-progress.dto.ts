import { LectureStatus } from '@enums/lecture-status.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

export class UpdateLectureProgressDto {
  @ApiProperty({ description: 'Average evaluation' })
  @IsEnum(LectureStatus)
  status: LectureStatus;
}
