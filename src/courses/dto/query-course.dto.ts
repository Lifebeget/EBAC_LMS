import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class QueryCourseDto {
  @Transform(flag => flag.value.toLowerCase() === 'true', {
    toClassOnly: true,
  })
  includeModules?: boolean;

  @Transform(flag => flag.value.toLowerCase() === 'true', {
    toClassOnly: true,
  })
  includeLectures?: boolean;

  @Transform(flag => flag.value.toLowerCase() === 'true', {
    toClassOnly: true,
  })
  includeCounters?: boolean;

  @ApiProperty({
    description: 'tag title',
    required: false,
  })
  tagTitle?: string;

  @ApiProperty({
    description: 'Filter by ids (array)',
    required: false,
  })
  ids?: string[];
}
