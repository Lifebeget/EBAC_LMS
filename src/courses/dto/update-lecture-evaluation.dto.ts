import { PartialType } from '@nestjs/swagger';
import { CreateLectureEvaluationDto } from './create-lecture-evaluation.dto';

export class UpdateLectureEvaluationDto extends PartialType(CreateLectureEvaluationDto) {}
