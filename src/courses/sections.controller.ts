import { Controller, Get, Post, Put, Body, Patch, Param, Delete, ParseUUIDPipe, NotFoundException } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { SectionsService } from './sections.service';
import { CreateSectionDto } from './dto/create-section.dto';
import { UpdateSectionDto } from './dto/update-section.dto';
import { PgConstraints } from '../pg.decorators';
import { Section } from '../lib/entities/section.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { Permissions } from 'src/permissions.decorator';
import { Module } from '../lib/entities/module.entity';

@ApiTags('sections')
@ApiBearerAuth()
@Controller()
export class SectionsController {
  constructor(private readonly sectionsService: SectionsService) {}

  @Post('courses/:courseId/sections')
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new section' })
  @ApiResponse({
    status: 201,
    description: 'Created section',
    type: Section,
  })
  @PgConstraints()
  create(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Body() createSectionDto: CreateSectionDto) {
    return this.sectionsService.create(courseId, createSectionDto);
  }

  @Get('courses/:courseId/sections')
  @ApiOperation({ summary: 'List all sections for a course' })
  @ApiResponse({
    status: 200,
    type: [Section],
  })
  findAll(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Permissions() perms: Permission[]) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    return this.sectionsService.findAll(courseId, includeHidden);
  }

  @Get('courses/:courseId/sections-for-admin-panel')
  @ApiOperation({ summary: 'List all sections for a course' })
  @ApiResponse({
    status: 200,
    type: [Section],
  })
  findForAdminPanel(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Permissions() perms: Permission[]) {
    return this.sectionsService.findForAdminPanel(courseId);
  }

  @Get('sections/:id/modules')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'Get modules by section id with active deep checking' })
  @ApiResponse({
    status: 200,
    description: 'Get modules by section id',
    type: [Module],
  })
  async findModulesBySection(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    const result = await this.sectionsService.findModulesBySectionIds([id]);
    if (!result) {
      throw new NotFoundException('ERRORS.MODULES_NOT_FOUND');
    }
    return result;
  }

  @Get('sections/:id')
  @ApiOperation({ summary: 'Get one Section by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a Section by id',
    type: Section,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.sectionsService.findOne(id, includeHidden);
    if (!result) {
      throw new NotFoundException('ERRORS.SECTION_NOT_FOUND');
    }
    return result;
  }

  @Patch('sections/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a Section' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateSectionDto: UpdateSectionDto) {
    return this.sectionsService.update(id, updateSectionDto);
  }

  @Put('sections/:id/move/:courseId')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Move section to another course.' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  move(@Param() params: { id: string; courseId: string }) {
    return this.sectionsService.move(params.id, params.courseId);
  }

  @Put('sections/:id/publish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Publish a Section' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  publish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectionsService.publish(id);
  }

  @Put('sections/:id/unpublish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Unpublish a Section' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  unpublish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectionsService.unpublish(id);
  }

  @Delete('sections/:id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Deletes a Section' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectionsService.remove(id);
  }
}
