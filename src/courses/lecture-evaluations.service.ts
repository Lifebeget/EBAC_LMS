import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LectureEvaluation } from 'src/lib/entities/lecture-evaluation.entity';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateLectureEvaluationDto } from './dto/create-lecture-evaluation.dto';
import { EvaluationStatisticDto } from './dto/evaluation-statistic.dto';
import { QueryEvaluationStatisticsDto } from './dto/query-evaluation-statistics.dto';
import { QueryLectureEvaluationDto } from './dto/query-lecture-evaluation.dto';
import { UpdateLectureEvaluationDto } from './dto/update-lecture-evaluation.dto';

@Injectable()
export class LectureEvaluationsService {
  constructor(
    @InjectRepository(LectureEvaluation)
    private readonly lectureEvaluationRepository: Repository<LectureEvaluation>,
  ) {}
  async save(evaluationDto: CreateLectureEvaluationDto) {
    const existed = await this.getUserEvaluation(evaluationDto.lectureId, evaluationDto.userId);

    if (existed) {
      return this.update(existed.id, evaluationDto);
    }

    return this.create(evaluationDto);
  }
  async create(evaluationDto: CreateLectureEvaluationDto) {
    const lectureEvaluation = {
      ...evaluationDto,
      user: { id: evaluationDto.userId },
      lecture: { id: evaluationDto.lectureId },
    };

    return this.lectureEvaluationRepository.save(lectureEvaluation);
  }

  async getUserEvaluation(lectureId: string, userId: string) {
    const result = await this.query({ lectureId, userId });

    if (result.length === 0) {
      return null;
    }

    return result[0];
  }

  async query(queryDto: QueryLectureEvaluationDto) {
    const where: { user?: any; lecture?: any } = {};

    if (queryDto.userId) {
      where.user = { id: queryDto.userId };
    }

    if (queryDto.lectureId) {
      where.lecture = { id: queryDto.lectureId };
    }

    return this.lectureEvaluationRepository.find({
      where,
      order: {
        created: 'ASC',
      },
    });
  }

  @Transactional()
  findOne(id: string, relations?: string[]) {
    return this.lectureEvaluationRepository.findOne(id, { relations });
  }

  async update(id: string, evaluationDto: UpdateLectureEvaluationDto) {
    const lectureEvaluation = {
      ...evaluationDto,
      user: { id: evaluationDto.userId },
      lecture: { id: evaluationDto.lectureId },
      id,
    };

    return this.lectureEvaluationRepository.save(lectureEvaluation);
  }

  async getStatistics(query: QueryEvaluationStatisticsDto) {
    const qb = this.lectureEvaluationRepository
      .createQueryBuilder('le')
      .innerJoin('le.user', 'user')
      .select('AVG(le.evaluation) as average, COUNT(le.id) as evaluationsCount, COUNT(DISTINCT user.id) as usersCount');

    if (query.lectureId) {
      qb.andWhere('le.lecture_id = :lectureId', { lectureId: query.lectureId });
    }

    if (query.periodStart) {
      qb.andWhere('le.created >= :periodStart', {
        periodStart: query.periodStart,
      });
    }

    if (query.periodEnd) {
      qb.andWhere('le.created <= :periodEnd', {
        periodEnd: query.periodEnd,
      });
    }

    return (await qb.getRawMany()) as EvaluationStatisticDto[];
  }

  async delete(id: string) {
    return this.lectureEvaluationRepository.delete(id);
  }
}
