import { EnrollmentRightsType, SystemRole } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import {
  Body,
  Controller,
  Delete,
  forwardRef,
  Get,
  Inject,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Interapp } from 'src/auth/interapp.decorator';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { populateLogByCourse, PopulateLogWith, SetLogTag } from 'src/service/log/logging';
import { WelcomeTriggerService } from 'src/trigger/services/welcome-trigger.service';
import { Enrollment } from '../lib/entities/enrollment.entity';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { CreateEnrollmentDto } from './dto/create-enrollment.dto';
import { QueryEnrollmentDto } from './dto/query-enrollment.dto';
import { UpdateEnrollmentDto } from './dto/update-enrollment.dto';
import { CreateCMSEnrollmentDto } from './dto/create-cms-enrollment.dto';
import { CreateTagEnrollmentDto } from './dto/create-tag-enrollment.dto';
import { EnrollmentsService } from './enrollments.service';
import { UsersService } from '../users/users.service';
import { CoursesService } from './courses.service';
import { CreateProfileDto } from '../users/dto/create-profile.dto';
import { ProfileService } from '../users/profile.service';
import { generatePassword } from '../helpers';
import { DeleteTagEnrollmentDto } from './dto/delete-tag-enrollment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Course } from '@entities/course.entity';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { BadRequestException } from '@nestjs/common/exceptions/bad-request.exception';
import { WelcomeEnrollmentSendDto } from './dto/welcome-enrollment-send.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { EnrollmentTriggerService } from 'src/trigger/services/enrollment-trigger.service';

@ApiTags('enrollments')
@ApiBearerAuth()
@Interapp()
@Controller()
export class EnrollmentsController {
  constructor(
    private readonly enrollmentsService: EnrollmentsService,
    private welcomeTriggerService: WelcomeTriggerService,
    private readonly coursesService: CoursesService,
    private readonly usersService: UsersService,
    private readonly profileService: ProfileService,
    @InjectRepository(Enrollment)
    private readonly enrollmentsRepository: Repository<Enrollment>,
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
    @Inject(forwardRef(() => EnrollmentTriggerService))
    private readonly enrollmentTriggerService: EnrollmentTriggerService,
  ) {}

  @Get('courses/:courseId/enrollments')
  @RequirePermissions(Permission.COURSE_ENROLLMENTS_VIEW)
  @ApiOperation({ summary: 'List all enrollments for a course' })
  @ApiPaginatedResponse(Enrollment)
  findAllForCourse(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Query() query: QueryEnrollmentDto,
    @Pagination() paging: PaginationDto,
  ) {
    return this.enrollmentsService.query({ courseId, ...query }, paging);
  }

  @Post('courses/:courseId/enrollments')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Enrol user to a course' })
  @ApiResponse({
    status: 201,
    description: 'Created enrollment',
    type: Enrollment,
  })
  @PgConstraints()
  @PopulateLogWith(async (log, context) => {
    const courseId = log.url.slice(1).split('/')[1];
    return populateLogByCourse(courseId, context);
  })
  @SetLogTag('EnrollmentsController.create')
  async create(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Body() createEnrollmentDto: CreateEnrollmentDto,
    @Req() req: Express.Request,
  ) {
    const course = await this.coursesService.findOne(courseId, {
      includeHidden: true,
    });
    // check course notForSale flag and enrollment type
    if (createEnrollmentDto.type === EnrollmentType.Buy && course.notForSale) {
      throw new BadRequestException('ENROLLMENTS.ERROR.NOT_FOR_SALE');
    }
    if (createEnrollmentDto.role === CourseRole.Student && !req.user.permissions.includes(Permission.COURSE_STUDENT_ENROLL)) {
      throw new UnauthorizedException("You don't have access to create enrollments with the student role");
    }
    return await this.enrollmentsService.create(courseId, createEnrollmentDto);
  }

  @Post('enrollments/welcome-send')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Send welcome email enroll user to a course' })
  @ApiResponse({
    status: 201,
    description: 'Email sent',
  })
  async welcomeEnrollmentSend(@Body() dto: WelcomeEnrollmentSendDto) {
    const enrollment = await this.enrollmentsService.findRecentByStudentAndCourse(dto.userId, dto.courseId);
    if (!enrollment) {
      throw new NotFoundException('ENROLLMENTS.ENROLLMENT_NOT_FOUND');
    }
    const event = await this.enrollmentTriggerService.prepareTriggerData(dto.courseId, enrollment, true);
    return await this.enrollmentTriggerService.userEnrolledWelcomeEmail(event);
  }

  @Post('cms/enroll')
  @Interapp()
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Enrol user to a course by cms course id' })
  @ApiResponse({
    status: 201,
    description: 'Created enrollment',
    type: Enrollment,
  })
  @PgConstraints()
  @SetLogTag('EnrollmentsController.create')
  createCMS(
    @Body()
    createCMSEnrollmentDto: CreateCMSEnrollmentDto,
  ) {
    return this.enrollmentsService.createCMS(createCMSEnrollmentDto);
  }

  @Get('/enrollments')
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @Interapp()
  @RequirePermissions(Permission.COURSE_ENROLLMENTS_VIEW)
  @ApiOperation({ summary: 'List all enrollments' })
  @ApiQuery({
    name: 'showInactive',
    enum: ['true', 'false'],
    description: 'Include inactive enrollments',
    required: false,
  })
  @ApiQuery({
    name: 'role',
    enum: CourseRole,
    description: 'Filter by role',
    required: false,
  })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'userId',
    description: 'Filter by user',
    required: false,
  })
  @ApiQuery({
    name: 'exactEmail',
    description: 'Filter by user email',
    required: false,
  })
  @ApiQuery({
    name: 'externalId',
    description: 'Filter by enrollment externalId',
    required: false,
  })
  @ApiQuery({
    name: 'externalIds',
    description: 'Filter by enrollment externalIds (array)',
    required: false,
  })
  @ApiQuery({
    name: 'sortField',
    description: 'Sort field',
    required: false,
  })
  @ApiQuery({
    name: 'sortDirection',
    description: 'Sort direction',
    required: false,
  })
  @ApiQuery({
    name: 'createdFrom',
    description: 'from date of creation',
    required: false,
  })
  @ApiQuery({
    name: 'createdTo',
    description: 'to date of creation',
    required: false,
  })
  @ApiQuery({
    name: 'authorName',
    description: 'Author Name',
    required: false,
  })
  @ApiQuery({
    name: 'anyAuthor',
    description: 'Filter by any author existence',
    required: false,
  })
  @ApiQuery({
    name: 'authorsExept',
    description: 'Excluded authors filter',
    required: false,
  })
  @ApiQuery({
    name: 'types',
    description: 'Filter by enrollment type',
    required: false,
  })
  @ApiQuery({
    name: 'active',
    description: 'Filter by activity',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    description: 'Filter by name/email/student Id/reason',
    required: false,
  })
  @ApiPaginatedResponse(Enrollment)
  findAll(@Query() query: QueryEnrollmentDto, @Pagination() paging: PaginationDto) {
    return this.enrollmentsService.query(query, paging);
  }

  @Get('user/enrollments')
  @ApiOperation({ summary: 'List all enrollments of the user' })
  @ApiPaginatedResponse(Enrollment)
  findAllForUser(@Pagination() paging: PaginationDto, @Req() req: Express.Request) {
    return this.enrollmentsService.query({ userId: req.user.id, role: CourseRole.Student }, paging);
  }

  @Get('enrollments/assignments')
  @ApiOperation({ summary: 'List tutor assignments' })
  @RequirePermissions(Permission.COURSE_ENROLLMENTS_VIEW)
  async findAssignments() {
    const enrollments = await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .select(['id', `json_build_object('id', enrollment.user_id) "user"`, `json_build_object('id', enrollment.course_id) "course"`])
      .andWhere('enrollment.role = :role', { role: SystemRole.Tutor })
      .getRawMany();

    const users = await this.usersService.findByRole(SystemRole.Tutor);

    const courses = await this.coursesRepository.createQueryBuilder('course').select(['id', 'title', 'version']).getRawMany();

    return { enrollments, users, courses };
  }

  @Get('enrollments/:id')
  @RequirePermissions(Permission.COURSE_ENROLLMENTS_VIEW)
  @ApiOperation({ summary: 'Get one Enrollment by id' })
  @ApiResponse({
    status: 200,
    description: 'Get Enrollment by id',
    type: Enrollment,
  })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.enrollmentsService.findOne(id);
  }

  @Patch('enrollments/:id')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Updates an Enrollment' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateEnrollmentDto: UpdateEnrollmentDto) {
    const course = await this.coursesService.findOne(updateEnrollmentDto.courseId, { includeHidden: true });
    // check course notForSale flag and enrollment type
    if (updateEnrollmentDto.type === EnrollmentType.Buy && course.notForSale) {
      throw new BadRequestException('ENROLLMENTS.ERROR.NOT_FOR_SALE');
    }
    return this.enrollmentsService.update(id, updateEnrollmentDto);
  }

  @Put('enrollments/:id/deactivate')
  @Interapp()
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Deactivate an Enrollment' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  deactivate(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.enrollmentsService.deactivate(id);
  }

  @Put('enrollments/:id/activate')
  @Interapp()
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Activate an Enrollment' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  activate(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.enrollmentsService.activate(id);
  }

  @Delete('enrollments/:id')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Deletes a Enrollment' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PopulateLogWith(async (log, context) => {
    const id = log.url.slice(1).split('/')[1];
    return populateLogByCourse(id, context);
  })
  @SetLogTag('EnrollmentsController.remove')
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.enrollmentsService.remove(id);
  }

  @Post('enrollments/create-by-tag')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Enrol user to a courses by tag id' })
  @ApiResponse({
    status: 201,
    description: 'Created enrollments',
  })
  @PgConstraints()
  @SetLogTag('EnrollmentsController.create')
  async createByTag(
    @Body()
    createTagEnrollmentDto: CreateTagEnrollmentDto,
    @Req() req: Express.Request,
  ) {
    return await this.enrollmentsService.createByTag(createTagEnrollmentDto, req.user.id);
  }

  @Post('enrollments/delete-by-tag')
  @RequirePermissions(Permission.COURSE_ENROLL)
  @ApiOperation({ summary: 'Remove user enrollments to a courses by tag id' })
  @ApiResponse({
    status: 201,
    description: 'Deleted enrollments',
  })
  @PgConstraints()
  @SetLogTag('EnrollmentsController.create')
  async disableByTag(
    @Body()
    deleteTagEnrollmentDto: DeleteTagEnrollmentDto,
  ) {
    return await this.enrollmentsService.disableByTag(deleteTagEnrollmentDto);
  }
}
