import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseBoolPipe,
  ParseUUIDPipe,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UnauthorizedException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LecturesService } from './lectures.service';
import { CoursesService } from './courses.service';
import { EnrollmentsService } from './enrollments.service';
import { CreateLectureDto } from './dto/create-lecture.dto';
import { UpdateLectureDto } from './dto/update-lecture.dto';
import { QueryLectureDto } from './dto/query-lecture.dto';
import { PgConstraints } from '../pg.decorators';
import { Lecture } from '@entities/lecture.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { Request } from 'express';
import { LectureProgress } from 'src/lib/entities/lecture-progress.entity';
import { Permissions } from 'src/permissions.decorator';
import { Pagination } from 'src/pagination.decorator';
import { PaginationDto } from 'src/pagination.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { MoveLectureDto } from './dto/move-lecture.dto';
import { CopyLectureDto } from './dto/copy-lecture.dto';
import { SetLectureStatusDto } from './dto/set-lecture-status.dto';
import { useCache } from 'src/helpers/cache';
import { UpdateLectureProgressDto } from './dto/update-lecture-progress.dto';
import { EnrollmentsHelper } from '@utils/enrollments.helper';

@ApiTags('lectures')
@ApiBearerAuth()
@Controller()
export class LecturesController {
  constructor(
    private readonly lecturesService: LecturesService,
    private readonly coursesService: CoursesService,
    private readonly enrollmentsService: EnrollmentsService,
    private readonly enrollmentsHelper: EnrollmentsHelper,
  ) {}

  @Transactional()
  @Get('user/lectures-progress')
  @ApiOperation({ summary: 'Returns progress for all lectures in course' })
  @ApiResponse({
    status: 200,
    type: [LectureProgress],
  })
  async lecturesProgress(@Req() req: Request, @Pagination() pagination: PaginationDto, @Query('courseId') courseId?: string) {
    return await this.lecturesService.findAllLecturesProgress(pagination, {
      userId: req.user.id,
      courseId,
    });
  }

  @Transactional()
  @Get('user/lectures-progress/:id')
  @ApiOperation({ summary: 'Returns progress for lecture' })
  @ApiResponse({ type: LectureProgress })
  async lectureViews(@Req() req: Request, @Param('id') lectureId?: string) {
    return await this.lecturesService.findOneLecturesProgress({
      userId: req.user?.id,
      lectureId,
    });
  }

  @Transactional()
  @Get('courses/:courseId/modules/:moduleId/lectures')
  @ApiOperation({ summary: 'List all lectures for a course module' })
  @ApiResponse({
    status: 200,
    type: [Lecture],
  })
  async findAllForModule(
    @Req() req: Request,
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Param('moduleId', new ParseUUIDPipe()) moduleId: string,
    @Permissions() perms: Permission[],
  ) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.lecturesService.findAll(courseId, moduleId, includeHidden, useCache(req));
    if (!result.length) {
      throw new NotFoundException('ERRORS.LECTURES_NOT_FOUND');
    }
    return result;
  }

  @Transactional()
  @Get('courses/:courseId/lectures')
  @ApiOperation({ summary: 'List all lectures for a course' })
  @ApiResponse({
    status: 200,
    type: [Lecture],
  })
  async findAllForCourse(
    @Req() req: Request,
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Permissions() perms: Permission[],
  ) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.lecturesService.findAll(courseId, undefined, includeHidden, useCache(req));
    if (!result.length) {
      throw new NotFoundException('ERRORS.LECTURES_NOT_FOUND');
    }
    return result;
  }

  @Transactional()
  @Post('/lectures')
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new lecture' })
  @ApiResponse({
    status: 201,
    description: 'Created lecture',
    type: Lecture,
  })
  @PgConstraints()
  create(@Body() createLectureDto: CreateLectureDto) {
    return this.lecturesService.create(createLectureDto);
  }

  @RequirePermissions(Permission.COURSE_VIEW)
  @Get('modules/:id/lectures-for-admin-panel')
  @ApiOperation({ summary: 'Get lectures by Module id with active deep checking' })
  @ApiResponse({
    status: 200,
    description: 'Get a Module by id',
    type: Lecture,
  })
  async findForAdminPanel(@Param('id', new ParseUUIDPipe()) id: string) {
    return await this.lecturesService.findForAdminPanel(id);
  }

  @Transactional()
  @Get('lectures')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all lectures' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'moduleId',
    description: 'Filter by module',
    required: false,
  })
  @ApiResponse({
    status: 200,
    type: [Lecture],
  })
  findAll(@Req() req: Request, @Query() query: QueryLectureDto) {
    return this.lecturesService.findAll(query.courseId, query.moduleId, true, useCache(req));
  }

  @Get('modules/:moduleId/withContentRelevance')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all lectures with checked contents' })
  @ApiResponse({
    status: 200,
    type: [Lecture],
  })
  findAllWithContentRelevance(@Param('moduleId') moduleId: string) {
    return this.lecturesService.findAllWithContentRelevance(moduleId);
  }

  @Transactional()
  @Get('lectures/:id')
  @ApiOperation({ summary: 'Get one Lecture by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a Lecture by id',
    type: Lecture,
  })
  async findOne(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Permissions() perms: Permission[],
    @Req() req: Request,
    @Query('includeGlobalAssessment', new DefaultValuePipe(false), new ParseBoolPipe())
    includeGlobalAssessment?: boolean,
  ) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.lecturesService.findOne(id, includeHidden, false, includeGlobalAssessment, useCache(req));
    if (!result) {
      throw new NotFoundException('ERRORS.LECTURE_NOT_FOUND');
    }
    return this.clearAnswers(result, req.user);
  }

  private clearAnswers(result, user) {
    if (user.isAdmin() || user.isSuperadmin() || user.hasPermission(Permission.COURSE_EDIT)) return result;

    for (const assessment of result.assessments) {
      for (const question of assessment?.questions || []) {
        for (const answer of (<any>question.data)?.answers || []) {
          delete answer.isCorrect;
        }
      }
    }

    return result;
  }

  @Transactional()
  @Patch('lectures/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a Lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateLectureDto: UpdateLectureDto) {
    return this.lecturesService.update(id, updateLectureDto);
  }

  @Transactional()
  @Put('lectures/:id/move')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Move lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  move(@Param('id', new ParseUUIDPipe()) lectureId: string, @Body() moveLectureDto: MoveLectureDto) {
    return this.lecturesService.move(lectureId, moveLectureDto.toModuleId);
  }

  @Transactional()
  @Post('lectures/:lectureId/copy')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Copy lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns copied lecture',
  })
  copy(@Param('lectureId', new ParseUUIDPipe()) lectureId: string, @Req() req: Request, @Body() copyLectureDto: CopyLectureDto) {
    return this.lecturesService.copy(lectureId, {
      toModuleId: copyLectureDto.toModuleId,
      toCourseId: copyLectureDto.toCourseId,
      userId: req.user.id,
      flatCopy: copyLectureDto.flatCopy,
      analogs: copyLectureDto.analogs,
    });
  }

  @Transactional()
  @Put('lectures/:id/publish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Publish a Lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  publish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.lecturesService.publish(id);
  }

  @Transactional()
  @Put('lectures/:id/unpublish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Unpublish a Lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  unpublish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.lecturesService.unpublish(id);
  }

  @Transactional()
  @Delete('lectures/:id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Deletes a Lecture' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.lecturesService.remove(id);
  }

  @Patch('lecture/:lectureId/status')
  @ApiOperation({ summary: 'Sets lecture status' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async setLectureStatus(
    @Param('lectureId', new ParseUUIDPipe()) lectureId: string,
    @Body() dto: SetLectureStatusDto,
    @Req() req: Request,
  ) {
    const { user } = req;

    if (!this.lecturesService.checkRightsOrFreeId(lectureId, user)) {
      throw new UnauthorizedException('Student is not enrolled to course of this lecture');
    }

    const result = await this.lecturesService.setLectureStatus(user.id, lectureId, dto.status);

    const _lecture = await this.lecturesService.findOne(lectureId);
    await this.enrollmentsHelper.setGraduatedDate(user.id, _lecture.course.id);

    return result;
  }

  @Get('lecture/:lectureId/analogs')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Return all analogs of lecture' })
  async analogs(@Param('lectureId', new ParseUUIDPipe()) lectureId: string) {
    return this.lecturesService.findAnalogs(lectureId);
  }

  @Patch('lecture/:firstLectureId/bind/:secondLectureId')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Bind 2 lecture' })
  async bind(
    @Param('firstLectureId', new ParseUUIDPipe()) firstLectureId: string,
    @Param('secondLectureId', new ParseUUIDPipe()) secondLectureId: string,
  ) {
    return this.lecturesService.bindLectures(firstLectureId, secondLectureId);
  }

  @Get('lecture/:lectureId/relations-to-delete')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'Get lecture relations count' })
  async getRelationForDelete(@Param('lectureId', new ParseUUIDPipe()) lectureId: string) {
    return this.lecturesService.getRelationForDelete(lectureId);
  }

  @Transactional()
  @Delete('lecture/:lectureId/delete-soft-with-attached-content')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Delete lecture with attached content' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async deleteSoftWithAttachedContent(@Param('lectureId', new ParseUUIDPipe()) lectureId: string) {
    return this.lecturesService.deleteSoftWithAttachedContent(lectureId);
  }

  @Patch('/lecture-progress/:userId/:lectureId')
  @RequirePermissions(Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Fix lecture progress status' })
  async updateLectureProgress(
    @Param('lectureId', new ParseUUIDPipe()) lectureId: string,
    @Param('userId', new ParseUUIDPipe()) userId: string,
    @Body() dto: UpdateLectureProgressDto,
  ) {
    return this.lecturesService.updateLectureProgress(lectureId, userId, dto.status);
  }

  @Delete('/lecture-progress/:lectureId')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Delete lecture progress (soft delete mode)' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async deleteLectureProgress(@Param('lectureId', new ParseUUIDPipe()) lectureId: string, @Req() req: Request) {
    if (!req.user.isAdmin() && !req.user.isSuperadmin()) {
      throw new UnauthorizedException();
    }

    return this.lecturesService.deleteLectureProgress(lectureId);
  }
}
