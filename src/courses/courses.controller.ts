import {
  Controller,
  Get,
  Post,
  Put,
  Body,
  Patch,
  Param,
  Query,
  Delete,
  ParseUUIDPipe,
  ParseBoolPipe,
  DefaultValuePipe,
  Req,
  NotFoundException,
  ForbiddenException,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiQuery, ApiBody, ApiBearerAuth } from '@nestjs/swagger';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { PgConstraints } from '../pg.decorators';
import { Course, ExtendedCourse } from '../lib/entities/course.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { Tag } from 'src/lib/entities/tag.entity';
import { AttachCourseTagsDto } from './dto/attach-course-tags.dto';
import { Request } from 'express';
import { Permissions } from 'src/permissions.decorator';
import { Interapp } from 'src/auth/interapp.decorator';
import { SyncCourseWithCmsDto } from './dto/sync-course-with-cms.dto';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { ViewUserProgressCount } from 'src/lib/views/view-user-progress-count.entity';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { Pagination } from 'src/pagination.decorator';
import { QueryCourseDto } from './dto/query-course.dto';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentsHelper } from '@utils/enrollments.helper';

@ApiTags('courses')
@ApiBearerAuth()
@Controller('courses')
export class CoursesController {
  constructor(
    private readonly coursesService: CoursesService,
    private readonly enrollmentsService: EnrollmentsService,
    private readonly enrollmentsHelper: EnrollmentsHelper,
  ) {}

  @Post()
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new course' })
  @ApiResponse({
    status: 201,
    description: 'Created course',
    type: ExtendedCourse,
  })
  @PgConstraints()
  create(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesService.create(createCourseDto);
  }

  @Get()
  @Interapp()
  @ApiOperation({ summary: 'List all courses' })
  @ApiQuery({
    name: 'includeModules',
    enum: ['true', 'false'],
    description: 'Include modules list',
    required: false,
  })
  @ApiQuery({
    name: 'includeLectures',
    enum: ['true', 'false'],
    description: 'Include modules and lectures',
    required: false,
  })
  @ApiQuery({
    name: 'includeCounters',
    enum: ['true', 'false'],
    description: 'Include counters',
    required: false,
  })
  @ApiQuery({
    name: 'tagTitle',
    description: 'Filter by tag title',
    required: false,
  })
  @ApiQuery({
    name: 'ids',
    description: 'Filter by course ids (array)',
    required: false,
    isArray: true,
  })
  @ApiResponse({
    status: 200,
    type: [ExtendedCourse],
  })
  findAll(
    @Req() req: Request,
    @Permissions() perms: Permission[],
    @Query('includeModules', new DefaultValuePipe(false), new ParseBoolPipe())
    includeModules?: boolean,
    @Query('includeLectures', new DefaultValuePipe(false), new ParseBoolPipe())
    includeLectures?: boolean,
    @Query('includeCounters', new DefaultValuePipe(false), new ParseBoolPipe())
    includeCounters?: boolean,
    @Query('tagTitle')
    tagTitle?: string,
    @Query('ids')
    ids?: string[],
  ) {
    const userId = req.user?.id;
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    return this.coursesService.findAll(includeModules, includeLectures, tagTitle, userId, includeHidden, ids, includeCounters);
  }

  @Get('/query')
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @Interapp()
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List courses pagination' })
  @ApiQuery({
    name: 'includeModules',
    enum: ['true', 'false'],
    description: 'Include modules list',
    required: false,
  })
  @ApiQuery({
    name: 'includeLectures',
    enum: ['true', 'false'],
    description: 'Include modules and lectures',
    required: false,
  })
  @ApiQuery({
    name: 'includeCounters',
    enum: ['true', 'false'],
    description: 'Include counters',
    required: false,
  })
  @ApiQuery({
    name: 'tagTitle',
    description: 'Filter by tag title',
    required: false,
  })
  @ApiQuery({
    name: 'ids',
    description: 'Filter by course ids (array)',
    required: false,
    isArray: true,
  })
  @ApiResponse({
    status: 200,
    type: [ExtendedCourse],
  })
  @ApiPaginatedResponse(Course)
  query(@Req() req: Request, @Permissions() perms: Permission[], @Query() query: QueryCourseDto, @Pagination() paging: PaginationDto) {
    const userId = req.user?.id;
    const includeHidden = perms.includes(Permission.COURSE_VIEW);

    return this.coursesService.query(userId, includeHidden, query, paging);
  }

  @Get('titles')
  @ApiOperation({ summary: 'Get full LMS courses list' })
  @ApiResponse({
    status: 200,
    description: 'Courses list',
    type: [Course],
  })
  @ApiQuery({
    name: 'filterByTutorUserId',
    description: 'Filter by enrollment where user is tutor',
    required: false,
  })
  findCoursesTitles(
    @Query('filterByTutorUserId')
    filterByTutorUserId?: string,
  ) {
    return this.coursesService.findCoursesTitles(filterByTutorUserId);
  }

  @Get('cms')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'Get full courses list from CMS' })
  @ApiResponse({
    status: 200,
    description: 'CMS courses list',
    type: [Course],
  })
  fetchAllCoursesFromCMS() {
    return this.coursesService.findAllCoursesFromCMS();
  }

  @Patch(':courseId/sync-with-cms')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Sync course data with a course from CMS' })
  @ApiResponse({
    status: 200,
    description: 'Updated course',
    type: Course,
  })
  SyncCourseWithCMS(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Body() dto: SyncCourseWithCmsDto) {
    return this.coursesService.syncCourseWithCMS(courseId, dto);
  }

  @Get(':courseId/progress')
  @ApiOperation({ summary: 'Get current user progress counts for course' })
  @ApiResponse({
    status: 200,
    description: 'Progress counters',
    type: ViewUserProgressCount,
  })
  async getUserCourseProgress(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Req() req: Request) {
    const userId = req.user?.id;
    if (!userId) {
      throw new NotFoundException('User not found');
    }

    await this.enrollmentsHelper.setGraduatedDate(userId, courseId);
    const courseProgress = await this.coursesService.viewUserProgressCount(userId, courseId);
    const progress = courseProgress[0];

    return progress || {};
  }

  @Get('progress')
  @ApiOperation({ summary: 'Get current user progress counts for course' })
  @ApiQuery({
    name: 'userId',
    description: 'User to get progress for',
    required: true,
    isArray: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Progress counters',
    type: ViewUserProgressCount,
  })
  @RequirePermissions(Permission.USERS_VIEW)
  async getCoursesProgress(@Query('userId', new ParseUUIDPipe()) userId: string) {
    return await this.coursesService.viewUserProgressCount(userId);
  }

  @Get(':id')
  @Interapp()
  @ApiOperation({ summary: 'Get one course by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a course by id',
    type: ExtendedCourse,
  })
  async findOne(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Req() req: Request,
    @Permissions() perms: Permission[],
    @Query('showTags')
    showTags?: boolean,
  ) {
    const { user } = req;
    const userId = user?.id;
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const course = await this.coursesService.findOne(id, {
      userId,
      includeHidden,
      showTags,
    });

    if (!course) {
      throw new NotFoundException('ERRORS.COURSE_NOT_FOUND');
    }

    if (!(await this.coursesService.checkCourseRights(course, user))) {
      throw new ForbiddenException('ERRORS.COURSE_ACCESS_FORBIDDEN');
    }

    return course;
  }

  @Post('/:courseId/fix-order')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Fix lectures and modules order' })
  @ApiResponse({
    status: 200,
    description: 'Set the number of lectures and modules',
  })
  async fixOrder(@Param('courseId', new ParseUUIDPipe()) courseId: string) {
    const course = await this.coursesService.findOne(courseId, {
      includeHidden: true,
    });
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    return this.coursesService.fixOrder(course);
  }

  @Patch(':id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a course' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateCourseDto: UpdateCourseDto) {
    const result = await this.coursesService.update(id, updateCourseDto);

    if (updateCourseDto.hasOwnProperty('isSatellite')) {
      const enrollments = await this.enrollmentsService.findAllByCourseId(id);
      for (const enrollment of enrollments) {
        if (enrollment.role === CourseRole.Student) {
          this.enrollmentsService.syncSatellitesForStudent(enrollment.userId);
        }
      }
    }

    return result;
  }

  @Put(':id/publish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Publish a course' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  publish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.coursesService.publish(id);
  }

  @Put(':id/unpublish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Unpublish a course' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  unpublish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.coursesService.unpublish(id);
  }

  @Delete(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Deletes a course' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.coursesService.remove(id);
  }

  @Post(':courseId/tag/:tagId')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({
    summary: 'Attach tag to course',
  })
  @ApiResponse({
    status: 200,
    type: Tag,
    description: 'Attached tag to course',
  })
  attachTag(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Param('tagId', new ParseUUIDPipe()) tagId: string) {
    return this.coursesService.attachTag(courseId, tagId);
  }

  @Delete(':courseId/tag/:tagId')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({
    summary: 'Detach tag from course',
  })
  @ApiResponse({
    status: 200,
    type: [Tag],
    description: 'List of tags that remains in course',
  })
  detachTag(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Param('tagId', new ParseUUIDPipe()) tagId: string) {
    return this.coursesService.detachTag(courseId, tagId);
  }

  @Put(':id/tags')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({
    summary: 'Attach list of tags to course',
  })
  @ApiBody({ type: AttachCourseTagsDto })
  @ApiResponse({
    status: 200,
    type: [Tag],
    description: 'Attached list of tags to course',
  })
  attachTags(@Param('id', new ParseUUIDPipe()) id: string, @Body() dto: AttachCourseTagsDto) {
    return this.coursesService.attachTags(id, dto);
  }

  @Patch(':id/contents/sync-kaltura')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Update kaltura cchunks data for whole course' })
  async SyncKalturaCChunks(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.coursesService.SyncKalturaCChunks(id);
  }

  @Post('sync-cms')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Sync CMS IDs for courses' })
  async SyncCMSIds() {
    return this.coursesService.SyncCMSIds();
  }

  @Get('cms/:id')
  @Interapp()
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'Get a course by CMS Id' })
  @ApiQuery({
    name: 'distributeSupport',
    enum: ['true', 'false'],
    description: 'Distribute support users (return sorted by lastAccessed and updates this field)',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Get a course by CMS Id',
    type: Course,
  })
  findByCmsId(
    @Param('id') id: string,
    @Query('distributeSupport', new DefaultValuePipe(false), new ParseBoolPipe())
    distributeSupport?: boolean,
  ) {
    return this.coursesService.findByCMSId(id, distributeSupport);
  }

  @Get('external/:id')
  @Interapp()
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'Get a course by CMS Id' })
  @ApiQuery({
    name: 'distributeSupport',
    enum: ['true', 'false'],
    description: 'Distribute support users (return sorted by lastAccessed and updates this field)',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Get a course by CMS Id',
    type: Course,
  })
  findByExternalId(
    @Param('id') id: string,
    @Query('distributeSupport', new DefaultValuePipe(false), new ParseBoolPipe())
    distributeSupport?: boolean,
  ) {
    return this.coursesService.findByExternalId(id, distributeSupport);
  }

  @Post('/:courseId/satellite/:satelliteCourseId')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Attach satellite to course' })
  @ApiResponse({
    status: 200,
    description: 'Attach satellite to course',
  })
  async attachSatellite(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Param('satelliteCourseId', new ParseUUIDPipe()) satelliteCourseId: string,
  ) {
    const course = await this.coursesService.findOne(courseId, { includeHidden: true });
    const satellite = await this.coursesService.findOne(satelliteCourseId, { includeHidden: true });
    if (!course || !satellite) {
      throw new NotFoundException('Course not found');
    }
    if (!satellite.hasOwnProperty('isSatellite')) {
      throw new NotFoundException('Course is not available, please check its parameters');
    }
    if (!satellite.isSatellite) {
      throw new NotFoundException('Course has not satellite state');
    }
    const result = await this.coursesService.attachSatelliteToCourse(course, satellite);
    const enrollments = await this.enrollmentsService.findAllByCourseId(course.id);
    for (const enrollment of enrollments) {
      if (enrollment.role === CourseRole.Student) {
        this.enrollmentsService.syncSatellitesForStudent(enrollment.userId);
      }
    }

    return result;
  }

  @Delete('/:courseId/satellite/:satelliteCourseId')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Detach satellite from course' })
  @ApiResponse({
    status: 200,
    description: 'Detach satellite from course',
  })
  async detachSatellite(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Param('satelliteCourseId', new ParseUUIDPipe()) satelliteCourseId: string,
  ) {
    const course = await this.coursesService.findOne(courseId, { includeHidden: true });
    const satellite = await this.coursesService.findOne(satelliteCourseId, { includeHidden: true });
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    if (!satellite) {
      throw new NotFoundException('Satellite not found');
    }

    const result = await this.coursesService.detachSatelliteFromCourse(course, satellite);

    const enrollments = await this.enrollmentsService.findAllByCourseId(course.id);
    for (const enrollment of enrollments) {
      if (enrollment.role === CourseRole.Student) {
        this.enrollmentsService.syncSatellitesForStudent(enrollment.userId);
      }
    }

    return result;
  }

  @Patch('/:courseId/satellite/:satelliteCourseId')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Update satellite' })
  @ApiResponse({
    status: 200,
    description: 'Attach satellite to course',
  })
  async updateSatellite(
    @Param('courseId', new ParseUUIDPipe()) courseId: string,
    @Param('satelliteCourseId', new ParseUUIDPipe()) satelliteCourseId: string,
    @Req() req: Request,
  ) {
    const course = await this.coursesService.findOne(courseId, { includeHidden: true });
    const satellite = await this.coursesService.findOne(satelliteCourseId, { includeHidden: true });
    if (!course || !satellite) {
      throw new NotFoundException('Course not found');
    }
    if (!satellite.hasOwnProperty('isSatellite')) {
      throw new NotFoundException('Course is not available, please check its parameters');
    }
    if (!satellite.isSatellite) {
      throw new NotFoundException('Course has not satellite state');
    }

    return await this.coursesService.updateSatellite(course, satellite, req.body.required);
  }

  @Get('utils/refresh-progress-view')
  @ApiOperation({ summary: 'Refresh progress view' })
  @ApiResponse({
    status: 200,
    description: 'Refresh progress view',
  })
  @RequirePermissions(Permission.ANALYTICS_VIEW)
  async refreshProgressView() {
    await this.coursesService.updateProgressMaterializedView();
    return { message: 'done' };
  }
}
