import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { In, Repository, getConnection } from 'typeorm';
import { Enrollment } from '../lib/entities/enrollment.entity';
import { User } from '../lib/entities/user.entity';
import { CreateEnrollmentDto } from './dto/create-enrollment.dto';
import { QueryEnrollmentDto } from './dto/query-enrollment.dto';
import { UpdateEnrollmentDto } from './dto/update-enrollment.dto';
import { CreateTagEnrollmentDto } from './dto/create-tag-enrollment.dto';
import { CoursesService } from './courses.service';
import { DeleteTagEnrollmentDto } from './dto/delete-tag-enrollment.dto';
import { EnrollmentTriggerService } from '../trigger/services/enrollment-trigger.service';
import { LecturesService } from './lectures.service';
import { EnrollmentRightsEnum, EnrollmentRightsType } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { lessThanOrNull } from 'src/lib/operators/less-than-or-null';
import { moreThanOrNull } from 'src/lib/operators/more-than-or-null';
import * as dayjs from 'dayjs';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateCMSEnrollmentDto } from './dto/create-cms-enrollment.dto';
import { UsersService } from 'src/users/users.service';
import { isEmptyObject } from 'src/helpers';
import config from 'config';

export const defaultEnrollmentRights: EnrollmentRightsType = {
  canWatchFreeLessons: true,
  canWatchAllLessons: true,
  canDownloadFiles: true,
  canReadForum: true,
  canWriteForum: true,
  canSubmitQuiz: true,
  canSubmitSurvey: true,
  canSubmitAssignment: true,
  canGraduate: true,
};

export const trialEnrollmentRights: EnrollmentRightsType = {
  ...defaultEnrollmentRights,
  canWatchAllLessons: false,
  canReadForum: false,
  canWriteForum: false,
  canSubmitAssignment: false,
  canGraduate: false,
};

@Injectable()
export class EnrollmentsService {
  constructor(
    @InjectRepository(Enrollment)
    private enrollmentsRepository: Repository<Enrollment>,
    @Inject(forwardRef(() => EnrollmentTriggerService))
    private readonly enrollmentTriggerService: EnrollmentTriggerService,
    @Inject(forwardRef(() => CoursesService))
    private readonly coursesService: CoursesService,
    @Inject(forwardRef(() => LecturesService))
    private readonly lectureService: LecturesService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
  ) {}

  prepareEnrollmentRigths(enrollmentRole: CourseRole, enrollmentType: EnrollmentType, rights?: EnrollmentRightsType): EnrollmentRightsType {
    if (enrollmentRole !== CourseRole.Student) {
      return {};
    }
    if (!rights || isEmptyObject(rights)) {
      return enrollmentType === EnrollmentType.Trial ? trialEnrollmentRights : defaultEnrollmentRights;
    }
    return Object.keys(EnrollmentRightsEnum).reduce((a, e) => ((a[e] = rights[e] || false), a), {}) as EnrollmentRightsType;
  }

  async create(courseId: string, createEnrollmentDto: CreateEnrollmentDto) {
    const rights = this.prepareEnrollmentRigths(createEnrollmentDto.role, createEnrollmentDto.type, createEnrollmentDto.rights);

    let serviceEndsAt = null;
    if (createEnrollmentDto.role === CourseRole.Student) {
      const date_from = [
        dayjs(createEnrollmentDto.dateFrom).format('YYYY-MM-DDTHH:mm:ssZ').toString(),
        dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ssZ').toString(),
      ].reduce(function (a, b) {
        return a < b ? a : b;
      });

      if (createEnrollmentDto.type === EnrollmentType.Satellite) {
        serviceEndsAt = dayjs(date_from).add(config.defaultSatelliteCourseSupportDuration, 'day').toDate();
      } else if (createEnrollmentDto.type === EnrollmentType.Trial) {
        if (createEnrollmentDto.dateTo) serviceEndsAt = createEnrollmentDto.dateTo;
        else {
          serviceEndsAt = createEnrollmentDto.dateTo = dayjs().add(config.defaultTrialCourseSupportDuration, 'day').toDate();
        }
      } else {
        serviceEndsAt = dayjs(date_from).add(config.defaultCourseSupportDuration, 'day').toDate();
      }
    }

    const enrollment = await this.enrollmentsRepository.save({
      ...createEnrollmentDto,
      serviceEndsAt,
      course: { id: courseId },
      user: { id: createEnrollmentDto.userId },
      rights,
    });

    // Background job
    this.enrollmentTriggerService.enrollmentCreated(courseId, enrollment, createEnrollmentDto.sendEmail);

    if (this.needToSyncSatellites(enrollment)) {
      // Background job
      this.syncSatellitesForStudent(createEnrollmentDto.userId);
    }

    if (enrollment.role == CourseRole.Student) {
      // Background job
      this.lectureService.syncProgressForNewCourse(createEnrollmentDto.userId, courseId);
    }

    return enrollment;
  }

  async createCMS(createCMSEnrollmentDto: CreateCMSEnrollmentDto) {
    // find course id
    const course = await this.coursesService.findByCMSId(createCMSEnrollmentDto.courseId);

    if (!course) {
      throw new NotFoundException('ENROLLMENTS.ENROLL_BY_CMS.COURSE_NOT_FOUND');
    }

    let sendEmail = true;
    if (createCMSEnrollmentDto.sendEmail !== undefined) {
      sendEmail = createCMSEnrollmentDto.sendEmail;
    }

    const user = await this.usersService.createCMS(createCMSEnrollmentDto);

    const type = createCMSEnrollmentDto.type ? createCMSEnrollmentDto.type : EnrollmentType.Buy;

    const rights = this.prepareEnrollmentRigths(CourseRole.Student, type);

    // enroll
    const createEnrollmentDto: CreateEnrollmentDto = {
      role: CourseRole.Student,
      userId: user.id,
      dateTo: createCMSEnrollmentDto.dateTo,
      reason: { message: createCMSEnrollmentDto.reason, authorId: null },
      externalId: createCMSEnrollmentDto.eadboxEnrollmentId,
      sendEmail: sendEmail,
      rights,
      type,
      options: createCMSEnrollmentDto.options,
    };
    if (createCMSEnrollmentDto.dateFrom) {
      createEnrollmentDto.dateFrom = createCMSEnrollmentDto.dateFrom;
    }

    return this.create(course.id, createEnrollmentDto);
  }

  async findAll(userId: string) {
    return await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.course', 'course')
      .where('enrollment.user.id = :userId', { userId })
      .getMany();
  }

  findAllByCourseId(courseId: string): Promise<Enrollment[]> {
    return this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.course', 'course')
      .where('enrollment.courseId = :courseId', { courseId })
      .getMany();
  }

  findSupportByCourseId(courseId: string) {
    return this.enrollmentsRepository
      .createQueryBuilder('enrollments')
      .leftJoinAndSelect('enrollments.user', 'user')
      .andWhere('enrollments.courseId = :courseId', { courseId })
      .andWhere('enrollments.role = :role', { role: CourseRole.Support })
      .andWhere('user.active = TRUE')
      .andWhere(
        'enrollments.active = TRUE' +
          ' AND (enrollments.dateFrom IS NULL OR enrollments.dateFrom <= NOW())' +
          ' AND (enrollments.dateTo IS NULL OR enrollments.dateTo > NOW())',
      )
      .orderBy('user.lastAccessed', 'ASC', 'NULLS FIRST')
      .getMany();
  }

  updateSupportAccessDate(userId: string) {
    return this.usersService.updateAccessDate(userId);
  }

  async findSatellites(userId: string) {
    return await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.course', 'course')
      .where('enrollment.user.id = :userId', { userId })
      .andWhere('enrollment.type = :type', { type: EnrollmentType.Satellite })
      .getMany();
  }

  async query(query: QueryEnrollmentDto, paging?: PaginationDto): Promise<Paginated<Enrollment[]>> {
    let queryBuilder = this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .innerJoinAndSelect('enrollment.course', 'course')
      .leftJoinAndSelect('course.image', 'files')
      .innerJoinAndSelect('enrollment.user', 'user')
      .leftJoinAndSelect('user.tags', 'tags')
      .addSelect(`u2.name`)
      .leftJoin(User, 'u2', `u2.id = uuid(enrollment.reason->>'authorId')`);
    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('course.id = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.userId) {
      queryBuilder = queryBuilder.andWhere('user.id = :userId', {
        userId: query.userId,
      });
    }
    if (query.exactEmail) {
      queryBuilder = queryBuilder.andWhere('user.email = :exactEmail', {
        exactEmail: query.exactEmail,
      });
    }
    if (query.externalId) {
      queryBuilder = queryBuilder.andWhere('enrollment.external_id = :externalId', {
        externalId: query.externalId,
      });
    }
    if (query.externalIds) {
      queryBuilder = queryBuilder.andWhere('enrollment.external_id = ANY (:externalIds)', {
        externalIds: query.externalIds,
      });
    }
    if (query.role) {
      queryBuilder = queryBuilder.andWhere('role = :role', {
        role: query.role,
      });
    }
    if (!query.showInactive) {
      queryBuilder = queryBuilder.andWhere('enrollment.active = TRUE');
      queryBuilder = queryBuilder.andWhere('date_from <= :dateFrom', {
        dateFrom: new Date(),
      });
      queryBuilder = queryBuilder.andWhere('(date_to IS NULL OR date_to > :dateTo)', {
        dateTo: new Date(),
      });
    }
    if (query.sortField) {
      switch (query.sortField) {
        case 'author':
          queryBuilder.orderBy(`u2.name`, query.sortDirection);
          break;

        case 'student_name':
          queryBuilder.orderBy(`user.name`, query.sortDirection);
          break;

        case 'course':
          queryBuilder.orderBy(`course.title`, query.sortDirection);
          break;

        default:
          queryBuilder.orderBy(`enrollment.${query.sortField}`, query.sortDirection);
          break;
      }
    } else {
      queryBuilder.orderBy(`enrollment.created`, query.sortDirection);
    }

    if (paging && !paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (
      query.createdFrom &&
      query.createdTo &&
      dayjs(query.createdTo, 'YYYY/MM/DD').isValid() &&
      dayjs(query.createdFrom, 'YYYY/MM/DD').isValid()
    ) {
      queryBuilder = queryBuilder.andWhere(`enrollment.created >= :createdDateFrom AND enrollment.created <= :createdDateTo`, {
        createdDateFrom: query.createdFrom,
        createdDateTo: query.createdTo,
      });
    }

    if (query.createdFrom && !query.createdTo && dayjs(query.createdFrom, 'YYYY/MM/DD').isValid()) {
      queryBuilder = queryBuilder.andWhere(`enrollment.created >= :createdDateFrom`, {
        createdDateFrom: query.createdFrom,
      });
    }

    if (!query.createdFrom && query.createdTo && dayjs(query.createdTo, 'YYYY/MM/DD').isValid()) {
      queryBuilder = queryBuilder.andWhere(`enrollment.created <= :createdDateTo`, {
        createdDateTo: query.createdTo,
      });
    }

    if (query.authorName) {
      queryBuilder = queryBuilder.andWhere(`u2.name ilike :authorName`, {
        authorName: query.authorName,
      });
    }

    if (query.anyAuthor) {
      queryBuilder = queryBuilder.andWhere(`u2.name IS NOT NULL`);
    }

    if (query.authorsExept) {
      queryBuilder = queryBuilder.andWhere(`u2.id != ALL(:authorsExept)`, {
        authorsExept: query.authorsExept,
      });
    }

    if (query.types) {
      queryBuilder = queryBuilder.andWhere(`enrollment.type = ANY(:types)`, {
        types: query.types,
      });
    }

    if (query.active) {
      queryBuilder = queryBuilder.andWhere(`enrollment.active = :active`, {
        active: query.active === 'active',
      });
    }

    if (query.search) {
      queryBuilder = queryBuilder.andWhere(
        `user.name ilike :search
        OR user.email ilike :search
        OR cast(enrollment.reason->>'message' as varchar) ilike :search
        OR cast(user.id as varchar) ilike :search
        OR cast(enrollment.id as varchar) ilike :search`,
        {
          search: query.search,
        },
      );
    }

    if (query.tagIds?.length) {
      queryBuilder.andWhere('tags.id in (:...tagIds)', {
        tagIds: query.tagIds,
      });
    }

    const { entities: results, raw } = await queryBuilder.getRawAndEntities();
    const total = await queryBuilder.getCount();

    return {
      results,
      meta: { ...paging, total },
      raw: raw.map(rawEnrollment => ({
        ...rawEnrollment,
        rawCreated: dayjs(rawEnrollment.enrollment_created).format('YYYY-MM-DD HH:mm'),
        isActiveByDate: dayjs().isBetween(rawEnrollment.enrollment_date_from, rawEnrollment.enrollment_date_to, null, '[]'),
      })),
    };
  }

  @Transactional()
  findOne(id: string): Promise<Enrollment> {
    return this.enrollmentsRepository.findOne(id, { relations: ['user', 'course', 'course.toSatelliteCourses'] });
  }

  findRecentByStudentAndCourse(userId: string, courseId: string): Promise<Enrollment> {
    return this.enrollmentsRepository.findOne({
      where: {
        userId: userId,
        courseId: courseId,
        role: CourseRole.Student,
        active: true,
      },
      order: { created: 'DESC' },
      relations: ['user', 'course', 'course.toSatelliteCourses'],
    });
  }

  async findOneByStudentAndCourse(userId: string, courseId: string): Promise<Enrollment> {
    return await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.course', 'course')
      .where('enrollment.user.id = :userId', { userId })
      .andWhere('enrollment.course.id = :courseId', { courseId })
      .andWhere('enrollment.role = :role', { role: CourseRole.Student })
      .andWhere('enrollment.type != :type', { type: EnrollmentType.Trial })
      .andWhere('enrollment.active = TRUE')
      .getOne();
  }

  async findByStudentAndCourse(userId: string, courseId: string): Promise<Enrollment[]> {
    return await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.course', 'course')
      .where('enrollment.user.id = :userId', { userId })
      .andWhere('enrollment.course.id = :courseId', { courseId })
      .andWhere('enrollment.role = :role', { role: CourseRole.Student })
      .andWhere('enrollment.type != :type', { type: EnrollmentType.Trial })
      .andWhere('enrollment.active = TRUE')
      .getMany();
  }

  async countOfGraduatedEnrollmentsByStudentAndCourses(userId: string, coursesId: string[]): Promise<number> {
    return await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .where('enrollment.graduatedAt IS NOT NULL')
      .andWhere('enrollment.userId = :userId', { userId })
      .andWhere('enrollment.courseId IN(:...coursesId)', { coursesId })
      .andWhere('enrollment.role = :role', { role: CourseRole.Student })
      .andWhere('enrollment.type = :type', { type: EnrollmentType.Satellite })
      .getCount();
  }

  findByExternalId(externalId: string): Promise<Enrollment> {
    return this.enrollmentsRepository.findOne({ externalId }, { relations: ['user', 'course', 'course.toSatelliteCourses'] });
  }

  @Transactional()
  async update(id: string, updateEnrollmentDto: UpdateEnrollmentDto) {
    const updateFields = { ...updateEnrollmentDto, id };
    this.enrollmentTriggerService.userEnrollUpdate(updateFields);
    const result = await this.enrollmentsRepository.save(updateFields);

    const enrollment = await this.findOne(id);
    if (enrollment.type != EnrollmentType.Satellite && enrollment.course.toSatelliteCourses.length) {
      await this.syncSatellitesForStudent(enrollment.userId);
    }

    return result;
  }

  async activate(id: string) {
    return this.update(id, { active: true });
  }

  async deactivate(id: string) {
    return this.update(id, { active: false });
  }

  async remove(id: string) {
    const enrollment = await this.enrollmentsRepository.findOne(id, {
      relations: ['user', 'course', 'course.toSatelliteCourses'],
    });
    if (!enrollment) {
      return;
    }
    this.enrollmentTriggerService.userEnrollDelete(enrollment);
    const result = await this.enrollmentsRepository.delete(id);

    if (enrollment.type != EnrollmentType.Satellite && enrollment.course.toSatelliteCourses.length) {
      await this.syncSatellitesForStudent(enrollment.userId);
    }

    return result;
  }

  async createByTag(createTagEnrollmentDto: CreateTagEnrollmentDto, authorId: string) {
    // find courses by tag id
    const courseIds = await this.coursesService.findIdsByTagId(createTagEnrollmentDto.tagId);
    if (courseIds.length === 0) {
      throw new NotFoundException('Courses not found');
    }

    let sendEmail = false;
    if (createTagEnrollmentDto.sendEmail !== undefined) {
      sendEmail = createTagEnrollmentDto.sendEmail;
    }
    const createEnrollmentDto: CreateEnrollmentDto = {
      role: createTagEnrollmentDto.role,
      type: createTagEnrollmentDto.type,
      userId: createTagEnrollmentDto.userId,
      dateTo: createTagEnrollmentDto.dateTo,
      dateFrom: createTagEnrollmentDto.dateFrom,
      reason: { message: createTagEnrollmentDto.reason, authorId: authorId },
      sendEmail: sendEmail,
      options: createTagEnrollmentDto.options,
    };

    // enroll
    const result = [];
    for (const course of courseIds) {
      const res = await this.create(course, createEnrollmentDto);
      result.push(res);
    }

    return result;
  }

  async disableByTag(deleteTagEnrollmentDto: DeleteTagEnrollmentDto) {
    // find courses by tag id
    const courseIds = await this.coursesService.findIdsByTagId(deleteTagEnrollmentDto.tagId);
    if (courseIds.length === 0) {
      throw new NotFoundException('Courses not found');
    }
    const queryBuilder = this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .innerJoinAndSelect('enrollment.course', 'course')
      .innerJoinAndSelect('enrollment.user', 'user')
      .andWhere('course.id = ANY (:courseIds)', {
        courseIds,
      })
      .andWhere('user.id = :userId', {
        userId: deleteTagEnrollmentDto.userId,
      })
      .andWhere('role = :role', {
        role: deleteTagEnrollmentDto.role,
      });
    const enrollments = await queryBuilder.getMany();

    for (const index in enrollments) {
      await this.enrollmentsRepository.update(enrollments[index].id, {
        active: false,
      });
    }

    return 'ok';
  }

  // This just returns all tutors from the courses of the teamlead
  async findTeamleadTutors(teamleadId): Promise<string[]> {
    const enrollments = await this.enrollmentsRepository.find({
      where: {
        userId: teamleadId,
        role: CourseRole.Teamlead,
        active: true,
        dateFrom: lessThanOrNull(new Date()),
        dateTo: moreThanOrNull(new Date()),
      },
      select: ['courseId'],
    });
    const courseIds = enrollments.map(e => e.courseId);

    const tutors = await this.enrollmentsRepository.find({
      join: {
        alias: 'enrollment',
        leftJoin: {
          user: 'enrollment.user',
        },
      },
      where: {
        role: CourseRole.Tutor,
        courseId: In(courseIds),
        active: true,
        dateFrom: lessThanOrNull(new Date()),
        dateTo: moreThanOrNull(new Date()),
        user: {
          active: true,
        },
      },
      select: ['userId'],
    });

    return tutors.map(t => t.userId);
  }

  purgeAll() {
    return this.enrollmentsRepository.clear();
  }

  async rawSave(enrollment) {
    return this.enrollmentsRepository.save(enrollment);
  }

  needToSyncSatellites(enrollment: Enrollment) {
    const availableEnrollmentTypes = [
      EnrollmentType.Buy,
      EnrollmentType.Inner,
      EnrollmentType.Present,
      EnrollmentType.Promo,
      EnrollmentType.Unknown,
    ];
    return enrollment.role == CourseRole.Student && availableEnrollmentTypes.includes(enrollment.type);
  }

  async syncSatellitesForStudent(userId: string) {
    const availableEnrollmentTypes = [
      EnrollmentType.Buy,
      EnrollmentType.Inner,
      EnrollmentType.Present,
      EnrollmentType.Promo,
      EnrollmentType.Unknown,
    ];

    function getEnrollmentsRelatedToSatellite(enrollments: Enrollment[], satellite: Enrollment) {
      return enrollments.filter(function (e) {
        if (!availableEnrollmentTypes.includes(e.type)) {
          return false;
        }
        const result = e.course.toSatelliteCourses.find(sc => sc.satelliteCourseId === satellite.course.id);

        return Boolean(result);
      });
    }

    function calculateStartDateForSatellite(enrollments: Enrollment[]) {
      let dateFrom = undefined;
      enrollments.map(function (e) {
        if (dateFrom === undefined || e.dateFrom === null || (dateFrom !== null && e.dateFrom < dateFrom)) {
          dateFrom = e.dateFrom;
        }
      });

      return dateFrom;
    }

    function calculateEndDateForSatellite(enrollments: Enrollment[]) {
      let dateTo = undefined;
      enrollments.map(function (e) {
        if (dateTo === undefined || e.dateTo === null || (dateTo !== null && e.dateTo > dateTo)) {
          dateTo = e.dateTo;
        }
      });

      return dateTo;
    }

    function hasSatellites(enrollment: Enrollment) {
      if (
        enrollment.course.isSatellite ||
        !enrollment.course.toSatelliteCourses.length ||
        !availableEnrollmentTypes.includes(enrollment.type)
      ) {
        return false;
      }
      return true;
    }

    const enrollments = await this.enrollmentsRepository.find({
      where: {
        userId,
        role: 'student',
      },
      relations: ['user', 'course', 'course.toSatelliteCourses'],
    });

    const satelliteEnrollments = enrollments.filter(e => e.type === EnrollmentType.Satellite);

    // Update/delite existed satellite enrollments
    for (const satelliteEnrollment of satelliteEnrollments) {
      const relatedEnrollments = getEnrollmentsRelatedToSatellite(enrollments, satelliteEnrollment);
      if (!relatedEnrollments?.length) {
        await this.remove(satelliteEnrollment.id);
        continue;
      }

      const activeRelatedEnrollments = relatedEnrollments.filter(e => e.active);
      satelliteEnrollment.active = Boolean(activeRelatedEnrollments?.length);
      satelliteEnrollment.dateFrom = satelliteEnrollment.active ? calculateStartDateForSatellite(activeRelatedEnrollments) : null;
      satelliteEnrollment.dateTo = satelliteEnrollment.active ? calculateEndDateForSatellite(activeRelatedEnrollments) : null;

      await this.update(satelliteEnrollment.id, satelliteEnrollment);
    }

    // Add new satellite enrollments
    for (const enrollment of enrollments) {
      if (!hasSatellites(enrollment)) {
        continue;
      }

      const coursesToEnroll = enrollment.course.toSatelliteCourses.filter(function (relation) {
        return !Boolean(enrollments.find(e => e.courseId === relation.satelliteCourseId));
      });

      for (const course of coursesToEnroll) {
        let serviceEndsAt = null;
        if (enrollment.role === CourseRole.Student) {
          const date_from = [
            dayjs(enrollment.dateFrom).format('YYYY-MM-DDTHH:mm:ssZ').toString(),
            dayjs(new Date()).format('YYYY-MM-DDTHH:mm:ssZ').toString(),
          ].reduce(function (a, b) {
            return a < b ? a : b;
          });
          serviceEndsAt = dayjs(date_from).add(config.defaultSatelliteCourseSupportDuration, 'day').toDate();
        }
        const createEnrollmentDto: CreateEnrollmentDto = {
          role: enrollment.role,
          userId: enrollment.userId,
          dateTo: enrollment.dateTo,
          dateFrom: enrollment.dateFrom,
          reason: enrollment.reason,
          options: enrollment.options,
          rights: enrollment.rights,
          active: enrollment.active,
          sendEmail: false,
          courseId: course.satelliteCourseId,
          type: EnrollmentType.Satellite,
          serviceEndsAt,
        };
        await this.enrollmentsRepository.save(createEnrollmentDto);
        // No special trigger required for satellite courses
        // this.enrollmentTriggerService.userEnrollCreate(course.satelliteCourseId, createEnrollmentDto);
      }
    }
  }

  async hasEnrollment(userId: string, courseId: string, role: CourseRole) {
    const result = await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .where('enrollment.user_id = :userId', { userId })
      .andWhere('enrollment.course_id = :courseId', { courseId })
      .andWhere('enrollment.role = :role', { role })
      .getOne();

    return Boolean(result);
  }

  async enrollPlanoDeCarreira() {
    console.log('Enrolling plano de carreira');
    const course_id = process.env.PLANO_DE_CARREIRA_ID;
    if (!course_id) {
      return;
    }
    try {
      const dto = {
        userId: '',
        courseId: course_id,
        role: CourseRole.Student,
        type: EnrollmentType.Present,
        dateFrom: '',
        active: true,
        sendEmail: true,
        reason: '{"message": "Plano de carreira for all the students", "authorId": null}',
        rights: '{"canGraduate": true, "canReadForum": true, "canSubmitQuiz": true, "canWriteForum": true, "canSubmitSurvey": true, "canDownloadFiles": true, "canWatchAllLessons": true, "canSubmitAssignment": true, "canWatchFreeLessons": true}',
      };

      const users = await getConnection().query(
        `
          SELECT DISTINCT ON (u.id) u.id as user_id, NOW() AS date_from 
          from "user" u
          inner join enrollment e on e.user_id = u.id and e.role = 'student' and e.active = true and e.type in ('buy', 'transfer', 'unknown')
          left join course c on e.course_id = c.id
          left join enrollment e2 on e2.user_id = u.id and e2.role = 'student' and e2.course_id = $1
          where c.not_for_sale = false and c.published and c.active and e2.id is null
          ORDER BY u.id;
        `,
        [course_id],
      );

      for (const user of users) {
        dto.userId = user.user_id;
        dto.dateFrom = user.date_from;
        // @ts-ignore - date_from from query is string, not compatible with Date
        await this.create(dto.courseId, dto);
      }
    } catch (e) {
      console.log(e);
    }
  }
}
