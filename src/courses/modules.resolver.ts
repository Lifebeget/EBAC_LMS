import { Resolver, Args, Query, ResolveField, Parent } from '@nestjs/graphql';
import { Permission } from '@enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { ModulesService } from './modules.service';
import { Module } from '@entities/module.entity';
import { LecturesService } from './lectures.service';

@Resolver(of => Module)
export class ModuleResolver {
  constructor(private readonly modulesService: ModulesService, private readonly lecturesService: LecturesService) {}

  @RequirePermissions(Permission.COURSE_VIEW)
  @Query(() => Module, { name: 'module' })
  async module(@Args('id') id: string): Promise<Module> {
    return await this.modulesService.findOneRaw(id);
  }

  @ResolveField()
  async lectures(@Parent() module: Module) {
    const { id } = module;
    return await this.lecturesService.findAllRaw(id);
  }
}
