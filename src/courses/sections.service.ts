import { forwardRef, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Section } from 'src/lib/entities/section.entity';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateSectionDto } from './dto/create-section.dto';
import { UpdateSectionDto } from './dto/update-section.dto';
import { ModulesService } from './modules.service';

@Injectable()
export class SectionsService {
  constructor(
    @InjectRepository(Section)
    private readonly sectionsRepository: Repository<Section>,

    @Inject(forwardRef(() => ModulesService))
    private readonly modulesService: ModulesService,
  ) {}

  create(courseId: string, createSectionDto: CreateSectionDto) {
    if (createSectionDto.published) {
      createSectionDto.publishedAt = new Date();
    }
    return this.sectionsRepository.save({
      ...createSectionDto,
      course: { id: courseId },
    });
  }

  findAllRaw(courseId) {
    return this.sectionsRepository.createQueryBuilder('section').andWhere('section.course.id = :courseId', { courseId }).getMany();
  }

  findAll(courseId: string, includeHidden?: boolean) {
    const qb = this.sectionsRepository.createQueryBuilder('section');

    if (includeHidden) {
      qb.leftJoin('section.course', 'course');
    } else {
      qb.innerJoin('section.course', 'course', 'course.active = true AND course.published = true');
      qb.andWhere('section.active = true');
      qb.andWhere('section.published = true');
    }
    qb.andWhere('course.id = :courseId', { courseId });
    qb.orderBy('section.sort', 'ASC');
    return qb.getMany();
  }

  @Transactional()
  findOne(id: string, includeHidden?: boolean): Promise<Section> {
    const qb = this.sectionsRepository.createQueryBuilder('section');

    if (includeHidden) {
      qb.leftJoinAndSelect('section.course', 'course');
    } else {
      qb.innerJoinAndSelect('section.course', 'course', 'course.active = true AND course.published = true');
      qb.andWhere('section.active = true');
      qb.andWhere('section.published = true');
    }
    qb.andWhere('section.id = :id', { id });
    return qb.getOne();
  }

  async update(id: string, updateSectionDto: UpdateSectionDto) {
    const updateFields = { ...updateSectionDto, id };
    if (updateFields.published === true) {
      const currentSection = await this.sectionsRepository.findOne(id);
      if (currentSection.published === false) {
        updateFields.publishedAt = new Date();
      }
    }
    return this.sectionsRepository.save(updateFields);
  }

  async move(id: string, courseId: string) {
    const currentSection = await this.sectionsRepository.findOne(id, {
      relations: ['modules'],
    });

    for (const module of currentSection.modules) {
      await this.modulesService.move(module.id, courseId);
    }

    return this.sectionsRepository.save({
      ...currentSection,
      course: { id: courseId },
    });
  }

  async publish(id: string) {
    const currentSection = await this.sectionsRepository.findOne(id);
    if (currentSection.published === false) {
      return this.sectionsRepository.save({
        id,
        published: true,
        publishedAt: new Date(),
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already published',
      };
    }
  }

  async unpublish(id: string) {
    const currentSection = await this.sectionsRepository.findOne(id);
    if (currentSection.published === true) {
      return this.sectionsRepository.save({
        id,
        published: false,
        publishedAt: null,
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already unpublished',
      };
    }
  }

  async remove(id: string) {
    const { courseId } = await this.sectionsRepository.findOne(id);
    const sectionsCountInTheCourse = await this.sectionsRepository.count({ courseId });

    if (sectionsCountInTheCourse === 1) {
      await this.modulesService.unbindModulesFromSection(id);
    }

    return await this.sectionsRepository.delete(id);
  }

  async removeList(sections: Section[]) {
    return this.sectionsRepository.remove(sections);
  }

  purgeAll() {
    return this.sectionsRepository.clear();
  }

  findModulesBySectionIds(ids: string[]) {
    return this.modulesService.findBySectionIds(ids);
  }

  async findForAdminPanel(courseId: string) {
    const qb = this.sectionsRepository
      .createQueryBuilder('section')
      .leftJoinAndSelect('section.course', 'course')
      .where('course.id = :courseId', { courseId })
      .orderBy('section.sort', 'ASC');

    const sections = await qb.getMany();
    const result = sections.map(el => ({
      id: el.id,
      title: el.title,
      isActive: el.active && el.published && el.course?.active && el.course?.published,
    }));
    return result;
  }
}
