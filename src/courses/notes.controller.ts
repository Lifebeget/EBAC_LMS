import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseUUIDPipe,
  Req,
  ForbiddenException,
  NotFoundException,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { CreateNoteDto } from './dto/create-note.dto';
import { Note } from '../lib/entities/note.entity';
import { UpdateNoteDto } from './dto/update-note.dto';
import { NotesService } from './notes.service';
import { Request } from 'express';
import { QueryNoteDto } from './dto/query-note.dto';
import { UsersService } from 'src/users/users.service';
import { LecturesService } from './lectures.service';

// #todo>
// - Why is courseId used where it is not required
// - This should be one of /user/* routes, not /courses. as it fetches user-relative information
@ApiTags('notes')
@ApiBearerAuth()
@Controller()
export class NotesController {
  constructor(private readonly notesService: NotesService, private userService: UsersService, private lectureService: LecturesService) {}

  isEnrollment(courseId: string, user: Express.User, onlyManagers = false) {
    return (
      user.enrolledAsSupport(courseId) ||
      (!onlyManagers && user.enrolledAsStudent(courseId)) ||
      user.enrolledAsTutor(courseId) ||
      user.enrolledAsTeamlead(courseId) ||
      user.enrolledAsModerator(courseId)
    );
  }

  private async checkTeamLeadCoursesAccess(courseId: string, user: Express.User) {
    if (!user.isTeamlead()) return false;

    const courses = (await this.userService.getCoursesForTutorsTeamlead(user.id)) || [];
    if (courses.filter(e => e === courseId).length) {
      return true;
    }
  }

  async isCourseAccessByRole(courseId: string, user: Express.User) {
    // эти роли любой курс
    if (user.isSuperadmin() || user.isAdmin() || user.isManager() || user.isObserver()) {
      return true;
    }

    // курсы подчиненных
    const teamleadCheck = await this.checkTeamLeadCoursesAccess(courseId, user);
    if (teamleadCheck) {
      return true;
    }
  }

  async isCourseAccess(courseId: string, user: Express.User, onlyManagers = false) {
    return this.isEnrollment(courseId, user, onlyManagers) || (await this.isCourseAccessByRole(courseId, user));
  }

  @Post('courses/:courseId/notes')
  @ApiOperation({ summary: 'Create a new note' })
  @ApiResponse({ status: 201, description: 'Created notee', type: Note })
  @PgConstraints()
  async create(@Req() req: Request, @Param('courseId') courseId, @Body() dto: CreateNoteDto) {
    // if (!dto.file && !dto.html) throw new BadRequestException('Empty note');
    if (!(await this.isCourseAccess(courseId, req.user))) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }
    return this.notesService.create(courseId, dto, req.user?.id);
  }

  @Get('courses/:courseId/notes')
  @ApiOperation({ summary: 'List all notes in course' })
  @ApiResponse({ status: 200, type: [Note] })
  findAllOwn(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Req() req: Request) {
    return this.notesService.findAllOwn(courseId, req.user?.id);
  }

  @Get('courses/:courseId/notes/:id')
  @ApiOperation({ summary: 'Get one note by id' })
  @ApiResponse({ status: 200, description: 'Get a note by id', type: Note })
  async findOneOwn(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const entity = await this.notesService.findOneOwn(id, req.user?.id);
    if (!entity) throw new NotFoundException('Not found');
    return entity;
  }

  @Patch('courses/:courseId/notes/:id')
  @ApiOperation({ summary: 'Updates a note' })
  @ApiResponse({ status: 200, description: 'Returns updated fields' })
  @PgConstraints()
  async updateOwn(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateNoteDto: UpdateNoteDto, @Req() req: Request) {
    await this.checkNote(id, req.user?.id);
    return this.notesService.update(id, updateNoteDto);
  }

  @Delete('courses/:courseId/notes/:id')
  @ApiOperation({ summary: 'Deletes a note' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async removeOwn(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkNote(id, req.user?.id);
    return this.notesService.remove(id);
  }

  @Post('courses/:courseId/notes/:id/restore')
  @ApiOperation({ summary: 'Restore a note' })
  @ApiResponse({
    status: 201,
    description: 'Returns restored note',
  })
  async restoreOwn(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkNote(id, req.user?.id);
    return this.notesService.restore(id);
  }

  /**
   * Admin routes
   */
  @Get('notes')
  @RequirePermissions(Permission.NOTES_MANAGE)
  @ApiOperation({ summary: 'List all notes of all users in course' })
  @ApiResponse({ status: 200, type: [Note] })
  findAll(@Query() query: QueryNoteDto) {
    return this.notesService.findAll(query);
  }

  @Get('notes/:id')
  @RequirePermissions(Permission.NOTES_MANAGE)
  @ApiOperation({ summary: 'Get one note by id' })
  @ApiResponse({ status: 200, description: 'Get a note by id', type: Note })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    const entity = await this.notesService.findOne(id);
    if (!entity) throw new NotFoundException('Not found');
    return entity;
  }

  @Patch('notes/:id')
  @RequirePermissions(Permission.NOTES_MANAGE)
  @ApiOperation({ summary: 'Updates a note' })
  @ApiResponse({ status: 200, description: 'Returns updated fields' })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateNoteDto: UpdateNoteDto) {
    await this.checkNote(id);
    return this.notesService.update(id, updateNoteDto);
  }

  @Delete('notes/:id')
  @RequirePermissions(Permission.NOTES_MANAGE)
  @ApiOperation({ summary: 'Deletes a note' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string) {
    await this.checkNote(id);
    return this.notesService.remove(id);
  }

  @Post('notes/:id/restore')
  @RequirePermissions(Permission.NOTES_MANAGE)
  @ApiOperation({ summary: 'Restore a note' })
  @ApiResponse({
    status: 201,
    description: 'Returns restored note',
  })
  async restore(@Param('id', new ParseUUIDPipe()) id: string) {
    await this.checkNote(id);
    return this.notesService.restore(id);
  }

  private async checkNote(id: string, userId: string = null): Promise<void> {
    const note = await this.notesService.findOne(id);
    if (!note) throw new NotFoundException('Note not found');
    if (userId && note.owner.id !== userId) {
      throw new ForbiddenException('You not owner');
    }
  }
}
