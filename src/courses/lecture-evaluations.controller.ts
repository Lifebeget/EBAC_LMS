import { CourseRole } from '@lib/types/enums/course-role.enum';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import * as assert from 'assert';
import { Request } from 'express';
import { Permission } from 'src/lib/enums/permission.enum';
import { UserWithPermissions } from 'src/lib/types/UserWithPermissions';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateLectureEvaluationDto } from './dto/create-lecture-evaluation.dto';
import { QueryEvaluationStatisticsDto } from './dto/query-evaluation-statistics.dto';
import { QueryLectureEvaluationDto } from './dto/query-lecture-evaluation.dto';
import { UpdateLectureEvaluationDto } from './dto/update-lecture-evaluation.dto';
import { LectureEvaluationsService } from './lecture-evaluations.service';
import { LecturesService } from './lectures.service';

@ApiTags('lecture-evaluations')
@Controller('lecture-evaluations')
export class LectureEvaluationsController {
  constructor(private readonly lectureEvaluationsService: LectureEvaluationsService, private readonly lecturesService: LecturesService) {}

  @Get()
  @ApiOperation({ summary: 'Find all lecture evaluations' })
  findAll(@Query() query: QueryLectureEvaluationDto, @Req() req: Request) {
    query = this.forceAuthUserIfNoPermissions(query, req.user, Permission.EVALUATIONS_VIEW);
    return this.lectureEvaluationsService.query(query);
  }

  @Get('stat')
  @ApiOperation({ summary: 'Get evaluation statistics' })
  @RequirePermissions(Permission.EVALUATIONS_VIEW)
  getStatistics(@Query() query: QueryEvaluationStatisticsDto) {
    return this.lectureEvaluationsService.getStatistics(query);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find lecture evaluation by id' })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.lectureEvaluationsService.findOne(id);
  }

  @Post()
  @ApiOperation({ summary: 'Create lecture evaluation' })
  async save(@Body() evaluationDto: CreateLectureEvaluationDto, @Req() req: Request) {
    this.validateEvaluation(evaluationDto.evaluation);
    await this.checkCreatePermissions(evaluationDto.lectureId, req.user);
    evaluationDto = this.forceAuthUserIfNoPermissions(evaluationDto, req.user, Permission.EVALUATIONS_MANAGE);

    return this.lectureEvaluationsService.save(evaluationDto);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update lecture evaluation' })
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() evaluationDto: UpdateLectureEvaluationDto, @Req() req: Request) {
    this.validateEvaluation(evaluationDto.evaluation);
    await this.checkUpdatePermissions(evaluationDto.lectureId, req.user);
    evaluationDto = this.forceAuthUserIfNoPermissions(evaluationDto, req.user, Permission.EVALUATIONS_MANAGE);
    return this.lectureEvaluationsService.update(id, evaluationDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete lecture evaluation' })
  async remove(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkUpdatePermissions(id, req.user);
    return this.lectureEvaluationsService.delete(id);
  }

  async checkCreatePermissions(lectureId: string, authUser: UserWithPermissions) {
    if (authUser.permissions.includes(Permission.EVALUATIONS_MANAGE)) {
      return;
    }

    const lecture = await this.lecturesService.findOne(lectureId);

    assert(lecture, new NotFoundException('Lecture is not found'));

    const enrolled = !!authUser.enrollments.find(
      enrollment => enrollment.courseId === lecture.course.id && enrollment.role === CourseRole.Student,
    );

    assert(enrolled, new ForbiddenException('This user is not enrolled to course'));
  }

  async checkUpdatePermissions(evaluationId: string, authUser: UserWithPermissions) {
    const evaluation = await this.lectureEvaluationsService.findOne(evaluationId, ['user']);

    assert(evaluation, new NotFoundException('Evaluation not found'));

    if (authUser.permissions.includes(Permission.EVALUATIONS_MANAGE)) {
      return;
    }

    assert(evaluation.user.id === authUser.id, new ForbiddenException('This evaluation has been created by another user'));
  }

  validateEvaluation(evaluation: number) {
    assert(evaluation >= 0 && evaluation < 4, new BadRequestException('Evaluation must be greater or equal to 0 and less then 4'));
  }

  forceAuthUserIfNoPermissions(dto: any, authUser: UserWithPermissions, passPermission: string) {
    if (authUser.permissions.includes(passPermission)) {
      return dto;
    }

    return { ...dto, userId: authUser.id };
  }
}
