import { Lecture } from '@entities/lecture.entity';
import { User } from '@entities/user.entity';
import { Permission } from '@enums/permission.enum';
import { Args, Resolver, Query, ResolveField, Parent } from '@nestjs/graphql';
import { CurrentUser } from 'src/decorators/whoami.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { ContentService } from './content.service';
import { LecturesService } from './lectures.service';

@Resolver(of => Lecture)
export class LecturesResolver {
  constructor(private readonly lecturesService: LecturesService, private readonly contentsService: ContentService) {}

  @RequirePermissions(Permission.COURSE_VIEW)
  @Query(() => Lecture)
  async lecture(@Args('id') id: string) {
    return await this.lecturesService.findOneRaw(id);
  }

  @ResolveField()
  async contents(@Parent() lecture: Lecture, @CurrentUser() user: User) {
    const { id } = lecture;
    return await this.contentsService.findAllLecture(id, user.id, true);
  }

  @ResolveField(type => [Lecture])
  async analogs(@Parent() lecture: Lecture): Promise<Array<Lecture>> {
    const { id } = lecture;
    return await this.lecturesService.findAnalogs(id);
  }
}
