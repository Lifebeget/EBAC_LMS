import { Course } from '@entities/course.entity';
import { Resolver, Args, Query, ResolveField, Parent } from '@nestjs/graphql';
import { Permission } from '@enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CoursesService } from './courses.service';
import { ModulesService } from './modules.service';
import { SectionsService } from './sections.service';

@Resolver(of => Course)
export class CourseResolver {
  constructor(
    private readonly coursesService: CoursesService,
    private readonly modulesService: ModulesService,
    private readonly sectionsService: SectionsService,
  ) {}

  @RequirePermissions(Permission.COURSE_VIEW)
  @Query(() => Course, { name: 'course' })
  async course(@Args('id') id: string): Promise<Course> {
    const course = await this.coursesService.findOneRaw(id);
    return course;
  }

  @ResolveField()
  async modules(@Parent() course: Course) {
    const { id } = course;
    return await this.modulesService.findAllRaw(id);
  }

  @ResolveField()
  async sections(@Parent() course: Course) {
    const { id } = course;
    return await this.sectionsService.findAllRaw(id);
  }
}
