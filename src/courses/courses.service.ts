import { Permission } from '@enums/permission.enum';
import { Injectable, HttpStatus, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as assert from 'assert';
import { Tag } from 'src/lib/entities/tag.entity';
import { TagsService } from 'src/tags/tags.service';
import { getConnection, getManager, Repository, SelectQueryBuilder } from 'typeorm';
import { Course, ExtendedCourse, CourseToSatellite } from '../lib/entities/course.entity';
import { AttachCourseTagsDto } from './dto/attach-course-tags.dto';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Lecture } from '@entities/lecture.entity';
import { ViewLecture } from 'src/lib/views/view-lecture.entity';
import { Module } from '@entities/module.entity';
import { Content } from '@entities/content.entity';
import axios from 'axios';
import { Enrollment } from '@entities/enrollment.entity';
import { SyncCourseWithCmsDto } from './dto/sync-course-with-cms.dto';
import { FilesService } from '../files/files.service';
import { FileFolder } from '@lib/types/enums/file-folder.enum';
import { ContentService } from './content.service';
import { ViewUserProgressCount } from 'src/lib/views/view-user-progress-count.entity';
import { ViewCourseCount } from 'src/lib/views/view-course-count.entity';
import { TagCategoriesService } from 'src/tags/tag-categories.service';
import { TagType } from '@lib/types/enums/tag-type.enum';
import { QueryCourseDto } from './dto/query-course.dto';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { ViewUserProgressCountMaterialized } from 'src/lib/views/view-user-progress-count-materialized.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { libConfig } from '@lib/config';
import { EnrollmentsService } from './enrollments.service';
import { User } from '@entities/user.entity';
import config from 'config';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<ExtendedCourse>,
    @InjectRepository(CourseToSatellite)
    private readonly courseToSatelliteRepository: Repository<CourseToSatellite>,
    @InjectRepository(Module)
    private readonly modulesRepository: Repository<Module>,
    @InjectRepository(Lecture)
    private readonly lecturesRepository: Repository<Lecture>,
    private readonly enrollmentsService: EnrollmentsService,
    private readonly tagsService: TagsService,
    private readonly tagCategoriesService: TagCategoriesService,
    private readonly contentService: ContentService,
    private readonly filesService: FilesService,
  ) {}

  create(dto: CreateCourseDto) {
    if (dto.published) {
      dto.publishedAt = new Date();
    }
    return this.coursesRepository.save({
      ...dto,
      image: dto.imageId ? { id: dto.imageId } : null,
      videoProducer: dto.videoProducerId ? { id: dto.videoProducerId } : null,
    });
  }

  findCoursesTitles(filterByTutorUserId?: string) {
    let qb = this.coursesRepository
      .createQueryBuilder('course')
      .select('course.id')
      .addSelect('course.title')
      .addSelect('course.version')
      .addSelect('course.isSatellite')
      .orderBy('course.title', 'ASC');

    if (filterByTutorUserId) {
      qb = qb
        .leftJoin('enrollment', 'enrollment', `enrollment.course = course.id AND enrollment.user = :userId`, {
          userId: filterByTutorUserId,
        })
        .andWhere(`enrollment.role = 'tutor'`)
        .andWhere(`enrollment.active`)
        .andWhere(`(enrollment.date_from IS NULL OR enrollment.date_from <= NOW())`)
        .andWhere(`(enrollment.date_to IS NULL OR enrollment.date_to >= NOW())`);
    }
    return qb.getMany();
  }

  async findAll(
    includeModules = false,
    includeLectures = false,
    tagTitle?: string,
    userId?: string,
    includeHidden = false,
    ids?: string[],
    includeCounters = false,
  ): Promise<ExtendedCourse[]> {
    const query = {
      includeModules,
      includeLectures,
      includeCounters,
      tagTitle,
      ids,
    };
    return (await this.query(userId, includeHidden, query, undefined)).results;
  }

  async query(
    userId?: string,
    includeHidden = false,
    query?: QueryCourseDto,
    paging?: PaginationDto,
  ): Promise<Paginated<ExtendedCourse[]>> {
    let qb = this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.image', 'course_image')
      .leftJoinAndSelect('course.videoProducer', 'users')
      .leftJoinAndSelect('course.sections', 'sections')
      .leftJoinAndSelect('course.toMasterCourses', 'toMasterCourses')
      .leftJoinAndSelect('course.toSatelliteCourses', 'toSatelliteCourses')
      .leftJoinAndSelect(
        qb => qb.select('count(1) trial_cnt, vl.course_id _id').from(ViewLecture, 'vl').where('vl.free').groupBy('vl.course_id'),
        'trial_lectures',
        'trial_lectures._id = course.id',
      );

    if (paging && !paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    if (query.includeLectures || query.includeModules) {
      if (includeHidden) {
        qb = qb.leftJoinAndSelect('course.modules', 'modules');
      } else {
        qb = qb.leftJoinAndSelect('course.modules', 'modules', 'modules.active = true AND modules.published = true');
      }
    }

    if (query.includeLectures) {
      if (includeHidden) {
        qb = qb.leftJoinAndSelect('modules.lectures', 'lectures');
      } else {
        qb = qb.leftJoinAndSelect('modules.lectures', 'lectures', 'lectures.active = true AND lectures.published = true');
      }
      qb = qb.leftJoinAndSelect('lectures.image', 'image');

      qb = qb
        .leftJoin('lectures.contents', 'content', 'content.active = true')
        .addSelect('content.type')
        .addSelect('content.id')
        .addSelect('content.active');

      if (userId) {
        qb = qb.leftJoinAndSelect('lectures.lectureProgress', 'lectureProgress', 'lectureProgress.user = :userId', { userId });
      }
    }

    qb = qb.orderBy('course.title', 'ASC');

    if (includeHidden) {
      qb = qb.leftJoinAndSelect('course.tags', 'tags').leftJoinAndSelect('tags.category', 'tag_categories');

      if (query.tagTitle) {
        qb = qb.leftJoin('course.tags', 'tagsSubSelect').andWhere('tagsSubSelect.title = :tagTitle', {
          tagTitle: query.tagTitle,
        });
      }
    } else {
      qb = qb.andWhere('course.active = true').andWhere('course.published = true');
    }

    if (query.ids) {
      qb = qb.andWhere('course.id = ANY (:ids)', { ids: query.ids });
    }

    const courses = await qb.getMany();
    const coursesRaw = await qb.getRawMany();
    const total = await qb.getCount();

    let stats = [];

    if (query.includeCounters) {
      stats = await this.getCounters(includeHidden);
    }

    const result = courses.map(course => {
      const stat = stats.find(c => c.id === course.id);
      const counters = stat?.counters || {};
      return {
        ...course,
        counters,
        trialAvailable: coursesRaw.find(raw => raw.course_id === course.id).trial_cnt > 0,
      };
    });

    return {
      results: result,
      meta: { ...paging, total },
    };
  }

  async getCounters(includeHidden = false) {
    const maybeIncludeHidden = (qb: SelectQueryBuilder<any>) => (includeHidden ? qb : qb.where('active=true').andWhere('published=true'));

    const qb = this.coursesRepository
      .createQueryBuilder('course')
      .select([
        'course.id id',
        `json_build_object(
          'modules', COALESCE(module.count, 0),
          'lectures', COALESCE(lecture.count, 0),
          'students', COALESCE(enrollment.count, 0)
        ) counters`,
      ])
      .leftJoin(
        subQuery => maybeIncludeHidden(subQuery.select(['COUNT(DISTINCT m) count', 'm.course_id']).from(Module, 'm').groupBy('course_id')),
        'module',
        'module.course_id=course.id',
      )
      .leftJoin(
        subQuery =>
          maybeIncludeHidden(subQuery.select(['COUNT(DISTINCT l) count', 'l.course_id']).from(Lecture, 'l').groupBy('l.course_id')),
        'lecture',
        'lecture.course_id=course.id',
      )
      .leftJoin(
        subQuery =>
          subQuery
            .select(['COUNT(DISTINCT e.user_id) count', 'e.course_id'])
            .from(Enrollment, 'e')
            .where('e.active=true')
            .andWhere('e.role = :role', { role: 'student' })
            .andWhere('(e.date_from IS NULL OR e.date_from <= NOW())')
            .andWhere('(e.date_to IS NULL OR e.date_to > NOW())')
            .groupBy('course_id'),
        'enrollment',
        'enrollment.course_id=course.id',
      );

    return qb.getRawMany();
  }

  findOneRaw(id: string) {
    const qb = this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.image', 'files')
      .leftJoinAndSelect('course.tags', 'tags')
      .leftJoinAndSelect('tags.category', 'tag_categories')
      .leftJoinAndSelect('course.videoProducer', 'users')
      .leftJoinAndSelect('course.toSatelliteCourses', 'toSatelliteCourses')
      .andWhere('course.id = :id', { id });

    return qb.getOne();
  }

  @Transactional()
  async findOne(id: string, param?: { userId?: string; includeHidden?: boolean; showTags?: boolean }): Promise<ExtendedCourse> {
    let qb = this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.image', 'files')
      .leftJoinAndSelect('course.videoProducer', 'users');

    if (param?.includeHidden) {
      qb = qb
        .leftJoinAndSelect('course.modules', 'modules')
        .leftJoinAndSelect('modules.lectures', 'lectures')
        .leftJoin('lectures.contents', 'content');

      if (param?.showTags) {
        qb = qb.leftJoinAndSelect('course.tags', 'tags').leftJoinAndSelect('tags.category', 'tag_categories');
      }
    } else {
      qb = qb
        .leftJoinAndSelect('course.modules', 'modules', 'modules.active = true AND modules.published = true')
        .leftJoinAndSelect('modules.lectures', 'lectures', 'lectures.active = true AND lectures.published = true')
        .leftJoin('lectures.contents', 'content', 'content.active = true')
        .andWhere('course.active = true')
        .andWhere('course.published = true');
    }

    if (param?.userId) {
      qb = qb.leftJoinAndSelect('lectures.lectureProgress', 'lectureProgress', 'lectureProgress.user = :userId', { userId: param.userId });
    }

    qb = qb
      .leftJoinAndSelect('lectures.image', 'image')
      .leftJoinAndSelect('course.sections', 'sections')
      .leftJoinAndSelect('course.toSatelliteCourses', 'toSatelliteCourses')
      .leftJoinAndSelect('course.toMasterCourses', 'toMasterCourses')
      .addSelect('content.id')
      .addSelect('content.type')
      .addSelect('content.active')
      .addSelect('content.assessmentId')
      .addSelect(
        `
        (select count(1) > 0 as trial_available from view_lecture
        where course_id = :id
        and free) as trial_available
      `,
      )
      .andWhere('course.id = :id', { id })
      .addOrderBy('modules.sort', 'ASC')
      .addOrderBy('lectures.sort', 'ASC');

    const course = await qb.getOne();
    const courseRaw = await qb.getRawOne();

    return {
      ...course,
      trialAvailable: courseRaw ? courseRaw.trial_available > 0 : false,
    };
  }

  @Transactional()
  async fixModulesOrder(course: Course) {
    const promises = [];
    const incrementModule = course.numberModulesFromZero ? 0 : 1;

    course.modules.forEach((module, moduleIndex) => {
      const moduleCurrentIndex = moduleIndex + incrementModule;
      if (!module.number || module.number !== moduleCurrentIndex) {
        const updateModulePromise = this.modulesRepository.update(module.id, {
          number: moduleCurrentIndex,
        });
        promises.push(updateModulePromise);
      }
    });
    await Promise.all(promises);
    return await this.findOne(course.id, { includeHidden: true });
  }

  async fixLecturesOrder(modules: Module[]) {
    const promises = [];

    modules.forEach(module => {
      const incrementLecture = module.numberLecturesFromZero ? 0 : 1;
      module.lectures.forEach((lecture, lectureIndex) => {
        const lectureCurrentIndex = lectureIndex + incrementLecture;
        if (!lecture.number || lecture.number !== lectureCurrentIndex) {
          const updateLecturePromise = this.lecturesRepository.update(lecture.id, {
            number: lectureCurrentIndex,
          });
          promises.push(updateLecturePromise);
        }
      });
    });
    return await Promise.all(promises);
  }

  async fixOrder(course: Course) {
    await this.fixModulesOrder(course);
    await this.fixLecturesOrder(course.modules);
    return;
  }

  async findByExternalId(externalId: string, distributeSupport?: boolean): Promise<Course & { support?: Enrollment[] }> {
    const course = (await this.coursesRepository.findOne(
      { externalId },
      {
        relations: ['modules', 'sections', 'modules.lectures', 'image', 'tags', 'videoProducer'],
      },
    )) as Course & { support?: Enrollment[] };

    if (course && distributeSupport) {
      course.support = await this.enrollmentsService.findSupportByCourseId(course.id);
      if (course.support.length > 0) {
        const user = course.support[0].user;
        await this.enrollmentsService.updateSupportAccessDate(user.id);
      }
    }
    return course;
  }

  async findByCMSId(cmsId: string, distributeSupport?: boolean): Promise<Course & { support?: Enrollment[] }> {
    const course = (await this.coursesRepository.findOne(
      { cmsId, active: true, published: true },
      {
        relations: ['modules', 'sections', 'modules.lectures', 'image', 'tags', 'videoProducer'],
        order: {
          version: 'DESC',
        },
      },
    )) as Course & { support?: Enrollment[] };
    if (course && distributeSupport) {
      course.support = await this.enrollmentsService.findSupportByCourseId(course.id);
      if (course.support.length > 0) {
        const user = course.support[0].user;
        await this.enrollmentsService.updateSupportAccessDate(user.id);
      }
    }
    return course;
  }

  compare(a: any[], b: any[]) {
    return b.every((v: any) => a.indexOf(v) >= 0);
  }

  async checkingTags(currentCourse: Course) {
    const tags = (
      await this.tagCategoriesService.findAll({ page: 0, itemsPerPage: 100, showAll: true }, { type: TagType.CourseVertical })
    ).results.map(tag => tag.title);
    const equal = this.compare(
      currentCourse.tags.map(tag => tag.category.title),
      tags,
    );
    if (!currentCourse.tags || !equal) {
      throw new BadRequestException('tagsCategory');
    } else {
      return true;
    }
  }

  async update(id: string, updateCourseDto: UpdateCourseDto) {
    const updateFields: any = { ...updateCourseDto, id };
    const currentCourse = await this.findOne(id, { includeHidden: true, showTags: true });

    if (updateCourseDto.hasOwnProperty('isSatellite')) {
      if (!currentCourse.isSatellite && updateCourseDto.isSatellite) {
        await this.detachAllSatellitesFromCourse(currentCourse);
      }
      if (currentCourse.isSatellite && !updateCourseDto.isSatellite) {
        await this.detachSatelliteFromAllCourses(currentCourse);
      }
    }

    if (currentCourse.numberModulesFromZero !== updateCourseDto.numberModulesFromZero) {
      await this.fixModulesOrder(currentCourse);
    }

    if (updateFields.published === true && currentCourse.published !== true) {
      await this.checkingTags(currentCourse);
      if (currentCourse.published === false) {
        updateFields.publishedAt = new Date();
      }
    }

    if ('imageId' in updateCourseDto) {
      updateFields.image = { id: updateCourseDto.imageId };
    }

    if ('videoProducerId' in updateCourseDto) {
      updateFields.videoProducer = { id: updateCourseDto.videoProducerId };
    }

    updateFields.toSatelliteCourses = currentCourse.toSatelliteCourses;

    return this.coursesRepository.save(updateFields);
  }

  async publish(id: string) {
    const currentCourse = await this.coursesRepository.findOne(id);
    await this.checkingTags(currentCourse);
    if (currentCourse.published === false) {
      return this.coursesRepository.save({
        id,
        published: true,
        publishedAt: new Date(),
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already published',
      };
    }
  }

  async unpublish(id: string) {
    const currentCourse = await this.coursesRepository.findOne(id);
    if (currentCourse.published === true) {
      return this.coursesRepository.save({
        id,
        published: false,
        publishedAt: null,
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already unpublished',
      };
    }
  }

  async attachTags(courseId: string, dto: AttachCourseTagsDto) {
    return this.findCourseAndRun(courseId, ['tags'], async (course: Course) => this.attachTagsToCourse(course, dto));
  }

  async attachTag(courseId: string, tagId: string) {
    return this.findCourseAndRun(courseId, ['tags'], course => this.attachTagToCourse(course, tagId));
  }

  async detachTag(courseId: string, tagId: string) {
    return this.findCourseAndRun(courseId, ['tags'], course => this.detachTagFromCourse(course, tagId));
  }

  async findCourseAndRun(courseId: string, relations: string[], func: any) {
    const course = await this.coursesRepository.findOne(courseId, {
      relations,
    });

    assert(course, new NotFoundException('Course is not found'));

    return func(course);
  }

  async attachTagsToCourse(course: Course, dto: AttachCourseTagsDto) {
    if (dto.replace) {
      course.tags = [];
    }

    for (const tagId of dto.tagIds) {
      await this.attachTagToCourse(course, tagId);
    }

    return course.tags;
  }

  async attachTagToCourse(course: Course, tagId: string) {
    const existedTag = course.tags.find(tag => tag.id === tagId);

    if (existedTag) {
      return existedTag;
    }

    const tag = await this.tagsService.findOne(tagId);

    assert(tag, new NotFoundException('Tag is not found'));

    if (tag && tag.category.type !== TagType.CourseVertical) {
      throw new BadRequestException('The tag could not be assigned to the course');
    }

    course.tags.push(tag);

    await this.coursesRepository.save(course);

    return tag;
  }

  async detachTagFromCourse(course: Course, tagId: string) {
    course.tags = course.tags.filter(tag => tag.id !== tagId);
    await this.coursesRepository.save(course);
    return course.tags;
  }

  async findByTagTitle(tagTitle: string, options: any) {
    const tag = await this.tagsService.findOneByTitle(tagTitle, []);

    if (!tag) {
      return [];
    }

    return this.getTagCourses(tag, options);
  }

  async findByTagId(tagId: string, options: any = {}) {
    const tag = await this.tagsService.findOne(tagId, true);

    if (!tag) {
      return [];
    }

    return this.getTagCourses(tag, options);
  }

  async findIdsByTagId(tagId: string) {
    const tag = await this.tagsService.findOne(tagId, true);

    if (!tag) {
      return [];
    }

    return tag.courses.map(course => course.id);
  }

  async getTagCourses(tag: Tag, options: any) {
    const courseIds = tag.courses.map(course => course.id);
    return this.coursesRepository.findByIds(courseIds, options);
  }

  remove(id: string) {
    return this.coursesRepository.delete(id);
  }

  removeList(courses: Course[]) {
    return this.coursesRepository.remove(courses);
  }

  purgeAll() {
    return this.coursesRepository.clear();
  }

  async checkCourseRights(course: Course, user: Express.User) {
    const isHasLectureFree = course.modules.some(module => module.lectures.some(lecture => lecture.free));
    const hasViewPerm = user.hasPermission(Permission.COURSE_VIEW);
    const hasEnrollment =
      user.enrolledAsStudent(course.id) ||
      user.enrolledAsTutor(course.id) ||
      user.enrolledAsTeamlead(course.id) ||
      user.enrolledAsModerator(course.id);
    return hasViewPerm || hasEnrollment || isHasLectureFree;
  }

  async checkCourseIdRights(courseId: string, user: Express.User) {
    const course = await this.findOne(courseId);
    return await this.checkCourseRights(course, user);
  }

  async SyncKalturaCChunks(courseId: string) {
    const LecturesWithContents = await this.lecturesRepository
      .createQueryBuilder('lecture')
      .leftJoinAndSelect('lecture.contents', 'contents')
      .andWhere("contents.type = 'kaltura'")
      .andWhere('lecture.course_id = :courseId', {
        courseId,
      })
      .getMany();

    let contents: Array<Content> = [];
    const lecture_ids = {};

    LecturesWithContents.forEach(l => {
      contents = [...contents, ...l.contents];
      lecture_ids[l.contents[0].id] = l.id;
    });

    await Promise.all(
      contents.map(async c => {
        if (!c.entityId) {
          console.error('Missing entityId of Video chunk ' + c.id);
          return;
        }

        const msDuration = await this.contentService.updateKalturaContent(c);

        // Update lecture duration
        const duration_seconds = Math.round(msDuration / 1000);
        if (lecture_ids[c.id]) {
          await this.lecturesRepository.save({
            id: lecture_ids[c.id],
            duration: duration_seconds,
          });
        }
      }),
    );

    return { message: 'Done' };
  }

  async findAllCoursesFromCMS() {
    const response = await axios.get(`${process.env.CMS_HOST}/api/raw-list/nomenclature`, {
      params: {
        apiKey: libConfig.CMS_API_KEY,
      },
    });
    const cmsCourses = response?.data.data.fields || [];

    cmsCourses.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    return cmsCourses.map(course => {
      return {
        id: course.id,
        title: course.name,
        image: course.file_showcase_image,
        duration: course.duration,
        color: course.background_color,
        active: course.active,
      };
    });
  }

  async syncCourseWithCMS(courseId: string, dto: SyncCourseWithCmsDto) {
    const savedFile = await this.filesService.uploadUrlToS3andUpdate(
      {
        title: dto.image.filename,
        filename: dto.image.filename,
        mimetype: dto.image.mimetype,
        size: dto.image.size,
        url: dto.image.url,
        extension: dto.image.extension,
      },
      FileFolder.covers,
    );
    return this.update(courseId, {
      imageId: savedFile.id,
      color: dto.color,
      duration: dto.duration,
      cmsId: dto.id,
    });
  }

  async SyncCMSIds() {
    const reqFromCMS = await axios.post(`${process.env.CMS_HOST}/api/public/showcase`);
    let total = 0;
    let found = 0;
    let notFound = 0;
    const notFoundItems = [];

    if (reqFromCMS?.data?.data?.data) {
      const coursesFromCMS = reqFromCMS.data.data.data;
      total = coursesFromCMS.length;
      for (const cmsCourse of coursesFromCMS) {
        // find by existent cms id (for manual ids check)
        let course = await this.coursesRepository.findOne({
          cmsId: cmsCourse.id,
        });
        // find by slug
        if (!course) {
          course = await this.coursesRepository.findOne({
            slug: cmsCourse.slug,
          });
        }
        // or find by name / short name
        if (!course) {
          course = await this.coursesRepository
            .createQueryBuilder('course')
            .where('course.title ILIKE :title', {
              title: `${cmsCourse.name}`,
            })
            .orWhere('course.title ILIKE :titleShort', {
              titleShort: `${cmsCourse.shortName}`,
            })
            .getOne();
        }
        // oor find by partial name (profissao problem)
        if (!course) {
          course = await this.coursesRepository
            .createQueryBuilder('course')
            .where('course.title ILIKE :title', {
              title: `%${cmsCourse.shortName}`,
            })
            .getOne();
        }
        // or show error
        if (!course) {
          notFoundItems.push(cmsCourse);
          notFound++;
        } else {
          found++;
          await this.coursesRepository.save({
            id: course.id,
            cmsId: cmsCourse.id,
          });
        }
      }
    }

    return { message: 'Done', total, found, notFound, notFoundItems };
  }

  generateCourseLink(courseId: string, user: Partial<User>) {
    return `${config.setUser(user).get('STUDENT_LMS_URL')}/courses/${courseId}`;
  }

  generateNewSurveyLinkByCourse(course: Course, user: Partial<User>) {
    let link = this.generateCourseLink(course.id, user);

    const zeroModule = course.modules.sort((a, b) => a.sort - b.sort)[0];
    if (zeroModule?.lectures) {
      zeroModule.lectures.forEach(l => {
        l.contents.forEach(c => {
          if (c.type === 'task' || c.type === 'survey' || c.type === 'globalSurvey') {
            link = `${config.setUser(user).get('STUDENT_LMS_URL')}/lesson/${l.id}/survey/${c.assessmentId}`;
          }
        });
      });
    }
    return link;
  }

  async generateNewSurveyLink(courseId: string, user: Partial<User>) {
    const course = await this.findOne(courseId, { includeHidden: true });
    return this.generateNewSurveyLinkByCourse(course, user);
  }

  async getEmptyProgressForCourse(userId: string, courseId: string): Promise<ViewUserProgressCount[]> {
    const entityManager = getManager();
    const courseCounters = await entityManager.findOne(ViewCourseCount, {
      relations: ['course'],
      where: { id: courseId },
    });
    return [
      {
        userId,
        courseId: courseId,
        lecturesTotal: courseCounters.lectures,
        lecturesRequiredTotal: courseCounters.lecturesRequired,
        lecturesCompleted: 0,
        lecturesRequiredCompleted: 0,
        modulesRequiredCompleted: 0,
        modulesCompleted: 0,
        modulesTotal: courseCounters.modules,
        modulesRequiredTotal: courseCounters.modulesRequired,
        homeworksTotal: courseCounters.homeworks,
        homeworksRequiredTotal: courseCounters.homeworksRequired,
        quizesTotal: courseCounters.quizes,
        quizesRequiredTotal: courseCounters.quizesRequired,
        surveysTotal: courseCounters.surveys,
        surveysRequiredTotal: courseCounters.surveysRequired,
        homeworksCompleted: 0,
        homeworksCompletedRequired: 0,
        quizesCompleted: 0,
        quizesCompletedRequired: 0,
        surveysCompleted: 0,
        surveysCompletedRequired: 0,
        progress: 0,
        graduated: false,
        course: courseCounters.course,
      } as ViewUserProgressCount,
    ];
  }

  async viewUserProgressCount(userId: string, courseId?: string): Promise<ViewUserProgressCount[]> {
    const entityManager = getManager();
    const where = { userId, courseId };
    if (!courseId) {
      delete where.courseId;
    }
    const progress = await entityManager.find(ViewUserProgressCount, {
      relations: ['course'],
      where: { ...where },
    });

    /** In cases when there is no actual enrollment exists for the user
     *  and no actual progress on the course
     *  ViewUserProgressCount table will be empty for that course
     *  but we still need to return course counters and zero progress
     */
    if (userId && courseId && !progress.length) {
      return this.getEmptyProgressForCourse(userId, courseId);
    }
    return progress;
  }

  // Materialized version of viewUserProgressCount
  async viewUserProgressCountMaterialized(userId: string, courseId?: string): Promise<ViewUserProgressCount[]> {
    const entityManager = getManager();
    const where = { userId, courseId };
    if (!courseId) {
      delete where.courseId;
    }
    const progress = await entityManager.find(ViewUserProgressCountMaterialized, {
      relations: ['course'],
      where: { ...where },
    });
    /** In cases when there is no actual enrollment exists for the user
     *  and no actual progress on the course
     *  ViewUserProgressCount table will be empty for that course
     *  but we still need to return course counters and zero progress
     */
    if (userId && courseId && !progress.length) {
      return this.getEmptyProgressForCourse(userId, courseId);
    }
    return progress as ViewUserProgressCount[];
  }

  updateProgressMaterializedView() {
    return getConnection().query('REFRESH MATERIALIZED VIEW view_user_progress_count_materialized');
  }

  findOneImportVideo(id: string, param?: { userId?: string; includeHidden?: boolean }): Promise<Course> {
    let qb = this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.image', 'files')
      .leftJoinAndSelect('course.videoProducer', 'users');

    if (param?.includeHidden) {
      qb = qb
        .leftJoinAndSelect('course.modules', 'modules')
        .leftJoinAndSelect('modules.lectures', 'lectures')
        .leftJoinAndSelect('lectures.contents', 'content');
    } else {
      qb = qb
        .leftJoinAndSelect('course.modules', 'modules', 'modules.active = true AND modules.published = true')
        .leftJoinAndSelect('modules.lectures', 'lectures', 'lectures.active = true AND lectures.published = true')
        .leftJoinAndSelect('lectures.contents', 'content', 'content.active = true')
        .andWhere('course.active = true')
        .andWhere('course.published = true');
    }

    if (param?.userId) {
      qb = qb.leftJoinAndSelect('lectures.lectureProgress', 'lectureProgress', 'lectureProgress.user = :userId', { userId: param.userId });
    }

    qb = qb
      .leftJoinAndSelect('lectures.image', 'image')
      .leftJoinAndSelect('course.tags', 'tags')
      .leftJoinAndSelect('course.sections', 'sections')
      .andWhere('course.id = :id', { id })
      .addOrderBy('modules.sort', 'ASC')
      .addOrderBy('lectures.sort', 'ASC');

    return qb.getOne();
  }

  async getAutoEnrollCourses() {
    return this.coursesRepository.find({
      where: {
        isAutoEnroll: true,
      },
    });
  }

  async attachSatelliteToCourse(course: Course, satellite: Course) {
    if (course?.isSatellite || !satellite?.isSatellite) {
      throw new BadRequestException('The course could not be assigned as satellite');
    }

    const existedRelation = await this.courseToSatelliteRepository.findOne({
      masterCourseId: course.id,
      satelliteCourseId: satellite.id,
    });
    if (existedRelation) {
      throw new BadRequestException('Satellite already attached');
    }

    const relation = new CourseToSatellite();
    relation.masterCourseId = course.id;
    relation.satelliteCourseId = satellite.id;
    relation.satelliteCourseTitle = satellite.title;

    return await this.courseToSatelliteRepository.save(relation);
  }

  async detachSatelliteFromCourse(course: Course, satellite: Course) {
    return await this.courseToSatelliteRepository.delete({
      masterCourseId: course.id,
      satelliteCourseId: satellite.id,
    });
  }

  async detachSatelliteFromAllCourses(satellite: Course) {
    return await this.courseToSatelliteRepository.delete({ satelliteCourseId: satellite.id });
  }

  async detachAllSatellitesFromCourse(course: Course) {
    return await this.courseToSatelliteRepository.delete({ masterCourseId: course.id });
  }

  async updateSatellite(course: Course, satellite: Course, required: boolean) {
    return await this.courseToSatelliteRepository.update(
      {
        satelliteCourseId: satellite.id,
        masterCourseId: course.id,
      },
      {
        required,
      },
    );
  }
}
