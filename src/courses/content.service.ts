import { ContentType } from '@enums/content-type.enum';
import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  UnprocessableEntityException,
  NotImplementedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { CoversService } from 'src/covers/covers.service';
import { Assessment } from 'src/lib/entities/assessment.entity';
import { Content } from 'src/lib/entities/content.entity';
import { Question } from 'src/lib/entities/question.entity';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { ContentTriggerService } from 'src/trigger/services/content-trigger.service';
import { Between, FindConditions, Repository } from 'typeorm';
import { KalturaService } from '../kaltura/kaltura.service';
import { CreateContentDto } from './dto/create-content.dto';
import { QueryContentDto } from './dto/query-content.dto';
import { RedisService } from 'nestjs-redis';
import { VersionUpdateDTO, VersionUpdateDTOResult } from './dto/update-content-version-update.dto';
import { UpdateContentDto } from './dto/update-content.dto';
import { LecturesService } from './lectures.service';
import { Submission } from '@entities/submission.entity';
import { QuestionsService } from '../assessments/questions.service';
import { Transactional } from 'typeorm-transactional-cls-hooked';

const defaultRelations = ['owner', 'lecture', 'assessment', 'files'];

@Injectable()
export class ContentService {
  constructor(
    @InjectRepository(Content)
    private contentRepository: Repository<Content>,

    @InjectRepository(Assessment)
    private assessmentRepository: Repository<Assessment>,

    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,

    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,

    private kalturaService: KalturaService,
    @Inject(forwardRef(() => AssessmentsService))
    private assessmentService: AssessmentsService,

    private readonly contentTriggerService: ContentTriggerService,
    @Inject(forwardRef(() => LecturesService))
    private lecturesService: LecturesService,
    @Inject(forwardRef(() => CoversService))
    private coversService: CoversService,
    private redisService: RedisService,
    @Inject(forwardRef(() => QuestionsService))
    private questionsService: QuestionsService,
  ) {}

  async create(lectureId: string, dto: CreateContentDto, ownerId: string = null): Promise<Content> {
    let assessment = null;

    if ('assessment' in dto && dto.assessment) {
      if (dto.type === 'task' && dto.assessment?.questions?.length !== 1) {
        throw new NotImplementedException('Multiple or empty questions for tasks are not supported yet');
      }
      if (dto.assessment.questions) {
        const promises = [];
        dto.assessment.questions.forEach(q => {
          promises.push(this.questionsService.create(q));
        });
        await Promise.all(promises);
      }

      if (dto.assessment.isGlobal) {
        assessment = await this.assessmentRepository.findOne({
          commonId: dto.assessment.commonId,
          currentVersion: true,
        });
      } else {
        assessment = await this.assessmentRepository.save({
          lecture: {
            id: lectureId,
          },
          currentVersion: true,
          ...dto.assessment,
        });
      }
    }

    const entity = {
      id: dto.id,
      type: dto.type,
      lecture: { id: lectureId },
      sort: 'sort' in dto ? dto.sort : 500,
      active: 'active' in dto ? dto.active : true,
      title: 'title' in dto ? dto.title : null,
      assessment: assessment || ('assessmentId' in dto ? { id: dto.assessmentId } : null),
      files: 'files' in dto ? dto.files : [],
      html: 'html' in dto ? dto.html : null,
      entityId: 'entityId' in dto ? dto.entityId : null,
      link: 'link' in dto ? dto.link : null,
      data: 'data' in dto ? dto.data : null,
      owner: ownerId ? { id: ownerId } : null,
      externalId: dto.externalId || null,
    };

    const content = await this.contentRepository.save(entity);

    this.clearCache({ lectureId });

    if (content.type === ContentType.kaltura) {
      await this.contentTriggerService.kalturaBlockUpdated(content);
    }
    return content;
  }

  async findAllLecture(lectureId: string, userId: string, includeHidden?: boolean, useCache?: boolean): Promise<Content[]> {
    const where: FindConditions<Content> = { lecture: { id: lectureId } };
    if (!includeHidden) {
      where.active = true;
    }
    let cache;
    if (useCache) {
      cache = {
        id: `db:content:${lectureId}:${includeHidden}`,
        milliseconds: 60000,
      };
    }

    const contents = await this.contentRepository.find({
      where,
      order: { sort: 'ASC' },
      relations: [
        'files',
        'assessment',
        'assessment.questions',
        'assessment.questions.files',
        'assessment.questions.assessmentCriteria',
        'assessment.level',
        'assessment.level.tariffs',
      ],
      cache,
    });

    for (const content of contents) {
      if (content.type === ContentType.kaltura && content.data.ks) {
        (<Record<string, unknown>>content.data).ks = this.kalturaService.getKSforEntryId(content.entityId, userId);
      }
      if (content.type === ContentType.task) {
        content.assessment.currentTariff = content.assessment.level.tariffs.find(t => dayjs().isBetween(t.activeFrom, t.activeTo));
      }
      if (content.type === ContentType.files && content.data.preview === undefined) {
        content.data = { preview: true };
      }
    }
    return contents;
  }

  @Transactional()
  findOne(id: string): Promise<Content> {
    return this.contentRepository.findOne(id, {
      relations: defaultRelations,
    });
  }

  async findAll(paging: PaginationDto, query: QueryContentDto = {}): Promise<Paginated<Content[]>> {
    const where: any = {};

    if ('active' in query) where.active = query.active;
    if ('lectureId' in query) where.lecture = query.lectureId;
    if ('ownerId' in query) where.owner = query.ownerId;
    if ('assessmentId' in query) where.assessment = query.assessmentId;
    if ('entityId' in query) where.entityId = query.entityId;
    if ('link' in query) where.link = query.link;
    if ('type' in query) where.type = query.type;

    if ('dateCreated' in query) {
      where.created = Between(
        dayjs(query.dateCreated).startOf('day').toDate(),
        dayjs(query.dateCreated).startOf('day').add(1, 'day').toDate(),
      );
    }

    const [results, total] = await this.contentRepository.findAndCount({
      where,
      order: { created: 'ASC' },
      relations: defaultRelations,
      skip: (paging.page - 1) * paging.itemsPerPage,
      take: paging.itemsPerPage,
    });

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  async update(id: string, dto: UpdateContentDto) {
    const entity: any = { id };
    if ('type' in dto) entity.type = dto.type;
    if ('sort' in dto) entity.sort = dto.sort;
    if ('active' in dto) entity.active = dto.active;
    if ('externalId' in dto) entity.externalId = dto.externalId;
    if ('title' in dto) entity.title = dto.title;
    if ('files' in dto) entity.files = dto.files;
    if ('html' in dto) entity.html = dto.html;
    if ('entityId' in dto) entity.entityId = dto.entityId;
    if ('link' in dto) entity.link = dto.link;
    if ('data' in dto) entity.data = dto.data;
    if ('assessmentId' in dto) entity.assessment = { id: dto.assessmentId };

    if (dto.type == ContentType.files && (!('data' in dto) || typeof dto.data?.preview !== 'boolean')) {
      throw new HttpException('BadRequestException', HttpStatus.BAD_REQUEST);
    }

    if ('assessment' in dto && dto.assessment) {
      if (dto.assessment.questions) {
        const promises = [];
        dto.assessment.questions.forEach((q: Question) => {
          promises.push(this.questionsService.update(q.id, q));
        });
        await Promise.all(promises);
      }
      await this.assessmentRepository.save(dto.assessment);
    }
    const result = await this.contentRepository.save(entity);
    const contentDb = await this.contentRepository.findOne(id);
    this.clearCache({ lectureId: contentDb?.lecture?.id, contentId: id });

    return result;
  }

  @Transactional()
  async remove(id: string) {
    const content = await this.contentRepository.findOne(id, {
      relations: ['assessment', 'assessment.submissions'],
    });
    if (content.assessment && !content.assessment.isGlobal && !content.assessment.isWebinarSurvey) {
      if (content.assessment.submissions) {
        for (const index in content.assessment.submissions) {
          await this.submissionsRepository.softDelete(content.assessment.submissions[index].id);
        }
      }
      await this.assessmentRepository.softDelete(content.assessment.id);
    }

    const result = await this.contentRepository.softDelete(id);

    this.clearCache({ lectureId: content?.lecture?.id, contentId: id });

    return result;
  }

  @Transactional()
  async updateContent(dto: UpdateContentDto, lectureId: string, ownerId: string) {
    let assessment = null;

    if ('assessment' in dto && dto.assessment) {
      if (dto.type === 'task' && dto.assessment?.questions?.length !== 1) {
        throw new NotImplementedException('Multiple or empty questions for tasks are not supported yet');
      }
      if (!dto.assessment.isGlobal) {
        const [isNewVersion, assessmentNew] = await this.assessmentService.updateAssessment(dto.assessment, lectureId);
        assessment = assessmentNew;

        if (isNewVersion) {
          await this.contentRepository.update(
            {
              id: dto.id,
            },
            {
              assessmentId: assessment.id,
            },
          );
        }
      } else {
        await this.contentRepository.update(
          {
            id: dto.id,
          },
          {
            assessmentId: dto.assessment.id,
          },
        );
      }
    }

    if (dto.type == ContentType.files && (!('data' in dto) || typeof dto.data?.preview !== 'boolean')) {
      throw new HttpException('BadRequestException', HttpStatus.BAD_REQUEST);
    }

    const entity = {
      id: dto.id,
      type: dto.type,
      lecture: { id: lectureId },
      sort: 'sort' in dto ? dto.sort : 500,
      active: 'active' in dto ? dto.active : true,
      title: 'title' in dto ? dto.title : null,
      assessment: assessment || ('assessmentId' in dto ? { id: dto.assessmentId } : null),
      files: 'files' in dto ? dto.files : [],
      html: 'html' in dto ? dto.html : null,
      entityId: 'entityId' in dto ? dto.entityId : null,
      link: 'link' in dto ? dto.link : null,
      data: 'data' in dto ? dto.data : null,
      owner: ownerId ? { id: ownerId } : null,
    };

    const result = await this.contentRepository.save(entity);

    if (entity.type === ContentType.kaltura) {
      await this.contentTriggerService.kalturaBlockUpdated(result);
    }

    this.clearCache({ lectureId, contentId: dto.id });
  }

  @Transactional()
  async versionUpdate(dto: VersionUpdateDTO, ownerId: string = null) {
    const lectureId = dto.lectureId;
    let promisesCreate = null;
    let promisesDelete = null;
    let promisesUpdate = null;
    const result: VersionUpdateDTOResult = {
      create: null,
      update: null,
      delete: null,
    };

    if (dto.create && dto.create.length) {
      promisesCreate = dto.create.reduce((a, e) => a.concat([this.create(lectureId, e, ownerId)]), []);
      result.create = await Promise.all(promisesCreate);
    }

    if (dto.delete && dto.delete.length) {
      promisesDelete = dto.delete.reduce((a, e) => a.concat([this.remove('' + e)]), []);
      result.delete = await Promise.all(promisesDelete);
    }

    if (dto.update && dto.update.length) {
      promisesUpdate = dto.update.reduce((a, e) => a.concat([this.updateContent(e, lectureId, ownerId)]), []);
      result.update = await Promise.all(promisesUpdate);
    }
    this.clearCache({ lectureId: dto.lectureId });
  }

  async clearCache(params: { lectureId?: string; contentId?: string }) {
    if (!params.lectureId && !params.contentId) {
      return;
    }

    try {
      let pattern;
      if (params.lectureId) {
        pattern = `db:lecture:${params.lectureId}*`;
      } else if (params.contentId) {
        pattern = `db:content:${params.contentId}*`;
      }

      const client = await this.redisService.getClient();
      client.scan(0, 'match', pattern).then(keys => {
        for (let index = 0; index < keys[1].length; index++) {
          client.unlink(keys[1][index]).catch(error => console.error(error));
        }
      });
    } catch (error) {
      console.error(error);
    }
  }

  //VALIDATE KALTURA VIDEO BLOCK
  async validateKalturaContent(content, inform = false) {
    let validateError = false;

    //validate meta
    //validate subs
    const contentDuration = await this.updateKalturaContent(content);

    //validate lecture
    const lecture = await this.lecturesService.findOneRaw(content.lecture.id);

    // We haven't content block duration or lecture duration => error
    if (!contentDuration || !lecture.duration) {
      validateError = true;
    } else if (!lecture.duration) {
      //if lecture haven't duration => update lecture
      lecture.duration = Math.round(contentDuration / 1000);
      await this.lecturesService.update(lecture.id, lecture);
    }

    //validate thumbnail
    const lectureCover = await this.coversService.findForLecture(lecture.id);
    if (!lectureCover) {
      validateError = true;
    } else if (!lecture.image) {
      validateError = true;
    } else {
      await this.kalturaService.setThumbnails({
        entryId: content.entityId,
        url: lecture.image.url,
      });
    }

    //Reject
    if (inform && validateError) {
      throw new UnprocessableEntityException(content, 'Kaltura block unvalidated.');
    }
  }

  async updateKalturaContent(content) {
    const meta = await this.kalturaService.getVideoMeta(content.entityId);
    const subs = await this.kalturaService.getVideoSubtitlesList(content.entityId);

    let subTitles = [];

    await Promise.all(
      subs.map(async s => {
        const res = await this.kalturaService.getVideoSubtitles(content.entityId, s.id);

        subTitles = [...subTitles, { ...s, subs: res }];
      }),
    );

    let subsCompleted = false;
    if (/\[CC\]/g.test(meta.name)) {
      subsCompleted = true;
    }

    content.data = {
      //default paragraph subslang and subsParagraphTime
      subsLang: 'pt',
      subsParagraphTime: 3,
      //
      ...content.data,
      ...meta,
      subs: subTitles,
      subsCompleted,
      ks: this.kalturaService.getKSforEntryId(meta.id),
    };

    await this.contentRepository.save(content);

    return meta.msDuration;
  }

  async getRelationForDelete(contentId: string) {
    return this.contentRepository
      .createQueryBuilder('content')
      .andWhere('content.id = :contentId', { contentId })

      .leftJoinAndSelect('content.assessment', 'assessment')
      .leftJoin('assessment.submissions', 'submissions')
      .loadRelationCountAndMap('assessment.countSubmissions', 'assessment.submissions')

      .getOne();
  }
}
