import { Controller, Get, Post, Put, Body, Patch, Param, Delete, ParseUUIDPipe, NotFoundException, Req } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { ModulesService } from './modules.service';
import { CreateModuleDto } from './dto/create-module.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { PgConstraints } from '../pg.decorators';
import { Module } from '../lib/entities/module.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { Permissions } from 'src/permissions.decorator';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { ModuleTriggerService } from 'src/trigger/services/module-trigger.service';
import { Request } from 'express';
import { CopyModuleDto } from './dto/copy-module.dto';
import { MoveModuleDto } from './dto/move-module.dto';

@ApiTags('modules')
@ApiBearerAuth()
@Controller()
export class ModulesController {
  constructor(private readonly modulesService: ModulesService, private readonly moduleTriggerService: ModuleTriggerService) {}

  @Post('modules/by-courses')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all modules for a courses' })
  @ApiResponse({
    status: 200,
    type: [Module],
  })
  findAllByCourses(@Body() body) {
    if (!body?.courses?.length) {
      throw new NotFoundException('ERRORS.MODULES_NOT_FOUND');
    }
    return this.modulesService.findAllByCourseIds(body.courses);
  }

  @Transactional()
  @Post('courses/:courseId/modules')
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new module' })
  @ApiResponse({
    status: 201,
    description: 'Created module',
    type: Module,
  })
  @PgConstraints()
  create(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Body() createModuleDto: CreateModuleDto) {
    return this.modulesService.create(courseId, createModuleDto);
  }

  @Transactional()
  @Get('courses/:courseId/modules')
  @ApiOperation({ summary: 'List all modules for a course' })
  @ApiResponse({
    status: 200,
    type: [Module],
  })
  async findAll(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Permissions() perms: Permission[]) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.modulesService.findAll(courseId, includeHidden);
    if (!result.length) {
      throw new NotFoundException('ERRORS.MODULES_NOT_FOUND');
    }
    return result;
  }

  @Get('courses/:courseId/modules-for-admin-panel')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all modules for a course with active deep checking' })
  @ApiResponse({
    status: 200,
    type: [Module],
  })
  async findAllForAdminPanel(@Param('courseId', new ParseUUIDPipe()) courseId: string, @Permissions() perms: Permission[]) {
    return await this.modulesService.findByCourseId(courseId);
  }

  @Get('courses/:courseId/modulesTitles')
  findCourseModulesTitles(@Param('courseId', new ParseUUIDPipe()) courseId: string) {
    return this.modulesService.findCourseModulesTitles(courseId);
  }

  @Transactional()
  @Get('modules/:id')
  @ApiOperation({ summary: 'Get one Module by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a Module by id',
    type: Module,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    const includeHidden = perms.includes(Permission.COURSE_VIEW);
    const result = await this.modulesService.findOne(id, includeHidden);
    if (!result) {
      throw new NotFoundException('ERRORS.MODULE_NOT_FOUND');
    }
    return result;
  }

  @Transactional()
  @Patch('modules/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a Module' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateModuleDto: UpdateModuleDto) {
    const module = await this.modulesService.update(id, updateModuleDto);

    if (updateModuleDto.publishTrigger) {
      this.moduleTriggerService.modulePublished(module, updateModuleDto.sendEmail);
    }
    return module;
  }

  @Transactional()
  @Put('modules/:moduleId/move')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Move module to another course.' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  move(@Param('moduleId', new ParseUUIDPipe()) moduleId: string, @Body() moveModuleDto: MoveModuleDto) {
    return this.modulesService.move(moduleId, moveModuleDto.toCourseId, moveModuleDto.toSectionId);
  }

  @Transactional()
  @Post('modules/:moduleId/copy')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Copy module' })
  @ApiResponse({
    status: 200,
    description: 'Returns copied module',
  })
  copy(
    @Param('moduleId', new ParseUUIDPipe()) moduleId: string,
    // @Param('toCourseId', new ParseUUIDPipe()) toCourseId: string,
    @Req() req: Request,
    @Body() copyModuleDto: CopyModuleDto,
  ) {
    return this.modulesService.copy(moduleId, {
      toCourseId: copyModuleDto.toCourseId,
      toSectionId: copyModuleDto.toSectionId,
      userId: req.user.id,
      flatCopy: copyModuleDto.flatCopy,
    });
  }

  @Transactional()
  @Put('modules/:id/publish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Publish a Module' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  publish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.modulesService.publish(id);
  }

  @Transactional()
  @Put('modules/:id/unpublish')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Unpublish a Module' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  unpublish(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.modulesService.unpublish(id);
  }

  @Transactional()
  @Delete('modules/:id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Deletes a Module' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.modulesService.remove(id);
  }
}
