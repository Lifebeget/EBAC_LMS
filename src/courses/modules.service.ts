import { Injectable, HttpStatus, Inject, forwardRef, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, Repository } from 'typeorm';
import { Module } from '../lib/entities/module.entity';
import { CreateModuleDto } from './dto/create-module.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { Course } from '../lib/entities/course.entity';
import { Lecture } from '../lib/entities/lecture.entity';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { CopyModuleDto } from './dto/copy-module.dto';
import { LecturesService } from './lectures.service';
import { SectionsService } from './sections.service';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class ModulesService {
  constructor(
    @InjectRepository(Module)
    private modulesRepository: Repository<Module>,
    @InjectRepository(Course)
    private coursesRepository: Repository<Course>,
    @InjectRepository(Lecture)
    private readonly lecturesRepository: Repository<Lecture>,
    @Inject(forwardRef(() => SubmissionsService))
    private submissionsService: SubmissionsService,
    private lecturesService: LecturesService,

    @Inject(forwardRef(() => SectionsService))
    private sectionsService: SectionsService,
  ) {}

  async lastModuleNumber(courseId: string) {
    const course = await this.coursesRepository
      .createQueryBuilder('course')
      .andWhere('course.id = :courseId', { courseId })
      .addSelect('course.numberModulesFromZero')
      .getOne();

    const numberModulesFromZero = course.numberModulesFromZero;

    const modulesCount = await this.modulesRepository
      .createQueryBuilder('modules')
      .where('modules.course.id = :courseId', { courseId })
      .getCount();

    const numberDecrement = numberModulesFromZero ? 1 : 0;

    return modulesCount - numberDecrement;
  }

  findCourseModulesTitles(courseId: string) {
    return this.modulesRepository
      .createQueryBuilder('modules')
      .select('modules.id')
      .addSelect('modules.title')
      .addSelect('modules.number')
      .addSelect('modules.customNumber')
      .addSelect('modules.section_id')
      .where('modules.courseId = :courseId', { courseId })
      .orderBy('modules.sort', 'ASC')
      .getMany();
  }

  async create(courseId: string, createModuleDto: CreateModuleDto) {
    if (createModuleDto.published) {
      createModuleDto.publishedAt = new Date();
    }
    if (createModuleDto.customNumber === '') {
      createModuleDto.customNumber = null;
    }
    const lastModuleNumber = await this.lastModuleNumber(courseId);
    return this.modulesRepository.save({
      ...createModuleDto,
      course: { id: courseId },
      number: lastModuleNumber + 1,
    });
  }

  findAll(courseId: string, includeHidden?: boolean, sectionId?: string, sortRange?: number): Promise<Module[]> {
    const where: FindConditions<Module> = {};
    if (includeHidden) {
      where.course = { id: courseId };
    } else {
      where.course = { id: courseId, active: true, published: true };
      where.active = true;
      where.published = true;
    }
    if (sectionId) {
      where.section = { id: sectionId };
    }
    if (sortRange) {
      where.sort >= sortRange;
    }
    return this.modulesRepository.find({
      where,
      order: { sort: 'ASC' },
    });
  }

  findAllByCourseIds(courses: string[]) {
    return this.modulesRepository
      .createQueryBuilder('modules')
      .select('modules.id')
      .addSelect('modules.title')
      .addSelect('modules.number')
      .addSelect('modules.customNumber')
      .where('modules.course_id IN (:...courses)', { courses })
      .orderBy('module.sort', 'ASC')
      .getMany();
  }

  findAllRaw(courseId: string) {
    return this.modulesRepository.createQueryBuilder('module').andWhere('module.course.id = :courseId', { courseId }).getMany();
  }

  findOneRaw(id: string): Promise<Module> {
    return this.modulesRepository.findOne(id);
  }

  @Transactional()
  findOne(id: string, includeHidden: boolean): Promise<Module> {
    const qb = this.modulesRepository.createQueryBuilder('module');
    qb.leftJoinAndSelect('module.course', 'course').leftJoinAndSelect('module.lectures', 'lectures');

    if (!includeHidden) {
      qb.andWhere('module.active = true')
        .andWhere('module.published = true')
        .andWhere('course.active = true')
        .andWhere('course.published = true');
    }
    qb.andWhere('module.id = :id', { id });
    return qb.getOne();
  }

  async fixModuleLecturesOrder(module: Module, numberFromZero: boolean) {
    const promises = [];
    const diff = numberFromZero ? 0 : 1;

    module.lectures.forEach((lecture, lectureIndex) => {
      const lectureCurrentIndex = lectureIndex + diff;
      if (!lecture.number || lecture.number !== lectureCurrentIndex) {
        const updateLecturePromise = this.lecturesRepository.update(lecture.id, {
          number: lectureCurrentIndex,
        });
        promises.push(updateLecturePromise);
      }
    });
  }

  async update(id: string, updateModuleDto: UpdateModuleDto) {
    if (updateModuleDto.customNumber === '') {
      updateModuleDto.customNumber = null;
    }
    const updateFields = { ...updateModuleDto, id };
    const currentModule = await this.findOne(id, true);
    if (!currentModule) {
      throw new NotFoundException(`Module ${id} not found`);
    }
    if (currentModule.numberLecturesFromZero !== updateModuleDto.numberLecturesFromZero) {
      await this.fixModuleLecturesOrder(currentModule, updateModuleDto.numberLecturesFromZero);
    }
    if (updateFields.published === true) {
      if (currentModule.published === false) {
        updateFields.publishedAt = new Date();
      }
    }
    return this.modulesRepository.save(updateFields);
  }

  async move(id: string, courseId: string, sectionId?: string) {
    let section = null;
    if (sectionId) {
      section = await this.sectionsService.findOne(sectionId);
      if (!section) {
        throw new NotFoundException('Section not found');
      }
    }

    const modules = await this.findAll(courseId, false, section ? section.id : null);
    const position = modules.length > 0 ? modules[modules.length - 1].sort + 10 : 0;

    const currentModule = await this.modulesRepository.findOne(id);
    const oldCourseId = currentModule.courseId;
    currentModule.course = await this.coursesRepository.findOne(courseId);
    currentModule.section_id = sectionId;
    currentModule.sort = position;

    const [result] = await Promise.all([
      this.modulesRepository.save(currentModule),
      this.lecturesRepository.update({ moduleId: id }, { courseId }),
      this.submissionsService.updateSubmissionMoveModule(currentModule, oldCourseId, courseId),
    ]);

    if (section) {
      await this.updateSortModulesAndSectioin(courseId, section.id, position);
    }

    return result;
  }

  async copy(moduleId: string, dto: CopyModuleDto) {
    const oldModuleLectures = await this.lecturesRepository
      .createQueryBuilder('lectures')
      .where('lectures.moduleId = :moduleId', { moduleId })
      .select('lectures.id')
      .addSelect('lectures.courseId')
      .getMany();

    const oldModule = await this.findOne(moduleId, true);
    if (!oldModule) {
      throw new NotFoundException('Module not found');
    }

    let section = null;
    if (dto.toSectionId) {
      section = await this.sectionsService.findOne(dto.toSectionId);
      if (!section) {
        throw new NotFoundException('Section not found');
      }
    }

    const modules = await this.findAll(dto.toCourseId, false, section ? section.id : null);
    const position = modules.length > 0 ? modules[modules.length - 1].sort + 10 : 0;

    const newModule = await this.create(dto.toCourseId, {
      title: oldModule.title,
      description: oldModule.description,
      active: oldModule.active,
      published: oldModule.published,
      sort: position,
      section_id: section ? section.id : null,
      numberLecturesFromZero: oldModule.numberLecturesFromZero,
    });

    const lecturePromises = [];
    for (const oldLecture of oldModuleLectures) {
      const newLecturePromise = this.lecturesService.copy(oldLecture.id, {
        toModuleId: newModule.id,
        toCourseId: dto.toCourseId,
        userId: dto.userId,
        flatCopy: dto.flatCopy,
      });
      lecturePromises.push(newLecturePromise);
    }
    await Promise.all(lecturePromises);

    if (section) {
      await this.updateSortModulesAndSectioin(dto.toCourseId, section.id, position);
    }
  }
  async publish(id: string) {
    const currentModule = await this.modulesRepository.findOne(id);
    if (currentModule.published === false) {
      return this.modulesRepository.save({
        id,
        published: true,
        publishedAt: new Date(),
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already published',
      };
    }
  }

  async unpublish(id: string) {
    const currentModule = await this.modulesRepository.findOne(id);
    if (currentModule.published === true) {
      return this.modulesRepository.save({
        id,
        published: false,
        publishedAt: null,
      });
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already unpublished',
      };
    }
  }

  remove(id: string) {
    return this.modulesRepository.delete(id);
  }

  async removeList(modules: Module[]) {
    return this.modulesRepository.remove(modules);
  }

  purgeAll() {
    return this.modulesRepository.clear();
  }

  eaxBoxfindAll(): Promise<Module[]> {
    return this.modulesRepository.find({
      loadRelationIds: true,
    });
  }

  async findBySectionIds(sectionIds: string[]) {
    const modules = await this.modulesRepository
      .createQueryBuilder('modules')
      .leftJoinAndSelect('modules.course', 'course')
      .leftJoinAndSelect('modules.section', 'section')
      .where('section.id in (:...sectionIds)', { sectionIds })
      .orderBy('modules.sort', 'ASC')
      .getMany();

    const result = modules.map(el => ({
      id: el.id,
      title: el.title,
      number: el.number,
      customNumber: el.customNumber,
      isActive: el.active && el.published && el.section?.active && el.section?.published && el.course?.active && el.course?.published,
      courseId: el.courseId,
    }));

    return result;
  }

  async findByCourseId(courseId: string) {
    const modules = await this.modulesRepository
      .createQueryBuilder('modules')
      .leftJoinAndSelect('modules.course', 'course')
      .where('course.id = :courseId', { courseId })
      .orderBy('modules.sort', 'ASC')
      .getMany();

    const result = modules.map(el => ({
      id: el.id,
      title: el.title,
      number: el.number,
      customNumber: el.customNumber,
      isActive: el.active && el.published && el.course?.active && el.course?.published,
    }));

    return result;
  }
  async updateSortModulesAndSectioin(courseId: string, sectionId: string, position: number) {
    // get modules that sort range more then [position]
    const promiseModules = this.findAll(courseId, false, null, position);
    const promiseSection = this.sectionsService.findAll(courseId);

    const result = await Promise.all([promiseModules, promiseSection]);

    const modules = result[0];
    const sections = result[1].sort((a, b) => a.sort);
    const currentSectionIndex = sections.findIndex(section => section.id === sectionId);
    const updatedSectionIndex = currentSectionIndex < sections.length - 1 ? currentSectionIndex + 1 : null;

    // Increase sort value section and modules
    if (updatedSectionIndex) {
      const updatePromises = [];
      // Update field sort in all sections that under the current section(where copy module)
      for (let index = updatedSectionIndex; index < sections.length; index++) {
        const sectionToUpdate = sections[updatedSectionIndex];
        sectionToUpdate.sort += 10;
        const promiseSection = this.sectionsService.update(sectionToUpdate.id, { sort: sectionToUpdate.sort });
        updatePromises.push(promiseSection);

        // Update field sort in all filtered modules
        const modulesFiltered = modules.filter(item => item.section_id === sectionToUpdate.id);
        modulesFiltered.forEach(moduleToUpdate => {
          moduleToUpdate.sort += 10;
          const promise = this.modulesRepository.save(moduleToUpdate);
          updatePromises.push(promise);
        });
      }
      await Promise.all(updatePromises);
    }
  }

  async unbindModulesFromSection(sectionId) {
    return await this.modulesRepository
      .createQueryBuilder()
      .update()
      .set({ section_id: null })
      .where('section_id = :sectionId', { sectionId })
      .execute();
  }
}
