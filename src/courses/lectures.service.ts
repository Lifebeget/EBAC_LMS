import { Lecture } from '@entities/lecture.entity';
import { Module } from '@entities/module.entity';
import { ContentType } from '@enums/content-type.enum';
import { Permission } from '@enums/permission.enum';
import { AssessmentType, LectureContentsRelevance, SubmissionStatus } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { BadRequestException, forwardRef, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { LectureProgress } from 'src/lib/entities/lecture-progress.entity';
import { LectureStatus } from 'src/lib/enums/lecture-status.enum';
import { PaginationDto } from 'src/pagination.dto';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { ContentService } from './content.service';
import { CreateLectureDto } from './dto/create-lecture.dto';
import { UpdateLectureDto } from './dto/update-lecture.dto';
import { CopyLectureDto } from './dto/copy-lecture.dto';
import { FilesService } from '../files/files.service';
import { Question } from '@entities/question.entity';
import { Assessment } from '@entities/assessment.entity';
import { FileFolder } from '@lib/types/enums/file-folder.enum';
import { CoversService } from '../covers/covers.service';
import { LectureProgressTriggerService } from '../trigger/services/lecture-progress-trigger.service';
import { Content } from '@entities/content.entity';
import { Enrollment } from '@entities/enrollment.entity';
import { RedisService } from 'nestjs-redis';
import { v4 as uuidv4 } from 'uuid';
import { Submission } from '@entities/submission.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { LectureCounts } from '@lib/api/types';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class LecturesService {
  constructor(
    @InjectRepository(Lecture)
    private lecturesRepository: Repository<Lecture>,
    @InjectRepository(Module)
    private modulesRepository: Repository<Module>,
    @InjectRepository(LectureProgress)
    private lectureProgressRepository: Repository<LectureProgress>,
    @Inject(forwardRef(() => ContentService))
    private contentsService: ContentService,
    @Inject(forwardRef(() => CoversService))
    private coversService: CoversService,
    @Inject(forwardRef(() => SubmissionsService))
    private submissionsService: SubmissionsService,
    @Inject(forwardRef(() => FilesService))
    private filesService: FilesService,
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,
    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,
    @InjectRepository(ForumMessages)
    private forumMessagesRepository: Repository<ForumMessages>,
    @Inject(forwardRef(() => LectureProgressTriggerService))
    private readonly lectureProgressTriggerService: LectureProgressTriggerService,
    @InjectRepository(Enrollment)
    private enrollmentRepository: Repository<Enrollment>,
    private redisService: RedisService,
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,
  ) {}

  async lastLectureNumber(moduleId: string) {
    const module = await this.modulesRepository
      .createQueryBuilder('module')
      .andWhere('module.id = :moduleId', { moduleId })
      .addSelect('module.numberLecturesFromZero')
      .getOne();

    const numberLecturesFromZero = module?.numberLecturesFromZero;

    const lecturesCount = await this.lecturesRepository
      .createQueryBuilder('lectures')
      .where('lectures.module.id = :moduleId', { moduleId })
      .getCount();

    const numberDecrement = numberLecturesFromZero ? 1 : 0;

    return lecturesCount - numberDecrement;
  }

  async findOneRaw(id: string) {
    return this.lecturesRepository
      .createQueryBuilder('lecture')
      .leftJoin('lecture.image', 'image')
      .addSelect('image.url')
      .where('lecture.id = :id', { id })
      .getOne();
  }

  async findAllRaw(moduleId: string) {
    return this.lecturesRepository
      .createQueryBuilder('lecture')
      .leftJoin('lecture.image', 'image')
      .addSelect('image.url')
      .andWhere('lecture.module.id = :moduleId', { moduleId })
      .getMany();
  }

  async findAllLecturesProgress(pagination: PaginationDto, param: { courseId?: string; userId?: string }) {
    const qb = this.lectureProgressRepository.createQueryBuilder('lecture_progress');

    qb.leftJoinAndSelect('lecture_progress.lecture', 'lecture')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('lecture.course', 'course')
      .leftJoinAndSelect('lecture.image', 'image')
      .leftJoinAndSelect('lecture_progress.user', 'user')
      .andWhere('lecture.required = true');

    if (param.courseId) {
      qb.where('lecture.course.id = :courseId', param);
    }

    if (param.userId) {
      qb.andWhere('user.id = :userId', param);
    }

    qb.orderBy('lecture_progress.viewed', 'DESC');

    if (!pagination.showAll) {
      qb.skip((pagination.page - 1) * pagination.itemsPerPage);
      qb.take(pagination.itemsPerPage);
    }

    const [results, total] = await qb.getManyAndCount();

    return {
      results,
      meta: { ...pagination, total },
    };
  }

  @Transactional()
  findOneLecturesProgress({ lectureId, userId }: { lectureId: string; userId: string }) {
    return this.lectureProgressRepository.findOne({
      where: {
        lecture: { id: lectureId },
        user: { id: userId },
      },
      relations: ['user', 'lecture', 'lecture.course', 'lecture.image'],
    });
  }

  @Transactional()
  async setLectureStatus(userId: string, lectureId: string, status: LectureStatus) {
    const currentProgress = await this.findOneLecturesProgress({
      lectureId,
      userId,
    });
    const data = { status };
    if (status === LectureStatus.success && !currentProgress?.ended) {
      data['ended'] = new Date();
    }
    await this.lectureProgressRepository.save({
      user: { id: userId },
      lecture: { id: lectureId },
      ...data,
    });

    const analogs = await this.findAnalogs(lectureId);
    const lecture = await this.findOne(lectureId);
    analogs.forEach(toLecture => {
      this.dupLectureProgress(lecture, toLecture);
    });

    this.lectureProgressTriggerService.lectureProgressUpdate(userId, lectureId, status);

    return this.findOneLecturesProgress({ lectureId, userId });
  }

  async saveLectureProgress(
    userId: string,
    lectureId: string,
    param: {
      viewed?: Date;
      ended?: Date;
      progress?: number;
      status?: LectureStatus;
    },
  ) {
    if (!userId || !lectureId) {
      throw 'Missing required parameters';
    }
    const lecture = await this.findOne(lectureId, true);
    if (!lecture) {
      console.error(`Lecture not found while saving progress: ${lectureId}`);
      return {};
    }
    const lectureView = await this.lectureProgressRepository.findOne({
      lecture,
      user: { id: userId },
    });
    let ret: any;

    if (lectureView) {
      if (param.status === LectureStatus.success && !lectureView.ended) {
        param.ended = new Date();
      }
      ret = await this.lectureProgressRepository.update({ lectureId: lectureView.lectureId, userId: lectureView.userId }, param);
    }
    if (!ret) {
      const saveParams = {
        user: { id: userId },
        status: LectureStatus.inProgress,
        ...param,
        lecture: { id: lectureId },
      };

      ret = await this.lectureProgressRepository.save(saveParams);
    }

    const analogs = await this.findAnalogs(lecture.id);
    analogs.forEach(toLecture => {
      this.dupLectureProgress(lecture, toLecture);
    });

    this.lectureProgressTriggerService.lectureProgressUpdate(userId, lecture, ret.status);
    return ret;
  }

  async syncProgressForNewCourse(userId: string, courseId: string) {
    // Check progress for analogs
    // get lectures for course
    const allCourseLectures = await this.findAllLecturesForCourse(courseId);

    // for each lecture dup progress from analogs
    allCourseLectures.forEach(async l => {
      await this.dupProgressFromAnalogs(l, userId);
    });
  }

  async updateLectureProgressAfterReview(userId: string, lectureId: string) {
    const currentProgress = await this.findOneLecturesProgress({
      lectureId,
      userId,
    });
    if (currentProgress?.status == LectureStatus.success) {
      // What's GREEN is GREEN
      return;
    }
    let status: LectureStatus;
    const submissions = await this.submissionsService.getSubmissionsViewByLecture(userId, lectureId);
    if (submissions.every(s => s.success === true)) {
      // All lecture homeworks/quizes/surveys are complete
      status = LectureStatus.success;
    } else if (submissions.some(s => s.in_progress === true)) {
      // At least one of them in progress
      status = LectureStatus.inProgress;
    } else if (submissions.some(s => s.success === false)) {
      // At least one failed submission
      status = LectureStatus.failed;
    } else {
      // No submissions was ever submitted (s.success === null)
      status = LectureStatus.inProgress;
    }

    const data = { status };
    if (status === LectureStatus.success && !currentProgress.ended) {
      data['ended'] = new Date();
    }

    return this.lectureProgressRepository.save({
      user: { id: userId },
      lecture: { id: lectureId },
      ...data,
    });
  }

  async updateLectureProgressByAssessments(
    userId: string,
    lectureId: string,
    lectureDb: Lecture,
    param: {
      viewed?: Date;
      ended?: Date;
      progress?: number;
    } = {},
  ) {
    const lecture = lectureDb || (await this.findOne(lectureId));
    const submissions: any = await this.submissionsService.findForLectureProgress(userId, lectureId);

    const isFail = submissions.filter(e => e.status == SubmissionStatus.Graded && !e.ok).length > 0;
    if (isFail) {
      return this.lectureProgressRepository.save({
        ...param,
        user: { id: userId },
        lecture: { id: lectureId },
        status: LectureStatus.failed,
      });
    }

    const isSuccess =
      submissions.filter(e => e.status == SubmissionStatus.Graded && e.ok).length ==
      lecture.contents.filter(e => e.type == ContentType.task && e.assessment && e.assessment.type == AssessmentType.Default).length;
    if (isSuccess) {
      return this.lectureProgressRepository.save({
        ...param,
        user: { id: userId },
        lecture: { id: lectureId },
        status: LectureStatus.success,
      });
    }

    return this.lectureProgressRepository.save({
      ...param,
      user: { id: userId },
      lecture: { id: lectureId },
      status: LectureStatus.inProgress,
    });
  }

  async saveLectureProgressMany(
    datasList: {
      userId: string;
      lectureId: string;
      viewed?: Date;
      ended?: Date;
      progress?: number;
      status?: LectureStatus;
    }[],
    debug: boolean,
  ) {
    const checkDouble = {};
    const datas = datasList.filter(e => {
      if (checkDouble[e.lectureId + e.userId]) {
        return false;
      }
      checkDouble[e.lectureId + e.userId] = 1;
      return true;
    });

    const limit = 1000;
    const num = 1 + Math.floor(datas.length / limit);
    for (let index = 0; index < num; index++) {
      debug && console.log(`save ${index}/${num}`);
      const element = datas.splice(0, limit);

      try {
        await this.lectureProgressRepository.save(element);
      } catch (error) {
        console.error(error);
      }
    }
  }

  async create(dto: CreateLectureDto) {
    if (dto.published) {
      dto.publishedAt = new Date();
    }
    if (dto.customNumber === '') {
      dto.customNumber = null;
    }

    const courseLectures = await this.findAll(dto.courseId, null, true);
    const existingSlugs = courseLectures.map(l => l.slug);
    const newUniqueSlug = `${dto.slug}-${Date.now().toString()}`;
    const newLectureSlug = existingSlugs.includes(dto.slug) ? newUniqueSlug : dto.slug;

    const lastLectureNumber = await this.lastLectureNumber(dto.moduleId);
    const result = await this.lecturesRepository.save({
      ...dto,
      course: { id: dto.courseId },
      module: { id: dto.moduleId },
      // image: dto.imageId ? { id: dto.moduleId } : null,
      number: lastLectureNumber + 1,
      slug: newLectureSlug,
    });

    this.clearCache({ courseId: dto?.courseId });
    return result;
  }

  findQb(includeHidden?: boolean): SelectQueryBuilder<Lecture> {
    const qb = this.lecturesRepository.createQueryBuilder('lecture');
    if (!includeHidden) {
      qb.innerJoinAndSelect('lecture.course', 'course', 'course.active = true AND course.published = true');
      qb.innerJoinAndSelect('lecture.module', 'module', 'module.active = true AND module.published = true');
      qb.andWhere('lecture.active = true');
      qb.andWhere('lecture.published = true');
    } else {
      qb.leftJoinAndSelect('lecture.course', 'course');
      qb.leftJoinAndSelect('lecture.module', 'module');
    }
    qb.leftJoinAndSelect('lecture.image', 'image');
    return qb;
  }

  findAll(courseId?: string, moduleId?: string, includeHidden?: boolean, useCache?: boolean): Promise<Lecture[]> {
    const qb = this.findQb(includeHidden);
    if (courseId) {
      qb.andWhere('course.id = :courseId', { courseId });
    }
    if (moduleId) {
      qb.andWhere('module.id = :moduleId', { moduleId });
    }
    qb.leftJoinAndSelect('lecture.assessments', 'assessments');
    qb.leftJoin('lecture.contents', 'content').addSelect('content.type').addSelect('content.active').addSelect('content.id');

    qb.orderBy('lecture.sort', 'ASC');
    if (useCache) {
      qb.cache(`db:lecture:findAll:${courseId}:${moduleId}:${includeHidden}`);
    }
    return qb.getMany();
  }

  async findAllWithContentRelevance(moduleId: string) {
    const qb = this.lecturesRepository
      .createQueryBuilder('lecture')
      .where('lecture.moduleId = :moduleId', { moduleId })
      .leftJoinAndSelect('lecture.contents', 'content')
      .leftJoinAndSelect('content.assessment', 'assessment')
      .leftJoinAndSelect('assessment.questions', 'questions')
      .leftJoinAndSelect('content.files', 'files')
      .orderBy('lecture.sort', 'ASC');

    const lecturesWithContentRelevance: (Lecture & LectureContentsRelevance)[] = await qb.getMany();

    for (const lecture of lecturesWithContentRelevance) {
      lecture.contentsRelevance = {
        containsContent: lecture.contents.length > 0,
      };
      if (!lecture.contentsRelevance.containsContent) {
        continue;
      }
      for (const content of lecture.contents) {
        const isKaltura = content.type === ContentType.kaltura;
        const isTask = content.type === ContentType.task;
        const isText = content.type === ContentType.text;
        const isSurvey = content.type === ContentType.survey;
        const isFiles = content.type === ContentType.files;
        const isQuiz = content.type === ContentType.quiz;
        const isYoutube = content.type === ContentType.youtube;

        const assessmentIsSurvey = content.assessment?.type === AssessmentType.Survey;
        const assessmentIsDefault = content.assessment?.type === AssessmentType.Default;
        const assessmentIsQuiz = content.assessment?.type === AssessmentType.Quiz;
        const _assessmentIsGradedSurvey = content.assessment?.type === AssessmentType.GradedSurvey;

        const questionsContainsContent = content.assessment?.questions?.length > 0;

        if (isKaltura) {
          lecture.contentsRelevance.kaltura = {
            containsContent: !!content.entityId,
          };
        }
        if (isTask) {
          lecture.contentsRelevance.task = {
            containsContent: (assessmentIsDefault && questionsContainsContent) || (!!content.title && !!content.html),
            hasTitle: !!content.title,
            hasText: !!content.html,
            hasQuestions: assessmentIsSurvey,
            questionsContainsContent,
          };
        }
        if (isText) {
          lecture.contentsRelevance.text = {
            hasText: !!content.html,
            hasTitle: !!content.title,
            containsContent: !!content.html,
          };
        }
        if (isSurvey) {
          lecture.contentsRelevance.survey = {
            containsContent: assessmentIsSurvey && questionsContainsContent,
            hasTitle: !!content.title,
            hasQuestions: assessmentIsSurvey,
            questionsContainsContent,
          };
        }
        if (isFiles) {
          const filesHaveUrl = content.files?.every(file => file.url);
          lecture.contentsRelevance.files = {
            containsContent: content.files?.length > 0,
            filesHaveUrl,
          };
        }
        if (isQuiz) {
          lecture.contentsRelevance.quiz = {
            containsContent: assessmentIsQuiz && questionsContainsContent,
            hasTitle: !!content.title,
            hasQuestions: assessmentIsSurvey,
            questionsContainsContent,
          };
        }
        if (isYoutube) {
          lecture.contentsRelevance.youtube = {
            containsContent: !!content.link,
            hasTitle: !!content.title,
          };
        }
        lecture.contents = lecture.contents.map(content => {
          return {
            id: content.id,
            type: content.type,
            active: content.active,
          } as Content;
        });
      }
    }
    return lecturesWithContentRelevance;
  }

  async findOne(
    id: string,
    includeHidden?: boolean,
    includeContent?: boolean,
    includeGlobalAssessment?: boolean,
    useCache?: boolean,
  ): Promise<Lecture> {
    const qb = this.findQb(includeHidden);
    qb.andWhere('lecture.id = :id', { id });
    if (includeGlobalAssessment) {
      qb.leftJoin('lecture.contents', 'contents');
      qb.addSelect('contents.type');
      qb.addSelect('contents.active');
      qb.addSelect('contents.id');
      qb.leftJoinAndSelect('contents.assessment', 'assessment2', 'assessment2.currentVersion = :currentVersion', { currentVersion: true });
      qb.leftJoinAndSelect('assessment2.questions', 'questions2');
    } else if (includeContent) {
      qb.leftJoinAndSelect('lecture.contents', 'content');
    } else {
      qb.leftJoin('lecture.contents', 'content');
      qb.addSelect('content.type');
      qb.addSelect('content.active');
      qb.addSelect('content.id');
    }
    qb.leftJoinAndSelect('lecture.assessments', 'assessments');
    qb.leftJoinAndSelect('assessments.questions', 'questions');

    if (useCache) {
      qb.cache(`db:lecture:${id}:${includeHidden}:${includeGlobalAssessment}:${includeContent}`);
    }
    const lecture = await qb.getOne();

    if (includeGlobalAssessment && lecture) {
      const contentAssessments = lecture.contents
        .filter(e => (e.type == ContentType.task || ContentType.globalSurvey) && e.assessment)
        .map(e => e.assessment);
      lecture.assessments = [...lecture.assessments, ...contentAssessments];
    }
    return lecture;
  }

  @Transactional()
  async update(id: string, updateLectureDto: UpdateLectureDto) {
    if (updateLectureDto.customNumber === '') {
      updateLectureDto.customNumber = null;
    }
    const updateFields: any = { ...updateLectureDto, id };
    if (updateFields.published === true) {
      const currentLecture = await this.lecturesRepository.findOne(id);
      if (currentLecture.published === false) {
        updateFields.publishedAt = new Date();
      }
    }
    if ('imageId' in updateLectureDto) {
      updateFields.image = { id: updateLectureDto.imageId };
    }
    if ('moduleId' in updateLectureDto) {
      updateFields.module = { id: updateLectureDto.moduleId };
    }
    if (updateFields.assessments && !updateFields.assessments.length) {
      delete updateFields.assessments;
    }
    const result = await this.lecturesRepository.save(updateFields);
    const lectureDb = await this.lecturesRepository.findOne(id);

    this.clearCache({
      lectureId: lectureDb?.id,
      courseId: lectureDb?.courseId,
    });

    return result;
  }

  async move(movableLectureId: string, toModuleId: string) {
    const currentLecture = await this.lecturesRepository.findOne(movableLectureId);
    if (!currentLecture) {
      throw new BadRequestException('ERRORS.LECTURE_NOT_FOUND');
    }
    const oldModuleId = currentLecture.moduleId;
    const newModule = await this.modulesRepository.findOne(toModuleId);

    if (!newModule) {
      throw new BadRequestException('ERRORS.MODULE_NOT_FOUND');
    }
    const [result] = await Promise.all([
      this.lecturesRepository.update(
        { id: movableLectureId },
        {
          moduleId: toModuleId,
          courseId: newModule.courseId,
        },
      ),
      this.submissionsService.updateSubmissionMoveLecture(currentLecture, oldModuleId, toModuleId),
    ]);

    this.clearCache({
      lectureId: currentLecture?.id,
      courseId: currentLecture?.courseId,
    });

    return result;
  }

  async copy(lectureId: string, dto: CopyLectureDto) {
    const oldLecture = await this.findOne(lectureId, true, true);
    const oldContentsPromise = this.contentsService.findAllLecture(lectureId, dto.userId, true);
    const oldContents = dto.flatCopy ? [] : await oldContentsPromise;

    let newLectureImage = null;
    if (oldLecture.image) {
      newLectureImage = await this.filesService.uploadUrlAndCreate(oldLecture.image, FileFolder.covers);
    }
    const oldLectureCover = await this.coversService.findForLecture(oldLecture.id);

    delete oldLecture.id;
    delete oldLecture.assessments;
    delete oldLecture.contents;
    delete oldLecture.course;
    delete oldLecture.module;
    delete oldLecture.image;

    if (!dto.analogs) {
      delete oldLecture.analogId;
    }

    const newLecture = await this.create({
      ...oldLecture,
      courseId: dto.toCourseId,
      moduleId: dto.toModuleId,
      image: newLectureImage,
    });

    if (oldLectureCover) {
      await this.coversService.saveCover({
        ...oldLectureCover,
        id: undefined,
        lectureId: newLecture.id,
        moduleId: newLecture.moduleId,
        courseId: newLecture.courseId,
      });
    }

    const newContents = [];

    for (const oldContent of oldContents) {
      const oldFiles = oldContent?.files || [];
      const newContentFiles = [];
      for (const oldFile of oldFiles) {
        const fileSavedInDb = await this.filesService.uploadUrlAndCreate(oldFile, FileFolder.contentFiles);

        newContentFiles.push(fileSavedInDb);
      }
      const oldAssessment = oldContent?.assessment;

      let newAssessment: Assessment;
      if (oldAssessment?.isGlobal) {
        newAssessment = await this.assessmentsRepository.findOne({
          commonId: oldAssessment.commonId,
          currentVersion: true,
        });
      } else if (oldAssessment) {
        const oldQuestions = oldAssessment.questions || [];
        const newQuestions: Question[] = [];
        for (const oldQuestion of oldQuestions) {
          const oldQuestionFiles = oldQuestion?.files || [];
          const newQuestionFiles = [];
          for (const oldQuestionFile of oldQuestionFiles) {
            const fileSavedInDb = await this.filesService.uploadUrlAndCreate(oldQuestionFile, FileFolder.contentFiles);

            newQuestionFiles.push(fileSavedInDb);
          }
          delete oldQuestion.id;
          const newQuestion = await this.questionsRepository.save({
            ...oldQuestion,
            files: newQuestionFiles,
          });
          newQuestions.push(newQuestion);
        }
        delete oldAssessment.id;
        newAssessment = await this.assessmentsRepository.save({
          ...oldAssessment,
          lecture: { id: newLecture.id },
          questions: newQuestions,
          commonId: uuidv4(),
        });
      }

      delete oldContent.id;
      const newContent = await this.contentsService.create(newLecture.id, {
        ...oldContent,
        files: newContentFiles,
        assessment: newAssessment,
      });
      newContents.push(newContent);
    }
    return newLecture;
  }

  async publish(id: string) {
    const currentLecture = await this.lecturesRepository.findOne(id);
    if (currentLecture.published === false) {
      const result = await this.lecturesRepository.save({
        id,
        published: true,
        publishedAt: new Date(),
      });

      this.clearCache({
        lectureId: currentLecture?.id,
        courseId: currentLecture?.courseId,
      });

      return result;
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already published',
      };
    }
  }

  async unpublish(id: string) {
    const currentLecture = await this.lecturesRepository.findOne(id);
    if (currentLecture.published === true) {
      const result = this.lecturesRepository.save({
        id,
        published: false,
        publishedAt: null,
      });

      this.clearCache({
        lectureId: currentLecture?.id,
        courseId: currentLecture?.courseId,
      });

      return result;
    } else {
      return {
        statusCode: HttpStatus.NOT_MODIFIED,
        message: 'Already unpublished',
      };
    }
  }

  async remove(id: string) {
    const lectureDb = await this.lecturesRepository.findOne(id);
    const result = await this.lecturesRepository.delete(id);

    this.clearCache({
      lectureId: lectureDb?.id,
      courseId: lectureDb?.courseId,
    });

    return result;
  }

  async removeList(lectures: Lecture[]) {
    const result = await this.lecturesRepository.remove(lectures);
    for (let index = 0; index < lectures.length; index++) {
      this.clearCache({
        lectureId: lectures[index]?.id,
      });
    }
    return result;
  }

  purgeAll() {
    return this.lecturesRepository.clear();
  }

  async checkRights(lecture: Lecture, user: Express.User) {
    const course = lecture?.course;
    const hasViewPerm = user.hasPermission(Permission.COURSE_VIEW);
    const hasEnrollment =
      user.enrolledAsSupport(course?.id) ||
      (user.enrolledAsStudent(course?.id) &&
        (user.coursePermissions[lecture.courseId]?.canWatchAllLessons ||
          (lecture.free && user.coursePermissions[lecture.courseId]?.canWatchFreeLessons))) ||
      user.enrolledAsTutor(course?.id) ||
      user.enrolledAsTeamlead(course?.id) ||
      user.enrolledAsModerator(course?.id);
    return !!(hasViewPerm || hasEnrollment);
  }

  async checkRightsOrFree(lecture: Lecture, user: Express.User) {
    const isCheckRights = await this.checkRights(lecture, user);
    return isCheckRights || (user.coursePermissions[lecture.courseId]?.canWatchFreeLessons && lecture?.free);
  }

  async checkRightsOrFreeId(lectureId: string, user: Express.User) {
    const lecture = await this.findOne(lectureId);
    const checkRightsOrFree = await this.checkRightsOrFree(lecture, user);
    return checkRightsOrFree;
  }

  async isFree(lectureId: string) {
    const lecture = await this.findOne(lectureId);
    return lecture?.free;
  }

  async findAnalogs(lectureId: string) {
    const lecture = await this.lecturesRepository.findOne(lectureId);
    return await this.lecturesRepository
      .createQueryBuilder('lecture')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('lecture.course', 'course')
      .andWhere('lecture.analog_id = :analogId', {
        analogId: lecture.analogId,
      })
      .andWhere('lecture.id != :lectureId', {
        lectureId,
      })
      .getMany();
  }

  async bindLectures(firstLectureId: string, secondLectureId: string) {
    const firstLecture = await this.lecturesRepository.findOne(firstLectureId);
    const secondLecture = await this.lecturesRepository.findOne(secondLectureId);

    //any lecture got analog id - set first as 'parent'
    if (!firstLecture.analogId && !secondLecture.analogId) {
      await this.lecturesRepository.save({
        ...firstLecture,
        analogId: firstLecture.id,
      });
      await this.lecturesRepository.save({
        ...secondLecture,
        analogId: firstLecture.id,
      });

      //first lecture got analog id - update second
    } else if (firstLecture.analogId && !secondLecture.analogId) {
      await this.lecturesRepository.save({
        ...secondLecture,
        analogId: firstLecture.id,
      });

      //second lecture got analog id - update first
    } else if (!firstLecture.analogId && secondLecture.analogId) {
      await this.lecturesRepository.save({
        ...firstLecture,
        analogId: secondLecture.analogId,
      });

      //both got analog id - need to merge
    } else {
      const firstGroup = await this.lecturesRepository
        .createQueryBuilder('lecture')
        .andWhere('analog_id = :analogId', {
          analogId: firstLecture.analogId,
        })
        .getMany();

      const secondGroup = await this.lecturesRepository
        .createQueryBuilder('lecture')
        .andWhere('analog_id = :analogId', {
          analogId: secondLecture.analogId,
        })
        .getMany();

      if (firstGroup.length > secondGroup.length) {
        secondGroup.map(el => (el.analogId = firstLecture.analogId));
        this.lecturesRepository.save(secondGroup);
      } else {
        firstGroup.map(el => (el.analogId = secondLecture.analogId));
        this.lecturesRepository.save(firstGroup);
      }
    }

    await this.dupLectureProgress(firstLecture, secondLecture);
    await this.dupLectureProgress(secondLecture, firstLecture);

    return this.lecturesRepository.findOne(secondLecture.id, {
      relations: ['course', 'module'],
    });
  }

  async dupLectureProgress(fromLecture: Lecture, toLecture: Lecture, userId: string | null = null) {
    //Get all lecture progress for first lecture
    const progressQuery = await this.lectureProgressRepository.createQueryBuilder('lecture_progress').andWhere('lecture_id = :id', {
      id: fromLecture.id,
    });

    let lectureProgress = null;

    //get progress only for user, if userId provided
    if (userId) {
      lectureProgress = await progressQuery.andWhere('user_id = :userId', { userId }).getMany();
    } else {
      lectureProgress = await progressQuery.getMany();
    }

    //Get enrollments for students
    const enrollmentQuery = await this.enrollmentRepository
      .createQueryBuilder('enrollment')
      .leftJoinAndSelect('enrollment.user', 'user')
      .andWhere('course_id = :id', {
        id: fromLecture.courseId,
      })
      .andWhere('role = :role', {
        role: CourseRole.Student,
      });

    let lectureCourseEnrollments = [];

    if (userId) {
      lectureCourseEnrollments = await enrollmentQuery.andWhere('user_id = :userId', { userId }).getMany();
    } else {
      lectureCourseEnrollments = await enrollmentQuery.getMany();
    }

    //We need only user's ids
    const usersIds = lectureCourseEnrollments.map(enr => enr.user?.id);

    const newLectureProgress = lectureProgress
      // filter lecture progress only for users
      .filter(l => usersIds.includes(l.userId))
      // remove id and change lectureId
      .map(l => {
        return {
          ...l,
          lectureId: toLecture.id,
          lecture: toLecture,
          id: undefined,
        };
      });

    await this.lectureProgressRepository.save(newLectureProgress);
  }

  async dupProgressFromAnalogs(lecture, userId) {
    //find analogs for lecture
    const analogs = await this.findAnalogs(lecture.id);
    //dup progress for current users
    analogs.forEach(async analog => {
      await this.dupLectureProgress(analog, lecture, userId);
    });
  }

  async findAllLecturesForCourse(courseId: string) {
    return this.lecturesRepository.createQueryBuilder('lectures').andWhere('course_id = :id', { id: courseId }).getMany();
  }

  async clearCache(params: { lectureId?: string; courseId?: string }) {
    if (!params.lectureId && !params.courseId) {
      return;
    }
    try {
      let pattern;
      if (params.lectureId) {
        pattern = `db:lecture:${params.lectureId}*`;
      } else if (params.courseId) {
        pattern = `db:lecture:findAll:${params.courseId}*`;
      }

      const client = await this.redisService.getClient();
      client.scan(0, 'match', pattern).then(keys => {
        for (let index = 0; index < keys[1].length; index++) {
          client.unlink(keys[1][index]).catch(error => console.error(error));
        }
      });
    } catch (error) {
      console.error(error);
    }
  }

  async getRelationForDelete(lectureId: string) {
    const lectureWithCounts: Lecture & LectureCounts = await this.lecturesRepository
      //selectLecture
      .createQueryBuilder('lecture')
      .andWhere('lecture.id = :lectureId', { lectureId })

      //count content blocks
      .leftJoin('lecture.contents', 'content')
      .loadRelationCountAndMap('lecture.countContents', 'lecture.contents')

      //count assessments
      .leftJoin('lecture.assessments', 'assessments')
      .loadRelationCountAndMap('lecture.countAssessments', 'lecture.assessments')

      //count lecture progress
      .leftJoin('lecture.lectureProgress', 'lectureProgress')
      .loadRelationCountAndMap('lecture.countLectureProgress', 'lecture.lectureProgress')
      .getOne();

    //count user submissions (hasn't described relation in lecture)
    lectureWithCounts.countSubmissions = await this.submissionsRepository
      .createQueryBuilder('submission')
      .andWhere('lecture_id = :lectureId', { lectureId })
      .getCount();

    //count Forum Messages (hasn't described relation in lecture)
    lectureWithCounts.countForumMessages = await this.forumMessagesRepository
      .createQueryBuilder('submission')
      .andWhere('lecture_id = :lectureId', { lectureId })
      .getCount();

    return lectureWithCounts;
  }

  async deleteSoftWithAttachedContent(lectureId: string) {
    const lecture = await this.lecturesRepository.findOne(lectureId, {
      relations: ['contents'],
    });

    for (const index in lecture.contents) {
      await this.contentsService.remove(lecture.contents[index].id);
    }

    await this.lectureProgressRepository.softDelete({
      lectureId: lecture.id,
    });

    await this.forumMessagesRepository.softDelete({
      lectureId: lecture.id,
    });

    /**
     * We can collect statistic about affected rows in content and lecture progress.
     * And return this combined with result. But shoud we?
     */

    const result = await this.lecturesRepository.softDelete(lectureId);

    this.clearCache({
      lectureId: lecture.id,
      courseId: lecture.courseId,
    });

    return result;
  }

  async deleteLectureProgress(lectureId: string, mode = 'hard') {
    if (mode === 'hard') {
      return await this.lectureProgressRepository.delete({ lectureId });
    } else if (mode === 'soft') {
      return await this.lectureProgressRepository.softDelete({ lectureId });
    }
  }

  async updateLectureProgress(lectureId: string, userId: string, status: LectureStatus) {
    const currentProgress = await this.findOneLecturesProgress({
      lectureId,
      userId,
    });
    const data = { status };
    if (status === LectureStatus.success && !currentProgress.ended) {
      data['ended'] = new Date();
    }
    return this.lectureProgressRepository.update({ lectureId, userId }, data);
  }

  async findForAdminPanel(moduleId: string) {
    const qb = this.lecturesRepository
      .createQueryBuilder('lecture')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoinAndSelect('lecture.course', 'course')
      .where('module.id = :moduleId', { moduleId })
      .orderBy('lecture.sort', 'ASC');

    const sections = await qb.getMany();
    const result = sections.map(el => ({
      id: el.id,
      title: el.title,
      number: el.number,
      customNumber: el.customNumber,
      isActive:
        el.active &&
        el.published &&
        el.course?.active &&
        el.course?.published &&
        el.module?.active &&
        el.module?.published &&
        (el.module?.section ? el.module?.section?.active && el.module?.section?.published : true),
    }));
    return result;
  }
}
