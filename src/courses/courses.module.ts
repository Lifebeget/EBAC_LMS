import { Trigger } from '@entities/trigger.entity';
import { User } from '@entities/user.entity';
import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { Assessment } from 'src/lib/entities/assessment.entity';
import { Content } from 'src/lib/entities/content.entity';
import { File } from 'src/lib/entities/file.entity';
import { LectureEvaluation } from 'src/lib/entities/lecture-evaluation.entity';
import { LectureProgress } from 'src/lib/entities/lecture-progress.entity';
import { Note } from 'src/lib/entities/note.entity';
import { Question } from 'src/lib/entities/question.entity';
import { Section } from 'src/lib/entities/section.entity';
import { Tag } from 'src/lib/entities/tag.entity';
import { TagsModule } from 'src/tags/tags.module';
import { KalturaModule } from '../kaltura/kaltura.module';
import { Course, CourseToSatellite } from '../lib/entities/course.entity';
import { Enrollment } from '../lib/entities/enrollment.entity';
import { Lecture as LectureEntity } from '../lib/entities/lecture.entity';
import { Module as ModuleEntity } from '../lib/entities/module.entity';
import { ContentController } from './content.controller';
import { ContentService } from './content.service';
import { CoursesController } from './courses.controller';
import { CourseResolver } from './courses.resolver';
import { CoursesService } from './courses.service';
import { EnrollmentsController } from './enrollments.controller';
import { EnrollmentsService } from './enrollments.service';
import { LectureEvaluationsController } from './lecture-evaluations.controller';
import { LectureEvaluationsService } from './lecture-evaluations.service';
import { LecturesController } from './lectures.controller';
import { LecturesResolver } from './lectures.resolver';
import { LecturesService } from './lectures.service';
import { ModulesController } from './modules.controller';
import { ModuleResolver } from './modules.resolver';
import { ModulesService } from './modules.service';
import { NotesController } from './notes.controller';
import { NotesService } from './notes.service';
import { SectionsController } from './sections.controller';
import { SectionsService } from './sections.service';
import { UsersModule } from '../users/users.module';
import { UserActivity } from '@entities/user-activity.entity';
import { Webinar } from '@entities/webinars.entity';
import { AmoModule } from '../amo/amo.module';
import { SettingsModule } from 'src/settings/settings.module';
import { Cover } from '@entities/cover.entity';
import { ViewUserProgressCount } from 'src/lib/views/view-user-progress-count.entity';
import { ForumModule } from 'src/forum/forum.module';
import { ForumMessages } from '@entities/forum-messages.entity';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { EnrollmentsHelper } from '@utils/enrollments.helper';
import { Segment } from '@entities/segment.entity';
import { TriggerModule } from 'src/trigger/trigger.module';
import { FilesModule } from 'src/files/files.module';
import { CoversModule } from 'src/covers/covers.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([LectureProgress]),
    TypeOrmModule.forFeature([Course]),
    TypeOrmModule.forFeature([CourseToSatellite]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([ModuleEntity]),
    TypeOrmModule.forFeature([LectureEntity]),
    TypeOrmModule.forFeature([Enrollment]),
    TypeOrmModule.forFeature([Assessment]),
    TypeOrmModule.forFeature([Question]),
    TypeOrmModule.forFeature([File]),
    TypeOrmModule.forFeature([Note]),
    TypeOrmModule.forFeature([Content]),
    TypeOrmModule.forFeature([LectureEvaluation]),
    TypeOrmModule.forFeature([Section]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([UserActivity]),
    TypeOrmModule.forFeature([Webinar]),
    TypeOrmModule.forFeature([Cover]),
    TypeOrmModule.forFeature([ViewUserProgressCount]),
    TypeOrmModule.forFeature([ForumMessages]),
    TypeOrmModule.forFeature([FeatureFlag]),
    TypeOrmModule.forFeature([Segment]),
    TagsModule,
    forwardRef(() => ForumModule),
    forwardRef(() => AssessmentsModule),
    forwardRef(() => UsersModule),
    forwardRef(() => KalturaModule),
    forwardRef(() => SettingsModule),
    forwardRef(() => TriggerModule),
    forwardRef(() => AmoModule),
    forwardRef(() => FilesModule),
    forwardRef(() => CoversModule),
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.JWT_TOKEN_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRATION_TIME,
        },
      }),
    }),
  ],
  controllers: [
    CoursesController,
    ModulesController,
    SectionsController,
    LecturesController,
    EnrollmentsController,
    NotesController,
    ContentController,
    LectureEvaluationsController,
  ],
  providers: [
    CoursesService,
    ModulesService,
    LecturesService,
    EnrollmentsService,
    NotesService,
    ContentService,
    LectureEvaluationsService,
    SectionsService,
    CourseResolver,
    ModuleResolver,
    LecturesResolver,
    EnrollmentsHelper,
  ],
  exports: [CoursesService, LecturesService, EnrollmentsService, ModulesService, ContentService, SectionsService],
})
export class CoursesModule {}
