import { KalturaService } from './../kaltura/kaltura.service';
import { FilesService } from './../files/files.service';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Between, Repository } from 'typeorm';
import { Note } from '../lib/entities/note.entity';
import { CreateNoteDto } from './dto/create-note.dto';
import { QueryNoteDto } from './dto/query-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';

const defaultRelations = ['owner', 'lecture', 'course', 'content', 'lecture.module'];

const singleNoteRelations = ['owner', 'lecture', 'course', 'content', 'lecture.module', 'files'];

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note)
    private notesRepository: Repository<Note>,
    private readonly filesService: FilesService,
    private readonly kalturaService: KalturaService,
  ) {}

  async create(courseId: string, dto: CreateNoteDto, ownerId: string) {
    const entity = {
      course: { id: courseId },
      content: dto.contentId ? { id: dto.contentId } : null,
      lecture: dto.lectureId ? { id: dto.lectureId } : null,
      timecode: dto.timecode,
      quote: dto.quote,
      anchor: dto.anchor,
      position: dto.position,
      html: dto.html,
      owner: { id: ownerId },
      files: [],
    };

    if (dto.file) {
      try {
        const thumb = await this.kalturaService.getThumbFile(dto.file.videoId, dto.file.timecode);

        const file = await this.filesService.create({ ...thumb, ownerId });
        entity.files = [file];
      } catch (error) {
        throw new UnprocessableEntityException('ERRORS.NOTES.CANT_SAVE_FILE');
      }
    }

    return this.notesRepository.save(entity);
  }

  findAll(query: QueryNoteDto = {}): Promise<Note[]> {
    const where: any = {};

    if (query.contentId) where.content = query.contentId;
    if (query.lectureId) where.lecture = query.lectureId;
    if (query.ownerId) where.owner = query.ownerId;
    if (query.courseId) where.course = query.courseId;
    if (query.deleted) {
      where.deleted = query.courseId;
    } else {
      where.deleted = null;
    }
    if (query.dateCreated) {
      where.created = Between(
        dayjs(query.dateCreated).startOf('day').toDate(),
        dayjs(query.dateCreated).startOf('day').add(1, 'day').toDate(),
      );
    }

    return this.notesRepository.find({
      where,
      order: { created: 'ASC' },
      relations: defaultRelations,
    });
  }

  @Transactional()
  findAllOwn(courseId: string, ownerId: string): Promise<Note[]> {
    return this.notesRepository.find({
      relations: ['lecture', 'lecture.module', 'files'],
      where: { course: courseId, deleted: null, owner: ownerId },
      order: { created: 'ASC' },
    });
  }

  @Transactional()
  findOne(id: string): Promise<Note> {
    return this.notesRepository.findOne(id, {
      relations: defaultRelations,
    });
  }

  @Transactional()
  findOneOwn(id: string, ownerId: string): Promise<Note> {
    return this.notesRepository.findOne(id, {
      where: { owner: ownerId },
      relations: singleNoteRelations,
    });
  }

  async update(id: string, dto: UpdateNoteDto) {
    const entity: any = { id };
    if (dto.lectureId) entity.lecture = { id: dto.lectureId };
    if (dto.anchor) entity.anchor = dto.anchor;
    if (dto.html) entity.html = dto.html;
    if (dto.position) entity.position = dto.position;
    if (dto.quote) entity.quote = dto.quote;
    if (dto.timecode) entity.timecode = dto.timecode;

    return this.notesRepository.update(id, entity);
  }

  remove(id: string) {
    const updateFields: Partial<Note> = { deleted: new Date(), id };
    return this.notesRepository.save(updateFields);
  }

  restore(id: string) {
    const updateFields: Partial<Note> = { deleted: null, id };
    return this.notesRepository.save(updateFields);
  }
}
