import { CourseRole } from '@lib/types/enums/course-role.enum';
import {
  Body,
  Controller,
  Delete,
  Get,
  BadRequestException,
  ForbiddenException,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { useCache } from 'src/helpers/cache';
import { Content } from 'src/lib/entities/content.entity';
import { Pagination } from 'src/pagination.decorator';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { ContentService } from './content.service';
import { CreateContentDto } from './dto/create-content.dto';
import { QueryContentDto } from './dto/query-content.dto';
import { VersionUpdateDTO } from './dto/update-content-version-update.dto';
import { UpdateContentDto } from './dto/update-content.dto';
import { LecturesService } from './lectures.service';

@ApiTags('content')
@ApiBearerAuth()
@Controller()
export class ContentController {
  constructor(private readonly contentService: ContentService, private readonly lecturesService: LecturesService) {}

  @Transactional()
  @Post('lectures/:lectureId/content')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Create a new content' })
  @ApiResponse({ status: 201, description: 'Created content', type: Content })
  @PgConstraints()
  create(@Req() req: Request, @Param('lectureId') lectureId, @Body() dto: CreateContentDto) {
    return this.contentService.create(lectureId, dto, req.user?.id);
  }

  @Transactional()
  @Post('lectures/versionUpdate')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Create/Update content' })
  @ApiResponse({
    status: 201,
    description: 'Created/updated content',
    type: Content,
  })
  @PgConstraints()
  versionUpdate(@Req() req: Request, @Body() dto: VersionUpdateDTO) {
    if (!dto.create && !dto.update && !dto.delete) return {};

    ['create', 'update'].map(function (key) {
      dto[key].map(function (item) {
        if (item.type == 'task' && !item.assessment?.levelId) {
          throw new BadRequestException('Empty service level in task');
        }

        if (item.type == 'task' && item.assessment?.maxAttempts < 1) {
          throw new BadRequestException('Max attempts value should be greater than 0');
        }
      });
    });

    return this.contentService.versionUpdate(dto, req.user?.id);
  }

  @Transactional()
  @Get('lectures/:lectureId/content-transactional')
  @ApiOperation({ summary: 'List all contents in lecture [Transactional]' })
  @ApiResponse({ status: 200, type: [Content] })
  async findAllLectureTransactional(@Param('lectureId', new ParseUUIDPipe()) lectureId: string, @Req() req: Request) {
    return await this.findAllLecture(lectureId, req);
  }

  @Get('lectures/:lectureId/content')
  @ApiOperation({ summary: 'List all contents in lecture' })
  @ApiResponse({ status: 200, type: [Content] })
  async findAllLecture(@Param('lectureId', new ParseUUIDPipe()) lectureId: string, @Req() req: Request) {
    const includeHidden = req.user.permissions.includes(Permission.COURSE_VIEW);
    const lecture = await this.lecturesService.findOne(lectureId, includeHidden, undefined, undefined, useCache(req));

    if (!lecture) {
      throw new NotFoundException('ERRORS.LECTURE_NOT_FOUND');
    } else if (!(await this.lecturesService.checkRightsOrFree(lecture, req.user))) {
      throw new ForbiddenException('ERRORS.LECTURE_ACCESS_FORBIDDEN');
    }

    const result = await this.contentService.findAllLecture(lectureId, req.user?.id, includeHidden, useCache(req));

    return this.clearAnswers(result, req.user);
  }

  private clearAnswers(result, user) {
    if (user.isAdmin() || user.isSuperadmin() || user.hasPermission(Permission.COURSE_EDIT)) return result;

    for (const content of result) {
      for (const question of content.assessment?.questions || []) {
        for (const answer of (<any>question.data)?.answers || []) {
          delete answer.isCorrect;
        }
      }
    }

    return result;
  }

  private async checkCourseRights(lectureId: string, user: Express.User): Promise<boolean> {
    const hasViewPerm = user.permissions.includes(Permission.COURSE_VIEW);

    // Active + Published checks on course and lecture are performed by lecture service
    const lecture = await this.lecturesService.findOne(lectureId, hasViewPerm);
    if (!lecture) {
      return false;
    }

    const hasEnrollment = user.enrollments
      .filter(e => e.courseId == lecture.course.id)
      .some(
        e =>
          (e.role == CourseRole.Student &&
            (user.coursePermissions[lecture.course.id].canWatchAllLessons ||
              (lecture.free && user.coursePermissions[lecture.course.id].canWatchFreeLessons))) ||
          e.role == CourseRole.Tutor,
      );

    if (!hasViewPerm && !hasEnrollment) {
      return false;
    }
    return true;
  }

  @Get('content')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all contents' })
  @ApiResponse({ status: 200, type: [Content] })
  findAll(@Query() query: QueryContentDto, @Pagination() paging: PaginationDto): Promise<Paginated<Content[]>> {
    return this.contentService.findAll(paging, query);
  }

  @Get('content/:id')
  @ApiOperation({ summary: 'Get one content entity' })
  @ApiResponse({ status: 200, type: [Content] })
  async find(@Param('id') id: string, @Req() req: Request) {
    const entity = await this.contentService.findOne(id);
    if (!entity) throw new NotFoundException('ERRORS.CONTENT_NOT_FOUND');
    const hasRights = await this.checkCourseRights(entity.lecture.id, req.user);
    if (!hasRights) throw new ForbiddenException();
    const includeHidden = req.user.permissions.includes(Permission.COURSE_VIEW);
    if (!includeHidden && !entity.active) {
      throw new NotFoundException('ERRORS.CONTENT_NOT_FOUND');
    }
    return entity;
  }

  @Transactional()
  @Patch('content/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Update content entity' })
  @ApiResponse({ status: 200, type: [Content] })
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() dto: UpdateContentDto) {
    return this.contentService.update(id, dto);
  }

  @Transactional()
  @Delete('content/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Deletes a content' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.contentService.remove(id);
  }

  @Get('content/:id/relations-to-delete')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Get content relations count' })
  async getRelationForDelete(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.contentService.getRelationForDelete(id);
  }
}
