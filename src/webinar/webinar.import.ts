import { File } from '@entities/file.entity';
import { Webinar } from '@entities/webinars.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import { FilesService } from 'src/files/files.service';
import { EadBoxDownloadHelper } from 'src/integration/eadBox.download.helper';
import { Repository } from 'typeorm';
import { CmsWebinar } from '../lib/types/CmsWebinar';
import { ImportOptions } from '../lib/types/ImportOptions';

export class ImportWebinars {
  private webinarsRepository: Repository<Webinar>;
  private apiKey: string;
  private url: string;
  private filesService: FilesService;
  private download: EadBoxDownloadHelper;

  constructor(context: INestApplicationContext) {
    this.webinarsRepository = context.get(getRepositoryToken(Webinar));
    this.apiKey = process.env.CMS_API_KEY;
    this.url = process.env.CMS_WEBINARS_URL;
    strict.ok(this.apiKey);
    strict.ok(this.url);
    this.download = new EadBoxDownloadHelper(process.env.EADBOX_API_URL, process.env.EADBOX_API_KEY);
    this.filesService = context.get(FilesService);
  }

  async getBatch(batchSize: number, batchIndex: number) {
    const res = await axios.get<{ data: { fields: CmsWebinar[] } }>(this.url, {
      params: {
        offset: batchIndex * batchSize,
        limit: batchSize,
        apiKey: this.apiKey,
      },
    });
    return {
      index: batchIndex,
      size: batchSize,
      items: res.data.data.fields,
    };
  }

  async *getItems(batchSize: number) {
    let batchIndex = 0;
    while (true) {
      const batch = await this.getBatch(batchSize, batchIndex);
      if (!batch.items.length) break;
      for (const item of batch.items) {
        yield { item, batch };
      }
      batchIndex += 1;
    }
  }

  async import({ pageSize = 500, noDownload = false }: ImportOptions) {
    let created = 0;
    let updated = 0;
    let batchIndex;

    console.log(`➡️ Importing webinars`, { pageSize });

    for await (const { item: cmsWebinar, batch } of this.getItems(pageSize)) {
      if (batchIndex !== batch.index) {
        batchIndex = batch.index;
        console.log(`📃 page ${batchIndex + 1}`);
      }
      console.log(`${cmsWebinar.slug}...`);
      const lmsWebinar: Partial<Webinar> = {
        externalId: cmsWebinar.id,
        slug: cmsWebinar.slug,
        name: cmsWebinar.name,
        type: cmsWebinar.type,
        additional: cmsWebinar.additional,
        subtitle: cmsWebinar.subtitle,
        startDateUtc: new Date(cmsWebinar.start_date_utc),
        startDateText: cmsWebinar.start_date_text,
        active: cmsWebinar.active,
        speakerBiography: cmsWebinar.speaker_biography,
        description: cmsWebinar.description,
        directionId: cmsWebinar.direction_id,
        youtubePlaylistId: cmsWebinar.youtube_playlist_id,
        recordLink: cmsWebinar.record_link,
        created: new Date(cmsWebinar.created),
        modified: new Date(cmsWebinar.modified),
        rev: cmsWebinar.rev,
        published: cmsWebinar.published,
        finished: false,
        deleted: cmsWebinar.deleted ? new Date(cmsWebinar.deleted) : null,
        pageUrl: cmsWebinar.page_url,
        pageUrlOriginal: cmsWebinar.page_url_generated,
        finalStartDateUtc: null, // TODO Use last topic start date
        public: cmsWebinar.public,
      };

      const existingLmsWebinar = await this.webinarsRepository.findOne(
        { externalId: lmsWebinar.externalId },
        { withDeleted: true, relations: ['showcaseImage'] },
      );

      const cmsImageUrl = cmsWebinar.file_showcase_image?.url;

      if (existingLmsWebinar) {
        // Update existing webinar
        // Replace image (if changed)
        const lmsImageUrl = existingLmsWebinar.showcaseImage?.eadboxUrl;
        let oldImage: File;
        if (!noDownload && cmsImageUrl && cmsImageUrl !== lmsImageUrl) {
          oldImage = existingLmsWebinar.showcaseImage;
          lmsWebinar.showcaseImage = await this.uploadImage(cmsImageUrl);
        }
        // Save webinar
        await this.webinarsRepository.save({
          ...existingLmsWebinar,
          ...lmsWebinar,
        });
        // Remove old image (if replaced)
        if (!noDownload && oldImage) {
          await this.deleteImage(oldImage);
        }
        updated += 1;
      } else {
        // Create new webinar
        // Upload image (if presented)
        if (!noDownload && cmsImageUrl) {
          lmsWebinar.showcaseImage = await this.uploadImage(cmsImageUrl);
        }
        // Save webinar
        await this.webinarsRepository.save(lmsWebinar);
        created += 1;
      }
    }

    console.log('✔️ done', {
      created,
      updated,
      total: created + updated,
    });
  }

  async uploadImage(url: string): Promise<File> {
    const fileInfo = await this.download.fileInfo(url);
    return this.filesService.uploadUrlAndCreate(
      {
        title: fileInfo.filename,
        filename: fileInfo.filename,
        extension: fileInfo.extension,
        mimetype: fileInfo.mimetype,
        size: fileInfo.size,
        url: url,
      },
      'webinars',
    );
  }

  async deleteImage(image: File) {
    await this.filesService.deleteOneStorage(image);
  }
}
