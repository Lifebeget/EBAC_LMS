import { Webinar } from '@entities/webinars.entity';
import { forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { PaginationDto } from 'src/pagination.dto';
import { SettingsService } from 'src/settings/settings.service';
import { Connection, FindManyOptions, FindOneOptions, Repository, SelectQueryBuilder } from 'typeorm';
import { CreateWebinarDto } from './dto/create-webinar.dto';
import { QueryWebinarDto } from './dto/query-webinar.dto';
import { UpdateWebinarDto } from './dto/update-webinar.dto';
import { Assessment } from '@entities/assessment.entity';
import { Submission } from '@entities/submission.entity';
import { pick } from 'lodash';
import { SubmissionsService } from '../assessments/submissions.service';
import { SubmissionStatus } from '@lib/types/enums';
import { User } from '@entities/user.entity';
import { WebinarConditions } from '@entities/webinars.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { SegmentsService } from 'src/segments/segments.service';

export enum WebinarFormat {
  DEFAULT = 'default',
  BILLING = 'billing',
}

@Injectable()
export class WebinarService {
  constructor(
    @Inject(forwardRef(() => SubmissionsService))
    private submissionsService: SubmissionsService,

    @Inject(forwardRef(() => SettingsService))
    private settingsService: SettingsService,

    @Inject(forwardRef(() => AssessmentsService))
    private assessmentsService: AssessmentsService,

    @Inject(forwardRef(() => SegmentsService))
    private segmentService: SegmentsService,

    @InjectConnection()
    private readonly connection: Connection,

    @InjectRepository(Webinar)
    private readonly webinarRepository: Repository<Webinar>,

    @InjectRepository(Submission)
    private submissionRepository: Repository<Submission>,

    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,

    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  create(dto: CreateWebinarDto) {
    return this.webinarRepository.save(dto);
  }

  update(dto: UpdateWebinarDto) {
    return this.webinarRepository.save(dto);
  }

  async findAll(paging: PaginationDto, query: QueryWebinarDto) {
    let queryBuilder = this.webinarRepository
      .createQueryBuilder('webinar')
      .leftJoinAndSelect('webinar.showcaseImage', 'showcaseImage')
      .leftJoinAndSelect('webinar.topics', 'topics')
      .leftJoinAndSelect('webinar.segment', 'segment');

    if (query.active) {
      queryBuilder = queryBuilder.andWhere('webinar.active = :active', {
        active: query.active,
      });
    }
    if (query.finished) {
      queryBuilder = queryBuilder.andWhere('webinar.active = :finished', {
        finished: query.finished,
      });
    }
    if (query.type) {
      queryBuilder = queryBuilder.andWhere('webinar.active = :type', {
        type: query.type,
      });
    }
    if (query.showClosest) {
      queryBuilder = queryBuilder.andWhere("DATE(webinar.startDateUtc) >= CURRENT_DATE - interval '5 days'");
    }
    if (!query.showUnpublished) {
      queryBuilder = queryBuilder.andWhere('published = true');
    }
    if (!query.showNonPublic && !query.userAvailable) {
      queryBuilder = queryBuilder.andWhere('public = true');
    }

    if (query.search) {
      queryBuilder = queryBuilder.andWhere('LOWER(webinar.name) like :search', {
        search: `%${query.search.toLowerCase()}%`,
      });
    }

    if (!query.showInactive) {
      queryBuilder = queryBuilder.andWhere('webinar.active = true');
    }

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    if (query.userAvailable) {
      queryBuilder.addOrderBy('webinar.public', 'ASC').addOrderBy('webinar.startDateUtc', 'DESC');
    } else {
      queryBuilder.addOrderBy('webinar.startDateUtc', 'DESC');
    }

    const [result, total] = await queryBuilder.getManyAndCount();
    let results = result;

    if (query.userAvailable) {
      const nonPublic = results.filter((web: Webinar) => web.public === false);
      if (nonPublic.length > 0) {
        for (const webinar of nonPublic) {
          const available = !!webinar?.segment?.condition
            ? await this.segmentService.validateConditionForUser(webinar.segment.condition, query.user)
            : false;
          if (!available) {
            results = results.filter((web: Webinar) => web.id !== webinar.id);
          }
        }
      }
    }

    return {
      results,
      meta: { ...paging, total },
    };
  }

  async find(values) {
    return this.webinarRepository.find({
      ...values,
      relations: ['showcaseImage'],
    });
  }

  @Transactional()
  async findOne(id: string) {
    return this.webinarRepository.findOne(id, { relations: ['topics', 'segment'] });
  }

  @Transactional()
  async findOneBySlug(slug: string) {
    return this.webinarRepository.findOne({ where: { slug }, relations: ['segment'] });
  }

  async save(values) {
    return this.webinarRepository.save(values);
  }

  async delete(values) {
    return this.webinarRepository.softDelete(values);
  }

  @Transactional()
  async findOneSurvey(id: string) {
    const result = await this.webinarRepository.findOne({
      relations: ['survey', 'survey.questions'],
      where: {
        id,
      },
    });

    if (result?.survey) {
      return {
        survey: result.survey,
        title: result.name,
        surveyOpenDate: result.surveyOpenDate,
        surveyCloseDate: result.surveyCloseDate,
        soon: dayjs(result.surveyOpenDate).isAfter(dayjs(new Date())),
        closed: dayjs(result.surveyCloseDate).isBefore(dayjs(new Date())),
      };
    }

    const settings = await this.settingsService.getSettings();
    const useDefaultSurvey = settings?.mainSettings?.webinars?.useDefaultSurvey || false;
    if (useDefaultSurvey && settings?.mainSettings?.webinars?.defaultSurvey) {
      const result2 = await this.assessmentsService.findOne(settings?.mainSettings?.webinars?.defaultSurvey);

      return { survey: result2, title: result.name };
    }
  }

  async surveyWebinarResults(id: string) {
    const webinar = await this.findOne(id);

    if (!webinar) throw new NotFoundException('ERRORS.WEBINAR_NOT_FOUND');

    const { results: submissions } = await this.submissionsService.findAll(null, {
      webinarId: id,
      status: SubmissionStatus.Graded,
    });

    const assessments = {};
    for (const submission of submissions) {
      if (!assessments[submission.assessment.id]) {
        assessments[submission.assessment.id] = {
          assessment: {},
          questions: {},
          results: [],
        };
      }
      if (Object.keys(assessments[submission.assessment.id].assessment).length === 0) {
        assessments[submission.assessment.id].assessment = pick(submission.assessment, [
          'id',
          'title',
          'description',
          'created',
          'modified',
        ]);
      }
      if (Object.keys(assessments[submission.assessment.id].questions).length === 0) {
        const { questions } = submission.assessment;
        const res_questions = {};

        for (const q of questions) {
          res_questions[q.id] = {
            ...pick(q, ['id', 'title', 'description', 'type', 'isRequired', 'created', 'modified']),
            answers: q.data?.answers,
          };
        }
        assessments[submission.assessment.id].questions = res_questions;
      }

      const answers = [];

      for (const answer of submission.answers) {
        const answer_data = {
          questionId: answer.questionId,
          files: answer.files.map(f => {
            return pick(f, ['id', 'filename', 'url']);
          }),
          data: [],
        };

        if (Array.isArray(answer.data)) {
          answer_data.data = answer.data.map(d => {
            return {
              id: d.id,
              value: d.isUserInput ? d.userInput : d.value,
            };
          });
        } else if (typeof answer.data === 'object') {
          answer_data.data.push({
            id: answer.data.id,
            value: answer.data.isUserInput ? answer.data.userInput : answer.data.value,
          });
        }

        answers.push(answer_data);
      }

      assessments[submission.assessment.id].results.push({
        submission: pick(submission, ['id', 'status', 'created', 'submittedAt', 'gradedAt', 'score']),
        answers,
        user: pick(submission.user, ['id', 'name', 'email']),
      });
    }

    return {
      webinar: pick(webinar, ['id', 'name', 'description', 'pageUrl', 'created', 'modified']),
      assessments: Object.values(assessments),
    };
  }

  @Transactional()
  async getTopics(webinarId: string) {
    this.webinarRepository.findOne(webinarId);
  }

  @Transactional()
  async findWebinar(options: FindOneOptions<Webinar>) {
    return this.webinarRepository.findOne(options);
  }

  async findWebinars(options: FindManyOptions<Webinar>): Promise<{ data: Webinar[]; count: number }> {
    const [data, count] = await this.webinarRepository.findAndCount(options);
    return { data, count };
  }

  findAllConditions() {
    return this.webinarRepository
      .createQueryBuilder('webinar')
      .select(['webinar.id', 'webinar.name', 'webinar.conditions'])
      .where('webinar.conditions is not null')
      .getMany();
  }

  async validateWebinarConditions(conditions: WebinarConditions) {
    return this.segmentService.validateCondition(conditions);
  }

  async validateWebinarConditionForUser(conditions: WebinarConditions, user: User) {
    return this.segmentService.validateConditionForUser(conditions, user);
  }

  public static formatWebinar(format: WebinarFormat, webinar: Webinar) {
    const pick = (keys: (keyof Webinar)[]) => keys.reduce((acc, k) => ({ ...acc, [k]: webinar[k] }), {});

    switch (format) {
      case WebinarFormat.DEFAULT:
        return { ...webinar };
      case WebinarFormat.BILLING:
        return pick(['id', 'slug', 'name', 'type', 'modified', 'startDateUtc']);
      default:
        throw new Error(`Unsupported webinar format: ${format}`);
    }
  }
}
