import { Source } from '@entities/source.entity';
import { Subscription } from '@entities/subscription.entity';
import { Webinar, WebinarConditions } from '@entities/webinars.entity';
import { Permission } from '@enums/permission.enum';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiProperty, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import * as assert from 'assert';
import axios, { AxiosResponse } from 'axios';
import { IsDateString, IsEnum, IsOptional } from 'class-validator';
import { Request } from 'express';
import { Interapp } from 'src/auth/interapp.decorator';
import { Pagination } from 'src/pagination.decorator';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Any, FindManyOptions, MoreThan, Repository } from 'typeorm';
import { QueryWebinarDto } from './dto/query-webinar.dto';
import { UpdateWebinarDto } from './dto/update-webinar.dto';
import { WebinarFormat, WebinarService } from './webinar.service';

interface WebinarT extends Webinar {
  isSubscribed?: boolean;
}

export interface GddWebinarSubscription {
  subscription_id: number;
  schedule_event_id: number;
  subscription_date_utc: string;
}

export class FindWebinarQuery {
  @ApiProperty({ required: false })
  slug?: string;
}

export class FindWebinarsQuery extends PaginationDto {
  @ApiProperty({
    description: 'Filter by webinar external IDs',
    type: [Number],
    required: false,
  })
  externalIds?: number[];

  @ApiProperty({ enum: WebinarFormat })
  @IsOptional()
  @IsEnum(WebinarFormat)
  format: WebinarFormat;

  @ApiProperty({ required: false })
  @IsDateString()
  @IsOptional()
  modifiedAfter?: string;

  @ApiProperty({ required: false })
  @IsDateString()
  @IsOptional()
  startDateAfter?: string;

  @ApiProperty({
    description: 'Include topics',
    required: false,
  })
  includeTopics?: boolean;
}

@Controller()
export class WebinarController {
  constructor(
    private webinarService: WebinarService,
    @InjectRepository(Subscription)
    private readonly subscriptionsRepository: Repository<Subscription>,
    @InjectRepository(Source)
    private readonly sourcesRepository: Repository<Source>,
  ) {
    assert(process.env.WEBINAR_ACCESS_KEY, 'WEBINAR_ACCESS_KEY env is not defined');
    assert(process.env.WEBINAR_CMS_EVENTS_URL, 'WEBINAR_CMS_EVENTS_URL env is not defined');
    assert(process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL, 'WEBINAR_GDD_SUBSCRIPTIONS_URL env is not defined');
  }

  // https://ebaconline.atlassian.net/browse/BPMTM-195
  @Get('webinar/find-many')
  @ApiOperation({
    summary: 'Finds webinars, allowing for pagination and formatting output for vaious external systems',
  })
  @Interapp()
  async findWebinars(@Query() query: FindWebinarsQuery) {
    const { modifiedAfter, startDateAfter, includeTopics, page = 1, itemsPerPage = 100, format = WebinarFormat.DEFAULT } = query;
    let externalIds = query.externalIds;
    const relations: Array<string> = [];
    const where: FindManyOptions<Webinar>['where'] = {};
    if (modifiedAfter) where.modified = MoreThan(new Date(modifiedAfter));
    if (startDateAfter) where.startDateUtc = MoreThan(new Date(startDateAfter));
    if (externalIds) {
      if (!Array.isArray(externalIds)) externalIds = [externalIds];
      where.externalId = Any(externalIds);
    }
    if (includeTopics) {
      relations.push('topics');
    }
    const { data, count } = await this.webinarService.findWebinars({
      where,
      order: { id: 'ASC' },
      take: itemsPerPage,
      skip: (page - 1) * itemsPerPage,
      relations,
    });
    return {
      count,
      data: data.map(WebinarService.formatWebinar.bind(null, format)),
    };
  }

  @Get('webinar/subscriptions')
  @ApiOperation({ summary: 'Get current user webinar subscriptions' })
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'Webinar user subscriptions',
  })
  async getSubscriptions(@Pagination() paging: PaginationDto, @Req() req: Request): Promise<AxiosResponse | any[]> {
    assert(req.user?.email, 'User needs to be defined');

    // Find local source
    const webinarSource = await this.sourcesRepository.findOne({
      where: { externalId: 1 },
    });
    assert.strict.ok(webinarSource);

    // Find local subscriptions by user + source
    const lmsSubscriptions = await this.subscriptionsRepository.find({
      where: { userId: req.user.id, sourceId: webinarSource.id },
      relations: ['webinar'],
      select: ['id', 'externalId', 'created'],
    });

    // Get GDD subscriptions
    return await axios
      .post<{
        status: 'ok';
        context: string;
        subscriptions: GddWebinarSubscription[];
      }>(process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL, {
        token: process.env.WEBINAR_ACCESS_KEY,
        email: req.user.email,
        timeout: 10000,
      })
      .then(res => {
        const gddSubscriptions = res.data.subscriptions;
        const gddSubscriptionIds = gddSubscriptions.map(s => s.subscription_id);

        // Merge GDD and local subscriptions, removing duplicates
        const addedGddSubscriptions: GddWebinarSubscription[] = lmsSubscriptions
          .filter(s => !gddSubscriptionIds.includes(s.externalId))
          .map(s => ({
            subscription_id: s.externalId,
            schedule_event_id: s.webinar.externalId,
            subscription_date_utc: s.created.toISOString(),
          }));
        return [...gddSubscriptions, ...addedGddSubscriptions];
      })
      .catch(err => {
        if (['invalid token', 'jwt malformed'].includes(err.response.data.error)) {
          console.warn(
            `${process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL} errored on request. Seems like auth token error. Check env WEBINAR_ACCESS_KEY value`,
          );
        } else {
          console.log(err);
          console.error(`${process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL} errored on request.`);
        }

        console.log('Assuming empty response\n\n');

        return [];
      });
  }

  @Get('webinar/find-one')
  @ApiOperation({
    summary: 'Find one webinar by secondary attribute, like slug',
    description: 'Currently used by BPM',
  })
  @Interapp()
  @RequirePermissions(Permission.WEBINAR_VIEW)
  async findWebinar(@Query() query: FindWebinarQuery) {
    if (query.slug) {
      const webinar = await this.webinarService.findOneBySlug(query.slug);
      assert.strict.ok(webinar, new NotFoundException());
      return webinar;
    }
    throw new BadRequestException();
  }

  @Get('webinar')
  @ApiOperation({ summary: 'List webinars' })
  @ApiBearerAuth()
  @ApiQuery({
    name: 'active',
    description: 'active webinars',
  })
  @ApiQuery({
    name: 'finished',
    description: 'finished webinars',
  })
  @ApiQuery({
    name: 'type',
    description: 'type webinar',
    required: false,
  })
  @PgConstraints()
  async findAll(@Req() req: Request, @Query() query: QueryWebinarDto, @Pagination() paging: PaginationDto): Promise<Paginated<WebinarT[]>> {
    if (query.userAvailable) {
      query.user = req.user;
    }
    return this.webinarService.findAll(paging, query);
  }

  @Get('webinar/conditions')
  @ApiOperation({ summary: 'Get webinar conditions' })
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Get webinar conditions' })
  async findAllConditions() {
    return this.webinarService.findAllConditions();
  }

  @Get('webinar/:id')
  @ApiOperation({ summary: 'Find one webinars' })
  @ApiBearerAuth()
  @PgConstraints()
  async findOne(@Param('id') id: string) {
    return this.webinarService.findOne(id);
  }

  @Get('webinar/:id/survey')
  @ApiOperation({ summary: 'Find one webinars survey' })
  @ApiBearerAuth()
  @PgConstraints()
  async findOneSurvey(@Param('id') id: string) {
    return this.webinarService.findOneSurvey(id);
  }

  @Get('webinar/:id/survey-results')
  @RequirePermissions(Permission.WEBINAR_SURVEY_VIEW)
  @Interapp()
  @ApiOperation({ summary: 'Get results of webinar survey' })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'Webinar id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'results',
  })
  @PgConstraints()
  surveyWebinarResults(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.webinarService.surveyWebinarResults(id);
  }

  @Patch('webinar/:id')
  @ApiOperation({ summary: 'Update webinar' })
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'update webinar', type: Webinar })
  async update(@Body() updateWebinarDto: UpdateWebinarDto) {
    return this.webinarService.update(updateWebinarDto);
  }

  @Post('webinar/validate')
  @ApiOperation({ summary: 'validate webinar conditions' })
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'validate webinar conditions' })
  async validateWebinarConditions(@Body() conditions: WebinarConditions) {
    return this.webinarService.validateWebinarConditions(conditions);
  }
}
