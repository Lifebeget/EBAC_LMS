import { Segment } from '@entities/segment.entity';
import { Source } from '@entities/source.entity';
import { Subscription } from '@entities/subscription.entity';
import { User } from '@entities/user.entity';
import { Webinar } from '@entities/webinars.entity';
import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { SegmentsModule } from 'src/segments/segments.module';
import { SettingsModule } from 'src/settings/settings.module';
import { WebinarController } from './webinar.controller';
import { WebinarService } from './webinar.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Webinar, Subscription, Source, User, Segment]),
    forwardRef(() => SettingsModule),
    forwardRef(() => AssessmentsModule),
    forwardRef(() => SegmentsModule),
  ],
  controllers: [WebinarController],
  providers: [WebinarService],
  exports: [WebinarService],
})
export class WebinarModule {}
