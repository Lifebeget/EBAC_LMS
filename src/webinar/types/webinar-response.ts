export interface WebinarResponseI {
  data: Data;
}

interface Data {
  meta: Meta;
  data: Datum[];
}

export interface Datum {
  isSubscribed?: boolean;
  id: number;
  slug: string;
  name: string;
  type: string;
  additional: null | string;
  subtitle: null | string;
  startDateUtc: Date;
  startDateText: string;
  active: boolean;
  finished: boolean;
  pageUrl: string;
  showcaseImage: string;
  speakerBiography: null | string;
  description: null | string;
  directionID: number | null;
  youtubePlaylistID: null;
  images: Images;
  nearestUpcomingTopic?: NearestTopic;
  nearestFinishedTopic?: NearestTopic;
  topics: number[];
  esListID: number;
  cmRoomIDS: string[];
  demioEventIDS: string[];
  recordLink: string;
  created: Date;
  modified: Date;
  deleted: null;
  rev: number;
  public: boolean;
  options: Meta;
  published: boolean;
  finalStartDateUtc: Date;
}

interface Images {
  showcaseImage: ShowcaseImage;
}

interface ShowcaseImage {
  uid: string;
  filename: string;
  extension: string;
  mimetype: string;
  description: string;
  path: string;
  url: string;
  size: number;
  options: null;
  created: Date;
  modified: null;
  rev: number;
}

interface NearestTopic {
  eventID: number;
  eventTopicID: number;
  eventTopicNum: number;
  eventTopicDay: number;
  eventDateUTC: Date;
  topicDateUTCDiff: number;
  topicStatus: string;
  topicDateUTC: Date;
  topicDeleted: null;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Meta {}
