import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';
import { CreateWebinarDto } from './create-webinar.dto';
import { WebinarConditions } from '../../lib/entities/webinars.entity';
import { Segment } from '@entities/segment.entity';

export class UpdateWebinarDto extends PartialType(CreateWebinarDto) {
  @ApiProperty({ description: 'UUID' })
  @IsUUID()
  id: string;

  @ApiProperty({ description: 'Webinar conditions' })
  conditions?: WebinarConditions;

  @ApiProperty({ description: 'User segment' })
  segment?: Segment;
}
