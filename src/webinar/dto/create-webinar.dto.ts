import { File } from '@entities/file.entity';
import { Topic } from '@entities/topic.entity';
import { IsBoolean, IsDate, IsDateString, IsNumber, IsString, ValidateIf } from 'class-validator';
export class CreateWebinarDto {
  @ValidateIf(o => o.hasOwnProperty('externalId'))
  @IsNumber()
  externalId: number;

  @ValidateIf(o => o.hasOwnProperty('slug'))
  @IsString()
  slug?: string;

  @ValidateIf(o => o.hasOwnProperty('name'))
  @IsString()
  name?: string;

  @ValidateIf(o => o.hasOwnProperty('type'))
  @IsString()
  type?: string;

  @ValidateIf(o => o.hasOwnProperty('additional'))
  @IsString()
  additional?: string;

  @ValidateIf(o => o.hasOwnProperty('subtitle'))
  @IsString()
  subtitle?: string;

  @ValidateIf(o => o.hasOwnProperty('startDateUTC'))
  @IsDateString()
  startDateUtc?: Date;

  @ValidateIf(o => o.hasOwnProperty('startDateText'))
  @IsString()
  startDateText?: string;

  @ValidateIf(o => o.hasOwnProperty('active'))
  @IsBoolean()
  active?: boolean;

  @ValidateIf(o => o.hasOwnProperty('additional'))
  @IsBoolean()
  finished?: boolean;

  @ValidateIf(o => o.hasOwnProperty('pageURL'))
  @IsString()
  pageUrl?: string;

  @ValidateIf(o => o.hasOwnProperty('showcaseImage'))
  showcaseImage?: File;

  @ValidateIf(o => o.hasOwnProperty('speakerBiography'))
  @IsString()
  speakerBiography?: string;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsString()
  description?: string;

  @ValidateIf(o => o.hasOwnProperty('directionID'))
  @IsNumber()
  directionId?: number;

  @ValidateIf(o => o.hasOwnProperty('youtubePlaylistID'))
  @IsNumber()
  youtubePlaylistId?: number;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsString()
  recordLink: string;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsDate()
  created: Date;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsDate()
  modified: Date;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsDate()
  deleted: null;

  @ValidateIf(o => o.hasOwnProperty('description'))
  @IsNumber()
  rev: number;

  @ValidateIf(o => o.hasOwnProperty('topics'))
  topics: DeepPartial<Topic>[];

  @ValidateIf(o => o.hasOwnProperty('published'))
  @IsBoolean()
  published?: boolean;

  @ValidateIf(o => o.hasOwnProperty('finalStartDateUtc'))
  @IsDateString()
  finalStartDateUtc?: Date;
}
