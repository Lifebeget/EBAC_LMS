import { User } from '@entities/user.entity';
import { IsBooleanString, IsString, ValidateIf } from 'class-validator';

export class QueryWebinarDto {
  @ValidateIf(o => o.hasOwnProperty('active'))
  @IsBooleanString()
  active?: boolean;

  @ValidateIf(o => o.hasOwnProperty('finished'))
  @IsBooleanString()
  finished?: boolean;

  @ValidateIf(o => o.hasOwnProperty('type'))
  @IsString()
  type?: string;

  @ValidateIf(o => o.hasOwnProperty('showClosest'))
  @IsString()
  showClosest?: boolean;

  @ValidateIf(o => o.hasOwnProperty('showUnpublished'))
  @IsString()
  showUnpublished?: boolean;

  @ValidateIf(o => o.hasOwnProperty('showNonPublic'))
  @IsString()
  showNonPublic?: boolean;

  @ValidateIf(o => o.hasOwnProperty('userAvailable'))
  @IsString()
  userAvailable?: boolean;

  showInactive?: boolean;

  search?: string;

  user?: User;
}
