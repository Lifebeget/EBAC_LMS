/** Provides error handling decorators for Postgres to use in controllers */
import { BadRequestException, HttpException, HttpStatus } from '@nestjs/common';

const PG_UNIQUE_KEY_CONSTRAINT_ERROR = '23505';
const PG_KEY_NOT_FOUND_CONSTRAINT_ERROR = '23503';
const PG_NOT_NULL_CONSTRAINT_ERROR = '23502';
const PG_UUID_SYNTAX_ERROR = '22P02';

export const PgConstraints = function () {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = new Proxy(originalMethod, {
      apply: async function (target, thisArg, args) {
        try {
          return await target.apply(thisArg, args);
        } catch (err) {
          if (err.code == PG_UNIQUE_KEY_CONSTRAINT_ERROR) {
            throw new HttpException(`Duplicate entry: ${err.detail}`, HttpStatus.CONFLICT);
          } else if (err.code == PG_KEY_NOT_FOUND_CONSTRAINT_ERROR) {
            throw new HttpException(`Foreign key violation: ${err.detail}`, HttpStatus.NOT_FOUND);
          } else if (err.code == PG_NOT_NULL_CONSTRAINT_ERROR) {
            throw new HttpException(`Property '${err.column}' cannot be NULL`, HttpStatus.BAD_REQUEST);
          } else if (err.code == PG_UUID_SYNTAX_ERROR) {
            throw new BadRequestException('Invalid UUID syntax');
          } else {
            console.dir({ code: err.code, detail: err.detail, err });
            throw err;
          }
        }
      },
    });
  };
};
