/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, PayloadTooLargeException, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import * as fs from 'fs';
import { InjectS3, S3 } from 'nestjs-s3';
import { PassThrough } from 'stream';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { File } from '../lib/entities/file.entity';
import { StorageFactory } from './types/storage';
import promisePool from 'src/_util/async-bottleneck';
import { getFileInfo } from 'src/helpers';
import { Endpoint } from 'aws-sdk';
const aws4 = require('aws4');

@Injectable()
export class FilesStorageS3Service implements StorageFactory {
  constructor(
    @InjectRepository(File)
    private filesRepository: Repository<File>,
    @InjectS3() private readonly s3: S3,
  ) {}

  processExtension(ext: string): string {
    const extLower = ext?.toLowerCase();
    return extLower == 'jpg' ? 'jpeg' : extLower;
  }

  async uploadDisk(file: any, folder = 'answers'): Promise<File> {
    const readStream = fs.createReadStream(file.path);
    const passThrough = new PassThrough();

    const Key = `${folder}/${uuidv4()}/original.${this.processExtension(file.extension)}`;

    const s3Res = this.s3
      .upload({
        Bucket: process.env.S3_BUCKET_NAME,
        Body: passThrough,
        Key,
        ContentType: file.mimetype,
        ContentDisposition: `filename="${encodeURI(file.title)}"`,
      })
      .promise();
    readStream.pipe(passThrough);
    await s3Res.catch(error => {
      throw new UnprocessableEntityException(error);
    });

    return {
      ...file,
      url: `${process.env.S3_WEBSITE_URL}/${Key}`,
      s3Key: Key,
    };
  }

  async uploadUrl(file: any, folder = 'answers'): Promise<File> {
    if (!file.mimetype) {
      const finfo = await getFileInfo(file.url);
      if (finfo) {
        file.mimetype = finfo.mimetype;
        file.size = finfo.size;
      }
    }

    const response = await axios({
      url: file.url,
      method: 'GET',
      responseType: 'stream',
    });
    const passThrough = new PassThrough();
    const Key = `${folder}/${uuidv4()}/original.${this.processExtension(file.extension)}`;
    const s3Res = this.s3
      .upload({
        Bucket: process.env.S3_BUCKET_NAME,
        Body: passThrough,
        Key,
        ContentType: file.mimetype,
        ContentDisposition: `filename="${encodeURI(file.title)}"`,
      })
      .promise();
    response.data.pipe(passThrough);
    const urlS3 = await s3Res.then(result => {
      return result.Location;
    });

    return {
      ...file,
      url: `${process.env.S3_WEBSITE_URL}/${Key}`,
      eadboxUrl: file.url,
      s3Key: Key,
    };
  }

  async uploadUrlSimple(urlFrom: any, fileTitle: string, filenameS3: string, bucketName: string, addExtension?: boolean): Promise<string> {
    let mimetype;
    const finfo = await getFileInfo(urlFrom);
    if (finfo) {
      mimetype = finfo.mimetype;
    }
    let _filenameS3 = filenameS3;
    let _fileTitle = fileTitle;
    if (addExtension && finfo.extension) {
      _filenameS3 += `.${finfo.extension}`;
      _fileTitle += `.${finfo.extension}`;
    }

    const response = await axios({
      url: urlFrom,
      method: 'GET',
      responseType: 'stream',
    });
    const passThrough = new PassThrough();
    const s3Res = this.s3
      .upload({
        Bucket: bucketName,
        Body: passThrough,
        Key: _filenameS3,
        ContentType: mimetype,
        ContentDisposition: `filename="${encodeURI(_fileTitle)}"`,
      })
      .promise();
    response.data.pipe(passThrough);
    const urlS3 = await s3Res.then(result => {
      return result.Location;
    });

    return urlS3;
  }

  async isExist(filenameS3: string, bucketName: string) {
    return this.s3
      .headObject({
        Bucket: bucketName,
        Key: filenameS3,
      })
      .promise()
      .catch(err => {
        if (err.name != 'NotFound') {
          console.error(err);
        }
        return null;
      });
  }

  async upsertUrlSimple(urlFrom: any, fileTitle: string, filenameS3: string, bucketName: string, addExtension?: boolean): Promise<any[]> {
    try {
      const head = await this.isExist(filenameS3, bucketName);
      if (!head) {
        const uploaded = await this.uploadUrlSimple(urlFrom, fileTitle, filenameS3, bucketName, addExtension);
        return [null, uploaded];
      }

      return [null, head, true];
    } catch (error) {
      return [error];
    }
  }

  async uploadDiskSimple(filenameLocal: string, filenameS3: string, fileTitle: string, bucketName: string, mimetype: string): Promise<any> {
    const readStream = fs.createReadStream(filenameLocal);
    const passThrough = new PassThrough();
    const s3Res = this.s3
      .upload({
        Bucket: bucketName,
        Body: passThrough,
        Key: filenameS3,
        ContentType: mimetype,
        ContentDisposition: `filename="${encodeURI(fileTitle)}"`,
      })
      .promise();
    readStream.pipe(passThrough);
    return s3Res;
  }

  async emptyS3Directory(dir) {
    const listParams = {
      Bucket: process.env.S3_BUCKET_NAME,
      Prefix: dir,
    };

    const listedObjects = await this.s3.listObjectsV2(listParams).promise();

    if (listedObjects.Contents.length === 0) return;

    const deleteParams = {
      Bucket: process.env.S3_BUCKET_NAME,
      Delete: { Objects: [] },
    };

    listedObjects.Contents.forEach(({ Key }) => {
      deleteParams.Delete.Objects.push({ Key });
    });

    await this.s3.deleteObjects(deleteParams).promise();

    if (listedObjects.IsTruncated) await this.emptyS3Directory(dir);
  }

  // check delete 1 file or all directory
  getDeletedLinkFolderBasedStorage(s3Key: string): [string?, string?] {
    const folderBasedDiresctories = ['webinars', 'avatars'];
    const match = s3Key.match(/(?:(?<directory>[\w\d\-\/]+)\/)*(?:(?<uuid>[\w\d\-]+?)\/)+original\.(?<ext>[\d\w]+)$/);
    if (!match || !folderBasedDiresctories.includes(match.groups?.directory)) return [s3Key];
    return [null, `${match.groups?.directory}/${match.groups?.uuid}/`];
  }

  async delete(file: any) {
    if (!file.s3Key) return;
    const [Key, dir] = this.getDeletedLinkFolderBasedStorage(file.s3Key);
    if (dir) {
      return this.emptyS3Directory(dir);
    }
    await this.s3
      .deleteObject({
        Bucket: process.env.S3_BUCKET_NAME,
        Key: Key,
      })
      .promise();
  }

  async deleteMany(files: File[], folder?) {
    const s3files = files.filter(e => e.s3Key);
    if (!s3files.length) return;

    await promisePool(s3files, this.delete.bind(this), 10);
  }

  async directories(folder = '', relativePath = false, bucket = process.env.S3_BUCKET_NAME) {
    if (folder && folder.slice(-1) != '/') folder = folder.trim() + '/';
    let directories = [];
    let result;
    do {
      result = await this.s3
        .listObjectsV2({
          Bucket: bucket,
          Delimiter: '/',
          Prefix: folder,
        })
        .promise();
      directories = directories.concat(result.CommonPrefixes);
    } while (result.IsTruncated);

    directories = directories.map(e => e.Prefix);
    if (relativePath) {
      const re = new RegExp(`^${folder}`, '');
      directories = directories.map(e => e.replace(re, ''));
    }
    return directories;
  }

  async files(folder = '', bucket = process.env.S3_BUCKET_NAME) {
    if (folder && folder.slice(-1) != '/') folder = folder.trim() + '/';
    let filesList = [];
    let result;
    do {
      result = await this.s3
        .listObjectsV2({
          Bucket: bucket,
          Prefix: folder,
        })
        .promise();
      filesList = filesList.concat(result.Contents);
    } while (result.IsTruncated);

    return filesList;
  }

  async uploadBase64(dataBase64: string, mimeType: string, extension = null, folder = 'embeds') {
    extension = extension || mimeType?.split('/')[1];
    const id = uuidv4();
    const Key = `${folder}/${uuidv4()}/original.${extension}`;

    const result = await this.s3
      .upload({
        Bucket: process.env.S3_BUCKET_NAME,
        Body: Buffer.from(dataBase64, 'base64'),
        Key,
        ContentType: mimeType,
      })
      .promise();

    return {
      title: `${id}.${extension}`,
      url: process.env.S3_WEBSITE_URL + '/' + result?.Key,
      filename: `${id}.${extension}`,
      mimetype: mimeType,
      extension: extension,
      s3Key: result?.Key,
      size: Buffer.from(dataBase64, 'base64').length,
    };
  }

  async replaceAndUploadBase64ImgS3(text: string, folder = 'embeds'): Promise<string> {
    if (!text) return text;

    const re = /<img src="data:(?<contentType>.*?);base64,(?<base64>.+?)">/gim;
    const imgs = [];
    let match = re.exec(text);
    while (match) {
      imgs.push(match);
      match = re.exec(text);
    }

    const imgsS3 = await Promise.all(imgs.map(f => this.uploadBase64(f.groups.base64, f.groups.contentType, null, folder)));

    for (const [i, img] of imgs.entries()) {
      text = text.replace(img[0], `<img src="${imgsS3[i].url}">`);
    }
    return text;
  }

  async createUploadLink(folder, filePath) {
    this.s3.endpoint = new Endpoint(process.env.S3_ENDPOINT_V2);

    const signedUploadLink = await this.s3.getSignedUrl('putObject', {
      Expires: 60 * 60,
      Key: filePath,
      //We use folder as a bucker for legacy link fix
      Bucket: folder,
      ContentType: 'application/octet-stream',
    });

    return signedUploadLink;
  }

  async createPromisedFile(filedata, folder) {
    const pregeneratedId = uuidv4();
    const key = `${uuidv4()}/original.${this.processExtension(filedata.extension)}`;

    const newFile = await this.filesRepository.create({
      id: pregeneratedId,
      url: `${process.env.S3_WEBSITE_URL}/${folder}/${key}`,
      s3Key: key,
      mimetype: filedata.mimetype,
      title: filedata.title.replace(`.${filedata.extension}`, ''),
      description: filedata.description || '',
      size: filedata.size,
      extension: filedata.extension,
      filename: filedata.filename,
      owner: null,
    });

    const signedS3Link = await this.createUploadLink(folder, key);

    return {
      file: newFile,
      signedS3Link,
    };
  }
}
