export interface StorageFactory {
  uploadUrl(file: any, folder?: string);
  delete(file: any, folder?: string);
  deleteMany(files: any[], folder?: string);
  uploadBase64(dataBase64: string, mimeType: string, extension?: string, folder?: string);
}
