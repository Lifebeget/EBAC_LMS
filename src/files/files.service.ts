/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InjectS3, S3 } from 'nestjs-s3';
import { IsNull, Like, Not, Repository } from 'typeorm';
import { File } from '../lib/entities/file.entity';
import { CreateFileDto } from './dto/create-file.dto';
import { QueryFileDto } from './dto/query-file.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import { UploadFileDto } from './dto/upload-file.dto';
import { FilesStorageLocalService } from './files.storage.local.service';
import { FilesStorageS3Service } from './files.storage.s3.service';
import { v4 as uuid4 } from 'uuid';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class FilesService {
  public storage: FilesStorageLocalService | FilesStorageS3Service;
  constructor(
    @InjectRepository(File)
    private filesRepository: Repository<File>,
    public storageLocal: FilesStorageLocalService,
    public storageS3: FilesStorageS3Service,
    @InjectS3() public readonly s3: S3,
  ) {
    this.storage = process.env.FILES_STORAGE_TYPE === 's3' ? this.storageS3 : this.storageLocal;
  }

  async create(createFileDto: CreateFileDto) {
    return this.filesRepository.save({
      owner: createFileDto.ownerId ? { id: createFileDto.ownerId } : null,
      ...createFileDto,
    });
  }

  async upload(files: Express.Multer.File[], uploadFileDto: UploadFileDto, ownerId: string, folder = 'upload') {
    const result = [];
    for (let idx = 0, l = files.length; idx < l; idx++) {
      let dbfile = new CreateFileDto();
      const file = files[idx];

      if (uploadFileDto.title && uploadFileDto.title[idx]) {
        dbfile.title = uploadFileDto.title[idx];
      } else {
        dbfile.title = file.originalname;
      }

      dbfile.size = file.size;
      dbfile.extension = file.originalname.split('.').pop();

      if (dbfile.extension.length > 20) {
        dbfile.extension = '';
      }

      dbfile.filename = file.originalname;
      dbfile.ownerId = ownerId;
      dbfile.mimetype = file.mimetype;
      dbfile.path = file.path;

      dbfile = await this.storage.uploadDisk(dbfile, folder);

      result.push(await this.create(dbfile));
    }
    return result;
  }

  findAll(query: QueryFileDto): Promise<File[]> {
    let queryBuilder = this.filesRepository.createQueryBuilder('file');
    queryBuilder = queryBuilder.leftJoinAndSelect('file.owner', 'owner');
    if (query.ownerId) {
      queryBuilder = queryBuilder.andWhere('owner_id = :ownerId', {
        ownerId: query.ownerId,
      });
    }
    if (query.search) {
      queryBuilder = queryBuilder.andWhere('title LIKE :search', {
        search: query.search,
      });
    }
    return queryBuilder.getMany();
  }

  @Transactional()
  findOne(id: string): Promise<File> {
    return this.filesRepository.findOne(id);
  }

  findByPath(path: string): Promise<File> {
    return this.filesRepository.findOne({
      where: { url: Like(`%${path}`) },
      relations: ['owner'],
    });
  }

  update(id: string, updateFileDto: UpdateFileDto) {
    return this.filesRepository.save({
      ...updateFileDto,
      id,
    });
  }

  remove(id: string) {
    return this.filesRepository.softDelete(id);
  }

  removePermanentMany(ids: string[]) {
    return this.filesRepository.createQueryBuilder('file').delete().from(File).where('file.id IN (:...ids)', { ids }).execute();
  }

  purgeAll() {
    return this.filesRepository.clear();
  }

  findAllDeleted(): Promise<File[]> {
    return this.filesRepository.find({
      where: {
        deleted: Not(IsNull()),
      },
      withDeleted: true,
    });
  }

  saveMany(values) {
    return this.filesRepository.save(values);
  }

  async uploadUrlToS3andUpdate(file: any, folder = 'answers') {
    const s3Key = file.s3Key;
    const fileUploaded = await this.storageS3.uploadUrl(file, folder);

    const result = await this.filesRepository.save(fileUploaded);
    if (s3Key) {
      await this.storage.delete({ s3Key });
    }

    return result;
  }

  async uploadUrlAndCreate(file: any, folder = 'upload'): Promise<File> {
    const fileUploaded = await this.storage.uploadUrl(file, folder);
    return this.create(fileUploaded);
  }

  async deleteOneStorage(file: any, folder = 'answers') {
    await this.storage.delete(file);
    await this.removePermanentMany([file.id]);
  }

  async deleteManyS3(files: File[]) {
    await this.storageS3.deleteMany(files);
    return this.removePermanentMany(files.map(e => e.id));
  }

  async replaceAndUploadBase64ImgS3(text: string, folder = 'embeds'): Promise<string> {
    if (!text) return text;
    return this.storageS3.replaceAndUploadBase64ImgS3(text, folder);
  }

  async findOldResize(dir = 'webinars') {
    return this.filesRepository
      .createQueryBuilder('file')
      .where('file.s3Key IS NOT NULL')
      .andWhere(`file.s3Key ~ E'^${dir}/[[:xdigit:]]{8}-([[:xdigit:]]{4}-){3}[[:xdigit:]]{12}\\.'`)
      .getMany();
  }

  async createPromisedFile(filedata, folder) {
    return await this.storageS3.createPromisedFile(filedata, folder);
  }

  async savePromisedFile(file) {
    return await this.filesRepository.save(file);
  }
}
