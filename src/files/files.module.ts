import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FilesService } from './files.service';
import { FilesController } from './files.controller';
import { File } from '../lib/entities/file.entity';
import { KalturaService } from 'src/kaltura/kaltura.service';
import { FilesStorageLocalService } from './files.storage.local.service';
import { FilesStorageS3Service } from './files.storage.s3.service';

@Module({
  imports: [TypeOrmModule.forFeature([File])],
  controllers: [FilesController],
  providers: [FilesService, KalturaService, FilesStorageLocalService, FilesStorageS3Service],
  exports: [FilesService],
})
export class FilesModule {}
