export class CreatePromisedFileDTO {
  filedata: {
    mimetype: string;
    title: string;
    description?: string;
    size: string;
    extension: string;
    filename: string;
  };

  folder: string;
}
