import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateFileDto {
  @ApiProperty({ description: 'File name' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'File URL' })
  url: string;

  @ApiProperty({ description: 'File URL eadbox' })
  eadboxUrl?: string;

  @ApiProperty({ description: 'File path if saved on server' })
  path?: string;

  @ApiProperty({ description: 'File mimetype' })
  mimetype?: string;

  @ApiProperty({ description: 'File size' })
  size: number;

  @ApiProperty({ description: 'File extension' })
  extension: string;

  @ApiProperty({ description: 'Filename' })
  filename: string;

  @ApiProperty({ description: 'File owner id' })
  ownerId?: string;

  @ApiProperty({ description: 's3Key' })
  s3Key?: string;
}
