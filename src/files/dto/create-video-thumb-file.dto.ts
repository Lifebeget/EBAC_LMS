import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateVideoThumbDto {
  @IsNotEmpty()
  @ApiProperty({ description: 'ID on kaltura/youtube' })
  videoId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Timecode' })
  timecode: number;
}
