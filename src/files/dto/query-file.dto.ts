import { IsUUID, ValidateIf } from 'class-validator';

export class QueryFileDto {
  @ValidateIf(o => o.hasOwnProperty('ownerId'))
  @IsUUID()
  ownerId?: string;

  search?: string;
}
