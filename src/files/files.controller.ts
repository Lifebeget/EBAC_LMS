import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  Res,
  UnauthorizedException,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FilesService } from './files.service';
import { CreateFileDto } from './dto/create-file.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import { PgConstraints } from '../pg.decorators';
import { File } from '../lib/entities/file.entity';
import { QueryFileDto } from './dto/query-file.dto';
import { UploadFileDto } from './dto/upload-file.dto';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Public } from 'src/auth/public.decorator';
import * as fs from 'fs';
import { Request, Response } from 'express';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { Permissions } from '../permissions.decorator';
import * as contentDisposition from 'content-disposition';
import { CreateVideoThumbDto } from './dto/create-video-thumb-file.dto';
import { KalturaService } from 'src/kaltura/kaltura.service';
import { FileFolder } from 'src/lib/enums/file-folder.enum';
import { getFileInfo } from 'src/helpers';
import { CreatePromisedFileDTO } from './dto/create-promised-file.dto';

@ApiTags('files')
@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService, private readonly kalturaService: KalturaService) {}

  @Post()
  @ApiBearerAuth()
  @RequirePermissions(Permission.FILES_UPLOAD)
  @ApiOperation({ summary: 'Create a new file' })
  @ApiResponse({
    status: 201,
    description: 'Created file',
    type: File,
  })
  @PgConstraints()
  create(@Body() createFileDto: CreateFileDto) {
    return this.filesService.create(createFileDto);
  }

  @Post('create-from-url')
  @RequirePermissions(Permission.FILES_UPLOAD)
  @ApiOperation({ summary: 'Create a new file from URL' })
  @ApiResponse({
    status: 201,
    description: 'Created file',
    type: File,
  })
  @PgConstraints()
  async createFromUrl(@Body() body: { url: string }, @Req() req: Request) {
    const exists = await this.filesService.findByPath(body.url);
    if (exists) return exists;

    const file = await getFileInfo(body.url);
    return this.filesService.create({ ...file, ownerId: req.user?.id });
  }

  @Post('upload-kaltura-thumb')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Takes a thumbnail of video and create new file' })
  @ApiResponse({
    status: 201,
    description: 'Created file',
    type: File,
  })
  @PgConstraints()
  async uploadKalturaThumb(@Body() dto: CreateVideoThumbDto, @Req() req: Request) {
    const file = await this.kalturaService.getThumbFile(dto.videoId, dto.timecode);
    return this.filesService.create({ ...file, ownerId: req.user?.id });
  }
  @Post('upload/support')
  @Public()
  @ApiOperation({ summary: 'Uploads a file or multiple file for support form' })
  @ApiResponse({
    status: 201,
    description: 'Created files',
    type: [File],
  })
  @ApiQuery({
    name: 'files',
    description: 'Files to upload (multipart/form-data)',
    required: true,
  })
  @UseInterceptors(FilesInterceptor('files', 10, { dest: './upload' }))
  uploadSupport(@UploadedFiles() files: Express.Multer.File[], @Body() uploadFileDto: UploadFileDto, @Req() req: Request) {
    const userId = req.user?.id || null;
    // deleting duplicate files by id and keeping the original file name
    const _files = [...new Map(files.map(item => [item.originalname, item])).values()].map(file => {
      return { ...file, originalname: file.originalname.replace(/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}==/, '') };
    });
    return this.filesService.upload(_files, uploadFileDto, userId, 'support').finally(() => {
      files.forEach(f => fs.unlinkSync(f.path));
    });
  }

  @Post('upload/:folder')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Uploads a file or multiple file' })
  @ApiResponse({
    status: 201,
    description: 'Created files',
    type: [File],
  })
  @ApiQuery({
    name: 'files',
    description: 'Files to upload (multipart/form-data)',
    required: true,
  })
  @UseInterceptors(FilesInterceptor('files', 10, { dest: './upload' }))
  upload(
    @Param('folder') folder: FileFolder,
    @UploadedFiles() files: Express.Multer.File[],
    @Body() uploadFileDto: UploadFileDto,
    @Req() req: Request,
  ) {
    // deleting duplicate files by id and keeping the original file name
    const _files = [...new Map(files.map(item => [item.originalname, item])).values()].map(file => {
      return { ...file, originalname: file.originalname.replace(/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}==/, '') };
    });
    return this.filesService.upload(_files, uploadFileDto, req.user.id, folder).finally(() => {
      files.forEach(f => fs.unlinkSync(f.path));
    });
  }

  @Get('public/:filePath*')
  @ApiOperation({
    summary: 'Get file by public url. This method ignores any rights check',
  })
  @ApiParam({
    name: 'filePath',
    description: 'Valid file path from file.path. Also in file.url',
  })
  @Public()
  async getByPath(@Param() filePath: { '0': string; filePath: string }, @Res() res: Response) {
    const path = filePath.filePath + filePath['0'];
    const info = await this.filesService.findByPath(path);
    if (info && info.id) {
      const file = __dirname + '/../../../../' + info.path;
      const filestream = fs.createReadStream(file);

      res.setHeader('Content-disposition', contentDisposition(info.filename, { type: 'inline' }));
      res.setHeader('Content-type', info.mimetype);

      filestream.pipe(res);
    } else {
      res.status(404).send('Not found');
    }
  }

  @Get()
  @ApiBearerAuth()
  @RequirePermissions(Permission.FILES_VIEW, Permission.FILES_VIEW_OWN)
  @ApiOperation({ summary: 'List all files' })
  @ApiQuery({
    name: 'ownerId',
    description: 'Filter by owner',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    description: 'Search files by title',
    required: false,
  })
  @ApiResponse({
    status: 200,
    type: [File],
  })
  findAll(@Query() query: QueryFileDto, @Permissions() perms: Permission[], @Req() req: Request) {
    if (!perms.includes(Permission.FILES_VIEW)) {
      query.ownerId = req.user.id;
    }
    return this.filesService.findAll(query);
  }

  @Get(':id')
  @ApiBearerAuth()
  @RequirePermissions(Permission.FILES_VIEW, Permission.FILES_VIEW_OWN)
  @ApiOperation({ summary: 'Get one file by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a file by id',
    type: File,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[], @Req() req: Request) {
    const file = await this.filesService.findOne(id);
    if (!perms.includes(Permission.FILES_VIEW) && file.owner.id != req.user.id) {
      throw new UnauthorizedException("You don't have access to this file");
    }
    return file;
  }

  @Patch(':id')
  @ApiBearerAuth()
  @RequirePermissions(Permission.FILES_MANAGE, Permission.FILES_MANAGE_OWN)
  @ApiOperation({ summary: 'Updates a file' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateFileDto: UpdateFileDto,
    @Permissions() perms: Permission[],
    @Req() req: Request,
  ) {
    const file = await this.filesService.findOne(id);
    if (!perms.includes(Permission.FILES_MANAGE) && file.owner.id != req.user.id) {
      throw new UnauthorizedException("You don't have access to this file");
    }
    return this.filesService.update(id, updateFileDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @RequirePermissions(Permission.FILES_MANAGE, Permission.FILES_MANAGE_OWN)
  @ApiOperation({ summary: 'Deletes a file' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[], @Req() req: Request) {
    const file = await this.filesService.findOne(id);
    if (!perms.includes(Permission.FILES_MANAGE) && file.owner.id != req.user.id) {
      throw new UnauthorizedException("You don't have access to this file");
    }
    return this.filesService.remove(id);
  }

  @Post('/create-promised-file')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create file without save to db' })
  async createPromisedFile(@Body() createUploadLinkDTO: CreatePromisedFileDTO) {
    return this.filesService.createPromisedFile(createUploadLinkDTO.filedata, createUploadLinkDTO.folder);
  }

  @Post('/save-promised-file')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Save file to db' })
  async savePromisedFile(@Body() file: any) {
    return this.filesService.savePromisedFile(file);
  }
}
