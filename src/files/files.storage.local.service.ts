/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import * as fs from 'fs';
import * as path from 'path';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { File } from '../lib/entities/file.entity';
import { StorageFactory } from './types/storage';

@Injectable()
export class FilesStorageLocalService implements StorageFactory {
  constructor(
    @InjectRepository(File)
    private filesRepository: Repository<File>,
  ) {}

  processExtension(ext: string): string {
    const extLower = ext?.toLowerCase();
    return extLower == 'jpg' ? 'jpeg' : extLower;
  }

  uploadDisk(file: any, folder = 'answers'): File {
    return {
      ...file,
      url: `${process.env.FILES_SERVER_URL}/files/public/files/${folder}/${uuidv4()}.${this.processExtension(file.extension)}`,
    };
  }

  async uploadUrl(file: any, folder = 'upload'): Promise<File> {
    const response = await axios({
      method: 'GET',
      url: file.url,
      responseType: 'stream',
    });

    const id = uuidv4();
    const localPath = path.join(folder, `${id}.${this.processExtension(file.extension)}`);

    response.data.pipe(fs.createWriteStream(localPath));

    const [error] = await new Promise((resolve, reject) => {
      response.data.on('end', () => {
        resolve([null]);
      });

      response.data.on('error', error => {
        reject([error]);
      });
    });
    if (error) throw error;

    return {
      ...file,
      eadbox_url: file.url,
      path: localPath,
      url: `${process.env.FILES_SERVER_URL}/files/public/files/${folder}/${id}.${file.extension}`,
    };
  }

  async delete(file: any, folder = 'answers') {
    if (!file.path) return;
    await fs.promises.unlink(file.path);
  }

  async deleteMany(files: any[], folder = 'answers') {
    for (const file of files) {
      if (!file.path) return;
      await fs.promises.unlink(file.path);
    }
  }

  async uploadBase64(dataBase64: string, mimeType: string, extension = null, folder = 'embeds') {
    extension = extension || mimeType?.split('/')[1];
    const id = uuidv4();
    const filePath = `${folder}/${id}.${extension}`;

    await fs.promises.writeFile(filePath, dataBase64, 'base64');

    return {
      title: `${id}.${extension}`,
      url: process.env.S3_WEBSITE_URL + '/' + filePath,
      path: filePath,
      filename: `${id}.${extension}`,
      mimetype: mimeType,
      extension: extension,
      size: Buffer.from(dataBase64, 'base64').length,
    };
  }
}
