import { DirectoryModule } from './directory/directory.module';
import { BullModule } from '@nestjs/bull';
import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD, APP_INTERCEPTOR, APP_PIPE, APP_FILTER } from '@nestjs/core';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GraphQLModule } from '@nestjs/graphql';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import GraphQLJSON from 'graphql-type-json';
import { RavenInterceptor, RavenModule } from 'nest-raven';
import { S3Module } from 'nestjs-s3';
import { ActivityInterceptor } from './activity.interceptor';
import { AppController } from './app.controller';
import { AssessmentsModule } from './assessments/assessments.module';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { InterappGuard } from './auth/interapp.guard';
import { CertModule } from './cert/cert.module';
import configuration from './config/configuration';
import { CoursesModule } from './courses/courses.module';
import { CoversModule } from './covers/covers.module';
import { FilesModule } from './files/files.module';
import { ForumModule } from './forum/forum.module';
import { ImportInfoModule } from './import-info/import-info.module';
import { KalturaModule } from './kaltura/kaltura.module';
import { HttpExceptionFilter, LoggingInterceptor } from './logger';
import { LoggingModule } from './logging/logging.module';
import { MailModule } from './mail/mail.module';
import { MetatypePipe } from './metatype.pipe';
import { NotificationsModule } from './notifications/notifications.module';
import * as connectionOptions from './ormconfig';
import { PdfModule } from './pdf/pdf.module';
import { ReportsModule } from './reports/reports.module';
import { RequirePermissionsGuard } from './require-permissions.guard';
import { SettingsModule } from './settings/settings.module';
import { SignalModule } from './signal/signal.module';
import { SocketsModule } from './sockets/sockets.module';
import { StatModule } from './stat/stat.module';
import { SystemNotificationsModule } from './system-notifications/system-notifications.module';
import { TagsModule } from './tags/tags.module';
import { TransformInterceptor } from './transform.interceptor';
import { TriggerModule } from './trigger/trigger.module';
import { UsersModule } from './users/users.module';
import { UtilsModule } from './utils/utils.module';
import { WebinarModule } from './webinar/webinar.module';
import { SupportMessagesModule } from './support-messages/support-messages.module';
import { LogsModule } from './clickhouse-logs/clickhouse-logs.module';
import { FaqModule } from './faq/faq.module';
import { RedisModule } from 'nestjs-redis';
import { SubscriptionsModule } from './subscriptions/subscriptions.module';
import { ClientsModule } from './clients/clients.module';
import { SourcesModule } from './sources/sources.module';
import { TopicModule } from './topic/topic.module';
import { SourceModule } from './source/source.module';
import { NomenclatureModule } from './nomenclature/nomenclature.module';
import { NpsModule } from './nps/nps.module';
import { PersonasModule } from './personas/personas.module';
import { MaintenanceModule } from './maintenance/maintenance.module';
import { TopicNomenclatureLinksModule } from './topic-nomenclature-links/topic-nomenclature-links.module';
import { FeatureFlagsModule } from './feature-flags/feature-flags.module';
import { CriteriaBasedAssessmentsModule } from './criteria-based-assessments/criteria-based-assessments.module';
import { BannersModule } from './banners/banners.module';
import { SegmentsModule } from './segments/segments.module';
import { StoreModule } from './store/store.module';
import { AmoModule } from './amo/amo.module';
import { KafkaModule } from './kafka/kafka.module';

@Module({
  imports: [
    CacheModule.register(),
    DirectoryModule,
    ConfigModule.forRoot({
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      useFactory: () => connectionOptions,
    }),
    S3Module.forRootAsync({
      useFactory: () => ({
        config: {
          accessKeyId: process.env.S3_ACCESS_KEY_ID,
          secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
          endpoint: process.env.S3_ENDPOINT,
          Bucket: process.env.S3_BUCKET_NAME,
          s3ForcePathStyle: true,
          signatureVersion: 'v4',
          region: process.env.S3_REGION,
        },
      }),
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      installSubscriptionHandlers: false,
      resolvers: { JSON: GraphQLJSON },
    }),
    MulterModule.register({
      dest: '../upload',
    }),
    EventEmitterModule.forRoot({
      global: true,
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async () => ({
        redis: {
          host: process.env.REDIS_HOST,
          port: Number.parseInt(process.env.REDIS_PORT) || 6379,
        },
      }),
    }),
    RedisModule.forRootAsync({
      useFactory: () => {
        return {
          host: process.env.REDIS_HOST,
          port: Number.parseInt(process.env.REDIS_PORT) || 6379,
        };
      },
    }),
    BullModule.registerQueue({
      name: 'activity',
    }),
    StoreModule,
    KafkaModule,
    RavenModule,
    UsersModule,
    CoursesModule,
    AssessmentsModule,
    AuthModule,
    FilesModule,
    StatModule,
    SocketsModule,
    ImportInfoModule,
    MailModule,
    KalturaModule,
    SignalModule,
    LoggingModule,
    NotificationsModule,
    UtilsModule,
    TagsModule,
    WebinarModule,
    TopicModule,
    SourceModule,
    NomenclatureModule,
    ForumModule,
    CertModule,
    SystemNotificationsModule,
    ReportsModule,
    TriggerModule,
    PdfModule,
    CoversModule,
    SettingsModule,
    SupportMessagesModule,
    LogsModule,
    FaqModule,
    SubscriptionsModule,
    ClientsModule,
    NpsModule,
    SourcesModule,
    PersonasModule,
    MaintenanceModule,
    TopicNomenclatureLinksModule,
    CriteriaBasedAssessmentsModule,
    BannersModule,
    FeatureFlagsModule,
    SegmentsModule,
    AmoModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: InterappGuard,
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RequirePermissionsGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ActivityInterceptor,
    },
    {
      provide: APP_PIPE,
      useClass: MetatypePipe,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useValue: new RavenInterceptor(),
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {}
