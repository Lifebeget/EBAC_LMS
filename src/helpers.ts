import axios from 'axios';
import { createHash } from 'crypto';
import * as fs from 'fs';

export const SlugRegexp = /[a-z0-9_-]/i;
export const KAFKA_PREFIX = `lms.${process.env.ENVIRONMENT || 'local'}`;
export const KAFKA_ENABLED = isTrue(process.env.KAFKA_ENABLED);

export function generatePasswordHash(pass: string) {
  return createHash('md5')
    .update(process.env.PASSWORD_SALT + pass)
    .digest('hex');
}

export function isTrue(val: any) {
  if (typeof val === 'boolean') {
    return val;
  }
  if (typeof val === 'string') {
    return val.toLowerCase() === 'true';
  }
  return false;
}

export function generatePassword(length: number): string {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export async function sleep(time: number): Promise<void> {
  return new Promise<void>(resolve => setTimeout(resolve, time));
}

export async function getFileInfo(url: string): Promise<{
  mimetype: string;
  size: any;
  filename: string;
  extension: string;
  title: string;
  url: string;
}> {
  const name = decodeURIComponent(url.substring(url.lastIndexOf('/') + 1));
  const extension = /[.]/.exec(name) ? /[^.]+$/.exec(name)?.pop() || '' : '';

  const file = {
    mimetype: 'application/octet-stream',
    size: 0,
    filename: name,
    extension,
    title: name,
    url,
  };

  try {
    const result = await axios.head(url);
    file.mimetype = result.headers['content-type'];
    file.size = parseInt(result.headers['content-length']) || 0;
  } catch (e) {
    console.log('Unexpected result while fileInfo.');
    console.dir(e);
  }

  return file;
}

export function generateNumericId(first: string | null, second: string | null, third: string | null, fourth: string | null): string {
  const firstPart = first
    ? first
        .match(/\d+/g)
        .join('')
        .substr(first.match(/\d+/g).join('').length - 5)
    : Math.floor(Math.random() * 99999)
        .toString()
        .padStart(5, '0');

  const secondPart = second
    ? second
        .match(/\d+/g)
        .join('')
        .substr(second.match(/\d+/g).join('').length - 5)
    : Math.floor(Math.random() * 99999)
        .toString()
        .padStart(5, '0');

  const thirdPart = third
    ? third
        .match(/\d+/g)
        .join('')
        .substr(third.match(/\d+/g).join('').length - 5)
    : Math.floor(Math.random() * 99999)
        .toString()
        .padStart(5, '0');

  const fourthPart = fourth
    ? fourth
        .match(/\d+/g)
        .join('')
        .substr(fourth.match(/\d+/g).join('').length - 5)
    : Math.floor(Math.random() * 99999)
        .toString()
        .padStart(5, '0');

  return `${firstPart}-${secondPart}-${thirdPart}-${fourthPart}`;
}

/*
 * Recursively merge properties of two objects
 */
export function MergeRecursive(obj1, obj2) {
  for (const p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = MergeRecursive(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }

  return obj1;
}

export function insertIntoString(str: string, pos: number, insert: string) {
  return str.substr(0, pos) + insert + str.substr(pos);
}

export function removeFromString(str: string, pos: number, length: number) {
  return str.substr(0, pos) + str.substr(pos + length);
}

export function getString(o: any) {
  if (o !== null) {
    if (typeof o === 'string') {
      return o;
    } else {
      return JSON.stringify(o);
    }
  } else {
    return null;
  }
}

export function dbgLog(...args) {
  let string = `[${new Date().toISOString()}] `;
  for (const arg of args) string += `${getString(arg)} `;
  string += '\n';

  if (!process.env.LOG_FILE) {
    console.log(string);
    return;
  }
  try {
    const logFolder = process.env.LOG_FILE.substr(0, process.env.LOG_FILE.lastIndexOf('/'));
    const dbgLogFile = logFolder + '/dbg.log';
    fs.appendFileSync(dbgLogFile, string);
  } catch (e) {
    console.error('welp, no debug for you: ', e);
  }
}

export function isEmptyObject(value) {
  return Object.keys(value).length === 0 && value.constructor === Object;
}

// export const toCamel = (s: string) => {
//   return s.replace(/([-_][a-z])/gi, ($1) => {
//     return $1.toUpperCase().replace('-', '').replace('_', '');
//   });
// };

// const isObject = function (o: any) {
//   return o === Object(o) && !Array.isArray(o) && typeof o !== 'function';
// };

// export const keysToCamel = function (o: any) {
//   if (isObject(o)) {
//     const n = {};

//     Object.keys(o).forEach((k) => {
//       n[toCamel(k)] = o[k] instanceof Date ? o[k] : keysToCamel(o[k]);
//     });

//     return n;
//   } else if (Array.isArray(o)) {
//     return o.map((i) => {
//       return keysToCamel(i);
//     });
//   }

//   return o;
// };

export function slugify(str: string): string {
  return str
    .toLowerCase()
    .trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '');
}

export function logMemory(message?: string) {
  console.log('-----------------------');
  if (message) {
    console.log(message);
  }
  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
}
