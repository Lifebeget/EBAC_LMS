import { Controller, Get, NotFoundException, Param } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiProperty, ApiTags } from '@nestjs/swagger';
import { strict } from 'assert';
import { SourceService } from './source.service';
import { Source } from '@entities/source.entity';

export class GetVariablesDto {
  @ApiProperty({ required: false })
  variableGroupId?: string;
}

@ApiTags('sources')
@Controller('sources')
export class SourceController {
  constructor(private readonly sourceService: SourceService) {}

  @Get(':id')
  @ApiOkResponse({ type: Source })
  @ApiNotFoundResponse()
  async getSourceById(@Param('id') sourceId: string) {
    const source = await this.sourceService.findOne(sourceId);
    strict.ok(source, new NotFoundException());
    return source;
  }

  @Get(':external-id')
  @ApiOkResponse({ type: Source })
  @ApiNotFoundResponse()
  async getSourceByExternalId(@Param('id') sourceExternalId: string) {
    const source = await this.sourceService.findOneByExternalId(sourceExternalId);
    strict.ok(source, new NotFoundException());
    return source;
  }
}
