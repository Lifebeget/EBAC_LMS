import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { SettingsService } from 'src/settings/settings.service';
import { Connection, getConnection, Repository } from 'typeorm';
import { Source } from '@entities/source.entity';
import { SourceActionCms } from '@enums/source-action-cms';
import { SourceActionLms } from '@enums/source-action-lms';
import { SourcePageTypeCms } from '@enums/source-page-type-cms';
import { SourcePageTypeLms } from '@enums/source-page-type-lms';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class SourceService {
  constructor(
    @Inject(forwardRef(() => SettingsService))
    private settingsService: SettingsService,

    @InjectConnection()
    private readonly connection: Connection,

    @InjectRepository(Source)
    private readonly sourceRepository: Repository<Source>,
  ) {}

  save(dto: Source) {
    return this.sourceRepository.save(dto);
  }

  async find(values) {
    return this.sourceRepository.find({
      ...values,
      relations: ['nomenclature'],
    });
  }

  @Transactional()
  async findOne(id: string) {
    return this.sourceRepository.findOne(id);
  }

  async findOneByExternalId(externalId: string) {
    const res = this.sourceRepository.find({
      where: { externalId },
    });
    return res[0];
  }

  async upsert(values) {
    let columns = getConnection()
      .getMetadata(Source)
      .ownColumns.map(column => {
        return column.databaseName;
      });
    columns = columns.filter(col => {
      return col !== 'id';
    });
    const res = await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Source)
      .values(values)
      .orUpdate(columns, ['external_id'])
      .execute();
    return res;
  }

  async delete(values) {
    return this.sourceRepository.softDelete(values);
  }
  cmsToLmsAction(cmsAction) {
    switch (cmsAction) {
      case SourceActionCms.LEAD:
        return SourceActionLms.LEAD;
      case SourceActionCms.CONSULT:
        return SourceActionLms.CONSULT;
      case SourceActionCms.SUBSCRIPTION:
        return SourceActionLms.SUBSCRIPTION;
      default:
        return null;
    }
  }
  cmsToLmsPageType(cmsPageType) {
    switch (cmsPageType) {
      case SourcePageTypeCms.LANDING:
        return SourcePageTypeLms.LANDING;
      case SourcePageTypeCms.EVENT:
        return SourcePageTypeLms.EVENT;
      case SourcePageTypeCms.OTHER:
        return SourcePageTypeLms.OTHER;
      case SourcePageTypeCms.WEBINAR:
        return SourcePageTypeLms.WEBINAR;
      case SourcePageTypeCms.WEBINAR_SALE:
        return SourcePageTypeLms.WEBINAR_SALE;
      case SourcePageTypeCms.FACEBOOK:
        return SourcePageTypeLms.FACEBOOK;
      default:
        return null;
    }
  }
}
