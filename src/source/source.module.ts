import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsModule } from 'src/settings/settings.module';
import { SourceService } from './source.service';
import { Source } from '@entities/source.entity';
import { SourceController } from './source.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Source]), forwardRef(() => SettingsModule)],
  controllers: [SourceController],
  providers: [SourceService],
  exports: [SourceService],
})
export class SourceModule {}
