import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { FilesService } from 'src/files/files.service';
import { counterTemplate } from '../lib/types/ImportCounters';
import { SourceService } from './source.service';
import { Source } from '@entities/source.entity';

export class ImportSources {
  private filesService: FilesService;
  private sourcesService: SourceService;
  private debug: boolean;
  constructor(context: INestApplicationContext, private counter = { ...counterTemplate }) {
    this.filesService = context.get(FilesService);
    this.sourcesService = context.get(SourceService);
    this.debug = true;
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  async import(_concurrency = 3, debug = true) {
    console.log('Starting import sources');
    this.debug = debug;

    await this.importData();

    this.printStats(this.counter);
  }

  async importData() {
    const start = Date.now();
    const cmsSources: Array<any> = await this.downloadAll();

    if (this.debug) console.log(`Load ${Date.now() - start}ms count = ${cmsSources.length}`);

    if (!cmsSources.length) return;
    const cmsSourceLength = cmsSources.length;
    for (const [cmsSourceIndex, cmsSource] of Object.entries(cmsSources)) {
      try {
        await this.upsert(cmsSource);
      } catch (e) {
        if (this.debug) {
          console.log(`Source create/update Error. Source external id ${cmsSource.id}.`);
          console.log(`Error:`, e);
        }
      }
      if (this.debug) console.log(`${+cmsSourceIndex + 1}/${cmsSourceLength}`, 'source id', cmsSource.id);
    }
    if (this.debug) console.log('Save time', Date.now() - start, 'ms');
  }

  async downloadAll(): Promise<any> {
    const result: Array<any> = [];
    let pageCounter = 1;
    const itemsPerPage = 300;
    while (true) {
      try {
        const res = await axios
          .get(process.env.CMS_SOURCES_URL, {
            params: {
              limit: itemsPerPage,
              offset: itemsPerPage * (pageCounter - 1),
              apiKey: process.env.CMS_API_KEY,
            },
          })
          .catch(err => {
            console.error('import sources', err);
          });
        if (!res || !(<any>res)?.data?.data?.fields?.length) {
          break;
        }
        // return res?.data.data.fields;

        result.push(...res?.data.data.fields);
        pageCounter++;
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }
  async upsert(cmsSource: any) {
    const source = new Source();
    source.externalId = cmsSource.id;
    source.name = cmsSource.name;
    source.pageType = this.sourcesService.cmsToLmsPageType(cmsSource.page_type);
    source.formType = 'form';
    source.action = this.sourcesService.cmsToLmsAction(cmsSource.action);
    source.nomenclatureId = cmsSource.nomenclature_id;
    source.created = cmsSource.created;
    source.modified = cmsSource.modified;
    source.esListId = cmsSource.es_list_id;
    source.esWelcomeMessageId = cmsSource.es_welcome_message_id;

    const dbSource = await this.sourcesService.find({
      select: ['externalId'],
      where: { externalId: cmsSource.id },
      withDeleted: true,
    });
    if (dbSource.length > 0) {
      this.counter.updated++;
    } else {
      this.counter.created++;
    }
    const _res = await this.sourcesService.upsert(source);
  }
}
