import { Assessment } from '@entities/assessment.entity';
import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { ServiceLevel } from '@entities/service-level.entity';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { Submission } from '@entities/submission.entity';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TariffsService } from './tariffs.service';

@Injectable()
export class AssessmentLevelsService {
  constructor(
    private readonly tariffService: TariffsService,
    @InjectRepository(Assessment)
    private readonly assessmentsRepository: Repository<Assessment>,
    @InjectRepository(Submission)
    private readonly submissionRepository: Repository<Submission>,
    @InjectRepository(SubmissionTariff)
    private readonly submissionTariffRepository: Repository<SubmissionTariff>,
    @InjectRepository(ServiceLevel)
    private readonly repository: Repository<ServiceLevel>,
  ) {}

  getDefault() {
    return this.repository.findOne({ where: { isDefault: true } });
  }

  async hasSubmissionsOfLevel(levelId: string) {
    const cnt = await this.submissionRepository
      .createQueryBuilder('s')
      .innerJoin('s.tariff', 'st')
      .innerJoin('st.createTariff', 'ct')
      .innerJoin('st.gradeTariff', 'gt')
      .where(
        `ct.level_id = :createLevelId or 
         gt.level_id = :gradeLevelId`,
        {
          createLevelId: levelId,
          gradeLevelId: levelId,
        },
      )
      .getCount();

    return cnt > 0;
  }

  async changeLevelToDefault(oldLevelId: string) {
    const defaultLevel = await this.getDefault();
    return this.changeLevel(oldLevelId, defaultLevel.id);
  }

  async changeLevel(oldLevelId: string, newLevelId: string) {
    await this.assessmentsRepository
      .createQueryBuilder('l')
      .update()
      .set({ levelId: newLevelId })
      .where('level_id = :oldLevelId', { oldLevelId })
      .execute();

    await this.changeLevelOfSubmissions(oldLevelId, newLevelId);
  }

  async changeLevelOfAssessment(oldLevelId: string, newLevelId: string, assessmentId: string) {
    return this.changeLevelOfSubmissions(oldLevelId, newLevelId, assessmentId);
  }

  async changeLevelOfSubmissions(oldLevelId: string, newLevelId: string, assessmentId?: string) {
    const newTariff = await this.tariffService.getTariff(newLevelId, new Date());

    const idsQb = this.submissionTariffRepository
      .createQueryBuilder('st')
      .innerJoin('st.submission', 's')
      .innerJoin('st.createTariff', 't')
      .where('t.level_id = :oldLevelId', { oldLevelId })
      .andWhere('s.gradedAt is NULL');

    if (assessmentId) {
      idsQb.andWhere('s.assessment_id = :assessmentId', {
        assessmentId,
      });
    }

    const submissionTariffs = await idsQb.getMany();

    if (submissionTariffs.length == 0) {
      return;
    }

    await this.submissionTariffRepository.update(
      submissionTariffs.map(st => st.submissionId),
      {
        createTariffId: newTariff.id,
        gradeTariffId: newTariff.id,
      },
    );
  }

  async getTariffForAssessment(assessmentId: string, date: Date) {
    const assessment = await this.assessmentsRepository.findOne(assessmentId);
    return this.tariffService.getTariff(assessment.levelId, date);
  }

  async getCourses(levelId: string) {
    const courses = await this.assessmentsRepository
      .createQueryBuilder('a')
      .select('c.id, c.title')
      .distinct()
      .innerJoin('a.lecture', 'l')
      .innerJoin('l.course', 'c')
      .where('a.level_id = :levelId', { levelId })
      .getRawMany();

    return courses;
  }

  async getLevelForSubmission(submissionId: string) {
    return this.repository
      .createQueryBuilder('l')
      .innerJoin(ServiceLevelTariff, 't', 't.level_id = l.id')
      .innerJoin(SubmissionTariff, 'st', 'st.createTariff = t.id')
      .where('st.submission_id = :submissionId', { submissionId })
      .getOne();
  }

  async getTariffForSubmission(submissionId: string, date = new Date()) {
    const level = await this.getLevelForSubmission(submissionId);

    if (!level) {
      return null;
    }

    return this.tariffService.getTariff(level.id, date);
  }

  async getCreateTariff(assessmentId: string, date: Date, isQuestion: boolean) {
    if (isQuestion) {
      return await this.tariffService.getTariffForQuestion(date);
    }

    const serviceLevelId = await this.getLevelId(assessmentId);

    return await this.tariffService.getTariff(serviceLevelId, date);
  }

  async getGradeTariff(submission: Submission, date = new Date()) {
    if (submission.isQuestion) {
      return await this.tariffService.getTariffForQuestion(date);
    }

    if (submission.tariff?.createTariff.level.target === ServiceLevelTarget.Submission) {
      return await this.getTariffForSubmission(submission.id, date);
    }

    return this.getTariffForAssessment(submission.assessment.id, date);
  }

  async getLevelId(assessmentId: string) {
    const result = await this.assessmentsRepository.findOne(assessmentId, {
      select: ['levelId'],
    });

    return result.levelId;
  }
}
