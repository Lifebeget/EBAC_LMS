import { Permission } from '@enums/permission.enum';
import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateTariffDto } from './dto/create-tariff.dto';
import { QueryTariffScheduleDto } from './dto/query-tariff-schedule.dto';
import { UpdateTariffDto } from './dto/update-tariff.dto';
import { TariffsService } from './tariffs.service';

@ApiTags('directories')
@ApiBearerAuth()
@Controller('')
export class TariffsController {
  constructor(private readonly service: TariffsService) {}

  @Transactional()
  @Post('assessments/levels/:levelId/tariffs')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Create service level tariff record' })
  @ApiResponse({
    status: 200,
    description: 'Returns created tariff',
  })
  @PgConstraints()
  createTariff(@Param('levelId', new ParseUUIDPipe()) levelId: string, @Body() dto: CreateTariffDto) {
    return this.service.createTariff(levelId, dto);
  }

  @Transactional()
  @Patch('assessments/levels/:levelId/tariffs/:tariffId')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Updates an service level tariff record' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  updateTariff(
    @Param('levelId', new ParseUUIDPipe()) levelId: string,
    @Param('tariffId', new ParseUUIDPipe()) tariffId: string,
    @Body() dto: UpdateTariffDto,
  ) {
    return this.service.updateTariff(levelId, tariffId, dto);
  }

  @Transactional()
  @Delete('assessments/levels/:levelId/tariffs/:tariffId')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Updates an service level tariff record' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  removeTariff(@Param('levelId', new ParseUUIDPipe()) levelId: string, @Param('tariffId', new ParseUUIDPipe()) tariffId: string) {
    return this.service.removeTariff(levelId, tariffId);
  }

  @Get('assessments/levels/tariffs/schedule')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_VIEW)
  @ApiOperation({ summary: 'Get tariffs schedule' })
  @ApiResponse({
    status: 200,
  })
  @PgConstraints()
  getSchedule(@Query() dto: QueryTariffScheduleDto) {
    return this.service.getSchedule(dto);
  }
}
