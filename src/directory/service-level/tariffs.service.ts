import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { ServiceLevel } from '@entities/service-level.entity';
import { Submission } from '@entities/submission.entity';
import { ServiceLevelTarget, TariffSchedule, TariffScheduleEntry } from '@lib/api/directory/service-levels';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import assert from 'assert';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { CreateTariffDto } from './dto/create-tariff.dto';
import { QueryTariffScheduleDto } from './dto/query-tariff-schedule.dto';
import { UpdateTariffDto } from './dto/update-tariff.dto';

Injectable();
export class TariffsService {
  constructor(
    @InjectRepository(ServiceLevelTariff)
    private readonly tariffsRepository: Repository<ServiceLevelTariff>,
    @InjectRepository(ServiceLevel)
    private readonly levelsRepository: Repository<ServiceLevel>,
    @InjectRepository(Submission)
    private readonly submissionsRepository: Repository<Submission>,
  ) {}

  async findTariffsForLevel(levelId: string) {
    return this.tariffsRepository.find({ where: { levelId } });
  }

  async createTariff(levelId: string, dto: CreateTariffDto) {
    await this.tariffsRepository.save({
      ...dto,
      activeTo: dayjs('9999-12-31').toDate(),
      level: { id: levelId },
    });

    const existedTariffs = await this.findTariffsForLevel(levelId);

    const tariffs = [...existedTariffs];

    const organized = TariffSchedule.organizeOneLevelTariffs(tariffs);

    return this.tariffsRepository.save(organized);
  }

  async updateTariff(levelId: string, tariffId: string, dto: UpdateTariffDto) {
    const existedTariffs = await this.findTariffsForLevel(levelId);

    const index = existedTariffs.findIndex(t => t.id === tariffId);

    assert(index >= 0, 'Tariff does not exists in tariffs of given level');

    existedTariffs[index] = { ...existedTariffs[index], ...dto };

    const organized = TariffSchedule.organizeOneLevelTariffs(existedTariffs);

    return this.tariffsRepository.save(organized);
  }

  async removeTariff(levelId: string, tariffId: string) {
    if (this.hasSubmissionsOfTariff(tariffId)) {
      await this.tariffsRepository.update(tariffId, { activeTo: new Date() });
      await this.tariffsRepository.softDelete(tariffId);
    } else {
      await this.tariffsRepository.delete(tariffId);
    }

    const existedTariffs = await this.findTariffsForLevel(levelId);
    const organized = TariffSchedule.organizeOneLevelTariffs(existedTariffs);

    const saved = await this.tariffsRepository.save(organized);

    return saved;
  }

  async getTariff(levelId: string, date: Date, withDeleted = false) {
    const qb = this.tariffsRepository.createQueryBuilder('t');

    if (withDeleted) {
      qb.withDeleted();
    }

    return qb
      .innerJoinAndSelect('t.level', 'l')
      .where(':date between t.active_from and t.active_to', { date })
      .andWhere('level_id = :levelId', { levelId })
      .getOne();
  }

  async getTariffForQuestion(date: Date) {
    return this.getTariffForTarget(date, ServiceLevelTarget.Question);
  }

  async getTariffForForum(date: Date) {
    return this.getTariffForTarget(date, ServiceLevelTarget.Forum);
  }

  async getTariffForTarget(date: Date, target: ServiceLevelTarget) {
    const level = await this.levelsRepository.findOne({
      target,
    });
    return this.getTariff(level.id, date);
  }

  async hasSubmissionsOfTariff(tariffId: string) {
    const cnt = await this.submissionsRepository
      .createQueryBuilder('s')
      .innerJoin('s.tariff', 't')
      .where('t.createTariffId = :createTariffId or t.gradeTariffId = :gradeTariffId', {
        createTariffId: tariffId,
        gradeTariffId: tariffId,
      })
      .getCount();

    return cnt > 0;
  }

  async getSchedule(query: QueryTariffScheduleDto): Promise<TariffScheduleEntry[]> {
    const tariffs = await this.getTariffsActiveInPeriod(query.startDate, query.endDate);

    const schedule = new TariffSchedule(dayjs, tariffs, query.startDate);

    return schedule.calculate(
      {
        periodStart: query.startDate,
        periodEnd: query.endDate,
      },
      query.locale,
      query.thisLevelsOnly,
    );
  }

  async getTariffsActiveInPeriod(startDate: Date | string, endDate: Date | string) {
    return this.tariffsRepository
      .createQueryBuilder('t')
      .withDeleted()
      .innerJoinAndSelect('t.level', 'l')
      .where('t.activeFrom < :endDate', { endDate })
      .andWhere('coalesce(t.deleted, t.activeTo) > :startDate', { startDate })
      .addOrderBy('l.target', 'DESC')
      .addOrderBy('l.title')
      .addOrderBy('t.active_from')
      .getMany();
  }

  getLevelsIds(tariffs: ServiceLevelTariff[]) {
    return tariffs.reduce((levels: string[], t) => {
      const exists = levels.includes(t.levelId);

      if (!exists) {
        levels.push(t.levelId);
      }

      return levels;
    }, []);
  }
}
