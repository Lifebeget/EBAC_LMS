import { CreateServiceLevelDto } from './dto/create-service-level.dto';
import { ServiceLevel } from '../../lib/entities/service-level.entity';
import { ServiceLevelsService } from './service-levels.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { PgConstraints } from 'src/pg.decorators';
import { UpdateServiceLevelDto } from './dto/update-service-level.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { QueryServiceLevelsDto } from './dto/query-service-levels.dto';

@ApiTags('directories')
@ApiBearerAuth()
@Controller('')
export class ServiceLevelsController {
  constructor(private readonly service: ServiceLevelsService) {}

  @Transactional()
  @Post('assessments/levels')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Create a new service level' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Created service level',
    type: ServiceLevel,
  })
  @PgConstraints()
  async create(@Body() dto: CreateServiceLevelDto) {
    const level = await this.service.create(dto);
    return level;
  }

  @Get('assessments/levels')
  @ApiOperation({ summary: 'Get list of service levels' })
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_VIEW)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of all service levels',
    type: [ServiceLevel],
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  find(@Query() query: QueryServiceLevelsDto) {
    return this.service.find(query);
  }

  @Get('assessments/levels/for-submissions')
  @ApiOperation({ summary: 'Get list of assessment levels for submissions' })
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_VIEW)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of all assessment levels for submissions',
    type: [ServiceLevel],
  })
  @PgConstraints()
  getForSubmissionsAndQuestions() {
    return this.service.findByTargets(['submission', 'question']);
  }

  @Get('assessments/levels/:id')
  @ApiOperation({ summary: 'Get one service level by id' })
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_VIEW)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Requested service level',
    type: ServiceLevel,
  })
  @PgConstraints()
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.service.findOne(id);
  }

  @Transactional()
  @Patch('assessments/levels/:id')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Updates an service level' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() dto: UpdateServiceLevelDto) {
    return this.service.update(id, dto);
  }

  @Transactional()
  @Delete('assessments/levels/:id')
  @RequirePermissions(Permission.ASSESSMENT_LEVELS_MANAGE)
  @ApiOperation({ summary: 'Deletes an service level' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.service.remove(id);
  }
}
