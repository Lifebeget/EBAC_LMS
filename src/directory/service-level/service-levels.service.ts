import { ServiceLevel } from '../../lib/entities/service-level.entity';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, FindManyOptions, Repository } from 'typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateServiceLevelDto } from './dto/create-service-level.dto';
import { UpdateServiceLevelDto } from './dto/update-service-level.dto';
import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import * as assert from 'assert';
import { AssessmentLevelsService } from './assessment-levels.service';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { QueryServiceLevelsDto } from './dto/query-service-levels.dto';

@Injectable()
export class ServiceLevelsService {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
    @InjectRepository(ServiceLevel)
    private readonly repository: Repository<ServiceLevel>,
    @InjectRepository(ServiceLevelTariff)
    private readonly tariffRepository: Repository<ServiceLevelTariff>,

    private readonly assessmentLevels: AssessmentLevelsService,
  ) {}

  async create(dto: CreateServiceLevelDto): Promise<ServiceLevel> {
    await this.canAddOrExcept(dto);

    if (dto.isDefault) {
      await this.clearIsDefaultForAll();
    }
    return this.repository.save(dto);
  }

  find({ withDeleted, withoutTariffs, ids, target }: QueryServiceLevelsDto): Promise<ServiceLevel[]> {
    const tariffsJoin = withoutTariffs ? 'leftJoinAndSelect' : 'innerJoinAndSelect';

    const options: FindManyOptions = {
      join: {
        alias: 'level',
        [tariffsJoin]: {
          tariffs: 'level.tariffs',
        },
      },
      order: {
        target: 'DESC',
        title: 'ASC',
      },
      withDeleted,
    };

    if (target) {
      options.where = { target };
    }

    return ids ? this.repository.findByIds(ids, options) : this.repository.find(options);
  }

  @Transactional()
  async findOne(id: string): Promise<ServiceLevel> {
    const level = await this.repository.findOne(id, { relations: ['tariffs'] });

    level.courses = await this.assessmentLevels.getCourses(id);

    return level;
  }

  async update(id: string, dto: UpdateServiceLevelDto) {
    if (dto.target !== ServiceLevelTarget.Submission) {
      dto.isDefault = false;
    }

    if (dto.isDefault) {
      await this.clearIsDefaultForAll();
    }

    const updateFields = {
      id,
      title: dto.title,
      isDefault: dto.isDefault,
    };
    return this.repository.save(updateFields);
  }

  async remove(id: string) {
    await this.canDeleteOrExcept(id);

    if (await this.assessmentLevels.hasSubmissionsOfLevel(id)) {
      return this.softDelete(id);
    }

    return this.hardDelete(id);
  }

  async hardDelete(id: string) {
    await this.assessmentLevels.changeLevelToDefault(id);

    await this.tariffRepository.delete({ levelId: id });
    return this.repository.delete(id);
  }

  async softDelete(id: string) {
    await this.assessmentLevels.changeLevelToDefault(id);

    await this.tariffRepository.softDelete({ levelId: id });
    return this.repository.softDelete(id);
  }

  async canAddOrExcept(dto: CreateServiceLevelDto) {
    if (dto.target === ServiceLevelTarget.Submission) {
      return;
    }

    const count = await this.repository.count({ target: dto.target });

    assert(count === 0, new BadRequestException(`Cant add another level for target "${dto.target}"`));
  }

  async canDeleteOrExcept(id: string) {
    const level = await this.findOne(id);

    assert(!level.isDefault, new BadRequestException('Cant delete default level'));

    assert(level.target === ServiceLevelTarget.Submission, new BadRequestException(`Cant delete level for target "${level.target}"`));
  }

  async clearIsDefaultForAll() {
    await this.repository
      .createQueryBuilder()
      .update()
      .set({ isDefault: false })
      .where('isDefault = :isDefault', { isDefault: true })
      .execute();
  }

  findByTargets(targets: string[]): Promise<ServiceLevel[]> {
    return this.repository
      .createQueryBuilder('assessment_level')
      .where('assessment_level.target in (:...targets)', { targets })
      .orderBy('assessment_level.title', 'ASC')
      .getMany();
  }
}
