import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsDate } from 'class-validator';
export class CreateTariffDto {
  @ApiProperty({ description: 'Tariff activation date' })
  @IsDate()
  @Type(() => Date)
  activeFrom: Date;

  @ApiProperty({
    description: 'Period after which the task becomes urgent',
  })
  urgentHours: number;

  @ApiProperty({
    description: 'Period after which the task becomes outdated',
  })
  outdatedHours: number;

  @ApiProperty({ description: 'Tariff amount' })
  value: number;
}
