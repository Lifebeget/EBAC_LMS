import { IsBoolean, ValidateIf } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { Optional } from '@nestjs/common';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { ToBoolean } from 'src/decorators/to-boolean.decorator';

export class QueryServiceLevelsDto {
  @ApiProperty({ description: 'Include levels without tariffs' })
  @ValidateIf(o => o.hasOwnProperty('withoutTariffs'))
  @ToBoolean()
  @IsBoolean()
  withoutTariffs?: boolean;

  @ApiProperty({ description: 'Include soft deleted levels' })
  @ValidateIf(o => o.hasOwnProperty('withDeleted'))
  @ToBoolean()
  @IsBoolean()
  withDeleted?: boolean;

  @ApiProperty({ description: 'List of ids to retrieve', default: [] })
  @Optional()
  ids?: string[];

  @ApiProperty({ description: 'List of ids to retrieve', enum: ServiceLevelTarget })
  @Optional()
  target?: ServiceLevelTarget;
}
