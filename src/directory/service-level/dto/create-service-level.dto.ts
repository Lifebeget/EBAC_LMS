import { ApiProperty } from '@nestjs/swagger';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
export class CreateServiceLevelDto {
  @ApiProperty({ description: 'Title of level' })
  title: string;

  @ApiProperty({ description: 'Is this level is default' })
  isDefault: boolean;

  target: ServiceLevelTarget;
}
