import { CreateServiceLevelDto } from './create-service-level.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateServiceLevelDto extends PartialType(CreateServiceLevelDto) {}
