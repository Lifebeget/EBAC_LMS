import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsDate } from 'class-validator';
import { ServiceLevelTitle } from '@lib/api/directory/service-levels';
export class QueryTariffScheduleDto {
  @ApiProperty({ description: 'Period start date' })
  @IsDate()
  @Type(() => Date)
  startDate: Date;

  @ApiProperty({ description: 'Period end date' })
  @IsDate()
  @Type(() => Date)
  endDate: Date;

  @ApiProperty({ description: 'Frontend locale' })
  @Type(() => String)
  locale: string;

  @ApiProperty({ description: 'Show only this levels' })
  thisLevelsOnly: ServiceLevelTitle[];
}
