import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { ServiceLevel } from '@entities/service-level.entity';
import { Assessment } from '@entities/assessment.entity';
import { Calendar } from '@entities/calendar.entity';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { Submission } from '@entities/submission.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceLevelsController } from './service-level/service-levels.controller';
import { ServiceLevelsService } from './service-level/service-levels.service';
import { CalendarsController } from './calendar/calendars.controller';
import { CalendarsService } from './calendar/calendars.service';
import { ForumMessageTariff } from '@entities/forum-tariff.entity';
import { AssessmentLevelsService } from './service-level/assessment-levels.service';
import { TariffsService } from './service-level/tariffs.service';
import { TariffsController } from './service-level/tariffs.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([Assessment, Submission, ServiceLevel, ServiceLevelTariff, SubmissionTariff, ForumMessageTariff, Calendar]),
  ],
  controllers: [ServiceLevelsController, TariffsController, CalendarsController],
  providers: [ServiceLevelsService, TariffsService, AssessmentLevelsService, CalendarsService],
  exports: [ServiceLevelsService, TariffsService, AssessmentLevelsService, CalendarsService],
})
export class DirectoryModule {}
