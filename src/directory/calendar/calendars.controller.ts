import { Calendar } from '@entities/calendar.entity';
import { Permission } from '@enums/permission.enum';
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { DeleteResult } from 'typeorm';
import { CalendarsService } from './calendars.service';
import { CreateCalendarDto } from './dto/create-calendar.dto';
import { UpdateCalendarDto } from './dto/update-calendar.dto';
@ApiTags('calendars')
@Controller('calendars')
export class CalendarsController {
  constructor(private readonly service: CalendarsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all calendars' })
  @ApiResponse({ status: 200, type: [Calendar] })
  @RequirePermissions(Permission.CALENDARS_VIEW)
  @PgConstraints()
  findAll() {
    return this.service.find();
  }

  @Post('/')
  @ApiOperation({ summary: 'Create calendar' })
  @ApiResponse({ status: 201, type: Calendar, description: 'Created calendar' })
  @RequirePermissions(Permission.CALENDARS_MANAGE)
  @PgConstraints()
  create(@Body() dto: CreateCalendarDto) {
    return this.service.create(dto);
  }

  @Patch(':year')
  @ApiOperation({ summary: 'Update calendar' })
  @ApiResponse({ status: 200, type: Calendar, description: 'Updated calendar' })
  @RequirePermissions(Permission.CALENDARS_MANAGE)
  @PgConstraints()
  update(@Param('year') year: number, @Body() dto: UpdateCalendarDto) {
    return this.service.update(+year, dto);
  }

  @Delete(':year')
  @ApiOperation({ summary: 'Delete calendar' })
  @ApiResponse({ status: 200, type: DeleteResult })
  @RequirePermissions(Permission.CALENDARS_MANAGE)
  @PgConstraints()
  remove(@Param('year') year: number) {
    return this.service.remove(year);
  }
}
