import { Calendar } from '@entities/calendar.entity';
import { Calendars, ICalendar } from '@lib/helpers/date';
import { CalendarValidator } from '@lib/api/directory/calendars';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCalendarDto } from './dto/create-calendar.dto';
import { UpdateCalendarDto } from './dto/update-calendar.dto';
import * as assert from 'assert';
import * as dayjs from 'dayjs';

@Injectable()
export class CalendarsService {
  private lastRefreshAt: Date = new Date();
  private calendars?: Calendars;
  private readonly validator = new CalendarValidator();

  constructor(
    @InjectRepository(Calendar)
    private readonly repository: Repository<Calendar>,
  ) {}

  async create(dto: CreateCalendarDto) {
    assert(this.validator.validate(dto), 'Invalid calendar data');

    const calendar = await this.repository.findOne(dto.year);
    assert(!calendar, 'Calendar for this year allready exists');

    this.removeDublicates(dto);
    return this.repository.save(dto);
  }

  async find() {
    return this.repository.find();
  }

  async update(year: number, dto: UpdateCalendarDto) {
    assert(this.validator.validate(dto), 'Invalid calendar data');
    this.removeDublicates(dto);
    return this.repository.save({ ...dto, year });
  }

  async remove(year: number) {
    return this.repository.delete(year);
  }

  async getCalendars() {
    if (!this.calendars || this.timeout()) {
      this.calendars = new Calendars(await this.repository.find(), dayjs);
    }

    return this.calendars;
  }

  private timeout() {
    return new Date().getTime() - this.lastRefreshAt.getTime() > 300000;
  }

  private removeDublicates(calendar: ICalendar) {
    calendar.vacationDays = Array.from(new Set(calendar.vacationDays));
    calendar.workingDays = Array.from(new Set(calendar.workingDays));
  }
}
