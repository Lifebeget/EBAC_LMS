import { ICalendar } from '@lib/helpers/date';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCalendarDto implements ICalendar {
  @ApiProperty({ description: 'Year of calendar' })
  year: number;

  @ApiProperty({ description: 'Array of vacation days (MM-DD)' })
  vacationDays: string[];

  @ApiProperty({ description: 'Array of forced working days (MM-DD)' })
  workingDays: string[];
}
