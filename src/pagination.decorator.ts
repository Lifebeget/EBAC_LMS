import { Query } from '@nestjs/common';
import { PaginationDto } from './pagination.dto';

export const Pagination = () =>
  Query(undefined, {
    transform: (value: PaginationDto) => {
      const page = value.page ? parseInt('' + value.page) : 1;
      const itemsPerPage = value.itemsPerPage ? parseInt('' + value.itemsPerPage) : 100;
      const showAll = value.showAll && '' + value.showAll === 'true' ? true : false;
      return { page, itemsPerPage, showAll };
    },
  });
