import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSupportMessageDto } from './dto/create-support-message.dto';
import { UpdateSupportMessageDto } from './dto/update-support-message.dto';
import { SupportMessage } from '../lib/entities/support-message.entity';
import { FindSupportMessageDto } from './dto/find-support-message.dto';
import { SupportMessagesTriggerService } from '../trigger/services/support-messages-trigger.service';

@Injectable()
export class SupportMessagesService {
  constructor(
    @InjectRepository(SupportMessage)
    private supportMessagesRepository: Repository<SupportMessage>,
    @Inject(forwardRef(() => SupportMessagesTriggerService))
    private supportMessagesTriggerService: SupportMessagesTriggerService,
  ) {}

  async create({ userId, ...dto }: CreateSupportMessageDto & { ip?: string }) {
    const result = await this.supportMessagesRepository.save({
      ...dto,
      user: userId ? { id: userId } : null,
    });
    this.supportMessagesTriggerService.supportMessageSent(result);
    return result;
  }

  find({ userId }: FindSupportMessageDto = {}) {
    const qb = this.supportMessagesRepository.createQueryBuilder('support_message').leftJoinAndSelect('support_message.user', 'user');
    if (userId) {
      qb.andWhere('user_id=:userId', { userId });
    }
    return qb.getMany();
  }

  update(supportMessageId: string, dto: UpdateSupportMessageDto) {
    return this.supportMessagesRepository.save({
      ...dto,
      id: supportMessageId,
    });
  }

  remove(supportMessageId: string) {
    return this.supportMessagesRepository.delete(supportMessageId);
  }
}
