import { forwardRef, Module } from '@nestjs/common';
import { SupportMessagesService } from './support-messages.service';
import { SupportMessagesController } from './support-messages.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SupportMessage } from '../lib/entities/support-message.entity';
import { User } from '@entities/user.entity';
import { Trigger } from '@entities/trigger.entity';
import { AmoModule } from '../amo/amo.module';
import { TriggerModule } from 'src/trigger/trigger.module';

@Module({
  imports: [
    AmoModule,
    TypeOrmModule.forFeature([SupportMessage]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([User]),
    forwardRef(() => TriggerModule),
  ],
  controllers: [SupportMessagesController],
  providers: [SupportMessagesService],
})
export class SupportMessagesModule {}
