import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class FindSupportMessageDto {
  @ApiProperty({ description: 'Message author ID', required: false })
  @IsOptional()
  @IsUUID()
  userId?: string;
}
