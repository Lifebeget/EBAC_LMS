import { ESupportMessageStatus } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsObject, IsOptional, IsString, IsUUID } from 'class-validator';

export class CreateSupportMessageDto {
  @ApiProperty({ description: 'Message author ID', required: false })
  @IsUUID()
  @IsOptional()
  userId?: string;

  @ApiProperty({ description: 'Message topic' })
  @IsString()
  topic: string;

  @ApiProperty({ description: 'Message text' })
  @IsString()
  text: string;

  @ApiProperty({ description: 'AMO CRM id', required: false, nullable: true })
  @IsOptional()
  @IsString()
  amoCrmId?: string;

  @ApiProperty({ description: 'Message data' })
  @IsObject()
  data: Record<string, any>;

  @ApiProperty({
    description: 'Additional email',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  email?: string;

  @ApiProperty({
    description: 'Additional name',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    description: 'Status',
    enum: ESupportMessageStatus,
    default: ESupportMessageStatus.NEW,
  })
  @IsOptional()
  @IsEnum(ESupportMessageStatus)
  status?: ESupportMessageStatus;
}
