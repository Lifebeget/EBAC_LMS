import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateSupportMessageDto } from './create-support-message.dto';

export class UpdateSupportMessageDto extends PartialType(OmitType(CreateSupportMessageDto, ['userId'])) {}
