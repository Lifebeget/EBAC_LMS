import { Controller, Get, Post, Body, Patch, Param, Delete, Query, ParseUUIDPipe, Request, BadRequestException } from '@nestjs/common';
import { SupportMessagesService } from './support-messages.service';
import { CreateSupportMessageDto } from './dto/create-support-message.dto';
import { UpdateSupportMessageDto } from './dto/update-support-message.dto';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SupportMessage } from '@entities/support-message.entity';
import { FindSupportMessageDto } from './dto/find-support-message.dto';
import { Permission } from '@enums/permission.enum';
import express from 'express';
import { Public } from 'src/auth/public.decorator';
import { ESupportMessageStatus } from '@lib/api/types';

@Controller('support-messages')
@ApiTags('support-messages')
@ApiBearerAuth()
export class SupportMessagesController {
  constructor(private readonly supportMessagesService: SupportMessagesService) {}

  // create

  @Post()
  @ApiOperation({ summary: 'Creates support message' })
  @ApiResponse({
    status: 201,
    description: 'Created support message',
    type: SupportMessage,
  })
  @Public()
  create(@Request() req: express.Request, @Body() dto: CreateSupportMessageDto) {
    const ip = <string>req.headers['x-forwarded-for'] || req.socket.remoteAddress;
    const isManager = req.user && req.user.hasPermission(Permission.SUPPORT_MESSAGES_MANAGE);
    // Ignore dto.userId from non-managers
    const userId = req.user ? (isManager ? dto.userId || req.user.id : req.user.id) : null;
    if (!userId && !dto.email) {
      throw new BadRequestException('userId or email required');
    }
    // Ignore status from non-managers
    const status = isManager ? dto.status : ESupportMessageStatus.NEW;

    return this.supportMessagesService.create({ ...dto, userId, ip, status });
  }

  // retrieve

  @Get()
  @ApiOperation({ summary: 'Finds support messages' })
  @ApiResponse({
    status: 201,
    description: 'Found support messages',
    type: [SupportMessage],
  })
  find(@Request() req: express.Request, @Query() query: FindSupportMessageDto) {
    // Ignore query.userId for non-managers
    const userId = req.user.hasPermission(Permission.SUPPORT_MESSAGES_VIEW) ? query.userId : req.user.id;
    return this.supportMessagesService.find({ ...query, userId });
  }

  // update
  // TODO check permissions

  @Patch(':supportMessageId')
  @ApiOperation({ summary: 'Updates support message' })
  @ApiResponse({
    status: 201,
    description: 'Updated support message',
    type: SupportMessage,
  })
  update(@Param('supportMessageId', new ParseUUIDPipe()) supportMessageId: string, @Body() dto: UpdateSupportMessageDto) {
    return this.supportMessagesService.update(supportMessageId, dto);
  }

  // remove
  // TODO check permissions

  @Delete(':supportMessageId')
  @ApiOperation({ summary: 'Deletes support message' })
  @ApiResponse({
    status: 201,
    description: 'Deleted support message',
    type: SupportMessage,
  })
  remove(@Param('supportMessageId') supportMessageId: string) {
    return this.supportMessagesService.remove(supportMessageId);
  }

  @Get('topics')
  @ApiOperation({ summary: 'Retrieves support message topics' })
  @ApiResponse({
    status: 201,
    description: 'Support message topics',
    type: [String],
  })
  @Public()
  findTopics() {
    // TODO Move to enum, translate
    return ['Student Support', 'Financial', 'Problems in the educational platform', 'Tutors', 'Complaints department', 'Other'];
  }
}
