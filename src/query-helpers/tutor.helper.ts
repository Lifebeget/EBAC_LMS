import { User } from '@entities/user.entity';
import { SystemRole } from '@lib/types/enums';
import { getManager } from 'typeorm';

export const getTutorsQB = () => {
  return getManager()
    .createQueryBuilder(User, 'user')
    .distinct()
    .innerJoin('user.roles', 'role')
    .where('role.role = :role', { role: SystemRole.Tutor })
    .andWhere(
      `:currentDate between coalesce(date_from, '1970-01-01') and
               coalesce(date_to, '9999-12-31')`,
      { currentDate: new Date() },
    );
};

export const getTutors = (select = 'user.id, user.name') => {
  const qb = getTutorsQB().select(select).orderBy('user.name');

  return qb.getRawMany();
};
