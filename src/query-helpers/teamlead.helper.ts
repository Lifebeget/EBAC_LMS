import { getManager } from 'typeorm';
import { Enrollment } from '@entities/enrollment.entity';
import { User } from '@entities/user.entity';
import { SelectQueryBuilder } from 'typeorm';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Course } from '@entities/course.entity';

const MIN_DATE = '1970-01-01';
const MAX_DATE = '9999-12-31';

export const addTeamleadFilterForSubmissions = <EntityType>(
  qb: SelectQueryBuilder<EntityType>,
  teamleadId: string,
  date: Date | string,
) => {
  addTeamleadFilter(qb, teamleadId, date, 'submission.course_id = teamlead_enrollment.course_id');
};

export const addTeamleadFilter = <EntityType>(
  qb: SelectQueryBuilder<EntityType>,
  teamleadId: string,
  date: Date | string,
  joinCondition: string,
) => {
  qb.innerJoin(Enrollment, 'teamlead_enrollment', joinCondition)
    .andWhere('teamlead_enrollment.active = :active', { active: true })
    .andWhere('teamlead_enrollment.role = :role', { role: CourseRole.Teamlead })
    .andWhere('teamlead_enrollment.user_id = :teamleadId', {
      teamleadId: teamleadId,
    })
    .andWhere(':date between coalesce(teamlead_enrollment.date_from, :min) and coalesce(teamlead_enrollment.date_to, :max)', {
      date,
      min: MIN_DATE,
      max: MAX_DATE,
    });
};

export const getteamlead_enrollmentsQB = (teamleadId: string, date: Date | string) => {
  return getManager()
    .createQueryBuilder(Enrollment, 'teamlead_enrollment')
    .where('teamlead_enrollment.user_id = :teamleadId', { teamleadId })
    .andWhere('teamlead_enrollment.active = :active', { active: true })
    .andWhere('teamlead_enrollment.role = :teamlead_role', {
      teamlead_role: CourseRole.Teamlead,
    })
    .andWhere(':date between coalesce(teamlead_enrollment.date_from, :min) and coalesce(teamlead_enrollment.date_to, :max)', {
      date,
      min: MIN_DATE,
      max: MAX_DATE,
    });
};

export const getTeamleadTutorsQB = (teamleadId: string, date: Date | string) => {
  const qb = getteamlead_enrollmentsQB(teamleadId, date)
    .innerJoin(Enrollment, 'tutorEnrollment', 'tutorEnrollment.course_id = teamlead_enrollment.course_id')
    .innerJoin(User, 'user', 'tutorEnrollment.user_id = user.id')
    .andWhere('tutorEnrollment.role = :tutorRole', {
      tutorRole: CourseRole.Tutor,
    });
  return qb;
};

export const getTeamleadTutorIds = async (teamleadId: string, date: Date | string) => {
  if (!teamleadId || teamleadId == '0') {
    return [];
  }

  const qb = getTeamleadTutorsQB(teamleadId, date);

  qb.select('distinct user.id as id');

  const tutors = await qb.getRawMany();
  return tutors.map(t => t.id);
};

export const getTeamleadTutors = async (teamleadId: string, date: Date | string) => {
  if (!teamleadId || teamleadId == '0') {
    return [];
  }

  const qb = getTeamleadTutorsQB(teamleadId, date);

  qb.select('distinct user.id, user.name').orderBy('user.name');

  const tutors = await qb.getRawMany();
  return tutors;
};

export const getTeamleadCourseIds = async (teamleadId: string, date: Date | string) => {
  if (!teamleadId || teamleadId == '0') {
    return [];
  }

  const em = getManager();

  const qb = em
    .createQueryBuilder(Course, 'c')
    .select('c.id as id')
    .innerJoin(Enrollment, 'teamlead_enrollment', 'teamlead_enrollment.course_id = c.id')
    .where('teamlead_enrollment.user_id = :teamleadId', { teamleadId })
    .andWhere('teamlead_enrollment.active = :active', { active: true })
    .andWhere('teamlead_enrollment.role = :teamlead_role', {
      teamlead_role: CourseRole.Teamlead,
    })
    .andWhere(':date between coalesce(teamlead_enrollment.date_from, :min) and coalesce(teamlead_enrollment.date_to, :max)', {
      date,
      min: MIN_DATE,
      max: MAX_DATE,
    });

  const courses = await qb.getRawMany();
  return courses.map((c: Course) => c.id);
};
