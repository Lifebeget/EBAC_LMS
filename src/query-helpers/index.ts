import { Enrollment } from '@entities/enrollment.entity';
import { MAX_DATE } from '@lib/helpers/date';
import { SelectQueryBuilder } from 'typeorm';
import { SystemRole } from '@lib/types/enums/system-role.enum';

export const dateOnly = (fieldName: string) => `date_trunc('day', ${fieldName})`;

export const filterByYearAndMonth = <T>(queryBuilder: SelectQueryBuilder<T>, fieldName: string, year?: number, month?: number) => {
  if (!year || !month) {
    return;
  }

  queryBuilder.andWhere(`extract(year from ${fieldName}) = :year`, { year });
  queryBuilder.andWhere(`extract(month from ${fieldName}) = :month`, { month });
};

export const filterByUserCourses = <T>(
  queryBuilder: SelectQueryBuilder<T>,
  userId: string,
  role: SystemRole,
  alias = 's',
  dateField = 'submitted_at',
  tutorField = 'tutor_id',
) => {
  if (![SystemRole.Tutor, SystemRole.Teamlead].includes(role)) {
    role = SystemRole.Tutor;
  }

  if (role == SystemRole.Tutor) {
    queryBuilder.andWhere(`${tutorField} = :userId`, { userId });
    return;
  }

  queryBuilder.andWhere(
    qb =>
      `
      ${alias}.course_id::uuid in ${qb
        .subQuery()
        .select('course_id')
        .from(Enrollment, 'e')
        .andWhere('e.user_id = :userId', { userId })
        .andWhere(`e.role = '${role}'`)
        .andWhere(
          ` ${alias}.${dateField} between
            coalesce(e.date_from, e.created) and
            coalesce(e.date_to, :maxDate)`,
          { maxDate: MAX_DATE },
        )
        .andWhere('active = true')
        .getQuery()}
        
    `,
  );
};

export const courseTitle = (alias = 'c') => `
  case 
    when ${alias}.version = 1 then ${alias}.title 
    else ${alias}.title||' v.'||${alias}.version 
  end`;
