import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { AttendanceType, SystemRole } from '@lib/api/types';

@Injectable()
export class TutorAttendanceStatService {
  parseIntOrNull(num: string | null): number | null {
    return num === null ? null : parseInt(num, 10);
  }

  async attendance(asRole = null, userId = null): Promise<AttendanceType[]> {
    const isAsTeamlead = asRole === SystemRole.Teamlead && userId;
    const teamleadInnerJoin = isAsTeamlead
      ? `INNER JOIN enrollment ON c.id = enrollment.course_id AND enrollment.role = 'teamlead' AND enrollment.user_id = '${userId}'
        AND enrollment.active = TRUE AND (enrollment.date_to > NOW() OR enrollment.date_to is NULL)`
      : '';

    let result = await getConnection().query(`
      SELECT
        c.id AS course_id,
        c.title AS course_name,
        c.version AS course_version,
        s1.ungraded,
        s1.undistributed,
        s2.last_correction,
        course_info.arr_tutors
      FROM course c
      ${teamleadInnerJoin}
      LEFT JOIN (
        SELECT
          count(s.id) AS ungraded,
          count(s.id) FILTER (WHERE s.tutor_id IS null) AS undistributed,
          s.course_id
        FROM submission s
        INNER JOIN assessment a ON a.id = s.assessment_id AND a.type = 'default'
        WHERE s.status = 'submitted' and s.deleted is NULL
        GROUP BY s.course_id
      ) s1 ON s1.course_id = c.id
      LEFT JOIN (
        SELECT
          MAX(s.graded_at) AS last_correction,
          s.course_id
        FROM submission s
        INNER JOIN assessment a ON a.id = s.assessment_id AND a.type = 'default'
        WHERE s.status = 'graded' and s.deleted IS NULL
        GROUP BY s.course_id
      ) s2 ON s2.course_id = c.id
      LEFT JOIN (
        SELECT
          json_agg(tutor) AS arr_tutors,
          course_id
        FROM (
          SELECT
            json_build_object(
              'name', tutor_name,
              'id', tutors.tutor_id,
              'partial', MAX(partial),
              'pause', pause,
              'teamlead', role.role,
              'last_correction', s3.last_correction
            ) AS tutor,
            course_id
            FROM (
              (
                SELECT
                  t.name AS tutor_name,
                  t.id AS tutor_id,
                  c.id AS course_id,
                  0 AS partial,
                  t.pause AS pause
                FROM "user" t
                INNER JOIN enrollment e ON e.user_id = t.id AND e.role = 'tutor' AND e.active = TRUE
                LEFT JOIN course c ON c.id = e.course_id
                WHERE t.active = TRUE
              ) UNION (
                SELECT
                  t.name AS tutor_name,
                  t.id AS tutor_id,
                  c.id AS course_id,
                  1 AS partial,
                  t.pause AS pause
                FROM "user" t
                INNER JOIN "module_assignment" ma ON ma.user_id = t.id
                LEFT JOIN "module" m ON m.id = ma.module_id
                LEFT JOIN course c ON c.id = m.course_id
                WHERE t.active = TRUE
              ) UNION (
                SELECT
                  t.name AS tutor_name,
                  t.id AS tutor_id,
                  c.id AS course_id,
                  2 AS partial,
                  t.pause AS pause
                FROM "user" t
                INNER JOIN "module_assignment" ma ON ma.user_id = t.id
                LEFT JOIN "module" m ON m.id = ma.module_id
                LEFT JOIN course c ON c.id = m.course_id
                WHERE t.active = TRUE and ma.exclusive = true
              ) UNION (
                SELECT
                  t.name AS tutor_name,
                  t.id AS tutor_id,
                  c.id AS course_id,
                  1 AS partial,
                  t.pause AS pause
                FROM "user" t
                INNER JOIN "section_assignment" sa ON sa.user_id = t.id
                LEFT JOIN "section" s ON s.id = sa.section_id
                LEFT JOIN course c ON c.id = s.course_id
                WHERE t.active = TRUE
              ) UNION (
                SELECT
                  t.name AS tutor_name,
                  t.id AS tutor_id,
                  c.id AS course_id,
                  2 AS partial,
                  t.pause AS pause
                FROM "user" t
                INNER JOIN "section_assignment" sa ON sa.user_id = t.id
                LEFT JOIN "section" s ON s.id = sa.section_id
                LEFT JOIN course c ON c.id = s.course_id
                WHERE t.active = TRUE and sa.exclusive = true
                GROUP BY t.name, t.id, c.id
              )
            ) AS tutors
            LEFT JOIN role ON role.user_id = tutors.tutor_id AND role.role = 'teamlead'
            LEFT JOIN (
              SELECT
                s.tutor_id,
                MAX(s.graded_at) AS last_correction
              FROM submission s
              LEFT JOIN assessment a ON a.id = s.assessment_id AND a.type = 'default'
              WHERE s.status = 'graded'
              GROUP BY s.tutor_id
            ) AS s3 ON s3.tutor_id = tutors.tutor_id
            GROUP BY tutor_name, tutors.tutor_id, course_id, pause, role.role, s3.last_correction
          ) AS tutor_info GROUP BY course_id
        ) AS course_info ON course_info.course_id = c.id;
    `);

    if (isAsTeamlead) {
      result = result.reduce((acc, el) => {
        if (acc.every(entry => entry.course_id !== el.course_id)) {
          acc.push(el);
        }
        return acc;
      }, []);
    }

    return result.map(course => {
      const courseId = course.course_id;
      const courseName = course.course_name;
      const courseVersion = course.course_version;
      const activeTutorsCount = course?.arr_tutors?.filter(t => !t.pause).length ?? 0;
      const tutorsCount = course?.arr_tutors?.length ?? 0;
      const ungraded = this.parseIntOrNull(course.ungraded) ?? 0;
      const undistributed = this.parseIntOrNull(course.undistributed);
      const lastCorrection = course.last_correction ?? null;

      const tutors =
        course.arr_tutors?.map(tutor => ({
          tutorId: tutor.id,
          tutorName: tutor.name,
          teamlead: tutor.teamlead,
          pause: tutor.pause,
          subscribedToPartNotExclusive: tutor.partial === 1,
          subscribedToPartExclusive: tutor.partial === 2,
          lastCorrection: tutor.last_correction,
        })) || [];

      return {
        courseId,
        courseName,
        courseVersion,
        activeTutorsCount,
        tutorsCount,
        ungraded,
        undistributed,
        lastCorrection,
        tutors,
      };
    });
  }
}
