import { Injectable, BadRequestException } from '@nestjs/common';
import { getConnection } from 'typeorm';
import * as dayjs from 'dayjs';
import { QueryByCoursesGraduatesDto } from './dto/query-courses.dto';

const validateUUID = (str: string) => {
  const reg = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
  if (!reg.test(str)) {
    throw new BadRequestException(`${str} is not a valid uuid`);
  }
};

const validateArray = (validOptions: string[], status?: string[]) => {
  if (status == null) return;
  for (const item of status) {
    const isValidItem = validOptions.includes(item);
    if (!isValidItem) {
      throw new BadRequestException(`${item} is not a valid option`);
    }
  }
};

const parseDate = (str?: string, defaultDate?: string | Date, endOf?: dayjs.OpUnitType) => {
  let d = dayjs(str);
  if (!(str && d) && defaultDate) {
    d = dayjs(defaultDate);
  }
  if (endOf) {
    d = d.endOf(endOf);
  }
  return d.toDate();
};

@Injectable()
export class ProgressStatService {
  parseIntOrNull(num: string | null): number | null {
    return num === null ? null : parseInt(num, 10);
  }

  async progressStat(coursesIds: string[], tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const result = await getConnection().query(`
      select
        v.course_id,
        count(v.user_id) as count,
        v.bracket
      from (
        select
          v.course_id,
          v.user_id,
          ceil(v.progress / 10.0) as bracket
        from view_user_progress_count_materialized v
        left join enrollment e on e.course_id = v.course_id and e.user_id = v.user_id
        where v.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and v.user_id in (
              select user_id
              from user_tags as u
              where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and (e.date_from is null or e.date_from < now())
          and (e.date_to is null or e.date_to > now())
          and e.role = 'student'
          and e.active = true
          group by v.course_id, v.user_id, ceil(v.progress / 10.0)
      ) as v
      group by v.course_id, v.bracket
    `);
    let extra = await getConnection().query(`
      select
        course_id,
        count(1) as students,
        count(1) filter (where last_active >= now() - interval '30 days') as active_students
      from (
        select distinct
          e.user_id,
          e.course_id,
          ua.modified as last_active
        from enrollment e
        left join (
            select lp.user_id, l.course_id, max(lp.modified) as modified
            from lecture_progress lp
            inner join lecture l on l.id = lp.lecture_id
            where lp.deleted is null
            group by lp.user_id, l.course_id
        ) ua on ua.user_id = e.user_id and ua.course_id = e.course_id
        where e.role = 'student'
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.active = true
          and (e.date_from is null or e.date_from < now())
          and (e.date_to is null or e.date_to > now())
      ) as enr
      group by course_id;
    `);
    if (coursesIds) {
      extra = extra.filter(e => coursesIds.includes(e.course_id));
    }
    return result.concat(
      extra.map(e => ({
        course_id: e.course_id,
        bracket: 'all',
        count: e.students,
      })),
      extra.map(e => ({
        course_id: e.course_id,
        bracket: 'active',
        count: e.active_students,
      })),
    );
  }

  async noProgressStat(coursesIds: string[], tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const result = await getConnection().query(`
      select
        s.course_id,
              date_part('month', s.enrollment_date::date) AS month,
              date_part('year', s.enrollment_date::date) AS year,
              count(*) as count
      from (
        select ((s.lectures_pct + s.assessments_pct) / 2 * 100) as progress, s.*
        from (
          select
            e.id,
            e.created as enrollment_date,
            e.course_id,
            e.user_id,
            t.total_lectures,
            t.total_assessments,
            coalesce(ls.completed_lectures_per_user::numeric, 0) completed_lectures_per_user,
            coalesce(ss.total_assestment_score_per_user::numeric, 0) total_assestment_score_per_user,
            coalesce(ls.completed_lectures_per_user::numeric, 0) / greatest(coalesce(t.total_lectures::numeric, 0), 1) as lectures_pct,
              coalesce(ss.total_assestment_score_per_user::numeric, 0) / greatest(coalesce(t.total_assessments::numeric, 0), 1) / 100 as assessments_pct
          from enrollment e
          left join (
            select l.course_id, lp.user_id, count (*) as completed_lectures_per_user
            from lecture l
            inner join lecture_progress lp on lp.lecture_id = l.id
            where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
              and l.required and l.active
              and lp.status = 'success'
              and lp.deleted is null
            group by l.course_id, lp.user_id
          ) as ls on ls.course_id = e.course_id and ls.user_id = e.user_id
          left join (
            select s.course_id, s.user_id, sum(s.assessment_score) as total_assestment_score_per_user
            from (
              select l.course_id, s.user_id, max(s.score) as assessment_score
              from lecture l
              inner join assessment a on a.lecture_id = l.id
              inner join submission s on s.assessment_id = a.id
              where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
                and l.required and l.active
                and (a.type = 'default' or a.type = 'quiz')
              group by l.course_id, a.id, s.user_id
            ) as s
            group by s.course_id, s.user_id
          ) as ss on ss.course_id = e.course_id and ss.user_id = e.user_id
          left join (
            select
              l.course_id,
              count(distinct l.id) filter (where l.id IS NOT NULL) as total_lectures,
              count(distinct a.id) filter (where a.id IS NOT NULL) as total_assessments
            from lecture as l
            left join assessment as a on a.lecture_id = l.id and (a.type = 'default' or a.type = 'quiz')
            where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
              and l.active and l.required
            group by l.course_id
          ) as t on t.course_id = e.course_id
          where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
        ) as s
      ) as s
      where s.progress <= 1
      group by s.course_id,
        date_part('year', s.enrollment_date::date),
        date_part('month', s.enrollment_date::date)
      order by s.course_id`);

    return result;
  }

  async graduatesStat(coursesIds: string[], tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const result = await getConnection().query(`
      with submissions_by_user as (
                select user_id, lecture_id, min(submitted_at) as submitted_at
                from submission
                where lecture_id is not null
                   and user_id is not null
                   and submitted_at is not null
                   and deleted is null
                group by user_id, lecture_id
            ),
            lectures_with_assessments as (
                select lecture_id
                from view_assessment
                group by lecture_id
            ),
            lecture_progress_extended as (
                select lp.*,
                  vl.required,
                  vl.course_id,
                  va.lecture_id is not null as has_assessment,
                  vs.submitted_at
                from lecture_progress lp
                left join view_lecture vl on vl.id = lp.lecture_id
                left join lectures_with_assessments va on va.lecture_id = lp.lecture_id
                left join submissions_by_user vs
                          on vs.lecture_id = lp.lecture_id and vs.user_id = lp.user_id
                where vl.course_id is not null
            ),
            course_count as (
                select vl.course_id,
                   count(1) filter (where va.lecture_id is not null) as exercises_total,
                   count(1) filter (where va.lecture_id is null) as lessons_total
                from view_lecture vl
                left join lectures_with_assessments va on va.lecture_id = vl.id
                where vl.required
                group by vl.course_id
            ),
            user_course_stat as (
              select lp2.user_id,
                 lp2.course_id,
                 count(1) filter (
                   where required and not has_assessment and status = 'success'
                   ) as lessons_success,
                 count(1) filter (
                   where required and has_assessment and status = 'success'
                   ) as exercises_success,
                 count(1) filter (
                   where required
                     and has_assessment and status <> 'success'
                     and submitted_at is not null
                   ) as exercises_not_success,
                 max(ended) filter (
                   where required and not has_assessment and status = 'success'
                   ) as dt_lessons_ended,
                 max(submitted_at) filter (
                   where required and has_assessment and status <> 'success'
                   ) as dt_exercises_submitted,
                 vcc.exercises_total,
                 vcc.lessons_total
              from lecture_progress_extended lp2
                     left join course_count vcc on vcc.course_id = lp2.course_id
              where lp2.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
                ${
                  tagIds && tagIds.length > 0
                    ? `and lp2.user_id in (
                        select user_id
                        from user_tags as u
                        where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
                    : ''
                }
              group by lp2.user_id, lp2.course_id, vcc.lessons_total, vcc.exercises_total
            ),
            student_enrollments as (
                select user_id, course_id, min(graduated_at) as graduated_at, min(created) as created
                 from enrollment
                 where role = 'student'
                   and active
                 group by user_id, course_id
            ),
            user_course_stat2 as (
                select stat.user_id,
                  stat.course_id,
                  case
                    when e.graduated_at is not null
                      then e.graduated_at
                    when lessons_success = lessons_total and
                         exercises_success + exercises_not_success = exercises_total
                      then dt_exercises_submitted
                    when lessons_success = lessons_total
                      then dt_lessons_ended
                    end as dt,
                  e.created as enrolled_at,
                  e.graduated_at is not null as graduated,
                  lessons_success = lessons_total
                    and exercises_success + exercises_not_success =
                        exercises_total as submitted_all,
                  lessons_success = lessons_total as viewed_all
                from user_course_stat stat
                left join student_enrollments e
                   on e.user_id = stat.user_id and e.course_id = stat.course_id
                where e.created is not null
            )
            select course_id,
               date_part('month', dt) as month,
               date_part('year', dt) as year,
               count(1) filter ( where graduated ) as graduated,
               count(1) filter ( where submitted_all and not graduated) as submitted,
               count(1) filter ( where viewed_all and not submitted_all and not graduated ) as viewed
            from user_course_stat2
            where dt is not null
            group by course_id,
               date_part('month', dt),
               date_part('year', dt)
           `);

    return result;
  }

  async withSubmissionsStat(coursesIds: string[], tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const result = await getConnection().query(`
      select e.course_id,
        count(distinct s.user_id) as count,
        date_part('year', e.created::date) as year,
          date_part('month', e.created::date) as month
      from enrollment e
      inner join (
        select s.course_id, s.user_id from lecture l
        inner join assessment a on a.lecture_id = l.id
        inner join submission s on s.assessment_id = a.id
        where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          and l.active
          and l.required
          and (a.type = 'default' or a.type = 'quiz')
        group by s.course_id , s.user_id
      ) as s on s.course_id = e.course_id and s.user_id = e.user_id
      where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
      ${
        tagIds && tagIds.length > 0
          ? `and e.user_id in (
        select user_id
        from user_tags as u
        where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
          : ''
      }
      and e.role = 'student'
      and e.active
      group by e.course_id,
        date_part('year', e.created::date),
          date_part('month', e.created::date)
      order by e.course_id,
        date_part('year', e.created::date),
        date_part('month', e.created::date)
      `);

    return result;
  }

  async homeworkLectureViews(coursesIds: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const result = await getConnection().query(`
      select
        e.course_id,
        e.user_id,
        coalesce(ss.assessment_count, 0) assessment_count,
        coalesce (ls.lecture_count, 0) lecture_count
      from enrollment e
      left join (
        select s.course_id, s.user_id, count(distinct s.assessment_id) assessment_count
        from (
          select l.course_id, a.id assessment_id, s.user_id
          from lecture l
          inner join assessment a on a.lecture_id = l.id
          inner join submission s on s.assessment_id = a.id
          where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
            and l.required and l.active
            and (a.type = 'default' or a.type = 'quiz')
          group by l.course_id, a.id, s.user_id
        ) s
        group by s.course_id, s.user_id
      ) as ss on ss.course_id = e.course_id and ss.user_id = e.user_id
      left join (
        select l.course_id, lp.user_id, count (lp.lecture_id) as lecture_count
        from lecture l
        inner join lecture_progress lp on lp.lecture_id = l.id
        where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          and l.required and l.active
          and lp.status = 'success'
          and lp.deleted is null
        group by l.course_id, lp.user_id
      ) as ls on ls.course_id = e.course_id and ls.user_id = e.user_id
      where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
        and e.role = 'student'
        and e.active
      `);

    return result.map(({ course_id, user_id, assessment_count, lecture_count }) => {
      return {
        course_id,
        user_id,
        assessment_count: Number.parseInt(assessment_count),
        lecture_count: Number.parseInt(lecture_count),
      };
    });
  }

  async homeworksSpreadPerModule(coursesIds: string[], minDate?: string, maxDate?: string, tagIds?: string[], activeState?: string) {
    coursesIds.forEach(id => validateUUID(id));

    const parsedMinDate = minDate && dayjs(minDate) ? dayjs(minDate).toDate() : dayjs('2000-01-01').toDate();
    const now = new Date();
    const parsedMaxDate = parseDate(maxDate, now, 'day');

    const result = await getConnection().query(
      `
      select
        r.course_id,
        r.module_id,
        min(r.module_number) as module_number,
        min(r.module_custom_number) as module_custom_number,
        r.submission_status,
        count (*) as count,
        r.mudule_active as mudule_active,
        r.module_sort as module_sort
      from (
        select
          e.course_id,
          m.id as module_id,
          a.id as assessment_id,
          e.user_id,
          m.title as module_title,
          m."number" as module_number,
          m.custom_number as module_custom_number,
          m.active as mudule_active,
          m.sort as module_sort,
          a.passing_score as passing_score,
          case when s.score >= a.passing_score then 'success'
            when s.score < a.passing_score then 'failure'
            when s.status = 'submitted' then 'submitted'
            else null end as submission_status
        from enrollment e
        inner join "module" m on m.course_id = e.course_id
        inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
        inner join assessment a on a.lecture_id = l.id
        inner join submission s on s.assessment_id = a.id and s.user_id = e.user_id
        where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
          ${
            activeState === 'active'
              ? `
            and l.active = true
            and m.active = true
          `
              : ''
          }
          ${
            activeState === 'inactive'
              ? `
            and (l.active = false or m.active = false)
          `
              : ''
          }
          and (a.type = 'default' or a.type = 'quiz')
          and (s.status = 'submitted' or s.score >= a.passing_score or s.score < a.passing_score)
          and s.submitted_at > $1
          and s.submitted_at <= $2
      ) as r
      group by r.course_id, r.module_id, r.submission_status, r.mudule_active, r.module_sort
      order by r.module_sort ASC
      `,
      [parsedMinDate, parsedMaxDate],
    );

    return result.map(({ count, course_id, module_id, module_number, module_custom_number, submission_status, mudule_active }) => {
      return {
        course_id,
        module_id,
        module_number: module_custom_number || module_number,
        mudule_active,
        submission_status,
        count: Number.parseInt(count),
      };
    });
  }

  async homeworksUsersPerModule(coursesIds: string[], minDate?: string, maxDate?: string, tagIds?: string[], activeState?: string) {
    coursesIds.forEach(id => validateUUID(id));

    const parsedMinDate = minDate && dayjs(minDate) ? dayjs(minDate).toDate() : dayjs('2000-01-01').toDate();
    const now = new Date();
    const parsedMaxDate = parseDate(maxDate, now, 'day');

    const result = await getConnection().query(
      `
      select
        r.course_id,
        r.module_id,
        min(r.module_number) as module_number,
        min(r.module_custom_number) as module_custom_number,
        r.submission_status,
        count (distinct r.user_id) as count,
        r.mudule_active as mudule_active,
        r.module_sort as module_sort
      from (
        select
          e.course_id,
          m.id as module_id,
          a.id as assessment_id,
          e.user_id,
          m.title as module_title,
          m."number" as module_number,
          m.custom_number as module_custom_number,
          m.active as mudule_active,
          m.sort as module_sort,
          a.passing_score as passing_score,
          case when s.score >= a.passing_score then 'success'
            when s.score < a.passing_score then 'failure'
            when s.status = 'submitted' then 'submitted'
            else null end as submission_status
        from enrollment e
        inner join "module" m on m.course_id = e.course_id
        inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
        inner join assessment a on a.lecture_id = l.id
        inner join submission s on s.assessment_id = a.id and s.user_id = e.user_id
        where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
          ${
            activeState === 'active'
              ? `
            and l.active = true
            and m.active = true
          `
              : ''
          }
          ${
            activeState === 'inactive'
              ? `
            and (l.active = false or m.active = false)
          `
              : ''
          }
          and (a.type = 'default' or a.type = 'quiz')
          and (s.status = 'submitted' or s.score >= a.passing_score or s.score < a.passing_score)
          and s.current_attempt
          and s.submitted_at > $1
          and s.submitted_at <= $2
      ) as r
      group by r.course_id, r.module_id, r.submission_status, r.mudule_active, r.module_sort
      order by r.module_sort ASC
      `,
      [parsedMinDate, parsedMaxDate],
    );

    return result.map(({ count, course_id, module_id, module_number, module_custom_number, submission_status, mudule_active }) => {
      return {
        course_id,
        module_id,
        module_number: module_custom_number || module_number,
        mudule_active,
        submission_status,
        count: Number.parseInt(count),
      };
    });
  }

  async homeworksViewsTotals() {
    const result = await getConnection().query(
      `select
        e.assessment_pct,
        e.lecture_pct,
        count(*) as count
      from (
        select
          e.course_id,
          e.user_id,
          round(e.assessment_count::numeric / greatest (e.total_assessment_count::numeric , 1) * 20) * 5 as assessment_pct,
          round(e.lecture_count::numeric / greatest (e.total_lectures_count::numeric , 1) * 20) * 5 as lecture_pct,
          e.assessment_count,
          e.lecture_count,
          e.total_assessment_count,
          e.total_lectures_count
        from (
          select
            e.course_id,
            e.user_id,
            coalesce(ss.assessment_count, 0) assessment_count,
            coalesce (ls.lecture_count, 0) lecture_count,
            l.total_lectures_count,
            a.total_assessment_count
          from enrollment e
          left join (
            select s.course_id, s.user_id, count(distinct s.assessment_id) assessment_count
            from (
              select l.course_id, a.id assessment_id, s.user_id
              from lecture l
              inner join assessment a on a.lecture_id = l.id
              inner join submission s on s.assessment_id = a.id
              where l.required and l.active
                and (a.type = 'default' or a.type = 'quiz')
              group by l.course_id, a.id, s.user_id
            ) s
            group by s.course_id, s.user_id
          ) as ss on ss.course_id = e.course_id and ss.user_id = e.user_id
          left join (
            select l.course_id, lp.user_id, count (lp.lecture_id) as lecture_count
            from lecture l
            inner join lecture_progress lp on lp.lecture_id = l.id
            where l.required and l.active
              and lp.status = 'success'
              and lp.deleted is null
            group by l.course_id, lp.user_id
          ) as ls on ls.course_id = e.course_id and ls.user_id = e.user_id
          inner join (
            select
              l.course_id,
              count(*) as total_lectures_count
            from lecture l
            where l.active and l.required
            group by l.course_id
          ) l on l.course_id = e.course_id
          inner join (
            select
              l.course_id,
              count(*) as total_assessment_count
            from assessment a
            inner join lecture l on l.id = a.lecture_id
            where
              l.active and l.required
              and (a.type = 'default' or a.type = 'quiz')
            group by l.course_id
          ) as a on a.course_id = e.course_id
          where e.role = 'student'
          and e.active
        ) as e
      ) as e
      group by e.assessment_pct, e.lecture_pct
    `,
    );

    return result.map(({ assessment_pct, lecture_pct, count }) => {
      return {
        assessment_pct: Number.parseInt(assessment_pct),
        lecture_pct: Number.parseInt(lecture_pct),
        count: Number.parseInt(count),
      };
    });
  }

  async evaluationsByCourseLectures(coursesIds: string[], minDate?: string, maxDate?: string, tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const parsedMinDate = minDate && dayjs(minDate) ? dayjs(minDate).toDate() : dayjs('2000-01-01').toDate();
    const now = new Date();
    const parsedMaxDate = parseDate(maxDate, now, 'day');

    const result = await getConnection().query(
      `
      select
        l.course_id,
        l.id as lecture_id,
        le.evaluation,
        count(*) as count,
        min(m."number"::numeric) as module_number,
        min(l."number"::numeric) as lecture_number
      from lecture l
      inner join lecture_evaluation le on le.lecture_id = l.id
      inner join "module" m on m.id = l.module_id
      where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
        ${
          tagIds && tagIds.length > 0
            ? `and le.user_id in (
          select user_id
          from user_tags as u
          where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
            : ''
        }
        and l.active
        and le.created > $1
        and le.created <= $2
      group by l.course_id, l.id, le.evaluation
      order by l.course_id, min(m."number"::numeric), min(l."number"::numeric)
      `,
      [parsedMinDate, parsedMaxDate],
    );

    return result.map(({ count, course_id, lecture_id, evaluation, module_number, lecture_number }) => {
      return {
        course_id,
        lecture_id,
        count: Number.parseInt(count),
        evaluation: Number.parseInt(evaluation),
        module_number: Number.parseInt(module_number),
        lecture_number: Number.parseInt(lecture_number),
      };
    });
  }

  async coursesSubmissionSuccessRate(minDate?: string, maxDate?: string) {
    const parsedMinDate = parseDate(minDate, new Date('2000-01-01'), 'day');
    const parsedMaxDate = parseDate(maxDate, new Date(), 'day');

    const result = await getConnection().query(
      `
      select
        s.*,
        c.title,
        c."version"
      from (
        select
          s.course_id,
          s.module_number,
          count (1) filter (where status = 'success') as success_count,
          count (1) filter (where status = 'failure') as failure_count,
          count (1) as total_count
        from (
          select
          c.id course_id,
          m."number" as module_number,
          case when s.score >= a.passing_score then 'success'
                when s.score < a.passing_score then 'failure'
                when s.status = 'submitted' then 'submitted'
                else null end as status
          from course c
          inner join "module" m on m.course_id = c.id
          inner join lecture l on l.module_id = m.id
          inner join assessment a on a.lecture_id = l.id
          inner join submission s on s.assessment_id = a.id
          where c.active
            and l.active
            and (a.type = 'default' or a.type = 'quiz')
            and s.submitted_at > $1
            and s.submitted_at <= $2
        ) s
        where s.status = 'success' or s.status  = 'failure'
        group by s.course_id, s.module_number
      ) as s
      inner join course c on s.course_id = c.id
      where s.module_number is not null
      order by c.id, s.module_number
      `,
      [parsedMinDate, parsedMaxDate],
    );

    return result.map(item => {
      const { course_id, failure_count, module_number, success_count, title, total_count, version } = item;
      return {
        course_id,
        module_number,
        title,
        version,
        failure_count: Number.parseInt(failure_count),
        success_count: Number.parseInt(success_count),
        total_count: Number.parseInt(total_count),
      };
    });
  }

  async homeworksUsersByModule(
    courseId: string,
    moduleId: string,
    limit: number,
    offset: number,
    minDate?: string,
    maxDate?: string,
    status?: string[],
    tagIds?: string[],
    sortBy: string = 'name',
    sortOrder: string = 'asc',
  ) {
    validateUUID(courseId);
    validateUUID(moduleId);
    validateArray(['success', 'failure', 'submitted'], status);
    validateArray(['asc', 'desc'], sortOrder ? [sortOrder] : []);
    validateArray(['name', 'submitted_at', 'submission_status'], sortBy ? [sortBy] : []);

    const parsedMinDate = parseDate(minDate, '2000-01-01');
    const now = new Date();
    const parsedMaxDate = parseDate(maxDate, now, 'day');

    const counts = await getConnection().query(
      `select
        count(*) as count
      from (
        select e.id,
          case when s.score >= a.passing_score then 'success'
            when s.score < a.passing_score then 'failure'
            when s.status = 'submitted' then 'submitted'
            else null end as submission_status
        from enrollment e
          inner join "module" m on m.course_id = e.course_id
          inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
          inner join assessment a on a.lecture_id = l.id
          inner join submission s on s.assessment_id = a.id and s.user_id = e.user_id
          inner join "user" u on u.id = s.user_id
        where e.course_id in (${[courseId].map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
          and l.active
          and (a.type = 'default' or a.type = 'quiz')
          and (s.status = 'submitted' or s.score >= a.passing_score or s.score < a.passing_score)
          and s.submitted_at > $1
          and s.submitted_at <= $2
          and m.id = ${`uuid('${moduleId}')`}
      ) e
      ${status && status.length > 0 ? `where e.submission_status in (${status.map(item => `'${item}'`).join(', ')})` : ''}
      `,
      [parsedMinDate, parsedMaxDate],
    );

    const results = await getConnection().query(
      `
      select *
      from (
      select
      s.id,
      u."name",
      u."email",
      e.course_id,
      m.id as module_id,
      a.id as assessment_id,
      m.title as module_title,
      m."number" as module_number,
      coalesce(l.custom_number::text, l."number"::text) as lecture_number,
      a.passing_score as passing_score,
      s.submitted_at as submitted_at,
      e.user_id,
      case when s.score >= a.passing_score then 'success'
        when s.score < a.passing_score then 'failure'
        when s.status = 'submitted' then 'submitted'
        else null end as submission_status
    from enrollment e
    inner join "module" m on m.course_id = e.course_id
    inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
    inner join assessment a on a.lecture_id = l.id
    inner join submission s on s.assessment_id = a.id and s.user_id = e.user_id
    inner join "user" u on u.id = s.user_id
    where e.course_id in (${[courseId].map(id => `uuid('${id}')`).join(', ')})
      ${
        tagIds && tagIds.length > 0
          ? `and e.user_id in (
        select user_id
        from user_tags as u
        where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
          : ''
      }
      and e.role = 'student'
      and e.active
      and l.active
      and (a.type = 'default' or a.type = 'quiz')
      and (s.status = 'submitted' or s.score >= a.passing_score or s.score < a.passing_score)
      and s.submitted_at > $1
      and s.submitted_at <= $4  
      and m.id = ${`uuid('${moduleId}')`}
      ) e
      ${status && status.length > 0 ? `where e.submission_status in (${status.map(item => `'${item}'`).join(', ')})` : ''}
   order by e."${sortBy}" ${sortOrder === 'desc' ? 'DESC' : 'ASC'}
   limit $2 offset $3
      `,
      [parsedMinDate, limit, offset, parsedMaxDate],
    );

    return {
      results,
      meta: {
        limit,
        offset,
        totals: counts.length ? Number.parseInt(counts[0].count) : 0,
      },
    };
  }

  async progressUsersByLecture(
    courseId: string,
    lectureId: string,
    limit: number,
    offset: number,
    _minDate?: string,
    _maxDate?: string,
    status?: string,
    tagIds?: string[],
  ) {
    validateUUID(courseId);
    validateUUID(lectureId);

    const counts = await getConnection().query(
      `select
        count(*) as count
      from (
        select e.id,
          lp.status as submission_status
        from enrollment e
          inner join "module" m on m.course_id = e.course_id
          inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
          inner join lecture_progress as lp on lp.lecture_id = l.id and lp.user_id = e.user_id
          inner join "user" u on u.id = lp.user_id
        where e.course_id in (${[courseId].map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
          and l.active
          and l.id = ${`uuid('${lectureId}')`}
          and lp.deleted is null
      ) e
      ${status ? 'where e.submission_status = $1' : ''}
      `,
      [...(status ? [status] : [])],
    );

    const results = await getConnection().query(
      `
      select *
      from (
      select
      u."name",
      u."email",
      e.course_id,
      m.id as module_id,
      m.title as module_title,
      m."number" as module_number,
      e.user_id,
      lp.status as submission_status,
      lp.progress,
      lp.created,
      lp.modified
    from enrollment e
    inner join "module" m on m.course_id = e.course_id
    inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
    inner join lecture_progress as lp on lp.lecture_id = l.id and lp.user_id = e.user_id
    inner join "user" u on u.id = lp.user_id
    where e.course_id in (${[courseId].map(id => `uuid('${id}')`).join(', ')})
      ${
        tagIds && tagIds.length > 0
          ? `and e.user_id in (
        select user_id
        from user_tags as u
        where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
          : ''
      }
      and e.role = 'student'
      and lp.deleted is null
      and e.active
      and l.active
      and l.id = ${`uuid('${lectureId}')`}
      ) e
      ${status ? 'where e.submission_status = $3' : ''}
   order by e."name"
   limit $1 offset $2
      `,
      [limit, offset, ...(status ? [status] : [])],
    );

    return {
      results,
      meta: {
        limit,
        offset,
        totals: counts.length ? Number.parseInt(counts[0].count) : 0,
      },
    };
  }

  async usersGraduates({ month, courses, tags, type, limit, offset, sortBy, sortOrder }: QueryByCoursesGraduatesDto) {
    courses.forEach(id => validateUUID(id));
    validateArray(['graduated', 'submitted', 'viewed'], type);
    validateArray(['asc', 'desc'], sortOrder ? [sortOrder] : []);
    validateArray(['name', 'course', 'type', 'dt'], sortBy ? [sortBy] : []);

    const sorts = {
      name: '"user"."name"',
      type: '"type"',
      course: '"course"',
      dt: 'ucs.dt',
    };

    const MAX_LIMIT = 1000;

    limit = limit ? Math.min(MAX_LIMIT, Math.max(1, limit)) : null;
    offset = offset ? Math.max(0, offset) : 0;

    const results = await getConnection().query(
      `
      with submissions_by_user as (
                select user_id, lecture_id, min(submitted_at) as submitted_at
                from submission
                where lecture_id is not null
                   and user_id is not null
                   and submitted_at is not null
                   and deleted is null
                group by user_id, lecture_id
            ),
            lectures_with_assessments as (
                select lecture_id
                from view_assessment
                group by lecture_id
            ),
            lecture_progress_extended as (
                select lp.*,
                  vl.required,
                  vl.course_id,
                  va.lecture_id is not null as has_assessment,
                  vs.submitted_at
                from lecture_progress lp
                left join view_lecture vl on vl.id = lp.lecture_id
                left join lectures_with_assessments va on va.lecture_id = lp.lecture_id
                left join submissions_by_user vs
                          on vs.lecture_id = lp.lecture_id and vs.user_id = lp.user_id
                where vl.course_id is not null
            ),
            course_count as (
                select vl.course_id,
                   count(1) filter (where va.lecture_id is not null) as exercises_total,
                   count(1) filter (where va.lecture_id is null) as lessons_total
                from view_lecture vl
                left join lectures_with_assessments va on va.lecture_id = vl.id
                where vl.required
                group by vl.course_id
            ),
            user_course_stat as (
              select lp2.user_id,
                 lp2.course_id,
                 count(1) filter (
                   where required and not has_assessment and status = 'success'
                   ) as lessons_success,
                 count(1) filter (
                   where required and has_assessment and status = 'success'
                   ) as exercises_success,
                 count(1) filter (
                   where required
                     and has_assessment and status <> 'success'
                     and submitted_at is not null
                   ) as exercises_not_success,
                 max(ended) filter (
                   where required and not has_assessment and status = 'success'
                   ) as dt_lessons_ended,
                 max(submitted_at) filter (
                   where required and has_assessment and status <> 'success'
                   ) as dt_exercises_submitted,
                 vcc.exercises_total,
                 vcc.lessons_total
              from lecture_progress_extended lp2
                     left join course_count vcc on vcc.course_id = lp2.course_id
              where lp2.course_id in (${courses.map(id => `uuid('${id}')`).join(', ')})
                ${
                  tags && tags.length > 0
                    ? `and lp2.user_id in (
                          select user_id
                          from user_tags as u
                          where u.tag_id in (${tags.map(id => `uuid('${id}')`).join(', ')}))`
                    : ''
                }
              group by lp2.user_id, lp2.course_id, vcc.lessons_total, vcc.exercises_total
            ),
            student_enrollments as (
                select user_id, course_id, min(graduated_at) as graduated_at, min(created) as created
                 from enrollment
                 where role = 'student'
                   and active
                 group by user_id, course_id
            ),
            user_course_stat2 as (
                select stat.user_id,
                  stat.course_id,
                  case
                    when e.graduated_at is not null
                      then e.graduated_at
                    when lessons_success = lessons_total and
                         exercises_success + exercises_not_success = exercises_total
                      then dt_exercises_submitted
                    when lessons_success = lessons_total
                      then dt_lessons_ended
                    end as dt,
                  e.created as enrolled_at,
                  e.graduated_at is not null as graduated,
                  lessons_success = lessons_total
                    and exercises_success + exercises_not_success =
                        exercises_total as submitted_all,
                  lessons_success = lessons_total as viewed_all
                from user_course_stat stat
                left join student_enrollments e
                   on e.user_id = stat.user_id and e.course_id = stat.course_id
                where e.created is not null
            )
            select ucs.user_id,
                   "user"."name",
                   "course"."title" as course,
                   ucs.course_id,
                   "user"."email",
                   date(ucs.dt) as dt,
                   case
                     when ucs.graduated
                       then 'graduated'
                     when ucs.submitted_all and not ucs.graduated
                       then 'submitted'
                     when ucs.viewed_all and not ucs.submitted_all and not ucs.graduated
                       then 'viewed'
                     end as "type",
                   count(1) OVER() AS total_count
            from user_course_stat2 as ucs
            left join "user" on "user".id = ucs.user_id
            left join "course" on "course".id = ucs.course_id
            where ucs.dt is not null
              ${month ? ` AND TO_CHAR(ucs.dt, 'YYYY-MM') = '${month}' ` : ''}
              ${
                type && type.length > 0
                  ? ` and case
                     when ucs.graduated
                         then 'graduated'
                     when ucs.submitted_all and not ucs.graduated
                         then 'submitted'
                     when ucs.viewed_all and not ucs.submitted_all and not ucs.graduated
                         then 'viewed'
                     end in (${type.map(item => `'${item}'`).join(', ')}) `
                  : ''
              }
            group by ucs.user_id,
                     "user"."name",
                     "course"."title",
                     ucs.course_id,
                     "user"."email",
                     ucs.dt,
                     ucs.graduated,
                     ucs.submitted_all,
                     ucs.viewed_all,
                     date_part('month', ucs.dt),
                     date_part('year', ucs.dt)
            order by ${sorts[sortBy]} ${sortOrder === 'desc' ? 'DESC' : 'ASC'}
            limit $1 offset $2
           `,
      [limit, offset],
    );

    return {
      results,
      meta: {
        limit,
        offset,
        totals: results.length ? Number.parseInt(results[0].total_count) : 0,
      },
    };
  }

  async progressDistributionUsersByBracket(coursesIds: string[], bracket: number, limit: number, offset: number, tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const results = await getConnection().query(
      `
      select
        e.user_id,
        e.course_id,
        min(e."name") as "name",
        min(e.email) as email,
        max(e.bracket) as bracket,
        max(e.progress) as progress,
        max(e.enrollment_date) as enrollment_date,
        max(e.enrollment_date_from) as enrollment_date_from
      from (
        select
        v.user_id,
        u."name",
        u.email,
        v.course_id,
        ceil(v.progress / 10.0) as bracket,
        v.progress,
        e.created as enrollment_date,
        e.date_from as enrollment_date_from
        from view_user_progress_count_materialized v
        left join enrollment e on e.course_id = v.course_id and e.user_id = v.user_id
        left join "user" u on u.id = v.user_id
        where v.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and (e.date_from is null or e.date_from < now())
          and (e.date_to is null or e.date_to > now())
          and e.role = 'student'
          and e.active = true
      ) as e
      where e.bracket = $1
      group by e.user_id, e.course_id
      order by min(e."name") asc
      limit $2 offset $3
    `,
      [bracket, limit, offset],
    );

    const counts = await getConnection().query(
      `
      select count(*) as count
      from (
        select e.user_id, e.course_id
        from (
          select
          v.user_id,
          u."name",
          u.email,
          v.course_id,
          ceil(v.progress / 10.0) as bracket,
          v.progress,
          e.created as enrollment_date,
          e.date_from as enrollment_date_from
          from view_user_progress_count_materialized v
          left join enrollment e on e.course_id = v.course_id and e.user_id = v.user_id
          left join "user" u on u.id = v.user_id
          where v.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and (e.date_from is null or e.date_from < now())
          and (e.date_to is null or e.date_to > now())
          and e.role = 'student'
          and e.active = true
        ) as e
        where e.bracket = $1
        group by e.user_id, e.course_id
      ) as e
    `,
      [bracket],
    );

    return {
      results: results.map(item => {
        const { enrollment_date, enrollment_date_from, ...other } = item;
        return {
          ...other,
          date: enrollment_date_from || enrollment_date,
          progress: Number.parseFloat(item.progress),
        };
      }),
      meta: {
        limit,
        offset,
        totals: counts.length ? Number.parseInt(counts[0].count) : 0,
      },
    };
  }

  async progressSpreadPerLecture(coursesIds: string[], minDate?: string, maxDate?: string, activeState?: string, tagIds?: string[]) {
    coursesIds.forEach(id => validateUUID(id));

    const lectures = await getConnection().query(`
      select
        c.id as course_id,
        s.id as section_id,
        s.title as section_title,
        s.sort as section_number,
        s.active as section_active,
        m.id as module_id,
        m.title as module_title,
        m."number" as module_number,
        m.custom_number as module_custom_number,
        m.active as module_active,
        l.id as lecture_id,
        l.title as lecture_title,
        l."number" as lecture_number,
        l.custom_number as lecture_custom_number,
        l.active as lecture_active
      from lecture l
      inner join course c on c.id = l.course_id
      inner join "module" m on m.id = l.module_id
      left outer join "section" s on s.id = m.section_id
      where l.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
        ${
          activeState === 'active'
            ? `
          and l.active = true
          and m.active = true
          and (s.active IS NULL or s.active = true)
        `
            : ''
        }
        ${
          activeState === 'inactive'
            ? `
          and (l.active = false or m.active = false or s.active = false)
        `
            : ''
        }
      order by c.id, s.sort asc, m.sort asc, l.sort asc
    `);

    const items = await getConnection().query(
      `
      select
        r.course_id,
        r.lecture_id,
        min(r.module_number) as module_number,
        min(r.module_custom_number) as module_custom_number,
        min(r.lecture_number) as lecture_number,
        min(r.lecture_custom_number) as lecture_custom_number,
        min(r.section_number) as section_number,
        r.submission_status,
        count (*) as count
      from (
        select
          e.course_id,
          m.id as module_id,
          l.id as lecture_id,
          e.user_id,
          m.title as module_title,
          m."number" as module_number,
          m.custom_number as module_custom_number,
          l."number" as lecture_number,
          l.custom_number as lecture_custom_number,
          sec.sort as section_number,
          lp.status as submission_status
        from enrollment e
        inner join "module" m on m.course_id = e.course_id
        inner join lecture l on l.course_id = m.course_id and l.module_id = m.id
        inner join lecture_progress as lp on lp.lecture_id = l.id and lp.user_id = e.user_id
        left join "section" sec on sec.id = m.section_id
        where e.course_id in (${coursesIds.map(id => `uuid('${id}')`).join(', ')})
          ${
            tagIds && tagIds.length > 0
              ? `and e.user_id in (
            select user_id
            from user_tags as u
            where u.tag_id in (${tagIds.map(id => `uuid('${id}')`).join(', ')}))`
              : ''
          }
          and e.role = 'student'
          and e.active
          and lp.deleted is null
        ${
          activeState === 'active'
            ? `
          and l.active = true
          and m.active = true
        `
            : ''
        }
        ${
          activeState === 'inactive'
            ? `
          and (l.active = false or m.active = false)
        `
            : ''
        }
      ) as r
      group by r.course_id, r.lecture_id, r.submission_status
      `,
    );

    const mappedItems = items.map(
      ({
        count,
        course_id,
        lecture_id,
        module_number,
        module_custom_number,
        lecture_number,
        lecture_custom_number,
        section_number,
        submission_status,
      }) => {
        return {
          course_id,
          lecture_id,
          module_number: module_custom_number || module_number,
          lecture_number: lecture_custom_number || lecture_number,
          section_number,
          submission_status,
          count: Number.parseInt(count),
        };
      },
    );

    return lectures.map(lecture => {
      const { course_id, lecture_id } = lecture;
      return {
        ...lecture,
        items: mappedItems.filter(item => item.course_id === course_id && item.lecture_id === lecture_id),
      };
    });
  }
}
