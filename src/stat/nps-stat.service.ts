import { Nps } from '@entities/nps.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { InjectEntityManager } from '@nestjs/typeorm';
import { QueryNpsStatDto } from './dto/query-npsStat.dto';
import { ResultNpsStatDto } from './dto/result-npsStat.dto';
import { Course } from '@entities/course.entity';
import { User } from '@entities/user.entity';

@Injectable()
export class NpsStatService {
  constructor(
    @InjectEntityManager()
    private entityManager: EntityManager,
  ) {}

  async npsStat({ period, groupBy, coursesIds, tagIds, type, dateFrom, dateTo }: QueryNpsStatDto): Promise<ResultNpsStatDto[]> {
    const qb = this.entityManager
      .createQueryBuilder()
      .from(subQuery => {
        subQuery
          .from(Nps, `nps`)
          .select(
            `
            count(1) as total,
            count(1) filter (where score >= 9) as promoters,
            count(1) filter (where score >= 7 and score <= 8) as neutral,
            count(1) filter (where score <= 6) as detractors
          `,
          )
          .andWhere('completed is not null');

        //Filter by courses
        if (coursesIds && coursesIds.length) {
          subQuery.andWhere('course_id in (:...coursesIds)', {
            coursesIds: coursesIds,
          });
        }

        //Date filter
        if (dateFrom && dateTo && dateTo < dateFrom) {
          throw new BadRequestException(`Before date should be greater than after date`);
        }

        if (dateFrom) {
          subQuery.andWhere('completed >= :dateFrom', {
            dateFrom: new Date(dateFrom),
          });
        }

        if (dateTo) {
          subQuery.andWhere('completed < :dateTo', {
            dateTo: new Date(dateTo),
          });
        }

        //Type filter
        if (type) {
          subQuery.andWhere(`nps.type = :type`, { type: type });
        }

        //User tags filter
        if (tagIds?.length) {
          subQuery
            .innerJoinAndMapOne('nps.user', User, 'user', 'uuid(nps.user_id) = "user".id')
            .leftJoin('user.tags', 'tags')
            .addGroupBy('user.id');

          subQuery.andWhere('tags.id in (:...tagIds)', {
            tagIds: tagIds,
          });
        }

        //Group by building
        if (groupBy && groupBy.length) {
          for (const item of groupBy) {
            if (item === 'course_id') {
              subQuery.innerJoinAndMapOne('nps.course', Course, 'course', 'uuid(nps.course_id) = course.id').addGroupBy('course.id');
            } else if (item === 'period') {
              subQuery.addSelect(`date_trunc('${period}', completed) as period`).addGroupBy(item).addOrderBy('period', 'ASC');
            } else {
              subQuery.addSelect(item).addGroupBy(item);
            }
          }
        }

        return subQuery;
      }, 'n')
      .select('*, case when n.total = 0 then n.total else (promoters - detractors) * 100 / n.total end as nps_value');

    //Filter items with total 0
    const data = (await qb.getRawMany()).filter(item => item.total != 0);

    return <ResultNpsStatDto[]>data;
  }
}
