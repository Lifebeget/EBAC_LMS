import { IsUUID, ValidateIf } from 'class-validator';

export class QueryCourseStatDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  periodStart?: Date;

  periodEnd?: Date;
}
