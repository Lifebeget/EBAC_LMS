import { SystemRole } from '@lib/types/enums';
import { ApiProperty } from '@nestjs/swagger';

export class QueryStatSummaryDto {
  @ApiProperty({
    description: 'Start date for filter',
    required: false,
  })
  periodStart?: Date;

  @ApiProperty({
    description: 'End date for filter',
    required: false,
  })
  periodEnd?: Date;

  @ApiProperty({
    description: 'Filter by user',
    required: false,
  })
  userId?: string;

  @ApiProperty({
    description: 'Filter by course',
    required: false,
  })
  courseId?: string;

  @ApiProperty({
    description: 'Filter by role',
    required: false,
  })
  role?: SystemRole;
}
