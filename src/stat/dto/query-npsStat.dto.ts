import { NpsPeriod } from './../../../../lib/src/types/enums/nps-period.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsOptional, IsEnum, IsIn, IsDate } from 'class-validator';
import config from 'config';
import { Transform } from 'class-transformer';

export class QueryNpsStatDto {
  @ApiProperty({
    description: 'Group by fields',
    required: false,
  })
  groupBy?: string[];

  @ApiProperty({
    description: 'Period',
    enum: NpsPeriod,
    required: false,
  })
  @IsOptional()
  @IsEnum(NpsPeriod)
  period?: NpsPeriod;

  @ApiProperty({
    description: 'Filter by courses',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  coursesIds?: string[];

  @ApiProperty({
    description: 'Filter by tags',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  tagIds?: string[];

  @ApiProperty({
    description: 'Type',
    required: false,
  })
  @IsOptional()
  @IsIn(config.nps.triggers.map(t => t.type))
  type?: string;

  @ApiProperty({
    description: 'Start date for filter',
    required: false,
  })
  @Transform(p => new Date(p.value))
  @IsOptional()
  @IsDate()
  dateFrom?: Date;

  @ApiProperty({
    description: 'End date for filter',
    required: false,
  })
  @Transform(p => new Date(p.value))
  @IsOptional()
  @IsDate()
  dateTo?: Date;
}
