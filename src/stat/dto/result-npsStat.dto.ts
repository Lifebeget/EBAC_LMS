import { ApiProperty } from '@nestjs/swagger';

export class ResultNpsStatDto {
  @ApiProperty({ description: 'total' })
  total: number;

  @ApiProperty({ description: 'detractors' })
  detractors: number;

  @ApiProperty({ description: 'neutral' })
  neutral: number;

  @ApiProperty({ description: 'promoters' })
  promoters: number;

  @ApiProperty({ description: 'NPS = (promoters - detractors) * 100 / total' })
  nps_value: number;

  @ApiProperty({ description: 'NPS type' })
  type?: string;

  @ApiProperty({ description: 'Period type' })
  period?: Date;

  @ApiProperty({ description: 'Course id' })
  course_id?: string;
}
