import { IsOptional, IsUUID, ValidateIf } from 'class-validator';

export class CoursesProgressDistributionDto {
  @ValidateIf(o => o.hasOwnProperty('courses'))
  @IsUUID(4, { each: true })
  courses?: string[];

  @IsOptional()
  @IsUUID(4, { each: true })
  tags?: string[];
}
