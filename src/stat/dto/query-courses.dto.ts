import { IsOptional, IsUUID } from 'class-validator';

export class QueryByCoursesDto {
  @IsUUID(4, { each: true })
  courses?: string[];
  minDate?: string;
  maxDate?: string;
  limit?: number;
  offset?: number;
  activeState?: string;

  @IsOptional()
  @IsUUID(4, { each: true })
  tags?: string[];
}

export class QueryUsersByBracketDto extends QueryByCoursesDto {
  bracket?: number;
}

export class QueryByCoursesGraduatesDto extends QueryByCoursesDto {
  @IsOptional()
  month?: string;
  @IsOptional()
  type?: string[];
  sortBy?: string;
  sortOrder?: string;
}
