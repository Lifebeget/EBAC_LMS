import { IsUUID, ValidateIf } from 'class-validator';

export class QueryTutorStatDto {
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId?: string;

  periodStart?: Date;

  periodEnd?: Date;
}
