import { SystemRole } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, ValidateIf } from 'class-validator';

export class QueryStatDto {
  @ApiProperty({
    description: 'Filter by course',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('id'))
  @IsUUID()
  courseId?: string;

  @ApiProperty({
    description: 'Filter by user',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('id'))
  @IsUUID()
  userId?: string;

  @ApiProperty({
    description: 'Start date for filter',
    required: false,
  })
  periodStart?: string;

  @ApiProperty({
    description: 'End date for filter',
    required: false,
  })
  periodEnd?: string;

  @ApiProperty({
    description: 'Role',
    required: false,
  })
  asRole?: SystemRole;
}
