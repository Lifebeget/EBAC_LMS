import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import * as dayjs from 'dayjs';

const getDateFromString = (str: string): dayjs.Dayjs => {
  if (str == null || str === '') {
    return null;
  }

  const d = dayjs(str);

  return d.isValid() ? d : null;
};

@Injectable()
export class SubmissionStatService {
  async getSubmissionsReport(periodFrom: string = null, periodTo: string = null) {
    const dateFrom = getDateFromString(periodFrom)?.startOf('day');
    const dateTo = getDateFromString(periodTo)?.add(1, 'day');
    const queryParams = [
      ...(dateFrom != null ? ['and s.submitted_at::date >= '] : []),
      ...(dateTo != null ? ['and s.submitted_at::date < '] : []),
    ];
    const sqlQuery = `
    select a.week, a.year, a.submitted, a.resp_time_avg, b.graded
    from (

      select year, week, sum(submitted) as submitted, avg(resp_time_avg) as resp_time_avg
      from (
      select week, month, submitted, resp_time_avg,
      case when month = 1 and week >= 52 then year - 1
        else year
      end as year
      from (
        select
              date_part('week', submitted_at::date) AS week,
              date_part('year', submitted_at::date) AS year,
              date_part('month', min(s.submitted_at)) as month,
              count(1) as submitted, AVG(DATE_PART('day', graded_at - submitted_at) * 24 + DATE_PART('hour', graded_at - submitted_at)) as resp_time_avg
            from submission s left join assessment a on a.id = s.assessment_id
            where a.type = 'default'
              and s.deleted IS NULL
              and s.submitted_at IS NOT NULL
              and ((date_part('year', submitted_at::date) = 2021 and date_part('week', submitted_at::date) > 14) or date_part('year', submitted_at::date) >= 2022)
              ${queryParams.map((item, index) => `${item}$${index + 1}`).join('\n')}
            group by year, week
      ) as s
      ) as s
      group by year, week
    ) as a
    left join (
      select year, week, sum(graded) as graded
      from (
      select week, month, graded,
      case when month = 1 and week >= 52 then year - 1
        else year
      end as year
      from (
        select
        date_part('week', graded_at::date) AS week,
        date_part('year', graded_at::date) AS year,
        date_part('month', min(s.graded_at)) as month,
        count(1) as graded
        from submission s
        left join assessment a on a.id = s.assessment_id
        where a.type = 'default'
        and s.graded_at is not null
        group by year, week
      ) as s
      ) as s
      group by year, week
      ) as b on b.week = a.week and b.year = a.year
    order by a.year desc, a.week desc
    `;
    const items = await getConnection().query(sqlQuery, [
      ...(dateFrom != null ? [dateFrom.format('YYYY-MM-DD')] : []),
      ...(dateTo != null ? [dateTo.format('YYYY-MM-DD')] : []),
    ]);

    return items;
  }
}
