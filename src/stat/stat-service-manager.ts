import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ForumGrouping } from '@lib/types/enums/forum-grouping.enum';
import { DateService } from 'src/utils/date.service';
import { UserForumStatService } from './forum/user-forum-stat-service';
import { CourseForumStatService } from './forum/course-forum-stat-service';
import { CourseSubmissionsStatService } from './submissions/course-submissions-stat.service';
import { TutorSubmissionsStatService } from './submissions/tutor-submissions-stat.service';

@Injectable()
export class StatServiceManager {
  private readonly _submissionsByCourse: CourseSubmissionsStatService;
  private readonly _submissionsByTutor: TutorSubmissionsStatService;

  private readonly _forumMessagesByCourse: CourseForumStatService;
  private readonly _forumThreadsByCourse: CourseForumStatService;

  private readonly _forumMessagesByUser: UserForumStatService;
  private readonly _forumThreadsByUser: UserForumStatService;

  constructor(dateService: DateService, configService: ConfigService) {
    const sla = {
      urgentHours: configService.get<number>('forumResolve.urgentHours'),
      outdatedHours: configService.get<number>('forumResolve.outdatedHours'),
    };

    this._forumMessagesByUser = new UserForumStatService(false, sla);

    this._forumThreadsByUser = new UserForumStatService(true, sla);

    this._forumMessagesByCourse = new CourseForumStatService(false, sla);

    this._forumThreadsByCourse = new CourseForumStatService(true, sla);

    this._submissionsByCourse = new CourseSubmissionsStatService(dateService, configService);

    this._submissionsByTutor = new TutorSubmissionsStatService(dateService, configService);
  }

  get submissionsByCourse() {
    return this._submissionsByCourse;
  }

  get submissionsByTutor() {
    return this._submissionsByTutor;
  }

  get forumMessagesByUser() {
    return this._forumMessagesByUser;
  }

  get forumThreadsByUser() {
    return this._forumThreadsByUser;
  }

  get forumMessagesByCourse() {
    return this._forumMessagesByCourse;
  }

  get forumThreadsByCourse() {
    return this._forumThreadsByCourse;
  }

  getForumByCourse(grouping: ForumGrouping) {
    return grouping == ForumGrouping.ByThreads ? this.forumThreadsByCourse : this.forumMessagesByCourse;
  }

  getForumByUser(grouping: ForumGrouping) {
    return grouping == ForumGrouping.ByThreads ? this.forumThreadsByUser : this.forumMessagesByUser;
  }
}
