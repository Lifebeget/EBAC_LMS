/* eslint-disable @typescript-eslint/no-unused-vars */
import { User } from '@entities/user.entity';
import { SubmissionStatus, SystemRole } from '@lib/api/types';
import { ConfigService } from '@nestjs/config';
import { Submission } from 'src/lib/entities/submission.entity';
import { dateOnly } from 'src/query-helpers';
import { addTeamleadFilterForSubmissions } from 'src/query-helpers/teamlead.helper';
import { DateService } from 'src/utils/date.service';
import { EntityManager, EntityTarget, getManager, SelectQueryBuilder } from 'typeorm';
import { QueryStatDto } from '../dto/query-stat.dto';
import { QueryStatSummaryDto } from '../dto/query-statSummary.dto';
import { mergeStatsTmpl, removeForDate } from '../stat.helper';

export abstract class SubmissionsStatService<EntityType> {
  protected id_field: string;
  protected title_field: string;
  protected user: User;
  protected readonly entityManager: EntityManager;

  constructor(
    protected readonly dateService: DateService,
    protected readonly configService: ConfigService,
    protected readonly idField: string,
    protected readonly titleField: string,
    protected readonly entityType: EntityTarget<EntityType>,
  ) {
    this.id_field = this.camelToUnderscore(idField);
    this.title_field = this.camelToUnderscore(titleField);
    this.entityManager = getManager();
  }

  setUser(user: User) {
    this.user = user;
  }

  abstract submissionsQB(date: Date | string, params?: QueryStatDto): SelectQueryBuilder<Submission>;

  abstract getStudents(date, params): Promise<any[]>;

  populate(qb: SelectQueryBuilder<EntityType>) {
    return;
  }
  filterStats(qb: SelectQueryBuilder<EntityType>, params?: QueryStatDto) {
    return;
  }
  async filterSummary(qb: SelectQueryBuilder<EntityType>, params?: QueryStatSummaryDto) {
    return;
  }

  async afterCalc(stats: any[], date: Date | string) {
    return stats;
  }

  async afterQuery(stats: any[], date: Date | string) {
    return stats;
  }

  async save(date: Date) {
    const statistics = await this.calculate(date, null);
    return await this.getRepository().save(statistics as any[]);
  }

  async query(date: Date | string, params: QueryStatDto) {
    const qb = this.getRepository().createQueryBuilder('stat').orderBy(`stat.${this.titleField}`);

    if (params.periodStart) {
      qb.andWhere(`${dateOnly('stat.statDate')} = :date`, {
        date,
      });
    }

    // if (params.periodEnd) {
    //   qb.andWhere('stat.statDate <= :periodEnd', {
    //     periodEnd: params.periodEnd,
    //   });
    // }

    this.populate(qb);
    this.filterStats(qb, params);

    const stats = await qb.getMany();

    const prepared = await this.afterQuery(stats, params.periodStart);

    return prepared;
  }

  async summary(query: QueryStatSummaryDto) {
    const qb = this.getRepository()
      .createQueryBuilder('stat')
      .select(
        `
        ${dateOnly('stat.stat_date')} as stat_date,
        sum(stat."all") as "all",
        sum(stat.ungraded) as ungraded,
        sum(stat.graded) as graded,
        sum(stat.urgent) as urgent,
        sum(stat.outdated) as outdated,
        sum(stat.created_in_day) as created_in_day,
        sum(stat.graded_in_day) as graded_in_day,
        sum(stat.students) as students`,
      )
      .groupBy(`stat_date`)
      .orderBy('stat_date');
    if (query.periodStart) {
      qb.andWhere(`${dateOnly('stat.stat_date')} >= :periodStart`, {
        periodStart: query.periodStart,
      });
    }

    if (query.periodEnd) {
      qb.andWhere(`${dateOnly('stat.stat_date')} <= :periodEnd`, {
        periodEnd: query.periodEnd,
      });
    }

    await this.filterSummary(qb, query);

    const result = await qb.getRawMany();

    return result.map(r => ({
      statDate: r.stat_date,
      all: r.all,
      ungraded: r.ungraded,
      graded: r.graded,
      urgent: r.urgent,
      outdated: r.outdated,
      createdInDay: r.created_in_day,
      gradedInDay: r.graded_in_day,
      students: r.students,
    }));
  }

  async calculate(date: Date | string, params: QueryStatDto) {
    const allSubmissions = await this.submissionsQB(date, params).getRawMany();

    const mergeStats = mergeStatsTmpl(allSubmissions, this.id_field);

    const qb = this.submissionsQB(date, params).andWhere(`(submission.status = :status or ${dateOnly('submission.graded_at')} > :date)`, {
      status: SubmissionStatus.Submitted,
      date,
    });

    const ungraded = await qb.getRawMany();

    mergeStats(ungraded, 'ungraded');

    const graded = await this.submissionsQB(date, params)
      .andWhere(`submission.status = :status and ${dateOnly('submission.graded_at')} <= :date `, {
        status: SubmissionStatus.Graded,
        date,
      })
      .getRawMany();

    mergeStats(graded, 'graded');

    graded.forEach(e => {
      const found = allSubmissions.find(s => s[this.id_field] == e[this.id_field]);

      if (found) found.graded = e.cnt;
    });

    const endOfDay = this.dateService.endOfDay(date);

    const urgent = await this.submissionsQB(date, params)
      .andWhere(':endOfDay <= tariff.outdated_date', { endOfDay })
      .andWhere(':endOfDay > tariff.urgent_date', { endOfDay })
      .andWhere(`(submission.status = :status or ${dateOnly('submission.gradedAt')} > :date)`, {
        status: SubmissionStatus.Submitted,
        date,
      })
      .getRawMany();

    mergeStats(urgent, 'urgent');

    const outdated = await this.submissionsQB(date, params)
      .andWhere(':endOfDay > tariff.outdated_date', { endOfDay })
      .andWhere(`(submission.status = :status or ${dateOnly('submission.gradedAt')} > :date)`, {
        status: SubmissionStatus.Submitted,
        date,
      })
      .getRawMany();

    mergeStats(outdated, 'outdated');

    const createdInDay = await this.submissionsQB(date, params)
      .andWhere(`${dateOnly('submission.submitted_at')} = :date`, {
        date,
      })
      .getRawMany();

    mergeStats(createdInDay, 'createdInDay');

    const gradedInDay = await this.submissionsQB(date, params)
      .andWhere(`${dateOnly('submission.graded_at')} = :date`, {
        date,
      })
      .getRawMany();

    mergeStats(gradedInDay, 'gradedInDay');

    const students = await this.getStudents(date, params);
    mergeStats(students, 'students');

    const statistics = allSubmissions.map(s => ({
      [this.idField]: s[this.id_field] ?? '0',
      statDate: date,
      [this.titleField]: s[this.title_field] ?? '',
      students: s.students ?? 0,
      all: s.cnt ?? 0,
      ungraded: s.ungraded ?? 0,
      graded: s.graded ?? 0,
      urgent: s.urgent ?? 0,
      outdated: s.outdated ?? 0,
      createdInDay: s.createdInDay ?? 0,
      gradedInDay: s.gradedInDay ?? 0,
      courseVersion: s.course_version,
    }));

    return await this.afterCalc(statistics, date);
  }

  userFilter(qb: any, params: QueryStatDto) {
    if (params?.userId) {
      if (params.asRole === SystemRole.Teamlead) {
        addTeamleadFilterForSubmissions(qb, params.userId, params.periodStart || new Date());
      } else {
        qb.andWhere('tutor_id = :tutorId', {
          tutorId: params.userId,
        });
      }
    }
  }

  async removeForDate(date: string) {
    return removeForDate(date, this.getRepository());
  }

  private camelToUnderscore(key: string) {
    return key.replace(/([A-Z])/g, '_$1').toLowerCase();
  }

  async limits() {
    const limits = await this.getRepository()
      .createQueryBuilder('stat')
      .select(
        `
        min(stat_date) as min_stat_date,
        max(stat_date) as max_stat_date
      `,
      )
      .getRawOne();

    return {
      minStatDate: limits.min_stat_date,
      maxStatDate: limits.max_stat_date,
    };
  }

  private getRepository() {
    return this.entityManager.getRepository(this.entityType);
  }
}
