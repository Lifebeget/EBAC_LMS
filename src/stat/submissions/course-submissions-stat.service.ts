import { Injectable } from '@nestjs/common';
import { CourseStatistic } from 'src/lib/entities/course.statistic.entity';
import { Submission } from 'src/lib/entities/submission.entity';
import { Enrollment } from 'src/lib/entities/enrollment.entity';
import { SelectQueryBuilder } from 'typeorm';
import { SubmissionsStatService } from './submissions-stat.service';
import { ConfigService } from '@nestjs/config';
import { DateService } from 'src/utils/date.service';
import { AssessmentType } from '@lib/types/enums';
import { Course } from '@entities/course.entity';
import { QueryStatSummaryDto } from '../dto/query-statSummary.dto';
import { QueryStatDto } from '../dto/query-stat.dto';
import { dateOnly } from 'src/query-helpers';

@Injectable()
export class CourseSubmissionsStatService extends SubmissionsStatService<CourseStatistic> {
  constructor(dateService: DateService, configService: ConfigService) {
    super(dateService, configService, 'courseId', 'courseTitle', CourseStatistic);
  }

  submissionsQB(date: any): SelectQueryBuilder<Submission> {
    const qb = this.entityManager
      .createQueryBuilder(Submission, 'submission')
      .select(
        `course.id as course_id, 
           course.title as course_title, 
           course.sort as course_sort,
           course.version as course_version,
           count(distinct submission.id) as cnt`,
      )
      .innerJoin('submission.course', 'course')
      .leftJoin('submission.tariff', 'tariff')
      .innerJoin('submission.assessment', 'assessment')
      .where(`${dateOnly('submission.submitted_at')} <= :date`, {
        date,
      })
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .groupBy('course.id')
      .orderBy('course.sort');
    return qb;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getStudents(date: any, params?: QueryStatDto) {
    return this.entityManager
      .createQueryBuilder(Enrollment, 'enrollment')
      .select(
        `course.id as course_id,
        course.title as course_title,
        count(enrollment.id) as cnt`,
      )
      .innerJoin('enrollment.course', 'course')
      .innerJoin('enrollment.user', 'user')
      .where(`:date between ${dateOnly('enrollment.dateFrom')} and ${dateOnly('enrollment.dateTo')}`, { date })
      .andWhere(`${dateOnly('user.created')} <= :lastCreated`, {
        lastCreated: date,
      })
      .groupBy('course.id')
      .getRawMany();
  }
  populate(qb: SelectQueryBuilder<CourseStatistic>) {
    qb.leftJoinAndMapOne('stat.course', Course, 'course', 'course.id::text = stat.course_id');
  }

  async filterSummary(qb: SelectQueryBuilder<CourseStatistic>, params?: QueryStatSummaryDto) {
    const { courseId } = params;
    if (courseId) {
      qb.andWhere('stat.course_id = :courseId', { courseId });
    }
  }
}
