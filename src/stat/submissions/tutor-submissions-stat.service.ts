import { User } from '@entities/user.entity';
import { AssessmentType, SystemRole } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Submission } from 'src/lib/entities/submission.entity';
import { TutorStatistic } from '@entities/tutor-statistics.entity';
import { DateService } from 'src/utils/date.service';
import { SelectQueryBuilder } from 'typeorm';
import { QueryStatDto } from '../dto/query-stat.dto';
import { QueryStatSummaryDto } from '../dto/query-statSummary.dto';
import { SubmissionsStatService } from './submissions-stat.service';
import { Enrollment } from '@entities/enrollment.entity';
import { getTeamleadTutors } from 'src/query-helpers/teamlead.helper';
import { dateOnly } from 'src/query-helpers';

@Injectable()
export class TutorSubmissionsStatService extends SubmissionsStatService<TutorStatistic> {
  constructor(dateService: DateService, configService: ConfigService) {
    super(dateService, configService, 'tutorId', 'tutorName', TutorStatistic);
  }

  filterStats(qb: SelectQueryBuilder<TutorStatistic>, params?: QueryStatDto) {
    if (params.userId) {
      if (params.asRole === SystemRole.Teamlead) {
        qb.innerJoin(Enrollment, 'te', 'enrollment.course_id = te.course_id')
          .andWhere('te.active = :active', { active: true })
          .andWhere('te.role = :role', { role: CourseRole.Teamlead })
          .andWhere('te.user_id = :teamleadId', { teamleadId: params.userId })
          .andWhere(':date between coalesce(te.date_from, :min) and coalesce(te.date_to, :max)', {
            date: params.periodStart,
            min: this.dateService.MIN_DATE,
            max: this.dateService.MAX_DATE,
          });
      } else {
        qb.andWhere('tutor_id = :tutorId', {
          tutorId: params.userId,
        });
      }
    }
  }
  populate(qb: SelectQueryBuilder<TutorStatistic>) {
    qb.leftJoinAndMapOne('stat.user', User, 'tutor', 'tutor.id::text = stat.tutor_id')
      .leftJoinAndSelect('tutor.modules', 'module')
      .leftJoinAndSelect('tutor.roles', 'roles')
      .leftJoinAndSelect('module.course', 'module_course')
      .leftJoinAndSelect('tutor.enrollments', 'enrollment')
      .leftJoinAndSelect('enrollment.course', 'course');
  }
  submissionsQB(date: Date | string, params?: QueryStatDto): SelectQueryBuilder<Submission> {
    const qb = this.entityManager
      .createQueryBuilder(Submission, 'submission')
      .innerJoin('submission.tutor', 'tutor')
      .innerJoin('submission.assessment', 'assessment')
      .innerJoin('submission.tariff', 'tariff')
      .where(`${dateOnly('submission.submitted_at')} <= :date`, { date })
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .groupBy('tutor.id')
      .select(
        `
        tutor.id as tutor_id,
        tutor.name as tutor_name,
        tutor,
        count(distinct submission.id) as cnt
      `,
      )
      .orderBy('tutor.name');

    this.userFilter(qb, params);

    return qb;
  }
  async afterCalc(stats: any[], date: Date | string) {
    let tutors = await this.entityManager
      .getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.moduleAssignments', 'moduleAssignments')
      .leftJoinAndSelect('moduleAssignments.module', 'module')
      .leftJoinAndSelect('module.course', 'moduleCourse')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoinAndSelect('user.enrollments', 'enrollments')
      .leftJoinAndSelect('enrollments.course', 'course')
      .where('user.id in (:...userIds)', { userIds: stats.filter(s => s.tutorId !== '0').map(s => s.tutorId) })
      .getMany();

    tutors = tutors.map(tutor => {
      let modules = tutor.moduleAssignments.map(moduleAssigment => ({ ...moduleAssigment.module, exclusive: moduleAssigment.exclusive }));
      if (modules.some(module => module.section)) {
        modules = modules.sort((a, b) => a?.sort - b?.sort).sort((a, b) => a?.section?.sort - b?.section?.sort);
      } else {
        modules = modules.sort((a, b) => a?.sort - b?.sort);
      }
      return {
        ...tutor,
        modules,
      };
    });

    for (const s of stats) {
      s.user = {
        ...{ enrollments: [], modules: [] },
        ...tutors.find(t => t.id === s.tutorId),
        teamleadTutors: await getTeamleadTutors(s.tutorId, date),
      };
    }

    return stats;
  }

  async afterQuery(stats: any[], date: Date | string) {
    for (const s of stats) {
      s.courseVersion = s.course?.version;
      delete s.course;
      if (s.user) {
        s.user.teamleadTutors = await getTeamleadTutors(s.tutorId, date);
      }
    }

    return stats;
  }

  async getStudents(date, params?: QueryStatDto) {
    const qb = this.entityManager
      .createQueryBuilder(Submission, 'submission')
      .select(
        `tutor.id as tutor_id,
        tutor.name as tutor_name,
        count(distinct submission.user_id) as cnt`,
      )
      .innerJoin('enrollment', 'enrollment', 'submission.user_id = enrollment.user_id')
      .leftJoin('submission.tutor', 'tutor')
      .innerJoin('submission.user', 'user')
      .leftJoin('submission.assessment', 'assessment')
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .where(`:date between ${dateOnly('enrollment.dateFrom')} and ${dateOnly('enrollment.dateTo')}`, { date })
      .andWhere(`${dateOnly('user.created')} <= :lastCreated`, {
        lastCreated: date,
      })
      .groupBy('tutor.id');

    this.userFilter(qb, params);

    return qb.getRawMany();
  }

  async filterSummary(qb: SelectQueryBuilder<TutorStatistic>, params?: QueryStatSummaryDto) {
    if (params.userId) {
      qb.andWhere('tutor_id = :tutorId', { tutorId: params.userId });
    } else if (params.role == SystemRole.Teamlead) {
      qb.innerJoin(User, 'tutor', 'tutor.id::text = stat.tutor_id')
        .innerJoin('tutor.enrollments', 'enrollment')
        .innerJoin(Enrollment, 'te', 'enrollment.course_id = te.course_id')
        .andWhere('te.active = :active', { active: true })
        .andWhere('te.role = :role', { role: CourseRole.Teamlead })
        .andWhere('te.user_id = :teamleadId', {
          teamleadId: this.user.id,
        });
    }
  }
}
