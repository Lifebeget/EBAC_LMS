import { Repository } from 'typeorm';
import * as dayjs from 'dayjs';

export const mergeStatsTmpl = (dst: any[], idField: string) => (src: any[], statField: string) => {
  src.forEach(s => {
    const found = dst.find(d => d[idField] === s[idField]);

    if (found) {
      found[statField] = Number(s.cnt);
      return;
    }

    dst.push({
      ...s,
      [statField]: s.cnt,
    });
  });
};

export const removeForDate = <StatEntity>(date: Date | string, repository: Repository<StatEntity>) => {
  const dayStart = dayjs(date).startOf('day');
  const dayEnd = dayjs(dayStart).endOf('day');

  const qb = repository.createQueryBuilder().delete().where('stat_date between :dayStart and :dayEnd', {
    dayStart,
    dayEnd,
  });
  qb.execute();
};
