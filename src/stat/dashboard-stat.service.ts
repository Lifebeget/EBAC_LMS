import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { DashboardStat } from '@lib/api/types';

type RawSqlEntry<Type> = {
  [Property in keyof Type]: string;
};

const queryOne = `
select
  count(1) as total_enrollments,
  count(distinct e.user_id) as total_students,
  count(1)
    filter (where e.type IN ('buy', 'unknown', 'transfer'))
    as paid_enrollments,
  count(distinct e.user_id)
    filter (where e.type IN ('buy', 'unknown', 'transfer'))
    as paid_students,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '7 DAYS')
    as enrollments_last_7_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '14 DAYS' and e.created < NOW() - INTERVAL '7 DAYS')
    as enrollments_previous_7_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '30 DAYS')
    as enrollments_last_30_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '60 DAYS' and e.created < NOW() - INTERVAL '30 DAYS')
    as enrollments_previous_30_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '7 DAYS' AND e.type IN ('buy', 'unknown', 'transfer'))
    as paid_enrollments_last_7_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '14 DAYS' and e.created < NOW() - INTERVAL '7 DAYS' AND e.type IN ('buy', 'unknown', 'transfer'))
    as paid_enrollments_previous_7_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '30 DAYS' AND e.type IN ('buy', 'unknown', 'transfer'))
    as paid_enrollments_last_30_days,
  count(1)
    filter (where e.created >= NOW() - INTERVAL '60 DAYS' and e.created < NOW() - INTERVAL '30 DAYS' AND e.type IN ('buy', 'unknown', 'transfer'))
    as paid_enrollments_previous_30_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view IS NOT NULL)
    as newlms_students,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '7 DAYS')
    as newlms_students_last_7_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '14 DAYS' and ua.first_lecture_view < NOW() - INTERVAL '7 DAYS')
    as newlms_students_previous_7_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view IS NOT NULL and u.external_id IS NOT NULL)
    as newlms_external_students,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '7 DAYS' and u.external_id IS NOT NULL)
    as newlms_external_students_last_7_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '14 DAYS' and ua.first_lecture_view < NOW() - INTERVAL '7 DAYS' and u.external_id IS NOT NULL)
    as newlms_external_students_previous_7_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view IS NOT NULL and u.external_id IS NULL)
    as newlms_not_external_students,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '7 DAYS' and u.external_id IS NULL)
    as newlms_not_external_students_last_7_days,
  count(distinct e.user_id)
    filter (where ua.first_lecture_view >= NOW() - INTERVAL '14 DAYS' and ua.first_lecture_view < NOW() - INTERVAL '7 DAYS' and u.external_id IS NULL)
    as newlms_not_external_students_previous_7_days,
  count(distinct e.user_id)
    filter (where ua.modified >= NOW() - INTERVAL '1 DAY')
    as students_active_last_1_day,
  count(distinct e.user_id)
    filter (where ua.modified >= NOW() - INTERVAL '7 DAYS')
    as students_active_last_7_days,
  count(distinct e.user_id)
    filter (where ua.modified >= NOW() - INTERVAL '30 DAYS')
    as students_active_last_30_days,
  count(distinct e.user_id)
    filter (where u.external_id IS NOT NULL and ua.first_lecture_view IS NULL)
    as students_migrated_remain,
  count(distinct e.user_id)
    filter (where e.graduated_at IS NOT NULL AND u.external_id IS NOT NULL and ua.first_lecture_view IS NULL)
    as graduated_external_students,
  count(distinct e.user_id)
    filter (where s1.submitted_at >= NOW() - INTERVAL '1 DAY')
    as students_submitted_last_1_day,
  count(distinct e.user_id)
    filter (where s1.submitted_at >= NOW() - INTERVAL '7 DAYS')
    as students_submitted_last_7_days,
  count(distinct e.user_id)
    filter (where s1.submitted_at >= NOW() - INTERVAL '30 DAYS')
    as students_submitted_last_30_days
from enrollment e
left join user_activity ua on ua.user_id = e.user_id
left join (
  select
    MAX(s.submitted_at) as submitted_at,
    s.user_id
  from submission s
  inner join assessment a ON s.assessment_id = a.id AND (a.type = 'default' OR a.type = 'quiz')
  where s.deleted IS NULL
  group by s.user_id
) s1 on s1.user_id = e.user_id
inner join "user" u on u.id = e.user_id
where
  e.active = true
  and e.role = 'student'
  and (e.date_from is null or e.date_from < now())
  and (e.date_to is null or e.date_to > now());
`;

const queryTwo = `
select
  count(distinct e.user_id) filter (where e.enrollment_date >= NOW() - INTERVAL '7 DAYS') as students_last_7_days,
  count(distinct e.user_id) filter (where e.enrollment_date >= NOW() - INTERVAL '14 DAYS' and e.enrollment_date < NOW() - INTERVAL '7 DAYS') as students_previous_7_days,
  count(distinct e.user_id) filter (where e.enrollment_date >= NOW() - INTERVAL '30 DAYS') as students_last_30_days,
  count(distinct e.user_id) filter (where e.enrollment_date >= NOW() - INTERVAL '60 DAYS' and e.enrollment_date < NOW() - INTERVAL '30 DAYS') as students_previous_30_days,
  count(distinct e.user_id)
    filter (where e.enrollment_date >= NOW() - INTERVAL '7 DAYS' AND en.type IN ('buy', 'unknown', 'transfer'))
    as paid_students_last_7_days,
  count(distinct e.user_id)
    filter (where e.enrollment_date >= NOW() - INTERVAL '14 DAYS' and e.enrollment_date < NOW() - INTERVAL '7 DAYS' AND en.type IN ('buy', 'unknown', 'transfer'))
    as paid_students_previous_7_days,
  count(distinct e.user_id)
    filter (where e.enrollment_date >= NOW() - INTERVAL '30 DAYS' AND en.type IN ('buy', 'unknown', 'transfer'))
    as paid_students_last_30_days,
  count(distinct e.user_id)
    filter (where e.enrollment_date >= NOW() - INTERVAL '60 DAYS' and e.enrollment_date < NOW() - INTERVAL '30 DAYS' AND en.type IN ('buy', 'unknown', 'transfer'))
    as paid_students_previous_30_days,
  count(distinct e.user_id) filter ( where en.graduated_at IS NOT NULL ) as graduated_students,
  count(distinct e.user_id)
      filter ( where en.graduated_at >= NOW() - INTERVAL '7 DAYS') as graduated_students_last_7_days,
  count(distinct e.user_id)
      filter ( where en.graduated_at >= NOW() - INTERVAL '14 DAYS' and en.graduated_at < NOW() - INTERVAL '7 DAYS')
      as graduated_students_previous_7_days,
  count(distinct e.user_id)
      filter ( where en.graduated_at >= NOW() - INTERVAL '30 DAYS') as graduated_students_last_30_days,
  count(distinct e.user_id)
      filter ( where en.graduated_at >= NOW() - INTERVAL '60 DAYS' and en.graduated_at < NOW() - INTERVAL '30 DAYS')
      as graduated_students_previous_30_days
from (
  select
    e.user_id,
    min(e.created) as enrollment_date
  from enrollment e
  where e.active
    and e.role = 'student'
    and (e.date_from is null or e.date_from < now())
    and (e.date_to is null or e.date_to > now())
  group by e.user_id
) e LEFT JOIN enrollment en ON en.user_id = e.user_id
`;

@Injectable()
export class DashboardStatService {
  parseIntOrNull(num: string | null): number | null {
    return num === null ? null : parseInt(num, 10);
  }

  async dashboardStat(): Promise<DashboardStat> {
    const dbConnection = getConnection();
    const resultArrays: RawSqlEntry<DashboardStat>[][] = await Promise.all([dbConnection.query(queryOne), dbConnection.query(queryTwo)]);

    const stat = {};

    for (const resultArr of resultArrays) {
      const result = resultArr[0];
      for (const key in result) {
        stat[key] = parseInt(result[key], 10);
      }
    }

    return stat as DashboardStat;
  }
}
