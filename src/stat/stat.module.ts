import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseStatistic } from 'src/lib/entities/course.statistic.entity';
import { TutorStatistic } from '@entities/tutor-statistics.entity';
import { StatController } from './stat.controller';
import { CourseSubmissionsStatService } from './submissions/course-submissions-stat.service';
import { TutorSubmissionsStatService } from './submissions/tutor-submissions-stat.service';
import { Submission } from 'src/lib/entities/submission.entity';
import { Enrollment } from 'src/lib/entities/enrollment.entity';
import { ConfigModule } from '@nestjs/config';
import { User } from '@entities/user.entity';
import { TutorAttendanceStatService } from './tutor-attendance-stat.service';
import { SubmissionStatService } from './submission-stat.service';
import { Course } from '@entities/course.entity';
import { DashboardStatService } from './dashboard-stat.service';
import { ProgressStatService } from './progress-stat.service';
import { NpsStatService } from './nps-stat.service';
import { UserForumStatistic } from '@entities/user-forum-statistics.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { CourseForumStatistic } from '@entities/course-forum-statistics.entity';
import { StatServiceManager } from './stat-service-manager';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([
      User,
      CourseStatistic,
      TutorStatistic,
      UserForumStatistic,
      CourseForumStatistic,
      Enrollment,
      Submission,
      Course,
      ForumMessages,
    ]),
  ],
  controllers: [StatController],
  providers: [
    CourseSubmissionsStatService,
    TutorSubmissionsStatService,
    StatServiceManager,
    TutorAttendanceStatService,
    SubmissionStatService,
    DashboardStatService,
    ProgressStatService,
    NpsStatService,
  ],
})
export class StatModule {}
