import { Enrollment } from '@entities/enrollment.entity';
import { emptyForumStatistic } from '@entities/forum-statistics.entity';
import { SystemRole } from '@lib/types/enums';
import * as dayjs from 'dayjs';
import { camelizeKeys } from 'humps';
import { dateOnly } from 'src/query-helpers';
import { addTeamleadFilter } from 'src/query-helpers/teamlead.helper';
import { EntityManager, EntityTarget, getManager } from 'typeorm';
import { QueryStatDto } from '../dto/query-stat.dto';
import { QueryStatSummaryDto } from '../dto/query-statSummary.dto';
import { mergeStatsTmpl } from '../stat.helper';
import { CourseForumStatQuery } from './query/course-forum-stat-query';
import { ForumStatQuery } from './query/forum-stat-query';
import { Sla } from './query/forum-stat-query.base';
import { UserForumStatQuery } from './query/user-forum-stat-query';

const createQuery = (forCourses: boolean, forThreads: boolean, date: string, sla: Sla, filter?: QueryStatDto): ForumStatQuery => {
  if (forCourses) {
    return new CourseForumStatQuery(forThreads, date, sla, filter);
  }

  return new UserForumStatQuery(forThreads, date, sla, filter);
};

export class ForumStatService<Entity> {
  protected readonly entityManager: EntityManager;

  constructor(
    protected readonly idField: string,
    protected readonly byCourses: boolean,
    protected readonly byThreads: boolean,
    protected readonly entityType: EntityTarget<Entity>,
    protected readonly sla: Sla,
  ) {
    this.entityManager = getManager();
  }

  async save(date: string) {
    const statistics = await this.calculate(date, null);
    return await this.getRepository().save(statistics as any[]);
  }

  async calculate(date: string, filter: QueryStatDto) {
    const q = createQuery(this.byCourses, this.byThreads, date, this.sla, filter);

    const sent = await q.sentAll().getRawMany();

    const mergeStats = mergeStatsTmpl(sent, this.idField);

    mergeStats(sent, 'sent');

    const sentToday = await q.sentToday().getRawMany();
    mergeStats(sentToday, 'sentToday');

    const resolved = await q.resolvedAll().getRawMany();
    mergeStats(resolved, 'resolved');

    const resolvedToday = await q.resolvedToday().getRawMany();
    mergeStats(resolvedToday, 'resolvedToday');

    const unresolved = await q.unresolvedAll().getRawMany();
    mergeStats(unresolved, 'unresolved');

    const unresolvedToday = await q.unresolvedToday().getRawMany();
    mergeStats(unresolvedToday, 'unresolvedToday');

    const urgent = await q.urgent().getRawMany();
    mergeStats(urgent, 'urgent');

    const outdated = await q.outdated().getRawMany();
    mergeStats(outdated, 'outdated');

    const formatted = sent.map(row => this.formatRow(row, date));

    const calculated = await this.afterCalc(formatted, date);

    return calculated;
  }

  async query(date: string, params: QueryStatDto) {
    const qb = this.createQueryBuilder()
      .where(`stat.stat_date = :date`, { date })
      .andWhere('stat.is_thread = :isThread', {
        isThread: this.byThreads,
      })
      .orderBy(`stat.${this.idField == 'user_id' ? 'user_name' : 'course_title'}`);

    if (params.asRole === SystemRole.Teamlead) {
      qb.innerJoin(Enrollment, 'e', `e.${this.idField}::text = stat.${this.idField}`);
      addTeamleadFilter(qb, params.userId, date, `e.course_id = teamlead_enrollment.course_id`);
    }

    const stats = await qb.getMany();

    const prepared = await this.afterCalc(stats, date);

    return prepared;
  }

  async summary(query: QueryStatSummaryDto) {
    const qb = this.createQueryBuilder()
      .select(
        `
        stat.stat_date as stat_date,
        sum(stat.sent) as sent,
        sum(stat.sentToday) as sent_today,
        sum(stat.resolved) as resolved,
        sum(stat.resolvedToday) as resolved_today,
        sum(stat.unresolved) as resolved,
        sum(stat.unresolvedToday) as resolved_today,
        sum(stat.urgent) as urgent,
        sum(stat.outdated) as outdated
        `,
      )
      .andWhere('stat.is_thread = :byThreads', { byThreads: this.byThreads })
      .groupBy('stat.stat_date')
      .orderBy('stat.stat_date');

    if (query.periodStart) {
      qb.andWhere('stat.stat_date >= :periodStart', {
        periodStart: query.periodStart,
      });
    }

    if (query.periodEnd) {
      qb.andWhere('stat.stat_date <= :periodEnd', {
        periodEnd: query.periodEnd,
      });
    }

    if (query.courseId) {
      qb.andWhere('stat.course_id = :courseId', { courseId: query.courseId });
    } else if (query.userId) {
      qb.andWhere('stat.user_id = :userId', { userId: query.userId });
    }

    const result = await qb.getRawMany();

    return result.map(r => ({
      ...camelizeKeys(r),
      statDate: dayjs(r.stat_date).utc(true).toISOString(),
    }));
  }

  async limits() {
    const limits = await this.createQueryBuilder()
      .select(
        `min(stat_date) as min_stat_date,
         max(stat_date) as max_stat_date`,
      )
      .where('is_thread = :isThread', {
        isThread: this.byThreads,
      })
      .getRawOne();

    return {
      minStatDate: limits.min_stat_date,
      maxStatDate: limits.max_stat_date,
    };
  }

  async removeForDate(date: string) {
    const qb = this.createQueryBuilder()
      .delete()
      .where(`${dateOnly('stat_date')} = :date`, {
        date,
      });
    qb.execute();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected async afterCalc(stats: any[], date: string) {
    return stats;
  }

  protected formatRow(row: any, statDate: string) {
    row[this.idField] = row[this.idField] ?? '0';

    const formatted = {
      ...emptyForumStatistic,
      ...camelizeKeys(row),
      statDate,
      isThread: this.byThreads,
    };

    return formatted;
  }

  private getRepository() {
    return this.entityManager.getRepository(this.entityType);
  }

  private createQueryBuilder() {
    return this.entityManager.createQueryBuilder(this.entityType, 'stat');
  }
}
