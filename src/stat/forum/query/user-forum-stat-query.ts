import { ForumMessages } from '@entities/forum-messages.entity';
import { SystemRole } from '@lib/types/enums';
import { QueryStatDto } from 'src/stat/dto/query-stat.dto';
import { SelectQueryBuilder } from 'typeorm';
import { ForumStatQueryBase, Sla } from './forum-stat-query.base';

export class UserForumStatQuery extends ForumStatQueryBase {
  constructor(byThreads: boolean, date: string, settings: Sla, filter?: QueryStatDto) {
    super(byThreads, date, settings, filter);
  }

  private groupByUser(qb: SelectQueryBuilder<ForumMessages>) {
    qb.addSelect('user.id as user_id, user.name as user_name').innerJoin('message.user', 'user').groupBy('user.id');

    return this.filterUsers(qb);
  }

  private groupByResolver(qb: SelectQueryBuilder<ForumMessages>) {
    qb.addSelect('user.id as user_id, user.name as user_name').leftJoin('message.resolvedUser', 'user').groupBy('user.id');

    return qb;
  }

  private filterUsers(qb: SelectQueryBuilder<ForumMessages>) {
    qb.innerJoin('user.roles', 'role').andWhere('role.role in (:...roles)', {
      roles: [SystemRole.Tutor, SystemRole.Teamlead, SystemRole.Support, SystemRole.Manager],
    });

    return qb;
  }

  protected groupSent(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByUser(qb);
  }
  protected groupResolved(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByResolver(qb);
  }
  protected groupUnresolved(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByResolver(qb);
  }
}
