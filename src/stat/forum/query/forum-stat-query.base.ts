import { ForumMessages } from '@entities/forum-messages.entity';
import { dateOnly } from 'src/query-helpers';
import { QueryStatDto } from 'src/stat/dto/query-stat.dto';
import { EntityManager, getManager, SelectQueryBuilder } from 'typeorm';
import { ForumStatQuery } from './forum-stat-query';
import * as dayjs from 'dayjs';
import { SystemRole } from '@lib/api/types';
import { addTeamleadFilter } from 'src/query-helpers/teamlead.helper';

export interface Sla {
  urgentHours: number;
  outdatedHours: number;
}

export abstract class ForumStatQueryBase implements ForumStatQuery {
  protected readonly countField: string;
  protected readonly em: EntityManager;
  protected readonly urgentDate: string;
  protected readonly outdatedDate: string;

  constructor(
    protected readonly byThreads: boolean,
    protected readonly date: string,
    protected readonly settings: Sla,
    protected readonly filter?: QueryStatDto,
  ) {
    this.countField = byThreads ? 'coalesce(message.thread_parent_id, message.id)' : 'message.id';

    this.em = getManager();

    this.urgentDate = dayjs(date).utc().subtract(settings.urgentHours, 'hour').toISOString();

    this.outdatedDate = dayjs(date).utc().subtract(settings.outdatedHours, 'hour').toISOString();
  }

  sentAll() {
    return this.sent('<=');
  }

  sentToday() {
    return this.sent('=');
  }

  private sent(op: string) {
    const qb = this.em
      .createQueryBuilder(ForumMessages, 'message')
      .select(`count(distinct ${this.countField})`, 'cnt')
      .where(`${dateOnly('message.created')} ${op} :date`, {
        date: this.date,
      });

    this.userFilter(qb);
    this.groupSent(qb);

    return qb;
  }

  resolvedAll() {
    return this.resolved('<=');
  }
  resolvedToday() {
    return this.resolved('=');
  }

  private resolved(op: string) {
    const qb = this.em
      .createQueryBuilder(ForumMessages, 'message')
      .select(`count(distinct ${this.countField})`, 'cnt')
      .where(
        `${dateOnly('message.created')} <= :createdMax and
         ${dateOnly('message.resolved')} ${op} :resolved and 
         message.resolved is not null
         and message.deleted is not null`,
        {
          createdMax: this.date,
          resolved: this.date,
        },
      );

    this.onlyThreadsFilter(qb);
    this.userFilter(qb);
    this.groupResolved(qb);

    return qb;
  }

  unresolvedAll() {
    return this.unresolved('<=');
  }
  unresolvedToday() {
    return this.unresolved('=');
  }

  private unresolved(op: string) {
    const qb = this.em
      .createQueryBuilder(ForumMessages, 'message')
      .select(`count(distinct ${this.countField})`, 'cnt')
      .where(
        `${dateOnly('message.created')} ${op} :created and
         (${dateOnly('message.resolved')} > :resolvedMin or 
         message.resolved is null)
         and message.deleted is null
         and message.skip_resolve = false
        `,
        {
          created: this.date,
          resolvedMin: this.date,
        },
      );

    this.onlyThreadsFilter(qb);
    this.userFilter(qb);
    this.groupUnresolved(qb);

    return qb;
  }

  urgent() {
    const qb = this.unresolvedAll() as SelectQueryBuilder<ForumMessages>;
    qb.andWhere(`coalesce(resolved, :currentDate) between :urgentDate and :outdatedDate`, {
      currentDate: this.date,
      urgentDate: this.urgentDate,
      outdatedDate: this.outdatedDate,
    });

    return qb;
  }
  outdated() {
    const qb = this.unresolvedAll() as SelectQueryBuilder<ForumMessages>;
    qb.andWhere(`coalesce(resolved, :currentDate) > :outdatedDate`, {
      currentDate: this.date,
      outdatedDate: this.outdatedDate,
    });

    return qb;
  }

  onlyThreadsFilter(qb: SelectQueryBuilder<ForumMessages>) {
    if (this.byThreads) {
      qb.andWhere('message.thread_parent_id is null');
    }
  }

  userFilter(qb: SelectQueryBuilder<ForumMessages>) {
    if (this.filter?.asRole === SystemRole.Teamlead) {
      addTeamleadFilter(qb, this.filter.userId, this.date, 'message.course_id = teamlead_enrollment.course_id');
    } else if (this.filter?.asRole === SystemRole.Tutor) {
      qb.andWhere('message.user_id = :userId', { userId: this.filter.userId });
    }
  }

  protected abstract groupSent(qb: SelectQueryBuilder<ForumMessages>);
  protected abstract groupResolved(qb: SelectQueryBuilder<ForumMessages>);
  protected abstract groupUnresolved(qb: SelectQueryBuilder<ForumMessages>);
}
