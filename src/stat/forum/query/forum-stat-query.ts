export interface ForumStatQuery {
  sentAll();
  sentToday();

  resolvedAll();
  resolvedToday();

  unresolvedAll();
  unresolvedToday();

  urgent();
  outdated();
}

export interface ForumStatConfig {
  urgentHours: number;
  outdatedHours: number;
}
