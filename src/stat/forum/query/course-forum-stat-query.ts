import { ForumMessages } from '@entities/forum-messages.entity';
import { QueryStatDto } from 'src/stat/dto/query-stat.dto';
import { SelectQueryBuilder } from 'typeorm';
import { ForumStatQueryBase, Sla } from './forum-stat-query.base';

export class CourseForumStatQuery extends ForumStatQueryBase {
  constructor(byThreads: boolean, date: string, settings: Sla, filter?: QueryStatDto) {
    super(byThreads, date, settings, filter);
  }

  private groupByCourse(qb: SelectQueryBuilder<ForumMessages>) {
    qb.addSelect('course.id as course_id, course.title as course_title').innerJoin('message.course', 'course').groupBy('course.id');

    return qb;
  }

  protected groupSent(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByCourse(qb);
  }
  protected groupResolved(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByCourse(qb);
  }
  protected groupUnresolved(qb: SelectQueryBuilder<ForumMessages>) {
    return this.groupByCourse(qb);
  }
}
