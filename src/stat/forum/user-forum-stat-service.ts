import { UserForumStatistic } from '@entities/user-forum-statistics.entity';
import { User } from '@entities/user.entity';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { getTeamleadTutors } from 'src/query-helpers/teamlead.helper';
import { getManager, In } from 'typeorm';
import { ForumStatService } from './forum-stat-service';
import { Sla } from './query/forum-stat-query.base';

export class UserForumStatService extends ForumStatService<UserForumStatistic> {
  constructor(byThreads: boolean, sliSettings: Sla) {
    super('user_id', false, byThreads, UserForumStatistic, sliSettings);
  }

  async afterCalc(stats: any[], date: Date | string) {
    const users = await getManager()
      .getRepository(User)
      .find({
        where: {
          id: In(stats.filter(s => s.userId !== '0').map(s => s.userId)),
        },
        relations: ['roles', 'modules', 'modules.course', 'enrollments', 'enrollments.course'],
      });

    for (const s of stats) {
      s.user = {
        ...{ enrollments: [], modules: [] },
        ...users.find(t => t.id === s.userId),
        teamleadTutors: await getTeamleadTutors(s.tutorId, date),
      };

      s.user.enrollments = s.user.enrollments.filter(e => e.role == CourseRole.Tutor);
    }

    return stats;
  }
}
