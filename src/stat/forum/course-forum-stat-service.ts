import { ForumStatService } from './forum-stat-service';
import { CourseForumStatistic } from '@entities/course-forum-statistics.entity';
import { Sla } from './query/forum-stat-query.base';

export class CourseForumStatService extends ForumStatService<CourseForumStatistic> {
  constructor(byThreads: boolean, sla: Sla) {
    super('course_id', true, byThreads, CourseForumStatistic, sla);
  }
}
