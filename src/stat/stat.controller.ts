import { SystemRole } from '@lib/api/types';
import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as dayjs from 'dayjs';
import { ForumGrouping } from '@lib/types/enums/forum-grouping.enum';
import { Request } from 'express';
import { CourseStatistic } from 'src/lib/entities/course.statistic.entity';
import { Permission } from 'src/lib/enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CourseSubmissionsStatService } from './submissions/course-submissions-stat.service';
import { QueryStatDto } from './dto/query-stat.dto';
import { QueryStatSummaryDto } from './dto/query-statSummary.dto';
import { TutorSubmissionsStatService } from './submissions/tutor-submissions-stat.service';
import { TutorAttendanceStatService } from './tutor-attendance-stat.service';
import { SubmissionStatService } from './submission-stat.service';
import { DashboardStatService } from './dashboard-stat.service';
import { ProgressStatService } from './progress-stat.service';
import { NpsStatService } from './nps-stat.service';
import { UserForumStatistic } from '@entities/user-forum-statistics.entity';
import { StatServiceManager } from './stat-service-manager';
import { CoursesProgressDistributionDto } from './dto/query-coursesProgressDistribution.dto';
import { QueryNpsStatDto } from './dto/query-npsStat.dto';
import { QueryByCoursesDto, QueryByCoursesGraduatesDto, QueryUsersByBracketDto } from './dto/query-courses.dto';

@ApiTags('statistic')
@ApiBearerAuth()
@Controller('stat')
export class StatController {
  constructor(
    private readonly coursesStatService: CourseSubmissionsStatService,
    private readonly tutorsStatService: TutorSubmissionsStatService,
    private readonly serviceManager: StatServiceManager,
    private readonly tutorAttendanceStatService: TutorAttendanceStatService,
    private readonly submissionStatService: SubmissionStatService,
    private readonly dashboardStatService: DashboardStatService,
    private readonly progressStatService: ProgressStatService,
    private readonly npsStatService: NpsStatService,
  ) {}

  @Get('courses')
  @RequirePermissions(Permission.STAT_VIEW)
  @ApiOperation({ summary: 'Submissions statisticts for courses' })
  @ApiResponse({
    status: 200,
    type: [CourseStatistic],
  })
  coursesStatistics(@Query() query: QueryStatDto = null, @Req() req: Request) {
    return this.getStatistics(query, req, {
      fieldsForQuery: ['courseId', 'periodStart', 'periodEnd'],
      service: this.serviceManager.submissionsByCourse,
    });
  }

  @Get('courses/summary')
  async coursesSummaryStatistics(@Query() query: QueryStatSummaryDto = null) {
    return this.serviceManager.submissionsByCourse.summary(query);
  }

  @Get('tutors')
  @RequirePermissions(Permission.STAT_VIEW)
  @ApiOperation({ summary: 'Submissions statisticts for tutors' })
  @ApiResponse({
    status: 200,
    type: [CourseStatistic],
  })
  async tutorsStatistics(@Query() query: QueryStatDto = null, @Req() req: Request) {
    return this.getStatistics(query, req, {
      fieldsForQuery: ['periodStart', 'periodEnd'],
      service: this.serviceManager.submissionsByTutor,
    });
  }

  @Get('tutors/summary')
  async tutorsSummaryStatistics(@Query() query: QueryStatSummaryDto = null, @Req() req: Request) {
    this.serviceManager.submissionsByTutor.setUser(req.user);
    return this.serviceManager.submissionsByTutor.summary(query);
  }

  @Get('forum/users/:grouping')
  @RequirePermissions(Permission.STAT_VIEW)
  @ApiOperation({ summary: 'Forum messages statisticts for users' })
  @ApiResponse({
    status: 200,
    type: [UserForumStatistic],
  })
  userForumStatistics(@Param('grouping') grouping: ForumGrouping, @Query() query: QueryStatDto = null, @Req() req: Request) {
    return this.getStatistics(query, req, {
      fieldsForQuery: ['periodStart', 'periodEnd'],
      service: this.serviceManager.getForumByUser(grouping),
    });
  }
  @Get('forum/users/:grouping/summary')
  async usersFoumStatisticsSummary(@Param('grouping') grouping: ForumGrouping, @Query() query: QueryStatSummaryDto = null) {
    const result = await this.serviceManager.getForumByUser(grouping).summary(query);
    return result;
  }
  @Get('forum/courses/:grouping')
  @RequirePermissions(Permission.STAT_VIEW)
  @ApiOperation({ summary: 'Forum messages statisticts for courses' })
  @ApiResponse({
    status: 200,
    type: [UserForumStatistic],
  })
  courseForumStatistics(@Param('grouping') grouping: ForumGrouping, @Query() query: QueryStatDto = null, @Req() req: Request) {
    return this.getStatistics(query, req, {
      fieldsForQuery: ['courseId', 'periodStart', 'periodEnd'],
      service: this.serviceManager.getForumByCourse(grouping),
    });
  }

  @Get('forum/courses/:grouping/summary')
  async coursesFoumStatisticsSummary(@Param('grouping') grouping: ForumGrouping, @Query() query: QueryStatSummaryDto = null) {
    return this.serviceManager.getForumByCourse(grouping).summary(query);
  }

  async getStatistics(
    query: QueryStatDto,
    req: Request,
    {
      service,
      fieldsForQuery,
    }: {
      service: any;
      fieldsForQuery: string[];
    },
  ) {
    const today = dayjs().startOf('day').utc(true).toISOString();

    if ([SystemRole.Teamlead, SystemRole.Tutor].includes(query.asRole)) {
      query.userId = req.user.id;
    }

    if (fieldsForQuery.some(f => query.hasOwnProperty(f))) {
      const dt = dayjs(query.periodStart).startOf('day').utc(true).toISOString();

      const isGradedToday = dt === today;

      if (isGradedToday) {
        return service.calculate(dt, false, query);
      }
      return service.query(dt, query);
    }

    return service.calculate(today, query);
  }

  @Get('forum/courses/:grouping/limits')
  forumCoursesLimits(@Param('grouping') grouping: ForumGrouping) {
    return this.serviceManager.getForumByCourse(grouping).limits();
  }

  @Get('forum/users/:grouping/limits')
  forumTutorsLimits(@Param('grouping') grouping: ForumGrouping) {
    return this.serviceManager.getForumByUser(grouping).limits();
  }

  @Get('tutors/attendance')
  @RequirePermissions(Permission.STAT_VIEW)
  tutorAttendance(@Param('grouping') grouping: ForumGrouping, @Query() query: QueryStatDto = null, @Req() req: Request) {
    return this.tutorAttendanceStatService.attendance(query.asRole, req.user.id);
  }

  @Get('dashboard')
  @RequirePermissions(Permission.STAT_VIEW)
  dashboard() {
    return this.dashboardStatService.dashboardStat();
  }

  @Post('courses-progress-distribution')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesProgressDistribution(@Body() params: CoursesProgressDistributionDto) {
    return this.progressStatService.progressStat(params.courses, params.tags);
  }

  @Post('courses-progress-distribution-users')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesProgressDistributionUsers(@Body() params: QueryUsersByBracketDto) {
    const DEFAULT_LIMIT = 50;
    const MAX_LIMIT = 1000;
    const DEFAULT_OFFSET = 0;

    const { bracket, courses, offset: rawOffset, limit: rawLimit, tags } = params;

    const limit = rawLimit ? Math.min(MAX_LIMIT, Math.max(1, rawLimit)) : DEFAULT_LIMIT;
    const offset = rawOffset ? Math.max(0, rawOffset) : DEFAULT_OFFSET;

    return this.progressStatService.progressDistributionUsersByBracket(courses, bracket, limit, offset, tags);
  }

  @Post('courses-no-progress')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsWithNoProgress(@Body() params: QueryByCoursesDto) {
    return this.progressStatService.noProgressStat(params.courses, params.tags);
  }

  @Post('courses-graduates')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsGraduates(@Body() params: QueryByCoursesDto) {
    return this.progressStatService.graduatesStat(params.courses, params.tags);
  }

  @Post('courses-with-submissions')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsWithSubmissions(@Body() params: QueryByCoursesDto) {
    return this.progressStatService.withSubmissionsStat(params.courses, params.tags);
  }

  @Post('courses-homeworks-lectures-views')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsHomeworksLecturesViews(@Body() params: QueryByCoursesDto) {
    return this.progressStatService.homeworkLectureViews(params.courses);
  }

  @Get('courses-homeworks-lectures-views-totals')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsHomeworksLecturesViewsTotals() {
    return this.progressStatService.homeworksViewsTotals();
  }

  @Post('courses-homeworks-spread-by-module')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsHomeworksSpreadPerModule(@Body() params: QueryByCoursesDto) {
    const { courses, minDate, maxDate, tags, activeState } = params;
    return this.progressStatService.homeworksSpreadPerModule(courses, minDate, maxDate, tags, activeState);
  }

  @Post('courses-homeworks-users-by-module')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsHomeworksUsersPerModule(@Body() params: QueryByCoursesDto) {
    const { courses, minDate, maxDate, tags, activeState } = params;
    return this.progressStatService.homeworksUsersPerModule(courses, minDate, maxDate, tags, activeState);
  }

  @Get('users-homeworks-by-course-module')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsUsersHomeworksByCourseModule(
    @Query('course') id: string,
    @Query('moduleId') moduleId: string,
    @Query('limit') rawLimit?: number,
    @Query('offset') rawOffset?: number,
    @Query('minDate') minDate?: string,
    @Query('maxDate') maxDate?: string,
    @Query('status') status?: string[],
    @Query('tags') tags?: string[],
    @Query('sortBy') sortBy: string = 'name',
    @Query('sortOrder') sortOrder: string = 'asc',
  ) {
    const DEFAULT_LIMIT = 50;
    const MAX_LIMIT = 1000;
    const DEFAULT_OFFSET = 0;

    const limit = rawLimit ? Math.min(MAX_LIMIT, Math.max(1, rawLimit)) : DEFAULT_LIMIT;
    const offset = rawOffset ? Math.max(0, rawOffset) : DEFAULT_OFFSET;

    return this.progressStatService.homeworksUsersByModule(
      id,
      moduleId,
      limit,
      offset,
      minDate,
      maxDate,
      status,
      tags,
      sortBy,
      sortOrder,
    );
  }

  @Get('users-progress-by-course-lecture')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsUsersProgressByCourseLecture(
    @Query('course') id: string,
    @Query('lecture') lectureId: string,
    @Query('limit') rawLimit?: number,
    @Query('offset') rawOffset?: number,
    @Query('minDate') minDate?: string,
    @Query('maxDate') maxDate?: string,
    @Query('status') status?: string,
    @Query('tags') tags?: string[],
  ) {
    const DEFAULT_LIMIT = 50;
    const MAX_LIMIT = 1000;
    const DEFAULT_OFFSET = 0;

    const limit = rawLimit ? Math.min(MAX_LIMIT, Math.max(1, rawLimit)) : DEFAULT_LIMIT;
    const offset = rawOffset ? Math.max(0, rawOffset) : DEFAULT_OFFSET;

    return this.progressStatService.progressUsersByLecture(id, lectureId, limit, offset, minDate, maxDate, status, tags);
  }

  @Post('users-graduates')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsUsersGraduates(@Body() params: QueryByCoursesGraduatesDto) {
    return this.progressStatService.usersGraduates(params);
  }

  @Post('courses-evaluations-by-lectures')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsEvaluationsPerLecture(@Body() params: QueryByCoursesDto) {
    const { courses, minDate, maxDate, tags } = params;
    return this.progressStatService.evaluationsByCourseLectures(courses, minDate, maxDate, tags);
  }

  @Get('courses/limits')
  coursesLimits() {
    return this.coursesStatService.limits();
  }

  @Get('tutors/limits')
  tutorsLimits() {
    return this.tutorsStatService.limits();
  }

  @Get('submissions')
  @RequirePermissions(Permission.STAT_VIEW)
  submissionsReport(@Query('from') periodFrom: string, @Query('to') periodTo: string) {
    return this.submissionStatService.getSubmissionsReport(periodFrom, periodTo);
  }

  @Get('courses-submission-success-rate')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesSubmissionSuccessRate(@Query('dateFrom') minDate?: string, @Query('dateTo') maxDate?: string) {
    return this.progressStatService.coursesSubmissionSuccessRate(minDate, maxDate);
  }

  @Post('nps')
  @RequirePermissions(Permission.STAT_VIEW)
  @ApiOperation({ summary: 'NPS statisticts' })
  @ApiResponse({
    status: 200,
  })
  npsStat(@Body() params: QueryNpsStatDto) {
    return this.npsStatService.npsStat(params);
  }

  @Post('courses-progress-spread-by-lecture')
  @RequirePermissions(Permission.STAT_VIEW)
  coursesStatsProgressSpreadPerLecture(@Body() params: QueryByCoursesDto) {
    const { courses, minDate, maxDate, activeState, tags } = params;
    return this.progressStatService.progressSpreadPerLecture(courses, minDate, maxDate, activeState, tags);
  }
}
