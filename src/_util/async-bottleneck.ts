export function asyncBottleneck(fn, concurrency = 1) {
  if (!Number.isInteger(concurrency)) {
    concurrency = 1;
  }
  const queue = [];
  let pending = 0;
  return async (...args) => {
    if (pending === concurrency) {
      await new Promise(resolve => queue.push(resolve));
    }

    pending++;

    return fn(...args).then(value => {
      pending--;
      queue.length && queue.shift()();
      return value;
    });
  };
}

export default function promisePool(dataArray, fn, concurrency = 1) {
  return Promise.all(dataArray.map(asyncBottleneck(fn, concurrency)));
}
