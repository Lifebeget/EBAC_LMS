export const createStats = () => {
  return {
    counter: 0,
    interval: undefined,

    start(time = 5000) {
      this.interval = setInterval(() => {
        console.log(`Processed: ${this.counter}`);
      }, time);
    },

    stop() {
      console.log(`Done: ${this.counter}`);
      clearInterval(this.interval);
    },
  };
};
