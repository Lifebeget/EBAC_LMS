import { InjectQueue, Process, Processor } from '@nestjs/bull';
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Job, Queue } from 'bull';
import { Request } from 'express';
import { CustomProcess } from 'src/decorators/custom-process.decorator';
import { AmoService } from './amo.service';

@Processor('amo-incoming-message')
@Injectable()
export class AmoIncomingMessageInterceptor implements NestInterceptor {
  constructor(@InjectQueue('amo-incoming-message') private amoIncomingMessageQueue: Queue, private readonly amoService: AmoService) {}
  async intercept(context: ExecutionContext, next: CallHandler) {
    if (context.getType() !== 'http') {
      return next.handle();
    }
    const req: Request = context.switchToHttp().getRequest();

    const _res = await this.amoIncomingMessageQueue.add(
      { body: req.body },
      {
        removeOnComplete: true,
        delay: 0,
      },
    );

    return next.handle();
  }

  @CustomProcess()
  async consumeAmoIncomingMessageQueue(job: Job<{ body: Record<string, any> }>) {
    const body = job.data.body;
    const res = await this.amoService.webhookIncommingMessage(body);
    return res;
  }
}
