import { Controller, Get, Post, Body, Req, BadRequestException, NotFoundException, Logger, UseInterceptors } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { PgConstraints } from '../pg.decorators';
import { AmoService } from './amo.service';
import { Amo } from '@entities/amo.entity';
import { Public } from '../auth/public.decorator';
import { AmoEmailInboxType } from '@enums/amo-email-inbox-type.enum';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { strict as assert } from 'assert';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { AmoIncomingMessageInterceptor } from './amo-incoming-message.interceptor';
import { SystemRole } from '@lib/types/enums';
import { getPipeline, getStatus, Pipeline, Status } from './amo.helpers';

@ApiTags('amo')
@ApiBearerAuth()
@Controller('amo')
export class AmoController {
  private logger = new Logger(AmoController.name);
  constructor(private readonly amoService: AmoService, private jwtService: JwtService) {}

  @Get('token')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Show local token' })
  @ApiResponse({
    status: 200,
    type: Amo,
  })
  findToken() {
    return this.amoService.getLocalToken();
  }

  @Post('code')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Post auth code and save token' })
  @ApiResponse({
    status: 201,
    description: 'Created token',
    type: Amo,
  })
  @PgConstraints()
  authCode(@Body() body: { code: string }) {
    return this.amoService.authCode(body.code);
  }

  @Get('get-users')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Get imported amo users' })
  async getUsers() {
    return this.amoService.getUsers();
  }

  @Get('test-lead')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Create test lead' })
  @ApiResponse({
    status: 200,
    type: Amo,
  })
  async testLead() {
    return this.amoService.createTestLead();
  }

  @Get('show-amo-users')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Test get amo users' })
  async testGetUsers() {
    return this.amoService.getAmoUsers();
  }

  @Get('import-users')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Test import amo users' })
  async testImportUsers() {
    return this.amoService.importAmoUsers();
  }

  @Public()
  @Post('webhook/profile-note')
  @ApiOperation({ summary: 'Create note for new leads' })
  @ApiResponse({
    status: 200,
  })
  webhookAmoProfileNoteHandler(@Body() body: any) {
    return this.amoService.webhookHandler(body);
  }

  @Public()
  @Post('webhook/incomming-message')
  @ApiOperation({ summary: 'Process incoming whatsapp message' })
  @ApiResponse({
    status: 200,
  })
  @UseInterceptors(AmoIncomingMessageInterceptor)
  webhookAmoIncomingMessageHandler(@Body() _body: any) {
    return true;
  }

  @Public()
  @Post('webhook/set-student-responsible')
  @ApiOperation({ summary: 'Set student support pipeline responsible' })
  @ApiResponse({
    status: 200,
  })
  webhookAmoStudentSupportResponsible(@Body() body: any) {
    this.amoService.webhookSetResponsible({
      body: body,
      role: SystemRole.Support,
      fromStatus: getStatus(Status.STUDENT_SUPPORT_SS_MANAGER_NOT_ASSIGNED),
      toStatus: getStatus(Status.STUDENT_SUPPORT_SS_MANAGER_ASSIGNED),
      pipeline: getPipeline(Pipeline.STUDENT_SUPPORT),
    });
    return true;
  }

  @Public()
  @Post('webhook/set-icg-responsible')
  @ApiOperation({ summary: 'Set ICG pipeline responsible' })
  @ApiResponse({
    status: 200,
  })
  webhookAmoICGResponsible(@Body() body: any) {
    this.amoService.webhookSetResponsible({
      body: body,
      role: SystemRole.ICGSupport,
      fromStatus: getStatus(Status.ICG_MANAGER_NOT_ASSIGNED),
      toStatus: getStatus(Status.ICG_MANAGER_ASSIGNED),
      pipeline: getPipeline(Pipeline.ICG_STUDENT_SUPPORT),
    });
    return true;
  }

  @Public()
  @Post('webhook/close-incident')
  @ApiOperation({ summary: 'Close parent leads by incident' })
  @ApiResponse({
    status: 200,
  })
  webhookAmoCloseIncidentHandler(@Body() body: any) {
    return this.amoService.webhookCloseParentLeads(body);
  }

  @Public()
  @Post('email/support')
  @ApiOperation({ summary: 'Create lead from email to support' })
  @ApiResponse({
    status: 200,
  })
  emailSupport(@Body() body: any) {
    return this.amoService.emailHandler(AmoEmailInboxType.support, body);
  }

  @Public()
  @Post('email/tutor-support')
  @ApiOperation({ summary: 'Create lead from email to tutor support' })
  @ApiResponse({
    status: 200,
  })
  emailTutorSupport(@Body() body: any) {
    return this.amoService.emailHandler(AmoEmailInboxType.tutorSupport, body);
  }

  @Public()
  @Post('email/sales')
  @ApiOperation({ summary: 'Create lead from email to sales' })
  @ApiResponse({
    status: 200,
  })
  emailSales(@Body() body: any) {
    return this.amoService.emailHandler(AmoEmailInboxType.sales, body);
  }

  @Public()
  @Post('email/test')
  @ApiOperation({
    summary: 'Create lead from email to support in test pipeline',
  })
  @ApiResponse({
    status: 200,
  })
  emailTestSupport(@Body() body: any) {
    return this.amoService.emailHandler(AmoEmailInboxType.test, body);
  }

  @Public()
  @Post('usedesk')
  @ApiOperation({ summary: 'Creates lead/note from usedesk webhook payload' })
  postUsedesk(@Body() body: any) {
    this.amoService.createUsedeskLead(body);
  }

  @Public()
  @Get('support-profile')
  async getAuth(@Req() req: Request) {
    this.logger.log('Handling support profile');
    const amoToken = req.headers['x-auth-token'] as string;
    const amoLeadId = req.query.amoLeadId as string;
    const amoContactId = req.query.amoContactId as string;
    assert.ok(amoToken, new BadRequestException('amoToken required'));
    assert.ok(amoLeadId || amoContactId, new BadRequestException('amoLeadId or amoContactId required'));

    this.logger.log(`Getting viewing user`);
    const viewingUser = await this.amoService.getUserByAmoToken(amoToken);
    assert.ok(viewingUser, 'viewingUser not found');
    this.logger.log(`Got viewing user ${viewingUser.id} (${viewingUser.email})`);

    this.logger.log(`Getting viewed user`);
    const viewedUser = amoContactId
      ? await this.amoService.getUserByAmoContactId(amoContactId)
      : await this.amoService.getUserByAmoLeadId(amoLeadId);
    assert.ok(viewedUser, new NotFoundException(`viewedUser not found: ${amoLeadId}`));
    this.logger.log(`Got viewed user ${viewedUser.id}`);

    return {
      lmsToken: this.jwtService.sign({
        email: viewingUser.email,
        sub: viewingUser.id,
      }),
      lmsProfileId: viewedUser.id,
    };
  }

  @Post('get-responsible-user-id')
  @Public()
  async getResponsibleUserId(@Body() body) {
    const responsibleUserId = await this.amoService.findResponsible(body);
    return { responsibleUserId };
  }
}
