import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { AmoService } from './amo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Amo } from '@entities/amo.entity';
import { AmoUser } from '@entities/amo-user.entity';
import { User } from '@entities/user.entity';
import { AmoController } from './amo.controller';
import { SettingsModule } from '../settings/settings.module';
import { UsersModule } from '../users/users.module';
import { Enrollment } from '@entities/enrollment.entity';
import { AmoUserEvent } from '@entities/amo-user-event.entity';
import { AmoEventsService } from './amo-events.service';
import { JwtModule } from '@nestjs/jwt';
import { BullModule } from '@nestjs/bull';
import { AmoIncomingMessageInterceptor } from './amo-incoming-message.interceptor';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Amo]),
    TypeOrmModule.forFeature([AmoUser]),
    TypeOrmModule.forFeature([AmoUserEvent]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Enrollment]),
    SettingsModule,
    BullModule.registerQueue({
      name: 'amo-incoming-message',
    }),
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.JWT_TOKEN_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRATION_TIME,
        },
      }),
    }),
    forwardRef(() => UsersModule),
    HttpModule,
    ConfigModule,
  ],
  controllers: [AmoController],
  exports: [AmoService, AmoEventsService],
  providers: [AmoService, AmoEventsService, AmoIncomingMessageInterceptor],
})
export class AmoModule {}
