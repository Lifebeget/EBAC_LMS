import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AmoUserEvent } from '@entities/amo-user-event.entity';
import { Repository } from 'typeorm';
import { QueryAmoEventsDto } from './dto/query-amo-events.dto';
import { CreateAmoEventsDto } from './dto/create-amo-events.dto';
import { UpdateAmoEventsDto } from './dto/update-amo-events.dto';

@Injectable()
export class AmoEventsService {
  constructor(
    @InjectRepository(AmoUserEvent)
    private readonly amoUserEventRepository: Repository<AmoUserEvent>,
  ) {}

  async findAll(query: QueryAmoEventsDto): Promise<AmoUserEvent[]> {
    const qb = this.amoUserEventRepository.createQueryBuilder('event');

    qb.andWhere('event.user.id = :userId', { userId: query.userId });
    qb.andWhere('event.eventType = :eventType', { eventType: query.eventType });
    if (query.courseId) {
      qb.andWhere('event.course.id = :courseId', { courseId: query.courseId });
    }

    return qb.getMany();
  }

  async findById(id: string) {
    const qb = await this.amoUserEventRepository.createQueryBuilder('event');
    qb.andWhere('event.id = :id', { id });
    return qb.getOne();
  }

  async findOne(query: QueryAmoEventsDto): Promise<AmoUserEvent> {
    const qb = await this.amoUserEventRepository.createQueryBuilder('event');
    qb.andWhere('event.user.id = :userId', { userId: query.userId });
    qb.andWhere('event.eventType = :eventType', { eventType: query.eventType });
    if (query.courseId) {
      qb.andWhere('event.course.id = :courseId', { courseId: query.courseId });
    }
    return qb.getOne();
  }

  async create(createAmoEventsDto: CreateAmoEventsDto) {
    const createFields = {
      ...createAmoEventsDto,
      course: createAmoEventsDto.courseId ? { id: createAmoEventsDto.courseId } : null,
      user: createAmoEventsDto.userId ? { id: createAmoEventsDto.userId } : null,
    };

    return this.amoUserEventRepository.save(createFields);
  }

  update(id: string, updateAmoEventsDto: UpdateAmoEventsDto) {
    const updateFields = { ...updateAmoEventsDto, id } as any;
    return this.amoUserEventRepository.save(updateFields);
  }

  remove(id: string) {
    return this.amoUserEventRepository.delete(id);
  }
}
