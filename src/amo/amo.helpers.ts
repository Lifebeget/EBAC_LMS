import config from 'config';

export enum Pipeline {
  INCOMING_CHANNELS,
  CALL_CENTER,
  STUDENT_SUPPORT,
  TUTOR_SUPPORT,
  LMS_BUG,
  TEST,
  COMPLAINTS,
  QA,
  EDU_TEAM,
  PRODUCTION_BUGS,
  THIRD_PARTIES,
  B2B,
  GRADUATION,
  SALES_PIPELINE_V1,
  ICG_STUDENT_SUPPORT,
}

const PipelinesBR: Record<Pipeline, number> = {
  [Pipeline.INCOMING_CHANNELS]: 4562134,
  [Pipeline.CALL_CENTER]: 4652041,
  [Pipeline.STUDENT_SUPPORT]: 4562218,
  [Pipeline.TUTOR_SUPPORT]: 4849057,
  [Pipeline.LMS_BUG]: 4730644,
  [Pipeline.TEST]: 4701826,
  [Pipeline.COMPLAINTS]: 4760998,
  [Pipeline.QA]: 4730608,
  [Pipeline.EDU_TEAM]: 4730611,
  [Pipeline.PRODUCTION_BUGS]: 4730614,
  [Pipeline.THIRD_PARTIES]: 4730665,
  [Pipeline.B2B]: 4899785,
  [Pipeline.GRADUATION]: 4899872,
  [Pipeline.SALES_PIPELINE_V1]: 4700614,
  [Pipeline.ICG_STUDENT_SUPPORT]: 5286005,
};

const PipelinesMX: Record<Pipeline, number> = {
  [Pipeline.INCOMING_CHANNELS]: 4672480, // not exist, so we use support
  [Pipeline.CALL_CENTER]: 4672480, // not exist, so we use support
  [Pipeline.STUDENT_SUPPORT]: 4672480,
  [Pipeline.TUTOR_SUPPORT]: 4916677,
  [Pipeline.LMS_BUG]: 4912183,
  [Pipeline.TEST]: 4672480,
  [Pipeline.COMPLAINTS]: 4672480, // not exist, so we use support
  [Pipeline.QA]: 4672480, // not exist, so we use support
  [Pipeline.EDU_TEAM]: 4672480, // not exist, so we use support
  [Pipeline.PRODUCTION_BUGS]: 4912402,
  [Pipeline.THIRD_PARTIES]: 4672480, // not exist, so we use support
  [Pipeline.B2B]: 4672480, // not exist, so we use support
  [Pipeline.GRADUATION]: 4672480, // not exist, so we use support
  [Pipeline.SALES_PIPELINE_V1]: 4672480, // not exist, so we use support
  [Pipeline.ICG_STUDENT_SUPPORT]: 4672480, // not exist, so we use support
};

export enum Status {
  INCOMING_CHANNELS_REPEATED_WA_REQUESTS,
  INCOMING_CHANNELS_IC_MANAGER_NEEDED,
  CALL_CENTER_INBOX,
  CALL_CENTER_FIRST_MESSAGE,
  CALL_CENTER_INCIDENT_RESOLVED,
  STUDENT_SUPPORT_INBOX,
  STUDENT_SUPPORT_INCIDENT_RESOLVED,
  STUDENT_SUPPORT_REPEATED_REQUESTS,
  STUDENT_SUPPORT_SS_MANAGER_NOT_ASSIGNED,
  STUDENT_SUPPORT_SS_MANAGER_ASSIGNED,
  ICG_STUDENT_SUPPORT_REPEATED_REQUESTS,
  TUTOR_SUPPORT_REPEATED_REQUESTS,
  TUTOR_SUPPORT_INBOX,
  TUTOR_SUPPORT_INCIDENT_RESOLVED,
  LMS_BUG_INBOX,
  LMS_BUG_INCIDENT_RESOLVED,
  TEST_INBOX,
  COMPLAINTS_INBOX,
  COMPLAINTS_INCIDENT_RESOLVED,
  QA_INBOX,
  QA_INCIDENT_RESOLVED,
  EDU_TEAM_INBOX,
  EDU_TEAM_INCIDENT_RESOLVED,
  PRODUCTION_BUGS_INBOX,
  PRODUCTION_BUGS_INCIDENT_RESOLVED,
  THIRD_PARTIES_INBOX,
  THIRD_PARTIES_INCIDENT_RESOLVED,
  SALES_PIPELINE_REQUEST,
  SUCCESS,
  LOST,
  ICG_MANAGER_NOT_ASSIGNED,
  ICG_MANAGER_ASSIGNED,
}

const StatusesBR: Record<Status, number> = {
  [Status.INCOMING_CHANNELS_REPEATED_WA_REQUESTS]: 45871244,
  [Status.INCOMING_CHANNELS_IC_MANAGER_NEEDED]: 42037054,
  [Status.CALL_CENTER_INBOX]: 42686440,
  [Status.CALL_CENTER_FIRST_MESSAGE]: 43440094,
  [Status.CALL_CENTER_INCIDENT_RESOLVED]: 43366768,
  [Status.STUDENT_SUPPORT_INBOX]: 42506315,
  [Status.STUDENT_SUPPORT_INCIDENT_RESOLVED]: 43366765,
  [Status.STUDENT_SUPPORT_REPEATED_REQUESTS]: 42824147,
  [Status.STUDENT_SUPPORT_SS_MANAGER_NOT_ASSIGNED]: 42506315,
  [Status.STUDENT_SUPPORT_SS_MANAGER_ASSIGNED]: 42747049,
  [Status.ICG_STUDENT_SUPPORT_REPEATED_REQUESTS]: 47126045,
  [Status.TUTOR_SUPPORT_REPEATED_REQUESTS]: 44759366,
  [Status.TUTOR_SUPPORT_INBOX]: 44041243,
  [Status.TUTOR_SUPPORT_INCIDENT_RESOLVED]: 44041270,
  [Status.LMS_BUG_INBOX]: 43244590,
  [Status.LMS_BUG_INCIDENT_RESOLVED]: 43366786,
  [Status.TEST_INBOX]: 43043224,
  [Status.COMPLAINTS_INBOX]: 43461940,
  [Status.COMPLAINTS_INCIDENT_RESOLVED]: 43366771,
  [Status.QA_INBOX]: 43244386,
  [Status.QA_INCIDENT_RESOLVED]: 43366771,
  [Status.EDU_TEAM_INBOX]: 43244398,
  [Status.EDU_TEAM_INCIDENT_RESOLVED]: 43366774,
  [Status.PRODUCTION_BUGS_INBOX]: 43244410,
  [Status.PRODUCTION_BUGS_INCIDENT_RESOLVED]: 43366777,
  [Status.THIRD_PARTIES_INBOX]: 43244716,
  [Status.THIRD_PARTIES_INCIDENT_RESOLVED]: 43366789,
  [Status.SALES_PIPELINE_REQUEST]: 43034383,
  [Status.SUCCESS]: 142,
  [Status.LOST]: 143,
  [Status.ICG_MANAGER_NOT_ASSIGNED]: 47126042, // TODO change to ICG SS status
  [Status.ICG_MANAGER_ASSIGNED]: 47126246, // TODO change to ICG SS status
};

const StatusesMX: Record<Status, number> = {
  [Status.INCOMING_CHANNELS_REPEATED_WA_REQUESTS]: 42832042, // not exist, so we use support
  [Status.INCOMING_CHANNELS_IC_MANAGER_NEEDED]: 42832042, // not exist, so we use support
  [Status.CALL_CENTER_INBOX]: 42832042, // not exist, so we use support
  [Status.CALL_CENTER_FIRST_MESSAGE]: 42832042, // not exist, so we use support
  [Status.CALL_CENTER_INCIDENT_RESOLVED]: 44461348, // not exist, so we use support
  [Status.STUDENT_SUPPORT_INBOX]: 42832042,
  [Status.STUDENT_SUPPORT_INCIDENT_RESOLVED]: 44461348,
  [Status.STUDENT_SUPPORT_REPEATED_REQUESTS]: 42832042, // not exist, so we use support
  [Status.STUDENT_SUPPORT_SS_MANAGER_NOT_ASSIGNED]: 42832042, // not exist, so we use support
  [Status.STUDENT_SUPPORT_SS_MANAGER_ASSIGNED]: 42832042, // not exist, so we use support
  [Status.ICG_STUDENT_SUPPORT_REPEATED_REQUESTS]: 42832042, // not exist, so we use support
  [Status.TUTOR_SUPPORT_REPEATED_REQUESTS]: 42832042, // not exist, so we use support
  [Status.TUTOR_SUPPORT_INBOX]: 44482882,
  [Status.TUTOR_SUPPORT_INCIDENT_RESOLVED]: 44483122,
  [Status.LMS_BUG_INBOX]: 44450188,
  [Status.LMS_BUG_INCIDENT_RESOLVED]: 44450275,
  [Status.TEST_INBOX]: 42832042, // not exist, so we use support
  [Status.COMPLAINTS_INBOX]: 42832042, // not exist, so we use support
  [Status.COMPLAINTS_INCIDENT_RESOLVED]: 44461348, // not exist, so we use support
  [Status.QA_INBOX]: 42832042, // not exist, so we use support
  [Status.QA_INCIDENT_RESOLVED]: 44461348, // not exist, so we use support
  [Status.EDU_TEAM_INBOX]: 42832042, // not exist, so we use support
  [Status.EDU_TEAM_INCIDENT_RESOLVED]: 44461348, // not exist, so we use support
  [Status.PRODUCTION_BUGS_INBOX]: 44451850,
  [Status.PRODUCTION_BUGS_INCIDENT_RESOLVED]: 44452114,
  [Status.THIRD_PARTIES_INBOX]: 42832042, // not exist, so we use support
  [Status.THIRD_PARTIES_INCIDENT_RESOLVED]: 44461348, // not exist, so we use support
  [Status.SALES_PIPELINE_REQUEST]: 42832042, // not exist, so we use support
  [Status.SUCCESS]: 142,
  [Status.LOST]: 143,
  [Status.ICG_MANAGER_NOT_ASSIGNED]: 0, // not exists
  [Status.ICG_MANAGER_ASSIGNED]: 0, // not exists
};

export enum CustomField {
  CONTACT_EMAIL,
  CONTACT_LAST_MESSAGE_TIME,
  LEAD_INCIDENT_PARENT_ID,
  CC_COURSE_NAME,
  CC_NUMBER_OF_MONTHS,
  CC_NUMBER_OF_MODULES,
  CC_LAST_ACCESSED,
  CC_ENROLLMENT_ID,
}

const CustomFieldsBR: Record<CustomField, number> = {
  [CustomField.CONTACT_EMAIL]: 927005,
  [CustomField.CONTACT_LAST_MESSAGE_TIME]: 1032961,
  [CustomField.LEAD_INCIDENT_PARENT_ID]: 1029813,
  [CustomField.CC_COURSE_NAME]: 1028481,
  [CustomField.CC_NUMBER_OF_MONTHS]: 1028483,
  [CustomField.CC_NUMBER_OF_MODULES]: 1028485,
  [CustomField.CC_LAST_ACCESSED]: 1028881,
  [CustomField.CC_ENROLLMENT_ID]: 1037372,
};
const CustomFieldsMX: Record<CustomField, number> = {
  [CustomField.CONTACT_EMAIL]: 63719,
  [CustomField.CONTACT_LAST_MESSAGE_TIME]: 0,
  [CustomField.LEAD_INCIDENT_PARENT_ID]: 0,
  [CustomField.CC_COURSE_NAME]: 1375814,
  [CustomField.CC_NUMBER_OF_MONTHS]: 1375816,
  [CustomField.CC_NUMBER_OF_MODULES]: 1375818,
  [CustomField.CC_LAST_ACCESSED]: 0,
  [CustomField.CC_ENROLLMENT_ID]: 1376526,
};

export enum Tag {
  PHONE_ONBOARDING,
}

const TagsBR: Record<Tag, number> = {
  [Tag.PHONE_ONBOARDING]: 61315,
};
const TagsMX: Record<Tag, number> = {
  [Tag.PHONE_ONBOARDING]: 86740,
};

export enum Identity {
  MAGIC_HELPER,
}

const IdentitiesBR: Record<Identity, number> = {
  [Identity.MAGIC_HELPER]: 7346836,
};

const IdentitiesMX: Record<Identity, number> = {
  [Identity.MAGIC_HELPER]: 7378054,
};

export enum Responsibility {
  COURSE_ALMOST_COMPLETED,
  COURSE_NOT_STARTED,
  ONBOARDING,
  STOPPED_LEARNING,
}

// Using emails, because they are shared between lms/amo users
export enum Responsible {
  LUCAS_BRITO = 'lucas.brito@ebac.art.br',
  BIANCA_VIOLA = 'bianca.viola@ebac.art.br',
  PAULO_MACHADO = 'paulo.machado@ebac.art.br',
  AIRA_TOMAS = 'aira.tomaz@ebac.art.br',
  ALESSANDRA_DIAS = 'alessandra.dias@ebac.art.br',
  VITORIA_ALMEIDA = 'vitoria.almeida@ebac.art.br',
  ESTEFANI_DE_ASSIS = 'estefani.assis@ebac.art.br',
  NATHALIA_MAGALHAES = 'nathalia.magalhaes@ebac.art.br',
}

const emailsByResponsibility: Record<Responsibility, Responsible[]> = {
  [Responsibility.COURSE_ALMOST_COMPLETED]: [
    //
    Responsible.LUCAS_BRITO,
  ],
  [Responsibility.COURSE_NOT_STARTED]: [Responsible.BIANCA_VIOLA, Responsible.ALESSANDRA_DIAS],
  [Responsibility.ONBOARDING]: [Responsible.VITORIA_ALMEIDA, Responsible.ESTEFANI_DE_ASSIS, Responsible.NATHALIA_MAGALHAES],
  [Responsibility.STOPPED_LEARNING]: [Responsible.LUCAS_BRITO, Responsible.PAULO_MACHADO, Responsible.AIRA_TOMAS],
};

function getPipeline(pipeline: Pipeline) {
  switch (config.language) {
    case 'es':
      return PipelinesMX[pipeline];
    case 'pt':
    default:
      return PipelinesBR[pipeline];
  }
}

function getStatus(status: Status) {
  switch (config.language) {
    case 'es':
      return StatusesMX[status];
    case 'pt':
    default:
      return StatusesBR[status];
  }
}

function getCustomField(customField: CustomField) {
  switch (config.language) {
    case 'es':
      return CustomFieldsMX[customField];
    case 'pt':
    default:
      return CustomFieldsBR[customField];
  }
}

function getTag(tag: Tag) {
  switch (config.language) {
    case 'es':
      return TagsMX[tag];
    case 'pt':
    default:
      return TagsBR[tag];
  }
}

function getIdentity(identity: Identity) {
  switch (config.language) {
    case 'es':
      return IdentitiesMX[identity];
    case 'pt':
    default:
      return IdentitiesBR[identity];
  }
}

function getEmailByResponsibility(responsibility: Responsibility) {
  switch (config.language) {
    case 'es':
      return 0;
    case 'pt':
    default:
      return emailsByResponsibility[responsibility];
  }
}

function getSuccessIncidentStatus(pipeline: Pipeline) {
  switch (config.language) {
    case 'es':
      return StatusesMX[Status.SUCCESS];
    case 'pt':
    default:
      switch (pipeline) {
        case PipelinesBR[Pipeline.CALL_CENTER]:
          return StatusesBR[Status.CALL_CENTER_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.STUDENT_SUPPORT]:
          return StatusesBR[Status.STUDENT_SUPPORT_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.TUTOR_SUPPORT]:
          return StatusesBR[Status.TUTOR_SUPPORT_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.LMS_BUG]:
          return StatusesBR[Status.LMS_BUG_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.COMPLAINTS]:
          return StatusesBR[Status.COMPLAINTS_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.QA]:
          return StatusesBR[Status.QA_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.EDU_TEAM]:
          return StatusesBR[Status.EDU_TEAM_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.PRODUCTION_BUGS]:
          return StatusesBR[Status.PRODUCTION_BUGS_INCIDENT_RESOLVED];
        case PipelinesBR[Pipeline.THIRD_PARTIES]:
          return StatusesBR[Status.THIRD_PARTIES_INCIDENT_RESOLVED];
        default:
          return StatusesBR[Status.SUCCESS];
      }
  }
}

export {
  //
  getPipeline,
  getStatus,
  getCustomField,
  getTag,
  getIdentity,
  getEmailByResponsibility,
  getSuccessIncidentStatus,
};
