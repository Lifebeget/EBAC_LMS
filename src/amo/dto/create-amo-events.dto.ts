import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, ValidateIf } from 'class-validator';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';

export class CreateAmoEventsDto {
  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  @ApiProperty({ description: 'User id' })
  userId?: string;

  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  @ApiProperty({ description: 'Course id' })
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('eventType'))
  @ApiProperty({ description: 'Event type' })
  eventType: AmoUserEventType;

  @ApiProperty({ description: 'Successfully created in AmoCRM' })
  success?: boolean;

  @ApiProperty({
    description: 'Time of event creation in AmoCRM',
  })
  sendTime?: Date;

  @ApiProperty({
    description: 'AmoCRM lead id',
  })
  amoId?: string;

  @ApiProperty({
    description: 'Additional event data',
  })
  data?: Record<string, unknown>;

  @ApiProperty({
    description: 'Response event data',
  })
  responseData?: any;
}
