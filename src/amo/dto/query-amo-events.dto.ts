import { ApiProperty } from '@nestjs/swagger';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';

export class QueryAmoEventsDto {
  @ApiProperty({ description: 'Filter by user' })
  userId: string;

  @ApiProperty({ description: 'Filter by event type' })
  eventType: AmoUserEventType;

  @ApiProperty({ description: 'Filter by course', required: false })
  courseId?: string;
}
