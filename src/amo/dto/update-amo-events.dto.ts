import { PartialType } from '@nestjs/swagger';
import { CreateAmoEventsDto } from './create-amo-events.dto';

export class UpdateAmoEventsDto extends PartialType(CreateAmoEventsDto) {}
