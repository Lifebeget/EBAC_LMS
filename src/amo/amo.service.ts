import { InjectRepository } from '@nestjs/typeorm';
import { forwardRef, HttpException, HttpService, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Amo } from '@entities/amo.entity';
import { AmoUser } from '@entities/amo-user.entity';
import { User } from '@entities/user.entity';
import { Role } from '@entities/role.entity';
import { Enrollment } from '@entities/enrollment.entity';
import { SettingsService } from '../settings/settings.service';
import { SupportService } from 'src/users/support.service';
import { UsersService } from 'src/users/users.service';
import { ProfileService } from '../users/profile.service';
import * as dayjs from 'dayjs';
import * as AmoCRM from 'amocrm-js';
import { SystemRole } from '@lib/api/types';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import { dbgLog, insertIntoString, removeFromString } from '../helpers';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { UsedeskChannel } from 'src/usedesk/usedesk-channel.enum';
import { UsedeskWebhookData } from 'src/usedesk/usedesk-webhook-data.interface';
import { AmoEmailInboxType } from '@enums/amo-email-inbox-type.enum';
import * as jwt from 'jsonwebtoken';
import { strict as assert, strict } from 'assert';
import {
  CustomField,
  getCustomField,
  getEmailByResponsibility,
  getIdentity,
  getPipeline,
  getStatus,
  getTag,
  getSuccessIncidentStatus,
  Identity,
  Pipeline,
  Responsibility,
  Status,
  Tag,
} from './amo.helpers';
import config from 'config';
import { whatsappPhrases } from './db/whatsapp-phrases';
import { MailerService } from '@nestjs-modules/mailer';
import { Profile } from '@entities/profile.entity';
import { ConfigService } from '@nestjs/config';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Course } from '@entities/course.entity';
import { OnboardingCallEvent } from 'src/trigger/events/enrollment-created.event';

dayjs.extend(customParseFormat);

interface CreateLead {
  pipelineId: number;
  statusId: number;
  name: string;
  contact: any;
  responsible?: number;
  customFields?: { id: number; value: any }[];
  tags?: number[];
}

@Injectable()
export class AmoService {
  crm = null;
  crmHandlerUpdate = null;
  token = null;
  phoneOnboardingTagId = 61315;
  courseAlmostCompletedTagId = 86815;
  courseCompletedTagId = 86817;
  stoppedLearningTagId = 88175;
  courseNotStartedTagId = 88297;
  emailMessageTagId = 90011;
  usedeskTagId = 89685;
  CFContactsPhone = 927003;
  CFContactsEmail = 927005;
  CFContactsLMSUserID = 1037368;
  CFNeedsExercise = 1028875;
  CFNotWatchedVideo = 1028877;
  CFNotPassed = 1028879;
  CFMailThreadId = 1030943;
  CFMailNameId = 1031463;
  CFMailSubjectId = 1031183;
  CFMailBodyId = 1031185;
  CFContactStoppedLearningIncidentCount = 1036886;
  createNewWhatsappLeadAfterHours = 24;
  messageRetries: Record<string, number> = {};

  constructor(
    @InjectRepository(Amo)
    private readonly amoRepository: Repository<Amo>,
    @InjectRepository(AmoUser)
    private readonly amoUserRepository: Repository<AmoUser>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Enrollment)
    private readonly enrollmentsRepository: Repository<Enrollment>,
    private readonly settingsService: SettingsService,
    private readonly profileService: ProfileService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly mailerService: MailerService,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    private readonly supportService: SupportService,
  ) {
    this.token = null;
    this.crmHandlerUpdate = null;
    this.tokenUpdateCycle();
  }

  async initCrm() {
    // skip init if not production
    if (!['lms-production', 'lms-production-mx'].includes(process.env.ENVIRONMENT)) {
      return false;
    }
    const settings = await this.settingsService.getSettings();
    if (settings.integrationSettings.amo && settings.integrationSettings.amo.domain) {
      try {
        this.crm = new AmoCRM({
          domain: settings.integrationSettings.amo.domain,
          auth: {
            client_id: settings.integrationSettings.amo.clientId,
            client_secret: settings.integrationSettings.amo.clientSecret,
            redirect_uri: settings.integrationSettings.amo.redirectUri,
          },
        });
      } catch (e) {
        console.error('AmoCRM connect error: ', e);
      }
    }
  }

  async getLocalToken() {
    return this.amoRepository.findOne({ type: 'token' });
  }

  async setLocalToken(data) {
    let res;
    const extToken = await this.getLocalToken();
    if (extToken) {
      res = await this.amoRepository.update({ type: 'token' }, { data });
    } else {
      res = await this.amoRepository.save({ type: 'token', data });
    }
    return res;
  }

  async authCode(code) {
    try {
      await this.initCrm();
      const res = await this.crm.connection.setCode(code);
      const token = this.crm.connection.getToken();
      await this.setLocalToken(token);
      await this.setToken();
      return res;
    } catch (e) {
      console.log('amo auth code error:', e);
    }
    return false;
  }

  async setToken() {
    const localToken = await this.getLocalToken();
    if (localToken?.data) {
      await this.crm.connection.handleToken(localToken);
      this.token = localToken.data;
    }
  }

  generatePhoneVariants(phone: string) {
    if (!phone) {
      return [];
    }
    const phoneVariants = [];
    let temp = '';
    const phoneNumbers = phone.replace(/[^0-9]+/g, '');
    temp = insertIntoString(phoneNumbers, 4, ' ');
    const phoneWithSpaces = insertIntoString(temp, 2, ' ');
    let phoneWithSpacesDash = '';
    if (phoneNumbers.length === 12) {
      phoneWithSpacesDash = insertIntoString(phoneWithSpaces, 10, '-');
    }
    if (phoneNumbers.length === 13) {
      phoneWithSpacesDash = insertIntoString(phoneWithSpaces, 11, '-');
    }
    phoneVariants.push(phoneNumbers);
    phoneVariants.push(phoneWithSpaces);
    phoneVariants.push(phoneWithSpacesDash);
    return phoneVariants;
  }

  generateAllBRPhoneVariants(phone: string) {
    if (!phone) {
      return [];
    }
    const phoneNumbers = phone.replace(/[^0-9]+/g, '');
    let phoneNumbersShort = '';
    let phoneNumbersLong = '';
    if (phoneNumbers.length === 12) {
      phoneNumbersShort = phoneNumbers;
      phoneNumbersLong = insertIntoString(phoneNumbers, 4, '9');
    }
    if (phoneNumbers.length === 13) {
      phoneNumbersShort = phoneNumbers;
      phoneNumbersLong = removeFromString(phoneNumbers, 4, 1);
    }
    const phVarShort = this.generatePhoneVariants(phoneNumbersShort);
    const phVarLong = this.generatePhoneVariants(phoneNumbersLong);
    return [...phVarShort, ...phVarLong];
  }

  async getAmoUsers() {
    let page = 1;
    const limit = 250;

    const response = await this.request({
      method: 'GET',
      url: `/api/v4/users`,
      params: { limit },
    });

    if (response.data._embedded?.users) {
      let result = response.data._embedded.users;

      if (response.data._page_count > 1) {
        do {
          page++;
          const pageResp = await this.request({
            method: 'GET',
            url: `/api/v4/users`,
            params: { page, limit },
          });
          result = [...result, ...pageResp.data._embedded.users];
        } while (page < response.data._page_count);
      }
      return { status: 'success', users: result };
    }
    return { status: 'error', info: response.data };
  }

  async importAmoUsers() {
    const amoUsers = await this.getAmoUsers();
    if (amoUsers.users && amoUsers.users.length > 0) {
      const users = amoUsers.users.map(user => {
        return { externalId: user.id, email: user.email, name: user.name };
      });

      for (const user of users) {
        const dbUser = await this.findOneUserByExtId(user.externalId);
        if (dbUser) {
          user.id = dbUser.id;
        }
        await this.amoUserRepository.save(user);

        // set profile viewer role for amo users
        const LMSUser = await this.usersRepository.findOne(
          {
            email: user.email.toLowerCase(),
          },
          { relations: ['roles'] },
        );
        if (LMSUser && !LMSUser.roles.some(role => role.role === SystemRole.ProfileViewer)) {
          const role = new Role();
          role.role = SystemRole.ProfileViewer;
          LMSUser.roles.push(role);
          await this.usersRepository.save(LMSUser);
        }
      }
    }
  }

  async getUsers() {
    return await this.amoUserRepository.createQueryBuilder('amo_users').getMany();
  }

  async findOneUserByEmail(email: string) {
    let user = await this.amoUserRepository.findOne({
      email: email.toLowerCase(),
    });
    if (!user) {
      // hack for old emails
      const otherEmail = email.replace('@ebac.art.br', '@ebaconline.com.br');
      user = await this.amoUserRepository.findOne({
        email: otherEmail.toLowerCase(),
      });
    }
    return user;
  }

  async findOneUserByExtId(externalId: string) {
    return await this.amoUserRepository.findOne({
      externalId,
    });
  }

  async getAmoUser(userId: string) {
    const res = await this.request({
      method: 'GET',
      url: `/api/v4/users/${userId}`,
    });
    return res.data;
  }

  async getAmoLead(leadId: string, paramWith = 'contacts') {
    const res = await this.request({
      method: 'GET',
      url: `/api/v4/leads/${leadId}?with=${paramWith},`, // Seems like the trailing comma is required to include the contact info
    });
    assert.ok(res.data.id, res.data.title);
    return res.data;
  }

  async getAmoContact(amoContactId: string) {
    const res = await this.request({
      method: 'GET',
      url: `/api/v4/contacts/${amoContactId}`,
    });
    assert.ok(res.data.id, res.data.title);
    return res.data;
  }

  async getUserByAmoLeadId(amoLeadId: string) {
    const amoLead = await this.getAmoLead(amoLeadId);
    assert.ok(amoLead, `amoLead not found: ${amoLeadId}`);
    const amoLeadContact = amoLead._embedded.contacts.find(c => c.is_main);
    return this.getUserByAmoContactId(amoLeadContact.id);
  }

  async getUserByAmoContactId(amoContactId: string) {
    const amoContact = await this.getAmoContact(amoContactId);
    assert.ok(amoContact, `amoContact not found: ${amoContactId}`);

    const isEmailField = f => f.field_id === getCustomField(CustomField.CONTACT_EMAIL);
    // const isEmailField = (f) => f.field_id === 1008143; // TODO ^
    const emailField = amoContact.custom_fields_values.find(isEmailField);
    assert.ok(emailField, `emailField not found`);

    const emailFieldValue = emailField.values[0]; // Is it possible for contact to have >1 emails?
    const email = emailFieldValue.value;
    // const email = 'jane.doe.1987@email.com'; // TODO ^
    return this.usersRepository.findOne({ email });
  }

  async getUserByAmoToken(amoToken: string) {
    const settings = await this.settingsService.getSettings();
    const secret = settings.integrationSettings.amo.profileWidgetSecret;
    assert.ok(secret, 'secret required');
    const payload: any = jwt.verify(amoToken, secret);
    const amoUserId = payload.user_id;
    const amoUser = await this.getAmoUser(amoUserId);
    const email = amoUser.email;
    // const email = 'admin@example.com'; // TODO ^
    const user = await this.usersRepository.findOne({ email });

    // Fallback to any tutor
    if (!user) {
      return this.usersService.findOneByRole(SystemRole.Tutor);
    }

    return user;
  }

  async createLead(data: CreateLead) {
    const amoData = {
      pipeline_id: data.pipelineId,
      status_id: data.statusId,
      created_by: 0,
      name: data.name,
      _embedded: {
        contacts: [data.contact],
      },
    } as any;
    if (data.responsible) {
      amoData.responsible_user_id = data.responsible;
    }
    if (data.tags?.length > 0) {
      amoData._embedded.tags = data.tags.map(id => {
        return { id };
      });
    }
    if (data.customFields?.length > 0) {
      amoData.custom_fields_values = data.customFields.map(cf => {
        return {
          field_id: cf.id,
          values: [
            {
              value: cf.value,
            },
          ],
        };
      });
    }

    const response = await this.request({
      method: 'POST',
      url: '/api/v4/leads/complex',
      data: [amoData],
    });

    if (response?.data?.length > 0 && response.data[0].id) {
      return { status: 'success', lead: response.data };
    }
    return { status: 'error', info: response.data };
  }

  async createOnboardCallLead({ studentId, enrollmentId, amoData: data }: OnboardingCallEvent) {
    const contact = await this.generateContactInfo(data, studentId);

    const createData = {
      pipelineId: getPipeline(Pipeline.CALL_CENTER),
      statusId: getStatus(Status.CALL_CENTER_FIRST_MESSAGE),
      name: data.lead,
      contact,
      tags: [getTag(Tag.PHONE_ONBOARDING)],
      customFields: [
        { id: getCustomField(CustomField.CC_COURSE_NAME), value: data.courseName },
        { id: getCustomField(CustomField.CC_NUMBER_OF_MONTHS), value: data.countMonths + '' },
        { id: getCustomField(CustomField.CC_NUMBER_OF_MODULES), value: data.countModules + '' },
        { id: getCustomField(CustomField.CC_ENROLLMENT_ID), value: enrollmentId },
      ],
      responsible: getIdentity(Identity.MAGIC_HELPER),
    } as CreateLead;

    return this.createLead(createData);
  }

  async createTestLead() {
    const contact = await this.generateContactInfo({
      name: 'Test contact',
      email: 'admin@example.com',
    });

    const createData = {
      pipelineId: getPipeline(Pipeline.TEST),
      statusId: getStatus(Status.TEST_INBOX),
      name: 'Test lead',
      contact,
    } as CreateLead;

    return this.createLead(createData);
  }

  async createAlmostCompletedCallLead({ studentId, enrollmentId, amoData: data }: OnboardingCallEvent) {
    const contact = await this.generateContactInfo(data, studentId);
    const callCenterResponsible = await this.getCallCenterResponsible(Responsibility.COURSE_ALMOST_COMPLETED);

    const createData = {
      pipelineId: getPipeline(Pipeline.CALL_CENTER),
      statusId: getStatus(Status.CALL_CENTER_INBOX),
      name: data.lead,
      contact,
      tags: [this.courseAlmostCompletedTagId],
      customFields: [
        { id: getCustomField(CustomField.CC_COURSE_NAME), value: data.courseName },
        { id: this.CFNeedsExercise, value: (data as any).needsExercise.join(', ') },
        { id: this.CFNotWatchedVideo, value: (data as any).notWatchedVideo.join(', ') },
        { id: this.CFNotPassed, value: (data as any).notPassed.join(', ') },
        { id: getCustomField(CustomField.CC_ENROLLMENT_ID), value: enrollmentId },
      ],
    } as CreateLead;
    if (callCenterResponsible) {
      createData.responsible = callCenterResponsible;
    }

    return this.createLead(createData);
  }

  async createCompletedCallLead({ studentId, enrollmentId, amoData: data }: OnboardingCallEvent) {
    const contact = await this.generateContactInfo(data, studentId);
    const callCenterResponsible = await this.getCallCenterResponsible(Responsibility.ONBOARDING);

    const createData = {
      pipelineId: getPipeline(Pipeline.CALL_CENTER),
      statusId: getStatus(Status.CALL_CENTER_INBOX),
      name: data.lead,
      contact,
      tags: [this.courseCompletedTagId],
      customFields: [
        { id: getCustomField(CustomField.CC_COURSE_NAME), value: data.courseName },
        { id: getCustomField(CustomField.CC_NUMBER_OF_MONTHS), value: data.countMonths + '' },
        { id: getCustomField(CustomField.CC_NUMBER_OF_MODULES), value: data.countModules + '' },
        { id: getCustomField(CustomField.CC_ENROLLMENT_ID), value: enrollmentId },
      ],
    } as CreateLead;
    if (callCenterResponsible) {
      createData.responsible = callCenterResponsible;
    }

    return this.createLead(createData);
  }

  async createStopLearningCallLead({ studentId, enrollmentId, amoData: data }: OnboardingCallEvent) {
    const contact = await this.generateContactInfo(data, studentId);
    const customFields = [
      { id: getCustomField(CustomField.CC_COURSE_NAME), value: data.courseName },
      { id: getCustomField(CustomField.CC_LAST_ACCESSED), value: (data as any).lastAccessed },
    ];
    // enrollmentId is empty and is optional for triggers created before this update:
    // https://gitlab.ebacdev.com/innervate.ru/ebac_lms/-/merge_requests/2499
    if (enrollmentId) {
      customFields.push({ id: getCustomField(CustomField.CC_ENROLLMENT_ID), value: enrollmentId });
    }

    const createData = {
      pipelineId: getPipeline(Pipeline.CALL_CENTER),
      // pipelineId: 5434757, // test pipeline
      statusId: getStatus(Status.CALL_CENTER_INBOX),
      // statusId: 48499220, // test status
      name: data.lead,
      contact: contact,
      tags: [this.stoppedLearningTagId],
      customFields,
    } as CreateLead;

    const createLeadRes = await this.createLead(createData);
    if (createLeadRes.status === 'error') {
      console.error(`Amo request failed: stopped learning ${studentId}:${enrollmentId}`);
      console.dir(createLeadRes.info, { depth: 10 });
      return createLeadRes;
    }

    const contactId = createLeadRes.lead[0].contact_id;
    strict.ok(contactId);
    await this.incrementContactStoppedLearningIncidentCount(contactId);

    return createLeadRes;
  }

  async incrementContactStoppedLearningIncidentCount(contactId) {
    const existingContact = await this.findAmoContactById(contactId);
    strict.ok(existingContact);

    const prevCfv = existingContact.custom_fields_values.find(cfv => cfv.field_id === this.CFContactStoppedLearningIncidentCount);
    if (prevCfv) {
      prevCfv.values[0].value = +prevCfv.values[0].value + 1;
    } else {
      existingContact.custom_fields_values.push({
        field_id: this.CFContactStoppedLearningIncidentCount,
        values: [{ value: 1 }],
      });
    }

    return this.request({
      method: 'PATCH',
      url: `/api/v4/contacts/${contactId}`,
      data: {
        custom_fields_values: existingContact.custom_fields_values.map(cfv => {
          // strip enum_code from enum fields to prevent FieldNotExpected errors
          const values = ['multiselect', 'select', 'radiobutton'].includes(cfv.field_type)
            ? cfv.values.map(cfvv => ({ value: cfvv.value, enum_id: cfvv.enum_id }))
            : cfv.values;
          return { ...cfv, values };
        }),
      },
    });
  }

  async findAmoContactById(contactId: string) {
    const res = await this.request({
      method: 'GET',
      url: `/api/v4/contacts/${contactId}`,
    });
    return res.data;
  }

  async createCourseNotStartedCallLead({ studentId, enrollmentId, amoData: data }: OnboardingCallEvent) {
    const contact = await this.generateContactInfo(data, studentId);
    const callCenterResponsible = await this.getCallCenterResponsible(Responsibility.COURSE_NOT_STARTED);

    const createData = {
      pipelineId: getPipeline(Pipeline.CALL_CENTER),
      statusId: getStatus(Status.CALL_CENTER_INBOX),
      name: data.lead,
      contact,
      tags: [this.courseNotStartedTagId],
      customFields: [
        { id: getCustomField(CustomField.CC_COURSE_NAME), value: data.courseName },
        { id: getCustomField(CustomField.CC_ENROLLMENT_ID), value: enrollmentId },
      ],
    } as CreateLead;
    if (callCenterResponsible) {
      createData.responsible = callCenterResponsible;
    }

    return this.createLead(createData);
  }

  async moveOnboardCallLead(data, endStatusId) {
    const filter = {
      'filter[pipeline_id]': getPipeline(Pipeline.CALL_CENTER),
      'filter[statuses][0][pipeline_id]': getPipeline(Pipeline.CALL_CENTER),
      'filter[statuses][0][status_id]': getStatus(Status.CALL_CENTER_INBOX),
      'filter[statuses][1][pipeline_id]': getPipeline(Pipeline.CALL_CENTER),
      'filter[statuses][1][status_id]': getStatus(Status.CALL_CENTER_FIRST_MESSAGE),
      'filter[name]': encodeURI(data.lead),
      'filter[custom_fields_values][1028481]': encodeURI(data.courseName), // todo wtf amo
      page: 1,
      limit: 50,
    };
    const filterArr = Object.keys(filter).map(key => [`${encodeURI(key)}=${filter[key]}`]);

    const filterStr = '?' + filterArr.join('&');

    const response = await this.request({
      method: 'GET',
      url: `/api/v4/leads${filterStr}`,
    });

    if (response?.data?._embedded) {
      if (response.data._embedded?.leads?.length > 0) {
        // amo filter not working, so we search by all onboarding leads
        const lead = response.data._embedded.leads.find(l => {
          const courseField = l.custom_fields_values.find(f => {
            return f.field_id === getCustomField(CustomField.CC_COURSE_NAME);
          });
          if (courseField) {
            return courseField.values[0].value === data.courseName;
          }
          return false;
        });
        if (lead) {
          const leadId = lead.id;
          const moveResponse = await this.request({
            method: 'PATCH',
            url: `/api/v4/leads/${leadId}`,
            data: { status_id: endStatusId },
          });
          if (moveResponse?.data?.updated_at) {
            return { status: 'success', lead: moveResponse.data };
          }
          return { status: 'error', info: moveResponse.data };
        }
      }
    }
    if (!response.data) {
      return { status: 'skip', info: 'No current lead found' };
    }
    return { status: 'error', info: response.data };
  }

  async removeOnboardCallLead(data) {
    return this.moveOnboardCallLead(data, getStatus(Status.LOST));
  }

  async successOnboardCallLead(data) {
    return this.moveOnboardCallLead(data, getStatus(Status.SUCCESS));
  }

  async pinNote(noteId: string) {
    return this.request({
      method: 'PATCH',
      url: `/api/v3/notes/${noteId}`,
      data: { pinned: true },
    });
  }

  async findAmoLead(query: string) {
    const resp = await this.request({
      method: 'GET',
      url: `/api/v4/leads`,
      params: { page: 1, limit: 50, query: query },
    });
    if (resp?.data) {
      if (resp.data._embedded?.leads?.length > 0) {
        return resp.data._embedded.leads[0];
      }
    }
    return false;
  }

  async findAmoContact(data: { phone?: string; email?: string }): Promise<boolean | any> {
    const { phone, email } = data;
    // for bright future when we're allowed to search by custom fields
    if (!phone && !email) {
      throw new HttpException('No phone or email provided', HttpStatus.BAD_REQUEST);
    }
    let res = false;

    if (email) {
      res = await this.findAmoContactByEmail(email);
    }
    if (!res && phone) {
      res = await this.findAmoContactByPhone(phone);
    }

    return res;
  }

  async findAmoContactByEmail(email: string) {
    // in bright future we'll be allowed to search by custom fields
    // but now we use only query

    const resp = await this.request({
      method: 'GET',
      url: `/api/v4/contacts`,
      params: { page: 1, limit: 50, query: email },
    });
    if (resp?.data) {
      if (resp.data._embedded?.contacts?.length > 0) {
        return resp.data._embedded.contacts[0];
      }
    }
    return false;
  }

  async findAmoContactByPhone(phone: string) {
    // in bright future we'll be allowed to search by custom fields
    // but now we use only query

    const resp = await this.request({
      method: 'GET',
      url: `/api/v4/contacts`,
      params: { page: 1, limit: 50, query: phone },
    });
    if (resp?.data) {
      if (resp.data._embedded?.contacts?.length > 0) {
        return resp.data._embedded.contacts[0];
      }
    }
    return false;
  }

  async generateContactInfo(data: { email?: string; phone?: string; name?: string }, studentId: string = null) {
    //todo rework
    let CFContactsEmail = this.CFContactsEmail;
    let CFContactsPhone = this.CFContactsPhone;
    let CFContactsLMSUserID = this.CFContactsLMSUserID;

    if (config.language === 'es') {
      CFContactsEmail = 63719;
      CFContactsPhone = 63717;
      CFContactsLMSUserID = 1376516;
    }

    const contactFields = [
      {
        field_id: CFContactsEmail, // email
        values: [
          {
            value: data.email,
          },
        ],
      },
    ];

    if (data.phone && data.phone !== 'null') {
      contactFields.push({
        field_id: CFContactsPhone, // phone
        values: [
          {
            value: data.phone,
          },
        ],
      });
    }
    if (studentId) {
      contactFields.push({
        field_id: CFContactsLMSUserID,
        values: [
          {
            value: studentId,
          },
        ],
      });
    }

    const name = data.name ? data.name : data.email;

    const contact = {
      first_name: name,
      updated_by: 0,
      custom_fields_values: contactFields,
    } as any;

    // search contact for duplicates
    const existingContact = await this.findAmoContact({
      email: data.email,
      phone: data.phone,
    });
    if (existingContact) {
      contact.id = existingContact.id;
      if (
        studentId &&
        !existingContact.custom_fields_values.find(f => f.field_id === CFContactsLMSUserID)?.values.some(({ value }) => value === studentId)
      ) {
        await this.request({
          method: 'PATCH',
          url: `/api/v4/contacts/${existingContact.id}`,
          data: {
            custom_fields_values: [
              {
                field_id: CFContactsLMSUserID,
                values: [
                  {
                    value: studentId,
                  },
                ],
              },
            ],
          },
        });
      }
    }
    return contact;
  }

  async getCallCenterResponsible(responsibility: Responsibility) {
    const qb = this.usersRepository
      .createQueryBuilder('user')
      .andWhere('user.active = TRUE')
      .andWhere('user.email IN (:...emails)', {
        emails: getEmailByResponsibility(responsibility),
      })
      .orderBy('user.lastAccessed', 'ASC', 'NULLS FIRST');
    const responsible = await qb.getMany();

    if (responsible && responsible.length > 0) {
      const user = responsible[0];
      user.lastAccessed = new Date();
      await this.usersRepository.save(user);
      let amoUser = await this.findOneUserByEmail(user.email);
      if (!amoUser) {
        await this.importAmoUsers();
        amoUser = await this.findOneUserByEmail(user.email);
      }
      if (amoUser) {
        return parseInt(amoUser.externalId, 10);
      }
    }
    return 0;
  }

  async getResponsibleUserId(course: Course, role: SystemRole) {
    const responsible = await this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoin('user.enrollments', 'enrollments')
      .andWhere('enrollments.courseId = :courseId', { courseId: course.id })
      .andWhere('enrollments.role = :role', { role })
      .andWhere('user.active = TRUE')
      .andWhere(
        'enrollments.active = TRUE' +
          ' AND (enrollments.dateFrom IS NULL OR enrollments.dateFrom <= NOW())' +
          ' AND (enrollments.dateTo IS NULL OR enrollments.dateTo > NOW())',
      )
      .orderBy('user.lastAccessed', 'ASC', 'NULLS FIRST')
      .getMany();

    if (responsible && responsible.length > 0) {
      const user = responsible[0];
      user.lastAccessed = new Date();
      await this.usersRepository.save(user);
      let amoUser = await this.findOneUserByEmail(user.email);
      if (!amoUser) {
        await this.importAmoUsers();
        amoUser = await this.findOneUserByEmail(user.email);
      }
      if (amoUser) {
        return parseInt(amoUser.externalId, 10);
      }
    }
    return null;
  }

  async webhookHandler(body: any) {
    if (body) {
      /*
      Template:

      EBAC LMS Note: Test User, https://lms.ebaconline.com.br/profile/a430fed5-8637-4f85-b674-803d0d60c4f2/, last activity: 2020-12-28 15:23:35
      Enrollments:
      1) Photoshop do Zero ao Pro, created 2020-12-28 15:23, from 2020-12-28 to
      2020-12-28
      Progress: 6 / 12 / 14 (completed / available / total planned)
      2) Profissão UX/UI Designer, created: 2020-12-28 15:23, from 2020-12-28 to
      2020-12-28
      Progress: 6 / 12 / 14 (completed / available / total planned)

      Info updated: 2020-12-28 15:23:35
       */

      // Get all leads from events
      let parsedLeads = [];

      // real data
      const { unsorted, leads } = body;
      unsorted?.add?.map(item => parsedLeads.push(item.id));
      unsorted?.update?.map(item => parsedLeads.push(item.id));
      leads?.add?.map(item => parsedLeads.push(item.id));
      leads?.update?.map(item => parsedLeads.push(item.id));
      unsorted?.status?.map(item => parsedLeads.push(item.id));
      leads?.status?.map(item => parsedLeads.push(item.id));

      parsedLeads = [...new Set(parsedLeads.filter(Boolean))];

      dbgLog('parsedLeads', parsedLeads);

      for (const leadId of parsedLeads) {
        // First of all check if note needed

        let createNote = true;
        let updateNote = false;
        let updateNoteId = '';
        let cntNotesBefore = 0;

        const existingNotesReq = await this.request({
          method: 'GET',
          url: `/api/v4/leads/${leadId}/notes`,
        });
        if (existingNotesReq?.data?._embedded?.notes) {
          const existingNotes = existingNotesReq?.data?._embedded?.notes;
          cntNotesBefore = existingNotes.length;

          for (const exNote of existingNotes) {
            if (exNote.params?.text) {
              const exNoteText = exNote.params?.text;
              if (exNoteText.includes('EBAC LMS Note:') && exNoteText.includes('Info updated:')) {
                createNote = false;
                const index = exNoteText.indexOf('Info updated:');
                const updatedText = exNoteText.substr(index + 14, 19 + index + 14);
                const updatedTime = dayjs(updatedText, 'YYYY.MM.DD HH:mm:ss');
                if (dayjs().diff(updatedTime, 'minute') > 5) {
                  updateNote = true;
                  updateNoteId = exNote.id;
                  dbgLog('updating note');
                } else {
                  dbgLog('no need to update note');
                }
              }
            }
          }
        }

        // We have actual note, no need to load server cpu
        if (!createNote && !updateNote) {
          return;
        }

        // Find lead
        const amoDeal = await this.request({
          url: `/api/v4/leads/${leadId}`,
          method: 'GET',
          params: {
            with: 'catalog_elements,is_price_modified_by_robot,loss_reason,contacts,source_id',
          },
        });
        if (!amoDeal?.data) continue;

        // Find contact
        const mainContact = amoDeal?.data?._embedded?.contacts.find(item => item.is_main);
        let amoContact = null;
        if (mainContact?.id) {
          amoContact = await this.request({
            method: 'GET',
            url: `/api/v4/contacts/${mainContact?.id}`,
          });
        } else {
          const firstContact = amoDeal?.data?._embedded?.contacts?.[0];
          if (firstContact?.id) {
            amoContact = await this.request({
              method: 'GET',
              url: `/api/v4/contacts/${firstContact?.id}`,
            });
          }
        }

        // for testing on test users only. Set false if ready to production
        let killswitch = false;

        let foundUserId = '';

        let userName = '';
        let profileLink = '';
        let lastActivity = '';
        const sendEnrollments = [];
        const lastUpdated = dayjs().format('YYYY.MM.DD HH:mm:ss');

        // Search for user in LMS by phone or email
        if (amoContact?.data) {
          const phoneValues = amoContact?.data?.custom_fields_values?.find(field => field.field_code === 'PHONE')?.values;

          if (phoneValues) {
            for (const value of phoneValues) {
              if (value?.value && value?.value !== '+5599999999999' && !foundUserId) {
                const phone = value?.value;
                dbgLog('phone', phone);
                if (phone === '+79999999999') {
                  killswitch = false;
                }
                const profile = await this.profileService.getProfileAndUserBySearch(phone);
                if (profile) {
                  foundUserId = profile.user.id;
                }
              }
            }
          }
          if (!foundUserId) {
            const emailValues = amoContact?.data?.custom_fields_values?.find(field => field.field_code === 'EMAIL')?.values;
            if (emailValues) {
              for (const value of emailValues) {
                if (value?.value && !foundUserId) {
                  dbgLog('email', value?.value);
                  const fUser = await this.usersRepository.findOne({
                    email: value?.value.toLowerCase(),
                  });
                  if (fUser) {
                    foundUserId = fUser.id;
                  }
                }
              }
            }
          }
        }

        // Create note data
        if (foundUserId) {
          dbgLog('found something');
          dbgLog(foundUserId);
          const profileInfo = await this.supportService.getProfile(foundUserId, false);
          const { user, enrollments, lectureProgress, lectures } = profileInfo;
          userName = user.name;
          //todo url from config
          profileLink = `https://lms.ebaconline.com.br/support/profile/${user.id}`;
          if (user.activity?.modified) {
            lastActivity = dayjs(user.activity.modified).format('YYYY.MM.DD HH:mm:ss');
          }
          const studentEnrollments = enrollments.filter(enr => {
            return enr.role === CourseRole.Student && enr.active === true;
          });
          const viewedLectures = lectureProgress.flatMap(lp => {
            return lp.lecture?.id ? [lp.lecture?.id] : [];
          });
          if (studentEnrollments.length > 0) {
            for (const studentEnrollment of studentEnrollments) {
              if (!lectures) continue;
              const courseLectures = lectures.filter(a => a.course.id === studentEnrollment.course.id);
              const counters = {
                completed: 0,
                available: 0,
                totalPlanned: 0,
              };
              courseLectures.forEach(lecture => {
                if (lecture.active) {
                  counters.totalPlanned++;
                  if (lecture.published) {
                    counters.available++;
                  }
                }
                if (viewedLectures.includes(lecture.id)) {
                  counters.completed++;
                }
              });
              sendEnrollments.push({
                course: studentEnrollment.course.title,
                created: dayjs(studentEnrollment.created).format('YYYY.MM.DD HH:mm'),
                from: dayjs(studentEnrollment.dateFrom).format('YYYY.MM.DD'),
                to: dayjs(studentEnrollment.dateTo).format('YYYY.MM.DD'),
                progress: counters,
              });
            }
          }

          let note = `EBAC LMS Note: ${userName}, ${profileLink}`;
          if (lastActivity) {
            note += `, last activity: ${lastActivity}`;
          }
          note += `\nPlease look for a profile in tab "LMS" on the left`;
          if (sendEnrollments.length > 0) {
            note += `\nEnrollments:`;
            sendEnrollments.forEach((enr, enrIndex) => {
              note += `\n${enrIndex + 1}) ${enr.course}, `;
              note += `created ${enr.created}, from ${enr.from} to ${enr.to}`;
              note += `\nProgress: ${enr.progress.completed} / ${enr.progress.available} / ${enr.progress.totalPlanned} `;
              note += `(completed / available / total planned)`;
            });
          }
          note += `\n\nInfo updated: ${lastUpdated}`;

          dbgLog('note');
          dbgLog(note);

          if (!killswitch) {
            // get notes and try to find ours
            if (updateNote) {
              const noteResult = await this.request({
                method: 'PATCH',
                url: `/api/v4/leads/${leadId}/notes/${updateNoteId}`,
                data: {
                  note_type: 'common',
                  params: {
                    text: note,
                  },
                },
              });
              dbgLog(`noteResult update`, noteResult.data);
            }

            if (createNote) {
              // first let's check that we don't have another note again
              const existingNotesReq = await this.request({
                method: 'GET',
                url: `/api/v4/leads/${leadId}/notes`,
              });
              if (existingNotesReq?.data?._embedded?.notes) {
                // todo check for existing right note
                if (existingNotesReq?.data?._embedded?.notes.length > cntNotesBefore) {
                  dbgLog('possible race condition, no need to create note');
                  return;
                }
              }
              const noteResult = await this.request({
                method: 'POST',
                url: `/api/v4/leads/${leadId}/notes`,
                data: [
                  {
                    entity_id: leadId,
                    note_type: 'common',
                    params: {
                      text: note,
                    },
                  },
                ],
              });
              dbgLog(`noteResult create`, noteResult.data);
            }
          }
        } else {
          dbgLog('no match');
        }
      }
    }
  }

  async webhookIncommingMessage(body: any) {
    if (body?.message?.add) {
      for (const messageData of body.message.add) {
        //Проверяем сообщение по фразам. Если сообщение совпадает с любой из фраз, выходим
        for (const phrase of whatsappPhrases) {
          if (messageData.text.trim().toLowerCase() == phrase) {
            const logMessage = `Message matched phrase "${phrase}". Skipping`;
            console.log(logMessage);
            return {
              status: 'skip',
              info: logMessage,
            };
          }
        }
        //Если сообщение содержит только цифры, выходим
        if (!isNaN(messageData.text.trim()) && !isNaN(parseFloat(messageData.text.trim()))) {
          const logMessage = `Message contains only digits. Skipping`;
          console.log(logMessage);
          return {
            status: 'skip',
            info: logMessage,
          };
        }
        //Получаем текущий timestamp в секундах
        const now: number = Math.floor(Date.now() / 1000);
        const messageTime = +messageData.created_at;
        const contactId = +messageData.contact_id;
        console.log(`Processing contact ${contactId}. Message: ${messageData.text}`);
        //Получаем данные по контакту
        const contactRes = await this.request({
          method: 'GET',
          url: `/api/v4/contacts/${contactId}`,
          params: { with: 'leads' },
        });

        //Если данные контакта не получены с первой попытки, перезапускаем вебхук через 20 секунд 5 раз.
        const messageId = messageData.id;
        if (!contactRes.data?.id) {
          if (!this.messageRetries[messageId]) this.messageRetries[messageId] = 0;

          if (this.messageRetries[messageId] < 5) {
            setTimeout(() => {
              this.webhookIncommingMessage(body);
            }, 20000);
            this.messageRetries[messageId]++;
            const logMessage = `Couldn't get contact data. Retry in 20 seconds...(${this.messageRetries[messageId]})`;
            console.log(logMessage);
            return {
              status: 'skip',
              info: logMessage,
            };
          } else {
            delete this.messageRetries[messageId];
            const logMessage = `Couldn't get contact data. Please check amoCRM auth data.`;
            console.log(logMessage);
            return {
              status: 'error',
              info: logMessage,
            };
          }
        }
        if (this.messageRetries[messageId]) delete this.messageRetries[messageId];

        const contact = contactRes.data;
        const contactLeads = contact._embedded?.leads;
        const activeLeads: Array<any> = [];
        let activeLeadFound = false;
        let youngestLead;
        console.log(`${contactId}: Leads count - ${contactLeads?.length || 0}`);
        let leads: Array<any> = []; //Сюда поместим развернутую инфу о сделках у контакта, ибо запрос по получению контакта со сделками выдаст только ID сделок в контакте
        const contactLeadsIds: Array<number> = [];
        if (contactLeads) {
          for (const contactLead of contactLeads) {
            contactLeadsIds.push(contactLead.id);
          }
          const leadsRes = await this.request({
            method: 'GET',
            url: `/api/v4/leads`,
            params: { filter: { id: contactLeadsIds } },
          });
          leads = leadsRes.data?._embedded?.leads || [];
          for (const lead of leads) {
            //Когда приходит сообщение, updated_at у сделки в которую пришло сообщение обновляется (становится на секунду больше от времени сообщения, но не факт что всегда на секунду).
            //Поэтому вычисляем самую молодую сделку (неважно активную или нет) - по ней мы будем сравнивать время сообщения.
            //Если разница между updated_at сделки и временем сообщения не больше 60 секунд, то сообщение считаем актуальным
            if (!youngestLead) youngestLead = lead;
            if (youngestLead.updated_at < lead.updated_at) {
              youngestLead = lead;
            }
            if (lead.status_id != 143 && lead.status_id != 142) {
              activeLeads.push(lead);
              activeLeadFound = true;
            }
          }
          if (youngestLead && messageTime + 60 < youngestLead.updated_at) {
            console.log(`${contactId}: Message is too old`);
            return { status: 'skip', info: 'Message is too old' };
          }
          console.log(`${contactId}: Active leads count - ${activeLeads?.length || 0}`);
        } else {
          console.log(`${contactId}: New contact, doing nothing`);
          return { status: 'skip', info: 'New contact, doing nothing' };
        }
        if (!leads.length) {
          const logMessage = `${contactId}: No leads found`;
          console.log(logMessage);
          return {
            status: 'skip',
            info: logMessage,
          };
        }
        if (!activeLeadFound) {
          //Вычисляем самую свежую закрытую сделку
          const youngestInactiveLead = leads.reduce((prev, current) => {
            if (prev.updated_at < current.updated_at) {
              return current;
            } else {
              return prev;
            }
          });
          //Получаем событие по изменению статуса
          const leadEventsRes = await this.request({
            method: 'GET',
            url: `/api/v4/events`,
            params: {
              limit: 1, //Нам нужно получить лишь 1 событие по изменению статуса, причем нужно событие где статус сменился на WIN или LOST
              filter: {
                type: ['lead_status_changed'],
                entity: 'lead',
                entity_id: youngestInactiveLead.id,
                value_after: {
                  leads_statuses: [
                    {
                      pipeline_id: youngestInactiveLead.pipeline_id,
                      status_id: 142,
                    },
                    {
                      pipeline_id: youngestInactiveLead.pipeline_id,
                      status_id: 143,
                    },
                  ],
                },
              },
            },
          });
          const leadEvents = leadEventsRes?.data?._embedded?.events || [];
          if (leadEvents.length == 0) {
            const logMessage = `${contactId}: No messages in inactive lead ${youngestInactiveLead.id}`;
            console.log(logMessage);
            return {
              status: 'skip',
              info: logMessage,
            };
          }
          const lastEvent = leadEvents[0];
          const becameInactiveTimestamp: number = lastEvent.created_at;
          console.log(
            `${contactId}: Lead had moved to inactive status at ${dayjs.unix(becameInactiveTimestamp).format('DD.MM.YYYY HH:mm:ss')}`,
          );
          console.log(`${contactId}: Lead pipeline id: ${youngestInactiveLead.pipeline_id}`);
          // Если у сделки дата перемещения в неактивный статус больше 2 суток
          // (время задается в свойстве createNewWhatsappLeadAfterHours в часах)
          // или это сейлзовая, SS или ICG SS воронка - создаем новую сделку, иначе меняем статус текущей
          if (
            youngestInactiveLead.pipeline_id == getPipeline(Pipeline.SALES_PIPELINE_V1) ||
            youngestInactiveLead.pipeline_id == getPipeline(Pipeline.ICG_STUDENT_SUPPORT) ||
            youngestInactiveLead.pipeline_id == getPipeline(Pipeline.STUDENT_SUPPORT) ||
            now - becameInactiveTimestamp > this.createNewWhatsappLeadAfterHours * 3600
          ) {
            //Если сделка в воронке Sales Pipeline, то создаем сделку в этой воронке на статусе Request. Иначе создаем сделку в Incoming Channels Pipeline в Repeated WA Requests
            const leadData: any = {
              name: `Incoming message from ${contact.name}`,
              _embedded: {
                contacts: [
                  {
                    id: contactId,
                  },
                ],
              },
            };
            if (youngestInactiveLead.pipeline_id == getPipeline(Pipeline.SALES_PIPELINE_V1)) {
              leadData.pipeline_id = getPipeline(Pipeline.SALES_PIPELINE_V1);
              leadData.status_id = getStatus(Status.SALES_PIPELINE_REQUEST);
              leadData.responsible_user_id = youngestInactiveLead.responsible_user_id;
            } else if (
              // Переоткрытые лиды из SS и ICG SS пересоздаются в ICG SS / Repeated requests
              youngestInactiveLead.pipeline_id == getPipeline(Pipeline.ICG_STUDENT_SUPPORT) ||
              youngestInactiveLead.pipeline_id == getPipeline(Pipeline.STUDENT_SUPPORT)
            ) {
              leadData.pipeline_id = getPipeline(Pipeline.ICG_STUDENT_SUPPORT);
              leadData.status_id = getStatus(Status.ICG_STUDENT_SUPPORT_REPEATED_REQUESTS);
              leadData.responsible_user_id = youngestInactiveLead.responsible_user_id;
            } else {
              leadData.pipeline_id = getPipeline(Pipeline.INCOMING_CHANNELS);
              leadData.status_id = getStatus(Status.INCOMING_CHANNELS_REPEATED_WA_REQUESTS);
            }

            const leadRes = await this.request({
              method: 'POST',
              url: `/api/v4/leads`,
              data: [leadData],
            });
            const logMessage = `${contactId}: Lead ${leadRes.data?._embedded?.leads[0]?.id} created`;
            console.log(logMessage);
            return {
              status: 'success',
              info: logMessage,
            };
          } else {
            let pipelineIdForMoving: number, statusIdForMoving: number;
            switch (youngestInactiveLead.pipeline_id) {
              case getPipeline(Pipeline.TUTOR_SUPPORT):
                pipelineIdForMoving = getPipeline(Pipeline.TUTOR_SUPPORT);
                statusIdForMoving = getStatus(Status.TUTOR_SUPPORT_REPEATED_REQUESTS);
                break;
              default:
                pipelineIdForMoving = getPipeline(Pipeline.INCOMING_CHANNELS);
                statusIdForMoving = getStatus(Status.INCOMING_CHANNELS_REPEATED_WA_REQUESTS);
            }
            const leadRes = await this.request({
              method: 'PATCH',
              url: `/api/v4/leads/${youngestInactiveLead.id}`,
              data: {
                pipeline_id: pipelineIdForMoving,
                status_id: statusIdForMoving,
              },
            });
            const logMessage = `${contactId}: Lead ${leadRes.data?.id} moved to pipeline ${pipelineIdForMoving} and status ${statusIdForMoving}`;
            console.log(logMessage);
            return {
              status: 'success',
              info: logMessage,
            };
          }
        } else {
          //Вычисляем самую молодую активную сделку
          const youngestActiveLead = activeLeads.reduce((prev, current) => {
            if (prev.updated_at < current.updated_at) {
              return current;
            } else {
              return prev;
            }
          });
          console.log(`${contactId}: Processing active lead ${youngestActiveLead.id}`);
          //Получаем события активной сделки
          const leadEventsRes = await this.request({
            method: 'GET',
            url: `/api/v4/events`,
            params: {
              limit: 2, //Нам нужно получить лишь 2 события по входящим сообщениям. В ответе события отсортированы в порядке убывания (от новых с к старым). Первое событие будет содержать инфу о только что написанном сообщении. Второе - инфу о предыдущем сообщении. Оно нам и нужно.
              filter: {
                type: ['incoming_chat_message'],
                entity: ['lead'],
                entity_id: [youngestActiveLead.id],
              },
            },
          });
          const leadEvents = leadEventsRes?.data?._embedded?.events || [];
          //Может быть такое что в молодой сделке сообщений еще нет, пропускаем.
          if (leadEvents.length == 0) {
            const logMessage = `${contactId}: No messages in active lead ${youngestActiveLead.id}`;
            console.log(logMessage);
            return {
              status: 'skip',
              info: logMessage,
            };
          }
          const lastEvent = leadEvents[1] || leadEvents[0]; // В приоритете второе событие. Первое берем на случай, если сообщение всего одно (а вдруг?).
          const lastMessageTime: number = lastEvent.created_at;
          console.log(`${contactId}: Last message was at ${dayjs.unix(lastMessageTime).format('DD.MM.YYYY HH:mm:ss')}`);
          //Если воронка не Call Center и не Sales Pipeline V1 и дата последнего сообщения более двух суток назад
          if (
            youngestActiveLead.pipeline_id !== getPipeline(Pipeline.CALL_CENTER) &&
            youngestActiveLead.pipeline_id !== getPipeline(Pipeline.SALES_PIPELINE_V1) &&
            now - lastMessageTime > 48 * 3600
          ) {
            const emailMessage = `This lead has been created more than 48 hours ago and the customer is writing us messages, figure it out: <a href="https://ebaconline.amocrm.com/leads/detail/${youngestActiveLead.id}" target="_blank">https://ebaconline.amocrm.com/leads/detail/${youngestActiveLead.id}</a> `;

            //Сюда будем собирать имейлы для отправки
            const toEmails: Array<string> = [];
            let emailSubject = 'Lead warning';

            //Тут будем проверять, есть ли уже информация о пользователе на случай если активных сделок несколько. Если нет - запрашиваем у амо
            const responsibleUsers: Record<number, any> = {};
            if (!responsibleUsers[youngestActiveLead.responsible_user_id]) {
              const userRes = await this.request({
                method: 'GET',
                url: `/api/v4/users/${youngestActiveLead.responsible_user_id}`,
              });
              responsibleUsers[youngestActiveLead.responsible_user_id] = userRes.data;
            }

            const responsibleUser = responsibleUsers[youngestActiveLead.responsible_user_id];
            toEmails.push(responsibleUser.email);
            switch (youngestActiveLead.pipeline_id) {
              case getPipeline(Pipeline.INCOMING_CHANNELS):
                toEmails.push('douglas.silva@ebaconline.com.br');
                emailSubject = 'Lead warning in Incoming Channels Pipeline';
                break;
              case getPipeline(Pipeline.STUDENT_SUPPORT):
                toEmails.push('juliana.alvarenga@ebaconline.com.br');
                emailSubject = 'Lead warning in Student Support Pipeline';
                break;
              case getPipeline(Pipeline.COMPLAINTS):
                toEmails.push('juliana.alvarenga@ebaconline.com.br');
                emailSubject = 'Lead warning in Ouvidoria pipeline';
                break;
              case getPipeline(Pipeline.TUTOR_SUPPORT):
                toEmails.push('marcela.moreira@ebac.art.br');
                emailSubject = 'Lead warning in Tutors support pipeline';
                break;
            }

            await this.mailerService.sendMail({
              to: toEmails,
              subject: emailSubject,
              text: emailMessage,
            });
            console.log(`${contactId}: Email message for lead ${youngestActiveLead.id} sent`);
          } else {
            console.log(`${contactId}: Lead is ok. Skipping`);
          }
        }
      }
      console.log(`All done`);
      return { status: 'success', info: 'All done' };
    }
    console.log('Unknown error');
    return { status: 'error', info: 'Unknown error' };
  }

  async webhookSetResponsible({ body, role, fromStatus, toStatus, pipeline }: SetResponsibleOptions) {
    const webhookLeads = body?.leads?.add || body?.leads?.status;
    if (!webhookLeads) {
      return { status: 'skip', info: 'Nothing to do' };
    }
    for (const webhookLead of webhookLeads) {
      if (+webhookLead.status_id !== fromStatus) {
        return { status: 'skip', info: 'Nothing to do' };
      }
      //Получаем данные по сделке
      const leadRes = await this.request({
        method: 'GET',
        url: `/api/v4/leads/${webhookLead.id}`,
        params: { with: 'contacts' },
      });

      //Если данных нет - скорее всего упал токен - выходим
      assert(leadRes.data?.id, "Couldn't get lead data. Please check amoCRM auth data.");

      console.log(`Processing lead ${leadRes.data?.id}`);

      const lead = leadRes.data;

      //Если у сделки есть контакты, то первый привязанный к сделке контакт является основным. У сделки всегда есть основной контакт.
      const mainContact = lead?._embedded?.contacts.find(c => c.is_main);
      if (!mainContact) {
        return { status: 'skip', info: `Lead ${lead.id}: No contacts in lead` };
      }

      //Получаем данные по контакту
      const contactRes = await this.request({
        method: 'GET',
        url: `/api/v4/contacts/${mainContact.id}`,
        params: { with: 'leads' },
      });
      const contact = contactRes?.data;

      let student: User;

      //Вытаскиваем имейлы из контакта
      const emails: Array<string> = [];
      contact?.custom_fields_values
        ?.find(field => field.field_code === 'EMAIL')
        ?.values?.map(value => {
          if (value?.value) emails.push(value?.value);
        });

      if (emails.length == 0) {
        const logMessage = `Student has no emails`;
        console.log(logMessage);
      } else {
        console.log(`Processing contact emails ${emails.join(', ')}`);

        //По этим имейлам ищем студента

        for (const email of emails) {
          student = await this.usersService.findOneByEmail(email);
          if (student) {
            break;
          }
        }
      }

      //Если не нашли студента по имейлу, ищем по телефонам
      if (!student) {
        const phones: Array<string> = [];
        contact?.custom_fields_values
          ?.find(field => field.field_code === 'PHONE')
          ?.values?.map(value => {
            if (value?.value) phones.push(value?.value);
          });
        if (phones.length == 0) {
          const logMessage = `Student has no phones`;
          console.log(logMessage);
        } else {
          console.log(`Processing contact phones ${phones.join(', ')}`);

          //По этим телефонам ищем профиль студента
          for (const phone of phones) {
            const studentProfile: Profile = await this.profileService.getProfileAndUserBySearch(phone);
            if (studentProfile) {
              student = studentProfile.user;
              break;
            }
          }
        }
      }

      //Если не нашли студента - выходим
      if (!student) {
        const logMessage = `Student not found`;
        console.log(logMessage);
        return { status: 'skip', info: logMessage };
      }

      //Получаем последний созданный enrollment у студента
      const lastStudentEnrollment: Enrollment = await this.enrollmentsRepository
        .createQueryBuilder('enrollments')
        .leftJoinAndSelect('enrollments.course', 'course')
        .andWhere('enrollments.user = :user', { user: student.id })
        .andWhere(
          `enrollments.active = TRUE
          AND (enrollments.dateFrom IS NULL OR enrollments.dateFrom <= NOW())
          AND (enrollments.dateTo IS NULL OR enrollments.dateTo > NOW())`,
        )
        .orderBy('enrollments.created', 'DESC')
        .getOne();

      if (!lastStudentEnrollment) {
        const logMessage = `${student.email}: Student's enrollment not found`;
        console.log(logMessage);
        return { status: 'skip', info: logMessage };
      }

      //Получаем ID ответственного
      const responsibleUserId = await this.getResponsibleUserId(lastStudentEnrollment.course, role);

      if (!responsibleUserId) {
        const logMessage = `Lead ${lead.id}: Support responsible not found`;
        console.log(logMessage);
        return { status: 'skip', info: logMessage };
      }

      //Обновляем ответственного сделки и переводим сделку в статус SS_MANAGER_ASSIGNED
      const leadStatusUpdateRes = await this.request({
        method: 'PATCH',
        url: `/api/v4/leads/${lead.id}`,
        data: {
          responsible_user_id: responsibleUserId,
          status_id: toStatus,
          pipeline_id: pipeline,
        },
      });

      if (leadStatusUpdateRes?.data) {
        const logMessage = `Lead ${lead.id}: All done`;
        console.log(logMessage);
      }
    }
    return { status: 'success', info: 'Completed' };
  }

  async webhookCloseParentLeads(body: any) {
    const resp = [];

    if (body) {
      // Get all leads from events (should be only one, but oh well)
      let parsedLeads = [];

      // real data
      const { unsorted, leads } = body;
      unsorted?.add?.map(item => parsedLeads.push(item.id));
      unsorted?.update?.map(item => parsedLeads.push(item.id));
      leads?.add?.map(item => parsedLeads.push(item.id));
      leads?.update?.map(item => parsedLeads.push(item.id));
      unsorted?.status?.map(item => parsedLeads.push(item.id));
      leads?.status?.map(item => parsedLeads.push(item.id));

      parsedLeads = [...new Set(parsedLeads.filter(Boolean))];

      const CFParentIncidentLeadId = getCustomField(CustomField.LEAD_INCIDENT_PARENT_ID);

      for (const leadId of parsedLeads) {
        // get field data
        // Find lead
        const amoDeal = await this.request({
          method: 'GET',
          url: `/api/v4/leads/${leadId}`,
        });
        if (!amoDeal?.data) continue;

        const parentIncidentLeadId = amoDeal?.data?.custom_fields_values?.find(field => field.field_id === CFParentIncidentLeadId)
          ?.values[0].value;

        // if we're moving parent lead itself, we don't need to move parent lead anymore :)
        if (parseInt(parentIncidentLeadId, 10) === parseInt(leadId, 10)) {
          // maybe close child leads
          continue;
        }

        // search for the parent leads and move them to closed
        const leads = await this.request({
          method: 'GET',
          url: '/api/v4/leads',
          params: {
            page: 1,
            limit: 50,
            filter: {
              custom_fields_values: {
                [CFParentIncidentLeadId]: parentIncidentLeadId,
              },
            },
          },
        });

        if (leads?.data?._embedded?.leads?.length > 0) {
          for (const lead of leads.data._embedded.leads) {
            // skip current lead
            if (parseInt(lead.id, 10) === parseInt(leadId, 10)) {
              continue;
            }
            const statusId = parseInt(lead.status_id, 10);
            const successId = getSuccessIncidentStatus(lead.pipeline_id);

            // skip leads that already moved to success
            if (statusId === getStatus(Status.SUCCESS) || statusId === successId) {
              continue;
            }

            const moveResponse = await this.request({
              method: 'PATCH',
              url: `/api/v4/leads/${lead.id}`,
              data: { status_id: successId },
            });
            if (moveResponse?.data?.updated_at) {
              resp.push({ status: 'success', lead: moveResponse.data });
            }
            resp.push({ status: 'error', info: moveResponse.data });
          }
        }
      }
    }
    return resp;
  }

  async emailHandler(
    type: string,
    data: {
      id: string;
      displayId: string;
      threadId: string;
      email: string;
      name: string;
      subject: string;
      body: string;
    },
  ) {
    // don't create replies from mails sent from amo. They are already in lead.
    if (/\(mail message #.*? thread\s/giu.test(data.body)) {
      return { status: 'success', info: 'Needed lead is already exists' };
    }

    const contact = await this.generateContactInfo({
      email: data.email,
      name: data.name,
    });

    const tagId = this.emailMessageTagId;

    // default Student Support pipeline
    let pipelineId = getPipeline(Pipeline.STUDENT_SUPPORT);
    let statusId = getStatus(Status.STUDENT_SUPPORT_INBOX);

    switch (type) {
      case AmoEmailInboxType.test:
        pipelineId = getPipeline(Pipeline.TEST);
        statusId = getStatus(Status.TEST_INBOX);
        break;
      case AmoEmailInboxType.tutorSupport:
        pipelineId = getPipeline(Pipeline.TUTOR_SUPPORT);
        statusId = getStatus(Status.TUTOR_SUPPORT_INBOX);
        break;
      case AmoEmailInboxType.sales:
        // todo another email types
        break;
    }

    const createData = {
      pipelineId: pipelineId,
      statusId: statusId,
      name: `Mail message #${data.displayId} thread #${data.threadId}`,
      contact,
      tags: [tagId],
      customFields: [
        { id: this.CFMailThreadId, value: data.threadId },
        {
          id: this.CFMailNameId,
          value: `mail message #${data.displayId} thread #${data.threadId}`,
        },
      ],
    } as CreateLead;

    if (data.subject) {
      createData.customFields.push({
        id: this.CFMailSubjectId,
        value: data.subject,
      });
    }
    if (data.body) {
      createData.customFields.push({ id: this.CFMailBodyId, value: data.body });
    }

    // find lead with thread
    let lead = await this.findAmoLead(`thread #${data.threadId}`);

    if (!lead) {
      const response = await this.createLead(createData);
      if (response.status === 'success' && response.lead.length > 0 && response.lead[0].id) {
        lead = response.lead[0];
      } else {
        return { status: 'error', info: response.info };
      }
    }

    if (lead) {
      let noteText = `Subject: ${data.subject}\n\n`;
      noteText += `Message: ${data.body}`;

      const noteResult = await this.request({
        method: 'POST',
        url: `/api/v4/leads/${lead.id}/notes`,
        data: [
          {
            entity_id: lead.id,
            note_type: 'common',
            params: {
              text: noteText,
            },
          },
        ],
      });

      return {
        status: 'success',
        lead: lead,
        note: noteResult.data,
      };
    }
    return { status: 'error', info: 'Unknown error' };
  }

  async processSupportMessage(data) {
    try {
      const res = await this.httpService
        .request({
          method: 'POST',
          // TODO move endpoint address to .env?
          url: 'https://aynmkgcycj.execute-api.sa-east-1.amazonaws.com/Prod/handle-support-message',
          data: {
            amoDispatcherUrl: this.configService.get('AMO_DISPATCHER_URL'),
            amoDispatcherAccountId: this.configService.get('AMO_DISPATCHER_ACCOUNT_ID'),
            amoDispatcherKey: this.configService.get('AMO_DISPATCHER_KEY'),
            country: config.language === 'pt' ? 'br' : 'mx',
            data: data,
          },
        })
        .toPromise();

      return { status: 'success', lead: res.data.lead, note: res.data.note };
    } catch (err) {
      return { status: 'error', info: err.response && err.response.data ? err.response.data : err };
    }
  }

  getUsedeskLeadData(data: UsedeskWebhookData): {
    pipelineId: number;
    statusId: number;
  } {
    const channelId = data.ticket.channel_id;
    switch (channelId) {
      case UsedeskChannel.STUDENT_SUPPORT:
      case UsedeskChannel.STUDENT_SUPPORT_CHAT:
        return {
          pipelineId: getPipeline(Pipeline.STUDENT_SUPPORT),
          statusId: getStatus(Status.STUDENT_SUPPORT_INBOX),
        };
      case UsedeskChannel.TUTOR_SUPPORT:
      case UsedeskChannel.TUTOR_SUPPORT_CHAT:
      case UsedeskChannel.TUTOR_SUPPORT_MX:
      case UsedeskChannel.TUTOR_SUPPORT_CHAT_MX:
        return {
          pipelineId: getPipeline(Pipeline.TUTOR_SUPPORT),
          statusId: getStatus(Status.TUTOR_SUPPORT_INBOX),
        };
      case UsedeskChannel.ADMIN_SUPPORT:
      case UsedeskChannel.ADMIN_SUPPORT_CHAT:
      case UsedeskChannel.ADMIN_SUPPORT_MX:
      case UsedeskChannel.ADMIN_SUPPORT_CHAT_MX:
        return {
          pipelineId: getPipeline(Pipeline.LMS_BUG),
          statusId: getStatus(Status.LMS_BUG_INBOX),
        };
      default:
        throw new Error(`Unsupported usedesk channel: ${channelId}`);
    }
  }

  async createUsedeskLead(data: UsedeskWebhookData) {
    const { pipelineId, statusId } = this.getUsedeskLeadData(data);

    const phone = data.client.phones[0] ? data.client.phones[0].phone : null;
    const email = data.client.emails[0] ? data.client.emails[0].email : null;
    const name = data.client.name;
    const contact = await this.generateContactInfo({ phone, email, name });

    const amoData = {
      pipeline_id: pipelineId,
      status_id: statusId,
      created_by: 0,
      name: `Usedesk ticket #${data.ticket.id}`,
      _embedded: {
        contacts: [contact],
        tags: [{ id: this.usedeskTagId }],
      },
    } as any;

    const leadRes = await this.request({
      method: 'POST',
      url: '/api/v4/leads/complex',
      data: [amoData],
    });

    let noteText = `Message:\n${data.ticket.message}`;
    if (data.ticket.files.length) {
      noteText += `\n\nFiles:\n${data.ticket.files.join('\n')}`;
    }

    if (leadRes.data.length > 0 && leadRes.data[0].id) {
      const createNoteRes = await this.request({
        method: 'POST',
        url: `/api/v4/leads/${leadRes.data[0].id}/notes`,
        data: [
          {
            entity_id: leadRes.data[0].id,
            note_type: 'common',
            params: { text: noteText },
          },
        ],
      });

      return {
        status: 'success',
        lead: leadRes.data,
        note: createNoteRes.data,
      };
    }
    return { status: 'error', info: leadRes.data };
  }

  async tokenUpdateCycle() {
    await this.tokenUpdate();
    this.crmHandlerUpdate = setInterval(async () => {
      await this.tokenUpdate();
      // Update token once per hour or so (for multiple servers)
    }, (Math.floor(Math.random() * 10) + 60) * 60000);
  }

  async tokenUpdate() {
    await this.initCrm();
    if (!this.crm) {
      return false;
    }
    await this.setToken();
    // Update token once per hour
    if (this.token) {
      const localToken = await this.getLocalToken();
      if (dayjs(localToken.modified).isBefore(dayjs().add(1, 'hour'))) {
        console.log('[AMO] updating token');
        const result = await this.crm.connection.refreshToken();
        if (result?.data?.access_token) {
          await this.setLocalToken(result.data);
          await this.setToken();
        }
      }
    }
  }

  async request(data: AxiosRequestConfig): Promise<AxiosResponse> {
    // Allow AMO requests only for production
    const isProduction = ['lms-production', 'lms-production-mx'].includes(process.env.ENVIRONMENT);
    if (!isProduction) {
      const message = `CRM is disabled for current environment`;
      console.log(message);
      return { data: message } as any;
    }

    const amoDispatcherProxyUrl = this.configService.get('AMO_DISPATCHER_PROXY_URL');
    const amoDispatcherUrl = this.configService.get('AMO_DISPATCHER_URL');
    const amoDispatcherAccountId = this.configService.get('AMO_DISPATCHER_ACCOUNT_ID');
    const amoDispatcherAccountKey = this.configService.get('AMO_DISPATCHER_KEY');

    assert.ok(amoDispatcherProxyUrl);
    assert.ok(amoDispatcherUrl);
    assert.ok(amoDispatcherAccountId);
    assert.ok(amoDispatcherAccountKey);

    // For GET requests, use "params" as "data"
    if (data.params) {
      data.data = data.params;
      delete data.params;
    }

    const res = await this.httpService
      .request({
        url: amoDispatcherProxyUrl,
        method: 'POST',
        data: {
          url: amoDispatcherUrl,
          accountId: amoDispatcherAccountId,
          key: amoDispatcherAccountKey,
          data: data,
        },
      })
      .toPromise();
    return res;
  }

  async findResponsible({ emails, phones, role }: { emails: string[]; phones: string[]; role: SystemRole }) {
    let student: User;

    for (const email of emails) {
      student = await this.usersService.findOneByEmail(email);
      if (student) {
        break;
      }
    }

    if (!student) {
      for (const phone of phones) {
        const studentProfile: Profile = await this.profileService.getProfileAndUserBySearch(phone);
        if (studentProfile) {
          student = studentProfile.user;
          break;
        }
      }
    }

    //Если не нашли студента - выходим
    if (!student) return null;

    //Получаем последний созданный enrollment у студента
    const lastStudentEnrollment: Enrollment = await this.enrollmentsRepository
      .createQueryBuilder('enrollments')
      .leftJoinAndSelect('enrollments.course', 'course')
      .andWhere('enrollments.user = :user', { user: student.id })
      .andWhere(
        `enrollments.active = TRUE
          AND (enrollments.dateFrom IS NULL OR enrollments.dateFrom <= NOW())
          AND (enrollments.dateTo IS NULL OR enrollments.dateTo > NOW())`,
      )
      .orderBy('enrollments.created', 'DESC')
      .getOne();

    if (!lastStudentEnrollment) return null;

    //Получаем ID ответственного
    return this.getResponsibleUserId(lastStudentEnrollment.course, role);
  }
}

export class SetResponsibleOptions {
  body: any;
  role: SystemRole;
  fromStatus: number;
  toStatus: number;
  pipeline: number;
}
