import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { KAFKA_PREFIX } from 'src/helpers';

@Injectable()
export class KafkaClientService {
  constructor(
    @Inject('KAFKA_SERVICE')
    private readonly kafkaClient: ClientKafka,
  ) {}

  emit(topic: string, data: any) {
    let event: string;
    if (typeof data === 'string') {
      event = data;
    } else {
      event = JSON.stringify(data);
    }
    return this.kafkaClient.emit(`${KAFKA_PREFIX}.${topic}`, event);
  }
}
