import { Module } from '@nestjs/common';
import { ClientKafka, ClientsModule, Transport } from '@nestjs/microservices';
import { SASLOptions } from '@nestjs/microservices/external/kafka.interface';
import { KafkaClientService } from './kafka-client.service';

const KAFKA_BROKERS: Array<string> = process.env.KAFKA_CLIENT_BROKERS?.split(',') || ['localhost:9092'];
const KAFKA_CLIENT_SSL = process.env.KAFKA_CLIENT_SSL === 'true';

type TSaslMechanism = 'plain' | 'scram-sha-256' | 'scram-sha-512';
let KAFKA_CLIENT_SASL_MECHANISM = <TSaslMechanism>process.env.KAFKA_CLIENT_SASL_MECHANISM;
if (!['plain', 'scram-sha-256', 'scram-sha-512'].includes(KAFKA_CLIENT_SASL_MECHANISM)) {
  KAFKA_CLIENT_SASL_MECHANISM = 'plain';
}

const KAFKA_CLIENT_SASL: SASLOptions = {
  mechanism: KAFKA_CLIENT_SASL_MECHANISM,
  username: process.env.KAFKA_CLIENT_SASL_USERNAME || '',
  password: process.env.KAFKA_CLIENT_SASL_PASSWORD || '',
};

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'KAFKA_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            brokers: KAFKA_BROKERS,
            ssl: KAFKA_CLIENT_SSL,
            sasl: KAFKA_CLIENT_SASL,
          },
        },
      },
    ]),
  ],
  providers: [KafkaClientService],
  exports: [KafkaClientService],
})
export class KafkaModule {}
