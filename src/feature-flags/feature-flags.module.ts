import { forwardRef, Module } from '@nestjs/common';
import { FeatureFlagsService } from './feature-flags.service';
import { FeatureFlagsController } from './feature-flags.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { FeatureFlagsEntryController } from './feature-flags-entry.controller';
import { FeatureFlagEntry } from '@entities/feature-flag-entry.entity';
import { FeatureFlagsEntryService } from './feature-flags-entry.service';
import { User } from '@entities/user.entity';
import { UsersModule } from '../users/users.module';
import { FeatureFlagRolesEntry } from '@entities/feature-flag-roles-entry.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([FeatureFlag]),
    TypeOrmModule.forFeature([FeatureFlagEntry]),
    TypeOrmModule.forFeature([FeatureFlagRolesEntry]),
    TypeOrmModule.forFeature([User]),
    forwardRef(() => UsersModule),
  ],
  controllers: [
    FeatureFlagsController,
    FeatureFlagsEntryController,
  ],
  providers: [
    FeatureFlagsService,
    FeatureFlagsEntryService,
  ],
  exports: [FeatureFlagsEntryService],
})
export class FeatureFlagsModule {}
