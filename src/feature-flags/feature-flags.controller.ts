import { Controller, Get, Post, Body, Patch, Param, Delete, Query, ValidationPipe } from '@nestjs/common';
import { FeatureFlagsService } from './feature-flags.service';
import { CreateFeatureFlagDto } from './dto/create-feature-flag.dto';
import { UpdateFeatureFlagDto } from './dto/update-feature-flag.dto';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { PgConstraints } from '../pg.decorators';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { QueryFeatureFlagDto } from './dto/query-feature-flag.dto';

@ApiTags('feature-flags')
@ApiBearerAuth()
@Controller('feature-flags')
export class FeatureFlagsController {
  constructor(
    private readonly featureFlagsService: FeatureFlagsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create new feature flag' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  create(@Body() createFeatureFlagDto: CreateFeatureFlagDto) {
    return this.featureFlagsService.create(createFeatureFlagDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get a list of all features' })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findAll() {
    return this.featureFlagsService.findAll();
  }

  @Get('/concat-with-config')
  @ApiOperation({
    summary: 'Get a list of all features concat with config',
  })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findAllConcatWithConfig(
    @Query(
      new ValidationPipe({ transform: true }),
    )
    query: QueryFeatureFlagDto,
  ) {
    return this.featureFlagsService.findAllConcatWithConfig(query);
  }

  @Get(':name/concat-with-config')
  @ApiOperation({ summary: 'Get one feature by name' })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findOneConcatWithConfig(@Param('name') name: FeatureName) {
    return this.featureFlagsService.findOneConcatWithConfig(name);
  }

  @Patch(':name')
  @ApiOperation({ summary: 'Update feature' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  update(
    @Param('name') name: FeatureName,
    @Body() updateFeatureFlagDto: UpdateFeatureFlagDto,
  ) {
    return this.featureFlagsService.update(name, updateFeatureFlagDto);
  }

  @Delete(':name')
  @ApiOperation({ summary: 'Remove feature' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  remove(@Param('name') name: FeatureName) {
    return this.featureFlagsService.remove(name);
  }
}
