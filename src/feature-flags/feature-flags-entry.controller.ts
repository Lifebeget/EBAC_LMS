import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { FeatureFlagsEntryService } from './feature-flags-entry.service';
import { QueryFeatureFlagEntryDto } from './dto/query-feature-flag-entry.dto';
import { UpdateFeatureFlagEntryDto } from './dto/update-feature-flag-entry.dto';
import { CreateFeatureUserListDto } from './dto/create-feature-user-list.dto';
import { Pagination } from '../pagination.decorator';
import { PaginationDto } from '../pagination.dto';
import { CreateFeatureEntryByUserFilterDto } from './dto/create-entry-by-user-filter.dto';
import { CreateFeatureFlagRolesEntryDto } from './dto/create-feature-flag-roles-entry.dto';

@ApiTags('feature-flags-entry')
@ApiBearerAuth()
@Controller('feature-flags-entry')
export class FeatureFlagsEntryController {
  constructor(
    private readonly featureFlagsEntryService: FeatureFlagsEntryService,
  ) {}

  @Post('features/:featureName')
  @ApiOperation({ summary: 'Create feature flags entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  create(
    @Param('featureName') featureName: FeatureName,
    @Body() dto: CreateFeatureUserListDto,
  ) {
    return this.featureFlagsEntryService.create(featureName, dto);
  }

  @Post('features/:featureName/by-user-filter')
  @ApiOperation({ summary: 'Create feature flag entries by user filter' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  createByUserFilter(
    @Param('featureName') featureName: FeatureName,
    @Body() dto: CreateFeatureEntryByUserFilterDto,
  ) {
    return this.featureFlagsEntryService.createByUserFilter(featureName, dto);
  }

  @Post('features/:featureName/by-user-roles')
  @ApiOperation({ summary: 'Create feature flag role entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  createByRolesList(
    @Param('featureName') featureName: FeatureName,
    @Body() dto: CreateFeatureFlagRolesEntryDto,
  ) {
    return this.featureFlagsEntryService.createByRolesList(featureName, dto);
  }

  @Get('features/:featureName/roles')
  @ApiOperation({ summary: 'Get a list of all feature entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findAllRoles(@Param('featureName') featureName: FeatureName) {
    return this.featureFlagsEntryService.findAllRoles(featureName);
  }

  @Get()
  @ApiOperation({ summary: 'Get a list of all feature entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findAll(
    @Query() query: QueryFeatureFlagEntryDto,
    @Pagination() paging: PaginationDto,
  ) {
    return this.featureFlagsEntryService.findAll(paging, query);
  }

  @Get('features/:featureName/users/:userId')
  @ApiOperation({ summary: 'Get one feature entry' })
  @RequirePermissions(Permission.FEATURE_FLAGS_VIEW)
  @PgConstraints()
  findOne(
    @Param('featureName') featureName: FeatureName,
    @Param('userId') userId: string,
  ) {
    return this.featureFlagsEntryService.findOne(featureName, userId);
  }

  @Patch('features/:featureName')
  @ApiOperation({ summary: 'Update feature entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  update(
    @Param('featureName') featureName: FeatureName,
    @Body() dto: UpdateFeatureFlagEntryDto,
  ) {
    return this.featureFlagsEntryService.update(featureName, dto);
  }

  @Delete('features/:featureName/users/:userId')
  @ApiOperation({ summary: 'Remove feature entry' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  removeOne(
    @Param('featureName') featureName: FeatureName,
    @Param('userId') userId: string,
  ) {
    return this.featureFlagsEntryService.removeOne(featureName, userId);
  }

  @Delete('features/:featureName')
  @ApiOperation({ summary: 'Remove feature entries' })
  @RequirePermissions(Permission.FEATURE_FLAGS_MANAGE)
  @PgConstraints()
  removeMany(
    @Param('featureName') featureName: FeatureName,
  ) {
    return this.featureFlagsEntryService.removeMany(featureName);
  }
}
