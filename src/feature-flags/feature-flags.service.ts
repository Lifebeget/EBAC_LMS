import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFeatureFlagDto } from './dto/create-feature-flag.dto';
import { UpdateFeatureFlagDto } from './dto/update-feature-flag.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { QueryFeatureFlagDto } from './dto/query-feature-flag.dto';
import { featuresConfig } from './features.config';

@Injectable()
export class FeatureFlagsService {
  constructor(
    @InjectRepository(FeatureFlag)
    private featureFlagsRepository: Repository<FeatureFlag>,
  ) {}

  create(dto: CreateFeatureFlagDto) {
    return this.featureFlagsRepository.save(dto);
  }

  async findAll() {
    return this.featureFlagsRepository
      .createQueryBuilder('feature')
      .getMany();
  }

  async findAllConcatWithConfig(query: QueryFeatureFlagDto) {
    let featuresFromRepository = await this.featureFlagsRepository.find(query);
    const repositoryFeatureNames = featuresFromRepository.map(
      (feature) => feature.name
    );
    const promises = [];
    for (const feature of featuresConfig) {
      if (!repositoryFeatureNames.includes(feature.name)) {
        const promise = this.create(feature);
        promises.push(promise);
      }
    }
    // Создание фич, которые описаны в конфиге, но которых нет в БД
    if (promises.length > 0) {
      await Promise.all(promises);
      featuresFromRepository = await this.featureFlagsRepository.find(query);
    }
    // Только фичи, описанные в конфиге features.config.ts
    return featuresFromRepository
      .filter(f => {
        return featuresConfig.map(f => f.name).includes(f.name);
      })
      .sort((a, b) => a.name.localeCompare(b.name));
  }

  async findOneConcatWithConfig(name: FeatureName) {
    const repositoryFeature = await this.featureFlagsRepository.findOne({ name });
    const configFeature = featuresConfig.find((feature) => {
      return feature.name === name;
    });
    if (!repositoryFeature && !configFeature) {
      throw new NotFoundException(`Feature ${name} not found`);
    }
    return repositoryFeature || configFeature;
  }

  async update(name: FeatureName, updateFeatureFlagDto: UpdateFeatureFlagDto) {
    await this.checkFeatureExistence(name);
    return this.featureFlagsRepository.save({
      name,
      ...updateFeatureFlagDto,
    });
  }

  async remove(name: FeatureName) {
    await this.checkFeatureExistence(name);
    return this.featureFlagsRepository.softDelete({ name });
  }

  async checkFeatureExistence(name: FeatureName) {
    const repositoryFeature = await this.featureFlagsRepository.findOne({ name });
    const configFeature = featuresConfig.find((feature) => {
      return feature.name === name;
    });
    const feature = repositoryFeature || configFeature;
    if (!feature) {
      throw new NotFoundException(`Feature ${name} not found.`)
    }
  }
}
