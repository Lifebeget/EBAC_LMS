import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { FeatureType } from '@lib/types/enums/feature-flags/feature-type.enum';
import { FeatureFlag } from '@lib/types/feature-flags/feature-flags';
import { FeatureStrategy } from '@lib/types/enums/feature-flags/feature-strategy.enum';

export const featuresConfig: FeatureFlag[] = [
  {
    enabled: false,
    name: FeatureName.CourseBanner,
    description: 'Feature for course banner component',
    type: FeatureType.Release,
    taskUrl: '',
    mergeRequestUrl: '',
    isConfig: true,
    strategy: FeatureStrategy.AllUsers,
  },
  {
    enabled: false,
    name: FeatureName.LessonAnswerSubmitted,
    description: 'Lesson answer submitted button',
    type: FeatureType.Release,
    taskUrl: 'https://ebaconline.atlassian.net/browse/LMSP-847',
    mergeRequestUrl: '',
    isConfig: true,
    strategy: FeatureStrategy.AllUsers,
  },
  {
    enabled: false,
    name: FeatureName.AssessmentAttemptLimits,
    description: 'Attempt limits for assessments',
    type: FeatureType.Release,
    taskUrl: 'https://ebaconline.atlassian.net/browse/LMSP-1141',
    mergeRequestUrl: '',
    isConfig: true,
    strategy: FeatureStrategy.UserIds,
  },
];
