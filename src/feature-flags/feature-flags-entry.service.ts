import { InjectRepository } from '@nestjs/typeorm';
import { FeatureFlagEntry } from '@entities/feature-flag-entry.entity';
import { Repository } from 'typeorm';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { QueryFeatureFlagEntryDto } from './dto/query-feature-flag-entry.dto';
import { UpdateFeatureFlagEntryDto } from './dto/update-feature-flag-entry.dto';
import { featuresConfig } from './features.config';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { CreateFeatureUserListDto } from './dto/create-feature-user-list.dto';
import { Paginated, PaginationDto } from '../pagination.dto';
import { UsersService } from '../users/users.service';
import { CreateFeatureEntryByUserFilterDto } from './dto/create-entry-by-user-filter.dto';
import { CreateFeatureFlagRolesEntryDto } from './dto/create-feature-flag-roles-entry.dto';
import { FeatureFlagRolesEntry } from '@entities/feature-flag-roles-entry.entity';
import { FeatureFlagsService } from './feature-flags.service';

export class FeatureFlagsEntryService {
  constructor(
    @InjectRepository(FeatureFlagEntry)
    private featureFlagEntriesRepository: Repository<FeatureFlagEntry>,
    @InjectRepository(FeatureFlag)
    private featureFlagsRepository: Repository<FeatureFlag>,
    @InjectRepository(FeatureFlagRolesEntry)
    private featureRolesEntryRepository: Repository<FeatureFlagRolesEntry>,
    private readonly usersService: UsersService,
    private readonly featureFlagsService: FeatureFlagsService,
  ) {}

  async create(featureName: FeatureName, dto: CreateFeatureUserListDto) {
    const featureFromConfig = this.checkFeatureExistenceInConfig(featureName);
    const featureFromDb = await this.checkFeatureExistenceInDb(featureName);
    if (!featureFromDb) {
      await this.featureFlagsRepository.save({
        ...featureFromConfig,
        isConfig: undefined,
      });
    }
    const userIds = dto.featureFlagEntries.map((entry) => entry.userId);

    if (dto.resetOldFilter && userIds.length > 0) {
      /* Удаление лишних строк из БД, не входящих в массив
      пользователей, для которых включаем фичу */
      await this.featureFlagEntriesRepository
        .createQueryBuilder('feature_flag_entry')
        .delete()
        .where('feature_flag_entry.feature_name = :featureName', {
          featureName,
        })
        .andWhere('feature_flag_entry.user_id NOT IN (:...userIds)', {
          userIds,
        })
        .execute();
    }
    const entries = dto.featureFlagEntries.map(entry => ({ featureName, ...entry }));
    // Для повышения производительности разобьем процесс вставки на чанки
    const chunk = Math.ceil(entries.length / 1000);
    const result = await this.featureFlagEntriesRepository.save(entries, { chunk });
    return {
      chunk,
      count: result.length,
    }
  }

  async createByRolesList(featureName: FeatureName, dto: CreateFeatureFlagRolesEntryDto) {
    const entries = dto.roles.map(role => ({ featureName, role }));
    await this.featureRolesEntryRepository.delete({ featureName });
    return this.featureRolesEntryRepository.save(entries);
  }

  async findAllRoles(featureName: FeatureName) {
    await this.checkFeatureExistenceInConfig(featureName);
    return this.featureRolesEntryRepository.find({ featureName });
  }

  async createByUserFilter(featureName: FeatureName, dto: CreateFeatureEntryByUserFilterDto) {
    // Количество параметров исключая resetOldFilter
    const paramsCount = Object.keys(dto)?.filter(param => param !== 'resetOldFilter')?.length ?? 0;
    if (paramsCount === 0) {
      throw new BadRequestException('Query params can not be empty');
    }

    const userPool = await this.usersService.findAllWithoutSelectedRelations(dto);
    const entries = userPool.map(user => {
      return {
        userId: user.id,
        enabled: true,
      };
    });
    const entriesPayload: CreateFeatureUserListDto = {
      featureFlagEntries: entries,
      resetOldFilter: dto.resetOldFilter,
    };
    return this.create(featureName, entriesPayload);
  }

  async findAll(
    paging: PaginationDto,
    query: QueryFeatureFlagEntryDto,
  ): Promise<Paginated<FeatureFlagEntry[]>> {
    const queryBuilder = this.featureFlagEntriesRepository.createQueryBuilder('entry');

    if (query.featureName) {
      queryBuilder.where('entry.featureName = :featureName', {
        featureName: query.featureName,
      });
    }
    if (query.userId) {
      queryBuilder.where('entry.userId = :userId', {
        userId: query.userId,
      });
    }
    if (query.detailUsers) {
      queryBuilder
        .leftJoinAndSelect('entry.user', 'user')
        .orderBy('user.name');

      this.usersService.joinAndSelectUserRelations(queryBuilder, query);
      await this.usersService.applyUserFilters(queryBuilder, query);
    }
    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    const [results, total] = await queryBuilder.getManyAndCount();
    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findOne(featureName: FeatureName, userId: string) {
    await Promise.all([
      this.usersService.checkUser(userId),
      this.featureFlagsService.findOneConcatWithConfig(featureName),
    ]);
    const entry = await this.featureFlagEntriesRepository.findOne({
      featureName,
      userId,
    });
    if (!entry) {
      throw new NotFoundException(`Feature entry not found`);
    }
    return entry;
  }

  update(featureName: FeatureName, dto: UpdateFeatureFlagEntryDto) {
    const entity = dto.featureFlagEntries.map((entry) => ({ featureName, ...entry }));
    return this.featureFlagEntriesRepository.save(entity);
  }

  async removeOne(featureName: FeatureName, userId: string) {
    const [featureFromDb] = await Promise.all([
      this.checkFeatureExistenceInDb(featureName),
      this.usersService.checkUser(userId),
    ]);
    if (!featureFromDb) {
      throw new NotFoundException(`Feature ${featureName} not found`);
    }
    return this.featureFlagEntriesRepository.delete({
      featureName,
      userId,
    });
  }

  async removeMany(featureName: FeatureName) {
    const featureFromDb = await this.checkFeatureExistenceInDb(featureName);
    if (!featureFromDb) {
      throw new NotFoundException(`Feature ${featureName} not found`);
    }
    return this.featureFlagEntriesRepository.delete({ featureName });
  }

  checkFeatureExistenceInConfig(featureName: FeatureName) {
    const feature = featuresConfig.find((feature) => {
      return feature.name === featureName;
    });
    if (!feature) {
      throw new NotFoundException(`Feature ${featureName} not found in config`);
    }
    return feature;
  }
  checkFeatureExistenceInDb(featureName: FeatureName) {
    return this.featureFlagsRepository.findOne({
      name: featureName,
    });
  }
}
