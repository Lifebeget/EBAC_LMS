import { CreateFeatureFlagEntryDto } from './create-feature-flag-entry.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateFeatureUserListDto {
  @ApiProperty({
    description: 'Feature flag entries list',
    type: () => [CreateFeatureFlagEntryDto],
    required: true,
  })
  @IsNotEmpty()
  @Type(() => CreateFeatureFlagEntryDto)
  @ValidateNested({ each: true })
  featureFlagEntries: CreateFeatureFlagEntryDto[];

  @ApiProperty({
    description: 'Remove old user list for feature and create new',
    required: false,
  })
  @IsBoolean()
  @IsOptional()
  resetOldFilter?: boolean;
}
