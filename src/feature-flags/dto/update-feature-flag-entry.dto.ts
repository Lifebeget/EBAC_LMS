import { CreateFeatureUserListDto } from './create-feature-user-list.dto';
import { OmitType } from '@nestjs/swagger';

export class UpdateFeatureFlagEntryDto extends OmitType(CreateFeatureUserListDto, ['resetOldFilter']) {}
