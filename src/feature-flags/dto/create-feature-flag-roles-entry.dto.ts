import { IsArray, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { AppRole } from '@lib/types/enums';

export class CreateFeatureFlagRolesEntryDto {
  @ApiProperty({ description: 'Roles list for feature' })
  @IsArray()
  @IsEnum(AppRole, { each: true })
  roles: AppRole[];
}
