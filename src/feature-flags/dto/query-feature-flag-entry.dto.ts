import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsEnum, IsOptional, IsString } from 'class-validator';
import { QueryUserDto } from '../../users/dto/query-user.dto';

export class QueryFeatureFlagEntryDto extends QueryUserDto {
  @ApiProperty({
    description: 'User ID filter',
    required: false,
  })
  @IsString()
  @IsOptional()
  userId?: string;

  @ApiProperty({
    description: 'Feature name filter',
    required: false,
  })
  @IsEnum(FeatureName)
  @IsOptional()
  featureName?: FeatureName;

  @ApiProperty({
    description: 'Show users detail',
    required: false,
  })
  @IsBooleanString()
  @IsOptional()
  detailUsers?: boolean;
}
