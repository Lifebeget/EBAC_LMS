import { QueryUserDto } from '../../users/dto/query-user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';

export class CreateFeatureEntryByUserFilterDto extends QueryUserDto {
  @ApiProperty({ description: 'Reset old filter', required: false })
  @IsBoolean()
  @IsOptional()
  resetOldFilter?: boolean;
}
