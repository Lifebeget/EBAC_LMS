import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsNotEmpty, IsOptional, IsString, IsUrl } from 'class-validator';
import { FeatureType } from '@lib/types/enums/feature-flags/feature-type.enum';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { FeatureStrategy } from '@lib/types/enums/feature-flags/feature-strategy.enum';

export class CreateFeatureFlagDto {
  @ApiProperty({
    description: 'Feature name',
    enum: FeatureName,
  })
  @IsEnum(FeatureName)
  @IsNotEmpty()
  name: FeatureName;

  @ApiProperty({ description: 'Whether the feature flag is enabled' })
  @IsBoolean()
  @IsNotEmpty()
  enabled: boolean;

  @ApiProperty({ description: 'Feature flag strategy' })
  @IsEnum(FeatureStrategy)
  @IsNotEmpty()
  strategy: FeatureStrategy;

  @ApiProperty({ description: 'Feature description' })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    description: 'Feature type',
    enum: FeatureType
  })
  @IsEnum(FeatureType)
  @IsOptional()
  type?: FeatureType;

  @ApiProperty({ description: 'Feature task URL' })
  @IsUrl()
  @IsOptional()
  taskUrl?: string;

  @ApiProperty({ description: 'Feature merge request URL' })
  @IsUrl()
  @IsOptional()
  mergeRequestUrl?: string;
}
