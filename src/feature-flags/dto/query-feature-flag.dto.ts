import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { FeatureStrategy } from '@lib/types/enums/feature-flags/feature-strategy.enum';

export class QueryFeatureFlagDto {
  @ApiProperty({
    description: 'Active filter',
    required: false,
  })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @ApiProperty({
    description: 'Feature strategy filter',
    required: false,
  })
  @IsEnum(FeatureStrategy)
  @IsOptional()
  strategy?: FeatureStrategy;
}
