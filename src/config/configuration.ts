export default () => ({
  submission: {
    urgentHours: 24,
    outdatedHours: 72,
    pauseHours: 3,
  },
  forumResolve: {
    urgentHours: 24,
    outdatedHours: 72,
  },
  date: {
    weekendDays: [0, 6],
  },
});
