import { InjectQueue, Process, Processor } from '@nestjs/bull';
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Job, Queue } from 'bull';
import { Request } from 'express';
import { tap } from 'rxjs/operators';
import { appPromise } from './app';
import { CustomProcess } from './decorators/custom-process.decorator';
import { logTagKey } from './service/log/logging';
import { UsersService } from './users/users.service';

@Processor('activity')
@Injectable()
export class ActivityInterceptor implements NestInterceptor {
  constructor(private reflector: Reflector, @InjectQueue('activity') private activityQueue: Queue) {}

  intercept(context: ExecutionContext, next: CallHandler) {
    const handle = next.handle();

    if (context.getType() !== 'http') {
      return handle;
    }

    const req: Request = context.switchToHttp().getRequest();

    const fn = async () => {
      const user = req.user;
      const handlerTag = this.reflector.get(logTagKey, context.getHandler()) || context.getHandler().name;

      if (user?.id) {
        if (['lectureViews', 'login'].includes(handlerTag)) {
          await this.activityQueue.add(
            { userId: user.id, activity: handlerTag },
            {
              removeOnComplete: true,
              delay: 0,
            },
          );
        } else {
          const job = await this.activityQueue.getJob(user.id);
          if (job) {
            try {
              await job.remove();
            } catch (e) {
              console.error(`activityQueue remove job failed ${job.id}`);
            }
          }
          try {
            await this.activityQueue.add(
              { userId: user.id, activity: handlerTag },
              {
                jobId: user.id,
                removeOnComplete: true,
                //1 min
                delay: 60000,
              },
            );
          } catch (e) {
            console.error(`activityQueue add job failed`);
            console.debug(e);
          }
        }
      }
    };

    return handle.pipe(tap(undefined, fn, fn));
  }

  @CustomProcess()
  async consumeActivityWrite(
    job: Job<{
      userId: string;
      activity: string;
    }>,
  ) {
    const app_ = await appPromise;
    const usersService = app_.get(UsersService);

    return await usersService
      .saveActivity(job.data.userId, { activity: job.data.activity })
      .catch(err => console.error('Failed to save user activity\n', err));
  }
}
