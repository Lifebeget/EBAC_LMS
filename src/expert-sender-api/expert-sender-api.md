# expert-sender-api

## Описание

Является клиентом к сервису [expert-sender](https://service7.esv2.com/) email рассылок.

Клиент содержит модули:

| name | description |
| --- | --- |
| subscribers-expert-sender | Реализует работу с [подписчиками](https://service7.esv2.com/Lists/List) api [методы](https://manual.expertsender.ru/metody/podpischiki/dobavlenie-podpischika-v-list) |
| tables-expert-sender | Реализует работу с [таблицами](https://service7.esv2.com/DataTables/Tables) api [методы](https://manual.expertsender.ru/metody/tablitsy-dannykh/dobavit-zapis) |
| transactionals-expert-sender | Реализует работу с [транзакционными письмами](https://service7.esv2.com/Transactional/List) api [методы](https://manual.expertsender.ru/metody/dejstviya-s-pismami/otpravit-tranzaktsionnoe-soobshchenie-podpischiku) |

## ENV

- EXPERTSENDER_API_KEY - ключ доступа к api `expert-sender`
  смотреть [здесь](https://service7.esv2.com/Settings/ServiceSettings)
- EXPERTSENDER_API_URL - http путь к api `expert-sender`
  смотреть [здесь](https://service7.esv2.com/Settings/ServiceSettings)

## Инжектирование клиента

Перед тем как инжектировать клиента в сервис (микросервис), убедись что проставлены `env` переменные и установлен npm
пакет `xmlbuilder2`

- Импортируем `ExpertSenderApiModule` в модульном файле модуля

```ts
import {ExpertSenderApiModule} from '../../../../../libs/http-api/clients/expert-sender-api/expert-sender-api.module';

@Module({
  imports: [
    ExpertSenderApiModule,
  ],
  providers: [CouponsService],
  exports: [CouponsService],
})
export class CouponsModule {
}
```

- Инжектируем `expertSenderApiService` в сервисном файле модуля, используя интерфейс `ExpertSenderApiServiceInterface` и
  константу имени `EXPERT_SENDER_API_SERVICE`

```ts
import {EXPERT_SENDER_API_SERVICE} from '../../../../../libs/http-api/clients/expert-sender-api/constants/constants';
import {ExpertSenderApiServiceInterface} from '../../../../../libs/http-api/clients/expert-sender-api/interfaces/expert-sender-api-service.interface';

@Injectable()
export class CouponsService implements CouponsServiceInterface {
  constructor(
    @Inject(EXPERT_SENDER_API_SERVICE)
    private readonly expertSenderApiService: ExpertSenderApiServiceInterface,
  ) {
  }
}
```

## Методы

### upsertSubscriber

Добавить или обновить подписчика,
использует [http метод](https://manual.expertsender.ru/metody/podpischiki/dobavlenie-podpischika-v-list). Если у
дополнительного поля установлен тип `date`, то метод конвертирует дату

Параметры:

| name | type | description |
| --- | --- | --- |
| [apiKey] | string | Ключ доступа к api `expert-sender`, если не задан, то берет из `EXPERTSENDER_API_KEY` |
| [returnData] | boolean | Если установлен `true`, то будет возвращена дополнительная информация |
| [VerboseErrors] | boolean | Если установлен в `true`, то будет возвращена расширенная информация об ошибках |
| subscriber | Subscriber | Подписчик или массив подписчиков |
| [context] | string | Уникальное значение сквозного логирования (внутрисервисного или межсервисного) операции |
| [user] | object | Пользователь вызвавший метод |

Subscriber:

| name | type | description |
| --- | --- | --- |
| [mode] | enum | Методы добавления, по умолчанию используется `AddAndUpdate` |
| [force] | boolean | Если установлено значение `true`, то при повторном запросе на добавление подписчика отправится еще одно письмо подтверждения. Используется только для листов с подтверждением подписки и для неподтвержденных пользователей. По умолчанию `false` |
| listId | number | Идентификатор листа, в который добавляем подписчика |
| [allowUnsubscribed] | boolean | При значении `false` система не позволит добавить подписчика, который ранее отписался. По умолчанию `true` |
| [allowRemoved] | boolean | При значении `false` система не позволит добавить подписчика, который ранее был удален пользователем. По умолчанию `true` |
| [matchingMode] | enum | Метод согласования подписчиков. Это поле будет использовано как основной идентификатор подписчика. По умолчанию `email` |
| [id] | number | Идентификатор подписчика. Укажите его, если хотите изменить `email` подписчика |
| email | string | `email` подписчика |
| [emailMd5] | string | MD5-хэш адреса подписчика |
| [phone] | string | Номер телефона подписчика |
| [customSubscriberId] | string | ID подписчика |
| [firstname] | string | Имя подписчика |
| [lastname] | string | Фамилия подписчика |
| [trackingCode] | string | Идентификатор источника подписчиков (например, конкретная форма на веб-сайте) |
| [name] | string | Полное имя. Можно использовать, когда невозможно отделить имя и фамилию. Шаблон — «Имя Фамилия» |
| [vendor] | string | Идентификатор/имя типа трафика, из которого пришел подписчик |
| [ip] | string | IP-адрес подписчика |
| [properties] | Property | Дополнительные параметры |

Property:

| name | type | description |
| --- | --- | --- |
| id | number | id дополнительного поля. Список дополнительных [полей](https://service7.esv2.com/Property/List). Можно выбрать одно из предопределенных в перечислении `AdditionalFieldsSubscribersExpertSender` или прописать вручную. Если на сайте меняется список, то желательно синхронизировать перечисление в `backend/libs/http-api/clients/expert-sender-api/subscribers-expert-sender/enums/enums.ts` |
| value | any | Значение дополнительного поля |
| type | enum | Тип дополнительного поля. Типы предопределены в `XmlTypeExpertSender` находится в `backend/libs/http-api/clients/expert-sender-api/enums/enums.ts` |

В ответ возвращает объект, который вернул `expert-sender`

Пример:

```ts
    const response: LooseObjectInterface = await this.expertSenderApiService.upsertSubscriber(
  {
    returnData: true,
    verboseErrors: true,
    subscriber: {
      listId: 1,
      email: 'test@test.tst',
      firstname: 'aaa',
      lastname: 'bbb',
      properties: [
        {
          id: AdditionalFieldsSubscribersExpertSender.date,
          type: XmlTypeExpertSender.dateTime,
          value: new Date(),
        },
      ],
    },
  },
);
```

### Методы таблиц

[Http методы](https://manual.expertsender.ru/metody/podpischiki/dobavlenie-podpischika-v-list)

[Список таблиц](https://service7.esv2.com/DataTables/Tables)

Каждому табличному методу, требуется задавать тип таблицы, с которой он будет работать. Типизация основана на таблицах
из [списка](https://service7.esv2.com/DataTables/Tables). Если меняется или добавляется таблица в `expert-sender`, то
желательно синхронизировать типизацию.

Описание типов таблиц находится
в `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/interfaces`

Названия таблиц находится в `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/enums/enums.ts`

Список уникальных полей в перечислении `primaryKeyFieldsTablesExpertSender`
расположен `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/enums/enums.ts`

Пример `upsertRow<NomenclatureTableInterface>(params)`. Но если требуется пренебречь типизацией, новая таблица не
типизирована или типизация не синхронизирована, то можно использовать тип js
объекта `upsertRow<LooseObjectInterface>(params)`

`expert-sender` использует `snake_case`, типизация использует `camelCase`, это нормально т.к. методы работы с таблицей
конвертируют названия полей таблицы в `snake_case`.

#### upsertRow

Добавить или обновить запись в таблицу. Нативного метода в `expert-sender` нет, поэтому он реализован из трех нативных
методов [получить записи по фильтру](https://manual.expertsender.ru/metody/tablitsy-dannykh/poluchit-dannye-iz-tablitsy)
, [добавить запись](https://manual.expertsender.ru/metody/tablitsy-dannykh/dobavit-zapis)
, [обновить запись](https://manual.expertsender.ru/metody/tablitsy-dannykh/obnovit-zapis)

Метод требует задать тип `<T>`
таблицы `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/interfaces`
или `LooseObjectInterface`

Параметры:

| name | type | description |
| --- | --- | --- |
| [apiKey] | string | Ключ доступа к api `expert-sender`, если не задан, то берет из `EXPERTSENDER_API_KEY` |
| tableName | string | Название таблицы, либо прописать вручную, либо выбрать одно из `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/enums/enums.ts` |
| row | `<T>` | Строка таблицы, `js object` key - название поля таблицы, value - значение поля таблицы. Если задан тип, поля будут выводиться автоматически |
| [primaryKeyFields] | string | уникальное поле или список уникальных полей, по которым проверяется существует ли такая запись в таблице. Если параметр не задан, то значение определяется методом `getPrimaryKeys` в `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables-expert-sender.service.ts` |
| [context] | string | Уникальное значение сквозного логирования (внутрисервисного или межсервисного) операции |
| [user] | object | Пользователь вызвавший метод |

Ничего не возвращает, вызывает исключение, если не удалось добавить или обновить запись в таблице.

Пример:

```ts
    try {
  await this.expertSenderApiService.upsertRow<NomenclatureTableInterface>({
    tableName: TableNamesTablesExpertSender.nomenclature,
    row: {
      id: 77,
      name: 'Name',
      shortName: 'shortName', // наименование поля будет преобразовано в short_name
    },
  });
} catch (error) {
  console.error(error);
}
```

#### getRows

Получить записи из таблицы по фильтру,
используется [http метод](https://manual.expertsender.ru/metody/tablitsy-dannykh/poluchit-dannye-iz-tablitsy)

Метод требует задать тип `<T>`
таблицы `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/interfaces`
или `LooseObjectInterface`

Параметры:

| name | type | description |
| --- | --- | --- |
| [apiKey] | string | Ключ доступа к api `expert-sender`, если не задан, то берет из `EXPERTSENDER_API_KEY` |
| tableName | string | Название таблицы, либо прописать вручную, либо выбрать одно из `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/enums/enums.ts` |
| [fields] | string[] | Массив полей которые требуется вернуть, если не задан, то возвращаются все |
| [whereConditions] | WhereCondition[] | Массив условий по которым требуется отобрать записи |
| [orderByFields] | Order[] | Список полей по которым сортировать, и направление возрастание/убывание |
| [limit] | number | Количество записей |
| [context] | string | Уникальное значение сквозного логирования (внутрисервисного или межсервисного) операции |
| [user] | object | Пользователь вызвавший метод |

WhereCondition:

| name | type | description |
| --- | --- | --- |
| fieldName | string | Наименование поля по которому требуется фильтровать, может быть `snake_case` или `camelCase` |
| operator | enum | Оператор условия |
| value | type | значение сравнения |

Order:

| name | type | description |
| --- | --- | --- |
| fieldName | string | Наименование поля по которому требуется сортировать, может быть `snake_case` или `camelCase` |
| [direction] | enum | по возрастанию или убыванию, если не задано, то по возрастанию |

Возвращает массив записей, каждая запись представляет собой объект заданного типа `<T>`, все наименования полей
преобразуются в `camelCase`

Пример:

```ts
    const rows: NomenclatureTableInterface[] = await this.expertSenderApiService.getRows<NomenclatureTableInterface>(
  {
    tableName: TableNamesTablesExpertSender.nomenclature,
    whereConditions: [
      {
        fieldName: 'id',
        operator: OperatorTablesExpertSender.Equals,
        value: 1,
      },
    ],
    orderByFields: [
      {
        fieldName: 'name',
        direction: DirectionTablesExpertSender.Descending,
      },
    ],
  },
);
```

## sendTransactionalToSubscriber

Отправить транзакционное письмо подписчику,
используется [http метод](https://manual.expertsender.ru/metody/dejstviya-s-pismami/otpravit-tranzaktsionnoe-soobshchenie-podpischiku)
, [список транзакционных писем](https://service7.esv2.com/Transactional/List)

Параметры:

| name | type | description |
| --- | --- | --- |
| name | type | description |
| [apiKey] | string | Ключ доступа к api `expert-sender`, если не задан, то берет из `EXPERTSENDER_API_KEY` |
| id | number | id транзакционного сообщения |
| [returnGuid] | boolean | Если установлен в `true` позволяет получить GUID отправляемого письма |
| receiver | Receiver | Определяет подписчика, который получит транзакционное письмо |
| [snippets] | Snippets | Параметры которые будут подставляться в шаблон письма. `js object` key - название переменной в шаблоне, value - подставляемое значение в переменную. Объект может быть любой, либо из уже предопределенных переменных в `backend/libs/http-api/clients/expert-sender-api/transactionals-expert-sender/interfaces/snippets-transactionals-expert-sender.interface.ts`. Названия переменных преобразуются в `snake_case` |
| [attachments] | Attachment[] | Файлы которые будут прикреплены к письму |
| [context] | string | Уникальное значение сквозного логирования (внутрисервисного или межсервисного) операции |
| [user] | object | Пользователь вызвавший метод |

Receiver:

| name | type | description |
| --- | --- | --- |
| [id] | number | Идентификатор подписчика|
| [email] | string | `email` подписчика |
| [emailMd5] | string | MD5-хэш адреса подписчика |
| [listId] | number | Идентификатор листа подписчика. Используется для определения листа используемого для отправки. Это полезно, если подписчик присутствует в нескольких листах. Если не задано, будет использоваться первый найденный лист |
| [bcc] | string | Адрес электронной почты, на который будет отправляться скрытая копия сообщения |

Attachment:

| name | type | description |
| --- | --- | --- |
| fileName | string | Наименование файла |
| [mimeType] | string | Либо заполняется строкой, либо одним из предопределенных значений `backend/libs/http-api/clients/expert-sender-api/transactionals-expert-sender/enums/enums.ts` |
| content | string | Содержимое файла вложения. Должно быть кодировано в Base64 |

В ответ возвращает объект, который вернул `expert-sender`

Пример:

```ts
    const response: LooseObjectInterface = await this.expertSenderApiService.sendTransactionalToSubscriber(
  {
    id: 1328,
    returnGuid: true,
    receiver: {email: 'test@test.tst'},
    snippets: {clientName: 'hello', paymentLink: 'link'},
    attachments: [
      {
        fileName: 'file.png',
        mimeType: MimeTypeTransactionalsExpertSender.png,
        content: 'base64',
      },
    ],
  },
);
```

## Чек листы

### Дополнительные поля подписчиков

- [ ] Проверить список дополнительных полей https://service7.esv2.com/Property/List
- [ ] Убедиться что поле есть в перечислении `AdditionalFieldsSubscribersExpertSender`
  путь `backend/libs/http-api/clients/expert-sender-api/subscribers-expert-sender/enums/enums.ts`
- [ ] Убедиться что `id` совпадает со значением перечисления

### Таблица

- [ ] Проверить список таблиц https://service7.esv2.com/DataTables/Tables
- [ ] Убедиться что имя таблицы есть в перечислении `TableNamesTablesExpertSender`
  путь `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/enums/enums.ts` имя желательно
  писать в `camelCase`
- [ ] Убедиться что существует интерфейс описывающий структуру таблицы, убедиться что типы полей
  совпадают `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables/interfaces` желательно писать
  в `camelCase`
- [ ] Убедиться что существует уникальный ключ таблицы в перечислении `primaryKeyFieldsTablesExpertSender`
  путь `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/enums/enums.ts`
- [ ] Убедиться что есть константа уникального ключа таблицы
  в `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/constants/constants.ts`
- [ ] Убедиться что константа уникального ключа используется в методе `getDefaultPrimaryKeyField`
  путь `backend/libs/http-api/clients/expert-sender-api/tables-expert-sender/tables-expert-sender.service.ts`

### Транзакционное письмо

- [ ] Посмотреть переменные(snippets) в транзакционных письмах https://service7.esv2.com/Transactional/List
- [ ] Убедиться что переменные есть в
  интерфейсе `backend/libs/http-api/clients/expert-sender-api/transactionals-expert-sender/interfaces/snippets-transactionals-expert-sender.interface.ts`
  желательно писать в `camelCase`
- [ ] При необходимости добавить новый mimeType
  в `backend/libs/http-api/clients/expert-sender-api/transactionals-expert-sender/enums/enums.ts`  