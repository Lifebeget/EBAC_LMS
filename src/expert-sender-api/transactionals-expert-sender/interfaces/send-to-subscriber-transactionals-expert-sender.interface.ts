import { LooseObjectInterface } from '../../interfaces/loose-object.interface';
import { ReceiverTransactionalsExpertSenderInterface } from './receiver-transactionals-expert-sender.interface';
import { SnippetsTransactionalsExpertSenderInterface } from './snippets-transactionals-expert-sender.interface';
import { AttachmentTransactionalsExpertSenderInterface } from './attachment-transactionals-expert-sender.interface';

export interface SendToSubscriberTransactionalsExpertSenderInterface {
  apiKey?: string;
  id: number;
  returnGuid?: boolean;
  receiver: ReceiverTransactionalsExpertSenderInterface;
  snippets?: SnippetsTransactionalsExpertSenderInterface | LooseObjectInterface;
  attachments?: AttachmentTransactionalsExpertSenderInterface[];

  context?: string;
  user?: LooseObjectInterface | string;
}
