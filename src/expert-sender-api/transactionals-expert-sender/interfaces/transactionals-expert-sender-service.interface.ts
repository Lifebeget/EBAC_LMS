import { SendToSubscriberTransactionalsExpertSenderInterface } from './send-to-subscriber-transactionals-expert-sender.interface';
import { LooseObjectInterface } from '../../interfaces/loose-object.interface';

export interface TransactionalsExpertSenderServiceInterface {
  sendToSubscriber(params: SendToSubscriberTransactionalsExpertSenderInterface): Promise<LooseObjectInterface>;
}
