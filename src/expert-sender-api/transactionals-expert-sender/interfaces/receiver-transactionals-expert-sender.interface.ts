export interface ReceiverTransactionalsExpertSenderInterface {
  id?: number;
  email?: string;
  emailMd5?: string;
  listId?: number;
  bcc?: string;
}
