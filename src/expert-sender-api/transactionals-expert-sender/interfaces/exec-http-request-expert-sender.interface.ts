import { HttpEntityTransactionalsExpertSender } from '../enums/enums';

export interface ExecHttpRequestExpertSenderInterface {
  id: number;
  xml: string;
  httpEntity: HttpEntityTransactionalsExpertSender;

  context?: string;
}
