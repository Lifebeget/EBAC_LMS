export interface SnippetsTransactionalsExpertSenderInterface {
  paymentLink?: string | number | boolean | Date;
  orderCrmId?: string | number | boolean | Date;
  clientName?: string | number | boolean | Date;
  courseMpPaymentLink?: string | number | boolean | Date;
  courseName?: string | number | boolean | Date;
  personalManagerWhatsapp: string | number | boolean | Date;
  paymentProducts?: string | number | boolean | Date;
  paymentTotal?: string | number | boolean | Date;
  courseLink?: string | number | boolean | Date;
}
