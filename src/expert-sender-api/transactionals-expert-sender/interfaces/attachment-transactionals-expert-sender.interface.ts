import { MimeTypeTransactionalsExpertSender } from '../enums/enums';

export interface AttachmentTransactionalsExpertSenderInterface {
  fileName: string;
  mimeType?: MimeTypeTransactionalsExpertSender | string;
  content: string;
}
