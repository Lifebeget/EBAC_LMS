export enum HttpEntityTransactionalsExpertSender {
  Transactionals = 'Transactionals',
}

export enum MimeTypeTransactionalsExpertSender {
  pdf = 'application/pdf',
  audioMp4 = 'audio/mp4',
  mp3 = 'audio/mpeg',
  audioOgg = 'audio/ogg',
  gif = 'image/gif',
  jpeg = 'image/jpeg',
  png = 'image/png',
  webp = 'image/webp',
  mpeg = 'video/mpeg',
  videoMp4 = 'video/mp4',
  videoOgg = 'video/ogg',
  webm = 'video/webm',
  word = 'application/msword',
  excel = 'application/vnd.ms-excel',
}
