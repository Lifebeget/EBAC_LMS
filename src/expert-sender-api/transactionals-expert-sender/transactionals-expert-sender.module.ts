import { HttpModule, Module } from '@nestjs/common';
import { TransactionalsExpertSenderService } from './transactionals-expert-sender.service';

@Module({
  imports: [HttpModule],
  providers: [TransactionalsExpertSenderService],
  exports: [TransactionalsExpertSenderService, HttpModule],
})
export class TransactionalsExpertSenderModule {}
