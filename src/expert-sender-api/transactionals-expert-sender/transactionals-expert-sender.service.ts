import { HttpService, Injectable } from '@nestjs/common';
import { SendToSubscriberTransactionalsExpertSenderInterface } from './interfaces/send-to-subscriber-transactionals-expert-sender.interface';
import { generateContext } from '../utils/generateContext';
import { WHO_COMPLETED } from './constants/constants';
import { createApiRequest } from '../utils/createApiRequest';
import { HttpEntityTransactionalsExpertSender } from './enums/enums';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';
import { ReceiverTransactionalsExpertSenderInterface } from './interfaces/receiver-transactionals-expert-sender.interface';
import { isEmpty } from '../utils/isEmpty';
import { valueToSnakeCase } from '../utils/toSnakeCase';
import { SnippetsTransactionalsExpertSenderInterface } from './interfaces/snippets-transactionals-expert-sender.interface';
import { LooseObjectInterface } from '../interfaces/loose-object.interface';
import { AttachmentTransactionalsExpertSenderInterface } from './interfaces/attachment-transactionals-expert-sender.interface';
import { convert } from 'xmlbuilder2';
import { ExecHttpRequestExpertSenderInterface } from './interfaces/exec-http-request-expert-sender.interface';
import { TransactionalsExpertSenderServiceInterface } from './interfaces/transactionals-expert-sender-service.interface';

@Injectable()
export class TransactionalsExpertSenderService implements TransactionalsExpertSenderServiceInterface {
  constructor(private readonly httpService: HttpService) {}

  // https://manual.expertsender.ru/metody/dejstviya-s-pismami/otpravit-tranzaktsionnoe-soobshchenie-podpischiku
  async sendToSubscriber(params: SendToSubscriberTransactionalsExpertSenderInterface): Promise<LooseObjectInterface> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const { root, apiRequest } = createApiRequest(params.apiKey);
      this.fillData(params, apiRequest);
      const xml: string = root.end({ prettyPrint: true });

      const response: LooseObjectInterface = await this.execHttpRequest({
        id: params.id,
        httpEntity: HttpEntityTransactionalsExpertSender.Transactionals,
        xml,
        context,
      });

      return response;
    } catch (error) {
      console.error({
        message: 'TransactionalsExpertSenderService.sendToSubscriber error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private fillData(params: SendToSubscriberTransactionalsExpertSenderInterface, apiRequest: XMLBuilder): void {
    const xmlData: XMLBuilder = apiRequest.ele('Data');

    this.fillReturnGuid(params.returnGuid, xmlData);
    this.fillReceiver(params.receiver, xmlData);
    this.fillSnippets(params.snippets, xmlData);
    this.fillAttachments(params.attachments, xmlData);

    xmlData.up();
  }

  private fillReturnGuid(returnGuid: boolean, xmlData: XMLBuilder): void {
    const strReturnGuid: string = returnGuid ? 'true' : 'false';
    xmlData.ele('ReturnGuid').txt(strReturnGuid).up();
  }

  private fillReceiver(receiver: ReceiverTransactionalsExpertSenderInterface, xmlData: XMLBuilder): void {
    const xmlReceiver: XMLBuilder = xmlData.ele('Receiver');

    if (!isEmpty<number>(receiver.id)) {
      xmlReceiver.ele('Id').txt(String(receiver.id)).up();
    }

    if (!isEmpty<string>(receiver.email)) {
      xmlReceiver.ele('Email').txt(String(receiver.email)).up();
    }

    if (!isEmpty<string>(receiver.emailMd5)) {
      xmlReceiver.ele('EmailMd5').txt(String(receiver.emailMd5)).up();
    }

    if (!isEmpty<number>(receiver.listId)) {
      xmlReceiver.ele('ListId').txt(String(receiver.listId)).up();
    }

    if (!isEmpty<string>(receiver.bcc)) {
      xmlReceiver.ele('Bcc').txt(String(receiver.bcc)).up();
    }

    xmlReceiver.up();
  }

  private fillSnippets(snippets: SnippetsTransactionalsExpertSenderInterface | LooseObjectInterface, xmlData: XMLBuilder): void {
    if (!snippets) {
      return;
    }

    const snippetNames: string[] = Object.keys(snippets);
    const xmlSnippets: XMLBuilder = xmlData.ele('Snippets');
    for (const snippetName of snippetNames) {
      const value = String(snippets[snippetName]);
      this.fillSnippet(snippetName, value, xmlSnippets);
    }
  }

  private fillSnippet(snippetName, value, xmlSnippets: XMLBuilder): void {
    const xmlSnippet: XMLBuilder = xmlSnippets.ele('Snippet');
    xmlSnippet.ele('Name').txt(valueToSnakeCase(snippetName)).up();
    xmlSnippet.ele('Value').txt(value).up();
    xmlSnippet.up();
  }

  private fillAttachments(attachments: AttachmentTransactionalsExpertSenderInterface[], xmlData: XMLBuilder): void {
    if (isEmpty<AttachmentTransactionalsExpertSenderInterface>(attachments)) {
      return;
    }

    const xmlAttachments: XMLBuilder = xmlData.ele('Attachments');
    attachments.forEach(attachment => this.fillAttachment(attachment, xmlAttachments));
  }

  private fillAttachment(attachment: AttachmentTransactionalsExpertSenderInterface, xmlAttachments: XMLBuilder): void {
    const xmlAttachment: XMLBuilder = xmlAttachments.ele('Attachment');
    xmlAttachment.ele('FileName').txt(attachment.fileName).up();

    if (attachment.mimeType) {
      xmlAttachment.ele('MimeType').txt(attachment.mimeType).up();
    }

    xmlAttachment.ele('Content').txt(attachment.content).up();
    xmlAttachment.up();
  }

  private async execHttpRequest(params: ExecHttpRequestExpertSenderInterface): Promise<LooseObjectInterface> {
    try {
      const httpApiPath: string = process.env.EXPERTSENDER_API_URL;
      const url = `${httpApiPath}/${params.httpEntity}/${params.id}`;

      const res = await this.httpService
        .request({
          headers: {
            'Content-Type': `text/xml`,
          },
          method: 'POST',
          url,
          data: params.xml,
        })
        .toPromise();

      const subscriberJson: string = convert(res.data, {
        format: 'json',
      });

      const subscriber: LooseObjectInterface = JSON.parse(subscriberJson);

      return subscriber;
    } catch (error) {
      const errorDescription = error?.response?.data;
      let response = '';
      if (errorDescription) {
        response = convert(errorDescription, { format: 'object' });
      }
      console.error({
        message: 'TransactionalsExpertSenderService.execHttpRequest error',
        params: { xml: params.xml, response },
        error,
        context: params.context,
      });
      throw error;
    }
  }
}
