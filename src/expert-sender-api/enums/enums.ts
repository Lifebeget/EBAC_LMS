export enum XmlTypeExpertSender {
  string = 'xs:string',
  integer = 'xs:integer',
  double = 'xs:double',
  date = 'xs:date', // 2014-04-25 Дату необходимо передавать в текущем часовом поясе субаккаунта.
  dateTime = 'xs:dateTime', // 2014-04-25T12:42:00 Дату необходимо передавать в текущем часовом поясе субаккаунта.
  boolean = 'xs:boolean',
}
