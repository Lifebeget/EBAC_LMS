import { Inject, Injectable, Logger } from '@nestjs/common';
import { WHO_COMPLETED } from './constants/constants';
import { CustomEventsExpertSenderService } from './custom-events-expert-sender/custom-events-expert-sender.service';
import { SendExpertSenderCustomEventDto } from './custom-events-expert-sender/dto/send-expert-sender-custom-event.dto';
import { ExpertSenderApiServiceInterface } from './interfaces/expert-sender-api-service.interface';
import { LooseObjectInterface } from './interfaces/loose-object.interface';
import { SUBSCRIBERS_EXPERT_SENDER_SERVICE } from './subscribers-expert-sender/constants/constants';
import { SubscribersExpertSenderServiceInterface } from './subscribers-expert-sender/interfaces/subscribers-expert-sender-service.interface';
import { UpsertSubscriberExpertSenderInterface } from './subscribers-expert-sender/interfaces/upsert-subscriber-expert-sender.interface';
import { TABLES_EXPERT_SENDER_SERVICE } from './tables-expert-sender/constants/constants';
import { GetRowsTablesExpertSenderInterface } from './tables-expert-sender/intefaces/get-rows-tables-expert-sender.interface';
import { TablesExpertSenderServiceInterface } from './tables-expert-sender/intefaces/tables-expert-sender-service.interface';
import { UpsertRowTablesExpertSenderInterface } from './tables-expert-sender/intefaces/upsert-row-tables-expert-sender.interface';
import { TRANSACTIONALS_EXPERT_SENDER_SERVICE } from './transactionals-expert-sender/constants/constants';
import { SendToSubscriberTransactionalsExpertSenderInterface } from './transactionals-expert-sender/interfaces/send-to-subscriber-transactionals-expert-sender.interface';
import { TransactionalsExpertSenderServiceInterface } from './transactionals-expert-sender/interfaces/transactionals-expert-sender-service.interface';
import { generateContext } from './utils/generateContext';

@Injectable()
export class ExpertSenderApiService implements ExpertSenderApiServiceInterface {
  constructor(
    @Inject(SUBSCRIBERS_EXPERT_SENDER_SERVICE)
    private readonly subscribersExpertSenderService: SubscribersExpertSenderServiceInterface,
    @Inject(TABLES_EXPERT_SENDER_SERVICE)
    private readonly tablesExpertSenderService: TablesExpertSenderServiceInterface,
    @Inject(TRANSACTIONALS_EXPERT_SENDER_SERVICE)
    private readonly transactionalsExpertSenderService: TransactionalsExpertSenderServiceInterface,
    @Inject('CustomEventsExpertSenderService')
    private customEventsExpertSenderService: CustomEventsExpertSenderService,
  ) {}

  async upsertSubscriber(params: UpsertSubscriberExpertSenderInterface): Promise<LooseObjectInterface> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const res: LooseObjectInterface = await this.subscribersExpertSenderService.upsert(params);
      return res;
    } catch (error) {
      console.error({
        message: 'ExpertSenderApiService.upsertSubscriber error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  async upsertRow<T>(params: UpsertRowTablesExpertSenderInterface<T>): Promise<void> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      await this.tablesExpertSenderService.upsertRow<T>(params);
    } catch (error) {
      console.error({
        message: 'ExpertSenderApiService.upsertRow error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  async getRows<T>(params: GetRowsTablesExpertSenderInterface): Promise<T[]> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const rows: T[] = await this.tablesExpertSenderService.getRows<T>(params);
      return rows;
    } catch (error) {
      console.error({
        message: 'ExpertSenderApiService.getRows error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  async sendTransactionalToSubscriber(params: SendToSubscriberTransactionalsExpertSenderInterface): Promise<LooseObjectInterface> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const res: LooseObjectInterface = await this.transactionalsExpertSenderService.sendToSubscriber(params);
      return res;
    } catch (error) {
      console.error({
        message: 'ExpertSenderApiService.sendTransactionalToSubscriber error',
        error,
        context,
        user,
        params,
      });
      Logger.log(error.response.data);
      throw error;
    }
  }

  async sendCustomEvent(dto: SendExpertSenderCustomEventDto) {
    return this.customEventsExpertSenderService.sendCustomEvent(dto);
  }
}
