import { HttpModule, Module } from '@nestjs/common';
import { SubscribersExpertSenderService } from './subscribers-expert-sender.service';

@Module({
  imports: [HttpModule],
  providers: [SubscribersExpertSenderService],
  exports: [SubscribersExpertSenderService, HttpModule],
})
export class SubscribersExpertSenderModule {}
