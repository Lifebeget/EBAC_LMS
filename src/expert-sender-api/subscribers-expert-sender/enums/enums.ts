export enum ModeSubscribersExpertSender {
  AddAndUpdate = 'AddAndUpdate',
  AddAndReplace = 'AddAndReplace',
  AddAndIgnore = 'AddAndIgnore',
  IgnoreAndUpdate = 'IgnoreAndUpdate',
  IgnoreAndReplace = 'IgnoreAndReplace',
  Synchronize = 'Synchronize',
}

export enum MatchingModeSubscribersExpertSender {
  Email = 'Email',
  CustomSubscriberId = 'CustomSubscriberId',
  Id = 'Id',
  Phone = 'Phone',
}

// https://service7.esv2.com/Property/List
export enum AdditionalFieldsSubscribersExpertSender {
  phone = 1,
  uxDesign = 2,
  interiorDesign = 3,
  motionDesign = 4,
  graphicalDesign = 5,
  webinarInterior = 6,
  special = 8,
  webinarUx = 9,
  webinarGraphical = 10,
  date = 11,
  importedFromMailchimp = 12,
  webinar1 = 13,
  purchaseWebinar1 = 14,
  callcenter = 15,
}
