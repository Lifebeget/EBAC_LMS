export const SUBSCRIBERS_EXPERT_SENDER_SERVICE = 'SubscribersExpertSenderService';

export const WHO_COMPLETED = 'subscribers-expert-sender-service';

export const HTTP_ENTITY = 'Subscribers';

export const FIELD_SEQUENCE = [
  'mode',
  'force',
  'listId',
  'allowUnsubscribed',
  'allowRemoved',
  'matchingMode',
  'id',
  'email',
  'emailMd5',
  'phone',
  'customSubscriberId',
  'firstname',
  'lastname',
  'trackingCode',
  'name',
  'vendor',
  'ip',
];
