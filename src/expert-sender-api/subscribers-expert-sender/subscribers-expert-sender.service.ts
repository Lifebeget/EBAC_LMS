import { HttpService, Injectable } from '@nestjs/common';
import { UpsertSubscriberExpertSenderInterface } from './interfaces/upsert-subscriber-expert-sender.interface';
import { generateContext } from '../utils/generateContext';
import { FIELD_SEQUENCE, HTTP_ENTITY, WHO_COMPLETED } from './constants/constants';
import { SubscriberExpertSenderInterface } from './interfaces/subscriber-expert-sender.interface';
import { ModeSubscribersExpertSender } from './enums/enums';
import { firstCharToUpperCase } from '../utils/firstCharToUpperCase';
import { createApiRequest } from '../utils/createApiRequest';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';
import { PropertyExpertSenderInterface } from './interfaces/property-expert-sender.interface';
import { isNull, isUndefined } from 'lodash';
import { convert } from 'xmlbuilder2';
import { LooseObjectInterface } from '../interfaces/loose-object.interface';
import { processProperty } from '../utils/processProperty';
import { SubscribersExpertSenderServiceInterface } from './interfaces/subscribers-expert-sender-service.interface';

@Injectable()
export class SubscribersExpertSenderService implements SubscribersExpertSenderServiceInterface {
  constructor(private readonly httpService: HttpService) {}

  // https://manual.expertsender.ru/metody/podpischiki/dobavlenie-podpischika-v-list
  async upsert(params: UpsertSubscriberExpertSenderInterface): Promise<LooseObjectInterface> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const { root, apiRequest } = createApiRequest(params.apiKey);
      this.fillApiRequestParams(apiRequest, params);

      const isArray: boolean = Array.isArray(params.subscriber);
      if (isArray) {
        this.fillMultiData(apiRequest, params);
      } else {
        this.fillData(apiRequest, params);
      }

      const xml: string = root.end({ prettyPrint: true });
      const res: LooseObjectInterface = await this.execHttpRequest(xml, context);
      return res;
    } catch (error) {
      console.error({
        message: 'ExpertSenderSubscribersService.upsert error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private fillApiRequestParams(apiRequest: XMLBuilder, params: UpsertSubscriberExpertSenderInterface): void {
    const returnData: string = params.returnData ? 'true' : 'false';
    const verboseErrors: string = params.verboseErrors ? 'true' : 'false';

    apiRequest.ele('ReturnData').txt(returnData).up().ele('VerboseErrors').txt(verboseErrors).up();
  }

  private fillMultiData(apiRequest: XMLBuilder, params: UpsertSubscriberExpertSenderInterface): void {
    const subscribers: SubscriberExpertSenderInterface[] = <SubscriberExpertSenderInterface[]>params.subscriber;

    const multiData: XMLBuilder = apiRequest.ele('MultiData');
    for (const subscriber of subscribers) {
      const xmlSubscriber: XMLBuilder = multiData.ele('Subscriber');
      this.fillSubscriberToXml(xmlSubscriber, subscriber);
      xmlSubscriber.up();
    }
  }

  private fillData(apiRequest: XMLBuilder, params: UpsertSubscriberExpertSenderInterface): void {
    const subscriber: SubscriberExpertSenderInterface = <SubscriberExpertSenderInterface>params.subscriber;

    const data: XMLBuilder = apiRequest.ele('Data', {
      'xsi:type': 'Subscriber',
    });

    this.fillSubscriberToXml(data, subscriber);
  }

  private fillSubscriberToXml(xmlElement: XMLBuilder, subscriber: SubscriberExpertSenderInterface): void {
    if (!subscriber.mode) {
      subscriber.mode = ModeSubscribersExpertSender.AddAndUpdate;
    }

    for (const fieldName of FIELD_SEQUENCE) {
      const value = subscriber[fieldName];
      if (value === undefined) {
        continue;
      }

      xmlElement.ele(firstCharToUpperCase(fieldName)).txt(String(value)).up();
    }

    this.fillProperties(xmlElement, subscriber);
  }

  private fillProperties(xmlElement: XMLBuilder, subscriber: SubscriberExpertSenderInterface): void {
    const properties: PropertyExpertSenderInterface[] = subscriber.properties;
    const empty: boolean = !properties || !properties.length;
    if (empty) {
      return;
    }

    const xmlProperties: XMLBuilder = xmlElement.ele('Properties');
    for (const property of properties) {
      this.fillProperty(property, xmlProperties);
    }

    xmlElement.up();
  }

  private fillProperty(property: PropertyExpertSenderInterface, xmlProperties: XMLBuilder): void {
    const xmlProperty: XMLBuilder = xmlProperties.ele('Property');
    xmlProperty.ele('Id').txt(String(property.id)).up();

    processProperty(property);

    const isNil: boolean = isNull(property.value) || isUndefined(property.value);
    const value = isNil ? '' : String(property.value);

    if (isNil) {
      xmlProperty.ele('Value', { 'xsi:type': property.type, 'xsi:nil': true }).up();
    } else {
      xmlProperty.ele('Value', { 'xsi:type': property.type }).txt(value).up();
    }
  }

  private async execHttpRequest(xml: string, context: string): Promise<LooseObjectInterface> {
    try {
      const httpApiPath: string = process.env.EXPERTSENDER_API_URL;
      const url = `${httpApiPath}/${HTTP_ENTITY}`;

      const res = await this.httpService
        .request({
          headers: {
            'Content-Type': `text/xml`,
          },
          method: 'POST',
          url,
          data: xml,
        })
        .toPromise();

      const subscriberJson: string = convert(res.data, {
        format: 'json',
      });

      const subscriber: LooseObjectInterface = JSON.parse(subscriberJson);

      return subscriber;
    } catch (error) {
      const errorDescription = error?.response?.data;
      let response = '';
      if (errorDescription) {
        response = convert(errorDescription, { format: 'object' });
      }
      console.error({
        message: 'SubscribersExpertSenderService.execHttpRequest error',
        params: { xml, response },
        error,
        context,
      });
      throw error;
    }
  }
}
