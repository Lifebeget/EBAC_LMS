import { XmlTypeExpertSender } from '../../enums/enums';
import { AdditionalFieldsSubscribersExpertSender } from '../enums/enums';

export interface PropertyExpertSenderInterface {
  id: string | number | AdditionalFieldsSubscribersExpertSender;
  value: string | number | boolean | Date;
  type?: XmlTypeExpertSender;
}
