import { LooseObjectInterface } from '../../interfaces/loose-object.interface';
import { SubscriberExpertSenderInterface } from './subscriber-expert-sender.interface';

export interface UpsertSubscriberExpertSenderInterface {
  apiKey?: string;
  returnData?: boolean;
  verboseErrors?: boolean;
  subscriber: SubscriberExpertSenderInterface | SubscriberExpertSenderInterface[];

  context?: string;
  user?: LooseObjectInterface | string;
}
