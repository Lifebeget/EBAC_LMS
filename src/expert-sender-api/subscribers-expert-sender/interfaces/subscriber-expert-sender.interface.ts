import { MatchingModeSubscribersExpertSender, ModeSubscribersExpertSender } from '../enums/enums';
import { PropertyExpertSenderInterface } from './property-expert-sender.interface';

export interface SubscriberExpertSenderInterface {
  mode?: ModeSubscribersExpertSender;
  force?: boolean;
  listId: number;
  allowUnsubscribed?: boolean;
  allowRemoved?: boolean;
  matchingMode?: MatchingModeSubscribersExpertSender;
  id?: number;
  email: string;
  emailMd5?: string;
  phone?: string;
  customSubscriberId?: string;
  firstname?: string;
  lastname?: string;
  trackingCode?: string;
  name?: string;
  vendor?: string;
  ip?: string;
  properties?: PropertyExpertSenderInterface[];
}
