import { UpsertSubscriberExpertSenderInterface } from './upsert-subscriber-expert-sender.interface';
import { LooseObjectInterface } from '../../interfaces/loose-object.interface';

export interface SubscribersExpertSenderServiceInterface {
  upsert(params: UpsertSubscriberExpertSenderInterface): Promise<LooseObjectInterface>;
}
