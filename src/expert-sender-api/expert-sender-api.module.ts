import { Module } from '@nestjs/common';
import { CustomEventsExpertSenderModule } from './custom-events-expert-sender/custom-events-expert-sender.module';
import { ExpertSenderApiService } from './expert-sender-api.service';
import { SubscribersExpertSenderModule } from './subscribers-expert-sender/subscribers-expert-sender.module';
import { TablesExpertSenderModule } from './tables-expert-sender/tables-expert-sender.module';
import { TransactionalsExpertSenderModule } from './transactionals-expert-sender/transactionals-expert-sender.module';

@Module({
  imports: [SubscribersExpertSenderModule, TablesExpertSenderModule, TransactionalsExpertSenderModule, CustomEventsExpertSenderModule],
  providers: [ExpertSenderApiService],
  exports: [ExpertSenderApiService],
})
export class ExpertSenderApiModule {}
