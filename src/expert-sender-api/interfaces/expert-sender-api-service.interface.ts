import { UpsertSubscriberExpertSenderInterface } from '../subscribers-expert-sender/interfaces/upsert-subscriber-expert-sender.interface';
import { LooseObjectInterface } from './loose-object.interface';
import { UpsertRowTablesExpertSenderInterface } from '../tables-expert-sender/intefaces/upsert-row-tables-expert-sender.interface';
import { GetRowsTablesExpertSenderInterface } from '../tables-expert-sender/intefaces/get-rows-tables-expert-sender.interface';
import { SendToSubscriberTransactionalsExpertSenderInterface } from '../transactionals-expert-sender/interfaces/send-to-subscriber-transactionals-expert-sender.interface';

// <T> - это тип таблицы, типы таблиц находятся в backend/gateway/src/modules/expert-sender-api/tables-expert-sender/tables/interfaces
//       если требуется обращение не к типизированной таблице, то задается тип LooseObjectInterface

export interface ExpertSenderApiServiceInterface {
  upsertSubscriber(params: UpsertSubscriberExpertSenderInterface): Promise<LooseObjectInterface>;

  upsertRow<T>(params: UpsertRowTablesExpertSenderInterface<T>): Promise<void>;

  getRows<T>(params: GetRowsTablesExpertSenderInterface): Promise<T[]>;

  sendTransactionalToSubscriber(params: SendToSubscriberTransactionalsExpertSenderInterface): Promise<LooseObjectInterface>;
}
