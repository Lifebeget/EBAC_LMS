import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';

export interface RootAndApiRequestInterface {
  root: XMLBuilder;
  apiRequest: XMLBuilder;
}
