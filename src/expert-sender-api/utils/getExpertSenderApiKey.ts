import { ConfigService } from '@nestjs/config';

export function getExpertSenderApiKey(apiKey: string, configService?: ConfigService): string {
  if (apiKey) {
    return apiKey;
  }

  if (configService) {
    apiKey = configService.get<string>('EXPERTSENDER_API_KEY');
  } else {
    apiKey = process.env.EXPERTSENDER_API_KEY;
  }

  if (!apiKey) {
    throw new Error('Expert sender api key is not filled');
  }

  return apiKey;
}
