import { create } from 'xmlbuilder2';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';
import { getExpertSenderApiKey } from './getExpertSenderApiKey';
import { RootAndApiRequestInterface } from '../interfaces/root-and-api-request.interface';

export function createApiRequest(apiKey: string): RootAndApiRequestInterface {
  apiKey = getExpertSenderApiKey(apiKey);

  const root: XMLBuilder = create({});
  const apiRequest: XMLBuilder = root
    .ele('ApiRequest', {
      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
      'xmlns:xs': 'http://www.w3.org/2001/XMLSchema',
    })
    .ele('ApiKey')
    .txt(apiKey)
    .up();

  return { root, apiRequest };
}
