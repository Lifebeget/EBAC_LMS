export function firstCharToLowerCase(str: string): string {
  return str.substr(0, 1).toLowerCase() + str.substr(1);
}
