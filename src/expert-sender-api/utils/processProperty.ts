import { PropertyExpertSenderInterface } from '../subscribers-expert-sender/interfaces/property-expert-sender.interface';
import { XmlTypeExpertSender } from '../enums/enums';
import { AdditionalFieldsSubscribersExpertSender } from '../subscribers-expert-sender/enums/enums';
import { isDate } from 'lodash';
import dayjs from 'dayjs';

const BOOLEAN_PROPERTIES: AdditionalFieldsSubscribersExpertSender[] = [
  AdditionalFieldsSubscribersExpertSender.special,
  AdditionalFieldsSubscribersExpertSender.importedFromMailchimp,
];

const DATE_TIME_PROPERTIES: AdditionalFieldsSubscribersExpertSender[] = [AdditionalFieldsSubscribersExpertSender.date];

export function processProperty(property: PropertyExpertSenderInterface): void {
  if (!property.type) {
    property.type = getPropertyType(property);
  }

  if (property.type === XmlTypeExpertSender.dateTime) {
    property.value = convertDateTime(property.value);
  }
}

function getPropertyType(property: PropertyExpertSenderInterface): XmlTypeExpertSender {
  if (property.type) {
    return property.type;
  }

  const id = Number(property.id);

  const isBoolean: boolean = BOOLEAN_PROPERTIES.includes(id);
  if (isBoolean) {
    return XmlTypeExpertSender.boolean;
  }

  const isDateTime: boolean = DATE_TIME_PROPERTIES.includes(id);
  if (isDateTime) {
    return XmlTypeExpertSender.dateTime;
  }

  return XmlTypeExpertSender.string;
}

function convertDateTime(date: string | number | boolean | Date): string | number | boolean | Date {
  const noDate = !isDate(date);
  if (noDate) {
    return date;
  }
  return dayjs(date as string).format('YYYY-MM-DDTHH:mm:ss');
}
