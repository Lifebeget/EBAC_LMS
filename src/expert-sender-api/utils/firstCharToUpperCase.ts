export function firstCharToUpperCase(str: string): string {
  return str.substr(0, 1).toUpperCase() + str.substr(1);
}
