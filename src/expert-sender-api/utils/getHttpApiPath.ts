import { ConfigService } from '@nestjs/config';

export function getHttpApiPath(configService: ConfigService): string {
  let apiPath = '';

  if (configService) {
    apiPath = configService.get<string>('EXPERTSENDER_API_URL');
  } else {
    apiPath = process.env.EXPERTSENDER_API_URL;
  }

  if (!apiPath) {
    throw new Error('Expert sender api path is not filled');
  }

  return apiPath;
}
