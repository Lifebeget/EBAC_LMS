import { customAlphabet } from 'nanoid';
import { nolookalikes } from 'nanoid-dictionary';
const nanoid = customAlphabet(nolookalikes, 21);

export function generateContext(): string {
  return nanoid();
}
