import { isNull, isUndefined, camelCase } from 'lodash';

export function valuesToCamelCase(values: string[]): string[] {
  return values.map(value => valueToCamelCase(value));
}

export function valueToCamelCase(value: string): string {
  if (isNull(value)) {
    return '';
  }

  if (isUndefined(value)) {
    return '';
  }

  return camelCase(value);
}
