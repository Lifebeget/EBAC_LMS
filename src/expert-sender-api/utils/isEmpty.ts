import { isNull, isUndefined } from 'lodash';

export function isEmpty<T>(value: T | T[]): boolean {
  if (isNull(value)) {
    return true;
  }

  if (isUndefined(value)) {
    return true;
  }

  const emptyArray: boolean = Array.isArray(value) && !value.length;
  if (emptyArray) {
    return true;
  }

  return false;
}
