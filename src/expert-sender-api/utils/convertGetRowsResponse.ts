import { LooseObjectInterface } from '../interfaces/loose-object.interface';
import { valueToCamelCase } from './toCamelCase';

export function convertGetRowsResponse(response: string): LooseObjectInterface[] {
  const responseRows: string[] = getResponseRows(response);
  const columns: string[] = getColumns(responseRows);
  const rows: LooseObjectInterface[] = responseRows.map(responseRow => convertTableRowToObject(responseRow, columns));

  return rows;
}

function getResponseRows(response: string): string[] {
  response = response.replace(/\r/g, '');
  const rows: string[] = response.split('\n').filter(row => Boolean(row));
  return rows;
}

function getColumns(responseRows: string[]): string[] {
  const columns: string[] = responseRows
    .shift()
    .split(',')
    .map(column => valueToCamelCase(column));
  return columns;
}

export function convertTableRowToObject(responseRow: string, columns: string[]): LooseObjectInterface {
  const obj: LooseObjectInterface = {};
  const values = responseRow.split(',');

  for (let i = 0; i < columns.length; i++) {
    const columnName: string = columns[i];
    const value = values[i];
    obj[columnName] = value;
  }

  return obj;
}
