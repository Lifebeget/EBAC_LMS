import { isNull, isUndefined, snakeCase } from 'lodash';

export function valuesToSnakeCase(values: string[]): string[] {
  return values.map(value => valueToSnakeCase(value));
}

export function valueToSnakeCase(value: string): string {
  if (isNull(value)) {
    return '';
  }

  if (isUndefined(value)) {
    return '';
  }

  return snakeCase(value);
}
