import { ApiProperty } from '@nestjs/swagger';

export class SendExpertSenderCustomEventDto {
  @ApiProperty()
  webinarId: string;

  @ApiProperty()
  clientId: string;
}
