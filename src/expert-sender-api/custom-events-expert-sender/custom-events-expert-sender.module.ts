import { HttpModule, Module } from '@nestjs/common';
import { ClientsModule } from 'src/clients/clients.module';
import { TopicModule } from 'src/topic/topic.module';
import { WebinarModule } from 'src/webinar/webinar.module';
import { CustomEventsExpertSenderService } from './custom-events-expert-sender.service';

@Module({
  imports: [WebinarModule, ClientsModule, TopicModule, HttpModule],
  providers: [CustomEventsExpertSenderService],
  exports: [CustomEventsExpertSenderService],
})
export class CustomEventsExpertSenderModule {}
