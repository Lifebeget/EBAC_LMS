import { HttpService, Injectable } from '@nestjs/common';
import { strict } from 'assert';
import * as moment from 'moment';
import { ClientsService } from 'src/clients/clients.service';
import { TopicService } from 'src/topic/topic.service';
import { WebinarService } from 'src/webinar/webinar.service';
import { create as createXML } from 'xmlbuilder';
import { SendExpertSenderCustomEventDto } from './dto/send-expert-sender-custom-event.dto';

@Injectable()
export class CustomEventsExpertSenderService {
  constructor(
    private webinarsService: WebinarService,
    private clientsService: ClientsService,
    private topicsService: TopicService,
    private httpService: HttpService,
  ) {}

  async sendCustomEvent(dto: SendExpertSenderCustomEventDto) {
    const { webinarId, clientId } = dto;

    const [webinar, client] = await Promise.all([this.webinarsService.findOne(webinarId), this.clientsService.findClientById(clientId)]);

    strict.ok(webinar);
    strict.ok(webinar.id);
    strict.ok(client);

    const topics = await this.topicsService.find({
      where: { scheduleId: webinar.id },
      order: { dayNumber: 'ASC' },
    });

    // TODO Enable Zapier request when ready & tested on dev
    /*
    const NOW_UTC = moment.utc();
    await this.httpService.post(
      'https://hooks.zapier.com/hooks/catch/9090994/buv8jvr/',
      {
        data: {
          user_agent: requestBody?.useragent || null,
          ip_address: requestBody?.ClientIP?.replace('ip=', '') || null,
          conversion_name: 'CompleteRegistration',
          conversion_time: NOW_UTC.clone().utcOffset(-180).format(),
          conversion_time_utc: NOW_UTC.clone().format(),
          email: client.email,
          name: client.name,
          event_id: scheduleEvent.id,
        },
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
    */

    const customEventId = 1;
    // if (
    //   webinar &&
    //   [596, 599, 600, 601, 602, 603, 604, 607].indexOf(scheduleEvent.id) !== -1
    // ) {
    //   customEventId = 6;
    // } else if (scheduleEvent?.id && [623].indexOf(scheduleEvent.id) !== -1) {
    //   customEventId = 7;
    // }

    const doc = createXML(
      {
        ApiRequest: {
          '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          '@xmlns:xs': 'http://www.w3.org/2001/XMLSchema',
          ApiKey: process.env.EXPERTSENDER_API_KEY,
          Data: {
            SubscriberEmail: client.email,
            CustomEventId: customEventId,
            DataFields: {
              DataField: [
                {
                  Name: 'schedule_id',
                  Type: 'Text',
                  Value: webinar.id,
                },
                {
                  Name: 'schedule_date',
                  Type: 'Datetime',
                  Value: moment.utc(webinar.startDateUtc).format('YYYY-MM-DD HH:mm:ss'),
                },
                {
                  Name: 'subscription_date',
                  Type: 'Datetime',
                  Value: moment.utc().format('YYYY-MM-DD HH:mm:ss'),
                },
                ...topics.map(topic => ({
                  Name: `schedule_topic_id_day${topic.dayNumber}`,
                  Type: 'Text',
                  Value: topic.id,
                })),
              ],
            },
          },
        },
      },
      { headless: true },
    );
    const xml = doc.end({ pretty: true });
    return this.execHttpRequest(xml);
  }

  async execHttpRequest(xml: string) {
    const httpApiPath: string = process.env.EXPERTSENDER_API_URL;
    const HTTP_ENTITY = 'WorkflowCustomEvents';
    const url = `${httpApiPath}/${HTTP_ENTITY}`;

    try {
      const { status, data } = await this.httpService
        .request({
          headers: {
            'Content-Type': `text/xml`,
          },
          method: 'POST',
          url,
          data: xml,
        })
        .toPromise();
      console.log({ status, data });
    } catch (err) {
      console.log(err.response.data);
    }
  }
}
