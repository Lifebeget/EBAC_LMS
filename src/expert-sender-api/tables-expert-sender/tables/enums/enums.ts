// https://service7.esv2.com/DataTables/Tables
export enum TableNamesTablesExpertSender {
  clients = 'clients',
  nomenclature = 'nomenclature',
  ordersRd = 'ordersRd',
  schedule = 'schedule',
  sources = 'sources',
  subscriptions = 'subscriptions',
  tempPayments = 'tempPayments',
}
