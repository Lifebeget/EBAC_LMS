export interface SubscriptionsTableInterface {
  id?: number;
  clientId?: string;
  sourceId?: string;
  scheduleEventId?: string;
  subscriptionDateBrazil?: Date;
  subscriptionDateUtc?: Date;
  utmContent?: string;
  utmSource?: string;
  utmMedium?: string;
  utmCampaign?: string;
  utmTerm?: string;
  created?: Date;
  modified?: Date;
  isDeleted?: boolean;
}
