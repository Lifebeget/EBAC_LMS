export interface ClientsTableInterface {
  id?: number;
  email?: string;
  phone?: string;
  fullName?: string;
  name?: string;
  lastName?: string;
  gender?: string;
  birthday?: Date;
  idFb?: string;
  idVk?: string;
  idTg?: string;
  idGoogle?: string;
  fullClientId?: number;
  created?: Date;
  modified?: Date;
}
