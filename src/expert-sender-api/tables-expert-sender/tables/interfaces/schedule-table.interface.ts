export interface ScheduleTableInterface {
  id?: number;
  code?: string;
  eventName?: string;
  eventType?: string;
  eventDateUtc?: Date;
  eventDateBrazil?: Date;
  created?: Date;
  modified?: Date;
}
