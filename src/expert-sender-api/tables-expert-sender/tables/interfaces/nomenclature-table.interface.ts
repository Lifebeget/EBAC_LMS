export interface NomenclatureTableInterface {
  id?: number;
  category?: string;
  type?: string;
  shortName?: string;
  name?: string;
  crmName?: string;
  pageUrl?: string;
  date?: Date;
  created?: Date;
  modified?: Date;
  productIdRd?: number;
}
