export interface TempPaymentsTableInterface {
  email?: string;
  date?: Date;
  course?: string;
  paid?: boolean;
  price?: number;
  discountPercent?: string;
  status?: string;
  commentary?: string;
  id?: number;
  modified?: Date;
}
