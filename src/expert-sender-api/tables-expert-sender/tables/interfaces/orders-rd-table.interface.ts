export interface OrdersRdTableInterface {
  id?: number;
  crmId?: string;
  crmStatus?: string;
  crmStatusId?: string;
  crmOrganizationId?: string;
  crmContactId?: string;
  pageUrl?: string;
  win?: boolean;
  origin?: string;
  subscriptionId?: number;
  sourceId?: number;
  clientId?: number;
  orderDate?: Date;
  created?: Date;
  modified?: Date;
  orderDateBrazil?: Date;
}
