export interface SourcesTableInterface {
  id?: number;
  name?: string;
  url?: string;
  action?: string;
  pageType?: string;
  formType?: string;
  nomenclatureId?: number;
  created?: Date;
  modified?: Date;
}
