import { HttpService, Injectable } from '@nestjs/common';
import { GetRowsTablesExpertSenderInterface } from './intefaces/get-rows-tables-expert-sender.interface';
import { LooseObjectInterface } from '../interfaces/loose-object.interface';
import { generateContext } from '../utils/generateContext';
import {
  DEFAULT_PRIMARY_KEY,
  DEFAULT_PRIMARY_KEY_CLIENTS,
  DEFAULT_PRIMARY_KEY_NOMENCLATURE,
  DEFAULT_PRIMARY_KEY_ORDERS_RD,
  DEFAULT_PRIMARY_KEY_SCHEDULE,
  DEFAULT_PRIMARY_KEY_SOURCES,
  DEFAULT_PRIMARY_KEY_SUBSCRIPTIONS,
  DEFAULT_PRIMARY_KEY_TEMP_PAYMENTS,
  WHO_COMPLETED,
} from './constants/constants';
import { createApiRequest } from '../utils/createApiRequest';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';
import { DirectionTablesExpertSender, HttpEntityTablesExpertSender, OperatorTablesExpertSender } from './enums/enums';
import { WhereConditionTablesExpertSenderInterface } from './intefaces/where-condition-tables-expert-sender.interface';
import { OrderTablesExpertSenderInterface } from './intefaces/order-tables-expert-sender.interface';
import { isEmpty } from '../utils/isEmpty';
import { ExecHttpRequestExpertSenderInterface } from './intefaces/exec-http-request-expert-sender.interface';
import { valueToSnakeCase } from '../utils/toSnakeCase';
import { convert } from 'xmlbuilder2';
import { TableNamesTablesExpertSender } from './tables/enums/enums';
import { convertGetRowsResponse } from '../utils/convertGetRowsResponse';
import { AddRowsTablesExpertSenderInterface } from './intefaces/add-rows-tables-expert-sender.interface';
import { UpdateRowTablesExpertSenderInterface } from './intefaces/update-row-tables-expert-sender.interface';
import { UpsertRowTablesExpertSenderInterface } from './intefaces/upsert-row-tables-expert-sender.interface';
import { TablesExpertSenderServiceInterface } from './intefaces/tables-expert-sender-service.interface';

@Injectable()
export class TablesExpertSenderService implements TablesExpertSenderServiceInterface {
  constructor(private readonly httpService: HttpService) {}

  async upsertRow<T>(params: UpsertRowTablesExpertSenderInterface<T>): Promise<void> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const rowExists = await this.rowExists<T>(params);
      if (rowExists) {
        await this.updateRow<T>(params);
      } else {
        await this.addRows<T>(params);
      }
    } catch (error) {
      console.error({
        message: 'TablesExpertSenderService.upsertRow error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private async rowExists<T>(params: UpsertRowTablesExpertSenderInterface<T>): Promise<boolean> {
    const primaryKeyColumns: string[] = this.getPrimaryKeys(params.primaryKeyFields, params.tableName);

    const whereConditions: WhereConditionTablesExpertSenderInterface[] = primaryKeyColumns.map(fieldName => {
      return {
        fieldName,
        operator: OperatorTablesExpertSender.Equals,
        value: params.row[fieldName],
      };
    });

    const rows: T[] = await this.getRows<T>({
      tableName: params.tableName,
      fields: primaryKeyColumns,
      whereConditions,
      context: params.context,
      user: params.user,
    });

    const rowExists = Boolean(rows.length);
    return rowExists;
  }

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/poluchit-dannye-iz-tablitsy
  async getRows<T>(params: GetRowsTablesExpertSenderInterface): Promise<T[]> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const { root, apiRequest } = createApiRequest(params.apiKey);

      this.fillTableName(params.tableName, apiRequest);
      this.fillColumns(params.fields, apiRequest);
      this.fillWhereConditions(params.whereConditions, apiRequest);
      this.fillOrderByColumns(params.orderByFields, apiRequest);
      this.fillLimit(params.limit, apiRequest);

      const xml: string = root.end({ prettyPrint: true });

      const response: string = await this.execHttpRequest({
        xml,
        context,
        httpEntity: HttpEntityTablesExpertSender.DataTablesGetData,
      });

      const rows: LooseObjectInterface[] = convertGetRowsResponse(response);
      return <T[]>rows;
    } catch (error) {
      console.error({
        message: 'TablesExpertSenderService.getRows error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private fillColumns(columns: string[], apiRequest: XMLBuilder): void {
    if (isEmpty<string>(columns)) {
      return;
    }

    const xmlColumns: XMLBuilder = apiRequest.ele('Columns');
    for (const column of columns) {
      xmlColumns.ele('Column').txt(valueToSnakeCase(column)).up();
    }
  }

  private fillWhereConditions(whereConditions: WhereConditionTablesExpertSenderInterface[], apiRequest: XMLBuilder): void {
    if (isEmpty<WhereConditionTablesExpertSenderInterface>(whereConditions)) {
      return;
    }

    const xmlWhereConditions: XMLBuilder = apiRequest.ele('WhereConditions');
    for (const whereCondition of whereConditions) {
      this.fillWhereCondition(whereCondition, xmlWhereConditions);
    }
  }

  private fillWhereCondition(whereCondition: WhereConditionTablesExpertSenderInterface, xmlWhereConditions: XMLBuilder): void {
    const xmlWhere: XMLBuilder = xmlWhereConditions.ele('Where');
    const columnName: string = valueToSnakeCase(String(whereCondition.fieldName));

    xmlWhere.ele('ColumnName').txt(columnName).up();
    xmlWhere.ele('Operator').txt(String(whereCondition.operator)).up();
    xmlWhere.ele('Value').txt(String(whereCondition.value)).up();
  }

  private fillOrderByColumns(orderByColumns: OrderTablesExpertSenderInterface[], apiRequest: XMLBuilder): void {
    if (isEmpty<OrderTablesExpertSenderInterface>(orderByColumns)) {
      return;
    }

    const xmlOrderByColumns: XMLBuilder = apiRequest.ele('OrderByColumns');
    for (const orderByColumn of orderByColumns) {
      this.fillOrderByColumn(orderByColumn, xmlOrderByColumns);
    }
  }

  private fillOrderByColumn(orderByColumn: OrderTablesExpertSenderInterface, xmlOrderByColumns: XMLBuilder): void {
    const direction: DirectionTablesExpertSender = orderByColumn.direction || DirectionTablesExpertSender.Ascending;
    const columnName: string = valueToSnakeCase(String(orderByColumn.fieldName));

    const xmlOrderBy: XMLBuilder = xmlOrderByColumns.ele('OrderBy');
    xmlOrderBy.ele('ColumnName').txt(columnName).up();
    xmlOrderBy.ele('Direction').txt(direction).up();
  }

  private fillLimit(limit: number, apiRequest: XMLBuilder): void {
    if (isEmpty<number>(limit)) {
      return;
    }
    apiRequest.ele('Limit').txt(String(limit)).up();
  }

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/dobavit-zapis
  async addRows<T>(params: AddRowsTablesExpertSenderInterface<T>): Promise<void> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      if (params.row) {
        params.rows = [params.row];
      }

      const { root, apiRequest } = createApiRequest(params.apiKey);
      this.fillTableName(params.tableName, apiRequest);
      this.fillMultiData<T>(params.rows, apiRequest);

      const xml: string = root.end({ prettyPrint: true });

      await this.execHttpRequest({
        xml,
        context,
        httpEntity: HttpEntityTablesExpertSender.DataTablesAddRow,
      });
    } catch (error) {
      console.error({
        message: 'TablesExpertSenderService.addRows error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private fillMultiData<T>(rows: T[], apiRequest: XMLBuilder): void {
    const xmlMultiData: XMLBuilder = apiRequest.ele('MultiData');
    for (const row of rows) {
      const xmlRow: XMLBuilder = xmlMultiData.ele('Row');
      this.fillColumnsInRow(row, xmlRow);
    }
    xmlMultiData.up();
  }

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/obnovit-zapis
  async updateRow<T>(params: UpdateRowTablesExpertSenderInterface<T>): Promise<void> {
    const context = params.context || generateContext();
    params.context = context;
    const user = params.user || WHO_COMPLETED;

    try {
      const { root, apiRequest } = createApiRequest(params.apiKey);
      this.fillTableName(params.tableName, apiRequest);
      this.fillPrimaryKeyColumns<T>(params, apiRequest);
      this.fillColumnsInRow<T>(params.row, apiRequest);

      const xml: string = root.end({ prettyPrint: true });

      await this.execHttpRequest({
        xml,
        context,
        httpEntity: HttpEntityTablesExpertSender.DataTablesUpdateRow,
      });
    } catch (error) {
      console.error({
        message: 'TablesExpertSenderService.updateRow error',
        error,
        context,
        user,
        params,
      });
      throw error;
    }
  }

  private fillTableName(tableName: string, apiRequest: XMLBuilder): void {
    apiRequest.ele('TableName').txt(valueToSnakeCase(tableName)).up();
  }

  private fillPrimaryKeyColumns<T>(params: UpdateRowTablesExpertSenderInterface<T>, apiRequest: XMLBuilder): void {
    const primaryKeyColumns: string[] = this.getPrimaryKeys(params.primaryKeyFields, params.tableName);

    const xmlPrimaryKeyColumns: XMLBuilder = apiRequest.ele('PrimaryKeyColumns');

    primaryKeyColumns.forEach(columnName => {
      const value = String(params.row[columnName]);
      this.fillColumn(columnName, value, xmlPrimaryKeyColumns);
    });

    xmlPrimaryKeyColumns.up();
  }

  private getPrimaryKeys(primaryKeyFields: string[] | string, tableName: string): string[] {
    if (isEmpty<string>(primaryKeyFields)) {
      return this.getDefaultPrimaryKeyField(tableName);
    } else {
      return this.getPrimaryKeyField(primaryKeyFields);
    }
  }

  private getDefaultPrimaryKeyField(tableName: string | TableNamesTablesExpertSender): string[] {
    switch (tableName) {
      case TableNamesTablesExpertSender.clients:
        return DEFAULT_PRIMARY_KEY_CLIENTS;
      case TableNamesTablesExpertSender.nomenclature:
        return DEFAULT_PRIMARY_KEY_NOMENCLATURE;
      case TableNamesTablesExpertSender.ordersRd:
        return DEFAULT_PRIMARY_KEY_ORDERS_RD;
      case TableNamesTablesExpertSender.schedule:
        return DEFAULT_PRIMARY_KEY_SCHEDULE;
      case TableNamesTablesExpertSender.sources:
        return DEFAULT_PRIMARY_KEY_SOURCES;
      case TableNamesTablesExpertSender.subscriptions:
        return DEFAULT_PRIMARY_KEY_SUBSCRIPTIONS;
      case TableNamesTablesExpertSender.tempPayments:
        return DEFAULT_PRIMARY_KEY_TEMP_PAYMENTS;
      default:
        return DEFAULT_PRIMARY_KEY;
    }
  }

  private getPrimaryKeyField(primaryKeyColumns: string | string[]): string[] {
    if (Array.isArray(primaryKeyColumns)) {
      return primaryKeyColumns;
    }
    return [primaryKeyColumns];
  }

  private fillColumnsInRow<T>(row: T, xmlRow: XMLBuilder): void {
    if (isEmpty<T>(row)) {
      return;
    }

    const columnNames: string[] = Object.keys(row);
    const xmlColumns: XMLBuilder = xmlRow.ele('Columns');
    for (const columnName of columnNames) {
      const value = String(row[columnName]);
      this.fillColumn(columnName, value, xmlColumns);
    }
  }

  private fillColumn(columnName, value, xmlColumns: XMLBuilder): void {
    const xmlColumn: XMLBuilder = xmlColumns.ele('Column');
    xmlColumn.ele('Name').txt(valueToSnakeCase(columnName)).up();
    xmlColumn.ele('Value').txt(value).up();
    xmlColumn.up();
  }

  private async execHttpRequest(params: ExecHttpRequestExpertSenderInterface): Promise<string> {
    try {
      const httpApiPath = process.env.EXPERTSENDER_API_URL;
      const url = `${httpApiPath}/${params.httpEntity}`;

      const res = await this.httpService
        .request({
          headers: {
            'Content-Type': `text/xml`,
          },
          method: 'POST',
          url,
          data: params.xml,
        })
        .toPromise();

      const data = res?.data;
      return data;
    } catch (error) {
      const errorDescription = error?.response?.data;
      let response = '';
      if (errorDescription) {
        response = convert(errorDescription, { format: 'object' });
      }

      console.error({
        message: 'TablesExpertSenderService.execHttpRequest error',
        params: { xml: params.xml, response },
        error,
        context: params.context,
      });
      throw error;
    }
  }
}
