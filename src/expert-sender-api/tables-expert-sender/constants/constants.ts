export const TABLES_EXPERT_SENDER_SERVICE = 'TablesExpertSenderService';
export const WHO_COMPLETED = 'tables-expert-sender-service';

export const DEFAULT_PRIMARY_KEY = ['id'];
export const DEFAULT_PRIMARY_KEY_CLIENTS = ['id'];
export const DEFAULT_PRIMARY_KEY_NOMENCLATURE = ['id'];
export const DEFAULT_PRIMARY_KEY_ORDERS_RD = ['id'];
export const DEFAULT_PRIMARY_KEY_SCHEDULE = ['id'];
export const DEFAULT_PRIMARY_KEY_SOURCES = ['id'];
export const DEFAULT_PRIMARY_KEY_SUBSCRIPTIONS = ['id'];
export const DEFAULT_PRIMARY_KEY_TEMP_PAYMENTS = ['tempPayments'];
