import { DirectionTablesExpertSender } from '../enums/enums';

export interface OrderTablesExpertSenderInterface {
  fieldName: string;
  direction?: DirectionTablesExpertSender;
}
