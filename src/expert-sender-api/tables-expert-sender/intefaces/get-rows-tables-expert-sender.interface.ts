import { LooseObjectInterface } from '../../interfaces/loose-object.interface';
import { WhereConditionTablesExpertSenderInterface } from './where-condition-tables-expert-sender.interface';
import { OrderTablesExpertSenderInterface } from './order-tables-expert-sender.interface';
import { TableNamesTablesExpertSender } from '../tables/enums/enums';

export interface GetRowsTablesExpertSenderInterface {
  apiKey?: string;
  tableName: string | TableNamesTablesExpertSender;
  fields?: string[];
  whereConditions?: WhereConditionTablesExpertSenderInterface[];
  orderByFields?: OrderTablesExpertSenderInterface[];
  limit?: number;

  context?: string;
  user?: LooseObjectInterface | string;
}
