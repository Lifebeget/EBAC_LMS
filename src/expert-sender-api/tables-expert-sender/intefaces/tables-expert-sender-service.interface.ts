import { UpsertRowTablesExpertSenderInterface } from './upsert-row-tables-expert-sender.interface';
import { GetRowsTablesExpertSenderInterface } from './get-rows-tables-expert-sender.interface';
import { AddRowsTablesExpertSenderInterface } from './add-rows-tables-expert-sender.interface';
import { UpdateRowTablesExpertSenderInterface } from './update-row-tables-expert-sender.interface';

// <T> - это тип таблицы, типы таблиц находятся в backend/gateway/src/modules/expert-sender-api/tables-expert-sender/tables/interfaces
//       если требуется обращение не к типизированной таблице, то задается тип LooseObjectInterface
export interface TablesExpertSenderServiceInterface {
  upsertRow<T>(params: UpsertRowTablesExpertSenderInterface<T>): Promise<void>;

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/poluchit-dannye-iz-tablitsy
  getRows<T>(params: GetRowsTablesExpertSenderInterface): Promise<T[]>;

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/dobavit-zapis
  addRows<T>(params: AddRowsTablesExpertSenderInterface<T>): Promise<void>;

  // https://manual.expertsender.ru/metody/tablitsy-dannykh/obnovit-zapis
  updateRow<T>(params: UpdateRowTablesExpertSenderInterface<T>): Promise<void>;
}
