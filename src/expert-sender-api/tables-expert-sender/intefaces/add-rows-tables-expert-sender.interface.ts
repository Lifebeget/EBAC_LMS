import { LooseObjectInterface } from '../../interfaces/loose-object.interface';
import { TableNamesTablesExpertSender } from '../tables/enums/enums';

export interface AddRowsTablesExpertSenderInterface<T> {
  apiKey?: string;
  tableName: string | TableNamesTablesExpertSender;
  rows?: T[];
  row?: T;

  context?: string;
  user?: LooseObjectInterface | string;
}
