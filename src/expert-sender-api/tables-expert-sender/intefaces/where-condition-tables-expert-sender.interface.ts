import { OperatorTablesExpertSender } from '../enums/enums';

export interface WhereConditionTablesExpertSenderInterface {
  fieldName: string;
  operator: OperatorTablesExpertSender;
  value: string | number | boolean | Date;
}
