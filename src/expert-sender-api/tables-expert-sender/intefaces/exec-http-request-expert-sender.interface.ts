import { HttpEntityTablesExpertSender } from '../enums/enums';

export interface ExecHttpRequestExpertSenderInterface {
  xml: string;
  httpEntity: HttpEntityTablesExpertSender;

  context?: string;
}
