import { LooseObjectInterface } from '../../interfaces/loose-object.interface';
import { TableNamesTablesExpertSender } from '../tables/enums/enums';
import { primaryKeyFieldsTablesExpertSender } from '../enums/enums';

export interface UpsertRowTablesExpertSenderInterface<T> {
  apiKey?: string;
  tableName: string | TableNamesTablesExpertSender;
  row: T;
  primaryKeyFields?: string[] | primaryKeyFieldsTablesExpertSender[] | string | primaryKeyFieldsTablesExpertSender;

  context?: string;
  user?: LooseObjectInterface | string;
}
