export enum OperatorTablesExpertSender {
  Equals = 'Equals',
  Greater = 'Greater',
  Lower = 'Lower',
  Like = 'Like',
}

export enum DirectionTablesExpertSender {
  Ascending = 'Ascending',
  Descending = 'Descending',
}

export enum HttpEntityTablesExpertSender {
  DataTablesGetData = 'DataTablesGetData',
  DataTablesUpdateRow = 'DataTablesUpdateRow',
  DataTablesAddRow = 'DataTablesAddRow',
}

export enum primaryKeyFieldsTablesExpertSender {
  id = 'id',
  email = 'email',
  code = 'code',
  clientId = 'clientId',
}
