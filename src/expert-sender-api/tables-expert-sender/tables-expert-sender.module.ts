import { HttpModule, Module } from '@nestjs/common';
import { TablesExpertSenderService } from './tables-expert-sender.service';

@Module({
  imports: [HttpModule],
  providers: [TablesExpertSenderService],
  exports: [TablesExpertSenderService, HttpModule],
})
export class TablesExpertSenderModule {}
