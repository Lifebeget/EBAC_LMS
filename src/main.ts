import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { SocketAdapter } from './sockets/socket.adapter';
import { urlencoded, json } from 'express';
import * as responseTime from 'response-time';
import 'reflect-metadata';
import { initializeTransactionalContext, patchTypeORMRepositoryWithBaseRepository } from 'typeorm-transactional-cls-hooked';

import './dayjs.config';
import { resolveApp } from './app';

import * as Sentry from '@sentry/node';
import { ErrorsToIgnore } from './sentry/IgnoreLists';
import { filterByTestEmail } from './sentry/Filters';
import { pipe } from 'rxjs';
import { MultidomainsMiddleware } from './middleware/multidomains';

async function bootstrap() {
  initializeTransactionalContext();
  patchTypeORMRepositoryWithBaseRepository();

  const app = await NestFactory.create(AppModule);

  Sentry.init({
    dsn: process.env.SENTRY_DNS,
    debug: !!process.env.SENTRY_DEBUG,
    environment: process.env.SENTRY_ENVIRONMENT || 'LOCAL',
    enabled: !!process.env.SENTRY_ENABLE,
    release: `backend@commit:${process.env.COMMIT ?? 'not-provided'}`,
    ignoreErrors: ErrorsToIgnore,
    beforeSend(event, hint) {
      const pipeResult: { event: Sentry.Event | null; hint: any } = pipe(filterByTestEmail)({ event, hint });
      return pipeResult.event;
    },
  });

  app.enableCors();
  app.useWebSocketAdapter(new SocketAdapter(app));

  app.use(responseTime());
  app.use(MultidomainsMiddleware);
  const config = new DocumentBuilder()
    .setTitle('EBAC Tutor')
    .setDescription('EBAC API for Tutor interface')
    .setVersion('1.0')
    .addBearerAuth()
    .addTag('public')
    .addTag('auth')
    .addTag('users')
    .addTag('courses')
    .addTag('modules')
    .addTag('lectures')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000, '0.0.0.0');
  resolveApp(app);
  return app;
}
bootstrap();
