import { Course } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { Section } from '@entities/section.entity';
import { Module } from '@entities/module.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      l.id, l.course_id, l.module_id,
      m.section_id,
      m.active as module_active,
      l.active as lecture_active,
      m.published as module_published,
      l.published as lecture_published,
      (m.active and l.active) as active,
      (m.active and l.active and m.published and l.published) as published,
      (l.required and m.active and l.active) as required,
      (m.active and l.active and m.published and l.published and l.free) as free
    from lecture l
    left join module m on m.id = l.module_id
    left join section s on s.id = m.section_id
    where l.deleted is null
  `,
})
export class ViewLecture {
  @PrimaryColumn()
  id: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  courseId: string;

  @ManyToOne(() => Section)
  @JoinColumn({ name: 'section_id' })
  section: Section;

  @ViewColumn()
  sectionId: string;

  @ManyToOne(() => Module)
  @JoinColumn({ name: 'module_id' })
  module: Module;

  @ViewColumn()
  moduleId: string;

  @ViewColumn()
  moduleActive: boolean;

  @ViewColumn()
  modulePublished: boolean;

  @ManyToOne(() => Lecture)
  @JoinColumn({ name: 'lecture_id' })
  lecture: Lecture;

  @ViewColumn()
  lectureId: string;

  @ViewColumn()
  lectureActive: boolean;

  @ViewColumn()
  lecturePublished: boolean;

  @ViewColumn()
  active: boolean;

  @ViewColumn()
  published: boolean;

  @ViewColumn()
  required: boolean;
}
