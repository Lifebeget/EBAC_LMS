import { Course } from '@entities/course.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from '@entities/user.entity';

@ViewEntity({
  expression: `
    select
      user_id,
      course_id,
      count(1) filter (where vs.type = 'default' and success and required) as homeworks_completed_required,
      count(1) filter (where vs.type = 'default' and success) as homeworks_completed,
      count(1) filter (where vs.type = 'quiz' and success and required) as quizes_completed_required,
      count(1) filter (where vs.type = 'quiz' and success) as quizes_completed,
      count(1) filter (where vs.type = 'survey' and success and required) as surveys_completed_required,
      count(1) filter (where vs.type = 'survey' and success) as surveys_completed
    from view_submission vs
    group by user_id, course_id
  `,
  dependsOn: ['ViewAssessment', 'ViewSubmission'],
})
export class ViewSubmissionCount {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @PrimaryColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  homeworksCompletedRequired: number;

  @ViewColumn()
  homeworksCompleted: number;

  @ViewColumn()
  quizesCompletedRequired: number;

  @ViewColumn()
  quizesCompleted: number;

  @ViewColumn()
  surveysCompletedRequired: number;

  @ViewColumn()
  surveysCompleted: number;
}
