import { Course } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { Section } from '@entities/section.entity';
import { Module } from '@entities/module.entity';
import { AssessmentGradingType, AssessmentType, ContentType } from '@lib/types/enums';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      a.id, a.type, a.score, a.passing_score, a.grading_type,
      a.common_id, a.current_version,
      course.id as course_id,
      c.type as content_type,
      s.id as section_id,
      m.id as module_id, m.active as module_active, m.published as module_published,
      l.id as lecture_id, l.active as lecture_active, l.published as lecture_published,
      (l.required and l.active and m.active) as required,
      (l.active and m.active) as active,
      (l.published and m.published and l.active and m.active) as published
    from assessment a
    left join "content" c on a.id = c.assessment_id
    left join lecture l on c.lecture_id = l.id
    left join module m on m.id = l.module_id
    left join course on course.id = l.course_id
    left join section s on m.section_id = s.id
    where
    a.deleted is null and
    c.id is not null
  `,
})
export class ViewAssessment {
  @PrimaryColumn()
  id: string;

  @ViewColumn()
  commonId: string;

  @ViewColumn()
  currentVersion: boolean;

  @ViewColumn()
  type: AssessmentType;

  @ViewColumn()
  contentType: ContentType;

  @ViewColumn()
  score: number;

  @ViewColumn()
  passingScore: number;

  @ViewColumn()
  gradingType: AssessmentGradingType;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  courseId: string;

  @ManyToOne(() => Section)
  @JoinColumn({ name: 'section_id' })
  section: Section;

  @ViewColumn()
  sectionId: string;

  @ManyToOne(() => Module)
  @JoinColumn({ name: 'module_id' })
  module: Module;

  @ViewColumn()
  moduleId: string;

  @ViewColumn()
  moduleActive: boolean;

  @ViewColumn()
  modulePublished: boolean;

  @ManyToOne(() => Lecture)
  @JoinColumn({ name: 'lecture_id' })
  lecture: Lecture;

  @ViewColumn()
  lectureId: string;

  @ViewColumn()
  lectureActive: boolean;

  @ViewColumn()
  lecturePublished: boolean;

  @ViewColumn()
  active: boolean;

  @ViewColumn()
  published: boolean;

  @ViewColumn()
  required: boolean;
}
