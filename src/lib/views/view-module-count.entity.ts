import { Course } from '@entities/course.entity';
import { Module } from '@entities/module.entity';
import { Section } from '@entities/section.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      m.id,
      m.course_id,
      m.section_id,
      coalesce(l.required, 0) as lectures_required,
      coalesce(l.total, 0) as lectures_total,
      m.published and m.active as published,
      m.active as active,
      l.required > 0 as required
    from module m
    left join (
      select
        module_id,
        count(1) filter (where required = true) as required,
        count(1) filter (where published = true or required = true) as total
      from lecture
      where active = true and deleted is null
      group by module_id
    ) as l on l.module_id = m.id
  `,
})
export class ViewModuleCount {
  @PrimaryColumn()
  id: string;

  @ManyToOne(() => Module)
  @JoinColumn({ name: 'id' })
  module: Course;

  @ViewColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  sectionId: string;

  @ManyToOne(() => Section)
  @JoinColumn({ name: 'section_id' })
  section: Course;

  @ViewColumn()
  lecturesTotal: number;

  @ViewColumn()
  lecturesRequired: number;

  @ViewColumn()
  published: boolean;

  @ViewColumn()
  active: boolean;

  @ViewColumn()
  required: boolean;
}
