import { Course } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { Module } from '@entities/module.entity';
import { AssessmentGradingType, AssessmentType } from '@lib/types/enums';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from '@entities/user.entity';

@ViewEntity({
  expression: `
    select
      a.common_id,
      s.user_id,
      s.lecture_id,
      s.course_id,
      s.module_id,
      a.type,
      max(s.score) filter (where s.status = 'graded') as score,
      coalesce(count(1) filter (where s.status = 'graded' and s.score >= a.passing_score) > 0, false) as success,
      min(s.submitted_at) as first_submission,
      min(s.graded_at) filter (where s.score >= a.passing_score) as first_success,
      count(1) filter (where not s.is_question) as attempts,
      count(1) filter (where s.status = 'submitted') > 0 as in_progress,
      l.required and l.active and m.active as required
    from submission s
    left join assessment a on a.id = s.assessment_id
    left join lecture l on s.lecture_id = l.id
    left join module m on l.module_id = m.id
    left join view_assessment va on va.common_id = a.common_id and va.lecture_id = s.lecture_id
    where s.status IN ('submitted', 'graded') and s.deleted is null
    group by
      s.user_id, s.lecture_id, s.course_id, s.module_id,
      l.required, l.active, m.active, a.type, a.common_id
    -- Filtering out all submissions that are not connected to content
    having count(1) filter (where va.id is not null) > 0
  `,
  dependsOn: ['ViewAssessment'],
})
export class ViewSubmission {
  @PrimaryColumn()
  commonId: string;

  @ViewColumn()
  userId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ViewColumn()
  lectureId: string;

  @ManyToOne(() => Lecture)
  @JoinColumn({ name: 'lecture_id' })
  lecture: Lecture;

  @ViewColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  moduleId: string;

  @ManyToOne(() => Module)
  @JoinColumn({ name: 'module_id' })
  module: Module;

  @ViewColumn()
  type: AssessmentType;

  @ViewColumn()
  score: number;

  @ViewColumn()
  success: boolean;

  @ViewColumn()
  required: boolean;

  @ViewColumn()
  gradingType: AssessmentGradingType;

  @ViewColumn()
  firstSubmission: Date;

  @ViewColumn()
  firstSuccess: Date;

  @ViewColumn()
  attempts: number;

  @ViewColumn()
  inProgress: boolean;
}
