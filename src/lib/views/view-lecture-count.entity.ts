import { Course } from '@entities/course.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      course_id,
      count(1) filter (where published or required) as lectures,
      count(1) filter (where required) as lectures_required,
      count(distinct module_id) filter (where published or required) as modules,
      count(distinct module_id) filter (where required) as modules_required
    from view_lecture
    group by course_id
  `,
  dependsOn: ['ViewLecture'],
})
export class ViewLectureCount {
  @PrimaryColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  lectures: number;

  @ViewColumn()
  lecturesRequired: number;

  @ViewColumn()
  modules: number;

  @ViewColumn()
  modulesRequired: number;
}
