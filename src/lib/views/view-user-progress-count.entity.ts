import { JoinColumn, ManyToOne, PrimaryColumn, ViewColumn, ViewEntity } from 'typeorm';

import { Course } from '@entities/course.entity';
import { CourseStatus } from '@enums/course-status.enum';
import { User } from '@entities/user.entity';

/**
 * WARNING: This view is not suited for analytics and aggregation
 * It is high on memory, cpu and execution time
 * Use it only with UserId filter for displaying on student profile / course page
 */

@ViewEntity({
  expression: `
    select
      e.user_id,
      e.course_id,
      vcc.status as course_status,
      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,
      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,
      count(1) filter (
        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required
      ) as modules_required_completed,
      count(1) filter (
        where vumc.lectures_completed >= vumc.lectures_total
      ) as modules_completed,
      vcc.lectures as lectures_total,
      vcc.lectures_required as lectures_required_total,
      vcc.modules as modules_total,
      vcc.modules_required as modules_required_total,
      vcc.homeworks as homeworks_total,
      vcc.homeworks_required as homeworks_required_total,
      vcc.quizes as quizes_total,
      vcc.quizes_required as quizes_required_total,
      vcc.surveys as surveys_total,
      vcc.surveys_required as surveys_required_total,
      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,
      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,
      coalesce(vsc.quizes_completed, 0) as quizes_completed,
      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,
      coalesce(vsc.surveys_completed, 0) as surveys_completed,
      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,
      case when vcc.lectures_required > 0 then
        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)
      else 0 end as progress,
      CASE
        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric AND vcc.status = 'completed'
        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric AND vcc.status = 'completed'
      END AS graduated
    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e
    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id
    left join view_course_count vcc on vcc.id = e.course_id
    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id
    group by e.user_id, e.course_id,
      vcc.lectures, vcc.lectures_required,
      vcc.modules, vcc.modules_required,
      vcc.homeworks, vcc.homeworks_required,
      vcc.quizes, vcc.quizes_required,
      vcc.surveys, vcc.surveys_required,
      vsc.homeworks_completed, vsc.homeworks_completed_required,
      vsc.quizes_completed, vsc.quizes_completed_required,
      vsc.surveys_completed, vsc.surveys_completed_required,
      vcc.status
  `,
  dependsOn: [
    'ViewLecture',
    'ViewModuleCount',
    'ViewAssessment',
    'ViewLectureCount',
    'ViewAssessmentCount',
    'ViewSubmission',
    'ViewUserModuleCount',
    'ViewCourseCount',
    'ViewSubmissionCount',
  ],
})
export class ViewUserProgressCount {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @PrimaryColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  courseStatus: CourseStatus;

  @ViewColumn()
  lecturesTotal: number;

  @ViewColumn()
  lecturesRequiredTotal: number;

  @ViewColumn()
  lecturesCompleted: number;

  @ViewColumn()
  lecturesRequiredCompleted: number;

  @ViewColumn()
  modulesRequiredCompleted: number;

  @ViewColumn()
  modulesCompleted: number;

  @ViewColumn()
  modulesTotal: number;

  @ViewColumn()
  modulesRequiredTotal: number;

  @ViewColumn()
  homeworksTotal: number;

  @ViewColumn()
  homeworksRequiredTotal: number;

  @ViewColumn()
  quizesTotal: number;

  @ViewColumn()
  quizesRequiredTotal: number;

  @ViewColumn()
  surveysTotal: number;

  @ViewColumn()
  surveysRequiredTotal: number;

  @ViewColumn()
  homeworksCompleted: number;

  @ViewColumn()
  homeworksCompletedRequired: number;

  @ViewColumn()
  quizesCompleted: number;

  @ViewColumn()
  quizesCompletedRequired: number;

  @ViewColumn()
  surveysCompleted: number;

  @ViewColumn()
  surveysCompletedRequired: number;

  @ViewColumn()
  progress: number;

  @ViewColumn()
  graduated: boolean;
}

/** ADDITIONAL PROGRESS CALCULATION METHODS:
    -- Progress method 2: Required "success" lectures + Required "success" activities
    case when (vcc.lectures_required + vcc.homeworks_required + vcc.surveys_required + vcc.quizes_required) > 0 then
        round(
            (sum(vumc.lectures_required_completed) +
            vsc.homeworks_completed_required +
            vsc.quizes_completed_required +
            vsc.surveys_completed_required
            )::float * 100 / (
                vcc.lectures_required +
                vcc.homeworks_required +
                vcc.surveys_required +
                vcc.quizes_required
            )::float
        )
    else 0 end as progress2,
    -- Progress method 3: 50% from required "success" lectures + 50% for required "success" activities
    case when (vcc.homeworks_required + vcc.surveys_required + vcc.quizes_required) > 0 then
        round(
            sum(vumc.lectures_required_completed)::float * 50 / vcc.lectures_required::float +
            (vsc.homeworks_completed_required::float +
            vsc.quizes_completed_required::float +
            vsc.surveys_completed_required::float
            ) * 50 / (
                vcc.homeworks_required +
                vcc.surveys_required +
                vcc.quizes_required
            )::float
        )
    when vcc.lectures_required > 0 then
      sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float
    else 0 end as progress3,
 */
