import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Course } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { ForumMessagesLock } from '@entities/forum-messages-lock.entity';
import { User } from '@entities/user.entity';

@ViewEntity({
  expression: `
    select fm.id,
      fm.pipeline,
      fm.created,
      fm.course_id,
      fm.lecture_id,
      fm.lock_id,
      fm.user_id,
      fm.resolved_user_id,
      fm.substitute_user_id,
      fm.resolved,
      coalesce(fm2.answers, 0) as replies,
      case when
        fm.resolved is null then coalesce(fm2.unresolved, 0) + 1
        else coalesce(fm2.unresolved, 0)
        end as unresolved_count,
      case when
        fm.resolved is null then fm.created
        else coalesce(fm2.unresolved_date, fm.created)
        end as unresolved_date,
      coalesce(fm2.last_resolved, fm.resolved) as last_resolved,
      coalesce(fm2.last_created, fm.created) as last_created,
      case when
        fm.resolved is null then fmt.urgent_date
        else fm2.urgent_date
        end as urgent_date,
      case when
        fm.resolved is null then fmt.outdated_date
        else fm2.outdated_date
        end as outdated_date
    from forum_messages fm
    left join (
      select
        thread_parent_id as id,
        count(1) as answers,
        count(1) filter (where resolved is null and skip_resolve = false) as unresolved,
        min(created) filter (where resolved is null and skip_resolve = false) as unresolved_date,
        max(created) as last_created,
        max(resolved) as last_resolved,
        min(urgent_date) filter (where resolved is null and skip_resolve = false) as urgent_date,
        min(outdated_date) filter (where resolved is null and skip_resolve = false) as outdated_date
      from forum_messages
      left join forum_message_tariff f on forum_messages.id = f.message_id
      where thread_parent_id is not null and deleted is null
      group by thread_parent_id
    ) as fm2 on fm2.id = fm.id
    left join forum_message_tariff fmt on fm.id = fmt.message_id
    where fm.thread_parent_id is null and
    fm.deleted is null;
  `,
})
export class ViewForumThread {
  @PrimaryColumn()
  id: string;

  @ViewColumn()
  created: Date;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  courseId: string;

  @ManyToOne(() => Lecture)
  @JoinColumn({ name: 'lecture_id' })
  lecture: Lecture;

  @ViewColumn()
  lectureId: string;

  @ManyToOne(() => ForumMessagesLock)
  @JoinColumn({ name: 'lock_id' })
  lock: ForumMessagesLock;

  @ViewColumn()
  lockId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ViewColumn()
  userId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'resolved_user_id' })
  resolvedUser: User;

  @ViewColumn()
  resolvedUserId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'substitute_user_id' })
  substituteUser: User;

  @ViewColumn()
  substituteUserId: string;

  @ViewColumn()
  replies: number;

  @ViewColumn()
  unresolvedCount: number;

  @ViewColumn()
  unresolvedDate: Date;

  @ViewColumn()
  resolved: Date;

  @ViewColumn()
  lastResolved: Date;

  @ViewColumn()
  lastCreated: Date;

  @ViewColumn()
  urgentDate: Date;

  @ViewColumn()
  outdatedDate: Date;
}
