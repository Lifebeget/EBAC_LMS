import { ViewEntity } from 'typeorm';
import { ViewUserProgressCount } from './view-user-progress-count.entity';

@ViewEntity({
  expression: `
    select * from view_user_progress_count
  `,
  dependsOn: [
    'ViewLecture',
    'ViewModuleCount',
    'ViewAssessment',
    'ViewLectureCount',
    'ViewAssessmentCount',
    'ViewSubmission',
    'ViewUserModuleCount',
    'ViewCourseCount',
    'ViewSubmissionCount',
    'ViewUserProgressCount',
  ],
  materialized: true,
})
export class ViewUserProgressCountMaterialized extends ViewUserProgressCount {}
