import { Course } from '@entities/course.entity';
import { Module } from '@entities/module.entity';
import { User } from '@entities/user.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      lp.user_id as user_id,
      vl.course_id as course_id,
      vl.module_id as module_id,
      count(1) filter (where vl.required and lp.status = 'success') as lectures_required_completed,
      count(1) filter (where lp.status = 'success') as lectures_completed,
      vmc.lectures_required,
      vmc.lectures_total,
      vmc.required
    from lecture_progress lp
    left join view_lecture vl on vl.id = lp.lecture_id
    left join view_module_count vmc on vmc.id = vl.module_id
    where lp.deleted is null
    group by lp.user_id, vl.course_id, vl.module_id, vmc.lectures_required, vmc.lectures_total, vmc.required
  `,
  dependsOn: ['ViewLecture', 'ViewModuleCount'],
})
export class ViewUserModuleCount {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @PrimaryColumn()
  courseId: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @PrimaryColumn()
  moduleId: string;

  @ManyToOne(() => Module)
  @JoinColumn({ name: 'module_id' })
  module: Course;

  @ViewColumn()
  lecturesTotal: number;

  @ViewColumn()
  lecturesRequired: number;

  @ViewColumn()
  lecturesCompleted: number;

  @ViewColumn()
  lecturesRequiredCompleted: number;

  @ViewColumn()
  required: boolean;
}
