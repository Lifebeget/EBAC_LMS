import { Course } from '@entities/course.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';

@ViewEntity({
  expression: `
    select
      course_id,
      count(1) filter (where type='default' and (published or required)) as homeworks,
      count(1) filter (where type='default' and required) as homeworks_required,
      count(1) filter (where type='quiz' and (published or required)) as quizes,
      count(1) filter (where type='quiz' and required) as quizes_required,
      count(1) filter (where type='survey' and (published or required)) as surveys,
      count(1) filter (where type='survey' and required) as surveys_required
    from view_assessment
    group by course_id
  `,
  dependsOn: ['ViewAssessment'],
})
export class ViewAssessmentCount {
  @PrimaryColumn()
  course_id: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ViewColumn()
  homeworks: number;

  @ViewColumn()
  homeworksRequired: number;

  @ViewColumn()
  quizes: number;

  @ViewColumn()
  quizesRequired: number;

  @ViewColumn()
  surveys: number;

  @ViewColumn()
  surveysRequired: number;
}
