import { Course } from '@entities/course.entity';
import { ViewEntity, ViewColumn, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { CourseStatus } from '@enums/course-status.enum';

@ViewEntity({
  expression: `
    select
      c.id, c.title, c.version, c.published, c.active,
      case when c.version > 1 then concat(c.title, ' v', c.version) else c.title end as title_display,
      c.status as status,
      coalesce(vlc.lectures, 0) as lectures,
      coalesce(vlc.lectures_required, 0) as lectures_required,
      coalesce(vlc.modules, 0) as modules,
      coalesce(vlc.modules_required, 0) as modules_required,
      coalesce(vac.homeworks, 0) as homeworks,
      coalesce(vac.homeworks_required, 0) as homeworks_required,
      coalesce(vac.quizes, 0) as quizes,
      coalesce(vac.quizes_required, 0) as quizes_required,
      coalesce(vac.surveys, 0) as surveys,
      coalesce(vac.surveys_required, 0) as surveys_required
    from course c
    left join view_lecture_count vlc on vlc.course_id = c.id
    left join view_assessment_count vac on vac.course_id = c.id
  `,
  dependsOn: ['ViewAssessment', 'ViewLecture', 'ViewLectureCount', 'ViewAssessmentCount'],
})
export class ViewCourseCount {
  @PrimaryColumn()
  id: string;

  @ManyToOne(() => Course)
  @JoinColumn({ name: 'id' })
  course: Course;

  @ViewColumn()
  title: string;

  @ViewColumn()
  version: number;

  @ViewColumn()
  titleDisplay: string;

  @ViewColumn()
  active: boolean;

  @ViewColumn()
  published: boolean;

  @ViewColumn()
  status: CourseStatus;

  @ViewColumn()
  lectures: number;

  @ViewColumn()
  lecturesRequired: number;

  @ViewColumn()
  modules: number;

  @ViewColumn()
  modulesRequired: number;

  @ViewColumn()
  homeworks: number;

  @ViewColumn()
  homeworksRequired: number;

  @ViewColumn()
  quizes: number;

  @ViewColumn()
  quizesRequired: number;

  @ViewColumn()
  surveys: number;

  @ViewColumn()
  surveysRequired: number;
}
