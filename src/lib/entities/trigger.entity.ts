import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Trigger {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    type: () => User,
    description: 'Related user',
  })
  @ManyToOne(() => User, { nullable: true })
  user?: User;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  @Index()
  created: Date;

  @ApiProperty({
    enum: () => TriggerType,
    description: 'Trigger type',
  })
  @Index()
  @Column('varchar', { length: 64, nullable: false })
  triggerType: TriggerType;

  @ApiProperty({
    description: 'Executed status',
  })
  @Index()
  @Column('boolean', { default: false })
  executed: boolean;

  @ApiProperty({
    description: 'Canceled status',
  })
  @Index()
  @Column('boolean', { default: false })
  canceled: boolean;

  @ApiProperty({
    description: 'Date when trigger should be executed',
  })
  @Column({ type: 'timestamptz', default: () => 'NOW()' })
  @Index()
  dateToExecute: Date;

  @ApiProperty({
    description: 'Additional trigger data',
  })
  @Column('jsonb', { nullable: true })
  data?: Record<string, unknown>;

  @ApiProperty({
    description: 'Priority',
  })
  @Column('varchar', { length: 16, nullable: true })
  priority: TriggerPriority;

  @ApiProperty({
    description: 'Unique hash for triggerOnce events',
  })
  @Index()
  @Column({ nullable: true })
  onceHash?: string;

  skipped?: boolean;
}
