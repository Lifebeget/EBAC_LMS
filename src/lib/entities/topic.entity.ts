import { Webinar } from '@entities/webinars.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Topic {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'externalId' })
  @Column({ unique: true })
  externalId: number;

  @ManyToOne(() => Webinar)
  schedule?: Webinar;

  @Column({ nullable: true })
  scheduleId?: string;

  @Field()
  @ApiProperty({ description: 'title' })
  @Column({ nullable: true })
  title?: string;

  @Field()
  @ApiProperty({ description: 'description' })
  @Column({ nullable: true })
  description?: string;

  @Field()
  @ApiProperty({ description: 'webinarLink' })
  @Column({ nullable: true })
  webinarLink?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'recordingLink' })
  @Column({ nullable: true })
  recordingLink?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'regularDiscountPromocode' })
  @Column({ nullable: true })
  regularDiscountPromocode?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'killerDiscountPromocode' })
  @Column({ nullable: true })
  killerDiscountPromocode?: string;

  @Column({ nullable: true })
  imageUrl?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'speakerName' })
  @Column({ nullable: true })
  speakerName: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'speakerDescription' })
  @Column({ nullable: true })
  speakerDescription?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'dayNumber' })
  @Column({ nullable: true })
  dayNumber: number;

  @Field()
  @ApiProperty({ description: 'created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'modified' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'deleted' })
  @DeleteDateColumn({ type: 'timestamptz', nullable: true })
  deleted: Date;

  @Column({ nullable: true })
  speakerImageUrl?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'regularDiscountDescription' })
  @Column({ nullable: true })
  regularDiscountDescription?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'killerDiscountDescription' })
  @Column({ nullable: true })
  killerDiscountDescription?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'topicDateUtc' })
  @Column({ type: 'timestamptz', nullable: true })
  topicDateUtc?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'topicDateBrazil' })
  @Column({ type: 'timestamptz', nullable: true })
  topicDateBrazil?: Date;

  @Field()
  @ApiProperty({ description: 'speakerSubtitle' })
  @Column({ nullable: true })
  speakerSubtitle?: string;

  @Field()
  @ApiProperty({ description: 'topicActive' })
  @Column({ default: true })
  topicActive?: boolean;

  @Field({ nullable: true })
  @ApiProperty({ description: 'speakerFromFirstDay' })
  @Column({ nullable: true, default: false })
  speakerFromFirstDay?: boolean;

  @Field({ nullable: true })
  @ApiProperty({ description: 'speakerFromSecondDay' })
  @Column({ nullable: true, default: false })
  speakerFromSecondDay?: boolean;
}
