import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { ServiceLevelTariff } from './service-level-tariff.entity';
import { ForumMessages } from './forum-messages.entity';
@ObjectType()
@Entity()
export class ForumMessageTariff {
  @Field(() => ForumMessages)
  @ApiProperty({ type: () => ForumMessages, description: 'ForumMessages' })
  @OneToOne(() => ForumMessages, message => message.tariff)
  @JoinColumn({ name: 'message_id' })
  message: ForumMessages;

  @Field()
  @PrimaryColumn()
  messageId: string;

  @Field(() => ServiceLevelTariff)
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'Tariff that was active at the time of creation',
  })
  @ManyToOne(() => ServiceLevelTariff)
  @JoinColumn({ name: 'create_tariff_id' })
  createTariff: ServiceLevelTariff;

  @Field()
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'ID of tariff that was active at the time of creation',
  })
  @Column()
  @Index()
  createTariffId: string;

  @Field(() => ServiceLevelTariff)
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'Tariff that was active at the time of grading',
  })
  @ManyToOne(() => ServiceLevelTariff)
  @JoinColumn({ name: 'resolve_tariff_id' })
  resolveTariff: ServiceLevelTariff;

  @Field()
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'ID of tariff that was active at the time of grading',
  })
  @Column({ nullable: true })
  @Index()
  resolveTariffId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date after which message becomes urgent' })
  @Column({ type: 'timestamptz', nullable: true })
  urgentDate: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date after which message becomes outdated' })
  @Column({ type: 'timestamptz', nullable: true })
  outdatedDate: Date;

  @ApiProperty({
    description: 'Title of current service level',
    required: false,
  })
  levelTitle?: string;
}
