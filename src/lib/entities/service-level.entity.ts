import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  OneToMany,
  PrimaryGeneratedColumn,
  Entity,
  AfterLoad,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { ServiceLevelTariff } from './service-level-tariff.entity';
import { Assessment } from './assessment.entity';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { ServiceLevel as ServiceLevelInterface } from '@lib/api/directory/service-levels/types/service-level';

@ObjectType()
@Entity('assessment_level')
export class ServiceLevel implements ServiceLevelInterface {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Title of level' })
  @Column()
  title: string;

  @Field()
  @ApiProperty({ description: 'Is this level is default' })
  @Column()
  isDefault: boolean;

  @Field(() => [ServiceLevelTariff])
  @ApiProperty({
    description: 'Tariffs history for this level',
  })
  @OneToMany(() => ServiceLevelTariff, tariff => tariff.level)
  tariffs?: ServiceLevelTariff[];

  @Field(() => [Assessment])
  @ApiProperty({
    description: 'Assessments of this level',
  })
  @OneToMany(() => Assessment, assessment => assessment.lecture)
  assessments: Assessment[];

  @Field()
  @ApiProperty({ enum: ServiceLevelTarget })
  @Column('varchar', { length: 15, default: ServiceLevelTarget.Submission })
  target: ServiceLevelTarget;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;

  @AfterLoad()
  sortTariffs() {
    if (this?.tariffs?.length) {
      this.tariffs.sort((a, b) => {
        const adate = typeof a.activeFrom == 'string' ? new Date(a.activeFrom) : a.activeFrom;
        const bdate = typeof b.activeFrom == 'string' ? new Date(b.activeFrom) : b.activeFrom;
        return adate.getTime() - bdate.getTime();
      });
    }
  }

  @ApiProperty({ description: 'List of courses with this level' })
  courses?: { id: string; title: string }[];
}
