import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { QueueStatus } from '@lib/types/enums/queue.enum';

@Entity()
export class CertsGenerationQueue {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Created date' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Modified date' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field({ nullable: true })
  @Column({ nullable: true })
  webinarId?: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  courseId?: string;

  @Field()
  @Column({ default: QueueStatus.Active })
  status: QueueStatus;

  @Field()
  @ApiProperty({ description: 'Total jobs count' })
  @Column({ default: 0 })
  total: number;

  @Field()
  @ApiProperty({ description: 'Processed jobs count' })
  @Column({ default: 0 })
  processed: number;

  @Column({ type: 'jsonb', default: [] })
  completedForUsers: string[];

  @Column({ type: 'jsonb', default: [] })
  failedForUsers: string[];
}
