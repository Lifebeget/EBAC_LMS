import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Assessment } from './assessment.entity';
import { File } from './file.entity';
import { Topic } from './topic.entity';
import { ArrayNotEmpty, ValidateIf, IsNotEmpty } from 'class-validator';
import { Segment } from './segment.entity';

export class WebinarConditions {
  @ValidateIf(o => o.any == null && o.fact == null)
  @ArrayNotEmpty()
  all?: WebinarConditions[];

  @ValidateIf(o => o.all == null && o.fact == null)
  @ArrayNotEmpty()
  any?: WebinarConditions[];

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  fact?: string;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  operator?: string;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  value?: any;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  subValue?: any;
}

@ObjectType()
@Entity()
export class Webinar {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'externalId' })
  @Column()
  @Index()
  externalId: number;

  @Field()
  @ApiProperty({ description: 'slug' })
  @Column()
  slug: string;

  @Field()
  @ApiProperty({ description: 'name' })
  @Column()
  name: string;

  @Field()
  @ApiProperty({ description: 'type' })
  @Column()
  type: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'additional' })
  @Column({ nullable: true })
  additional: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'subtitle' })
  @Column({ nullable: true })
  subtitle: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'startDateUTC' })
  @Column({ type: 'timestamptz', nullable: true })
  startDateUtc?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'startDateText' })
  @Column({ nullable: true })
  startDateText: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'active' })
  @Column({ default: true })
  active?: boolean;

  @Field()
  @ApiProperty({ description: 'finished' })
  @Column({ default: false })
  finished?: boolean;

  @Field({ nullable: true })
  @ApiProperty({ description: 'pageURL' })
  @Column({ nullable: true })
  pageUrl?: string;

  @Column({ nullable: true })
  @ApiProperty()
  pageUrlOriginal: string | null;

  @Field(type => File, { nullable: true })
  @ApiProperty({ type: () => File, description: 'showcaseImage' })
  @OneToOne(() => File, { cascade: true })
  @JoinColumn()
  showcaseImage?: File;

  @Field({ nullable: true })
  @ApiProperty({ description: 'speakerBiography' })
  @Column({ nullable: true })
  speakerBiography?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'description' })
  @Column({ nullable: true })
  description: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'directionId' })
  @Column({ nullable: true })
  directionId: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'youtubePlaylistId' })
  @Column({ nullable: true })
  youtubePlaylistId: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'recordLink' })
  @Column({ nullable: true })
  recordLink: string;

  @Field()
  @ApiProperty({ description: 'created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'modified' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'deleted' })
  @DeleteDateColumn({ type: 'timestamptz', nullable: true, default: null })
  deleted: Date;

  @Field()
  @ApiProperty({ description: 'rev' })
  @Column()
  rev: number;

  @ApiProperty({ type: Topic, isArray: true })
  @OneToMany(() => Topic, topic => topic.schedule)
  topics: Topic[];

  @ApiProperty({ description: 'published' })
  @Column({ default: false })
  published?: boolean;

  @ApiProperty({ description: 'finalStartDateUtc' })
  @Column({ type: 'timestamptz', nullable: true })
  finalStartDateUtc?: Date;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 64, default: 'default' })
  certTemplate?: string;

  @Field()
  @ApiProperty()
  @Column({ default: '{}' })
  certTemplateData?: string;

  @Field(type => Assessment, { nullable: true })
  @ApiProperty({ type: () => Assessment })
  @ManyToOne(() => Assessment)
  @JoinColumn({ name: 'survey_id' })
  survey?: Assessment;

  @Field()
  @Column({ nullable: true })
  surveyId: string;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  @ApiProperty({ description: 'Survey open date' })
  surveyOpenDate?: Date;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  @ApiProperty({ description: 'Survey close date' })
  surveyCloseDate?: Date;

  @Column({ default: true })
  @ApiProperty()
  public: boolean;

  @Column({ type: 'jsonb', nullable: true })
  conditions: WebinarConditions;

  @ApiProperty({ type: Segment })
  @ManyToOne(() => Segment)
  segment?: Segment;
}
