import { HierarchyContext } from '@enums/hierarchy-context';
import { HierarchyRole } from '@enums/hierarchy-role.enum';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class Hierarchy {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => User)
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { nullable: false })
  parent: User;

  @Field(type => User, { nullable: true })
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { nullable: true })
  child: User;

  @Field()
  @ApiProperty({ enum: HierarchyRole })
  @Column('varchar', { length: 40 })
  role: HierarchyRole;

  @Field({ nullable: true })
  @ApiProperty({ enum: HierarchyContext })
  @Column('varchar', { length: 40, nullable: true })
  context?: HierarchyContext;

  @Field({ nullable: true })
  @Column('uuid', { nullable: true })
  contextId?: string;
}
