import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsNotEmpty, ValidateIf } from 'class-validator';
import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export class Conditions {
  @ValidateIf(o => o.any == null && o.fact == null)
  @ArrayNotEmpty()
  all?: Conditions[];

  @ValidateIf(o => o.all == null && o.fact == null)
  @ArrayNotEmpty()
  any?: Conditions[];

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  fact?: string;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  operator?: string;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  value?: any;

  @ValidateIf(o => o.any == null && o.all == null)
  @IsNotEmpty()
  subValue?: any;
}

@Entity()
export class Segment {
  @Column()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  @ApiProperty({ description: 'Segment name' })
  @IsNotEmpty()
  name: string;

  @Column()
  @ApiProperty({ description: 'Segment description', required: false })
  description?: string;

  @Column({ type: 'jsonb', nullable: true })
  @ApiProperty({ description: 'Segment condition' })
  condition?: Conditions;

  @DeleteDateColumn({ type: 'timestamptz' })
  deletedAt?: Date;

  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
