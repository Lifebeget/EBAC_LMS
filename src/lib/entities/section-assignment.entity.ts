import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { Section } from './section.entity';

@ObjectType()
@Entity()
export class SectionAssignment {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => [SectionAssignment])
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Field()
  @Column()
  userId: string;

  @Field(type => [SectionAssignment])
  @ApiProperty({ type: () => Section })
  @ManyToOne(() => Section, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'section_id' })
  section: Section;

  @Field()
  @Column()
  sectionId: string;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  exclusive?: boolean;
}
