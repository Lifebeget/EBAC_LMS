import { CriterionAnswer } from '@lib/types/enums/criterion-answer.enum';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, DeleteDateColumn, Index } from 'typeorm';

@ObjectType()
export abstract class BaseCriterionEntry {
  @Field()
  @ApiProperty({ description: 'Criteria state' })
  @Column({
    type: 'enum',
    enum: CriterionAnswer,
  })
  state: CriterionAnswer;

  @Field()
  @ApiProperty({ description: 'Deletion date' })
  @Index()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;
}
