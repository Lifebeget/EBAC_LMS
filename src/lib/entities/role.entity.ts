import { SystemRole } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class Role {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => [Role])
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  user: User;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 40 })
  role: SystemRole;

  @Field()
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  dateFrom?: Date;

  @Field()
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  dateTo?: Date;
}
