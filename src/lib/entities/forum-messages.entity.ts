import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Course } from './course.entity';
import { ForumMessagesLock } from './forum-messages-lock.entity';
import { ForumMessageTariff } from './forum-tariff.entity';
import { Lecture } from './lecture.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class ForumMessages {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX). Must be unique' })
  @Column({ nullable: true, unique: true })
  @Index()
  externalId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Message title' })
  @Column({ nullable: true })
  title?: string;

  @Field()
  @ApiProperty({ description: 'Message text' })
  @Column()
  body: string;

  @Field()
  @ApiProperty({ description: 'Like count' })
  @Column({ default: 0 })
  score: number;

  @Field(type => [ForumMessages])
  @ApiProperty({ description: 'First message of thread(main parent)' })
  @ManyToOne(() => ForumMessages)
  threadParent: ForumMessages;

  @Field({ nullable: true })
  @Index()
  @Column({ nullable: true })
  threadParentId: string;

  @Field(type => ForumMessages)
  @ApiProperty({ description: 'Parent message' })
  @ManyToOne(() => ForumMessages)
  parent: ForumMessages;

  @Field({ nullable: true })
  @Column({ nullable: true })
  parentId: string;

  @Field()
  @ApiProperty({ description: 'Sticky' })
  @Column({ default: false })
  sticky: boolean;

  @Field()
  @ApiProperty({ description: 'Hidden(banned)' })
  @Column({ default: false })
  hidden: boolean;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true, default: null })
  edited?: Date;

  @Field()
  @Index()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture, description: 'Lecture' })
  @ManyToOne(() => Lecture)
  lecture?: Lecture;

  @Field({ nullable: true })
  @Index()
  @Column({ nullable: true })
  lectureId?: string;

  @Field(type => Course)
  @ApiProperty({ type: () => Course, description: 'Course' })
  @ManyToOne(() => Course)
  course?: Course;

  @Field()
  @Index()
  @Column()
  courseId: string;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User)
  user: User;

  @Field()
  @Column()
  userId: string;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'Resolved user(support, tutor)',
  })
  @ManyToOne(() => User)
  resolvedUser: User;

  @Field()
  @Column({ nullable: true })
  resolvedUserId: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true, default: null })
  resolved?: Date;

  @Field()
  @ApiProperty({ description: 'Skip resolve' })
  @Column({ default: false })
  skipResolve: boolean;

  @Field()
  @ApiProperty({
    enum: () => ForumThreadPipeline,
    description: 'Thread pipeline',
  })
  @Index()
  @Column('varchar', {
    length: 64,
    nullable: true,
    default: ForumThreadPipeline.support,
  })
  pipeline: ForumThreadPipeline;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'Substitute user(support, tutor)',
  })
  @ManyToOne(() => User)
  substituteUser: User;

  @Field()
  @Column({ nullable: true })
  substituteUserId: string;

  @Field(type => ForumMessagesLock)
  @ApiProperty({
    type: () => ForumMessagesLock,
    description: 'message lock',
  })
  @OneToOne(() => ForumMessagesLock)
  @JoinColumn({ name: 'lock_id' })
  lock?: ForumMessagesLock;

  @Field(() => ForumMessageTariff)
  @ApiProperty({
    type: () => ForumMessageTariff,
    description: 'Tariff',
  })
  @OneToOne(() => ForumMessageTariff, tariff => tariff.message, {
    cascade: true,
  })
  tariff: ForumMessageTariff;
}
