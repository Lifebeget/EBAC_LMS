import { Field, ObjectType } from '@nestjs/graphql';
import { Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Answer } from '@entities/answer.entity';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { BaseCriterionEntry } from './base-criterion-entry.entity';

@ObjectType()
@Unique('UNIQUE_ANSWER_AND_CRITERION', ['answerId', 'criteriaId'])
@Entity()
export class AssessmentCriterionEntry extends BaseCriterionEntry {
  @Field()
  @ApiProperty({ description: 'Answer UUID' })
  @PrimaryColumn({ update: false })
  answerId: string;

  @Field(() => Answer)
  @ApiProperty({ description: 'Answer', type: () => Answer })
  @ManyToOne(() => Answer, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'answer_id' })
  answer: Answer;

  @Field()
  @ApiProperty({ description: 'Criteria UUID' })
  @PrimaryColumn({ update: false })
  criteriaId: string;

  @Field(() => AssessmentCriterion)
  @ApiProperty({
    description: 'Criteria',
    type: () => AssessmentCriterion,
  })
  @ManyToOne(() => AssessmentCriterion, criterion => criterion.assessmentCriteriaEntries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'criteria_id' })
  criteria: AssessmentCriterion;
}
