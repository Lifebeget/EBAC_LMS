import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  Index,
} from 'typeorm';
import { File } from './file.entity';
import { Course } from './course.entity';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { Webinar } from './webinars.entity';

@ObjectType()
@Entity()
export class Cert {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Unique cert number' })
  @Column('varchar', { length: 24, default: '00000-00000-00000-00000' })
  uniqueCertId: string;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 64, default: 'default' })
  template?: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field(type => File)
  @ApiProperty({ type: () => File })
  @OneToOne(() => File)
  @JoinColumn()
  file?: File;

  @Field(type => User)
  @Index()
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User)
  user: User;

  @Field(type => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course)
  course?: Course;

  @Field(type => Webinar)
  @ApiProperty({ type: () => Webinar })
  @ManyToOne(() => Webinar)
  webinar?: Webinar;
}
