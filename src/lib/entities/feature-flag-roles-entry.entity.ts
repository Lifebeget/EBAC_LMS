import { Field, ObjectType } from '@nestjs/graphql';
import { Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { AppRole } from '@lib/types/enums';

@ObjectType()
@Unique('UNIQUE_FEATURE_USER_ROLE', ['featureName', 'role'])
@Entity()
export class FeatureFlagRolesEntry {
  @Field()
  @ApiProperty({
    description: 'Feature name',
    enum: FeatureName,
  })
  @PrimaryColumn({
    enum: FeatureName,
    update: false,
  })
  featureName: FeatureName;

  @Field(() => FeatureFlag)
  @ApiProperty({ description: 'Feature flag' })
  @ManyToOne(() => FeatureFlag, feature => feature.roleEntries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'feature_name' })
  featureFlag: FeatureFlag;

  @Field()
  @ApiProperty({
    description: 'Feature flag entry for user role',
  })
  @PrimaryColumn({ update: false, enum: AppRole })
  role: AppRole;
}
