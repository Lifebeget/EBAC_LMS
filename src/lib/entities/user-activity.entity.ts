import { Field, ObjectType } from '@nestjs/graphql';
import { Entity, Column, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn, JoinColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class UserActivity {
  @Field()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => User)
  @OneToOne(() => User, user => user.activity)
  @JoinColumn()
  user: User;

  @Field()
  @Column('varchar', { length: 40, nullable: true })
  activity: string;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  lastLogin: Date;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  lastSubmissionView: Date;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  firstLectureView: Date;

  @Field()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
