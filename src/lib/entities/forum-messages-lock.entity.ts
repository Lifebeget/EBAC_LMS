import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class ForumMessagesLock {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User)
  user: User;

  @Field()
  @Column()
  userId: string;

  @ApiProperty()
  @Column({ type: 'timestamptz' })
  mutex?: Date;
}
