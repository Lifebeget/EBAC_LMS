import { Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, PrimaryColumn } from 'typeorm';

export class ForumStatistic {
  @Field()
  @ApiProperty({ description: 'Statistic slice date' })
  @PrimaryColumn({ type: 'timestamptz' })
  statDate: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of sent messages or threads' })
  @Column({ nullable: true })
  sent: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Number of messages or threads sent in statDate',
  })
  @Column({ nullable: true })
  sentToday: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of resolved messages or threads' })
  @Column({ nullable: true })
  resolved: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Number of messages resolved in statDate (for threads is null)',
  })
  @Column({ nullable: true })
  resolvedToday: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of unresolved messages or threads' })
  @Column({ nullable: true })
  unresolved: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Number of messages or threads unresolved in startDate',
  })
  @Column({ nullable: true })
  unresolvedToday: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Number of urgent messages or threads',
  })
  @Column({ nullable: true })
  urgent: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Number of outdated messages or threads',
  })
  @Column({ nullable: true })
  outdated: number;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'Is this statistics for threads',
  })
  @PrimaryColumn({ nullable: false })
  isThread: boolean;
}

export const emptyForumStatistic: ForumStatistic = {
  sent: 0,
  sentToday: 0,
  resolved: 0,
  resolvedToday: 0,
  unresolved: 0,
  unresolvedToday: 0,
  urgent: 0,
  outdated: 0,
  isThread: false,
  statDate: new Date(),
};
