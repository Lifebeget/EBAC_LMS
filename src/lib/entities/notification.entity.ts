import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

import { ApiProperty } from '@nestjs/swagger';
import { NotificationData } from '../types/NotificationData';
import { NotificationType } from '../enums/notification-type.enum';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class Notification {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Notification title' })
  @Column()
  title: string;

  @Field()
  @ApiProperty({ description: 'Notification body' })
  @Column('text')
  body: string;

  @Field()
  @ApiProperty({ description: 'When notification was created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'When notification was read' })
  @Column({ type: 'timestamptz', nullable: true })
  readAt?: Date;

  @Field()
  @ApiProperty({ description: 'Notification has been read' })
  @Column({ nullable: false, default: false })
  isRead: boolean;

  @Field()
  @ApiProperty({ description: 'Notification has been deleted' })
  @Column({ nullable: false, default: false })
  deleted: boolean;

  @Field()
  @ApiProperty({ enum: NotificationType, description: 'Type of notification' })
  @Column('varchar', { length: 32 })
  notificationType: NotificationType;

  @ApiProperty({ description: 'Additional notification data' })
  @Column('jsonb', { nullable: true })
  notificationData?: NotificationData;

  @Field(type => User, { nullable: true })
  @ApiProperty({
    type: () => User,
    description: 'Notification author, can be null',
  })
  @ManyToOne(() => User, { nullable: true })
  author?: User;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'Notification recipient',
  })
  @ManyToOne(() => User)
  recipient: User;

  @Field()
  @ApiProperty({ description: 'Is notification important' })
  @Column({ nullable: true })
  important?: boolean;
}
