import { NomenclatureType } from '@enums/nomenclature-type';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Nomenclature {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'External ID' })
  @Column({ unique: true })
  externalId: number;

  @Field()
  @ApiProperty({ description: 'Category' })
  @Column({ nullable: true })
  category?: string;

  @Field()
  @ApiProperty({ description: 'Type' })
  @Column({
    type: 'enum',
    enum: NomenclatureType,
    default: NomenclatureType.COURSE,
  })
  type: NomenclatureType;

  @Field()
  @ApiProperty({ description: 'Short Name' })
  @Column()
  shortName?: string;

  @Field()
  @ApiProperty({ description: 'Name' })
  @Column()
  name?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Page Url' })
  @Column({ nullable: true })
  pageUrl?: string;

  @Field()
  @ApiProperty({ description: 'created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'modified' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Field()
  @ApiProperty({ description: 'Product Id Rd' })
  @Column({ nullable: true })
  productIdRd: number;

  @Field()
  @ApiProperty({ description: 'Program Url' })
  @Column({ nullable: true })
  programUrl?: string;

  @Field()
  @ApiProperty({ description: 'Color' })
  @Column()
  color?: string;

  @Field()
  @ApiProperty({ description: 'Campaign Id Rd' })
  @Column({ nullable: true })
  campaignIdRd: string;
}
