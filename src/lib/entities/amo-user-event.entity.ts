import { Entity, Column, PrimaryGeneratedColumn, UpdateDateColumn, CreateDateColumn, JoinColumn, ManyToOne, Index } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '@entities/user.entity';
import { Course } from '@entities/course.entity';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';
import { type } from 'os';

@Entity()
export class AmoUserEvent {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    type: () => User,
    description: 'Related user',
  })
  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  @Index()
  user?: User;

  @Column({ nullable: true })
  userId?: string;

  @ApiProperty({
    type: () => Course,
    description: 'Related course',
  })
  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  @Index()
  course?: Course;

  @Column({ nullable: true })
  courseId?: string;

  @ApiProperty({
    enum: () => AmoUserEventType,
    description: 'Trigger type',
  })
  @Index()
  @Column('varchar', { length: 64, nullable: false })
  eventType: AmoUserEventType;

  @ApiProperty({
    description: 'Is event successfully created in AmoCRM',
  })
  @Column('boolean', { default: false })
  success: boolean;

  @ApiProperty({
    description: 'Time of event creation in AmoCRM',
  })
  @Column({ nullable: true })
  sendTime?: Date;

  @ApiProperty({
    description: 'AmoCRM lead id',
  })
  @Column({ nullable: true })
  amoId?: string;

  @ApiProperty({
    description: 'Additional event data',
  })
  @Column('jsonb', { nullable: true })
  data?: Record<string, unknown>;

  @ApiProperty({
    description: 'Amo response data',
  })
  @Column('jsonb', { nullable: true })
  responseData?: Record<string, unknown>;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
