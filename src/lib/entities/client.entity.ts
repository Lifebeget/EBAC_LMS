import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import GraphQLJSON from 'graphql-type-json';
import { Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Client {
  @Field()
  @ApiProperty({ description: 'id' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column('integer', { nullable: true })
  @Index()
  externalId: number;

  @Field()
  @ApiProperty({ description: 'client email' })
  @Column('varchar', { nullable: true })
  email: string;

  @Field()
  @ApiProperty({ description: 'client phone' })
  @Column('varchar', { nullable: true })
  phone: string;

  @Field()
  @ApiProperty({ description: 'client full name' })
  @Column('varchar', { nullable: true })
  fullName: string;

  @Field()
  @ApiProperty({ description: 'client name' })
  @Column('varchar', { nullable: true })
  name: string;

  @Field()
  @ApiProperty({ description: 'client last name' })
  @Column('varchar', { nullable: true })
  lastName: string;

  @Field()
  @ApiProperty({ description: 'created at' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'modified at' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Field(() => GraphQLJSON, { nullable: true })
  @ApiProperty({ description: 'options' })
  @Column('jsonb', { nullable: true })
  options: Record<string, unknown>;

  @Column({ nullable: true })
  @ApiProperty()
  country?: string;
}
