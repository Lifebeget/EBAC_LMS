import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Column, DeleteDateColumn, Index, JoinColumn, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '@entities/base.entity';
import { User } from '@entities/user.entity';
import { Min } from 'class-validator';

@ObjectType()
export abstract class BaseCriterion extends BaseEntity {
  @Field()
  @ApiProperty({ description: 'Criterion category ID' })
  @Index()
  @Column()
  categoryId: string;

  @Field()
  @ApiProperty({ description: 'Criterion title' })
  @Column('text', { nullable: false })
  @Min(1)
  title: string;

  @Field(() => Int)
  @ApiProperty({ description: 'Weight of criterion evaluation' })
  @Column('int', { default: 1 })
  weight: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Criterion author ID' })
  @Index()
  @Column({ nullable: true })
  authorId?: string;

  @Field(() => User)
  @ApiProperty({ type: () => User, description: 'Author (user)' })
  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({ name: 'author_id' })
  author?: User;

  @Field(() => Int)
  @ApiProperty({ description: 'Sort value' })
  @Column('int', { default: 500 })
  sort: number;

  @Field()
  @ApiProperty({ description: 'Deletion date' })
  @Index()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;
}
