import { Entity, Column, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { EncryptionTransformer } from 'typeorm-encrypted';
import * as assert from 'assert';
import * as dotenv from 'dotenv';
import { Field, ObjectType } from '@nestjs/graphql';
import { Exclude } from 'class-transformer';
dotenv.config();
assert(process.env.CRYPTO_KEY);

@ObjectType()
@Entity()
export class Profile {
  @Field(() => User)
  @ApiProperty({ type: () => User })
  @OneToOne(() => User, {
    primary: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  //@PrimaryColumn('uuid')
  userId: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  name?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  surname?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  birthdate?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    type: 'varchar',
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  cpf?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    type: 'varchar',
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  city?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    type: 'varchar',
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  phone?: string;

  @ApiProperty()
  @Exclude()
  @Column({
    nullable: true,
    type: 'varchar',
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  phoneClean?: string;

  @Field()
  @ApiProperty()
  @Column({
    nullable: true,
    type: 'varchar',
    transformer: new EncryptionTransformer({
      key: process.env.CRYPTO_KEY,
      algorithm: 'aes-256-cbc',
      ivLength: 16,
      iv: process.env.CRYPTO_IV,
    }),
  })
  data?: string;
}
