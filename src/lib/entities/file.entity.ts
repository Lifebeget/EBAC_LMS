import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, DeleteDateColumn, Index } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class File {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty()
  @Column()
  url: string;

  @ApiProperty()
  @Column({ nullable: true })
  showExpanded: boolean;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true })
  eadboxUrl: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true })
  s3Key: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true })
  path?: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true })
  mimetype?: string;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field()
  @ApiProperty()
  @Column({ nullable: true, default: '' })
  description: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('bigint', { nullable: true })
  size: number;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('varchar', { length: 20 })
  extension: string;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 255 })
  filename: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  @Index()
  deleted?: Date;

  @Field(() => User, { nullable: true })
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { nullable: true })
  owner?: User;
}
