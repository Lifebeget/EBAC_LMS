import { Field, ObjectType } from '@nestjs/graphql';
import { Column, DeleteDateColumn, Generated, Index } from 'typeorm';
import { BaseEntity } from '@entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { BaseQuestion } from './base-question.entity';
import { BaseAnswer } from './base-answer.entity';

@ObjectType()
export abstract class BaseAssessment extends BaseEntity {
  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  description?: string;

  @Field()
  @ApiProperty()
  @Column('boolean', { default: true })
  currentVersion: boolean;

  @Field()
  @ApiProperty({ description: 'common id for same assessment' })
  @Column()
  @Generated('uuid')
  commonId: string;

  @Field()
  @ApiProperty({ description: 'Deletion date' })
  @Index()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;

  questions: BaseQuestion[];

  submissions: BaseAnswer[];
}
