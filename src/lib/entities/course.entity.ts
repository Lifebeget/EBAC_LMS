import {
  Entity,
  Column,
  Index,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  OneToOne,
  ManyToMany,
  ManyToOne,
  JoinTable,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Module } from './module.entity';
import { Lecture } from './lecture.entity';
import { Tag } from './tag.entity';
import { File } from './file.entity';
import { CourseType } from '../enums/course-type.enum';
import { Section } from './section.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from '@entities/user.entity';
import { CourseStatus } from '@enums/course-status.enum';
import { Enrollment } from './enrollment.entity';
import { Faq } from 'src/faq/entities/faq.entity';
import { index } from 'cheerio/lib/api/traversing';
import { Banner } from './banner.entity';

export interface CourseCounters {
  modules?: number;
  lectures?: number;
  students?: number;
  tutors?: number;
  views?: number;
  completed?: number;
  assessments?: number;
  certs?: number;
}

@ObjectType()
@Entity()
export class Course {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'ID in CMS' })
  @Column({ nullable: true })
  @Index()
  cmsId: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty({ description: 'URL part. Must be unique' })
  @Column({ unique: true })
  slug: string;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field()
  @ApiProperty()
  @Column('text')
  description?: string;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  active?: boolean;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  showLink?: boolean;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  @Index()
  published?: boolean;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  publishedAt?: Date;

  @Field(type => [Module])
  @ApiProperty({ type: () => [Module] })
  @OneToMany(() => Module, module => module.course)
  modules: Module[];

  @Field(type => [Section])
  @ApiProperty({ type: () => [Section] })
  @OneToMany(() => Section, section => section.course)
  sections: Section[];

  @Field(type => [Lecture])
  @ApiProperty({ type: () => [Lecture] })
  @OneToMany(() => Lecture, lecture => lecture.course)
  lectures: Lecture[];

  @Field(type => [Enrollment])
  @ApiProperty({ type: () => [Enrollment] })
  @OneToMany(() => Enrollment, enrollment => enrollment.course)
  enrollments: Enrollment[];

  @Field(type => [Tag])
  @ApiProperty({ type: () => [Tag] })
  @ManyToMany(() => Tag, tag => tag.courses)
  @JoinTable({ name: 'course_tags' })
  tags: Tag[];

  @Field(type => [CourseToSatellite], { nullable: true })
  @ApiProperty({ type: () => [CourseToSatellite] })
  @OneToMany(() => CourseToSatellite, CourseToSatellite => CourseToSatellite.masterCourse, { nullable: true })
  @JoinTable({ name: 'course_to_satellite' })
  toSatelliteCourses?: CourseToSatellite[];

  @Field(type => [CourseToSatellite], { nullable: true })
  @ApiProperty({ type: () => [CourseToSatellite] })
  @OneToMany(() => CourseToSatellite, CourseToSatellite => CourseToSatellite.satelliteCourse, { nullable: true })
  @JoinTable({ name: 'course_to_satellite' })
  toMasterCourses?: CourseToSatellite[];

  @Field(type => File, { nullable: true })
  @ApiProperty({ type: () => [File] })
  @ManyToOne(() => File, { nullable: true })
  image?: File;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course type' })
  @Column({ type: 'enum', enum: CourseType, nullable: true })
  type?: CourseType;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course duration in months' })
  @Column({ nullable: true })
  duration?: number;

  @Field({ defaultValue: 1 })
  @ApiProperty({ description: 'Course version' })
  @Column({ default: 1 })
  version?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course accent color in hex format' })
  @Column({ nullable: true })
  color?: string;

  counters?: CourseCounters;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 64, default: 'default' })
  certTemplate?: string;

  @Field()
  @ApiProperty()
  @Column({ default: '{}' })
  certTemplateData?: string;

  @Field()
  @ApiProperty({ description: 'Numeration of course modules from 0' })
  @Column({ type: 'boolean', default: false })
  numberModulesFromZero: boolean;

  @Field()
  @ApiProperty({ description: 'Auto numbering of course lectures' })
  @Column({ type: 'boolean', default: false })
  autoNumbering: boolean;

  @Field()
  @ApiProperty({ description: 'Demand level for support' })
  @Column({ type: 'int', default: 0 })
  supportDemand: number;

  @Field(() => User, { nullable: true })
  @ApiProperty({
    description: 'Video producer of the course',
    type: () => User,
  })
  @ManyToOne(() => User, { nullable: true })
  videoProducer?: User;

  @Field()
  @ApiProperty()
  @Column('varchar', { length: 32, default: 'created' })
  @Index()
  status: CourseStatus;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  startAt?: Date;

  @Field()
  @ApiProperty({ description: 'Auto enroll course like present' })
  @Column({ type: 'boolean', default: false })
  isAutoEnroll: boolean;

  @Field()
  @ApiProperty({ description: 'The course is not for sale' })
  @Column({ type: 'boolean', default: false })
  notForSale: boolean;

  @Field()
  @ApiProperty({ description: 'Disables forum for course' })
  @Column({ type: 'boolean', default: true })
  isForumEnabled: boolean;

  @Field()
  @ApiProperty({ description: 'Is satellited to other course' })
  @Column({ type: 'boolean', default: false })
  @Index()
  isSatellite: boolean;

  @Field(type => [Faq])
  @ApiProperty({ type: () => [Faq] })
  @ManyToMany(() => Faq)
  faqs: Faq[];

  @Field({ nullable: true })
  @ApiProperty({ description: 'Modules expected' })
  @Column({ nullable: true })
  expModules?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Lectures expected' })
  @Column({ nullable: true })
  expLectures?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Assignments expected' })
  @Column({ nullable: true })
  expAssignments?: true;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Quizzes expected' })
  @Column({ nullable: true })
  expQuizzes?: number;

  // @Field(type => [Banner])
  // @ApiProperty({ type: () => [Banner] })
  // @ManyToMany(() => Banner, banner => banner.courses)
  // @JoinTable({ name: 'banner_courses' })
  // banners: Banner[];
}

export class ExtendedCourse extends Course {
  @ApiProperty({ description: 'Trial available flag' })
  trialAvailable?: boolean;
}

@ObjectType()
@Entity()
export class CourseToSatellite {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'ID of master course' })
  @Column()
  @Index()
  masterCourseId: string;

  @Field()
  @ApiProperty({ description: 'ID of satellite course' })
  @Column()
  @Index()
  satelliteCourseId: string;

  @Field()
  @ApiProperty({ description: 'Master course' })
  @ManyToOne(() => Course, course => course.toSatelliteCourses)
  masterCourse: Course;

  @Field()
  @ApiProperty({ description: 'Satellite course' })
  @ManyToOne(() => Course, course => course.toMasterCourses)
  satelliteCourse: Course;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Satellite course title' })
  @Column({ nullable: true })
  satelliteCourseTitle: string;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  required: boolean;
}
