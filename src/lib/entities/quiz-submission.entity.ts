import { QuizAnswer } from './quiz-answer.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToOne, OneToMany } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Course } from './course.entity';
import { Lecture } from './lecture.entity';
import { Module } from './module.entity';
import { BaseSubmission } from './base-submission.entity';
import { Quiz } from './quiz.entity';

export interface ImportData {
  page?: number;
  index?: number;
}

@ObjectType()
@Entity()
export class QuizSubmission extends BaseSubmission {
  @Field({ nullable: true })
  @ApiProperty()
  @Column('int', { nullable: true })
  score?: number;

  @ApiProperty()
  @Column('int', { nullable: false, default: 0 })
  correctAnswerCount?: number;

  @ApiProperty()
  @Column('int', { nullable: false, default: 0 })
  incorrectAnswerCount?: number;

  @Field(type => [QuizAnswer])
  @ApiProperty()
  @OneToMany(() => QuizAnswer, answer => answer.submission, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  answers?: QuizAnswer[];

  @Field(type => Quiz)
  @Index()
  @ApiProperty({ type: () => Quiz })
  @ManyToOne(() => Quiz, quiz => quiz.questions)
  quiz: Quiz;

  @Field(() => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, { nullable: true })
  @Index()
  course?: Course;

  @Field(() => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture, { nullable: true })
  @Index()
  lecture?: Lecture;

  @Field(() => Module)
  @ApiProperty()
  @ManyToOne(() => Module, { nullable: true })
  @Index()
  module?: Module;
}
