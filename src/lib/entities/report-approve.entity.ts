import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { ReportType } from '@lib/api/reports';
import { User } from './user.entity';

@Entity()
export class ReportApprove {
  @ApiProperty({ description: 'Period of approved report' })
  @PrimaryColumn()
  period: string;

  @ApiProperty({
    description: 'Type of approved report',
    enum: ReportType,
  })
  @PrimaryColumn('varchar', { length: 15 })
  reportType: ReportType;

  @ApiProperty({ description: 'Id of approver' })
  @Column()
  @Index()
  approverId: string;

  @ApiProperty({ description: 'User who approved the report' })
  @ManyToOne(() => User)
  @JoinColumn({ name: 'approver_id' })
  approver: User;
}
