import { SingleChoiceAnswerI, MultiChoiceAnswerI, TrueFalseAnswerI, RateAnswerI, AllowedFileTypes } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, DeleteDateColumn, Entity, Index, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { Assessment } from './assessment.entity';
import { File } from './file.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { BaseQuestion } from './base-question.entity';

@ObjectType()
@Entity()
export class Question extends BaseQuestion {
  @Field(type => Assessment)
  @Index()
  @ApiProperty({ type: () => Assessment })
  @ManyToOne(() => Assessment, assessment => assessment.questions)
  assessment: Assessment;

  @Column({ nullable: true })
  assessmentId?: string;

  // Homeworks, Quiz
  @Field()
  @ApiProperty()
  @Column({ default: true })
  gradable?: boolean;

  // Homeworks
  @Field()
  @ApiProperty({ default: 100 })
  @Column('int', { default: 100 })
  score?: number;

  // Homeworks
  @Field()
  @ApiProperty({ default: 70 })
  @Column('int', { default: 70 })
  passingScore?: number;

  // Homeworks
  @Field(type => [File])
  @ApiProperty({
    type: () => [File],
    description: 'Attached files',
    default: [],
  })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  // Survey, Quiz
  @ApiProperty({ description: 'config answers' })
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data?: {
    answers: SingleChoiceAnswerI[] | MultiChoiceAnswerI[] | TrueFalseAnswerI[] | RateAnswerI[];
  };

  // Homeworks, Survey
  @ApiProperty({ description: 'Allow Multiple Files' })
  @Column('boolean', { default: true })
  allowMultipleFiles: boolean;

  // Homeworks, Survey
  @ApiProperty({ description: 'Allowed File Types' })
  @Column('varchar', { length: 20, default: AllowedFileTypes.Images })
  allowedFileTypes: AllowedFileTypes;

  // Homeworks, Survey
  @ApiProperty({ description: 'Max Files' })
  @Column('int', { default: 10 })
  maxFiles: number;

  // Quiz
  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackCommon?: string;

  // Quiz
  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackCorrect?: string;

  // Quiz
  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackIncorrect?: string;

  // Homeworks
  @Field(() => [AssessmentCriterion])
  @ApiProperty({ type: () => [AssessmentCriterion] })
  @OneToMany(() => AssessmentCriterion, entry => entry.question)
  assessmentCriteria?: AssessmentCriterion[];
}
