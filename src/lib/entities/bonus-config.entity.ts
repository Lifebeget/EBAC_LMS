import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class BonusConfig {
  @PrimaryColumn({ type: 'timestamptz' })
  @ApiProperty({ description: 'Bonus config activation date' })
  activeFrom: Date;

  @PrimaryColumn({ type: 'timestamptz' })
  @ApiProperty({ description: 'Bonus config deactivation date' })
  activeTo: Date;

  @Column('int', { array: true, nullable: false })
  @ApiProperty({
    description: 'Array of week checkpoints (0 - sunday, ... 6 - saturday)',
  })
  checkpoints: number[];

  @ApiProperty({ description: 'Percent of payment total for bonus' })
  @Column()
  bonusPercent: number;
}
