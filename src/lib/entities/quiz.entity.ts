import { AssessmentType, ShowCorrectAnswers, ShowResult } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToOne, OneToMany } from 'typeorm';
import { Lecture } from './lecture.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { BaseAssessment } from './base-assessment.entity';
import { QuizQuestion } from './quiz-question.entity';
import { QuizSubmission } from './quiz-submission.entity';

@ObjectType()
@Entity()
export class Quiz extends BaseAssessment {
  @Field()
  @ApiProperty({ default: 100 })
  @Column('int', { default: 100 })
  score?: number;

  @Field()
  @ApiProperty({ default: 70 })
  @Column('int', { default: 70 })
  @Index()
  passingScore?: number;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'Time limit for 1 try, 0 - unlimited',
  })
  // лимит времени на 1 попытку(0 - бесконечно)
  @Column('int', { default: 0 })
  timeLimitSeconds: number;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'Estimated time for survey (minutes)',
  })
  // примерное времени на выполнение(для опросов)
  @Column('int', { default: 0 })
  timeSeconds: number;

  // Task
  @Field()
  @ApiProperty({
    default: 3,
    description: 'Limit number of retries for tasks',
  })
  @Column('int', { default: 3 })
  maxAttempts: number;

  @Field()
  @ApiProperty({
    default: 24,
    description: 'Pause between quiz retries (hours)',
  })
  // пауза до следующей пересдачи(24 часа)
  @Column('int', { default: 24 })
  timeDelaytoRetestHours: number;

  @Field()
  @ApiProperty({
    default: ShowResult.onFinish,
    description: 'When to show quiz results',
  })
  // показывать результат
  @Column('int', { default: ShowResult.onFinish })
  showResult: ShowResult;

  @Field()
  @ApiProperty({
    default: ShowCorrectAnswers.showOnlyIncorrect,
    description: 'Different modes for displaying correct answers',
  })
  // показывать ответы
  @Column('int', { default: ShowCorrectAnswers.showOnlyIncorrect })
  showCorrectAnswers: ShowCorrectAnswers;

  @Field()
  @ApiProperty({
    default: true,
    description: 'Can user change answers',
  })
  // можно ли изменить ответ(до завершения)
  @Column('boolean', { default: true })
  canChangeAnswer: boolean;

  @Field()
  @ApiProperty({ default: true, description: 'Shuffle order of question' })
  // перемешивать вопросы
  @Column('boolean', { default: true })
  shuffleQuestions: boolean;

  @Field()
  @ApiProperty({ default: true, description: 'Shuffle order of answers' })
  // перемешивать ответы
  @Column('boolean', { default: true })
  shuffleAnswers: boolean;

  // Временно оставить везде, потом выпилить
  @Field()
  @ApiProperty({
    enum: AssessmentType,
    default: AssessmentType.Default,
  })
  @Column({ default: AssessmentType.Default })
  @Index()
  type: AssessmentType;

  questionCount?: number;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'Limit number of retries for quiz. 0 for unlimited',
  })
  // лимит кол-ва попыток(2 - можно сдать 2 раза, 0 - бесконечно)
  @Column('int', { default: 0 })
  countLimit: number;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture)
  lecture: Lecture;

  @Field(type => [QuizQuestion])
  @ApiProperty({ type: () => [QuizQuestion] })
  @OneToMany(() => QuizQuestion, question => question.quiz)
  questions: QuizQuestion[];

  @Field(type => [QuizSubmission])
  @ApiProperty({ type: () => [QuizSubmission] })
  @OneToMany(() => QuizSubmission, submission => submission.quiz)
  submissions: QuizSubmission[];
}
