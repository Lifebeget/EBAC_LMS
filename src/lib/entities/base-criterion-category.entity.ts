import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Column, DeleteDateColumn, Index } from 'typeorm';
import { BaseEntity } from '@entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { CriterionSelector } from '@lib/types/enums/criterion-selector.enum';

@ObjectType()
export abstract class BaseCriterionCategory extends BaseEntity {
  @Field()
  @ApiProperty({ description: 'Criterion category title' })
  @Column('varchar', { length: 128, unique: true })
  title: string;

  @Field(() => String, { nullable: true })
  @ApiProperty({ description: 'Criterion category description' })
  @Column('text', { nullable: true })
  description?: string;

  @Field()
  @ApiProperty({ description: 'Criterion category default weight' })
  @Column('int', { default: 1 })
  defaultWeight: number;

  @Field()
  @ApiProperty({
    enum: CriterionSelector,
    description: 'Element type',
  })
  @Column({
    type: 'enum',
    enum: CriterionSelector,
    default: CriterionSelector.Checkbox,
  })
  display: CriterionSelector;

  @Field(() => Int)
  @ApiProperty({ description: 'Sort value' })
  @Column('int', { default: 500 })
  sort: number;

  @Field()
  @ApiProperty({
    description: 'Is it worth judging by the criteria of this category',
  })
  @Column('boolean', { default: true })
  gradable: boolean;

  @Field()
  @ApiProperty({ description: 'Deletion date' })
  @Index()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;
}
