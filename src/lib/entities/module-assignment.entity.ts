import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';
import { Module } from './module.entity';

@ObjectType()
@Entity()
export class ModuleAssignment {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Field(type => [ModuleAssignment])
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  user?: User;

  @Field()
  @Column()
  userId?: string;

  @Field(type => [ModuleAssignment])
  @ApiProperty({ type: () => Module })
  @ManyToOne(() => Module, { onDelete: 'CASCADE' })
  module?: Module;

  @Field()
  @Column()
  moduleId: string;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  exclusive?: boolean;

  courseId?: string;
}
