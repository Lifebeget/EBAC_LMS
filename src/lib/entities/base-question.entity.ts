import { BaseEntity } from '@entities/base.entity';
import { QuestionType, TextMode } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Index } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export abstract class BaseQuestion extends BaseEntity {
  @Field()
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort?: number;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field()
  @ApiProperty()
  @Column('text')
  description?: string;

  @Field()
  @ApiProperty({ enum: QuestionType, default: QuestionType.Upload })
  @Column('varchar', { length: 80 })
  type: QuestionType;

  // All (maybe only surveys)
  @Field()
  @ApiProperty({ enum: TextMode, default: TextMode.OneLine })
  @Column('varchar', { length: 80, nullable: true })
  textMode?: TextMode;

  // All (maybe only surveys)
  @Field()
  @ApiProperty({ default: 0 })
  @Column('int', { default: 0 })
  textModeMin?: number;

  // All (maybe only surveys)
  @Field()
  @ApiProperty({ default: 0 })
  @Column('int', { default: 0 })
  textModeMax?: number;

  @ApiProperty({ description: 'Have to filled' })
  @Column('boolean', { default: true })
  isRequired: boolean;
}
