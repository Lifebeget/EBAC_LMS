import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { Assessment } from './assessment.entity';
import { Content } from './content.entity';
import { Course } from './course.entity';
import { File } from './file.entity';
import { LectureProgress } from './lecture-progress.entity';
import { Module } from './module.entity';

@ObjectType()
@Entity()
@Unique('LECTURE_SLUG_WITHIN_COURSE', ['course', 'slug'])
export class Lecture {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  active?: boolean;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field()
  @ApiProperty({ description: 'URL part. Must be unique within course' })
  @Column()
  slug: string;

  @Field()
  @ApiProperty()
  @Column('text')
  description?: string;

  @Field()
  @ApiProperty({
    default: false,
    description: 'Is this lesson available without enrollment',
  })
  @Column({ default: false })
  @Index()
  free?: boolean;

  @Field(() => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, course => course.lectures)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Field()
  @Column()
  @Index()
  courseId: string;

  @Field(() => Module)
  @ApiProperty({ type: () => Module })
  @ManyToOne(() => Module, module => module.lectures)
  @JoinColumn({ name: 'module_id' })
  module: Module;

  @Field()
  @Column()
  @Index()
  moduleId: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  @Index()
  published?: boolean;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  publishedAt?: Date;

  @Field(() => [Assessment])
  @OneToMany(() => Assessment, assessment => assessment.lecture)
  assessments: Assessment[];

  @Field(() => File, { nullable: true })
  @ApiProperty({ type: () => File })
  @ManyToOne(() => File, { nullable: true })
  image?: File;

  @Field(() => [LectureProgress])
  @OneToMany(() => LectureProgress, it => it.lecture)
  lectureProgress: LectureProgress[];

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true })
  duration?: number;

  @Field(() => [Content])
  @OneToMany(() => Content, it => it.lecture)
  contents: Content[];

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  required?: boolean;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Lecture number' })
  @Column({ nullable: true })
  number: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Lecture custom number' })
  @Column({ nullable: true })
  customNumber: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Lecture analogs group id' })
  @Column({ nullable: true })
  analogId: string;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  @Index()
  deleted?: Date;
}
