import { StoredBonus as StoredBonusInterface } from '@lib/api/reports';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class StoredBonus implements StoredBonusInterface {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @ApiProperty({ description: 'Course title' })
  @Column()
  courseTitle: string;

  @ApiProperty({ description: 'Id of course' })
  @Column()
  @Index()
  courseId: string;

  @ApiProperty({ description: 'Outdated count' })
  @Column({ type: 'int' })
  failsCount: number;

  @ApiProperty({ description: 'Checkpoint date' })
  @Column({ type: 'date' })
  date: string;

  @ApiProperty({ description: 'Period of report' })
  @Column()
  @Index()
  period: string;

  @Column({ type: 'jsonb', default: [] })
  @ApiProperty({ description: 'Ids of submissions' })
  sources?: string[];
}
