import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import GraphQLJSON from 'graphql-type-json';
import { CreateSubscriptionDto } from 'src/subscriptions/dto/create-subscription.dto';
import { Column, CreateDateColumn, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Client } from './client.entity';
import { Source } from './source.entity';
import { User } from './user.entity';
import { Webinar } from './webinars.entity';

@ObjectType()
@Entity()
export class Subscription {
  @Field()
  @ApiProperty({ description: 'id' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column('integer', { nullable: true })
  @Index()
  externalId: number;

  @ApiProperty({ description: 'Client', type: () => Client })
  @ManyToOne(() => Client)
  @JoinColumn({ name: 'client_id' })
  client?: Client;

  @Column({ nullable: true })
  clientId?: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user?: User;

  @Column('uuid', { nullable: true })
  userId?: string;

  @ApiProperty({ description: 'Source', type: () => Source })
  @ManyToOne(() => Source)
  @JoinColumn({ name: 'source_id' })
  source?: Source;

  @Column({ nullable: true })
  sourceId?: string;

  @Field(() => Webinar)
  @ApiProperty({ type: () => Webinar })
  @ManyToOne(() => Webinar)
  @JoinColumn({ name: 'webinar_id' })
  webinar: Webinar;

  @ApiProperty()
  @Column({ nullable: true })
  webinarId?: string;

  @Field()
  @ApiProperty()
  @Column({ nullable: true })
  reference: string;

  @Field(() => GraphQLJSON, { nullable: true })
  @ApiProperty({ description: 'options' })
  @Column('jsonb', { nullable: true })
  options: CreateSubscriptionDto;

  @Field()
  @ApiProperty({ description: 'created at' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'modified at' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Column({ nullable: true })
  @ApiProperty()
  country?: string;
}
