import { Entity, Column, ManyToOne, ManyToMany, Index, OneToMany, JoinTable } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Submission } from './submission.entity';
import { Question } from './question.entity';
import { File } from './file.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { AnswerDataI } from '@lib/api/types';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';
import { BaseAnswer } from './base-answer.entity';

@ObjectType()
@Entity()
export class Answer extends BaseAnswer {
  @Field(type => Submission)
  @Index()
  @ApiProperty({ type: () => Submission })
  @ManyToOne(() => Submission, submission => submission.answers, {
    onDelete: 'CASCADE',
  })
  submission: Submission;

  @Field(type => Question)
  @ApiProperty({ type: () => Question })
  @ManyToOne(() => Question)
  question: Question;

  @Field({ nullable: true })
  @Column({ nullable: true })
  questionId?: string;

  // Homeworks
  @Field()
  @ApiProperty({ default: 0 })
  @Column('int', { default: 0 })
  score?: number;

  // Homeworks
  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  tutorComment: string;

  // Homeworks, Surveys
  @Field(type => [File])
  @ApiProperty({ type: () => [File] })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  // Homeworks
  @Field(type => [File])
  @ApiProperty({ type: () => [File] })
  @ManyToMany(() => File)
  @JoinTable()
  tutorFiles?: File[];

  // Homeworks
  @Field(() => [AssessmentCriterionEntry])
  @ApiProperty({ type: () => [AssessmentCriterionEntry] })
  @OneToMany(() => AssessmentCriterionEntry, entry => entry.answer)
  assessmentCriteriaEntries?: AssessmentCriterionEntry[];

  // Survey, Quiz
  @ApiProperty()
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data: AnswerDataI | AnswerDataI[];
}
