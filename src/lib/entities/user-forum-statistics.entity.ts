import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ForumStatistic } from './forum-statistics.entity';

@ObjectType()
@Entity({ name: 'user_forum_stat' })
export class UserForumStatistic extends ForumStatistic {
  @Field()
  @ApiProperty({ description: 'User UUID' })
  @PrimaryColumn()
  userId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'User name' })
  @Column({ nullable: true })
  userName: string;
}
