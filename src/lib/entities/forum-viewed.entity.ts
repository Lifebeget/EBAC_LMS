import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, ManyToOne, UpdateDateColumn } from 'typeorm';
import { ForumMessages } from './forum-messages.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class ForumViewed {
  @Field(type => ForumMessages)
  @ApiProperty({
    type: () => ForumMessages,
    description: 'Message',
  })
  @ManyToOne(() => ForumMessages, { primary: true, onDelete: 'CASCADE' })
  message: ForumMessages;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User, { primary: true })
  user: User;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
