import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Index, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class Token {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => User)
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User)
  user: User;

  @Field()
  @ApiProperty({ description: 'Token string' })
  @Column({ unique: true })
  @Index()
  token: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  used?: Date;
}
