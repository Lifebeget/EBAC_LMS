import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Nomenclature } from './nomenclature.entity';
import { Topic } from './topic.entity';

@ObjectType()
@Entity()
export class TopicNomenclatureLink {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Topic)
  @JoinColumn({ name: 'topic_id' })
  topic: Topic;

  @Column()
  topicId: string;

  @ManyToOne(() => Nomenclature)
  @JoinColumn({ name: 'nomenclature_id' })
  nomenclature: Nomenclature;

  @Column()
  nomenclatureId: string;
}
