import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { EvaluationFeature } from '../enums/evaluation-feature.enum';
import { Evaluation } from '../enums/evaluation.enum';
import { Lecture } from './lecture.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class LectureEvaluation {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => User)
  @ApiProperty({ type: User, description: 'User who apply evaluations' })
  @ManyToOne(() => User, { nullable: false })
  user: User;

  @Field(type => Lecture)
  @ApiProperty({ type: Lecture, description: 'Evaluated lecture' })
  @ManyToOne(() => Lecture)
  lecture: Lecture;

  @Field()
  @ApiProperty({ description: 'Lecture evaluation', type: () => Evaluation })
  @Column('int')
  evaluation: Evaluation;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Why this lecture is good' })
  @Column('varchar', { length: 16, nullable: true })
  likedFeature?: EvaluationFeature;

  @Field({ nullable: true })
  @ApiProperty({ description: 'What can be better in lecture' })
  @Column('text', { nullable: true })
  userWishes?: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;
}
