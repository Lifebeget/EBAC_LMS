import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ImportState } from '../enums/import-state.enum';
import { ImportType } from '../enums/import-type.enum';
import { ImportCounters } from '../types/ImportCounters';

@ObjectType()
@Entity()
export class ImportInfo {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Field()
  @ApiProperty({ enum: ImportType })
  @Column('varchar', { length: 16 })
  importType: ImportType;

  @Field()
  @ApiProperty({ enum: ImportState })
  @Column('varchar', { length: 16 })
  state: ImportState;

  @Field()
  @ApiProperty({ description: 'Import start time' })
  @Column({ type: 'timestamptz' })
  startedAt: Date;

  @Field()
  @ApiProperty({ description: 'Import finish time' })
  @Column({ type: 'timestamptz', nullable: true })
  finishedAt?: Date;

  @Field()
  @ApiProperty({ description: 'Expiration time' })
  @Column({ type: 'timestamptz', nullable: true })
  expiredAt?: Date;

  @ApiProperty()
  @Column('jsonb', { nullable: true })
  counters?: ImportCounters;
}
