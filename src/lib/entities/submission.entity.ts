import { ApiProperty } from '@nestjs/swagger';
import { Column, DeleteDateColumn, Entity, Index, JoinColumn, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { Answer } from './answer.entity';
import { Assessment } from './assessment.entity';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { Course } from './course.entity';
import { Lecture } from './lecture.entity';
import { Module } from './module.entity';
import { Webinar } from './webinars.entity';
import { SubmissionTariff } from './submission-tariff.entity';
import { BaseSubmission } from './base-submission.entity';

export interface ImportData {
  page?: number;
  index?: number;
}

@ObjectType()
@Entity()
export class Submission extends BaseSubmission {
  @Field(type => Assessment)
  @ApiProperty({ type: () => Assessment })
  @ManyToOne(() => Assessment, assessment => assessment.questions)
  @Index()
  assessment: Assessment;

  // Homeworks, unused выпилить
  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  due?: Date;

  // Выпилить
  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  comment?: string;

  // Homeworks, Quiz
  @Field({ nullable: true })
  @ApiProperty()
  @Column('int', { nullable: true })
  score?: number;

  // All
  @Field(type => [Answer])
  @ApiProperty()
  @OneToMany(() => Answer, answer => answer.submission, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  answers?: Answer[];

  // Homewroks
  @ApiProperty({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  mutex?: Date;

  // Homeworks
  @Field()
  @ApiProperty()
  @Column({ default: false })
  isMutexAuto?: boolean;

  // Homeworks
  @Field(type => User, { nullable: true })
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { nullable: true })
  @Index()
  tutor?: User;

  // Homeworks, выпилить
  @ApiProperty({ nullable: true })
  @Column('jsonb', { nullable: true })
  importData?: ImportData;

  // Homeworks
  @Field()
  @ApiProperty()
  @Column({ default: false })
  bindTutor?: boolean;

  // Homeworks, возможно выпилить, выпилить из tutor-frontend
  @Field()
  @ApiProperty({ description: 'Link to EadBox assignment for this submission' })
  link?: string;

  // Quiz
  @ApiProperty()
  @Column('int', { nullable: false, default: 0 })
  correctAnswerCount?: number;

  // Quiz
  @ApiProperty()
  @Column('int', { nullable: false, default: 0 })
  incorrectAnswerCount?: number;

  // Homeworks
  @Field()
  @ApiProperty({ description: 'Is this submission is just a question' })
  @Column({ nullable: false, default: false })
  @Index()
  isQuestion?: boolean;

  // Homeworks
  @Field()
  @ApiProperty({ description: 'Is this submission is urgent' })
  isUrgent?: boolean;

  // Homeworks
  @Field()
  @ApiProperty({ description: 'Is this submission is outdated' })
  isOutdated?: boolean;

  // Homeworks
  @Field()
  @ApiProperty({ description: 'Is this submission is graded today' })
  isGradedToday?: boolean;

  // Homeworks
  @Field()
  @ApiProperty({ description: 'Is this submission is locked' })
  isLocked?: boolean;

  // All exclude webinar surveys
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, { nullable: true })
  @Index()
  course?: Course;

  // All exclude webinar surveys
  @Field(() => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture, { nullable: true })
  @JoinColumn({ name: 'lecture_id' })
  lecture?: Lecture;

  // All exclude webinar surveys
  @Field()
  @Column({ nullable: true })
  @Index()
  lectureId: string;

  // All exclude webinar surveys
  @Field()
  @ApiProperty()
  @ManyToOne(() => Module, { nullable: true })
  @JoinColumn({ name: 'module_id' })
  module?: Module;

  // All exclude webinar surveys
  @Field()
  @Column({ nullable: true })
  @Index()
  moduleId: string;

  // Only webinar surveys
  @Field(type => Webinar)
  @ApiProperty({ type: () => Webinar })
  @ManyToOne(() => Webinar)
  @JoinColumn({ name: 'webinar_id' })
  webinar: Webinar;

  // Only webinar surveys
  @Field()
  @Column({ nullable: true })
  @Index()
  webinarId: string;

  // Homeworks
  @Field(() => SubmissionTariff)
  @OneToOne(() => SubmissionTariff, tariff => tariff.submission, {
    cascade: true,
  })
  tariff: SubmissionTariff;

  // All
  @Field()
  @ApiProperty()
  @Column({ nullable: true, default: null })
  success?: boolean;
}
