import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';
import { NotificationParam } from '@lib/api/system-notification/types';
import { ReadStatus } from './read-status.entity';
import { SystemNotificationButtonType } from '@enums/system-notification-button-type.enum';
import { SystemNotificationCondition } from '../types/SystemNotificationCondition';
import { SystemNotificationType } from '@enums/system-notification-type.enum';
import { SystemRole } from '@lib/types/enums';
import { User } from './user.entity';

// type Param = { title: string; value: string; type: 'period' | 'string' };

@Entity()
export class SystemNotification {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    type: () => User,
    description: 'System notification author',
  })
  @ManyToOne(() => User)
  @JoinColumn({ name: 'author_id' })
  author: User;

  @Column()
  authorId: string;

  @ApiProperty({ description: 'System notification title' })
  @Column()
  title: string;

  @ApiProperty({ description: 'System notification description' })
  @Column('text', { nullable: true })
  description?: string;

  @ApiProperty({ description: 'Is system notification for tutor interface' })
  @Column({ nullable: false, default: false })
  forTutor: boolean;

  @ApiProperty({ description: 'Is system notification for student interface' })
  @Column({ nullable: false, default: false })
  forStudent: boolean;

  @ApiProperty({ description: 'Is system notification for admin interface' })
  @Column({ nullable: false, default: false })
  forAdmin: boolean;

  @ApiProperty({
    description: 'If set, notification must be read in its entirety before it can be closed',
  })
  @Column({ nullable: false, default: false })
  mustRead: boolean;

  @ApiProperty({
    description: "Don't mark as read until the flag is lifted or notification is deleted",
  })
  @Column({ nullable: false, default: false })
  sticky: boolean;

  @ApiProperty({
    enum: SystemNotificationType,
    description: 'Type of system notification',
  })
  @Column('varchar', { length: 32 })
  type: SystemNotificationType;

  @ApiProperty({
    enum: SystemNotificationButtonType,
    description: 'Type of system notification button',
  })
  @Column('varchar', { length: 32, nullable: true })
  buttonType?: SystemNotificationButtonType;

  @ApiProperty({ description: 'System notification view conditions' })
  @Column('jsonb', { nullable: true })
  conditions?: SystemNotificationCondition;

  @ApiProperty({ description: 'System notification lifetime in hours' })
  @Column({ nullable: false, default: 720 })
  lifetimeHours: number;

  @ApiProperty({ description: 'Route to follow after notification click' })
  @Column({ nullable: true })
  to?: string;

  @ApiProperty({ description: 'Show notification only for this role', enum: SystemRole })
  @Column({ nullable: true })
  forRole?: SystemRole;

  @ApiProperty({ description: 'Need to translate message', enum: SystemRole })
  @Column({ nullable: true })
  translate?: boolean;

  @Column({ nullable: true, type: 'jsonb' })
  params?: NotificationParam[];

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @ApiProperty({ description: 'System notification has been deleted' })
  @Column({ nullable: false, default: false })
  deleted: boolean;

  // need only for to get one
  @OneToMany(() => ReadStatus, readStatus => readStatus.systemNotification)
  readStatuses?: ReadStatus[];

  readStatus?: ReadStatus;

  read?: number;
  open?: number;
}
