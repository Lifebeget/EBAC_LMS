import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Nomenclature } from '@entities/nomenclature.entity';
import { SourceActionLms } from '@enums/source-action-lms';
import { SourcePageTypeLms } from '@enums/source-page-type-lms';

@ObjectType()
@Entity()
export class Source {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'externalId' })
  @Column({ unique: true })
  externalId: number;

  @Field()
  @ApiProperty({ description: 'Form name' })
  @Column('varchar')
  name: string;

  @Field()
  @ApiProperty({ description: 'Page type' })
  @Column({
    type: 'enum',
    enum: SourcePageTypeLms,
  })
  pageType: SourcePageTypeLms;

  @Field()
  @ApiProperty({ description: 'Form type' })
  @Column()
  formType: string;

  @Field()
  @ApiProperty({ description: 'Form action' })
  @Column({
    type: 'enum',
    enum: SourceActionLms,
  })
  action: SourceActionLms;

  @Column({ nullable: true })
  nomenclatureId?: number;
  @ManyToOne(() => Nomenclature, (webinar: Nomenclature) => webinar.externalId, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'nomenclature_id', referencedColumnName: 'externalId' })
  nomenclature?: Nomenclature;

  @Field()
  @ApiProperty({ description: 'Created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @ApiProperty({ description: 'Modified' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified: Date;

  @Column({ nullable: true })
  esWelcomeMessageId: number;

  @Column({ nullable: true })
  esListId: number;
}
