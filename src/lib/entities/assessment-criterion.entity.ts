import { BaseCriterion } from './base-criterion.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { Question } from '@entities/question.entity';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';

@ObjectType()
@Entity()
export class AssessmentCriterion extends BaseCriterion {
  @Field(() => AssessmentCriterionCategory)
  @ApiProperty({
    type: () => AssessmentCriterionCategory,
    description: 'Assessment criterion category',
  })
  @JoinColumn({ name: 'category_id' })
  @ManyToOne(() => AssessmentCriterionCategory, category => category.criteria)
  category: AssessmentCriterionCategory;

  @Field()
  @ApiProperty({ description: 'Question ID' })
  @Index()
  @Column()
  questionId: string;

  @Field(() => Question)
  @ApiProperty({ description: 'Question', type: () => Question })
  @ManyToOne(() => Question, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'question_id' })
  question: Question;

  @Field(() => [AssessmentCriterionEntry], {
    nullable: true,
  })
  @ApiProperty({
    description: 'Assessment criterion entries',
    nullable: true,
    type: () => [AssessmentCriterionEntry],
  })
  @OneToMany(() => AssessmentCriterionEntry, entry => entry.criteria)
  assessmentCriteriaEntries?: AssessmentCriterionEntry[];
}
