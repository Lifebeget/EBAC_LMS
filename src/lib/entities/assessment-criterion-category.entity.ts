import { Field, ObjectType } from '@nestjs/graphql';
import { Entity, OneToMany } from 'typeorm';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { ApiProperty } from '@nestjs/swagger';
import { BaseCriterionCategory } from './base-criterion-category.entity';

@ObjectType()
@Entity()
export class AssessmentCriterionCategory extends BaseCriterionCategory {
  @Field(() => [AssessmentCriterion])
  @ApiProperty({
    type: () => [AssessmentCriterion],
    description: 'Assessment criteria',
  })
  @OneToMany(() => AssessmentCriterion, criterion => criterion.category)
  criteria: AssessmentCriterion[];
}
