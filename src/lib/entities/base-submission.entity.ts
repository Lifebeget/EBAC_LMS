import { BaseEntity } from '@entities/base.entity';
import { SubmissionStatus } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, DeleteDateColumn, Index, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { BaseAnswer } from './base-answer.entity';

@ObjectType()
export abstract class BaseSubmission extends BaseEntity {
  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field(type => User)
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User)
  @Index()
  user: User;

  @Field()
  @ApiProperty({ enum: SubmissionStatus })
  @Column('varchar', { length: 40, default: SubmissionStatus.New })
  @Index()
  status: SubmissionStatus;

  // TODO: это немного вредное поле, т.к. инкрементится оно в том числе и когда отправляется вопрос по Homework, то есть с фичей AttemptLimits оно не сочетается.
  @Field()
  @ApiProperty()
  @Column({ default: 0 })
  attempt?: number;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  current_attempt?: boolean;

  @Field()
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  draftAt?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  @Index()
  submittedAt?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  @Index()
  gradedAt?: Date;

  // All, посмотреть где используется
  @Field()
  @ApiProperty()
  userSubmissionsCount?: number;

  @Field()
  @ApiProperty()
  @Column({ nullable: true, default: null })
  success?: boolean;

  @Field()
  @ApiProperty({ description: 'Deletion date' })
  @Index()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;

  answers?: BaseAnswer[];
}
