import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { File } from 'src/lib/entities/file.entity';
import { Segment } from './segment.entity';
import { Course } from './course.entity';

@ObjectType()
@Entity({ name: 'banner' })
export class Banner {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Title' })
  @Column({ nullable: false })
  title: string;

  @Field()
  @ApiProperty({ description: 'Placement location' })
  @Column({ nullable: false })
  place: string;

  @Field()
  @ApiProperty({ description: 'Slug' })
  @Column({ nullable: false, unique: true })
  slug: string;

  @Field()
  @ApiProperty({ description: 'Google tag manager label' })
  @Column({ nullable: false })
  labelGTM: string;

  @Field()
  @ApiProperty({ description: 'Label for save closed action in frontend local storage' })
  @Column({ nullable: false })
  labelVisibility: string;

  @Field(type => File)
  @ApiProperty({ type: () => [File], description: 'Desktop image' })
  @ManyToOne(() => File)
  desktopImageFile?: File;

  @Field()
  @ApiProperty({ description: 'Desktop image url' })
  @Column({ nullable: false })
  desktopImageUrl: string;

  @Field(type => File)
  @ApiProperty({ type: () => [File], description: 'Mobile image' })
  @ManyToOne(() => File)
  mobileImageFile?: File;

  @Field()
  @ApiProperty({ description: 'Mobile image url' })
  @Column({ nullable: false })
  mobileImageUrl: string;

  @Field()
  @ApiProperty({ description: 'Active from' })
  @Column({ nullable: false, type: 'timestamptz' })
  activeFrom: Date;

  @Field()
  @ApiProperty({ description: 'Active to' })
  @Column({ nullable: false, type: 'timestamptz' })
  activeTo: Date;

  @Field()
  @ApiProperty({ description: 'Link' })
  @Column({ nullable: false })
  href: string;

  @Field()
  @ApiProperty({ description: 'Display flag' })
  @Column('boolean', { nullable: false })
  isActive: boolean;

  @Field()
  @ApiProperty({ description: 'Date of creation' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Date of change' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @ApiProperty({ type: Segment })
  @ManyToOne(() => Segment)
  segment?: Segment;

  @Field(type => [Course])
  @ApiProperty({ type: () => [Course] })
  @ManyToMany(() => Course)
  @JoinTable()
  courses?: Course[];
}