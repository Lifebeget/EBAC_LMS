import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, OneToMany } from 'typeorm';
import { BaseCriterionCategory } from './base-criterion-category.entity';
import { HomeworkCriterion } from './homework-criterion.entity';

@ObjectType()
@Entity()
export class HomeworkCriterionCategory extends BaseCriterionCategory {
  @Field(() => [HomeworkCriterion])
  @ApiProperty({
    type: () => [HomeworkCriterion],
    description: 'Homework criteria',
  })
  @OneToMany(() => HomeworkCriterion, criterion => criterion.category)
  criteria: HomeworkCriterion[];
}
