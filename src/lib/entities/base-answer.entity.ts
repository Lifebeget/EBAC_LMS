import { BaseEntity } from '@entities/base.entity';
import { Column, Index } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export abstract class BaseAnswer extends BaseEntity {
  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  comment?: string;
}
