import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { SubmissionStatistic } from './submission-statistics.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class TutorStatistic extends SubmissionStatistic {
  @Field()
  @ApiProperty({ description: 'Tutor UUID' })
  @PrimaryColumn()
  tutorId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Tutor name' })
  @Column({ nullable: true })
  tutorName: string;

  @Field(type => User)
  @ApiProperty({ description: 'Tutor' })
  tutor?: User;
}
