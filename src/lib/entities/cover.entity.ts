import { Field, ObjectType } from '@nestjs/graphql';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
@Entity()
export class Cover {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'SVG template id' })
  @Column()
  templateId: string;

  @Field()
  @ApiProperty({ description: 'Color palette id' })
  @Column()
  paletteId: string;

  @Field()
  @Column({ unique: true })
  @ApiProperty({ description: 'Lecture id' })
  lectureId: string;

  @Field()
  @Column()
  @ApiProperty({ description: 'Module id' })
  moduleId: string;

  @Field()
  @Column()
  @ApiProperty({ description: 'Course id' })
  courseId: string;

  @Field()
  @Column()
  @ApiProperty({ description: 'Cover module title' })
  moduleTitle: string;

  @Field()
  @Column()
  @ApiProperty({ description: 'Cover module number' })
  moduleNumber: number;

  @Field()
  @Column()
  @ApiProperty({ description: 'Cover lecture number' })
  lectureNumber: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  @ApiProperty({ description: 'Lecture content type' })
  contentType?: string;

  @Field()
  @ApiProperty({ description: 'Cover created date' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Cover updated date' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
