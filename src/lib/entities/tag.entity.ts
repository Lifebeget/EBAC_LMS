import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Course } from './course.entity';
import { TagCategory } from '@entities/tag-category.entity';

@ObjectType()
@Entity()
@Unique('index_title', ['title', 'category'])
export class Tag {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Tag title' })
  @Column('varchar', { length: 255 })
  title: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field(() => [Course])
  @ApiProperty()
  @ManyToMany(() => Course, course => course.tags)
  courses: Course[];

  @Field(() => TagCategory)
  @ApiProperty({ type: () => TagCategory, description: 'Category of the tag' })
  @ManyToOne(() => TagCategory)
  category: TagCategory;
}
