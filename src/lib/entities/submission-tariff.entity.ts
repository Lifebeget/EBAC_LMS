import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { ServiceLevelTariff } from './service-level-tariff.entity';
import { Submission } from './submission.entity';
@ObjectType()
@Entity()
export class SubmissionTariff {
  @Field(() => Submission)
  @ApiProperty({ type: () => Submission, description: 'Submission' })
  @OneToOne(() => Submission, submission => submission.tariff)
  @JoinColumn({ name: 'submission_id' })
  submission: Submission;

  @Field()
  @PrimaryColumn()
  submissionId: string;

  @Field(() => ServiceLevelTariff)
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'Tariff that was active at the time of creation',
  })
  @ManyToOne(() => ServiceLevelTariff)
  @JoinColumn({ name: 'create_tariff_id' })
  createTariff: ServiceLevelTariff;

  @Field()
  @ApiProperty({
    description: 'ID of tariff that was active at the time of creation',
  })
  @Column()
  @Index()
  createTariffId: string;

  @Field(() => ServiceLevelTariff)
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'Tariff that was active at the time of grading',
  })
  @ManyToOne(() => ServiceLevelTariff)
  @JoinColumn({ name: 'grade_tariff_id' })
  gradeTariff: ServiceLevelTariff;

  @Field()
  @ApiProperty({
    description: 'ID of tariff that was active at the time of grading',
  })
  @Column({ nullable: true })
  @Index()
  gradeTariffId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date after which submission becomes urgent' })
  @Column({ type: 'timestamptz', nullable: true })
  @Index()
  urgentDate: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date after which submission becomes outdated' })
  @Column({ type: 'timestamptz', nullable: true })
  @Index()
  outdatedDate: Date;

  @ApiProperty({
    description: 'Title of current service level',
    required: false,
  })
  levelTitle?: string;
}
