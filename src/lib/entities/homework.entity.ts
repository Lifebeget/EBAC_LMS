import { AssessmentGradingType } from '@lib/types/enums';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseAssessment } from './base-assessment.entity';
import { HomeworkQuestion } from './homework-question.entity';
import { HomeworkSubmission } from './homework-submission.entity';
import { Lecture } from './lecture.entity';
import { ServiceLevel } from './service-level.entity';

@ObjectType()
@Entity()
export class Homework extends BaseAssessment {
  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  tutorMessage?: string;

  @Field()
  @ApiProperty({ default: 100 })
  @Column('int', { default: 100 })
  score?: number;

  @Field()
  @ApiProperty({ default: 70 })
  @Column('int', { default: 70 })
  @Index()
  passingScore?: number;

  @Field()
  @ApiProperty({
    enum: AssessmentGradingType,
    default: AssessmentGradingType.OverallShouldPass,
  })
  @Column('varchar', {
    length: 80,
    default: AssessmentGradingType.OverallShouldPass,
  })
  gradingType: AssessmentGradingType;

  @Field(type => ServiceLevel)
  @ApiProperty({ type: () => ServiceLevel, description: 'Service level' })
  @ManyToOne(() => ServiceLevel)
  @JoinColumn({ name: 'level_id' })
  level: ServiceLevel;

  @Field({ nullable: true })
  @ApiProperty({ description: 'ID of service level' })
  @Column({ nullable: true })
  @Index()
  levelId: string;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture)
  lecture: Lecture;

  @Field(type => [HomeworkQuestion])
  @ApiProperty({ type: () => [HomeworkQuestion] })
  @OneToMany(() => HomeworkQuestion, homeworkQuestion => homeworkQuestion.homework)
  questions: HomeworkQuestion[];

  @Field(type => [HomeworkSubmission])
  @ApiProperty({ type: () => [HomeworkSubmission] })
  @OneToMany(() => HomeworkSubmission, homeworkSubmission => homeworkSubmission.homework)
  submissions: HomeworkSubmission[];
}
