import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, ManyToMany, JoinTable, OneToMany, ManyToOne, Index } from 'typeorm';
import { BaseAnswer } from './base-answer.entity';
import { HomeworkCriterionEntry } from './homework-criterion-entry.entity';
import { HomeworkQuestion } from './homework-question.entity';
import { HomeworkSubmission } from './homework-submission.entity';
import { File } from './file.entity';

@ObjectType()
@Entity()
export class HomeworkAnswer extends BaseAnswer {
  @Field()
  @ApiProperty({ default: 0 })
  @Column('int', { default: 0 })
  score?: number;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  tutorComment: string;

  @Field(type => [File])
  @ApiProperty({ type: () => [File] })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  @Field(type => [File])
  @ApiProperty({ type: () => [File] })
  @ManyToMany(() => File)
  @JoinTable()
  tutorFiles?: File[];

  @Field(() => [HomeworkCriterionEntry])
  @ApiProperty({ type: () => [HomeworkCriterionEntry] })
  @OneToMany(() => HomeworkCriterionEntry, entry => entry.answer)
  homeworkCriteriaEntries?: HomeworkCriterionEntry[];

  @Field(type => HomeworkSubmission)
  @ApiProperty({ type: () => HomeworkSubmission })
  @ManyToOne(() => HomeworkSubmission, submission => submission.answers, { onDelete: 'CASCADE' })
  @Index()
  submission: HomeworkSubmission;

  @Field(type => HomeworkQuestion)
  @ApiProperty({ type: () => HomeworkQuestion })
  @ManyToOne(() => HomeworkQuestion, { nullable: true })
  question: HomeworkQuestion;
}
