import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, Index, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import { BaseSubmission } from './base-submission.entity';
import { Course } from './course.entity';
import { HomeworkAnswer } from './homework-answer.entity';
import { Homework } from './homework.entity';
import { Lecture } from './lecture.entity';
import { Module } from './module.entity';
import { SubmissionTariff } from './submission-tariff.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class HomeworkSubmission extends BaseSubmission {
  @Field({ nullable: true })
  @ApiProperty()
  @Column('int', { nullable: true })
  score?: number;

  @ApiProperty({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  mutex?: Date;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  isMutexAuto?: boolean;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  bindTutor?: boolean;

  @Field()
  @ApiProperty({ description: 'Link to EadBox assignment for this submission' })
  link?: string;

  // TODO: потом выпиливается!
  @Field()
  @ApiProperty({ description: 'Is this submission is just a question' })
  @Column({ nullable: false, default: false })
  @Index()
  isQuestion?: boolean;

  @Field()
  @ApiProperty({ description: 'Is this submission is urgent' })
  isUrgent?: boolean;

  @Field()
  @ApiProperty({ description: 'Is this submission is outdated' })
  isOutdated?: boolean;

  @Field()
  @ApiProperty({ description: 'Is this submission is graded today' })
  isGradedToday?: boolean;

  @Field()
  @ApiProperty({ description: 'Is this submission is locked' })
  isLocked?: boolean;

  @Field(() => SubmissionTariff)
  @OneToOne(() => SubmissionTariff, tariff => tariff.submission, {
    cascade: true,
  })
  tariff: SubmissionTariff;

  @Field()
  @ApiProperty()
  @Column({ nullable: true, default: null })
  success?: boolean;

  @Field(type => User, { nullable: true })
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, { nullable: true })
  @Index()
  tutor?: User;

  @Field(type => Homework)
  @ApiProperty({ type: () => Homework })
  @ManyToOne(() => Homework, homework => homework.questions)
  @Index()
  homework: Homework;

  @Field(() => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, { nullable: true })
  @Index()
  course?: Course;

  @Field(() => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture, { nullable: true })
  @Index()
  lecture?: Lecture;

  @Field(() => Module)
  @ApiProperty()
  @ManyToOne(() => Module, { nullable: true })
  @Index()
  module?: Module;

  @Field(type => [HomeworkAnswer])
  @ApiProperty()
  @OneToMany(() => HomeworkAnswer, answer => answer.submission, { cascade: true })
  answers?: HomeworkAnswer[];
}
