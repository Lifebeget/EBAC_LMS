import { Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, PrimaryColumn } from 'typeorm';

export class SubmissionStatistic {
  @Field()
  @ApiProperty({ description: 'Statistic slice date' })
  @PrimaryColumn({ type: 'timestamptz' })
  statDate: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of students' })
  @Column({ nullable: true })
  students: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of submission' })
  @Column({ nullable: true })
  all: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of ungraded submissions' })
  @Column({ nullable: true })
  ungraded: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of graded submissions' })
  @Column({ nullable: true })
  graded: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of urgent submissions' })
  @Column({ nullable: true })
  urgent: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of outdated submissions' })
  @Column({ nullable: true })
  outdated: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of submissions created during day' })
  @Column({ nullable: true })
  createdInDay: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Number of submission graded in day' })
  @Column({ nullable: true })
  gradedInDay: number;
}
