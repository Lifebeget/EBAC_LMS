import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from './user.entity';
import { Assessment } from './assessment.entity';
import { Lecture } from './lecture.entity';
import { Course } from './course.entity';

@ObjectType()
@Entity()
export class Attempt {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => User)
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User)
  user: User;

  @Field(() => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course)
  course: Course;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture)
  lecture: Lecture;

  @Field(type => Assessment)
  @ApiProperty({ type: () => Assessment })
  @ManyToOne(() => Assessment)
  assessment: Assessment;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'Additional attempts',
  })
  @Column('int', { default: 0 })
  additionalAttempts: number;

  @Field()
  @ApiProperty()
  @CreateDateColumn()
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn()
  modified?: Date;
}
