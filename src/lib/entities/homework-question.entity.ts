import { AllowedFileTypes } from '@lib/types/enums';
import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, OneToMany, ManyToMany, JoinTable, ManyToOne, Index } from 'typeorm';
import { BaseQuestion } from './base-question.entity';
import { HomeworkCriterion } from './homework-criterion.entity';
import { Homework } from './homework.entity';
import { File } from './file.entity';

@ObjectType()
@Entity()
export class HomeworkQuestion extends BaseQuestion {
  @Field()
  @ApiProperty()
  @Column({ default: true })
  gradable?: boolean;

  @Field()
  @ApiProperty({ default: 100 })
  @Column('int', { default: 100 })
  score?: number;

  @Field()
  @ApiProperty({ default: 70 })
  @Column('int', { default: 70 })
  passingScore?: number;

  @ApiProperty({ description: 'Allow Multiple Files' })
  @Column('boolean', { default: true })
  allowMultipleFiles: boolean;

  @ApiProperty({ description: 'Allowed File Types' })
  @Column('varchar', { length: 20, default: AllowedFileTypes.Images })
  allowedFileTypes: AllowedFileTypes;

  @ApiProperty({ description: 'Max Files' })
  @Column('int', { default: 10 })
  maxFiles: number;

  @Field(() => [HomeworkCriterion])
  @ApiProperty({ type: () => [HomeworkCriterion] })
  @OneToMany(() => HomeworkCriterion, entry => entry.question)
  homeworkCriteria?: HomeworkCriterion[];

  @Field(type => [File])
  @ApiProperty({
    type: () => [File],
    description: 'Attached files',
    default: [],
  })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  @Field(type => Homework)
  @ApiProperty({ type: () => Homework })
  @ManyToOne(() => Homework, homework => homework.questions)
  @Index()
  homework: Homework;
}
