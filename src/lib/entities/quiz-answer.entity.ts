import { QuizQuestion } from './quiz-question.entity';
import { Entity, Column, ManyToOne, Index } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Field, ObjectType } from '@nestjs/graphql';
import { AnswerDataI } from '@lib/api/types';
import { BaseAnswer } from './base-answer.entity';
import { QuizSubmission } from './quiz-submission.entity';

@ObjectType()
@Entity()
export class QuizAnswer extends BaseAnswer {
  @Field(type => QuizQuestion)
  @ApiProperty({ type: () => QuizQuestion })
  @ManyToOne(() => QuizQuestion)
  question: QuizQuestion;

  @Field({ nullable: true })
  @Column({ nullable: true })
  questionId?: string;

  @ApiProperty()
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data: AnswerDataI | AnswerDataI[];

  @Field(type => QuizSubmission)
  @Index()
  @ApiProperty({ type: () => QuizSubmission })
  @ManyToOne(() => QuizSubmission, submission => submission.answers, {
    onDelete: 'CASCADE',
  })
  submission: QuizSubmission;
}
