import { SettingsImport, SettingsIntegration, SettingsMain } from '@lib/api/types';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Settings {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ description: 'main setting' })
  @Column({ type: 'jsonb', default: {} })
  mainSettings?: SettingsMain;

  @ApiProperty({ description: 'integration setting' })
  @Column({ type: 'jsonb', default: {} })
  integrationSettings?: SettingsIntegration;

  @ApiProperty({ description: 'import setting' })
  @Column({ type: 'jsonb', default: {} })
  importSettings?: SettingsImport;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
