import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '@entities/user.entity';
import { File } from '@entities/file.entity';

@Entity()
@ObjectType()
export class Persona {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(() => User, { nullable: true })
  @ApiProperty({ type: () => User, description: 'User' })
  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user?: User;

  @Field({ nullable: true })
  @ApiProperty({ description: 'ID of the user' })
  @Column({ type: 'uuid', nullable: true, unique: true })
  @Index()
  userId?: string;

  @Field(type => File, { nullable: true })
  @ApiProperty({ type: () => File, description: 'Signature' })
  @OneToOne(() => File, { cascade: true })
  @JoinColumn({ name: 'signature_id' })
  signature?: File;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Signature file ID' })
  @Column({ type: 'uuid', nullable: true, unique: true })
  signatureId?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Job title' })
  @Column('varchar', { length: 128, nullable: true })
  jobTitle?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Job description' })
  @Column('text', { nullable: true })
  jobDescription?: string;

  @Field()
  @ApiProperty({ description: 'Persona name' })
  @Column('varchar', { length: 128 })
  name: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Some passport details' })
  @Column('text', { nullable: true })
  legalData?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Hidden description' })
  @Column('text', { nullable: true })
  hiddenDescription?: string;

  @Field()
  @ApiProperty({ description: 'Is the person active' })
  @Column('boolean', { default: true })
  active: boolean;

  @Field()
  @ApiProperty({ description: 'Created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Modified' })
  @UpdateDateColumn({ type: 'timestamptz', nullable: true })
  modified?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Deleted' })
  @DeleteDateColumn({ type: 'timestamptz', nullable: true })
  deleted?: Date;
}
