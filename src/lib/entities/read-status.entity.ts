import { ReadStatusResult } from '@enums/read-status-result.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, Unique, UpdateDateColumn } from 'typeorm';
import { SystemNotification } from './system-notification.entity';
import { User } from './user.entity';

@Entity()
@Unique('pair_unique', ['user', 'systemNotification'])
export class ReadStatus {
  @ApiProperty({
    type: () => User,
    description: 'User who readed the notification',
  })
  @ManyToOne(() => User, { primary: true })
  user: User;

  @ApiProperty({
    type: () => SystemNotification,
    description: 'Related system notification',
  })
  @ManyToOne(() => SystemNotification, { primary: true })
  systemNotification: SystemNotification;

  @ApiProperty({
    enum: () => ReadStatusResult,
    description: 'Read status result (ok, dismiss, great, fine or bad)',
  })
  @Column('varchar', { length: 32, nullable: true })
  result?: ReadStatusResult;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  static of(options: Partial<ReadStatus> = {}): ReadStatus {
    return Object.assign(new ReadStatus(), options);
  }
}
