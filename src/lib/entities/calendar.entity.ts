import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ICalendar } from '@lib/helpers/date';

@Entity()
export class Calendar implements ICalendar {
  @PrimaryColumn()
  @ApiProperty({ description: 'Year of calendar' })
  year: number;

  @Column('varchar', { array: true, nullable: false, length: 5 })
  @ApiProperty({ description: 'Array of vacation days (MM-DD)' })
  vacationDays: string[];

  @Column('varchar', { array: true, nullable: false, length: 5 })
  @ApiProperty({ description: 'Array of forced working days (MM-DD)' })
  workingDays: string[];
}
