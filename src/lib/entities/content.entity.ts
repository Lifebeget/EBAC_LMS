import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ContentType } from '../enums/content-type.enum';
import { Assessment } from './assessment.entity';
import { File } from './file.entity';
import { Lecture } from './lecture.entity';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import GraphQLJSON from 'graphql-type-json';

@ObjectType()
@Entity()
export class Content {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture, description: 'Lecture' })
  @ManyToOne(() => Lecture)
  @Index()
  lecture: Lecture;

  @Field()
  @ApiProperty({ description: 'Content type' })
  @Column({ type: 'enum', enum: ContentType })
  type: ContentType;

  @Field()
  @ApiProperty({ description: 'Sorting. Min is high priority' })
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty({ description: 'Active' })
  @Column({ type: 'boolean', default: true })
  active: boolean;

  @Field()
  @ApiProperty({ description: 'Creation date' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Modified date' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field(type => User, { nullable: true })
  @ApiProperty({
    type: () => User,
    description: 'Owner of content',
    nullable: true,
  })
  @ManyToOne(() => User)
  owner?: User;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Title' })
  @Column({ type: 'varchar', nullable: true })
  title?: string;

  @Field(type => Assessment, { nullable: true })
  @ApiProperty({ type: () => Assessment, description: 'Assessment' })
  @ManyToOne(() => Assessment, { nullable: true })
  assessment?: Assessment;

  @Field({ nullable: true })
  @Column({ nullable: true })
  @Index()
  assessmentId?: string;

  @Field(type => [File])
  @ApiProperty({
    type: () => [File],
    description: 'Attached files',
    default: [],
  })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  @Field({ nullable: true })
  @ApiProperty({ description: 'Text of content' })
  @Column({ type: 'text', nullable: true })
  html?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'ID kaltura/youtube' })
  @Column({ type: 'varchar', nullable: true })
  entityId?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Link' })
  @Column({ type: 'varchar', nullable: true })
  link?: string;

  @Field(type => GraphQLJSON, { nullable: true })
  @ApiProperty({ description: 'Data' })
  @Column({ type: 'json', nullable: true })
  data?: Record<string, unknown>;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  externalId: string;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  @Index()
  deleted?: Date;
}
