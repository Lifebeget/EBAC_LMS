import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { SubmissionStatistic } from './submission-statistics.entity';

@ObjectType()
@Entity()
export class CourseStatistic extends SubmissionStatistic {
  @Field()
  @ApiProperty({ description: 'Course UUID' })
  @PrimaryColumn()
  courseId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course title' })
  @Column({ nullable: true })
  courseTitle: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course version' })
  @Column({ nullable: true })
  courseVersion: number;
}
