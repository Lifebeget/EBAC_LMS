import { Entity, Column, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Amo {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column({ unique: true })
  type: string;

  @ApiProperty()
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data: Record<string, unknown>;

  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
