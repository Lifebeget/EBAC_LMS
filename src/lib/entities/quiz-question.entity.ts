import { SingleChoiceAnswerI, MultiChoiceAnswerI, TrueFalseAnswerI, RateAnswerI } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToOne } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { BaseQuestion } from './base-question.entity';
import { Quiz } from './quiz.entity';

@ObjectType()
@Entity()
export class QuizQuestion extends BaseQuestion {
  @Field()
  @ApiProperty()
  @Column({ default: true })
  gradable?: boolean;

  @ApiProperty({ description: 'config answers' })
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data?: {
    answers: SingleChoiceAnswerI[] | MultiChoiceAnswerI[] | TrueFalseAnswerI[] | RateAnswerI[];
  };

  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackCommon?: string;

  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackCorrect?: string;

  @Field()
  @ApiProperty()
  @Column('text', { default: '' })
  feedbackIncorrect?: string;

  @Field(type => Quiz)
  @Index()
  @ApiProperty({ type: () => Quiz })
  @ManyToOne(() => Quiz, quiz => quiz.questions)
  quiz: Quiz;
}
