import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, Unique } from 'typeorm';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '@entities/user.entity';
import { FeatureFlag } from '@entities/feature-flag.entity';

@ObjectType()
@Unique('UNIQUE_FEATURE_USER', ['featureName', 'userId'])
@Entity()
export class FeatureFlagEntry {
  @Field()
  @ApiProperty({
    description: 'Feature name',
    enum: FeatureName,
  })
  @PrimaryColumn({
    enum: FeatureName,
    update: false,
  })
  featureName: FeatureName;

  @Field(() => FeatureFlag)
  @ApiProperty({ description: 'Feature flag' })
  @ManyToOne(() => FeatureFlag, feature => feature.entries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'feature_name' })
  featureFlag: FeatureFlag;

  @Field()
  @ApiProperty({
    description: 'Feature flag entry for user ID',
  })
  @PrimaryColumn({ update: false })
  userId: string;

  @Field(() => User)
  @ApiProperty({
    description: 'User entity',
  })
  @ManyToOne(() => User, user => user.featureFlagEntries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Field()
  @ApiProperty({
    description: 'Whether the feature flag is enabled',
  })
  @Column('boolean')
  enabled: boolean;
}
