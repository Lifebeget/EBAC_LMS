import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ForumStatistic } from './forum-statistics.entity';

@ObjectType()
@Entity({ name: 'course_forum_stat' })
export class CourseForumStatistic extends ForumStatistic {
  @Field()
  @ApiProperty({ description: 'Course UUID' })
  @PrimaryColumn()
  courseId: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Course title' })
  @Column({ nullable: true })
  courseTitle: string;
}
