import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ServiceLevel } from './service-level.entity';

@ObjectType()
@Entity('assessment_level_tariff')
export class ServiceLevelTariff {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Tariff activation date' })
  @Column({ type: 'timestamptz' })
  activeFrom: Date;

  @Field()
  @ApiProperty({ description: 'Tariff deactivation date' })
  @Column({ type: 'timestamptz' })
  activeTo: Date;

  @Field()
  @ApiProperty({ description: 'ID of service level' })
  @Column()
  levelId: string;

  @ApiProperty({ type: () => ServiceLevel })
  @ManyToOne(() => ServiceLevel, level => level.tariffs)
  @JoinColumn({ name: 'level_id' })
  level: ServiceLevel;

  @Field()
  @ApiProperty({
    description: 'Period after which the task becomes urgent',
  })
  @Column({ nullable: false })
  urgentHours: number;

  @Field()
  @ApiProperty({
    description: 'Period after which the task becomes outdated',
  })
  @Column({ nullable: false })
  outdatedHours: number;

  @Field()
  @ApiProperty({ description: 'Tariff amount' })
  @Column()
  value: number;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  deleted?: Date;
}
