import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, JoinColumn, ManyToOne, Index, Column, OneToMany } from 'typeorm';
import { BaseCriterion } from './base-criterion.entity';
import { HomeworkCriterionCategory } from './homework-category.entity';
import { HomeworkCriterionEntry } from './homework-criterion-entry.entity';
import { HomeworkQuestion } from './homework-question.entity';

@ObjectType()
@Entity()
export class HomeworkCriterion extends BaseCriterion {
  @Field(() => HomeworkCriterionCategory)
  @ApiProperty({
    type: () => HomeworkCriterionCategory,
    description: 'Homework criterion category',
  })
  @JoinColumn({ name: 'category_id' })
  @ManyToOne(() => HomeworkCriterionCategory, category => category.criteria)
  category: HomeworkCriterionCategory;

  @Field()
  @ApiProperty({ description: 'Question ID' })
  @Index()
  @Column()
  questionId: string;

  @Field(() => HomeworkQuestion)
  @ApiProperty({ description: 'Question', type: () => HomeworkQuestion })
  @ManyToOne(() => HomeworkQuestion, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'question_id' })
  question: HomeworkQuestion;

  @Field(() => [HomeworkCriterionEntry], {
    nullable: true,
  })
  @ApiProperty({
    description: 'Homework criterion entries',
    nullable: true,
    type: () => [HomeworkCriterionEntry],
  })
  @OneToMany(() => HomeworkCriterionEntry, entry => entry.criteria)
  homeworkCriteriaEntries?: HomeworkCriterionEntry[];
}
