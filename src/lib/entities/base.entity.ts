import { Field, ObjectType } from '@nestjs/graphql';
import { Column, CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export abstract class BaseEntity {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Is active' })
  @Column('boolean', { default: true })
  active: boolean;

  @Field()
  @ApiProperty({ description: 'Date of creation' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Date of change' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
