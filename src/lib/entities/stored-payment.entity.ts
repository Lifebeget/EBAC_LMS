import { PaymentType } from '@lib/api/reports/payments/types/payment-type.enum';
import { StoredPayment as StoredPaymentInterface } from '@lib/api/reports/payments/types/stored-payment';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ServiceLevel } from './service-level.entity';
import { User } from './user.entity';

@Entity()
export class StoredPayment implements StoredPaymentInterface {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @ApiProperty({ description: 'Id of user' })
  @Index()
  @Column()
  userId: string;

  @ApiProperty({ description: 'Date of payment' })
  @Column({ type: 'date' })
  date: string;

  @ApiProperty({ description: 'User' })
  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user?: User;

  @ApiProperty({ enum: PaymentType })
  @Column('varchar', { length: 15 })
  type: PaymentType;

  @ApiProperty({ description: 'Id of course' })
  @Column({ nullable: true })
  @Index()
  courseId?: string;

  @ApiProperty({ description: 'Title of record' })
  @Column()
  title: string;

  @ApiProperty({ description: 'Id of service level' })
  @Column({ nullable: true })
  @Index()
  levelId?: string;

  @ApiProperty({ description: 'Service level' })
  @ManyToOne(() => ServiceLevel)
  @JoinColumn({ name: 'level_id' })
  level?: ServiceLevel;

  @ApiProperty({ description: 'Value of payment (count for submission and forum, teamlead fee for teamlead bonus)' })
  @Column({
    type: 'numeric',
    precision: 15,
    scale: 2,
  })
  value: number;

  @ApiProperty({ description: 'Amount of money' })
  @Column({
    type: 'numeric',
    precision: 15,
    scale: 2,
    nullable: true,
  })
  money: number;

  @ApiProperty({ description: 'Period of report' })
  @Column()
  @Index()
  period: string;

  @Column({ type: 'jsonb', default: [] })
  @ApiProperty({ description: 'Ids of payment sourse (submissions or forum messages)' })
  sources?: string[];
}
