import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Field, ObjectType } from '@nestjs/graphql';

export interface ImportData {
  page?: number;
  index?: number;
}

@ObjectType()
@Entity()
export class DistributeLog {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Tutor ID' })
  @Column({ nullable: true })
  tutorId: string;

  @Field({ nullable: false })
  @ApiProperty({ description: 'Submission ID' })
  @Column({ nullable: false })
  submissionId: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;
}
