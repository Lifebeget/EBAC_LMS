import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  Index,
  ManyToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
  AfterLoad,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from './role.entity';
import { Enrollment } from './enrollment.entity';
import { Module } from './module.entity';
import { Exclude } from 'class-transformer';
import { UserActivity } from './user-activity.entity';
import { LectureProgress } from './lecture-progress.entity';
import { Profile } from './profile.entity';
import { File } from './file.entity';
import { Hierarchy } from './hierarchy.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { Cert } from './cert.entity';
import { Note } from './note.entity';
import { ForumMessages } from './forum-messages.entity';
import { UserSettings } from '@lib/types/settings';
import GraphQLJSON from 'graphql-type-json';
import { Submission } from '@entities/submission.entity';
import { PaymentCorrection } from './payment-correction.entity';
import { Tag } from './tag.entity';
import { FeatureFlagEntry } from '@entities/feature-flag-entry.entity';
import { BlacklistItem } from './blacklist.entity';
import { ModuleAssignment } from './module-assignment.entity';
import { SectionAssignment } from './section-assignment.entity';
import { MultiDomainsEnum } from '@lib/api/types';

@ObjectType()
@Entity()
export class User {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field()
  @ApiProperty()
  @Column()
  name: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('varchar', { length: 32, nullable: true })
  @Exclude()
  password?: string;

  @Field({ nullable: true })
  @ApiProperty()
  @Column('varchar', { length: 32, nullable: true })
  eadboxToken?: string;

  @ApiProperty()
  @Column({ unique: true })
  @Index()
  email: string;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  emailConfirmed: boolean;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  active: boolean;

  @Field()
  @ApiProperty()
  @Index()
  @Column({ default: false })
  pause: boolean;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  lastAccessed?: Date;

  @Field(() => [Submission])
  @ApiProperty({ type: () => [Submission] })
  @OneToMany(() => Submission, submission => submission.user)
  submissions?: Submission[];

  @Field(() => [Enrollment])
  @ApiProperty({ type: () => [Enrollment] })
  @OneToMany(() => Enrollment, enrollment => enrollment.user)
  enrollments?: Enrollment[];

  @Field(() => [Role])
  @ApiProperty({ type: () => [Role] })
  @OneToMany(() => Role, role => role.user, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  roles?: Role[];

  @Field(() => [Module])
  @ApiProperty({ type: () => [Module] })
  @ManyToMany(() => Module)
  @JoinTable()
  modules?: Module[];

  @Field(() => UserActivity)
  @ApiProperty({ type: () => UserActivity })
  @OneToOne(() => UserActivity, userActivity => userActivity.user, {
    cascade: true,
  })
  activity: UserActivity;

  @Field(() => [LectureProgress])
  @ApiProperty({ type: () => [LectureProgress] })
  @OneToMany(() => LectureProgress, it => it.user, {})
  lectureProgress: LectureProgress[];

  @Field(() => [Note])
  @ApiProperty({ type: () => [Note] })
  @OneToMany(() => Note, n => n.owner, {})
  notes: Note[];

  @Field(() => [ForumMessages])
  @ApiProperty({ type: () => [ForumMessages] })
  @OneToMany(() => ForumMessages, fm => fm.user, {})
  forumMessages: ForumMessages[];

  @Field(() => [Hierarchy])
  @ApiProperty({ type: () => [Hierarchy] })
  @OneToMany(() => Hierarchy, h => h.parent, {})
  hierarchyChilds: Hierarchy[];

  @Field(() => [Hierarchy])
  @ApiProperty({
    type: () => [Hierarchy],
  })
  @OneToMany(() => Hierarchy, h => h.child, {})
  hierarchyParents: Hierarchy[];

  @Field(() => [String])
  @ApiProperty({
    type: () => [String],
    description: 'Tutors ids for this teamlead',
  })
  teamleadTutorIds: string[] = [];

  @Field(() => [User])
  @ApiProperty({
    type: () => [User],
    description: 'Tutors for this teamlead',
  })
  teamleadTutors: User[] = [];

  @Field(() => [String])
  @ApiProperty({
    type: () => [String],
    description: 'Courses ids for this teamlead',
  })
  teamleadCourseIds: string[] = [];

  @Field(() => [User])
  @ApiProperty({ type: () => [User], description: 'Teamleads for this tutor' })
  teamleads: User[];

  @Field(() => Profile)
  @OneToOne(() => Profile, profile => profile.user, {
    cascade: true,
  })
  profile: Profile;

  @Field(() => File, { nullable: true })
  @ApiProperty({ type: () => File })
  @OneToOne(() => File, file => file.id, { nullable: true })
  @JoinColumn()
  avatar?: File;

  @Field({ nullable: true })
  @Column({ nullable: true })
  avatarId: string;

  @ApiProperty()
  hasPassword: boolean;

  @AfterLoad()
  getUserPasswordExists?(): void {
    this.hasPassword = this.password !== null;
  }

  @Field(() => [Cert])
  @ApiProperty({ type: () => [Cert] })
  @ApiProperty({
    type: () => [Cert],
  })
  @OneToMany(() => Cert, c => c.user, {})
  certs: Cert[];

  @Field(() => GraphQLJSON, { nullable: true })
  @ApiProperty({ description: 'User settings' })
  @Column({
    type: 'jsonb',
    nullable: true,
    array: false,
  })
  settings?: UserSettings;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Interapp user token' })
  @Column({
    type: 'varchar',
    nullable: true,
    select: false,
  })
  @Index()
  @Exclude()
  interappToken?: string;

  @Field()
  @ApiProperty({ enum: MultiDomainsEnum })
  @Column({
    type: 'varchar',
    length: 3,
    nullable: true,
  })
  domain?: MultiDomainsEnum;

  @OneToMany(() => PaymentCorrection, correction => correction.user)
  paymentCorrections?: PaymentCorrection[];

  @Field(type => [Tag])
  @ApiProperty({ type: () => [Tag] })
  @ManyToMany(() => Tag, tag => tag.courses)
  @JoinTable({ name: 'user_tags' })
  tags: Tag[];

  @Field(() => [BlacklistItem])
  @ApiProperty({ description: 'Students in tutor blacklist', type: () => [BlacklistItem] })
  @OneToMany(() => BlacklistItem, blacklistItem => blacklistItem.tutor)
  @JoinTable()
  blacklistedStudents?: BlacklistItem[];

  @Field(() => [BlacklistItem])
  @ApiProperty({ description: 'Tutors in student blacklist', type: () => [BlacklistItem] })
  @OneToMany(() => BlacklistItem, blacklistItem => blacklistItem.student)
  @JoinTable()
  blacklistedTutors?: BlacklistItem[];

  //TODO: DEPRECATED. DELETE LATER
  @Field(() => [BlacklistItem])
  @ApiProperty({ description: 'User balck list', type: () => [BlacklistItem] })
  @OneToMany(() => BlacklistItem, blacklistItem => blacklistItem.user)
  @JoinTable()
  blacklist?: BlacklistItem[];

  //TODO: DEPRECATED. DELETE LATER
  @Field(() => [BlacklistItem])
  @ApiProperty({ description: 'Blacklisted by user', type: () => [BlacklistItem] })
  @OneToMany(() => BlacklistItem, blacklistItem => blacklistItem.blockedUser)
  @JoinTable()
  blacklistedBy?: BlacklistItem[];

  @Field(() => [FeatureFlagEntry], { nullable: true })
  @ApiProperty({ description: 'User feature flag entries' })
  @OneToMany(() => FeatureFlagEntry, entry => entry.user)
  featureFlagEntries?: FeatureFlagEntry[];

  @Field(() => [String], { nullable: true })
  @ApiProperty({ description: 'User features' })
  features?: string[];

  @Field(() => [ModuleAssignment])
  @ApiProperty({ type: () => [ModuleAssignment] })
  @OneToMany(() => ModuleAssignment, moduleAssignment => moduleAssignment.user, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinTable()
  moduleAssignments?: ModuleAssignment[];

  @Field(() => [SectionAssignment])
  @ApiProperty({ type: () => [SectionAssignment] })
  @OneToMany(() => SectionAssignment, sectionAssignment => sectionAssignment.user, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinTable()
  sectionAssignments?: SectionAssignment[];
}
