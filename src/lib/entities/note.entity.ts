import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Lecture } from './lecture.entity';
import { Course } from './course.entity';
import { User } from './user.entity';
import { File } from './file.entity';

import { NoteAnchor } from '../enums/note-anchor.enum';
import { Content } from './content.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class Note {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => Lecture, { nullable: true })
  @ApiProperty({ type: () => Lecture, description: 'Lecture' })
  @ManyToOne(() => Lecture, { nullable: true })
  lecture?: Lecture;

  @ApiProperty({ type: () => Content, description: 'Content' })
  @ManyToOne(() => Content, { nullable: true })
  content?: Content;

  @Field(type => Course)
  @ApiProperty({ type: () => Course, description: 'Course' })
  @ManyToOne(() => Course)
  course: Course;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Timecode in video' })
  @Column({ type: 'double precision', nullable: true })
  timecode?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Quoted text' })
  @Column({ type: 'varchar', nullable: true })
  quote?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Anchor to text' })
  @Column({ type: 'enum', enum: NoteAnchor, nullable: true })
  anchor?: NoteAnchor;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Position in text (percent)' })
  @Column({ type: 'double precision', nullable: true })
  position?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Text of note' })
  @Column({ type: 'text', nullable: true })
  html?: string;

  @Field(type => [File])
  @ApiProperty({ type: () => [File], description: 'Attached files' })
  @ManyToMany(() => File)
  @JoinTable()
  files?: File[];

  @Field()
  @ApiProperty({ description: 'Creation date' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Modified date' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty({ description: 'Deleted date' })
  @Column({ type: 'timestamptz', nullable: true })
  deleted?: Date;

  @Field(type => User)
  @ApiProperty({ type: () => User, description: 'Owner of note' })
  @ManyToOne(() => User)
  owner: User;
}
