import { EnrollmentRightsType } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Course } from './course.entity';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';

@ObjectType()
@Entity()
export class Enrollment {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field(type => User)
  @ApiProperty({ type: () => User })
  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  @Index()
  user: User;

  @Field({ nullable: true })
  @Column({ nullable: true })
  userId: string;

  @Field(type => Course)
  @Index()
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Field({ nullable: true })
  @Column({ nullable: true })
  courseId: string;

  @Field()
  @Index()
  @ApiProperty({ enum: CourseRole })
  @Column('varchar', { length: 40 })
  role: CourseRole;

  @Field()
  @ApiProperty({ enum: EnrollmentType })
  @Column('varchar', { length: 40, default: EnrollmentType.Unknown })
  @Index()
  type: EnrollmentType;

  @Field()
  @Index()
  @ApiProperty()
  @Column({ default: true })
  active?: boolean;

  @Field({ nullable: true })
  @Index()
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  dateFrom?: Date;

  @Field({ nullable: true })
  @Index()
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  dateTo?: Date;

  @Field()
  @Index()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @Index()
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  graduatedAt?: Date;

  @Field()
  @ApiProperty()
  @Column({ nullable: true, type: 'timestamptz' })
  serviceEndsAt?: Date;

  @ApiProperty({ nullable: true })
  @Column('jsonb', { nullable: true })
  reason?: Record<string, unknown>;

  @ApiProperty({
    description: 'Tariff for tutor interactivity (percents of income)',
  })
  @Column({ nullable: false, default: 10 })
  interactiveTariff?: number;

  @ApiProperty({
    description: 'Fee for teamlead activity',
  })
  @Column({ nullable: true })
  teamleadFee?: number;

  @ApiProperty({
    description: 'Enrollment right',
  })
  @Column('jsonb', { default: {} })
  rights?: EnrollmentRightsType;

  @ApiProperty({ nullable: true })
  @Column('jsonb', { nullable: true })
  options?: Record<string, unknown>;
}
