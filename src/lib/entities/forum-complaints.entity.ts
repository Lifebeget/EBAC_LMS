import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ComplaintType } from '@enums/complaint-type.enum';
import { ForumMessages } from './forum-messages.entity';
import { User } from './user.entity';
import { JoinColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class ForumComplaints {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => ForumMessages)
  @ApiProperty({
    type: () => ForumMessages,
    description: 'Message',
  })
  @ManyToOne(() => ForumMessages, { nullable: false })
  @JoinColumn({ name: 'message_id' })
  message: ForumMessages;

  @Field()
  @Column({ nullable: false })
  messageId: string;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Field()
  @Column({ nullable: false })
  userId: string;

  @Field()
  @ApiProperty()
  @Column({
    type: 'enum',
    enum: ComplaintType,
    default: ComplaintType.other,
  })
  complaintType?: ComplaintType;

  @Field({ nullable: true })
  @ApiProperty({
    description: 'custom text of compaint(for ContentType.other)',
  })
  @Column({ nullable: true })
  text?: string;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  active: boolean;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
