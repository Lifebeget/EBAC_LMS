import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, ManyToOne, UpdateDateColumn, Unique } from 'typeorm';
import { ForumMessages } from './forum-messages.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
@Unique('FORUM_LIKES_MESSAGE_USER', ['message', 'user'])
export class ForumLikes {
  @Field(type => ForumMessages)
  @ApiProperty({
    type: () => ForumMessages,
    description: 'Message',
  })
  @ManyToOne(() => ForumMessages, { primary: true, onDelete: 'CASCADE' })
  message: ForumMessages;

  @Field(type => User)
  @Column({ nullable: false, primary: true })
  messageId: string;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User, { primary: true })
  user: User;

  @Field()
  @Column({ nullable: false, primary: true })
  userId: string;

  @Field()
  @ApiProperty({ description: 'like value +1/-1' })
  @Column()
  like: number;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
