import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany, JoinColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Course } from './course.entity';
import { Module } from './module.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { SectionAssignment } from './section-assignment.entity';

@ObjectType()
@Entity()
export class Section {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Description' })
  @Column('text', { nullable: true })
  description?: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  active?: boolean;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field(type => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, course => course.sections)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Field({ nullable: true })
  @Column({ nullable: true })
  courseId: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  published?: boolean;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  publishedAt?: Date;

  @Field(type => [Module])
  @ApiProperty({ type: () => [Module] })
  @OneToMany(() => Module, module => module.section)
  modules: Module[];

  @Field(() => [SectionAssignment])
  @ApiProperty({ type: () => [SectionAssignment] })
  @OneToMany(() => SectionAssignment, sectionAssignment => sectionAssignment.section, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  sectionAssignments?: SectionAssignment[];
}
