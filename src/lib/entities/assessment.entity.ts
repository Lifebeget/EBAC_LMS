import { ServiceLevel } from './service-level.entity';
import { AssessmentGradingType, AssessmentType, ShowCorrectAnswers, ShowResult } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, DeleteDateColumn, Entity, Index, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Lecture } from './lecture.entity';
import { Question } from './question.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { Submission } from './submission.entity';
import { ServiceLevelTariff } from './service-level-tariff.entity';
import { BaseAssessment } from './base-assessment.entity';

@ObjectType()
@Entity()
export class Assessment extends BaseAssessment {
  // Homeworks
  @Field({ nullable: true })
  @ApiProperty()
  @Column('text', { nullable: true })
  tutorMessage?: string;

  // Homeworks, Quiz
  @Field()
  @ApiProperty({ default: 100 })
  @Column('int', { default: 100 })
  score?: number;

  // Homeworks, Quiz
  @Field()
  @ApiProperty({ default: 70 })
  @Column('int', { default: 70 })
  @Index()
  passingScore?: number;

  // Quiz
  @Field()
  @ApiProperty({
    default: 0,
    description: 'Time limit for 1 try, 0 - unlimited',
  })
  // лимит времени на 1 попытку(0 - бесконечно)
  @Column('int', { default: 0 })
  timeLimitSeconds: number;

  // Quiz, Survey
  @Field()
  @ApiProperty({
    default: 0,
    description: 'Estimated time for survey (minutes)',
  })
  // примерное времени на выполнение(для опросов)
  @Column('int', { default: 0 })
  timeSeconds: number;

  // Quiz, Survey
  // TODO: this field should be removed when Quiz and Survey will be moved in its own entities
  @Field()
  @ApiProperty({
    default: 0,
    description: 'Limit number of retries for quiz and survey. 0 for unlimited',
  })
  // лимит кол-ва попыток(2 - можно сдать 2 раза, 0 - бесконечно)
  @Column('int', { default: 0 })
  countLimit: number;

  // Task
  @Field()
  @ApiProperty({
    default: 3,
    description: 'Limit number of retries for tasks',
  })
  @Column('int', { default: 3 })
  maxAttempts: number;

  // Survey, Quiz
  @Field()
  @ApiProperty({
    default: 24,
    description: 'Pause between quiz retries (hours)',
  })
  // пауза до следующей пересдачи(24 часа)
  @Column('int', { default: 24 })
  timeDelaytoRetestHours: number;

  // Quiz
  @Field()
  @ApiProperty({
    default: ShowResult.onFinish,
    description: 'When to show quiz results',
  })
  // показывать результат
  @Column('int', { default: ShowResult.onFinish })
  showResult: ShowResult;

  // Quiz
  @Field()
  @ApiProperty({
    default: ShowCorrectAnswers.showOnlyIncorrect,
    description: 'Different modes for displaying correct answers',
  })
  // показывать ответы
  @Column('int', { default: ShowCorrectAnswers.showOnlyIncorrect })
  showCorrectAnswers: ShowCorrectAnswers;

  // Survey
  @Field()
  @ApiProperty({
    default: true,
    description: 'Can user skip the questions',
  })
  // можно ли пропускать вопросы(перейти к след без ответа)
  @Column('boolean', { default: true })
  canSkipQuestion: boolean;

  // Survey, Quiz
  @Field()
  @ApiProperty({
    default: true,
    description: 'Can user change answers',
  })
  // можно ли изменить ответ(до завершения)
  @Column('boolean', { default: true })
  canChangeAnswer: boolean;

  // Quiz
  @Field()
  @ApiProperty({ default: true, description: 'Shuffle order of question' })
  // перемешивать вопросы
  @Column('boolean', { default: true })
  shuffleQuestions: boolean;

  // Quiz
  @Field()
  @ApiProperty({ default: true, description: 'Shuffle order of answers' })
  // перемешивать ответы
  @Column('boolean', { default: true })
  shuffleAnswers: boolean;

  // Howemorks
  @Field()
  @ApiProperty({
    enum: AssessmentGradingType,
    default: AssessmentGradingType.OverallShouldPass,
  })
  @Column('varchar', {
    length: 80,
    default: AssessmentGradingType.OverallShouldPass,
  })
  gradingType: AssessmentGradingType;

  // Homeworks, Quiz, Surveys exclude webinar surveys, /exclude global surveys???/
  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture })
  @ManyToOne(() => Lecture, lecture => lecture.assessments)
  lecture: Lecture;

  // Webinar survey
  @Field({ nullable: true })
  @ApiProperty({ description: 'ID of webinar' })
  @Column({ nullable: true })
  @Index()
  webinarId: string;

  // Homeworks
  @Field(type => ServiceLevel)
  @ApiProperty({ type: () => ServiceLevel, description: 'Service level' })
  @ManyToOne(() => ServiceLevel, level => level.assessments)
  @JoinColumn({ name: 'level_id' })
  level: ServiceLevel;

  // Homeworks
  @Field({ nullable: true })
  @ApiProperty({ description: 'ID of service level' })
  @Column({ nullable: true })
  @Index()
  levelId: string;

  // All
  @Field(type => [Question])
  @ApiProperty({ type: () => [Question] })
  @OneToMany(() => Question, question => question.assessment)
  questions: Question[];

  // Временно оставить везде, потом выпилить
  @Field()
  @ApiProperty({
    enum: AssessmentType,
    default: AssessmentType.Default,
  })
  @Column({ default: AssessmentType.Default })
  @Index()
  type: AssessmentType;

  // Survey
  @Field()
  @ApiProperty({ description: 'Global assessment' })
  @Column('boolean', { default: false })
  isGlobal: boolean;

  // Webinar survey only
  @Field()
  @ApiProperty({ description: 'Webinar assessment' })
  @Column('boolean', { default: false })
  isWebinarSurvey: boolean;

  // All
  @Field(type => [Submission])
  @ApiProperty({ type: () => [Submission] })
  @OneToMany(() => Submission, submission => submission.assessment)
  submissions: Submission[];

  // Homeworks
  @ApiProperty({
    type: () => ServiceLevelTariff,
    description: 'Current tariff of assessment',
  })
  currentTariff?: ServiceLevelTariff;

  // Survey, Quiz
  questionCount?: number;
}
