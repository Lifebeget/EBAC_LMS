import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Assessment } from './assessment.entity';
import { Course } from './course.entity';
import { Lecture } from './lecture.entity';

@ObjectType()
@Entity()
export class ImportSurveyLink {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Survey ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  surveyId: string;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  isGlobal?: boolean;

  @Field(type => Course)
  @ApiProperty({ type: () => Course, description: 'Course' })
  @ManyToOne(() => Course, { nullable: true })
  course?: Course;

  @Field({ nullable: true })
  @Column({ nullable: true })
  courseId?: string;

  @Field(type => Lecture)
  @ApiProperty({ type: () => Lecture, description: 'Lecture' })
  @ManyToOne(() => Lecture, { nullable: true })
  lecture?: Lecture;

  @Field()
  @Column({ nullable: true })
  lectureId?: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field(type => Assessment)
  @ApiProperty({ type: () => Course, description: 'Course' })
  @ManyToOne(() => Assessment, { nullable: true })
  assessment?: Assessment;

  @Field()
  @Column({ nullable: true })
  assessmentId?: string;

  @ApiProperty()
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  data: Record<string, unknown>;
}
