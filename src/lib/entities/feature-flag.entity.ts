import { Field, ObjectType } from '@nestjs/graphql';
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryColumn, UpdateDateColumn } from 'typeorm';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { ApiProperty } from '@nestjs/swagger';
import { FeatureType } from '@lib/types/enums/feature-flags/feature-type.enum';
import { FeatureFlagEntry } from '@entities/feature-flag-entry.entity';
import { FeatureStrategy } from '@lib/types/enums/feature-flags/feature-strategy.enum';
import { FeatureFlagRolesEntry } from '@entities/feature-flag-roles-entry.entity';

@ObjectType()
@Entity()
export class FeatureFlag {
  @Field()
  @ApiProperty({
    description: 'Feature name',
    enum: FeatureName,
  })
  @Column({
    enum: FeatureName,
    unique: true,
    update: false,
  })
  @PrimaryColumn()
  name: FeatureName;

  @Field(() => [FeatureFlagEntry])
  @ApiProperty({ description: 'Feature entries' })
  @OneToMany(() => FeatureFlagEntry, entry => entry.featureFlag, { cascade: true, onDelete: 'CASCADE' })
  entries?: FeatureFlagEntry[];

  @Field(() => [FeatureFlagRolesEntry])
  @ApiProperty({ description: 'User roles for feature' })
  @OneToMany(() => FeatureFlagRolesEntry, roleEntry => roleEntry.featureFlag, { cascade: true, onDelete: 'CASCADE' })
  roleEntries?: FeatureFlagRolesEntry[];

  @Field()
  @ApiProperty({ description: 'Feature is enabled' })
  @Column('boolean')
  enabled: boolean;

  @Field(() => String, { nullable: true })
  @ApiProperty({ description: 'Feature description' })
  @Column('text', { nullable: true })
  description?: string;

  @Field()
  @ApiProperty({
    enum: FeatureType,
    description: 'Feature type',
  })
  @Column({ enum: FeatureType, default: FeatureType.Release })
  type: FeatureType;

  @Field()
  @ApiProperty({
    enum: FeatureType,
    description: 'Feature type',
  })
  @Column({ enum: FeatureStrategy, default: FeatureStrategy.AllUsers })
  strategy: FeatureStrategy;

  @Field(() => String, { nullable: true })
  @ApiProperty({
    description: 'Feature task URL',
  })
  @Column('text', { nullable: true })
  taskUrl?: string;

  @Field(() => String, { nullable: true })
  @ApiProperty({
    description: 'Feature merge request URL',
  })
  @Column('text', { nullable: true })
  mergeRequestUrl?: string;

  @Field()
  @ApiProperty({ description: 'Created' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Modified' })
  @UpdateDateColumn({ nullable: true, type: 'timestamptz' })
  modified?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Deleted' })
  @DeleteDateColumn({ nullable: true, type: 'timestamptz' })
  deleted?: Date;
}
