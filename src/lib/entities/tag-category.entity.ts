import { TagType } from '@lib/types/enums/tag-type.enum';
import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Tag } from './tag.entity';

@ObjectType()
@Entity()
export class TagCategory {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Category title' })
  @Index({ unique: true })
  @Column('varchar', { length: 100 })
  title: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty({ description: 'Category type' })
  @Column('varchar', { length: 40, default: TagType.CourseVertical })
  type: TagType;

  @Field(() => [Tag])
  @ApiProperty({ type: () => [Tag] })
  @OneToMany(() => Tag, tag => tag.category)
  tags: Tag[];
}
