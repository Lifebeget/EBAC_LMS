import { EmailStatus } from '@enums/email-status.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EmailQueue {
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @ApiProperty({ description: "Recipient's email" })
  @Column()
  to: string;

  @ApiProperty({ description: "Recipient's id" })
  @Column()
  userId: string;

  @ApiProperty({ enum: TriggerType, description: 'Email notification type' })
  @Column('varchar', { length: 64 })
  triggerType: TriggerType;

  @ApiProperty({ enum: EmailStatus, description: 'Send status' })
  @Column('varchar', { length: 32 })
  status: EmailStatus;

  @ApiProperty({ description: 'Template fields data' })
  @Column('jsonb', { nullable: true })
  data?: Record<string, unknown>;

  @ApiProperty({ description: 'Failed status code' })
  @Column({ nullable: true })
  statusCode: number;

  @ApiProperty({ description: 'Failed status text' })
  @Column({ nullable: true })
  statusText?: string;
}
