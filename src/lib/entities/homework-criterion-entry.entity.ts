import { ObjectType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Unique, Entity, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { BaseCriterionEntry } from './base-criterion-entry.entity';
import { HomeworkAnswer } from './homework-answer.entity';
import { HomeworkCriterion } from './homework-criterion.entity';

@ObjectType()
@Unique('UNIQUE_HOMEWORK_ANSWER_AND_HOMEWORK_CRITERION', ['answerId', 'criteriaId'])
@Entity()
export class HomeworkCriterionEntry extends BaseCriterionEntry {
  @Field()
  @ApiProperty({ description: 'Answer UUID' })
  @PrimaryColumn({ update: false })
  answerId: string;

  @Field(() => HomeworkAnswer)
  @ApiProperty({ description: 'Answer', type: () => HomeworkAnswer })
  @ManyToOne(() => HomeworkAnswer, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'answer_id' })
  answer: HomeworkAnswer;

  @Field()
  @ApiProperty({ description: 'Criteria UUID' })
  @PrimaryColumn({ update: false })
  criteriaId: string;

  @Field(() => HomeworkCriterion)
  @ApiProperty({
    description: 'Criteria',
    type: () => HomeworkCriterion,
  })
  @ManyToOne(() => HomeworkCriterion, criterion => criterion.homeworkCriteriaEntries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'criteria_id' })
  criteria: HomeworkCriterion;
}
