import { ESupportMessageStatus } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class SupportMessage {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty({ description: 'UUID' })
  id: string;

  @Index()
  @ManyToOne(() => User)
  @ApiProperty({
    type: () => User,
    description: 'Message author',
    nullable: true,
  })
  user: User | null;

  @Column('text')
  @ApiProperty({ description: 'Message topic' })
  topic: string;

  @Column('text')
  @ApiProperty({ description: 'Message text' })
  text: string;

  @Column('varchar', { nullable: true })
  @ApiProperty({ description: 'AMO CRM id', nullable: true })
  amoCrmId: string | null;

  @Column('varchar', { nullable: true })
  @ApiProperty({ description: 'Author IP address' })
  ip: string | null;

  @Column('json')
  @ApiProperty({ description: 'Message data' })
  data: Record<string, any>;

  @Column('varchar', { nullable: true })
  @ApiProperty({ description: 'Additional email', nullable: true })
  email: string | null;

  @Column('varchar', { nullable: true })
  @ApiProperty({ description: 'Additional name', nullable: true })
  name: string | null;

  @CreateDateColumn({ type: 'timestamptz' })
  @ApiProperty({ description: 'Created at' })
  created: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  @ApiProperty({ description: 'Updated at' })
  updated: Date;

  @Column({
    type: 'enum',
    enum: ESupportMessageStatus,
    default: ESupportMessageStatus.NEW,
  })
  @ApiProperty({
    description: 'Status',
    enum: ESupportMessageStatus,
    default: ESupportMessageStatus.NEW,
  })
  status: ESupportMessageStatus;
}
