import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinColumn, ManyToOne, Index, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity({ name: 'blacklist' })
export class BlacklistItem {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(type => [BlacklistItem])
  @ApiProperty({ description: 'Tutor', type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'tutor_id' })
  tutor: User;

  @Column({ nullable: true })
  @Index()
  tutorId: string;

  @Field(type => [BlacklistItem])
  @ApiProperty({ description: 'Student', type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'student_id' })
  student: User;

  @Column({ nullable: true })
  @Index()
  studentId: string;

  //TODO: DEPRECATED. DELETE LATER
  @Field(type => [BlacklistItem])
  @ApiProperty({ description: 'For whom it is blocked (Tutor)', type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'user_id' })
  user: User;

  //TODO: DEPRECATED. DELETE LATER
  @Column({ nullable: true })
  @Index()
  userId: string;

  //TODO: DEPRECATED. DELETE LATER
  @Field(type => [BlacklistItem])
  @ApiProperty({ description: 'Who is blocked (Student)', type: () => User })
  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'blocked_user_id' })
  blockedUser: User;

  //TODO: DEPRECATED. DELETE LATER
  @Column({ nullable: true })
  @Index()
  blockedUserId: string;

  @Field()
  @ApiProperty({ description: 'Reason' })
  @Column('text', { nullable: false })
  reason: string;

  @Field()
  @ApiProperty({ description: 'Date of creation' })
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Date of change' })
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;
}
