import { Field, ObjectType } from '@nestjs/graphql';
import { Column, CreateDateColumn, DeleteDateColumn, Entity, Index, JoinColumn, ManyToOne, PrimaryColumn, UpdateDateColumn } from 'typeorm';
import { LectureStatus } from '../enums/lecture-status.enum';
import { Lecture } from './lecture.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class LectureProgress {
  @Field(type => User)
  @ManyToOne(() => User, it => it.lectureProgress)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @PrimaryColumn()
  @Index()
  userId: string;

  @Field(type => Lecture)
  @ManyToOne(() => Lecture, it => it.lectureProgress)
  @JoinColumn({ name: 'lecture_id' })
  lecture: Lecture;

  @PrimaryColumn()
  @Index()
  lectureId: string;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  viewed: Date;

  @Field({ nullable: true })
  @Column({ type: 'timestamptz', nullable: true })
  ended: Date;

  @Field()
  @Column({ type: 'float', default: 0 })
  progress: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  @Index()
  status: LectureStatus;

  @Field()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  @Field()
  @DeleteDateColumn({ type: 'timestamptz' })
  @Index()
  deleted?: Date;
}
