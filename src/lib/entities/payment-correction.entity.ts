import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Entity('correction')
export class PaymentCorrection {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ type: () => User, description: 'User for correction' })
  @Field(() => User)
  @ManyToOne(() => User, user => user.paymentCorrections)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @ApiProperty({ description: 'Id of user for correction' })
  @Field()
  @Column()
  userId: string;

  @Field()
  @ApiProperty({ description: 'Date of correction' })
  @Column({ type: 'date' })
  date: Date;

  @Field()
  @ApiProperty({ description: 'Correction value' })
  @Column()
  value: number;

  @Field()
  @ApiProperty({ description: 'Correction reason' })
  @Column()
  reason: string;
}
