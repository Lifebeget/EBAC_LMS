import { Field, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column } from 'typeorm';
import { CreateDateColumn, Entity, ManyToOne } from 'typeorm';
import { ForumMessages } from './forum-messages.entity';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class ForumSubscribe {
  @Field(type => ForumMessages)
  @ApiProperty({
    type: () => ForumMessages,
    description: 'Message',
  })
  @ManyToOne(() => ForumMessages, { primary: true, onDelete: 'CASCADE' })
  message: ForumMessages;

  @Field()
  @Column({ primary: true, nullable: false })
  messageId: string;

  @Field(type => User)
  @ApiProperty({
    type: () => User,
    description: 'User',
  })
  @ManyToOne(() => User, { primary: true })
  user: User;

  @Field()
  @Column({ primary: true, nullable: false })
  userId: string;

  @Field(type => ForumMessages)
  @ApiProperty({
    type: () => ForumMessages,
    description: 'Thread parent message',
  })
  @ManyToOne(() => ForumMessages, { onDelete: 'CASCADE' })
  thread: ForumMessages;

  @Field()
  @Column({ nullable: false })
  threadId: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;
}
