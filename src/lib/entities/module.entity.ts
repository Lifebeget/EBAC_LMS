import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  Index,
  JoinColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Course } from './course.entity';
import { Lecture } from './lecture.entity';
import { Section } from './section.entity';
import { Field, ObjectType } from '@nestjs/graphql';
import { ModuleAssignment } from './module-assignment.entity';

@ObjectType()
@Entity()
export class Module {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'External ID (EADBOX)' })
  @Column({ nullable: true })
  @Index()
  externalId: string;

  @Field(() => String, { nullable: true })
  @ApiProperty({ description: 'Description' })
  @Column('text', { nullable: true })
  description?: string;

  @Field()
  @ApiProperty()
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty()
  @Column({ default: true })
  @Index()
  active?: boolean;

  @Field()
  @ApiProperty()
  @Column()
  title: string;

  @Field(() => Course)
  @ApiProperty({ type: () => Course })
  @ManyToOne(() => Course, course => course.modules)
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Field({ nullable: true })
  @Column({ nullable: true })
  @Index()
  courseId: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Module number' })
  @Column({ nullable: true })
  number: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Module custom number' })
  @Column({ nullable: true })
  customNumber: string;

  @Field()
  @ApiProperty()
  @Column({ default: false })
  @Index()
  published?: boolean;

  @Field({ nullable: true })
  @ApiProperty()
  @Column({ type: 'timestamptz', nullable: true })
  publishedAt?: Date;

  @Field(() => [Lecture])
  @ApiProperty({ type: () => [Lecture] })
  @OneToMany(() => Lecture, lecture => lecture.module)
  lectures: Lecture[];

  @Field(() => Section, { nullable: true })
  @ApiProperty({ type: () => Section, required: false })
  @ManyToOne(() => Section, section => section.modules, { nullable: true })
  @JoinColumn({ name: 'section_id' })
  section: Section;

  @Field({ nullable: true })
  @Column({ type: 'uuid', nullable: true })
  @Index()
  section_id: string;

  @Field()
  @ApiProperty({ description: 'Numeration of module lectures from 0' })
  @Column({ type: 'boolean', default: false })
  numberLecturesFromZero: boolean;

  @Field(() => [ModuleAssignment])
  @ApiProperty({ type: () => [ModuleAssignment] })
  @OneToMany(() => ModuleAssignment, moduleAssignment => moduleAssignment.module, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  moduleAssignments?: ModuleAssignment[];
}
