import { ApiProperty } from '@nestjs/swagger';
import { Column, CreateDateColumn, DeleteDateColumn, Entity, Index, JoinColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from './user.entity';
import { Course } from './course.entity';

@ObjectType()
@Entity()
export class Nps {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(() => User)
  @ApiProperty({ type: () => User, description: 'User' })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Field({ nullable: false })
  @ApiProperty({ description: 'ID of the user' })
  @Column({ nullable: false })
  @Index()
  userId: string;

  @Field(() => Course)
  @ApiProperty({ type: () => Course, description: 'Course' })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Field({ nullable: false })
  @ApiProperty({ description: 'ID of the course' })
  @Column({ nullable: false })
  @Index()
  courseId: string;

  @Field({ nullable: false })
  @ApiProperty({ description: 'NPS type (first, second...)' })
  @Column('varchar', { length: 100, nullable: false })
  type?: string;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'Number of completed modules by user at the time of creation',
  })
  @Column('int', { default: 0 })
  modulesCompleted: number;

  @Field()
  @ApiProperty({
    default: 0,
    description: 'User progress on the course at the time of creation',
  })
  @Column('int', { default: 0 })
  courseProgress: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Score from 0 to 10' })
  @Column('int', { nullable: true })
  score?: number;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date and time when last time suspended' })
  @Column({ type: 'timestamptz', nullable: true })
  suspended?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date and time when declined by the user' })
  @Column({ type: 'timestamptz', nullable: true })
  declined?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Date and time of completion' })
  @Column({ type: 'timestamptz', nullable: true })
  completed?: Date;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Positive comment' })
  @Column('text', { nullable: true })
  positiveComment?: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Suggestions for improvement' })
  @Column('text', { nullable: true })
  negativeComment?: string;

  @Field()
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamptz' })
  created?: Date;

  @Field()
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamptz' })
  modified?: Date;

  @Field()
  @ApiProperty()
  @DeleteDateColumn({ type: 'timestamptz' })
  @Index()
  deleted?: Date;
}
