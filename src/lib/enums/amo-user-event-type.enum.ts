export enum AmoUserEventType {
  courseOnboarding = 'Course onboarding',
  courseOnboardingSuccess = 'Course onboarding success',
  courseOnboardingLost = 'Course onboarding lost',
  courseNotStarted = 'Course not started',
  courseStarted = 'Course started',
  courseAlmostCompleted = 'Course almost completed',
  courseCompleted = 'Course completed',
  courseStoppedStudying = 'Course stopped studying',
  supportMessageCreated = 'Support message created',
}
