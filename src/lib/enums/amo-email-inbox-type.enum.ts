export enum AmoEmailInboxType {
  support = 'support',
  tutorSupport = 'tutorSupport',
  sales = 'sales',
  test = 'test',
}
