export enum SourcePageTypeCms {
  LANDING = 'Course Landing',
  WEBINAR = 'Schedule Event',
  WEBINAR_SALE = 'Schedule Event Discount',
  FACEBOOK = 'Facebook',
  EVENT = 'Sale Page',
  OTHER = 'Other',
}
