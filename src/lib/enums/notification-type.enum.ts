export enum NotificationType {
  SubmissionGraded = 'submission_graded',
  CourseNewLesson = 'course_new_lesson',
  ForumNewReply = 'forum_new_reply',
  ForumNewComplaint = 'forum_new_complaint',
  SubmissionAnswered = 'submission_answered',
}
