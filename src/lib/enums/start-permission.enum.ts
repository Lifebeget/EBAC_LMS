export enum StartPermission {
  Allow = 'allow',
  Deny = 'deny',
  Wait = 'wait',
}
