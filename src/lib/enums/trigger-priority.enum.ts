export enum TriggerPriority {
  instantly = 0,
  high = 10,
  normal = 20,
  low = 30,
}
