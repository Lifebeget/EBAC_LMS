export enum LectureStatus {
  success = 'success',
  inProgress = 'inProgress',
  failed = 'failed',
}
