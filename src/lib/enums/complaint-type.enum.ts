export enum ComplaintType {
  sexualContent = 'Sexual content',
  intolerance = 'intolerance',
  insultsOrThreats = 'Insults or threats',
  terroristPropaganda = 'Terrorist propaganda',
  spam = 'spam',
  other = 'other',
}
