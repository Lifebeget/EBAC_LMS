export enum ImportType {
  None = 'none',
  Full = 'full',
  Fast = 'fast',
  FastCourse = 'fast-course',
  Users = 'partial',
  Mutex = 'mutex',
}
