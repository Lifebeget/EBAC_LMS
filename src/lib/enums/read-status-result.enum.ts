export enum ReadStatusResult {
  Ok = 'ok',
  Dismiss = 'dismiss',
  Great = 'great',
  Fine = 'fine',
  Bad = 'bad',
}
