export enum SourceActionCms {
  LEAD = 'CRM Lead', // заявка из этой формы превращается в сделку в CRM
  SUBSCRIPTION = 'Event Subscription', // заявка из этой формы превращается в подписку на вебинар
  CONSULT = 'Consultation', //  на текущий момент не используется
}
