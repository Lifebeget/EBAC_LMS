export enum SourceActionLms {
  LEAD = 'lead', // заявка из этой формы превращается в сделку в CRM
  SUBSCRIPTION = 'subscription', // заявка из этой формы превращается в подписку на вебинар
  CONSULT = 'consult', //  на текущий момент не используется
}
