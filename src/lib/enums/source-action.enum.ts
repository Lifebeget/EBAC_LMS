export enum SourceAction {
  CRM_LEAD = 'CRM Lead', // заявка из этой формы превращается в сделку в CRM
  EVENT_SUBSCRIPTION = 'Event Subscription', // заявка из этой формы превращается в подписку на вебинар
  CONSULTATION = 'Consultation', //  на текущий момент не используется
}
