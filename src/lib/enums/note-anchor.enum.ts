export enum NoteAnchor {
  title = 'title',
  description = 'description',
  content = 'content',
}
