export enum ImportState {
  Running = 'running',
  Completed = 'completed',
  Fail = 'fail',
  Interrupt = 'interrupt',
}
