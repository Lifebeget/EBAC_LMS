import { CreateRoleDto } from 'src/users/dto/create-role.dto';
import { SystemRole } from '@lib/api/types';

export const RoleGradeMap = {
  [SystemRole.Superadmin]: 100,
  [SystemRole.Admin]: 90,
  [SystemRole.Observer]: 70,
  [SystemRole.Teamlead]: 70,
  [SystemRole.Manager]: 60,
  [SystemRole.Support]: 60,
  [SystemRole.Designer]: 60,
  [SystemRole.EventManager]: 60,
  [SystemRole.Tutor]: 40,
  [SystemRole.ProfileViewer]: 20,
};

export function mapGradeForRoles(roles: CreateRoleDto[]) {
  const grades = roles.map(role => RoleGradeMap[role.role]);
  const filteredGrades = grades.filter(grade => grade != null);
  return Math.max(...filteredGrades);
}
