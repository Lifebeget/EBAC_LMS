export enum SourcePageTypeLms {
  LANDING = 'landing',
  WEBINAR = 'webinar',
  WEBINAR_SALE = 'webinar_sale',
  FACEBOOK = 'facebook',
  EVENT = 'event',
  OTHER = 'other',
}
