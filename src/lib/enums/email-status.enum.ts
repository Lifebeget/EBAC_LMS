export enum EmailStatus {
  sending = 'Sending',
  sent = 'Sent',
  skipped = 'Skipped',
  failed = 'Failed',
}
