export enum FileFolder {
  contentFiles = 'content-files',
  avatars = 'avatars',
  answers = 'answers',
  embeds = 'embeds',
  webinars = 'webinars',
  covers = 'covers',
  support = 'support',
}
