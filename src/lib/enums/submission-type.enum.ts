export enum SubmissionState {
  Main = 'main',
  Urgent = 'urgent',
  Outdated = 'outdated',
  Today = 'today',
}
