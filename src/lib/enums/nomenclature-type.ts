export enum NomenclatureType {
  COURSE = 'course',
  PROFESSION = 'profession',
}
