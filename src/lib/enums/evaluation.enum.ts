export enum Evaluation {
  VeryBad = 0,
  Bad = 1,
  Good = 2,
  VeryGood = 3,
}
