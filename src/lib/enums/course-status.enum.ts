export enum CourseStatus {
  draft = 'draft',
  new = 'new',
  created = 'created',
  uncompleted = 'uncompleted',
  completed = 'completed',
}
