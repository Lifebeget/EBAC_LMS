export enum ContentType {
  text = 'text',
  kaltura = 'kaltura',
  youtube = 'youtube',
  files = 'files',
  task = 'task',
  quiz = 'quiz',
  survey = 'survey',
  globalSurvey = 'globalSurvey',
}
