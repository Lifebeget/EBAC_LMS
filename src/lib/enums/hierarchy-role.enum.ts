export enum HierarchyRole {
  Teamlead = 'teamlead',
  Tutor = 'tutor',
}
