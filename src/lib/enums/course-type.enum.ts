export enum CourseType {
  course = 'course',
  profession = 'profession',
  project = 'project',
}
