export enum ResolveState {
  Main = 'main',
  Urgent = 'urgent',
  Outdated = 'outdated',
  Today = 'today',
}
