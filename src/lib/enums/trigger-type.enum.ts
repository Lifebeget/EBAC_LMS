export enum TriggerType {
  betaWelcome = 'betaWelcome', // DEPRECATED. beta welcome letter
  webinarCertCreated = 'webinarCertCreated', // created new cert for webinar
  courseCertCreated = 'courseCertCreated', // created new cert for webinar
  registration = 'registration', // registration',
  courseNotStarted = 'courseNotStarted', // new student (course wasn't started)",
  courseNotCompleted = 'courseNotCompleted', // new student (course wasn't complete yet)",
  courseFullyPublished = 'courseFullyPublished', // new student (course fully published)',
  submissionGradedSuccess = 'submissionGradedSuccess',
  submissionGradedFailed = 'submissionGradedFailed',
  submissionGradedQuestion = 'submissionGradedQuestion',
  forgotPassword = 'forgotPassword',
  tutorWelcome = 'tutorWelcome',
  supportMessageSent = 'supportMessageSent',
  enrollmentCreated = 'enrollmentCreated', // General Enrollment creation trigger
  // TODO: Remove unnecessary enrollment triggers
  onboardingCallTrigger = 'userEnrollmentCreated', // Should probably rename to onboardingCallTrigger but need migration
  userEnrollmentUpdated = 'userEnrollmentUpdated',
  userEnrollmentDeleted = 'userEnrollmentDeleted',
  userEnrollmentCreatedGivePresent = 'userEnrollmentCreatedGivePresent',
  userEnrollmentCheckPresent = 'userEnrollmentCreatedGivePresent', //Check active enrollements and validate present

  lectureProgressUpdated = 'lectureProgressUpdated',
  userProgressChanged = 'userProgressChanged', // Triggers on any progress update
  userCourseAlmostCompleted = 'userCourseAlmostCompleted',
  userCourseCompleted = 'userCourseCompleted',
  userCourseNotStarted = 'userCourseNotStarted', // different for delayed trigger
  userStoppedLearning = 'userStoppedLearning',
  contentKalturaUpdated = 'contentKalturaUpdated', // Kaltura block created
  forumMessageSent = 'forumMessageSent',
  englishFirst = 'englishFirst', // EnglishFirst promo
  userEnrolledTrial = 'userEnrolledTrial',
  moveTrialLead = 'moveTrialLead',
  // TODO remove in accordance to the 'Reactivation program'
  reactivateMoveLead = 'reactivateMoveLead',
  userProfileUpdated = 'userProfileUpdated',

  // not implemented
  newcomersSurvey = 'newcomersSurvey', // newcomers survey',
  firstModuleFinished = 'firstModuleFinished', // finished first module',
  taskMotivation = 'taskMotivation', // motivation to complete task',
  taskFeedback = 'taskFeedback', // task feedback',
  taskFeedbackWithCorrection = 'taskFeedbackWithCorrection', // task feedback with correction',
  lastModuleFinished = 'lastModuleFinished', // finished last module',
  midCourseSurvey = 'midCourseSurvey', // mid course survey',
  lastModuleSurvey = 'lastModuleSurvey', // last module survey',
  endCourseWithCertificate = 'endCourseWithCertificate', // end of course with sertificate',
  endCourseWithoutCertificate = 'endCourseWithoutCertificate', // end of course without sertificate',
  trialModuleFinished = 'trialModuleFinished', // finished trial module',
  modulePublished = 'modulePublished', // new module published',
  homeworkReminder = 'homeworkReminder', // homework reminder after 5 days',
  missing = 'missing', // student hadn't watched a video or completed a task in 2 weeks",
  noLoginAfterBuy = 'noLoginAfterBuy', // didn't login after buy for more than 7 days",
  graduationProject = 'graduationProject', // graduation project',
  demoCourseAccess = 'demoCourseAccess', // demo course access opened',
}
