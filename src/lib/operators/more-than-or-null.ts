import { Raw } from 'typeorm';

let uid = 0;
export function moreThanOrNull(value: number | Date) {
  const paramId = `mton${uid++}`;
  return Raw((pathAlias: string) => `(${pathAlias} >= :${paramId} OR ${pathAlias} IS NULL)`, {
    [paramId]: value,
  });
}
