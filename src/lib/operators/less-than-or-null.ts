import { Raw } from 'typeorm';

let uid = 0;
export function lessThanOrNull(value: number | Date) {
  const paramId = `lton${uid++}`;
  return Raw((pathAlias: string) => `(${pathAlias} <= :${paramId} OR ${pathAlias} IS NULL)`, {
    [paramId]: value,
  });
}
