export interface KalturaCallback {
  createdAt: number;
  description: string;
  duration: string;
  entry_id: string;
  fullDescription: string;
  height: number;
  owner: string;
  playerId: number;
  referenceId: string;
  return_type: string;
  size: string;
  tags: string;
  thumbnailUrl: string;
  title: string;
  url: string;
  width: string;
  ks: string;
}
