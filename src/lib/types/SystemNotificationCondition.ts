import { SystemRole } from '@lib/api/types';

export interface SystemNotificationCondition {
  roles: SystemRole[];
}
