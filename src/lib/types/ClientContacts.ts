export type ClientContacts = {
  name: string;
  phone: string;
  email?: string | null;
};
