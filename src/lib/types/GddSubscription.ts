export interface GDDSubscription {
  id: number;
  created: string;
  modified: string;
  options: any;
  client_id: number;
  source_id: number;
  schedule_event_id: number;
  subscription_date: string;
  subscription_date_brazil: string;
  subscription_date_utc: string;
  utm_content: string;
  utm_source: string;
  utm_medium: string;
  utm_campaign: string;
  utm_term: string;
  is_deleted: boolean;
  reference: any;
}
