import { ApiProperty } from '@nestjs/swagger';

export class KalturaMediaEntity {
  @ApiProperty()
  mediaType: number;

  @ApiProperty()
  conversionQuality: number;

  @ApiProperty()
  sourceType: string;

  @ApiProperty()
  sourceVersion: string;

  @ApiProperty()
  dataUrl: string;

  @ApiProperty()
  flavorParamsIds: string;

  @ApiProperty()
  plays: number;

  @ApiProperty()
  views: number;

  @ApiProperty()
  width: number;

  @ApiProperty()
  height: number;

  @ApiProperty()
  duration: number;

  @ApiProperty()
  msDuration: number;

  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  partnerId: number;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  creatorId: string;

  @ApiProperty()
  status: number;

  @ApiProperty()
  moderationStatus: number;

  @ApiProperty()
  moderationCount: number;

  @ApiProperty()
  type: number;

  @ApiProperty()
  createdAt: number;

  @ApiProperty()
  updatedAt: number;

  @ApiProperty()
  rank: number;

  @ApiProperty()
  totalRank: number;

  @ApiProperty()
  votes: number;

  @ApiProperty()
  downloadUrl: string;

  @ApiProperty()
  searchText: string;

  @ApiProperty()
  licenseType: number;

  @ApiProperty()
  version: number;

  @ApiProperty()
  thumbnailUrl: string;

  @ApiProperty()
  accessControlId: number;

  @ApiProperty()
  replacementStatus: number;

  @ApiProperty()
  partnerSortValue: number;

  @ApiProperty()
  conversionProfileId: number;

  @ApiProperty()
  rootEntryId: string;

  @ApiProperty()
  operationAttributes: Record<string, string>[];

  @ApiProperty()
  entitledUsersEdit: string;

  @ApiProperty()
  entitledUsersPublish: string;

  @ApiProperty()
  entitledUsersView: string;

  @ApiProperty()
  capabilities: string;

  @ApiProperty()
  displayInSearch: number;

  @ApiProperty()
  application: string;

  @ApiProperty()
  applicationVersion: string;

  @ApiProperty()
  blockAutoTranscript: boolean;

  @ApiProperty()
  objectType: string;
}
