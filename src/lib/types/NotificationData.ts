import { ForumMessages } from '@entities/forum-messages.entity';

export interface NotificationData {
  courseId?: string;
  submissionId?: string;
  lectureId?: string;
  courseSlug?: string;
  forumNewReply?: {
    message: ForumMessages;
    messageParent: ForumMessages;
  };
}
