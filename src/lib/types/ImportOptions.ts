export interface ImportOptions {
  pageSize: number;
  chill: boolean;
  noDownload: boolean;
}
