export type Product = {
  id: number;
  price: number;
  percent: number;
  shortName: string;
  name: string;
};
