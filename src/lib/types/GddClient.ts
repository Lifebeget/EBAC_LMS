export interface GddClient {
  id: number;
  email: string;
  phone: string;
  full_name: string;
  name: string;
  last_name: string;
  gender: any;
  birthday: any;
  id_fb: any;
  id_vk: any;
  id_tg: any;
  id_google: any;
  full_client_id: any;
  created: string;
  modified: string;
  options: any;
}
