export interface CmsTopicNomenclatureLink {
  nomenclature_id: number;
  event_schedule_topic_id: number;
  price_id: any;
  discount: any;
  created: string;
  modified: any;
  deleted: any;
  rev: number;
  product_price_id: any;
}
