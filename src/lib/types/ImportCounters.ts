export interface ImportCounter {
  created: number;
  updated: number;
  skipped: number;
}

export interface ImportCounters {
  assessments?: ImportCounter;
  questions?: ImportCounter;
  submissions?: ImportCounter;
  answers?: ImportCounter;
  users?: ImportCounter;
  modules?: ImportCounter;
  lectures?: ImportCounter;
  enrollments?: ImportCounter;
}

export const counterTemplate: ImportCounter = {
  created: 0,
  updated: 0,
  skipped: 0,
};

export const countersTemplate: ImportCounters = {
  assessments: { ...counterTemplate },
  questions: { ...counterTemplate },
  submissions: { ...counterTemplate },
  answers: { ...counterTemplate },
  users: { ...counterTemplate },
  modules: { ...counterTemplate },
  lectures: { ...counterTemplate },
  enrollments: { ...counterTemplate },
};
