import { PartialType } from '@nestjs/swagger';
import { User } from '../entities/user.entity';

export class UserWithPermissions extends PartialType(User) {
  permissions: string[];
}
