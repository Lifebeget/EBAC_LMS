import { TutorInfo } from './TutorInfo';

export interface TutorAttendance {
  course_name: string;
  course_version?: number;
  active_tutors: number | null;
  all_tutors: number | null;
  ungraded: number | null;
  undistributed: number | null;
  last_correction: Date | null;
  arr_tutors: TutorInfo[];
}
