export interface CmsTopic {
  id: number;
  event_schedule_id: number;
  num: number;
  title: string;
  description: string;
  preview_image: string;
  created: string;
  modified: string;
  deleted: any;
  rev: number;
  options: any;
  webinar_link: string;
  webinar_recording: string;
  discounter_course_link_regular: any;
  discounter_course_link_killer: any;
  speaker_text: string;
  speaker_name: string;
  speaker_image: string;
  discounter_course_link_regular_description: any;
  discounter_course_link_killer_description: any;
  speaker_from_day_one: boolean;
  speaker_subtitle: string;
  topic_date_utc: string;
  youtube_broadcast_id: number;
  call_to_actions: {
    title: string;
    subtitle: string;
    buttonUrl: string;
    buttonText: string;
  }[];
  active: boolean;
  chat_type: string;
  speaker_from_day_two: boolean;
  file_speaker_image: any;
  file_preview_image: any;
  nomenclature_links: {
    nomenclature_id: number;
    event_schedule_topic_id: number;
    price_id: number;
    discount: any;
    created: string;
    modified: string;
    deleted: any;
    rev: number;
    product_price_id: any;
  }[];
}
