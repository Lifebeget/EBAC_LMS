import { NodeList } from 'subtitle';

export interface KalturaCaptionAsset {
  captionParamsId: number;
  language: string;
  languageCode: string;
  isDefault: boolean;
  label: string;
  format: string;
  status: number;
  accuracy: number;
  displayOnPlayer: boolean;
  associatedTranscriptIds: string;
  id: string;
  entryId: string;
  partnerId: number;
  version: string;
  size: number;
  tags: string;
  fileExt: string;
  createdAt: number;
  updatedAt: number;
  description: string;
  sizeInBytes: string;
  objectType: string;
  subs?: NodeList;
}
