export interface TutorInfo {
  name: string;
  id: string;
  partial: boolean;
  pause: boolean;
  teamlead: boolean;
  last_correction: Date | null;
}
