export interface CRUD {
  create?(dto: any, ...params: any): Promise<any>;
  findAll?(query: any, ...params: any): Promise<any>;
  findOne?(id: any, ...params: any): Promise<any>;
  update?(id: string, dto: any, ...params: any): Promise<any>;
  remove?(id: string, ...params: any): Promise<any>;
}
