export interface External {
  findByExternalId(externalId: string): Promise<any>;
}
