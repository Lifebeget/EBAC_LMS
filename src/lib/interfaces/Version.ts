export interface Version {
  versionUpdate(dto: any, ...params: any): any;
  findVersions(id: string, dto: any, ...params: any): Promise<any>;
}
