import { Request } from 'express';

export function useCache(req: Request) {
  if (!req || !req.headers) {
    return true;
  }
  return req.headers['cache-control-ebac'] !== 'false';
}
