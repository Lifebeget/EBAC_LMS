import { Enrollment } from '@entities/enrollment.entity';
import { EnrollmentRightsEnum } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';

export function mapCoursePermissions(enrollments: Enrollment[]) {
  const m = {};
  enrollments.forEach(e => {
    if (!m[e.courseId]) {
      m[e.courseId] = {};
    }
    if (e.role == CourseRole.Student) {
      Object.keys(EnrollmentRightsEnum).forEach(k => (m[e.courseId][k] = m[e.courseId][k] || e.rights[k]));
    }
  });
  return m;
}
