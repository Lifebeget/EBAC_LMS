import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { QueryBannerDto } from './dto/query-banner.dto';
import { BannersService } from './banners.service';
import { Banner } from '../lib/entities/banner.entity';
import { Controller, Get, Post, Patch, Delete, Body, Param, Query, Req, ParseUUIDPipe } from '@nestjs/common';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Pagination } from '../pagination.decorator';
import { Request } from 'express';
import { PaginationDto } from '../pagination.dto';

@ApiTags('banners')
@ApiBearerAuth()
@Controller('banners')
export class BannersController {
  constructor(private bannersService: BannersService) {}

  @Get('/')
  @RequirePermissions(Permission.BANNERS_VIEW)
  getAllBanners(@Query() query: QueryBannerDto, @Pagination() paging: PaginationDto) {
    return this.bannersService.findAll(query, paging);
  }

  @Get('/active')
  getActiveBanners(@Req() req: Request): Promise<Banner[]> {
    const dto = new QueryBannerDto();
    if (req.query?.courseId) {
      // @ts-ignore - hide error with multiple types
      dto.courses = [{ id: req.query.courseId }];
    }

    // @ts-ignore - hide error with multiple types
    return this.bannersService.findActiveBanners(req.user, dto);
  }

  @Get('/:id')
  @RequirePermissions(Permission.BANNERS_VIEW)
  getBannerById(@Param('id', new ParseUUIDPipe()) id: string): Promise<Banner> {
    return this.bannersService.findOne(id);
  }

  @Post('/')
  @RequirePermissions(Permission.BANNERS_CREATE)
  createBanner(@Body() createBannerDto: CreateBannerDto): Promise<Banner> {
    return this.bannersService.create(createBannerDto);
  }

  @Patch('/:id')
  @RequirePermissions(Permission.BANNERS_MANAGE)
  updateBanner(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateBannerDto: UpdateBannerDto): Promise<Banner> {
    return this.bannersService.update(id, updateBannerDto);
  }

  @Delete('/:id')
  @RequirePermissions(Permission.BANNERS_MANAGE)
  deleteBanner(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.bannersService.remove(id);
  }

  @Post(':bannerId/course/:courseId')
  @RequirePermissions(Permission.BANNERS_MANAGE)
  attachCourse(@Param('bannerId', new ParseUUIDPipe()) bannerId: string, @Param('courseId', new ParseUUIDPipe()) courseId: string) {
    return this.bannersService.attachCourseToBanner(bannerId, courseId);
  }

  @Delete(':bannerId/course/:courseId')
  @RequirePermissions(Permission.BANNERS_MANAGE)
  detachCourse(@Param('bannerId', new ParseUUIDPipe()) bannerId: string, @Param('courseId', new ParseUUIDPipe()) courseId: string) {
    return this.bannersService.detachCourseFromBanner(bannerId, courseId);
  }
}
