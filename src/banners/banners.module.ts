import { Module, forwardRef } from '@nestjs/common';
import { Banner } from '@entities/banner.entity';
import { Course } from '@entities/course.entity';
import { BannersService } from 'src/banners/banners.service';
import { BannersController } from 'src/banners/banners.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SegmentsModule } from 'src/segments/segments.module';

@Module({
  imports: [TypeOrmModule.forFeature([Banner]), TypeOrmModule.forFeature([Course]), forwardRef(() => SegmentsModule)],
  controllers: [BannersController],
  providers: [BannersService],
  exports: [BannersService],
})
export class BannersModule {}
