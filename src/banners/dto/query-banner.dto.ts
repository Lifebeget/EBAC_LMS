import { PartialType, ApiProperty } from '@nestjs/swagger';
import { CreateBannerDto } from './create-banner.dto';

export class QueryBannerDto extends PartialType(CreateBannerDto) {}
