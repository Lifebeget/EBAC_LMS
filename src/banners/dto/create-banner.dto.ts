import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsBoolean, IsString, IsObject } from 'class-validator';
import { File } from 'src/lib/entities/file.entity';

export class CreateBannerDto {
  @ApiProperty({ description: 'Title' })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({ description: 'Slug' })
  slug?: string;

  @ApiProperty({ description: 'Place' })
  @IsNotEmpty()
  @IsString()
  place: string;

  @ApiProperty({ description: 'Google tag manager label' })
  labelGTM?: string;

  @ApiProperty({ description: 'Visibility label' })
  labelVisibility?: string;

  @ApiProperty({ description: 'Desktop image' })
  @IsNotEmpty()
  @IsObject()
  desktopImageFile: File;

  @ApiProperty({ description: 'Desktop image url' })
  desktopImageUrl?: string;

  @ApiProperty({ description: 'Mobile image' })
  @IsNotEmpty()
  @IsObject()
  mobileImageFile: File;

  @ApiProperty({ description: 'Mobile image url' })
  mobileImageUrl?: string;

  @ApiProperty({ description: 'Active from' })
  @IsNotEmpty()
  @IsString()
  activeFrom: Date;

  @ApiProperty({ description: 'Active to' })
  @IsNotEmpty()
  @IsString()
  activeTo: Date;

  @ApiProperty({ description: 'Link' })
  @IsNotEmpty()
  @IsString()
  href: string;

  @ApiProperty({ description: 'Display flag' })
  @IsNotEmpty()
  @IsBoolean()
  isActive: boolean;

  @ApiProperty({
    required: false,
    description: 'Related courses',
  })
  courses: Array<{ id: string }>;
}
