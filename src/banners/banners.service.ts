import { BadRequestException, forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { QueryBannerDto } from './dto/query-banner.dto';
import { Banner } from '@entities/banner.entity';
import { Course } from '@entities/course.entity';
import { User } from '@entities/user.entity';
import { PaginationDto } from '../pagination.dto';
import * as dayjs from 'dayjs';
import { SegmentsService } from 'src/segments/segments.service';
import { BannerPlace } from '@lib/types/enums/banner-place.enum';
import { String } from 'aws-sdk/clients/cloudhsm';

@Injectable()
export class BannersService {
  constructor(
    @InjectRepository(Banner)
    private readonly bannerRepository: Repository<Banner>,
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
    @Inject(forwardRef(() => SegmentsService))
    private segmentService: SegmentsService,
  ) {}

  bannersQueryBuilder(query: QueryBannerDto) {
    const repository = this.bannerRepository
      .createQueryBuilder('banner')
      .leftJoinAndSelect('banner.desktopImageFile', 'desktopFiles')
      .leftJoinAndSelect('banner.mobileImageFile', 'mobileFiles')
      .leftJoinAndSelect('banner.segment', 'segment')
      .leftJoin('banner.courses', 'courses')
      .addSelect(['courses.id', 'courses.title']);

    if (query?.title) {
      repository.where('banner.title ilike :title', { title: `%${query.title}%` });
    }
    if (query?.courses) {
      repository.innerJoin('banner.courses', 'course', 'course.id IN (:...courseId)', { courseId: query.courses.map(c => c.id) });
    }

    return repository;
  }

  public async findAll(query: QueryBannerDto, paging?: PaginationDto) {
    const queryBuilder = this.bannersQueryBuilder(query);
    const countPromise = queryBuilder.getCount();
    const itemsPromise = queryBuilder
      .skip((paging.page - 1) * paging.itemsPerPage)
      .take(paging.itemsPerPage)
      .orderBy('banner.created', 'DESC')
      .getMany();

    const [items, count] = await Promise.all([itemsPromise, countPromise]);

    return {
      results: items,
      meta: {
        ...paging,
        total: count,
      },
    };
  }

  public async findOne(id: string, query?: QueryBannerDto): Promise<Banner> {
    const queryBuilder = this.bannersQueryBuilder(query).where('banner.id = :id', { id });
    return queryBuilder.getOne();
  }

  public async findOneBySlug(slug: string, query?: QueryBannerDto): Promise<Banner> {
    const queryBuilder = this.bannersQueryBuilder(query).where('banner.slug = :slug', { slug });
    return queryBuilder.getOne();
  }

  public async findActiveBanners(user: User, query?: QueryBannerDto): Promise<Banner[]> {
    const queryBuilder = this.bannersQueryBuilder(query)
      .where('banner.isActive = true')
      .andWhere('banner.activeFrom < :now', { now: new Date() })
      .andWhere('banner.activeTo > :now', { now: new Date() })
      .orderBy('banner.created', 'ASC');
    const banners = await queryBuilder.getMany();

    const result = [];
    for (const banner of banners) {
      const segment = !!banner?.segment ? await this.segmentService.validateConditionForUser(banner?.segment.condition, user) : true;
      if (segment) {
        result.push(banner);
      }
    }

    return result;
  }

  public async create(createBannerDto: CreateBannerDto): Promise<Banner> {
    const existed = await this.findOneBySlug(createBannerDto.slug);
    if (existed) {
      throw new BadRequestException('Banner with this slug already exists');
    }
    if (!createBannerDto.href.match(/^([\w\d])+:\/\/([\w\d%?=/#-.$&])+$/)) {
      throw new BadRequestException('Invalid href');
    }
    if (!createBannerDto.desktopImageFile?.url) {
      throw new BadRequestException('Desktop image file has not url');
    }
    if (!createBannerDto.mobileImageFile?.url) {
      throw new BadRequestException('Mobile image file has not url');
    }
    if (createBannerDto.place !== BannerPlace.Course && createBannerDto.courses?.length) {
      createBannerDto.courses = [];
    }
    createBannerDto.labelGTM = createBannerDto.slug + '_' + dayjs(createBannerDto.activeFrom).format('MMMM_D_YYYY');
    createBannerDto.labelVisibility = createBannerDto.slug + '_' + dayjs(createBannerDto.activeFrom).format('MMMM_D_YYYY');
    createBannerDto.desktopImageUrl = createBannerDto.desktopImageFile.url;
    createBannerDto.mobileImageUrl = createBannerDto.mobileImageFile.url;
    const banner = this.bannerRepository.create(createBannerDto);
    return this.bannerRepository.save(banner).catch((err: Error) => {
      throw new BadRequestException(err.message);
    });
  }

  public async update(id: string, updateBannerDto: UpdateBannerDto): Promise<Banner> {
    const existed = await this.findOneBySlug(updateBannerDto.slug);
    if (existed && existed.id !== id) {
      throw new BadRequestException('Banner with this slug already exists');
    }
    if (updateBannerDto.href && !updateBannerDto.href.match(/^([\w\d])+:\/\/([\w\d%?=/#-.$&])+$/)) {
      throw new BadRequestException('Invalid href');
    }
    if (updateBannerDto.desktopImageFile && !updateBannerDto.desktopImageFile.url) {
      throw new BadRequestException('Desktop image file has not url');
    }
    if (updateBannerDto.mobileImageFile && !updateBannerDto.mobileImageFile.url) {
      throw new BadRequestException('Mobile image file has not url');
    }
    if (updateBannerDto.place !== BannerPlace.Course && updateBannerDto.courses?.length) {
      updateBannerDto.courses = [];
    }
    try {
      const banner = await this.bannerRepository.findOne(id);
      updateBannerDto.desktopImageUrl = updateBannerDto.desktopImageFile.url;
      updateBannerDto.mobileImageUrl = updateBannerDto.mobileImageFile.url;
      this.bannerRepository.merge(banner, updateBannerDto);
      return this.bannerRepository.save(banner);
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  public async remove(id: string) {
    try {
      return await this.bannerRepository.delete(id);
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  async attachCourseToBanner(bannerId: string, courseId: string) {
    const banner = await this.bannerRepository.findOne(bannerId, { relations: ['courses'] });
    if (!banner) {
      throw new BadRequestException('Banner not found');
    }

    if (banner.place !== BannerPlace.Course) {
      throw new BadRequestException('Banner is not course banner');
    }

    if (banner.courses.find(c => c.id === courseId)) {
      throw new BadRequestException('Course already attached to banner');
    }

    const course = await this.courseRepository.findOne(courseId);
    if (!course) {
      throw new BadRequestException('Course not found');
    }

    banner.courses.push(course);
    return this.bannerRepository.save(banner);
  }

  async detachCourseFromBanner(bannerId: string, courseId: string) {
    const banner = await this.bannerRepository.findOne(bannerId, { relations: ['courses'] });
    if (!banner) {
      throw new BadRequestException('Banner not found');
    }

    banner.courses = banner.courses.filter(c => c.id !== courseId);
    return this.bannerRepository.save(banner);
  }
}
