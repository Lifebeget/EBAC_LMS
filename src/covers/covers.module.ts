import { forwardRef, Module } from '@nestjs/common';
import { CoversService } from './covers.service';
import { CoversController } from './covers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cover } from '@entities/cover.entity';
import { FilesModule } from '../files/files.module';
import { KalturaModule } from '../kaltura/kaltura.module';
import { CoursesModule } from '../courses/courses.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cover]),
    forwardRef(() => FilesModule),
    forwardRef(() => KalturaModule),
    forwardRef(() => CoursesModule),
  ],
  controllers: [CoversController],
  providers: [
    CoversService,
  ],
  exports: [CoversService],
})
export class CoversModule {}
