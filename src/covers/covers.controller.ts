import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { CoversService } from './covers.service';
import { CreateCoverDto } from './dto/create-cover.dto';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';

@ApiTags('covers')
@ApiBearerAuth()
@Controller('covers')
export class CoversController {
  constructor(private readonly coversService: CoversService) {}

  @Post()
  @RequirePermissions(Permission.COVERS_CREATE)
  create(@Body() createCoversDto: CreateCoverDto[]) {
    return this.coversService.create(createCoversDto);
  }

  @RequirePermissions(Permission.COVERS_CREATE)
  @ApiOperation({ summary: 'Set thumbnails forcibly' })
  @Post('toThumbnailsForcibly')
  coversToThumbnailsForcibly() {
    return this.coversService.coversToThumbnails();
  }

  @Get()
  findAll() {
    return this.coversService.findAll();
  }

  @Get('/modules/:moduleId')
  findForModule(@Param('moduleId') moduleId: string) {
    return this.coversService.findForModule(moduleId);
  }

  @Get('/lectures/:lectureId')
  findForLecture(@Param('lectureId') lectureId: string) {
    return this.coversService.findForLecture(lectureId);
  }

  @Get('/courses/:courseId')
  findForCourse(@Param('courseId') courseId: string) {
    return this.coversService.findForCourse(courseId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.coversService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.coversService.remove(id);
  }
}
