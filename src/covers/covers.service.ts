import { forwardRef, Inject, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateCoverDto } from './dto/create-cover.dto';
import { UpdateCoverDto } from './dto/update-cover.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cover } from '@entities/cover.entity';
import { FilesService } from '../files/files.service';
import { LecturesService } from '../courses/lectures.service';
import { KalturaService } from '../kaltura/kaltura.service';
import { prepareDataForThumbnails } from '@lib/helpers/prepareDataForThumbnails';
import { Lecture } from '@entities/lecture.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class CoversService {
  constructor(
    @InjectRepository(Cover)
    private readonly coversRepository: Repository<Cover>,
    @Inject(forwardRef(() => LecturesService))
    private readonly lecturesService: LecturesService,
    private readonly filesService: FilesService,
    private readonly kalturaService: KalturaService,
  ) {}

  async create(createCoversDto: CreateCoverDto[]) {
    return await Promise.all(
      createCoversDto.map(cover => {
        return new Promise(async resolve => {
          const fileSavedInS3 = await this.filesService.storage.uploadBase64(cover.imageBase64, 'image/svg+xml', 'svg', 'covers');
          if (!fileSavedInS3) {
            return new InternalServerErrorException('File not uploaded to s3');
          }
          const fileSavedInDb = await this.filesService.create(fileSavedInS3);
          if (!fileSavedInDb) {
            return new InternalServerErrorException('File not uploaded to database');
          }
          await this.saveCover(cover);

          const updatedLecture = await this.lecturesService.update(cover.lectureId, {
            imageId: fileSavedInDb.id,
          });
          if (!updatedLecture) {
            return new InternalServerErrorException('Lecture has not been updated');
          }
          const lecture = await this.lecturesService.findOne(
            updatedLecture.id,
            true, // includeHidden
            true, // includeContent
          );
          resolve(lecture);
        });
      }),
    );
  }

  async saveCover(cover: CreateCoverDto) {
    const existingLectureCover = await this.findForLecture(cover.lectureId);
    const params = {
      templateId: cover.templateId,
      paletteId: cover.paletteId,
      moduleId: cover.moduleId,
      courseId: cover.courseId,
      moduleTitle: cover.moduleTitle,
      moduleNumber: cover.moduleNumber,
      lectureNumber: cover.lectureNumber,
      contentType: cover.contentType,
    };
    if (existingLectureCover) {
      await this.coversRepository.update(existingLectureCover.id, params);
    } else {
      await this.coversRepository.save({
        ...params,
        lectureId: cover.lectureId,
      });
    }
  }

  findAll() {
    return this.coversRepository.find();
  }

  async coversToThumbnails() {
    const covers = await this.findAll();
    const lectures: Lecture[] = [];
    for (const cover of covers) {
      const lecture = await this.lecturesService.findOne(cover.lectureId, true);
      if (lecture) {
        lectures.push(lecture);
      }
    }
    const preparedData = prepareDataForThumbnails(lectures);
    await this.kalturaService.setThumbnails(preparedData);
  }

  findForLecture(lectureId: string) {
    return this.coversRepository.createQueryBuilder('covers').where('covers.lectureId = :lectureId', { lectureId }).getOne();
  }
  findForModule(moduleId: string) {
    return this.coversRepository.find({
      where: { moduleId },
    });
  }
  findForCourse(courseId: string) {
    const qb = this.coversRepository.createQueryBuilder('cover').andWhere('cover.courseId = :courseId', { courseId });

    return qb.getMany();
  }

  @Transactional()
  findOne(id: string) {
    return this.coversRepository.findOne(id);
  }

  update(id: string, updateCoversDto: UpdateCoverDto) {
    return this.coversRepository.update(id, updateCoversDto);
  }

  remove(id: string) {
    return this.coversRepository.delete(id);
  }
}
