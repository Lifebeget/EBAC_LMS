import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateCoverDto {
  @ApiProperty({ description: 'Cover id', required: false })
  id?: string;

  @ApiProperty({ description: 'Image base64 string', required: false })
  imageBase64?: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Color palette id' })
  paletteId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'SVG template id' })
  templateId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lecture id' })
  lectureId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lecture module id' })
  moduleId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Course id' })
  courseId: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Module title' })
  moduleTitle: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Module number' })
  moduleNumber: number;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lecture number' })
  lectureNumber: number;

  @ApiProperty({ description: 'Lecture content type' })
  contentType?: string;
}
