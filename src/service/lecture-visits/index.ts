import { NestFactory } from '@nestjs/core';
import { appPromise, resolveApp } from 'src/app';
import { AppModule } from 'src/app.module';
import * as assert from 'assert';
import { LecturesService } from 'src/courses/lectures.service';
import axios_ from 'axios';
import { asyncBottleneck } from 'src/_util/async-bottleneck';
import { insertJson, query } from 'src/service/_common/clickhouse/db';
import { UsersService } from 'src/users/users.service';
import { createStats } from 'src/_util/stats';
import { Lecture } from 'src/lib/entities/lecture.entity';
import isitsohardtocorrectlydefinetstypes from 'axios-retry';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const axiosRetry: typeof isitsohardtocorrectlydefinetstypes = require('axios-retry');

const axios = axios_.create();

axiosRetry(axios, {
  retries: 4,
  retryDelay: axiosRetry.exponentialDelay,
  retryCondition: () => true,
});

const mbCreateTable = () => {
  return query(
    `CREATE TABLE IF NOT EXISTS ${process.env.CLICKHOUSE_DB_NAME}.lecture
(
  CourseId String NULL,
  CourseEadboxId String NULL,
  CourseTitle String NULL,
  LectureId String,
  LectureTitle String NULL,
  ModuleId String NULL,
  ModuleTitle String NULL,
  UserEadboxId String NULL,
  UserId String NULL,
  UserName String NULL,
  UserEmail String NULL,
  LoggedInIp String NULL,
  LoggedIn String NULL,
  LoggedOut String NULL,
  Duration String NULL
)
ENGINE = MergeTree() ORDER BY (LectureId)`,
  );
};

export const populateVisits = async (concurrency = 1) => {
  const appPromise = NestFactory.createApplicationContext(AppModule, {
    logger: false,
  });

  resolveApp(appPromise);
  const app = await appPromise;

  await mbCreateTable();

  assert(process.env.EADBOX_API_KEY, 'Env variable not found');

  const betterFn = asyncBottleneck(async (it: Lecture) => {
    try {
      return await processLecture(it);
    } catch (err) {
      console.warn(`failed to populate lecture ${it.id}`);
      console.warn(err);

      return Promise.resolve();
    }
  }, concurrency);

  const stats = createStats();
  stats.start();

  await Promise.all((await app.get(LecturesService).findAll()).map(betterFn).map(it => it.then(() => stats.counter++)));

  stats.stop();
};

async function processLecture(lecture: Await<ReturnType<LecturesService['findAll']>>[number]) {
  const app = await appPromise;
  const result = [];

  let pageCounter = 1;
  while (true) {
    const { data } = await axios.get<import('./types').LectureVisit[]>(
      `${process.env.EADBOX_API_URL}/admin/courses/${lecture.course.externalId}/lectures/${lecture.externalId}/lecture_visits`,
      {
        params: {
          auth_token: process.env.EADBOX_API_KEY,
          page: pageCounter++,
        },
      },
    );
    if (data.length === 0) {
      break;
    }
    result.push(...data);
  }

  const total = result.map(async it => {
    const user =
      it?.user?.user_id &&
      (await app
        .get(UsersService)
        .findAll({ itemsPerPage: 1, page: 1, showAll: false }, { externalId: it.user.user_id })
        .then(it => it.results[0]));

    return {
      CourseId: lecture?.course?.id,
      CourseEadboxId: lecture?.course?.externalId,
      CourseTitle: lecture?.course?.title,
      LectureId: lecture?.id,
      LectureTitle: lecture?.title,
      ModuleId: lecture?.module?.id,
      ModuleTitle: lecture?.module?.title,
      UserId: user?.id,
      UserEadboxId: it?.user?.user_id,
      UserName: user?.name,
      UserEmail: user?.email,
      LoggedInIp: it?.logged_in_ip,
      LoggedIn: it?.logged_in_at,
      LoggedOut: it?.logged_out_at,
      Duration: it?.duration + '',
    };
  });

  return insertJson('lecture', await Promise.all(total));
}
