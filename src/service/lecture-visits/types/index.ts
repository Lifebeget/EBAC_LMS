export interface LectureVisit {
  lecture_visit_id: string;
  open: null;
  logged_in_ip: string;
  logged_in_at: Date;
  logged_out_at: null;
  duration: number;
  user: User;
}

export interface User {
  user_id: string;
  user_slug: string;
  email: string;
  name: string;
}
