import { appPromise } from 'src/app';
import fetch from 'node-fetch';

export const query = async (str: string): Promise<any> => {
  const data = await fetch(`${process.env.CLICKHOUSE_DB_HOST}:${process.env.CLICKHOUSE_DB_PORT}`, {
    method: 'POST',
    body: str,
    headers: {
      'X-ClickHouse-User': process.env.CLICKHOUSE_DB_USER,
      'X-ClickHouse-Key': process.env.CLICKHOUSE_DB_PASSWORD,
    },
  });

  if (!data.ok) {
    throw new Error(await data.text());
  }
  const values = await data.text();
  return JSON.parse(values);
};

export const insertJson = async (table: string, values: Record<string, any> | Record<string, any>[]) => {
  await appPromise;
  const insertData = Array.isArray(values) ? values.map(it => JSON.stringify(it)).join('\n') : [JSON.stringify(values)];

  if (insertData.length === 0) {
    return false;
  }

  const data = await fetch(`${process.env.CLICKHOUSE_DB_HOST}:${process.env.CLICKHOUSE_DB_PORT}`, {
    method: 'POST',
    body: `INSERT INTO ${process.env.CLICKHOUSE_DB_NAME}.${table} FORMAT JSONEachRow ${insertData}`,
    headers: {
      'X-ClickHouse-User': process.env.CLICKHOUSE_DB_USER,
      'X-ClickHouse-Key': process.env.CLICKHOUSE_DB_PASSWORD,
    },
  });

  if (!data.ok) {
    throw new Error(await data.text());
  }
};

export const toClickhouseTime = (arg: Date) => ~~(arg.getTime() / 1000);
