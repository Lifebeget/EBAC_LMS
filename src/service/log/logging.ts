import { SetMetadata, INestApplicationContext } from '@nestjs/common';
import * as assert from 'assert';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { CoursesService } from 'src/courses/courses.service';
import { LogEntry } from 'src/logger';

interface PopulatedLogT {
  course?: {
    title?: string;
    id?: string;
  };
  module?: {
    title?: string;
    id?: string;
  };
  lecture?: {
    title?: string;
    id?: string;
  };
  tutor?: {
    id?: string;
    name?: string;
  };
  student?: {
    id?: string;
    name?: string;
  };
}

export const logTagToHandler: Record<string, (arg1: LogEntry, arg2: INestApplicationContext) => Promise<PopulatedLogT>> = {};

export const logTagKey = Symbol('LogTagKey');

export const SetLogTag = (tagName: string) => {
  assert(tagName);
  return SetMetadata(logTagKey, tagName);
};

export function PopulateLogWith<T>(handler: (arg1: LogEntry<T>, arg2: INestApplicationContext) => Promise<PopulatedLogT>) {
  return function (target: any, propertyName: string, _descriptor: PropertyDescriptor) {
    const logTag = Reflect.getMetadata(logTagKey, target[propertyName]);
    assert(logTag, `${logTagKey.toString()} needs to be defined. Verify your decorator location.`);
    assert(!logTagToHandler[logTag], `Handler for ${logTag} tag was alredy defined`);
    logTagToHandler[logTag] = handler;
  };
}

export const populateLogBySubmission = async function (submissionId: string, context: INestApplicationContext): Promise<PopulatedLogT> {
  const submissionsService = context.get(SubmissionsService);
  const submission = await submissionsService
    .findAll(
      {
        itemsPerPage: 1,
        page: 1,
        showAll: false,
      },
      { submissionId },
    )
    .then(it => it.results[0]);

  return {
    tutor: {
      id: submission?.tutor?.id,
      name: submission?.tutor?.name,
    },
    student: {
      id: submission?.user?.id,
      name: submission?.user?.name,
    },
    lecture: {
      id: submission?.assessment?.lecture?.id,
      title: submission?.assessment?.lecture?.title,
    },
    course: {
      id: submission?.assessment?.lecture?.course?.id,
      title: submission?.assessment?.lecture?.course?.title,
    },
    module: {
      id: submission?.assessment?.lecture?.module?.id,
      title: submission?.assessment?.lecture?.module?.title,
    },
  };
};

export const populateLogByAssessment = async function (assessmentId: string, context: INestApplicationContext): Promise<PopulatedLogT> {
  const assessmentsService = context.get(AssessmentsService);
  const assessment = await assessmentsService.findOne(assessmentId);
  const lecture = assessment.lecture;

  return {
    course: {
      id: lecture?.course?.id,
      title: lecture?.course?.title,
    },
    lecture: {
      id: lecture?.id,
      title: lecture?.title,
    },
    module: {
      id: lecture?.module?.id,
      title: lecture?.module?.title,
    },
  };
};

export const populateLogByCourse = async function (courseId: string, context: INestApplicationContext): Promise<PopulatedLogT> {
  const courseService = context.get(CoursesService);
  const course = await courseService.findOne(courseId);

  return {
    course: {
      id: course?.id,
      title: course?.title,
    },
  };
};
