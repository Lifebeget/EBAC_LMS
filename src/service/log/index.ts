import { NestFactory } from '@nestjs/core';
import axios from 'axios';
import * as dayjs from 'dayjs';
import * as fss from 'fs';
import fetch from 'node-fetch';
import * as readline from 'readline';
import { resolveApp } from 'src/app';
import { AppModule } from 'src/app.module';
import { LogEntry } from 'src/logger';
import { logTagToHandler } from 'src/service/log/logging';
import { insertJson, query, toClickhouseTime } from 'src/service/_common/clickhouse/db';
import { UsersService } from 'src/users/users.service';
import { createStats } from 'src/_util/stats';
const fs = fss.promises;

let app = null;

export async function initApp() {
  app = await NestFactory.createApplicationContext(AppModule, { logger: false });
  resolveApp(app);
}

async function onLogHandler(data_: string) {
  const context = await app;
  const data = data_.toString();

  const parsed =
    /(?<time>[^\t]+)\t(?<traceId>[^\t]+)?\t(?<ip>[^\t]+)?\t(?<method>[^\t]+)?\t(?<url>[^\t]+)?\t(:?(?<userId>[^\t]+))?\t(:?(?<body>[^\t]+))?(:?\t(?<handlerTag>[^\t]+))?/.exec(
      data,
    )?.groups;

  if (!parsed) {
    console.error('Failed to parse corrupted log:\n', data);
    return;
  }

  const body = await (async () => {
    try {
      return parsed.body ? JSON.parse(parsed.body) : undefined;
    } catch (err) {
      // await axios.post(`${process.env.API_URL}/signal/save-log`, {
      //   challenge: process.env.INTERAPP_SECRET,
      //   type: LogSignalEnum.PopulationError,
      //   info: {
      //     message: 'Failed to JSON parse log body',
      //     rawLog: data,
      //     parsedLog: parsed,
      //   },
      // });
      // throw err;
      console.error('JSON Body parse error: ' + parsed.body);
      return { message: 'JSON Body parse error' };
    }
  })();

  const result: LogEntry = {
    traceId: parsed.traceId,
    body,
    ip: parsed.ip,
    method: parsed.method as MethodT,
    time: new Date(+parsed.time),
    url: parsed.url,
    userId: parsed.userId,
    handlerTag: parsed.handlerTag,
  };

  const populated = logTagToHandler[result.handlerTag] ? await logTagToHandler[result.handlerTag](result, context) : {};

  const userName =
    result.userId &&
    (await context
      .get(UsersService)
      .findOne(result.userId)
      .then(it => it?.name));

  const total = {
    UserName: userName,
    Body: JSON.stringify(result.body),
    Ip: result.ip,
    Method: result.method,
    Time: toClickhouseTime(result.time),
    Url: result.url,
    UserId: result.userId,
    HandlerTag: result.handlerTag,
    CourseId: populated?.course?.id,
    CourseTitle: populated?.course?.title,
    LectureId: populated?.lecture?.id,
    LectureTitle: populated?.lecture?.title,
    ModuleId: populated?.module?.id,
    ModuleTitle: populated?.module?.title,
    StudentId: populated?.student?.id,
    StudenName: populated?.student?.name,
    TutorId: populated?.tutor?.id,
    TutorName: populated?.tutor?.name,
  };

  return await insertJson('log', total);
}

const mbCreateLogTable = () => {
  return query(
    `CREATE TABLE IF NOT EXISTS ${process.env.CLICKHOUSE_DB_NAME}.log
    (
      Time DateTime('America/Sao_Paulo'),
      CourseId String NULL,
      CourseTitle String NULL,
      LectureId String NULL,
      LectureTitle String NULL,
      ModuleId String NULL,
      ModuleTitle String NULL,
      StudentId String NULL,
      StudenName String NULL,
      TutorId String NULL,
      TutorName String NULL,
      UserName String NULL,
      Body String NULL,
      Ip String NULL,
      Method String NULL,
      Url String NULL,
      UserId String NULL,
      HandlerTag String NULL
    )
    ENGINE = MergeTree() ORDER BY (Time)`,
  );
};

export const populateServerLog = async (serverUrl: string) => {
  await mbCreateLogTable();

  const logFileStream = await fetch(serverUrl + '/log/file', {
    headers: { 'X-Custom-Auth': process.env.INTERAPP_SECRET },
  });

  if (logFileStream.status === 409) {
    console.log(`${serverUrl} didnt provide logfile. Safe - skipping`);
    console.log(await logFileStream.text());

    return;
  }

  if (!logFileStream.ok) {
    console.warn(`${serverUrl} raised unknown error`);
    throw new Error(await logFileStream.text());
  }

  const lineByLineLogStream = readline.createInterface({
    input: logFileStream.body,
  });

  const currDate = dayjs().format('YYYY-MM-DDTHH:mm:ss');

  const stats = createStats();
  stats.start();

  for await (const line of lineByLineLogStream) {
    stats.counter++;

    if (process.env.LOG_DIR_PERSIST) {
      await fs.appendFile(`${process.env.LOG_DIR_PERSIST}/${currDate}.log`, line + '\n').catch(err => {
        console.warn(`Failed to save raw logfile\n`, err);
      });
    }

    await onLogHandler(line).catch(err => {
      console.warn(`Log population failed\n> ${line}\n`, err);
    });
  }

  stats.stop();

  await axios
    .post(serverUrl + '/signal/log-population-finish', {
      challenge: process.env.INTERAPP_SECRET,
    })
    .catch(err => {
      console.warn(`Failed to notify server about import finish\n`, err);
    });
};
