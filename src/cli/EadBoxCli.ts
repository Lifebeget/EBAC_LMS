import { EadBox } from '../integration/eadBox';

import { INestApplicationContext } from '@nestjs/common';
import { ImportInfoService } from 'src/import-info/import-info.service';
import { ImportType } from 'src/lib/enums/import-type.enum';
import * as chalk from 'chalk';
import { countersTemplate, ImportCounters } from 'src/lib/types/ImportCounters';
import { StartPermission } from 'src/lib/enums/start-permission.enum';
import { sleep } from 'src/helpers';

export class EadBoxCli {
  private eadBox: EadBox;
  private importInfoService: ImportInfoService;

  constructor(private readonly context: INestApplicationContext) {
    this.eadBox = new EadBox(this.context);
    this.importInfoService = this.context.get(ImportInfoService);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async checkAndImport(importType: ImportType, importMethod: () => Promise<ImportCounters>) {
    const startPermission = await this.importInfoService.getStartPermission(importType);

    if (startPermission === StartPermission.Deny) {
      console.log(chalk.red(`Execution denied (type: ${importType})`));
      return countersTemplate;
    }

    if (startPermission === StartPermission.Wait) {
      console.log(chalk.yellow(`Waiting for start import (type: ${importType})`));
      await sleep(10000);
      return this.checkAndImport(importType, importMethod);
    }
    return await this.runImport(importType, importMethod);
  }

  async importComments(arg: { concurrency: string }) {
    console.log('Starting comments import');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importComments(concurrency);
  }

  async importForums(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting forum import');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importForums(concurrency, !arg.nodebug);
  }

  async importSurveys(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting surveys import');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importSurveys(concurrency, !arg.nodebug);
  }

  async importModules(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting modules import');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importModules(concurrency, !arg.nodebug);
  }

  async importAvatars(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting profiles import');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importAvatars(concurrency, !arg.nodebug);
  }

  async importUploadS3(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting upload files');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importUploadS3(concurrency, !arg.nodebug);
  }

  async importDeleteFiles(arg: { concurrency: string; nodebug: string }) {
    console.log('Starting delete files');
    const concurrency = (arg.concurrency && +arg.concurrency) || 3;
    return this.eadBox.importDeleteFiles(concurrency, !arg.nodebug);
  }

  async runImport(importType: ImportType, importMethod: () => Promise<ImportCounters>) {
    const importInfo = await this.importInfoService.saveStart(importType);

    try {
      process.on('SIGINT', async () => {
        console.log(chalk.red('Interrupt... Saving importInfo'));
        await this.importInfoService.saveInterrupt({ ...importInfo });
        process.exit();
      });

      const counters = await importMethod();
      await this.importInfoService.saveCompletion({ ...importInfo, counters });
    } catch (er) {
      console.error(er);
      await this.importInfoService.saveFail(importInfo);
    }
  }

  async importAll(concurrency: number, perpage: number, startDate: string) {
    console.log('Starting full import');
    return this.checkAndImport(ImportType.Full, () => this.eadBox.fullImport(concurrency, perpage, startDate));
  }

  async importUsers(options) {
    console.log('Starting users import');
    return this.checkAndImport(ImportType.Users, () =>
      this.eadBox.import({
        downloadUsers: true,
        downloadEnrollments: options?.locked !== 'true',
        perpage: options?.perpage,
        locked: options?.locked === 'true',
        startDate: options?.startDate,
      }),
    );
  }

  async import(options) {
    console.log('Starting import');
    return this.checkAndImport(ImportType.Users, () => this.eadBox.import(options));
  }

  async importTest() {
    return this.eadBox.import({ pagesLimit: 10, lecturesLimit: 100 });
  }

  async importAssesments({ mutex, fast, course, concurrency, log, validate }) {
    if (mutex) {
      return this.checkAndImport(ImportType.Mutex, () => this.eadBox.importMutex({ concurrency: +concurrency }));
    }

    if (fast) {
      return this.checkAndImport(ImportType.Fast, () => this.eadBox.importFast({ concurrency: +concurrency, log, validate }));
    }

    if (course) {
      return this.checkAndImport(ImportType.FastCourse, () => this.eadBox.importFast({ course, concurrency: +concurrency }));
    }
  }
}
