import { INestApplicationContext } from '@nestjs/common';
import { TriggerService } from 'src/trigger/trigger.service';

export class TriggerExecuter {
  private triggerService: TriggerService;

  constructor(private readonly context: INestApplicationContext) {
    this.triggerService = context.get(TriggerService);
  }

  async execute(count) {
    await this.triggerService.executeTriggers(count);
  }
}
