import { INestApplicationContext } from '@nestjs/common';
import * as chalk from 'chalk';
import * as dayjs from 'dayjs';
import { StatServiceManager } from 'src/stat/stat-service-manager';

export interface StatCalcOptions {
  dt?: string;
  yesterday?: boolean;
  removeFirst?: boolean;
}
export class StatCli {
  private readonly service: StatServiceManager;
  constructor(private readonly context: INestApplicationContext) {
    this.service = this.context.get(StatServiceManager) as StatServiceManager;
  }

  async allSubmissions(options: StatCalcOptions) {
    await this.submissionsByCourses(options);
    await this.submissionsByTutors(options);
  }

  async submissionsByCourses({ dt = null, yesterday = false, removeFirst = false }: StatCalcOptions) {
    if (removeFirst) {
      this.service.submissionsByCourse.removeForDate(dt);
    }
    await this.calculate(this.service.submissionsByCourse, 'submissions by courses', {
      dt,
      yesterday,
    });
  }

  async submissionsByTutors({ dt = null, yesterday = false, removeFirst = false }: StatCalcOptions) {
    if (removeFirst) {
      this.service.submissionsByTutor.removeForDate(dt);
    }
    await this.calculate(this.service.submissionsByTutor, 'submissions by tutors', {
      dt,
      yesterday,
      removeFirst,
    });
  }

  async allForum(options: StatCalcOptions) {
    await this.forumByUsers(options);
    await this.forumByCourses(options);
  }

  async forumByCourses({ dt = null, yesterday = false, removeFirst = false }: StatCalcOptions) {
    if (removeFirst) {
      this.service.forumMessagesByCourse.removeForDate(dt);
    }

    const params = {
      dt,
      yesterday,
      removeFirst,
    };

    await this.calculate(this.service.forumMessagesByCourse, 'forum messages by courses', params);

    await this.calculate(this.service.forumThreadsByCourse, 'forum threads by courses', params);
  }

  async forumByUsers({ dt = null, yesterday = false, removeFirst = false }: StatCalcOptions) {
    if (removeFirst) {
      this.service.forumMessagesByUser.removeForDate(dt);
    }

    const params = {
      dt,
      yesterday,
      removeFirst,
    };

    await this.calculate(this.service.forumMessagesByUser, 'forum messages by users', params);

    await this.calculate(this.service.forumThreadsByUser, 'forum threads by users', params);
  }

  private async calculate(service: any, title: string, { dt = null, yesterday = false, removeFirst = false }) {
    console.log(chalk.blue(`Calculating and saving statistics for ${title}`));
    const savedStat = await service.save(this.getStatDate({ dt, yesterday }), removeFirst);
    console.log(chalk.green('DONE: '), savedStat);
  }

  private getStatDate({ dt = null, yesterday = false }: StatCalcOptions) {
    // const date = dayjs(dt ?? new Date()).utc(true);
    const date = dayjs(dt ?? new Date());

    const result = yesterday ? date.subtract(1, 'day').startOf('day') : date;

    return result.subtract(new Date().getTimezoneOffset(), 'minutes').toISOString().slice(0, -1);
  }
}
