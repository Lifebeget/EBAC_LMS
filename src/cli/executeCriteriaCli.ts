import { INestApplicationContext } from '@nestjs/common';
import { Assessment } from '@entities/assessment.entity';
import { AssessmentCriteriaService } from '../criteria-based-assessments/assessment-criteria.service';
import { AssessmentCriteriaCategoryService } from '../criteria-based-assessments/assessment-criteria-category.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CriterionSelector } from '@lib/types/enums/criterion-selector.enum';
import cheerio from 'cheerio';

export class CriteriaExecuter {
  private assessmentCriteriaService: AssessmentCriteriaService;
  private assessmentCriteriaCategoryService: AssessmentCriteriaCategoryService;
  private assessmentsRepository: Repository<Assessment>;

  constructor(private readonly context: INestApplicationContext) {
    this.assessmentCriteriaCategoryService = context.get(AssessmentCriteriaCategoryService);
    this.assessmentCriteriaService = context.get(AssessmentCriteriaService);
    this.assessmentsRepository = context.get(getRepositoryToken(Assessment));
  }

  async execute() {
    let categories = await this.getCategory();
    if (!categories.length) {
      categories = await this.createCategory();
    }
    categories.sort((a, b) => -1 * a.display.localeCompare(b.display));

    const tutor_messages = await this.assessmentsRepository
      .createQueryBuilder('assessment')
      .select('assessment.tutorMessage')
      .leftJoin('assessment.questions', 'questions')
      .addSelect('questions.id')
      .where('assessment.tutorMessage IS NOT NULL AND questions.id IS NOT NULL')
      .getRawMany();

    for (const tm of tutor_messages) {
      const $ = cheerio.load(tm.assessment_tutor_message, null, false);
      const questionId = tm.questions_id;

      let iteration_max = 1;

      if (
        tm.assessment_tutor_message.indexOf('Tarefa 1') !== -1 &&
        tm.assessment_tutor_message.indexOf('Tarefa 2') !== -1 &&
        tm.assessment_tutor_message.indexOf('Tarefa 3') !== -1
      ) {
        // если задачи пронумеровны, то скорее всего это больше ul-ок
        iteration_max = 3;
      } else if (tm.assessment_tutor_message.indexOf('Tarefa 1') !== -1 && tm.assessment_tutor_message.indexOf('Tarefa 2') !== -1) {
        // если задачи пронумеровны, то скорее всего это больше ul-ок
        iteration_max = 2;
      }

      let iteration = 0;
      let cat_iteration = iteration;
      for (const ul of $('ul')) {
        if (iteration > iteration_max) break;
        let sort = 100;
        cat_iteration = iteration;
        if (iteration_max > 1) {
          // если итераций больше 2-х и это последняя итерация, то считаем, что это вторая категория, иначе первая
          if (iteration === iteration_max) cat_iteration = 1;
          else cat_iteration = 0;
        }
        const categoryId = categories[cat_iteration].id;

        for (const li of $('li', ul)) {
          const title = $(li).text();

          // если заголовок в li-шке, пропускаем
          if (
            (title.indexOf('Tarefas') !== -1 && title.indexOf('Avaliação') !== -1) ||
            (title.indexOf('Régua') !== -1 && title.indexOf('criatividade') !== -1)
          )
            continue;
          await this.createCreteria({
            categoryId,
            title,
            questionId,
            sort,
          });
          sort += 100;
        }
        iteration++;
      }
      console.log('questionId INSERTED ' + questionId);
    }
  }

  async getCategory() {
    return await this.assessmentCriteriaCategoryService.findAll({
      active: true,
    });
  }

  async createCreteria(data) {
    await this.assessmentCriteriaService.create({ ...data, weight: 1 });
  }

  async createCategory() {
    const cat1 = await this.assessmentCriteriaCategoryService.create({
      title: 'Tarefas - Avaliação técnica',
      defaultWeight: 1,
      display: CriterionSelector.Threebox,
      active: true,
      gradable: true,
    });
    const cat2 = await this.assessmentCriteriaCategoryService.create({
      title: 'Régua da criatividade',
      defaultWeight: 1,
      display: CriterionSelector.Checkbox,
      active: true,
      gradable: false,
    });

    return [cat1, cat2];
  }
}
