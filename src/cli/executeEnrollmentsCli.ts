import { INestApplicationContext } from '@nestjs/common';
import { EnrollmentsService } from 'src/courses/enrollments.service';

export class EnrollmentsExecuter {
  private enrollmentsService: EnrollmentsService;

  constructor(private readonly context: INestApplicationContext) {
    this.enrollmentsService = context.get(EnrollmentsService);
  }

  async execute() {
    await this.enrollmentsService.enrollPlanoDeCarreira();
  }
}
