import { NestFactory } from '@nestjs/core';

import { AppModule } from '../app.module';
import { printheading } from './cli.helper';

import * as Confirm from 'prompt-confirm';
import { promises as fs } from 'fs';
import { FilesModule } from 'src/files/files.module';
import { FilesService } from 'src/files/files.service';
import { Client } from 'pg';

export * from './cli.helper';

const purgeFiles = async app => {
  process.stdout.write('Cleaning up files...');
  await fs.rmdir('upload', { recursive: true });
  process.stdout.write('Done\n');

  process.stdout.write('Cleaning up files table...');
  const filesService = app.select(FilesModule).get(FilesService, { strict: true });
  await filesService.purgeAll();
  process.stdout.write('Done\n');
};

const purgeTables = async () => {
  process.stdout.write('Truncating tables...');
  const client = new Client({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
  });
  await client.connect();
  await client.query(
    `TRUNCATE TABLE
    answer
    assessment
    course
    course_statistic
    enrollment
    file
    import_info
    lecture
    module
    question
    "role"
    submission
    submission_statistic
    tutor_statistic
    "user"
    user_modules_module
    `,
  );
  await client.end();
  process.stdout.write('Done\n');
};

export const bootstrap = async () => {
  const app = await NestFactory.createApplicationContext(AppModule);

  printheading('This script will purge the database and clean out the files');

  const prompt = new Confirm('Are you sure?');
  const answer = await prompt.run();

  if (!answer) {
    await app.close();
    process.exit(0);
  }

  await purgeFiles(app);
  await purgeTables();

  await app.close();
};
