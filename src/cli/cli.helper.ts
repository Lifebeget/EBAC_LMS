export function prettyprint(obj: any) {
  console.log(JSON.stringify(obj, null, 2));
}

export function printheading(text: string) {
  console.log(`===== ${text} =====`);
}
