import { NestFactory } from '@nestjs/core';
import { AppModule } from 'src/app.module';

import { EadBoxCli } from './EadBoxCli';
import { StatCli } from './StatCli';

import { ImportWebinars } from '../webinar/webinar.import';
import { OneTimeCommentToS3 } from './oneTime/OneTimeCommentToS3';
import { OneTimeImportVideo } from './oneTime/OneTimeImportVideo';
import { OneTimeImportVideoS3 } from './oneTime/OneTimeImportVideoS3';
import { OneTimeAvatarToS3 } from './oneTime/OneTimeAvatarToS3';
import { OneTimeMigrateResizeS3 } from './oneTime/OneTimeMigrateResizeS3';
import { OneTimeFillLectureProgress } from './oneTime/OneTimeFillLectureProgress';
import { FixDuplicateAssessments } from './oneTime/FixDuplicateAssessments';
import { ImportProgress } from './oneTime/importProgress';
import { TriggerExecuter } from './executeTriggersCli';
import { SubmissionsExecuter } from './executeSubmissionsCli';
import { EnrollmentsExecuter } from './executeEnrollmentsCli';
import { CriteriaExecuter } from './executeCriteriaCli';
import { UpdateViews } from './oneTime/UpdateViews';
import { ImportTopics } from '../topic/topic.import';
import { ImportSources } from '../source/source.import';
import { ImportClients } from '../clients/clients.import';
import { ImportSubscriptions } from '../subscriptions/subscriptions.import';
import { ImportNomenclatures } from '../nomenclature/nomenclature.import';
import { ImportTopicsNomenclaturesLinks } from '../topic-nomenclature-links/topic-nomenclature-links.import';
import { initializeTransactionalContext, patchTypeORMRepositoryWithBaseRepository } from 'typeorm-transactional-cls-hooked';
export * from './cli.helper';
export * from './EadBoxCli';
export * from './StatCli';

export const initCli = async () => {
  initializeTransactionalContext();
  patchTypeORMRepositoryWithBaseRepository();

  const context = await NestFactory.createApplicationContext(AppModule);

  const eadBoxCli = new EadBoxCli(context);
  const statCli = new StatCli(context);
  const importWebinars = new ImportWebinars(context);
  const importSources = new ImportSources(context);
  const importTopics = new ImportTopics(context);
  const importNomenclatures = new ImportNomenclatures(context);
  const importTopicsNomenclaturesLinks = new ImportTopicsNomenclaturesLinks(context);
  const importClients = new ImportClients(context);
  const importSubscriptions = new ImportSubscriptions(context);

  const triggerExecuter = new TriggerExecuter(context);
  const submissionsExecuter = new SubmissionsExecuter(context);
  const enrollmentsExecuter = new EnrollmentsExecuter(context);
  const criteriaExecuter = new CriteriaExecuter(context);

  const oneTimeCommentToS3 = new OneTimeCommentToS3(context);
  const oneTimeImportVideo = new OneTimeImportVideo(context);
  const oneTimeImportVideoS3 = new OneTimeImportVideoS3(context);
  const oneTimeAvatarToS3 = new OneTimeAvatarToS3(context);

  const oneTimeMigrateResizeS3 = new OneTimeMigrateResizeS3(context);
  const oneTimeFillLectureProgress = new OneTimeFillLectureProgress(context);
  const fixDuplicateAssessments = new FixDuplicateAssessments(context);
  const importProgress = new ImportProgress(context);
  const updateViews = new UpdateViews(context);

  return {
    eadBoxCli,
    statCli,
    importWebinars,
    importSources,
    importTopics,
    importNomenclatures,
    importTopicsNomenclaturesLinks,
    importClients,
    importSubscriptions,
    triggerExecuter,
    oneTimeCommentToS3,
    oneTimeAvatarToS3,
    oneTimeMigrateResizeS3,
    oneTimeFillLectureProgress,
    oneTimeImportVideo,
    oneTimeImportVideoS3,
    fixDuplicateAssessments,
    importProgress,
    submissionsExecuter,
    enrollmentsExecuter,
    criteriaExecuter,
    updateViews,
  };
};
