import { INestApplicationContext } from '@nestjs/common';
import { SubmissionsService } from 'src/assessments/submissions.service';

export class SubmissionsExecuter {
  private submissionsService: SubmissionsService;

  constructor(private readonly context: INestApplicationContext) {
    this.submissionsService = context.get(SubmissionsService);
  }

  async execute() {
    await this.submissionsService.fixSubmissions();
  }

  async tutorPause() {
    await this.submissionsService.tutorsPauseIfIdle();
    await this.submissionsService.distribute();
  }
}
