import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { spawn } from 'child_process';
import { mkdirSync, writeFileSync } from 'fs';
import { rm } from 'fs/promises';
import * as m3u8Parser from 'm3u8-parser';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { ContentService } from 'src/courses/content.service';
import { CoursesService } from 'src/courses/courses.service';
import { FilesStorageS3Service } from 'src/files/files.storage.s3.service';
import promisePool from 'src/_util/async-bottleneck';
import { Daum, Root } from './types/OneTimeImportVideoTypes';
import { PlaylistType } from './types/PlaylistTypes';

type CounterType = {
  coconutCount: number;
  sparkCount: number;
  noLecture: number;
  cantGetHTTP: any[];
  cantConvert: any[];
  cantUpload: any[];
};

const DOWNLOAD_RETRY_COUNT = 3;

export class OneTimeImportVideoS3 {
  private eadboxSessionCookie: string;
  private submissionsService: SubmissionsService;
  private contentService: ContentService;
  private coursesService: CoursesService;
  private FilesStorageS3Service: FilesStorageS3Service;
  private counter: CounterType;
  private hideOutput: boolean;
  constructor(private readonly context: INestApplicationContext) {
    this.submissionsService = context.get(SubmissionsService);
    this.contentService = context.get(ContentService);
    this.coursesService = context.get(CoursesService);
    this.FilesStorageS3Service = this.context.get(FilesStorageS3Service);
    this.counter = {
      coconutCount: 0,
      sparkCount: 0,
      noLecture: 0,
      cantGetHTTP: [],
      cantConvert: [],
      cantUpload: [],
    };
    this.hideOutput = false;
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
    console.log(this.eadboxSessionCookie);
  }

  async downloadCourseModules(externalId): Promise<Root> {
    const url = `https://ebac-online.eadbox.com.br/ng/api/v2/teacher/courses/${externalId}/modules?&page[size]=100000000&page[number]=1`;
    let data;
    let tryCount = 0;
    do {
      console.log('start download from eadbox');
      data = await axios
        .get(url, {
          headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
          timeout: 5 * 60000,
        })
        .then(res => {
          console.log('     finish download');
          return res.data;
        })
        .catch(() => {
          console.error('cant download course data from eadbox! externalId =', externalId);
        });
      tryCount++;
    } while (!data && tryCount < DOWNLOAD_RETRY_COUNT);

    return data;
  }

  formatNameToS3Standart(name) {
    return name.replace(/[$&@=;.+ ,?\\{^}%`\]>[~<#|/]/gi, '-');
  }

  buildS3Key(e, courseDb, moduleMap, lectureMap, videos) {
    const module: Daum = moduleMap[videos[e.id]];
    const modulePosition = module?.attributes?.position || 0;
    const lecturePositionIndex = module?.relationships?.lectures?.data?.findIndex(t => t.id === videos[e.id]);
    const lecturePosition = lecturePositionIndex === -1 ? 1 : lecturePositionIndex + 1;
    const contentPosition = e.attributes.position + 1;

    const s3url = `${courseDb.slug}/M${('00' + modulePosition).slice(-2)}-L${('00' + lecturePosition).slice(-2)}-C${(
      '00' + contentPosition
    ).slice(-2)}-${this.formatNameToS3Standart(e.attributes.title)}`;
    return {
      ...e,
      lectureExternalId: videos[e.id],
      lecture: lectureMap[videos[e.id]],
      module: moduleMap[videos[e.id]],
      s3url,
    };
  }

  async parseCourseModules(data: Root, lectureMap, courseDb) {
    const moduleMap: Record<string, Daum> = data.data.reduce((a, e) => {
      e.relationships?.lectures?.data?.forEach(l => (a[l.id] = e));
      return a;
    }, {});

    const videoLectureMap = data.included.reduce(
      (a, l) => {
        l.relationships?.contents?.data.map(v => {
          if (v.type === 'contents--coconutVideo') a.coconutVideo[v.id] = l.id;
          if (v.type === 'contents--sparkVideo') a.sparkVideo[v.id] = l.id;
        });
        return a;
      },
      { coconutVideo: {}, sparkVideo: {} },
    );

    const coconutVideo = data.included
      .filter(e => e.type === 'contents--coconutVideo')
      .map(e => {
        return this.buildS3Key(e, courseDb, moduleMap, lectureMap, videoLectureMap.coconutVideo);
      });
    const sparkVideo = data.included
      .filter(e => e.type === 'contents--sparkVideo')
      .map(e => {
        return this.buildS3Key(e, courseDb, moduleMap, lectureMap, videoLectureMap.sparkVideo);
      });

    return {
      coconutVideo,
      sparkVideo,
    };
  }

  async downloadPlaylist(element) {
    const urlBase = `https://videos.herospark.com/a0c81400-9dd5-11eb-9304-0e85033f114d/${element.attributes.sparkVideosUuid}`;
    const manifest = await axios
      .get(`${urlBase}/playlist.m3u8`)
      .then(res => {
        return res.data;
      })
      .catch(err => {
        console.error(`cant get playlist ${urlBase}/playlist.m3u8`);
        console.error(err);
      });
    if (!manifest) {
      return false;
    }

    const parser = new m3u8Parser.Parser();
    parser.push(manifest);
    parser.end();

    return parser.manifest;
  }

  fixPlaylist(playlist: string, basePath: string) {
    const parser = new m3u8Parser.Parser();
    parser.push(playlist);
    parser.end();

    parser.manifest.segments = parser.manifest.segments.map(e => {
      e.uri = `${basePath}/${e.uri}`;
      e.key.uri = `${basePath}/${e.key.uri}`;
      return e;
    });
    return parser.manifest;
  }

  getVideoMaxResolution(parsedManifest: PlaylistType) {
    return parsedManifest.playlists.sort((a, b) => b.attributes.RESOLUTION.width - a.attributes.RESOLUTION.width)[0];
  }

  async getPlaylist(element) {
    const urlBase = `https://videos.herospark.com/a0c81400-9dd5-11eb-9304-0e85033f114d/${element.attributes.sparkVideosUuid}`;

    const parsedManifest = await this.downloadPlaylist(element);
    if (!parsedManifest) {
      return false;
    }
    const maxResolution = this.getVideoMaxResolution(parsedManifest);
    const width = maxResolution.attributes.RESOLUTION.width;
    const playlist = await axios
      .get(`${urlBase}/${maxResolution.uri}`)
      .then(res =>
        res.data
          ?.split(`index_${width}`)
          .join(`${urlBase}/${width}/index_${width}`)
          .replace(/\n(index[0-9]+.ts)\n/gi, `\n${urlBase}/${width}/$1\n`)
          .replace('URI="enc.key"', `URI="${urlBase}/${width}/enc.key"`),
      )
      .catch(err => {
        console.error(err);
      });

    mkdirSync(`m3u8/${element.id}/`, { recursive: true });
    writeFileSync(`m3u8/${element.id}/index.m3u8`, playlist);

    return maxResolution;
  }

  async uploadEntry(element) {
    try {
      const maxResolutionPlaylistLoaded = await this.getPlaylist(element);
      if (!maxResolutionPlaylistLoaded) {
        return [1];
      }

      const [error] = await this.execFF(
        `ffmpeg -y -protocol_whitelist file,http,https,tcp,tls,crypto -i m3u8/${element.id}/index.m3u8  -c copy -bsf:a aac_adtstoasc m3u8/${element.id}/index.mp4`,
      );

      if (error) {
        this.counter.cantConvert.push(element);
        return [1];
      }
      let uploadedS3;
      try {
        uploadedS3 = await this.FilesStorageS3Service.uploadDiskSimple(
          `m3u8/${element.id}/index.mp4`,
          `${element.s3url}.mp4`,
          element.attributes.title,
          process.env.S3_VIDEO_BUCKET,
          'video/mp4',
        );
      } catch (error) {
        this.counter.cantUpload.push(element);
        return [1];
      }

      return [null, uploadedS3];
    } catch (error) {
      this.counter.cantUpload.push(element);
      console.error(error);
      return [error];
    } finally {
      await rm(`m3u8/${element.id}`, { recursive: true, force: true });
    }
  }

  async sparkVideo(sparkVideo) {
    for (let index = 0; index < sparkVideo.length; index++) {
      const element = sparkVideo[index];
      console.log(`sparkVideo ${element.s3url}`);

      try {
        const exist = await this.FilesStorageS3Service.isExist(`${element.s3url}.mp4`, process.env.S3_VIDEO_BUCKET);

        if (!exist) {
          const [err, uploaded] = await this.uploadEntry(element);
          if (err) {
            this.counter.cantUpload.push(element);
            continue;
          }
          if (uploaded) {
            console.log(`   + video uploaded ${element.s3url}`);
          }
        } else {
          console.log(`   + video already exist in S3 ${element.s3url}`);
        }
      } catch (error) {
        console.log(element);
        console.error(error);
      }
    }
  }

  execFF(cmd): Promise<any> {
    console.log('   - start ffmpeg');
    return new Promise(resolve => {
      const child = spawn(cmd, [], { shell: true });

      child.stdout.on('data', data => {
        !this.hideOutput && console.log(`${data}`);
      });

      child.stderr.on('data', data => {
        !this.hideOutput && console.log(`${data}`);
      });

      child.on('close', code => {
        if (code > 0) {
          console.error(`child process exited with code ${code}`);
        }

        resolve([code]);
      });
    });
  }

  async upsertCoconut({ url, title, s3path }) {
    console.log(`   - start upload ${url}`);
    const [err, res, exist] = await this.FilesStorageS3Service.upsertUrlSimple(
      url,
      title,
      `${s3path}.mp4`,
      process.env.S3_VIDEO_BUCKET,
      false,
    );

    if (!err) {
      if (exist) {
        console.log(`   + already exist s3 ${url}`);
      } else {
        console.log(`   + finish upload ${url}`);
      }
    } else {
      console.error(`   ! error upload ${url} ${err}`);
    }
    return [err, res];
  }

  print(courseId, courseDb) {
    console.log(`courseId ${courseId} `);
    if (courseDb) {
      console.log(`https://ebac-online.eadbox.com/ng/teacher/courses/${courseDb.slug}/lectures`);
    }
    console.log('coconutCount', this.counter.coconutCount);
    console.log('sparkCount', this.counter.sparkCount);
    console.log('noLecture', this.counter.noLecture);
    if (this.counter.cantGetHTTP.length) {
      console.log(`cantGetHTTP: ${this.counter.cantGetHTTP.length}`);
      this.counter.cantGetHTTP.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    if (this.counter.cantConvert.length) {
      console.log(`cantConvert: ${this.counter.cantConvert.length}`);
      this.counter.cantConvert.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    if (this.counter.cantUpload.length) {
      console.log(`cantUpload:  ${this.counter.cantUpload.length}`);
      this.counter.cantUpload.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    console.log(`finish upload course ${courseId}`);
  }

  async importOneCourse({ concurrency = 1, courseId }) {
    let courseDb;
    try {
      const courseDb = await this.coursesService.findOneImportVideo(courseId, {
        includeHidden: true,
      });
      if (!courseDb) {
        console.error(courseId, 'not found in DB');
        return;
      }

      const lectureMap = courseDb.modules.reduce((a, e) => {
        e.lectures.forEach(l => {
          a[l.externalId] = l;
        });
        return a;
      }, {});

      await this.startSession();

      const data = await this.downloadCourseModules(courseDb.externalId);
      if (!data) {
        console.error('cannot download modules from eadbox');
        return;
      }

      const videos = await this.parseCourseModules(data, lectureMap, courseDb);

      this.counter.coconutCount = videos.coconutVideo.length;
      this.counter.sparkCount = videos.sparkVideo.length;

      const coconutVideo = videos.coconutVideo.map(e => ({
        url: e.attributes.unencodedUrl,
        title: e.attributes.title,
        s3path: e.s3url,
      }));

      await promisePool(coconutVideo, this.upsertCoconut.bind(this), concurrency);

      await this.sparkVideo(videos.sparkVideo);
    } catch (error) {
      console.error(error);
    }

    this.print(courseId, courseDb);
  }

  async import({ concurrency = 1, courseId = '', hideOutput = false }) {
    this.hideOutput = hideOutput;
    if (!process.env.S3_VIDEO_BUCKET) {
      console.error(`require env variable S3_VIDEO_BUCKET`);
      return;
    }
    if (courseId) {
      return this.importOneCourse({ concurrency, courseId });
    }

    try {
      const coursesAll = await this.coursesService.findAll(false, false, '', '', true);
      const courses = coursesAll.filter(e => e.externalId);
      for (let index = 0; index < courses.length; index++) {
        const course = courses[index];
        await this.importOneCourse({ concurrency, courseId: course.id });
      }
    } catch (error) {
      console.error(error);
    }
  }
}
