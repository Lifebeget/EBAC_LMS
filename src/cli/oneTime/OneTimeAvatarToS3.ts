import { FileFolder } from '@enums/file-folder.enum';
import { INestApplicationContext } from '@nestjs/common';
import { FilesService } from 'src/files/files.service';
import { UsersService } from 'src/users/users.service';

import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate } from '../../lib/types/ImportCounters';

export class OneTimeAvatarToS3 {
  private filesService: FilesService;
  private usersService: UsersService;
  private force: boolean;

  constructor(private readonly context: INestApplicationContext, private counter = { ...counterTemplate }) {
    this.filesService = this.context.get(FilesService);
    this.usersService = this.context.get(UsersService);
    this.force = false;
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  async update(user: any) {
    try {
      if (this.force) {
        user.avatar.mimetype = null;
        if (user.avatar.eadboxUrl) {
          user.avatar.url = user.avatar.eadboxUrl;
        }
      }
      user.avatar.title = user.id;

      await this.filesService.uploadUrlToS3andUpdate(user.avatar, FileFolder.avatars);

      this.counter.updated++;
      console.log(' updated', user.id);
    } catch (error) {
      console.error('user id', user.id, 'avatar url', user.avatar.url, error);
      await this.usersService.removeSelfAvatar(user.id);
    }
  }

  async updateAvatar({ concurrency, force = false }) {
    this.force = force;
    if (force) {
      const dbusers = await this.usersService.findAllWithS3Avatar();
      console.log('reupload avatar S3', dbusers.length);
      await promisePool(dbusers, this.update.bind(this), 1 || concurrency);
    } else {
      const dbusers = await this.usersService.findAllWithNotS3Avatar();
      console.log('upload avatar S3', dbusers.length);
      await promisePool(dbusers, this.update.bind(this), 1 || concurrency);
    }
    console.log('     FINISH upload avatar S3');
    this.printStats(this.counter);
  }
}
