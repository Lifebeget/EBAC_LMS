import { INestApplicationContext } from '@nestjs/common';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { CoursesService } from 'src/courses/courses.service';
import { LecturesService } from 'src/courses/lectures.service';
import { ContentService } from 'src/courses/content.service';
import { AssessmentType } from '@lib/types/enums';
import { Assessment } from '@entities/assessment.entity';
import { ContentType } from '@enums/content-type.enum';
import { PaginationDto } from 'src/pagination.dto';
import { getConnection } from 'typeorm';

export class FixDuplicateAssessments {
  private assessmentsService: AssessmentsService;
  private submissionsService: SubmissionsService;
  private lecturesService: LecturesService;
  private coursesService: CoursesService;
  private contentService: ContentService;

  constructor(private readonly context: INestApplicationContext) {
    this.submissionsService = this.context.get(SubmissionsService);
    this.lecturesService = this.context.get(LecturesService);
    this.coursesService = context.get(CoursesService);
    this.assessmentsService = context.get(AssessmentsService);
    this.contentService = context.get(ContentService);
  }

  async fixDuplicateAssessments() {
    console.log('Running fixDuplicateAssessments...');

    // Getting courses
    const courses = await this.coursesService.findAll(true, true, undefined, undefined, true, undefined, false);

    for (let i = 0, l = courses.length; i < l; i++) {
      const course = courses[i];
      console.log(`=== Checking course: ${course.title}`);

      // Getting all course assessments
      const allAssessments = await this.assessmentsService.findAll({
        courseId: course.id,
      });
      const assessments = allAssessments.filter(a => a.type == AssessmentType.Default);

      // Group by lectures
      const groupedAssessments: Record<string, Assessment[]> = {};
      assessments.forEach(a => {
        if (!groupedAssessments.hasOwnProperty(a.lecture.id)) {
          groupedAssessments[a.lecture.id] = [];
        }
        groupedAssessments[a.lecture.id].push(a);
      });

      // Deal with lecture
      for (const lectureId in groupedAssessments) {
        if (groupedAssessments[lectureId].length > 1) {
          console.log(`Has duplicate assessments in lecture ${lectureId}`);
          await this.resolveDuplicatesForLecture(lectureId, groupedAssessments[lectureId]);
        }
      }
    }
  }

  async resolveDuplicatesForLecture(lectureId: string, assessments: Assessment[]) {
    // Determining ExternalId
    const externalIds = assessments.reduce((prev, cur) => {
      if (cur.externalId && !prev.includes(cur.externalId)) {
        prev.push(cur.externalId);
      }
      return prev;
    }, [] as string[]);
    if (externalIds.length > 1) {
      console.log('!!! Has multiple external ids: [' + externalIds.join(', ') + '] must be resolved manually');
      return;
    }
    const externalId = externalIds[0];

    // Getting content blocks
    const contentResponse = await this.contentService.findAll({ showAll: true } as PaginationDto, { lectureId, type: ContentType.task });
    const content = contentResponse.results;
    if (content.length > 1) {
      console.log('!!! Lecture has multiple Task blocks, must be resolved manually');
      return;
    }

    const contentId = content[0]?.id;

    // Can be fixed probably, choosing best candidate
    const candidates = assessments.filter(a => a.questions.length == 1 && a.externalId == externalId);
    if (candidates.length == 0) {
      console.log('!!! No acceptable candidates, probably broken assessment');
      return;
    }
    const resultAssessment = candidates[0];

    // If there is a single content block — switching it to this assessment
    if (contentId && content[0].assessment?.id != resultAssessment.id) {
      await this.contentService.update(contentId, {
        assessmentId: resultAssessment.id,
      });
      console.log('Updated content block');
    }

    // Switching submission answers to correct question
    const correctQuestionId = resultAssessment.questions[0].id;
    const brokenQuestionIds = assessments.reduce((prev, cur) => {
      cur.questions.forEach(q => {
        if (q.id != correctQuestionId && !prev.includes(q.id)) {
          prev.push(q.id);
        }
      });
      return prev;
    }, [] as string[]);
    if (brokenQuestionIds.length > 0) {
      const result = await getConnection().query(
        `
          UPDATE answer SET question_id = '${resultAssessment.questions[0].id}'
          WHERE question_id IN ('${brokenQuestionIds.join("', '")}');
        `,
      );
      console.log('Answers fixed: ' + result[1]);
    }

    // Switching submissions to correct assessments
    const brokenAssessmentIds = assessments.reduce((prev, cur) => {
      if (cur.id != resultAssessment.id && !prev.includes(cur.id)) {
        prev.push(cur.id);
      }
      return prev;
    }, [] as string[]);
    if (brokenAssessmentIds.length > 0) {
      const result = await getConnection().query(
        `
          UPDATE submission SET assessment_id = '${resultAssessment.id}'
          WHERE assessment_id IN ('${brokenAssessmentIds.join("', '")}');
        `,
      );
      console.log('Submissions fixed: ' + result[1]);
    }

    // Delete questions
    if (brokenQuestionIds.length > 0) {
      const result = await getConnection().query(
        `
          DELETE FROM question
          WHERE id IN ('${brokenQuestionIds.join("', '")}');
        `,
      );
      console.log('Questions deleted: ' + result[1]);
    }

    // Delete broken assessments
    if (brokenAssessmentIds.length > 0) {
      const result = await getConnection().query(
        `
          DELETE from assessment
          WHERE id IN ('${brokenAssessmentIds.join("', '")}');
        `,
      );
      console.log('Assessments deleted: ' + result[1]);
    }
  }

  async run() {
    await this.fixDuplicateAssessments();
  }
}
