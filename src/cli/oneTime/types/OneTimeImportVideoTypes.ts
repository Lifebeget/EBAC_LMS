export interface Root {
  data: Daum[];
  included: Included[];
}

export interface Daum {
  id: string;
  type: string;
  attributes: Attributes;
  relationships: Relationships;
}

export interface Attributes {
  title: string;
  description: string;
  position: number;
}

export interface Relationships {
  lectures: Lectures;
}

export interface Lectures {
  data: Daum2[];
}

export interface Daum2 {
  id: string;
  type: string;
}

export interface Included {
  id: string;
  type: string;
  attributes: Attributes2;
  relationships: Relationships2;
}

export interface Attributes2 {
  createdAt?: string;
  slug?: string;
  description: any;
  position?: number;
  title: string;
  updatedAt?: string;
  extra?: boolean;
  minimumGrade?: number;
  publishedAt?: string;
  alwaysReleased?: boolean;
  releasesAt: any;
  releasesAtFuture: any;
  releasesIn: any;
  showForum?: boolean;
  free?: boolean;
  private?: boolean;
  amountOfContents?: number;
  blockCopy?: boolean;
  processing?: boolean;
  invalidSparkVideo?: boolean;
  downloadable?: boolean;
  file?: string;
  viewUrl?: string;
  viewLimit: any;
  encodedUrl?: string;
  unencodedUrl?: string;
  subtitle: any;
  videoLimitEnabled?: boolean;
  sparkVideosId?: string;
  sparkVideosUuid?: string;
  thumbnail?: string;
  manual?: boolean;
  randomQuestions?: number;
  deadline?: number;
  redos?: number;
  showMultipleChoiceResult?: string;
  mediated: any;
  startsAt: any;
  endsAt: any;
  weight?: number;
  maximumGrade?: boolean;
  neverRequireCorrection?: boolean;
  canMediate?: boolean;
  url?: string;
}

export interface Relationships2 {
  attendances?: Attendances;
  contents?: Contents;
  simpleContents?: SimpleContents;
  subscriptions?: Subscriptions;
  teams?: Teams;
  course?: Course;
  module?: Module;
  lectureStatus?: LectureStatus;
  permissions?: Permissions;
  contentStatus?: ContentStatus;
  survey?: Survey;
  questions?: Questions;
  assessment?: Assessment;
}

export interface Attendances {
  data: any[];
}

export interface Contents {
  data: Daum3[];
}

export interface Daum3 {
  id: string;
  type: string;
}

export interface SimpleContents {
  data: Daum4[];
}

export interface Daum4 {
  id: string;
  type: string;
}

export interface Subscriptions {
  data: any[];
}

export interface Teams {
  data: any[];
}

export interface Course {
  data: Data;
}

export interface Data {
  id: string;
  type: string;
}

export interface Module {
  data: Data2;
}

export interface Data2 {
  id: string;
  type: string;
}

export interface LectureStatus {
  data: any;
}

export interface Permissions {
  data: Daum5[];
}

export interface Daum5 {
  id: string;
  type: string;
}

export interface ContentStatus {
  data: any;
}

export interface Survey {
  data: Data3;
}

export interface Data3 {
  id: string;
  type: string;
}

export interface Questions {
  data: Daum6[];
}

export interface Daum6 {
  id: string;
  type: string;
}

export interface Assessment {
  data: Data4;
}

export interface Data4 {
  id: string;
  type: string;
}
