export interface PlaylistType {
  allowCache: boolean;
  discontinuityStarts: any[];
  segments: any[];
  version: number;
  playlists: Playlist[];
  mediaGroups: MediaGroups;
}

export interface MediaGroups {
  audio: Audio;
  video: Audio;
  closedCaptions: Audio;
  subtitles: Audio;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Audio {}

export interface Playlist {
  attributes: Attributes;
  uri: string;
  timeline: number;
}

export interface Attributes {
  RESOLUTION: Resolution;
  BANDWIDTH: number;
}

export interface Resolution {
  width: number;
  height: number;
}
