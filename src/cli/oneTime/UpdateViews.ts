import { INestApplicationContext } from '@nestjs/common';
import { CoursesService } from 'src/courses/courses.service';

export class UpdateViews {
  private coursesService: CoursesService;

  constructor(private readonly context: INestApplicationContext) {
    this.coursesService = context.get(CoursesService);
  }

  async updateViews() {
    await this.coursesService.updateProgressMaterializedView();
    console.log('DONE');
  }
  async run() {
    await this.updateViews();
  }
}
