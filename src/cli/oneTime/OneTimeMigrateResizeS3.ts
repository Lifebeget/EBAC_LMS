import { File } from '@entities/file.entity';
import { INestApplicationContext } from '@nestjs/common';
import { FilesService } from 'src/files/files.service';
import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate, ImportCounter } from '../../lib/types/ImportCounters';

export class OneTimeMigrateResizeS3 {
  private filesService: FilesService;
  private counter: ImportCounter;

  constructor(private readonly context: INestApplicationContext) {
    this.counter = { ...counterTemplate };
    this.filesService = this.context.get(FilesService);
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  async move(file: File, dir = 'webinars') {
    const re = new RegExp(`${dir}\\/(?<uuid>[\\S]+)\\.(?<ext>.*)`, '');
    try {
      const m = file.s3Key.match(re);
      if (!m) {
        this.counter.skipped++;
        return console.error('not match url', file);
      }
      const uuid = m.groups?.uuid;
      const ext = m.groups?.ext;
      const newKey = `${dir}/${uuid}/original.${ext}`;
      const newUrl = file.url.replace(file.s3Key, newKey);

      await this.filesService.s3
        .copyObject({
          Bucket: process.env.S3_BUCKET_NAME,
          CopySource: `${process.env.S3_BUCKET_NAME}/${file.s3Key}`,
          Key: newKey,
          ContentDisposition: `filename="${encodeURI(file.title)}"`,
          ContentType: file.mimetype,
        })
        .promise();

      await this.filesService.update(file.id, {
        s3Key: newKey,
        url: newUrl,
      });

      await this.filesService.s3
        .deleteObjects({
          Bucket: process.env.S3_BUCKET_NAME,
          Delete: {
            Objects: [
              {
                Key: file.s3Key,
              },
            ],
          },
        })
        .promise();
      this.counter.updated++;
    } catch (error) {
      console.error('move', error);
    }
  }

  moveWebinars(file: File) {
    return this.move(file, 'webinars');
  }

  async moveFiles(concurrency = 3) {
    console.log('search in DB...');
    const dbFiles = await this.filesService.findOldResize();
    console.log(dbFiles);
    console.log('found in DB', dbFiles.length);
    await promisePool(dbFiles, this.moveWebinars.bind(this), concurrency);
    console.log('file move end');
    this.printStats(this.counter);
  }

  async moveResize({ concurrency }) {
    await this.moveFiles(+concurrency || 1);
  }
}
