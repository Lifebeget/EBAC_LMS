import { INestApplicationContext } from '@nestjs/common';
import { AnswersService } from 'src/assessments/answers.service';
import { UpdateAnswerDto } from 'src/assessments/dto/update-answer.dto';
import { FilesService } from 'src/files/files.service';
import { UpdateMessageDto } from 'src/forum/dto/update-forum-message.dto';
import { ForumService } from 'src/forum/forum.service';
import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate } from '../../lib/types/ImportCounters';

export class OneTimeCommentToS3 {
  private answersService: AnswersService;
  private filesService: FilesService;
  private forumService: ForumService;
  constructor(
    private readonly context: INestApplicationContext,
    private counterAnswer = { ...counterTemplate },
    private counterForum = { ...counterTemplate },
  ) {
    this.answersService = this.context.get(AnswersService);
    this.filesService = this.context.get(FilesService);
    this.forumService = this.context.get(ForumService);
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  async updateAnswer(answer: { comment?: string; id: string; tutorComment?: string }) {
    try {
      const update = new UpdateAnswerDto();
      if (answer.comment) {
        update.comment = await this.filesService.replaceAndUploadBase64ImgS3(answer.comment);
      }
      if (answer.tutorComment) {
        update.tutorComment = await this.filesService.replaceAndUploadBase64ImgS3(answer.tutorComment);
      }
      if (Object.keys(update)) {
        this.counterAnswer.updated++;
        await this.answersService.update(answer.id, update);
      } else this.counterAnswer.skipped++;
      console.log('updateAnswer id =', answer.id);
    } catch (error) {
      console.error('updateAnswer', error);
    }
  }

  async updateAnswers(concurrency = 3) {
    console.log('Answers start find in DB...');
    const dbAnswers = await this.answersService.findAllWithBase64Img();
    console.log('Answers to upload S3 in DB', dbAnswers.length);
    await promisePool(dbAnswers, this.updateAnswer.bind(this), concurrency);
    console.log('Answers upload finish');
    this.printStats(this.counterAnswer);
  }

  async updateForumMessage(forumMessage: { body?: string; id: string }) {
    try {
      const update = new UpdateMessageDto();
      update.body = await this.filesService.replaceAndUploadBase64ImgS3(forumMessage.body);
      this.counterForum.updated++;
      await this.forumService.update(forumMessage.id, update);
      console.log('updateForumMessage id =', forumMessage.id);
    } catch (error) {
      console.error('updateForumMessage', error);
    }
  }

  async updateForumMessages(concurrency = 3) {
    console.log('ForumMessages start find in DB...');
    const dbForumMessages = await this.forumService.findAllWithBase64Img();
    console.log('ForumMessages to upload S3 in DB', dbForumMessages.length);
    await promisePool(dbForumMessages, this.updateForumMessage.bind(this), concurrency);
    console.log('ForumMessages upload finish');
    this.printStats(this.counterForum);
  }

  async updateComments({ concurrency }) {
    await this.updateAnswers(+concurrency || 1);
    await this.updateForumMessages(+concurrency || 1);
  }
}
