import { User } from '@entities/user.entity';
import { ContentType } from '@enums/content-type.enum';
import { LectureStatus } from '@lib/api/types';
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import * as cheerio from 'cheerio';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { ContentService } from 'src/courses/content.service';
import { CoursesService } from 'src/courses/courses.service';
import { LecturesService } from 'src/courses/lectures.service';
import { UsersService } from 'src/users/users.service';
import promisePool from 'src/_util/async-bottleneck';

const BASE_URL = 'https://ebac-online.eadbox.com';
const MAX_RETRY = 10;

type TList = {
  url?: string;
  course?: {
    title?: string;
  };
  user?: {
    name?: string;
    externalId?: string;
  };
  list?: {
    lectures?: any[];
  };
};
export class ImportProgress {
  private assessmentsService: AssessmentsService;
  private submissionsService: SubmissionsService;
  private lecturesService: LecturesService;
  private usersService: UsersService;
  private coursesService: CoursesService;
  private contentService: ContentService;
  private concurrency = 1;
  private nodebug = true;
  private eadboxSessionCookie: string;

  constructor(private readonly context: INestApplicationContext) {
    this.submissionsService = this.context.get(SubmissionsService);
    this.lecturesService = this.context.get(LecturesService);
    this.coursesService = context.get(CoursesService);
    this.assessmentsService = context.get(AssessmentsService);
    this.contentService = context.get(ContentService);
    this.usersService = context.get(UsersService);
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async parseList($): Promise<TList[]> {
    try {
      const rows = $('#subscriptions > tbody > tr').toArray();

      const list: TList[] = rows.map(e => {
        try {
          const url = $(e).find('td:nth-child(2) > a')[0].attribs.href; // '/admin/subscriptions/academic/5f1f19c25d86cb00104eb8e9'
          const courseTitle = $(e).find('td:nth-child(2)').text();

          const username = $(e).find('td:nth-child(3)').text(); // 'user name'
          const userExternalId = $(e)
            .find('td:nth-child(3) > a')[0]
            .attribs.href.match(/admin\/users\/(.+)/)[1]; // '/admin/users/5f1b68a5f9130bcf065c0742'
          return {
            url,
            course: {
              title: courseTitle,
            },
            user: {
              name: username,
              externalId: userExternalId,
            },
          };
        } catch (error) {
          console.error(error);
          return {};
        }
      });

      return list || [];
    } catch (error) {
      console.error(error);
      return [];
    }
  }

  async parseProgress($) {
    let result: any = {};
    try {
      const userHref = $(
        '#content > div > div > div.inner-page > section > div.pages-details > div.user-info > div:nth-child(1) > h3 > small > a',
      ).attr('href'); // '/admin/users/5f1b68a5f9130bcf065c0760'
      result.userExternalId = userHref.match('/admin/users/(.+)')[1];

      const rows = $("#lectures > table > tbody > tr:not([class^='success'])").toArray();
      result.lectures = rows.map(e => {
        const lectureHref = $(e).find('td:nth-child(2) > h4 > a')[0].attribs.href; // '/student/courses/ux-design/lectures/pesquisa'

        const [_, courseSlug, lectureSlug] = lectureHref.match('/student/courses/(.+?)/lectures/(.+)');
        const classLecture = $(e).find('td:nth-child(3)> span')[0].attribs.class; // 'label label-success'
        let status = '';
        if (classLecture.includes('label-success')) {
          status = LectureStatus.success;
        } else if (classLecture.includes('label-warning')) {
          status = LectureStatus.inProgress;
        } else if (classLecture.includes('label-default')) {
          // status = LectureStatus.failed;
        }

        return { courseSlug, lectureSlug, status };
      });
    } catch (error) {
      console.error(error);
      result = {};
    }
    return result;
  }

  async parseLastPage($) {
    try {
      const href = $('#content > div > div > div.inner-page > section > ul > li.page-item.page-last > a')[0].attribs.href;
      return +href.match(/page=([0-9]+)/)[1] || 2000;
    } catch (error) {
      console.error(error);
      return 2000;
    }
  }

  async getLastPage(): Promise<[number, TList[]]> {
    const html = await this.downloadOnePage(`${BASE_URL}/admin/subscriptions/academic?page=${1}`);
    const $ = cheerio.load(html);
    const maxPage = await this.parseLastPage($);
    const list = await this.parseList($);

    return [maxPage, list];
  }

  async downloadOnePage(url) {
    let res;
    let count = 0;

    do {
      count++;
      !this.nodebug && console.log(`download ${url}`);

      res = await axios
        .get(url, {
          headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
        })
        .catch(error => {
          console.log(error);
        });
    } while (count <= MAX_RETRY && res?.status != 200);

    if (res?.status != 200) {
      return '';
    }
    !this.nodebug && console.log(`  finish donwload ${url}`);
    return res.data;
  }

  async downloadAndParseListPage(url) {
    const html = await this.downloadOnePage(url);
    const list = await this.parseList(cheerio.load(html));
    return list;
  }

  async getList() {
    const [maxPage, list] = await this.getLastPage();
    const donwloadList = Array.from({ length: +maxPage }, (_, i) => i + 1)
      .splice(1, +maxPage)
      .map(e => `${BASE_URL}/admin/subscriptions/academic?page=${e}`);

    const parsedList = await promisePool(donwloadList, this.downloadAndParseListPage.bind(this), this.concurrency);
    parsedList.forEach((e: Array<TList>) => list.push(...e));

    return list.map(e => {
      e.url = `${BASE_URL}${e.url}`;
      return e;
    });
  }

  async downloadAndParsePage(data: TList): Promise<TList> {
    const html = await this.downloadOnePage(data.url);
    data.list = await this.parseProgress(cheerio.load(html));
    return data;
  }

  async importList() {
    const list = await this.getList();

    const parsedList = await promisePool(list, this.downloadAndParsePage.bind(this), this.concurrency);
    return <TList[]>parsedList;
  }

  async loadLectures() {
    const courses = await this.coursesService.findAll(true, true, undefined, undefined, true, undefined, false);

    const lectureMap = {};
    courses.forEach(c => {
      if (!c.slug) return;
      c.modules.forEach(m =>
        m.lectures.forEach(l => {
          if (!l.slug) return;
          lectureMap[`${c.slug}=${l.slug}`] = l;
        }),
      );
    });
    return lectureMap;
  }

  async loadUsers(): Promise<Record<string, User>> {
    const users = await this.usersService.eadBoxFindManyWithExternalId();

    const userMap = {};
    users.forEach(u => {
      userMap[u.externalId] = u;
    });
    return userMap;
  }

  async importProgress() {
    console.log('Running import progress...');
    const list = await this.importList();

    const lectureMap = await this.loadLectures();
    const userMap = await this.loadUsers();
    const counter: any = {
      parsedProgress: list.length,
      'user not found by externalid': 0,
      'lecture not found by slug': 0,
      '[SKIP] same progress': 0,
    };
    for (let i = 0, l = list.length; i < l; i++) {
      const el: TList = list[i];
      const userDb = userMap[el.user.externalId];
      if (!userDb) {
        console.error('user not found by externalid' + el.user.externalId);
        counter['user not found by externalid'] += 1;
        continue;
      }

      for (let index = 0; index < el.list.lectures.length; index++) {
        try {
          const item = el.list.lectures[index];
          const key = `${item.courseSlug}=${item.lectureSlug}`;
          const lectuteDb = lectureMap[key];
          if (!lectuteDb) {
            console.error('lecture not found by slug' + item.lectureSlug);
            counter['lecture not found by slug'] += 1;
            continue;
          }
          const lectureProgressDb = await this.lecturesService.findOneLecturesProgress({
            lectureId: lectuteDb.id,
            userId: userDb.id,
          });
          if ((!item.status && !lectureProgressDb) || (lectureProgressDb && item.status == lectureProgressDb.status)) {
            counter['[SKIP] same progress'] += 1;
            continue;
          }

          if (
            lectuteDb.contents.filter(e =>
              [ContentType.globalSurvey, ContentType.quiz, ContentType.survey, ContentType.task].includes(e.type),
            ).length
          ) {
            const key = `[SKIP] task lecture import[${item.status}] => db[${lectureProgressDb && lectureProgressDb.status}]`;
            counter[key] = counter[key] ? counter[key] + 1 : 1;
            continue;
          }

          if (!item.status) {
            const key = `[SKIP] 'not started' lecture import[${item.status}] = skip update progress`;
            counter[key] = counter[key] ? counter[key] + 1 : 1;
            continue;
          }

          if (lectureProgressDb) {
            const key2 = `update status from db[${lectureProgressDb.status}] ==> import[${item.status}] `;
            counter[key2] = counter[key2] ? counter[key2] + 1 : 1;
          } else {
            const key2 = `create status ==> import[${item.status}] `;
            counter[key2] = counter[key2] ? counter[key2] + 1 : 1;
          }

          await this.lecturesService.setLectureStatus(userDb.id, lectuteDb.id, item.status);
        } catch (error) {
          console.error(error);
        }
      }

      !this.nodebug && console.log(counter);
    }
    console.log('FINISH');
    console.log(counter);
  }

  async import(options) {
    this.concurrency = +options.concurrency;
    this.nodebug = options.nodebug;
    await this.startSession();
    await this.importProgress();
  }
}
