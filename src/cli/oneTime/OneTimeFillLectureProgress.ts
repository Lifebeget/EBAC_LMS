import { LectureStatus } from '@lib/api/types';
import { SubmissionStatus } from '@lib/types/enums';
import { INestApplicationContext } from '@nestjs/common';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { CoursesService } from 'src/courses/courses.service';
import { LecturesService } from 'src/courses/lectures.service';
import { counterTemplate, ImportCounter } from '../../lib/types/ImportCounters';

export class OneTimeFillLectureProgress {
  private assessmentsService: AssessmentsService;
  private submissionsService: SubmissionsService;
  private lecturesService: LecturesService;
  private counter: ImportCounter;
  private coursesService: CoursesService;
  private onlyTask: boolean;

  constructor(private readonly context: INestApplicationContext) {
    this.counter = { ...counterTemplate };
    this.submissionsService = this.context.get(SubmissionsService);
    this.lecturesService = this.context.get(LecturesService);
    this.coursesService = context.get(CoursesService);
    this.assessmentsService = context.get(AssessmentsService);
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  one(dbSubmissionAll, assessmentsLecturesListOne) {
    const dbSubmission = dbSubmissionAll.filter(e => assessmentsLecturesListOne[e.assessment_id]);
    const rewriteProgress = [];
    for (let index = 0; index < dbSubmission.length; index++) {
      const element = dbSubmission[index];
      let status = null;
      let progress = 0;
      if (element.status === SubmissionStatus.Graded) {
        if (element.ok) {
          status = LectureStatus.success;
          progress = 100;
        } else {
          status = LectureStatus.failed;
          progress = 0;
        }
      } else {
        status = LectureStatus.inProgress;
        progress = 50;
      }
      if (!this.onlyTask) {
        assessmentsLecturesListOne[element.assessment_id].lecturesBefore.forEach(lecture => {
          rewriteProgress.push({
            lectureId: lecture,
            userId: element.user_id,
            viewed: element.submitted_at,
            ended: element.submitted_at,
            progress: 100,
            status: LectureStatus.success,
          });
        });
      }

      assessmentsLecturesListOne[element.assessment_id].lectureWithAssessment.forEach(lecture => {
        rewriteProgress.push({
          lectureId: lecture,
          userId: element.user_id,
          viewed: element.submitted_at,
          ended: element.submitted_at,
          progress: progress,
          status: status,
        });
      });
    }
    return rewriteProgress;
  }

  many(dbSubmissionAll, assessmentsLecturesListOne, assessmentsLecturesListMulti) {
    const dbSubmissionMany = dbSubmissionAll.filter(e => !assessmentsLecturesListOne[e.assessment_id]);

    const rewriteProgress = [];

    const dbSubmissionUserLecture = dbSubmissionMany.reduce((a, e) => {
      if (!a[e.user_id]) {
        a[e.user_id] = {};
      }
      if (!a[e.user_id][e.lecture_id]) {
        a[e.user_id][e.lecture_id] = [];
      }
      a[e.user_id][e.lecture_id].push(e);
      return a;
    }, {});

    for (const id of Object.keys(dbSubmissionUserLecture)) {
      const user = dbSubmissionUserLecture[id];
      for (const lecture of Object.keys(user)) {
        const assessments = user[lecture];
        const foundMulti = Object.values(assessmentsLecturesListMulti).filter((e: any) =>
          e.assessments.map(e => e.id).includes(assessments[0].assessment_id),
        )[0];
        let success = 0;
        let fail = 0;
        assessments.forEach(element => {
          if (element.status === SubmissionStatus.Graded) {
            if (element.ok) {
              success++;
            } else {
              fail++;
            }
          }
        });

        let progress;
        let status;

        if (success === assessments.length) {
          progress = 100;
          status = LectureStatus.success;
          if (!this.onlyTask) {
            // @ts-ignore TODO: need description
            foundMulti.lecturesBefore.forEach(lecture => {
              const element = assessments[0];
              rewriteProgress.push({
                lectureId: lecture,
                userId: element.user_id,
                viewed: element.submitted_at,
                ended: element.submitted_at,
                progress,
                status,
              });
            });
          }
          // @ts-ignore TODO: need description
          foundMulti.lectureWithAssessment.forEach(lecture => {
            const element = assessments[0];
            rewriteProgress.push({
              lectureId: lecture,
              userId: element.user_id,
              viewed: element.submitted_at,
              ended: element.submitted_at,
              progress,
              status,
            });
          });
        } else if (fail > 0) {
          progress = 0;
          status = LectureStatus.failed;
          if (!this.onlyTask) {
            // @ts-ignore TODO: need description
            foundMulti.lecturesBefore.forEach(lecture => {
              const element = assessments[0];
              rewriteProgress.push({
                lectureId: lecture,
                userId: element.user_id,
                viewed: element.submitted_at,
                ended: element.submitted_at,
                progress: 100,
                status: LectureStatus.success,
              });
            });
          }
          // @ts-ignore TODO: need description
          foundMulti.lectureWithAssessment.forEach(lecture => {
            const element = assessments[0];
            rewriteProgress.push({
              lectureId: lecture,
              userId: element.user_id,
              viewed: element.submitted_at,
              ended: element.submitted_at,
              progress,
              status,
            });
          });
        } else {
          progress = 50;
          status = LectureStatus.inProgress;
          if (!this.onlyTask) {
            // @ts-ignore TODO: need description
            foundMulti.lecturesBefore.forEach(lecture => {
              const element = assessments[0];
              rewriteProgress.push({
                lectureId: lecture,
                userId: element.user_id,
                viewed: element.submitted_at,
                ended: element.submitted_at,
                progress: 100,
                status: LectureStatus.success,
              });
            });
          }
          // @ts-ignore TODO: need description
          foundMulti.lectureWithAssessment.forEach(lecture => {
            const element = assessments[0];
            rewriteProgress.push({
              lectureId: lecture,
              userId: element.user_id,
              viewed: element.submitted_at,
              ended: element.submitted_at,
              progress,
              status,
            });
          });
        }
      }
    }
    return rewriteProgress;
  }

  async empty() {
    const rewriteProgress = [];
    const db = await this.submissionsService.findForLectureProgressWithoutSubmissionAndViewed();
    for (let index = 0; index < db.length; index++) {
      const element = db[index];
      rewriteProgress.push({
        lectureId: element.lecture_id,
        userId: element.user_id,
        viewed: element.viewed,
        ended: element.ended,
        progress: element.progress,
        status: LectureStatus.inProgress,
      });
    }
    return rewriteProgress;
  }

  async getSortedAssessment() {
    const assessments = await this.assessmentsService.findAll({});
    const assessmentsLecture = {};
    assessments.forEach(e => {
      if (!assessmentsLecture[e.lecture.id]) {
        assessmentsLecture[e.lecture.id] = [];
      }
      assessmentsLecture[e.lecture.id].push(e);
    });

    const courses = await this.coursesService.findAll(false, true, null, null, true);

    const assessmentsLecturesListOne = {};
    const assessmentsLecturesListMulti = [];

    courses.map(e => {
      const lectures = e.modules.reduce((a, l) => a.concat(l.lectures), []).sort((a, b) => a.sort - b.sort);

      let lectureWithAssessment = [];
      let lecturesBefore = [];

      for (let index = 0; index < lectures.length; index++) {
        const lecture = lectures[index];

        if (!assessmentsLecture[lecture.id]) {
          lecturesBefore.push(lecture.id);
          continue;
        }
        if (assessmentsLecture[lecture.id].length === 1) {
          lectureWithAssessment.push(lecture.id);
          assessmentsLecturesListOne[assessmentsLecture[lecture.id][0].id] = {
            lectureWithAssessment,
            lecturesBefore,
          };
          lecturesBefore = [...lecturesBefore];
          lectureWithAssessment = [];
          continue;
        }
        lectureWithAssessment.push(lecture.id);
        assessmentsLecturesListMulti.push({
          assessments: assessmentsLecture[lecture.id],
          lectureWithAssessment,
          lecturesBefore,
        });
        lecturesBefore = [...lecturesBefore];
        lectureWithAssessment = [];
      }

      return lectures;
    });

    return [assessmentsLecturesListOne, assessmentsLecturesListMulti];
  }

  async fillProgress() {
    try {
      const [assessmentsLecturesListOne, assessmentsLecturesListMulti] = await this.getSortedAssessment();

      console.log('search in DB...');
      const dbSubmission = await this.submissionsService.getSubmissionForFillProgress();
      console.log(dbSubmission);
      console.log('found in DB', dbSubmission.length);

      const rewriteProgress = [];

      const rewriteProgressOne = this.one(dbSubmission, assessmentsLecturesListOne);

      const rewriteProgressMany = this.many(dbSubmission, assessmentsLecturesListOne, assessmentsLecturesListMulti);

      const rewriteProgressEmpty = await this.empty();

      await this.lecturesService.saveLectureProgressMany(rewriteProgressMany.concat(rewriteProgressOne).concat(rewriteProgressEmpty), true);

      console.log('saved', rewriteProgress.length);
    } catch (error) {
      console.error('fillProgress:', error);
    }
  }

  async run({ _concurrency = 1, onlyTask = false }) {
    this.onlyTask = onlyTask;
    await this.fillProgress();
  }
}
