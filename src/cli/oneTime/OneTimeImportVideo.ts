import { ContentType } from '@enums/content-type.enum';
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { spawn } from 'child_process';
import { mkdirSync, writeFileSync } from 'fs';
import { open, rm, stat } from 'fs/promises';
import * as kaltura from 'kaltura-client';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { ContentService } from 'src/courses/content.service';
import { CoursesService } from 'src/courses/courses.service';
import { KalturaService } from 'src/kaltura/kaltura.service';
import { Daum, Root } from './types/OneTimeImportVideoTypes';
import * as m3u8Parser from 'm3u8-parser';
import { PlaylistType } from './types/PlaylistTypes';

type CounterType = {
  coconutCount: number;
  sparkCount: number;
  noLecture: number;
  canGetHTTP: any[];
  cantConvert: any[];
  cantUpload: any[];
};

const CHUNC_SIZE = 100000000;
const CHUNC_RETRY_COUNT = 200;
const SESSION_RETRY_COUNT = 200;
const DOWNLOAD_RETRY_COUNT = 3;

export class OneTimeImportVideo {
  private eadboxSessionCookie: string;
  private submissionsService: SubmissionsService;
  private contentService: ContentService;
  private coursesService: CoursesService;
  private kalturaService: KalturaService;
  private counter: CounterType;
  private client: any;
  private hideOutput: boolean;
  private appendExist: boolean;
  constructor(private readonly context: INestApplicationContext) {
    this.kalturaService = this.context.get(KalturaService);
    this.submissionsService = context.get(SubmissionsService);
    this.contentService = context.get(ContentService);
    this.coursesService = context.get(CoursesService);
    this.counter = {
      coconutCount: 0,
      sparkCount: 0,
      noLecture: 0,
      canGetHTTP: [],
      cantConvert: [],
      cantUpload: [],
    };

    const config = new kaltura.Configuration();

    this.client = new kaltura.Client(config);
    this.hideOutput = false;
  }

  async kaltura_session_start() {
    const userId = null;
    const type = kaltura.enums.SessionType.ADMIN;
    const expiry = null;
    const privileges = 'disableentitlement, disableentitlementforentry';
    const secret = process.env.KALTURA_SECRET;
    const partnerId = process.env.KALTURA_PARTNER_ID;
    const tryCount = 0;
    let ks;
    do {
      ks = await kaltura.services.session
        .start(secret, userId, type, partnerId, expiry, privileges)
        .completion((success, ks) => {
          if (!success) {
            console.error(ks);
            return;
          }

          this.client.setKs(ks);
          console.log('Session started: ' + ks);
          return ks;
        })
        .execute(this.client);
    } while (!ks && tryCount < SESSION_RETRY_COUNT);
  }

  async upsertEntryUrl(entryName, entryDescription, url, categories) {
    const exists: any = await this.getEntriesByNameAndCategory(entryName, categories);
    if (exists && exists.length) {
      return exists[0].id;
    }
    return this.addFromUrl(entryName, entryDescription, url, categories);
  }

  addFromUrl(entryName, entryDescription, url, categories) {
    const mediaEntry = new kaltura.objects.MediaEntry({
      mediaType: kaltura.enums.MediaType.VIDEO,
      name: entryName,
      description: entryDescription,
      categories,
    });
    return new Promise((resolve, reject) => {
      kaltura.services.media
        .addFromUrl(mediaEntry, url)
        .completion((success, entry) => {
          if (!success) {
            reject(entry.message);
            return;
          }

          console.log('addFromUrl: ' + entry.id);
          resolve(entry.id);
        })
        .execute(this.client);
    });
  }

  getEntriesByNameAndCategory(entryName, categories) {
    const mediaEntry = new kaltura.objects.MediaEntryFilter({
      nameEqual: entryName,
      categoriesMatchAnd: categories,
    });
    return new Promise((resolve, reject) => {
      kaltura.services.media
        .listAction(mediaEntry)
        .completion((success, entry) => {
          if (!success) {
            reject(entry.message);
            return;
          }

          resolve(entry.objects || []);
        })
        .execute(this.client);
    });
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
    console.log(this.eadboxSessionCookie);
  }

  async downloadCourseModules(externalId): Promise<Root> {
    const url = `https://ebac-online.eadbox.com.br/ng/api/v2/teacher/courses/${externalId}/modules?&page[size]=100000000&page[number]=1`;
    let data;
    let tryCount = 0;
    do {
      console.log('start download from eadbox');
      data = await axios
        .get(url, {
          headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
          timeout: 5 * 60000,
        })
        .then(res => {
          console.log('     finish download');
          return res.data;
        })
        .catch(() => {
          console.error('cant download course data from eadbox! externalId =', externalId);
        });
      tryCount++;
    } while (!data && tryCount < DOWNLOAD_RETRY_COUNT);

    return data;
  }

  async parseCourseModules(data: Root, lectureMap, courseDb) {
    const moduleMap: Record<string, Daum> = data.data.reduce((a, e) => {
      e.relationships?.lectures?.data?.forEach(l => (a[l.id] = e));
      return a;
    }, {});

    const videoLectureMap = data.included.reduce(
      (a, l) => {
        l.relationships?.contents?.data.map(v => {
          if (v.type === 'contents--coconutVideo') a.coconutVideo[v.id] = l.id;
          if (v.type === 'contents--sparkVideo') a.sparkVideo[v.id] = l.id;
        });
        return a;
      },
      { coconutVideo: {}, sparkVideo: {} },
    );

    const coconutVideo = data.included
      .filter(e => e.type === 'contents--coconutVideo')
      .map(e => {
        const module: Daum = moduleMap[videoLectureMap.coconutVideo[e.id]];
        const modulePosition = module?.attributes?.position || 1;
        const lecturePositionIndex = module?.relationships?.lectures?.data?.findIndex(t => t.id === videoLectureMap.coconutVideo[e.id]);
        const lecturePosition = lecturePositionIndex === -1 ? 1 : lecturePositionIndex + 1;
        const contentPosition = e.attributes.position + 1;

        const kalturaVideoName = `${courseDb.slug} M${('00' + modulePosition).slice(-2)} L${('00' + lecturePosition).slice(-2)} C${(
          '00' + contentPosition
        ).slice(-2)} ${e.attributes.title}`;
        return {
          ...e,
          lectureExternalId: videoLectureMap.coconutVideo[e.id],
          lecture: lectureMap[videoLectureMap.coconutVideo[e.id]],
          module: moduleMap[videoLectureMap.coconutVideo[e.id]],
          kalturaVideoName,
        };
      });
    const sparkVideo = data.included
      .filter(e => e.type === 'contents--sparkVideo')
      .map(e => {
        const module: Daum = moduleMap[videoLectureMap.sparkVideo[e.id]];
        const modulePosition = module?.attributes?.position || 1;
        const lecturePositionIndex = module?.relationships?.lectures?.data?.findIndex(t => t.id === videoLectureMap.sparkVideo[e.id]);
        const lecturePosition = lecturePositionIndex === -1 ? 1 : lecturePositionIndex + 1;
        const contentPosition = e.attributes.position + 1;

        const kalturaVideoName = `${courseDb.slug} M${('00' + modulePosition).slice(-2)} L${('00' + lecturePosition).slice(-2)} C${(
          '00' + contentPosition
        ).slice(-2)} ${e.attributes.title}`;
        return {
          ...e,
          lectureExternalId: videoLectureMap.sparkVideo[e.id],
          lecture: lectureMap[videoLectureMap.sparkVideo[e.id]],
          module: moduleMap[videoLectureMap.sparkVideo[e.id]],
          kalturaVideoName,
        };
      });

    return {
      coconutVideo,
      sparkVideo,
    };
  }

  async coconutVideo(coconutVideo, courseDb, kalturaCourseFolder) {
    for (let index = 0; index < coconutVideo.length; index++) {
      try {
        const element = coconutVideo[index];

        if (!element.lecture) {
          this.counter.noLecture++;
          console.error('content externalId = ', element.id, ' lecture not found lectureExternalId =', element.lectureExternalId);
          continue;
        }
        const content = element.lecture?.contents?.find(e => e.externalId === element.id);

        if (!this.appendExist && !content && element.lecture?.contents?.filter(e => e.type === ContentType.kaltura)?.length) {
          console.log('[ appendExist = false ] not found content by externalId, skip lecture ( already have kaltura content block )');
          continue;
        }

        if (!content) {
          // create content
          const sort = element.lecture?.contents?.length ? Math.max(...element.lecture.contents.map(e => e.sort || 0)) + 10 : 0;

          const unencodedUrl = element?.attributes?.unencodedUrl?.replace(/sd_mp4/gi, 'fhd_mp4');
          const entity = await this.upsertEntryUrl(
            element.kalturaVideoName,
            element.attributes.title,
            unencodedUrl,
            `MediaSpace>site>channels>${kalturaCourseFolder}`,
          );
          const _content = await this.contentService.create(element.lecture.id, {
            type: ContentType.kaltura,
            sort,
            active: true,
            title: element.attributes.title,
            entityId: entity,
            externalId: element.id,
            data: {
              ks: this.kalturaService.getKSforEntryId(entity),
            },
          });
        } else {
          // update content
          if (!content.entityId) {
            const unencodedUrl = element?.attributes?.unencodedUrl?.replace(/sd_mp4/gi, 'fhd_mp4');
            const entity = await this.upsertEntryUrl(
              element.kalturaVideoName,
              element.attributes.title,
              unencodedUrl,
              `MediaSpace>site>channels>${kalturaCourseFolder}`,
            );
            await this.contentService.update(content.id, {
              entityId: entity.id,
              data: {
                ...(content.data || {}),
                ks: this.kalturaService.getKSforEntryId(entity),
              },
            });
          }
        }
      } catch (error) {
        console.error(error);
      }
    }
  }
  async isExistEntryAtKaltura(entryName, categories) {
    const exists: any = await this.getEntriesByNameAndCategory(entryName, categories);
    if (exists && exists.length) {
      return [null, exists[0].id];
    }
    return [null, null];
  }

  addEntry(entryName, entryDescription, tokenId, categories) {
    const mediaEntry = new kaltura.objects.MediaEntry({
      mediaType: kaltura.enums.MediaType.VIDEO,
      name: entryName,
      description: entryDescription,
      categories,
    });
    return new Promise((resolve, reject) => {
      kaltura.services.media
        .addFromUploadedFile(mediaEntry, tokenId)
        .completion((success, entry) => {
          if (!success) {
            reject(entry.message);
            return;
          }

          console.log('addEntry: ' + entry.id);
          resolve(entry.id);
        })
        .execute(this.client);
    });
  }

  async downloadPlaylist(element) {
    const urlBase = `https://videos.herospark.com/a0c81400-9dd5-11eb-9304-0e85033f114d/${element.attributes.sparkVideosUuid}`;
    const manifest = await axios
      .get(`${urlBase}/playlist.m3u8`)
      .then(res => {
        return res.data;
      })
      .catch(err => {
        console.error(`cant get playlist ${urlBase}/playlist.m3u8`);
        console.error(err);
      });
    if (!manifest) {
      return false;
    }

    const parser = new m3u8Parser.Parser();
    parser.push(manifest);
    parser.end();

    return parser.manifest;
  }

  fixPlaylist(playlist: string, basePath: string) {
    const parser = new m3u8Parser.Parser();
    parser.push(playlist);
    parser.end();

    parser.manifest.segments = parser.manifest.segments.map(e => {
      e.uri = `${basePath}/${e.uri}`;
      e.key.uri = `${basePath}/${e.key.uri}`;
      return e;
    });
    return parser.manifest;
  }

  getVideoMaxResolution(parsedManifest: PlaylistType) {
    return parsedManifest.playlists.sort((a, b) => b.attributes.RESOLUTION.width - a.attributes.RESOLUTION.width)[0];
  }

  async getPlaylist(element) {
    const urlBase = `https://videos.herospark.com/a0c81400-9dd5-11eb-9304-0e85033f114d/${element.attributes.sparkVideosUuid}`;

    const parsedManifest = await this.downloadPlaylist(element);
    if (!parsedManifest) {
      return false;
    }
    const maxResolution = this.getVideoMaxResolution(parsedManifest);
    const width = maxResolution.attributes.RESOLUTION.width;
    const playlist = await axios
      .get(`${urlBase}/${maxResolution.uri}`)
      .then(res =>
        res.data
          ?.split(`index_${width}`)
          .join(`${urlBase}/${width}/index_${width}`)
          .replace(/\n(index[0-9]+.ts)\n/gi, `\n${urlBase}/${width}/$1\n`)
          .replace('URI="enc.key"', `URI="${urlBase}/${width}/enc.key"`),
      )
      .catch(err => {
        console.error(err);
      });

    mkdirSync(`m3u8/${element.id}/`, { recursive: true });
    writeFileSync(`m3u8/${element.id}/index.m3u8`, playlist);

    return maxResolution;
  }

  async uploadEntry(element, courseDb, kalturaCourseFolder) {
    try {
      const [_err, entryId] = await this.isExistEntryAtKaltura(element.kalturaVideoName, `MediaSpace>site>channels>${kalturaCourseFolder}`);
      if (entryId) {
        console.log('   + video already uploaded at kaltura');
        return [null, entryId];
      } else {
        console.log('   + video not uploaded at kaltura');
      }

      const maxResolutionPlaylistLoaded = await this.getPlaylist(element);
      if (!maxResolutionPlaylistLoaded) {
        return [1];
      }

      const [error] = await this.execFF(
        `ffmpeg -y -protocol_whitelist file,http,https,tcp,tls,crypto -i m3u8/${element.id}/index.m3u8  -c copy -bsf:a aac_adtstoasc m3u8/${element.id}/index.mp4`,
      );

      if (error) {
        this.counter.cantConvert.push(element);
        return [1];
      }
      const [error2, entity] = await this.uploadBigFile(
        `m3u8/${element.id}/index.mp4`,
        element.kalturaVideoName,
        element.attributes.title,
        `MediaSpace>site>channels>${kalturaCourseFolder}`,
      );

      if (error2) {
        this.counter.cantUpload.push(element);
        return [1];
      }

      return [null, entity];
    } catch (error) {
      this.counter.cantUpload.push(element);
      console.error(error);
      return [error];
    } finally {
      await rm(`m3u8/${element.id}`, { recursive: true, force: true });
    }
  }

  async sparkVideo(sparkVideo, courseDb, kalturaCourseFolder) {
    for (let index = 0; index < sparkVideo.length; index++) {
      const element = sparkVideo[index];
      console.log(`sparkVideo ${element.kalturaVideoName}`);

      if (!element.lecture) {
        console.error('   content externalId = ', element.id, ' lecture not found lectureExternalId =', element.lectureExternalId);
        continue;
      }

      const content = element.lecture?.contents?.find(e => e.externalId === element.id);

      if (!this.appendExist && !content && element.lecture?.contents?.filter(e => e.type === ContentType.kaltura)?.length) {
        console.log('[ appendExist = false ] not found content by externalId, skip lecture ( already have kaltura content block )');
        continue;
      }

      if (!content) {
        console.log(`   - content not found: create`);
        // create content
        try {
          const [error, entity] = await this.uploadEntry(element, courseDb, kalturaCourseFolder);

          if (error) {
            continue;
          }

          const sort = element.lecture?.contents?.length ? Math.max(...element.lecture.contents.map(e => e.sort || 0)) + 10 : 0;

          const _content = await this.contentService.create(element.lecture.id, {
            type: ContentType.kaltura,
            sort,
            active: true,
            title: element.attributes.title,
            entityId: entity,
            externalId: element.id,
            data: {
              ks: this.kalturaService.getKSforEntryId(entity),
            },
          });
        } catch (error) {
          console.log(element);
          console.error(error);
        }
      } else {
        // update content

        if (!content.entityId) {
          console.log(`   - content found without entryId`);
          try {
            const [error, entity] = await this.uploadEntry(element, courseDb, kalturaCourseFolder);

            if (error) {
              continue;
            }

            await this.contentService.update(content.id, {
              entityId: entity,
              data: {
                ...(content.data || {}),
                ks: this.kalturaService.getKSforEntryId(entity),
              },
            });
          } catch (error) {
            console.error(error);
          }
        } else {
          console.log(`   + content found with entryId: OK`);
        }
      }
    }
  }

  execFF(cmd): Promise<any> {
    console.log('   - start ffmpeg');
    return new Promise(resolve => {
      const child = spawn(cmd, [], { shell: true });

      child.stdout.on('data', data => {
        !this.hideOutput && console.log(`${data}`);
      });

      child.stderr.on('data', data => {
        !this.hideOutput && console.log(`${data}`);
      });

      child.on('close', code => {
        if (code > 0) {
          console.error(`child process exited with code ${code}`);
        }

        resolve([code]);
      });
    });
  }

  async uploadToken(createdUploadTokenId, buffer, final, resumeAt) {
    return new Promise(resolve => {
      kaltura.services.uploadToken
        .upload(createdUploadTokenId, buffer, true, final, resumeAt)
        .completion((success, entry) => {
          if (!success) {
            console.error(entry);
            return resolve(null);
          }
          return resolve(entry);
        })
        .execute(this.client);
    });
  }

  async uploadBigFile(filePath, entryName = '', entryDescription = '', categories = '') {
    const uploadToken = new kaltura.objects.UploadToken();

    const createdUploadToken = await kaltura.services.uploadToken.add(uploadToken).execute(this.client);

    await kaltura.services.uploadToken.upload(createdUploadToken.id, Buffer.alloc(0), false, false, 0).execute(this.client);

    const mediaEntry = new kaltura.objects.MediaEntry({
      mediaType: kaltura.enums.MediaType.VIDEO,
      name: entryName,
      description: entryDescription,
      categories,
    });

    const entry = await kaltura.services.media.add(mediaEntry).execute(this.client);

    const resource = new kaltura.objects.UploadedFileTokenResource();
    resource.token = createdUploadToken.id;
    await kaltura.services.media.addContent(entry.id, resource).execute(this.client);

    const fileStats = await stat(filePath);
    const FileHandle = await open(filePath, 'r');
    const countChunk = Math.ceil(fileStats.size / CHUNC_SIZE);
    let resumeAt = 0;
    try {
      for (let index = 0; index < countChunk; index++) {
        console.log(`   - start upload chunk ${index + 1}/${countChunk}`);
        const chunkSize = CHUNC_SIZE * (index + 1) > fileStats.size ? fileStats.size - CHUNC_SIZE * index : CHUNC_SIZE;
        const buffer = Buffer.alloc(chunkSize);
        const res = await FileHandle.read(buffer, 0, chunkSize);

        let errCount = 0;
        let uploaded = null;
        do {
          uploaded = await this.uploadToken(createdUploadToken.id, res.buffer, resumeAt + res.bytesRead === fileStats.size, resumeAt);

          errCount++;
        } while (!uploaded && errCount < CHUNC_RETRY_COUNT);

        if (errCount > CHUNC_RETRY_COUNT) {
          console.error(`   ! cant upload chunk ${index} by try limit`);
          return [`cant upload chunk ${index} by try limit`];
        }

        resumeAt += res.bytesRead;
      }
    } finally {
      FileHandle.close();
    }

    return [null, entry.id];
  }

  async import({ _concurrency = 1, courseId = '', hideOutput = false, appendExist = false }) {
    this.hideOutput = hideOutput;
    this.appendExist = appendExist;
    if (this.hideOutput) {
      this.client.shouldLog = false;
    }
    let courseDb;
    try {
      if (!courseId) {
        console.error('require param: --courseId d199ca10-6b7f-4010-b556-93cf8f591f9b ');
        return;
      }

      const courseDb = await this.coursesService.findOneImportVideo(courseId, {
        includeHidden: true,
      });
      if (!courseDb) {
        console.error(courseId, 'not found in DB');
        return;
      }

      const lectureMap = courseDb.modules.reduce((a, e) => {
        e.lectures.forEach(l => {
          a[l.externalId] = l;
        });
        return a;
      }, {});

      await this.startSession();

      const data = await this.downloadCourseModules(courseDb.externalId);
      if (!data) {
        console.error('cannot download modules from eadbox');
        return;
      }

      const videos = await this.parseCourseModules(data, lectureMap, courseDb);

      this.counter.coconutCount = videos.coconutVideo.length;
      this.counter.sparkCount = videos.sparkVideo.length;

      await this.kaltura_session_start();

      const kalturaCourseFolder = await this.kalturaService.findCategoryByCourse(courseDb);

      await this.coconutVideo(videos.coconutVideo, courseDb, kalturaCourseFolder);
      await this.sparkVideo(videos.sparkVideo, courseDb, kalturaCourseFolder);
    } catch (error) {
      console.error(error);
    }

    console.log(`courseId ${courseId} `);
    if (courseDb) {
      console.log(`https://ebac-online.eadbox.com/ng/teacher/courses/${courseDb.slug}/lectures`);
    }
    console.log('coconutCount', this.counter.coconutCount);
    console.log('sparkCount', this.counter.sparkCount);
    console.log('noLecture', this.counter.noLecture);
    if (this.counter.canGetHTTP.length) {
      console.log(`canGetHTTP: ${this.counter.canGetHTTP.length}`);
      this.counter.canGetHTTP.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    if (this.counter.cantConvert.length) {
      console.log(`cantConvert: ${this.counter.cantConvert.length}`);
      this.counter.cantConvert.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    if (this.counter.cantUpload.length) {
      console.log(`cantUpload:  ${this.counter.cantUpload.length}`);
      this.counter.cantUpload.forEach(e => {
        console.log(`   ${e.url}`);
      });
    }
    console.log('finish');
  }
}
