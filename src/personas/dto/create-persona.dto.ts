import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, IsString, IsUUID, Length, Matches, ValidateIf } from 'class-validator';

export class CreatePersonaDto {
  @ApiProperty({ description: 'Persona name' })
  @IsString()
  @ValidateIf(o => o.hasOwnProperty('name'))
  @Length(2, 100)
  @Matches(/\w+/, {
    message: 'Incorrect name',
  })
  @IsNotEmpty()
  name?: string;

  @ApiProperty({ description: 'User ID', required: false })
  @IsUUID()
  @IsOptional()
  userId?: string;

  @ApiProperty({ description: 'Signature file ID', required: false })
  @IsUUID()
  @IsOptional()
  signatureId?: string;

  @ApiProperty({ description: 'Job title', required: false })
  @IsString()
  @IsOptional()
  jobTitle?: string;

  @ApiProperty({ description: 'Job description', required: false })
  @IsString()
  @IsOptional()
  jobDescription?: string;

  @ApiProperty({ description: 'Some passport details', required: false })
  @IsString()
  @IsOptional()
  legalData?: string;

  @ApiProperty({ description: 'Hidden description', required: false })
  @IsString()
  @IsOptional()
  hiddenDescription?: string;

  @ApiProperty({ description: 'Is active', required: false })
  @IsBoolean()
  @IsOptional()
  active?: boolean;
}
