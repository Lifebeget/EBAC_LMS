import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class QueryPersonaDto {
  @ApiProperty({ description: 'Is active', required: false })
  @Transform(p => p.value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @ApiProperty({ description: 'Persona user ID', required: false })
  @IsUUID()
  @IsOptional()
  userId?: string;

  @ApiProperty({ description: 'Filter by name', required: false })
  @IsString()
  @IsOptional()
  name?: string;
}
