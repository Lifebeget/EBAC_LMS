import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Persona } from '@entities/persona.entity';
import { Repository } from 'typeorm';
import { QueryPersonaDto } from './dto/query-persona.dto';
import { User } from '@entities/user.entity';
import { File } from '@entities/file.entity';
import { Paginated, PaginationDto } from '../pagination.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class PersonasService {
  constructor(
    @InjectRepository(Persona)
    private personasRepository: Repository<Persona>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(File)
    private filesRepository: Repository<File>,
  ) {}

  async create(createPersonaDto: CreatePersonaDto) {
    const entity: any = { ...createPersonaDto };
    if (createPersonaDto.userId) {
      const [user] = await Promise.all([
        this.checkUserExistence(createPersonaDto.userId),
        this.checkPersonaUserUsage(createPersonaDto.userId),
      ]);
      entity.name = createPersonaDto.name || this.getUserName(user);
    }
    if (createPersonaDto.signatureId) {
      await Promise.all([
        this.checkSignatureExistence(createPersonaDto.signatureId),
        this.checkSignatureUsage(createPersonaDto.signatureId),
      ]);
      entity.signature = { id: createPersonaDto.signatureId };
    }
    return this.personasRepository.save(entity);
  }

  async findAll(paging: PaginationDto, query: QueryPersonaDto): Promise<Paginated<Persona[]>> {
    const personaQb = this.personaQueryBuilder();

    if (query.active === true) {
      personaQb.andWhere('persona.active = TRUE');
    } else if (query.active === false) {
      personaQb.andWhere('persona.active = FALSE');
    }
    if (!paging.showAll) {
      personaQb.skip((paging.page - 1) * paging.itemsPerPage);
      personaQb.take(paging.itemsPerPage);
    }
    if (query.name) {
      personaQb.andWhere('LOWER(persona.name) LIKE LOWER(:name)', {
        name: `%${query.name}%`,
      });
    }

    const [results, total] = await personaQb.getManyAndCount();

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  async findOne(id: string) {
    const qb = this.personaQueryBuilder();
    const persona = await qb.andWhere('persona.id = :id', { id }).getOne();

    if (!persona) {
      throw new NotFoundException(`Persona ${id} not found`);
    }
    return persona;
  }

  async findByUserId(userId: string) {
    await this.checkUserExistence(userId);
    const personaQb = this.personaQueryBuilder();
    const persona = await personaQb.andWhere('persona.user_id = :userId', { userId }).getOne();

    if (!persona) {
      throw new NotFoundException(`Persona with userId ${userId} not found`);
    }
    return persona;
  }

  async update(id: string, updatePersonaDto: UpdatePersonaDto) {
    const entity: any = { id, ...updatePersonaDto };
    const persona = await this.findOne(id);

    if (updatePersonaDto.userId && persona.userId !== updatePersonaDto.userId) {
      const [user] = await Promise.all([
        this.checkUserExistence(updatePersonaDto.userId),
        this.checkPersonaUserUsage(updatePersonaDto.userId),
      ]);
      entity.user = { id: updatePersonaDto.userId };
      entity.name = updatePersonaDto.name || this.getUserName(user);
    }
    if (updatePersonaDto.signatureId && persona.signatureId !== updatePersonaDto.signatureId) {
      await Promise.all([
        this.checkSignatureExistence(updatePersonaDto.signatureId),
        this.checkSignatureUsage(updatePersonaDto.signatureId),
      ]);
      entity.signature = { id: updatePersonaDto.signatureId };
    }
    if ('name' in updatePersonaDto && !updatePersonaDto.name) {
      if (!persona.userId) {
        throw new BadRequestException('Enter the name of a person to which no user is associated');
      } else {
        throw new BadRequestException('Enter the name');
      }
    }
    return this.personasRepository.save(entity);
  }

  async remove(id: string) {
    await this.findOne(id);
    return this.personasRepository.delete(id);
  }

  async checkUserExistence(userId: string) {
    const user = await this.usersRepository.findOne(userId, {
      relations: ['profile'],
    });
    if (!user) {
      throw new NotFoundException(`User ${userId} not found`);
    }
    return user;
  }

  async checkSignatureExistence(fileId: string) {
    const signature = await this.filesRepository.findOne(fileId);
    if (!signature) {
      throw new NotFoundException(`Signature file ${fileId} not found`);
    }
  }

  async checkSignatureUsage(fileId: string) {
    const persona = await this.personasRepository.findOne({
      signatureId: fileId,
    });
    if (persona) {
      throw new BadRequestException(`Signature file ${fileId} already in use`);
    }
  }

  async checkPersonaUserUsage(userId: string) {
    const persona = await this.personasRepository.findOne({
      userId,
    });
    if (persona) {
      throw new BadRequestException(`Persona with userId ${userId} already exists`);
    }
  }

  personaQueryBuilder() {
    return this.personasRepository
      .createQueryBuilder('persona')
      .leftJoinAndSelect('persona.user', 'user')
      .leftJoinAndSelect('persona.signature', 'signature')
      .orderBy('persona.created', 'DESC');
  }

  getUserName(user: User) {
    let userName = '';
    if (user.name) {
      userName = user.name;
    } else {
      userName = `${user.profile.name} ${user.profile.surname}`;
    }
    return userName;
  }
}
