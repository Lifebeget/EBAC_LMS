import { Controller, Get, Post, Body, Patch, Param, Delete, Query, ValidationPipe } from '@nestjs/common';
import { PersonasService } from './personas.service';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { QueryPersonaDto } from './dto/query-persona.dto';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Pagination } from '../pagination.decorator';
import { Paginated, PaginationDto } from '../pagination.dto';
import { Persona } from '@entities/persona.entity';
import { PgConstraints } from '../pg.decorators';

@Controller('personas')
@ApiBearerAuth()
export class PersonasController {
  constructor(private readonly personasService: PersonasService) {}

  @Post()
  @RequirePermissions(Permission.PERSONAS_MANAGE)
  @PgConstraints()
  create(@Body() createPersonaDto: CreatePersonaDto) {
    return this.personasService.create(createPersonaDto);
  }

  @Get()
  @RequirePermissions(Permission.PERSONAS_VIEW)
  @PgConstraints()
  findAll(
    @Query(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    query: QueryPersonaDto,
    @Pagination() paging: PaginationDto,
  ): Promise<Paginated<Persona[]>> {
    return this.personasService.findAll(paging, query);
  }

  @Get(':id')
  @RequirePermissions(Permission.PERSONAS_VIEW)
  @PgConstraints()
  findOne(@Param('id') id: string) {
    return this.personasService.findOne(id);
  }

  @Get('users/:userId')
  @RequirePermissions(Permission.PERSONAS_VIEW)
  findByUserId(@Param('userId') userId: string) {
    return this.personasService.findByUserId(userId);
  }

  @Patch(':id')
  @RequirePermissions(Permission.PERSONAS_MANAGE)
  @PgConstraints()
  update(@Param('id') id: string, @Body() updatePersonaDto: UpdatePersonaDto) {
    return this.personasService.update(id, updatePersonaDto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.PERSONAS_MANAGE)
  remove(@Param('id') id: string) {
    return this.personasService.remove(id);
  }
}
