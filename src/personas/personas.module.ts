import { Module } from '@nestjs/common';
import { PersonasService } from './personas.service';
import { PersonasController } from './personas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Persona } from '@entities/persona.entity';
import { User } from '@entities/user.entity';
import { File } from '@entities/file.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Persona]), TypeOrmModule.forFeature([User]), TypeOrmModule.forFeature([File])],
  controllers: [PersonasController],
  providers: [PersonasService],
})
export class PersonasModule {}
