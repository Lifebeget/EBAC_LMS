import { ConfigInstance } from '../../config';
import { Request, Response, NextFunction } from 'express';

export function MultidomainsMiddleware(req: Request, res: Response, next: NextFunction) {
  ConfigInstance.getInstance().setDefault(req.get('host'));
  next();
}
