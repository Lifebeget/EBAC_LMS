export type Root = Root2[];

export interface Root2 {
  id: string;
  type: string;
  attributes: Attributes;
  lectures: Lecture[];
}

export interface Attributes {
  title: string;
  description: string;
  position: number;
}

export interface Lecture {
  id: string;
  type: string;
  attributes: Attributes2;
  attendances: any[];
  contents: Content[];
  simpleContents: SimpleContent[];
  subscriptions: any[];
  teams: any[];
  course: Course;
  module: Module;
  permissions: Permission[];
}

export interface Attributes2 {
  createdAt: string;
  slug: string;
  description: any;
  position: number;
  title: string;
  updatedAt: string;
  extra: boolean;
  minimumGrade: number;
  publishedAt: string;
  alwaysReleased: boolean;
  releasesAt: any;
  releasesAtFuture: any;
  releasesIn: any;
  showForum: boolean;
  free: boolean;
  private: boolean;
  amountOfContents: number;
  blockCopy: boolean;
}

export interface Content {
  id: string;
  type: string;
  attributes: Attributes3;
  survey?: Survey;
  assessment?: Assessment;
}

export interface Attributes3 {
  title: string;
  extra?: boolean;
  processing: boolean;
  position: number;
  createdAt?: string;
  updatedAt: string;
  blockCopy: boolean;
  invalidSparkVideo: boolean;
  viewLimit: any;
  encodedUrl?: string;
  downloadable?: boolean;
  unencodedUrl?: string;
  subtitle: any;
  videoLimitEnabled?: boolean;
  thumbnail?: string;
  file?: string;
  viewUrl?: string;
  sparkVideosId?: string;
  sparkVideosUuid?: string;
}

export interface Survey {
  id: string;
  type: string;
  attributes: Attributes4;
  questions: Question[];
}

export interface Attributes4 {
  title: string;
  extra: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface Question {
  id: string;
  type: string;
}

export interface Assessment {
  id: string;
  type: string;
  attributes: Attributes5;
  questions: Question2[];
}

export interface Attributes5 {
  title: string;
  extra: any;
  manual: boolean;
  randomQuestions: number;
  deadline: number;
  redos: number;
  showMultipleChoiceResult: string;
  mediated: any;
  startsAt: any;
  endsAt: any;
  weight: number;
  maximumGrade: boolean;
  neverRequireCorrection: boolean;
  createdAt: string;
  updatedAt: string;
  canMediate: boolean;
}

export interface Question2 {
  id: string;
  type: string;
}

export interface SimpleContent {
  id: string;
  type: string;
  attributes: Attributes6;
  survey?: Survey2;
  assessment?: Assessment2;
}

export interface Attributes6 {
  title: string;
  extra?: boolean;
  processing: boolean;
  position: number;
  createdAt?: string;
  updatedAt: string;
  blockCopy: boolean;
  invalidSparkVideo: boolean;
  viewLimit: any;
  encodedUrl?: string;
  downloadable?: boolean;
  unencodedUrl?: string;
  subtitle: any;
  videoLimitEnabled?: boolean;
  thumbnail?: string;
  file?: string;
  viewUrl?: string;
  sparkVideosId?: string;
  sparkVideosUuid?: string;
}

export interface Survey2 {
  id: string;
  type: string;
  attributes: Attributes7;
  questions: Question3[];
}

export interface Attributes7 {
  title: string;
  extra: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface Question3 {
  id: string;
  type: string;
}

export interface Assessment2 {
  id: string;
  type: string;
  attributes: Attributes8;
  questions: Question4[];
}

export interface Attributes8 {
  title: string;
  extra: any;
  manual: boolean;
  randomQuestions: number;
  deadline: number;
  redos: number;
  showMultipleChoiceResult: string;
  mediated: any;
  startsAt: any;
  endsAt: any;
  weight: number;
  maximumGrade: boolean;
  neverRequireCorrection: boolean;
  createdAt: string;
  updatedAt: string;
  canMediate: boolean;
}

export interface Question4 {
  id: string;
  type: string;
}

export interface Course {
  id: string;
  type: string;
}

export interface Module {
  id: string;
  type: string;
}

export interface Permission {
  id: string;
  type: string;
}
