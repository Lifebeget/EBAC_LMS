/* eslint-disable @typescript-eslint/no-namespace */

export namespace Response {
  export interface Init {
    data: ForumData;
  }

  export interface ForumData {
    topics: Topic[];
    total: number;
    total_pages: number;
  }

  interface Topic {
    id: string;
    saas_id: string;
    lecture_id: string | null;
    content_id: string | null;
    user_id: string;
    created_at: string;
    updated_at: string;
    title: string;
    slug: string;
    from_content: string;

    original_post: Post;

    posts: Post[];

    forum_id: string;
    score: number;
    locked?: boolean;
    sticky?: boolean;
    can_stick?: boolean;
    can_unstick?: boolean;
    can_lock?: boolean;
    can_unlock?: boolean;
    can_edit?: boolean;
    can_remove?: boolean;
    is_up_voted?: boolean;
    is_down_voted?: boolean;
    is_reply?: boolean;
  }

  interface Post {
    id: string;
    created_at: string;
    updated_at: string;
    body: string;
    replies: Reply[];
    topic_id: string;
    forum_id: string;
    user_name: string;
    user_avatar: {
      url: string;
    };
    score: number;
    can_edit: boolean;
    can_remove: boolean;
    is_up_voted: boolean;
    is_down_voted: boolean;
    is_reply: boolean;
  }

  interface Reply extends Omit<Post, 'replies'> {
    post_id?: string; // parent topic id
  }
}
