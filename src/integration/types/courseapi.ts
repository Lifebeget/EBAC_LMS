export interface Root {
  data: Modules[];
  included: Included[];
}

export interface Modules {
  id: string;
  type: string;
  attributes: Attributes;
  relationships: Relationships;
}

export interface Attributes {
  title: string;
  description: string;
  position: number;
  slug?: string;
}

export interface Relationships {
  lectures: Lectures;
}

export interface Lectures {
  data: Daum2[];
}

export interface Daum2 {
  id: string;
  type: string;
}

export interface Included {
  id: string;
  type: string;
  attributes: Attributes2;
  relationships?: Relationships2;
}

export interface Attributes2 {
  createdAt?: string;
  slug?: string;
  description: any;
  position?: number;
  title: string;
  updatedAt: string;
  extra?: boolean;
  minimumGrade?: number;
  publishedAt?: string;
  alwaysReleased?: boolean;
  releasesAt: any;
  releasesAtFuture: any;
  releasesIn: any;
  showForum?: boolean;
  free?: boolean;
  private?: boolean;
  amountOfContents?: number;
  blockCopy?: boolean;
  processing?: boolean;
  invalidSparkVideo?: boolean;
  viewLimit: any;
  encodedUrl?: string;
  downloadable?: boolean;
  unencodedUrl?: string;
  subtitle: any;
  videoLimitEnabled?: boolean;
  thumbnail?: string;
  file?: string;
  viewUrl?: string;
  sparkVideosId?: string;
  sparkVideosUuid?: string;
  manual?: boolean;
  randomQuestions?: number;
  deadline?: number;
  redos?: number;
  showMultipleChoiceResult?: string;
  mediated: any;
  startsAt: any;
  endsAt: any;
  weight?: number;
  maximumGrade?: boolean;
  neverRequireCorrection?: boolean;
  canMediate?: boolean;
  'invalidSparkVideo…s'?: InvalidSparkVideoS;
  simpleContents?: SimpleContents;
  subscriptions?: Subscriptions;
  teams?: Teams;
  course?: Course;
  module?: Module;
  lectureStatus?: LectureStatus;
  permissions?: Permissions;
}

export interface InvalidSparkVideoS {
  data: Daum3[];
}

export interface Daum3 {
  id: string;
  type: string;
}

export interface SimpleContents {
  data: Daum4[];
}

export interface Daum4 {
  id: string;
  type: string;
}

export interface Subscriptions {
  data: any[];
}

export interface Teams {
  data: any[];
}

export interface Course {
  data: Data;
}

export interface Data {
  id: string;
  type: string;
}

export interface Module {
  data: Data2;
}

export interface Data2 {
  id: string;
  type: string;
}

export interface LectureStatus {
  data: any;
}

export interface Permissions {
  data: Daum5[];
}

export interface Daum5 {
  id: string;
  type: string;
}

export interface Relationships2 {
  questions?: Questions;
  contentStatus?: ContentStatus;
  survey?: Survey;
  attendances?: Attendances;
  contents?: Contents;
  simpleContents?: SimpleContents2;
  subscriptions?: Subscriptions2;
  teams?: Teams2;
  course?: Course2;
  module?: Module2;
  lectureStatus?: LectureStatus2;
  permissions?: Permissions2;
  assessment?: Assessment;
}

export interface Questions {
  data: Daum6[];
}

export interface Daum6 {
  id: string;
  type: string;
}

export interface ContentStatus {
  data: any;
}

export interface Survey {
  data: Data3;
}

export interface Data3 {
  id: string;
  type: string;
}

export interface Attendances {
  data: any[];
}

export interface Contents {
  data: Daum7[];
}

export interface Daum7 {
  id: string;
  type: string;
}

export interface SimpleContents2 {
  data: Daum8[];
}

export interface Daum8 {
  id: string;
  type: string;
}

export interface Subscriptions2 {
  data: any[];
}

export interface Teams2 {
  data: any[];
}

export interface Course2 {
  data: Data4;
}

export interface Data4 {
  id: string;
  type: string;
}

export interface Module2 {
  data: Data5;
}

export interface Data5 {
  id: string;
  type: string;
}

export interface LectureStatus2 {
  data: any;
}

export interface Permissions2 {
  data: Daum9[];
}

export interface Daum9 {
  id: string;
  type: string;
}

export interface Assessment {
  data: Data6;
}

export interface Data6 {
  id: string;
  type: string;
}
