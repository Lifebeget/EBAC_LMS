/* eslint-disable @typescript-eslint/no-namespace */
export namespace Response {
  export interface Course {
    course_id: string;
    course_slug: string;
    title: string;
  }

  export interface Lecture {
    lecture_id: string;
    lecture_slug: string;
    title: string;
  }

  export interface Survey {
    survey_id: string;
    title: string;
  }

  export interface User {
    user_id: string;
    user_slug: string;
    email: string;
    name: string;
  }

  export interface Question {
    question_id: string;
    body: string;
    type: string;
  }

  export interface Choice {
    choice_id: string;
    body: string;
  }

  export interface Answer {
    question: Question;
    choice: Choice;
    body: string;
  }

  export interface SurveysResponse {
    user_survey_id: string;
    course: Course;
    lecture: Lecture;
    survey: Survey;
    user: User;
    answers: Answer[];
  }
}
