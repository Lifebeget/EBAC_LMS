/* eslint-disable @typescript-eslint/no-namespace */
declare namespace CourseResponse {
  export interface Chat {
    chat_id: string;
    channel: string;
    messages: any[];
  }

  export interface Owner {
    name: string;
    avatar: string;
  }

  export interface Module {
    module_id: string;
    title: string;
    description: string;
  }

  export interface Content {
    title: string;
  }

  export interface Lecture {
    lecture_id: string;
    lecture_slug: string;
    title: string;
    description?: any;
    module: Module;
    contents: Content[];
  }

  export interface Module2 {
    module_slug: string;
    title: string;
    description: string;
    url: string;
  }

  export interface Avatar {
    url: string;
  }

  export interface User {
    name: string;
    slug: string;
    avatar: Avatar;
  }

  export interface Tutor {
    user: User;
  }

  export interface CoursesData {
    course_id: string;
    course_slug: string;
    title: string;
    is_paid: boolean;
    price: number;
    topics: string[];
    disable_certificate: boolean;
    workload: number;
    hours: number;
    minutes: number;
    objective: string;
    target_audience: string;
    certification: string;
    methodology: string;
    published: boolean;
    show_forum: boolean;
    hide_video_progress_bar: boolean;
    chat: Chat;
    owner: Owner;
    category: string;
    lectures: Lecture[];
    modules: Module2[];
    tutors: Tutor[];
    comment_rating?: any;
    skip_course_home: boolean;
    first_content_path: string;
    subscribable: boolean;
    requestable: boolean;
    has_forum: boolean;
    has_chat: boolean;
    can_clear_chat: boolean;
    full: boolean;
    description: string;
    logo: string;
    banner: string;
    has_custom_certificate: boolean;
    url: string;
  }
}

export default CourseResponse;
