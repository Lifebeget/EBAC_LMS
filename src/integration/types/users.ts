/* eslint-disable @typescript-eslint/no-namespace */

declare namespace usersResponse {
  export interface SaasPermissions {
    _id: string;
    extensions: string[];
    name: string;
  }

  export interface Group {
    slug: string;
    saas_permissions: SaasPermissions;
  }

  export interface User {
    user_id: string;
    user_slug: string;
    auth_token: string;
    active: boolean;
    email: string;
    name: string;
    tags: string;
    biography: string;
    cover_color: string;
    last_badges_points: number;
    document_id: string;
    mobile_phone: number;
    home_phone: number;
    profession: string;
    street: string;
    number: string;
    complement: string;
    neighborhood: string;
    zip: string;
    city: string;
    state: string;
    country: string;
    group: Group;
    campaign_id?: any;
    created_at: Date;
    subscribed_courses_count: number;
  }
}

export default usersResponse;
