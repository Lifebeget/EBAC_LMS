/* eslint-disable @typescript-eslint/no-namespace */
export namespace surveyapi {
  export interface Attributes {
    title: string;
    extra: boolean;
    createdAt: Date;
    updatedAt: Date;
  }

  export interface Datum {
    id: string;
    type: string;
  }

  export interface Questions {
    data: Datum[];
  }

  export interface Relationships {
    questions: Questions;
  }

  export interface Data {
    id: string;
    type: string;
    attributes: Attributes;
    relationships: Relationships;
  }

  export interface Attributes2 {
    body: string;
    percentage?: number;
  }

  export interface Datum2 {
    id: string;
    type: string;
  }

  export interface Choices {
    data: Datum2[];
  }

  export interface Relationships2 {
    choices: Choices;
  }

  export interface Included {
    id: string;
    type: string;
    attributes: Attributes2;
    relationships: Relationships2;
  }

  export interface RootObject {
    data: Data;
    included: Included[];
  }
}
