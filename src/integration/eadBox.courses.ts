import { INestApplicationContext } from '@nestjs/common';
import { CreateCourseDto } from 'src/courses/dto/create-course.dto';
import { CoursesService } from 'src/courses/courses.service';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { counterTemplate } from '../lib/types/ImportCounters';
import { UpdateCourseDto } from 'src/courses/dto/update-course.dto';

export class EadBoxCourses {
  private coursesService: CoursesService;
  constructor(private context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.coursesService = context.get(CoursesService);
  }

  async import(pagesLimit: number = null) {
    const courses = await this.download.downloadByPage({
      name: 'courses',
      fnClear: null,
      fnGet: (page: number) => this.download.get('/admin/courses', { page }),
      fnSave: async (courses: any[]) => this.save(courses),
      lastPage: pagesLimit,
    });
    return {
      courses,
      stats: this.counter,
    };
  }

  async saveOne(eadCourse: any, sort: number) {
    const dto = new CreateCourseDto();

    dto.slug = eadCourse.course_slug;
    dto.sort = sort;
    dto.title = eadCourse.title;
    dto.active = true;
    // dto.published = eadCourse.published ? true : false;
    dto.published = false;
    dto.externalId = eadCourse.course_id;
    dto.description = eadCourse.description ?? 'no description';
    dto.publishedAt = eadCourse.published_at;

    const entity = await this.coursesService.create(dto);

    return {
      ...eadCourse,
      entity,
    };
  }

  async updateOne(dbCourse, eadCourse: any, sort: number) {
    const dto = new UpdateCourseDto();

    if (dbCourse.slug != eadCourse.course_slug) {
      dto.slug = eadCourse.course_slug;
    }
    if (dbCourse.sort != sort) {
      dto.sort = sort;
    }
    if (dbCourse.title != eadCourse.title) {
      dto.title = eadCourse.title;
    }
    // const published = eadCourse.published ? true : false;
    // if (dbCourse.published != published) {
    //   dto.published = published;
    // }
    const description = eadCourse.description ?? '';
    if (dbCourse.description != description) {
      dto.description = description;
    }
    if (dbCourse.sort != sort) {
      dto.sort = sort;
    }

    let entity = dbCourse;
    let updated = false;
    if (Object.keys(dto).length > 0) {
      entity = await this.coursesService.update(dbCourse.id, dto);
      updated = true;
    }

    return {
      ...eadCourse,
      entity,
      updated,
    };
  }

  async save(eadCourses: any[]) {
    const result = [];
    for (let i = 0, l = eadCourses.length; i < l; i++) {
      const eadCourse = eadCourses[i];
      const dbCourse = await this.coursesService.findByExternalId(eadCourse.course_id);
      const sort = (i + 1) * 10;
      if (dbCourse) {
        const updated = await this.updateOne(dbCourse, eadCourse, sort);
        result.push(updated);
        if (updated.updated) {
          this.counter.updated++;
        } else {
          this.counter.skipped++;
        }
      } else {
        const created = await this.saveOne(eadCourse, sort);
        result.push(created);
        this.counter.created++;
      }
    }
    return result;
  }
}
