import { INestApplicationContext } from '@nestjs/common';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { counterTemplate } from '../lib/types/ImportCounters';
import { CoursesService } from 'src/courses/courses.service';
import { UsersService } from 'src/users/users.service';
import { FilesService } from 'src/files/files.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { ForumService } from 'src/forum/forum.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import axios from 'axios';
import promisePool from 'src/_util/async-bottleneck';
import { Course } from 'src/lib/entities/course.entity';
import { CreateMessageImportDto } from 'src/forum/dto/create-forum-message-import.dto';
import { User } from '@entities/user.entity';
import * as dayjs from 'dayjs';

interface UserT {
  externalId?: string;
  userAvatar?: string;
  name: string;
  tag?: string;
  userId?: string;
}
type UserMapT = Map<string, UserT>;

export class EadBoxForums {
  private coursesService: CoursesService;
  private forumService: ForumService;
  private submissionsService: SubmissionsService;
  private usersService: UsersService;
  private filesService: FilesService;
  private eadboxSessionCookie: string;
  private courcesMap: Map<string, string>;
  private lectureMap: Map<string, string>;
  private fixedUsers: {
    EBAC: User;
    NONAME: User;
  };
  private debug: boolean;
  enrollmentService: any;
  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.coursesService = context.get(CoursesService);
    this.submissionsService = context.get(SubmissionsService);
    this.forumService = context.get(ForumService);
    this.filesService = context.get(FilesService);
    this.usersService = context.get(UsersService);
    this.enrollmentService = context.get(EnrollmentsService);
    this.fixedUsers = {
      EBAC: null,
      NONAME: null,
    };
    this.debug = true;
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async import(concurrency = 3, debug = true) {
    this.debug = debug;
    await this.startSession();
    await this.loadFixedUsers();

    const courses = await this.coursesService.findAll(false, true, null, null, true);
    this.courcesMap = new Map(courses.filter(e => e.externalId).map(cource => [cource.externalId, cource.id]));
    this.lectureMap = new Map(
      courses
        .reduce((a, cource) => (a.push(...cource.modules), a), [])
        .reduce((a, module) => (a.push(...module.lectures), a), [])
        .map(e => [e.externalId, e.id]),
    );

    await promisePool(courses, this.importForum.bind(this), concurrency);

    return this.counter;
  }

  async loadFixedUsers() {
    const usersRepository = this.usersService.getUsersRepository();
    this.fixedUsers.EBAC = await usersRepository.findOne({
      email: 'online@ebac.art.br',
    });
    if (!this.fixedUsers.EBAC) throw 'not fount fixed EBAC user by email online@ebac.art.br';
    this.fixedUsers.NONAME = await usersRepository.findOne({
      email: 'EBACUser@1.ru',
    });
    if (!this.fixedUsers.NONAME) {
      const res = await usersRepository.insert({
        name: 'EBAC User',
        email: 'EBACUser@1.ru',
        active: true,
      });
      this.fixedUsers.NONAME = res.raw[0];
    }
  }

  async donwloadForumAll(it: Course) {
    const boxApiUrl = `https://ebac-online.eadbox.com.br/ng/api/forums/${it.slug}/topics.json`;

    const result = [];
    let pageCounter = 1;
    while (true) {
      try {
        const data = await axios
          .get<import('./types/forum').Response.ForumData>(boxApiUrl, {
            headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
            params: {
              per_page: 10,
              page: pageCounter++,
            },
          })
          .catch(err => {
            console.error('import forum', it.slug, err);
          })
          .then(it => it && it.data);

        if (!data || !data?.topics?.length) {
          break;
        }

        result.push(...data.topics);
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }

  fillUserIdByAvatar(e) {
    if (!e.user_id && e.user_avatar?.url?.includes('system/uploads/user/avatar')) {
      e.user_id = e.user_avatar?.url?.match(/user\/avatar\/(?<userId>(.*))\//)?.groups?.userId;
    }
    return e;
  }

  sortUsers(topicsArray): [UserMapT, UserMapT] {
    const users: UserMapT = new Map<string, UserT>();
    const usersNoname: UserMapT = new Map<string, UserT>();

    topicsArray
      .map(
        e =>
          ({
            externalId: e.user_id,
            userAvatar: e.user_avatar?.url,
            name: e.user_name,
            tag: `${e.user_name}=${e.user_avatar?.url}`,
          } as UserT),
      )
      .filter(e => {
        if (e.name === 'EBAC Online') e.externalId = this.fixedUsers.EBAC.externalId;

        if (e.externalId) {
          users.set(e.tag, e);
          return false;
        }
        return true;
      })
      .filter(e => {
        if (users.has(e.tag)) {
          return false;
        }
        usersNoname.set(e.name, e);
      });

    return [users, usersNoname];
  }

  async fillByUniqueNames(users, usersNoname) {
    if (!usersNoname.length) return;
    const userNames = Array.from(usersNoname.values()).map((e: UserT) => e.name);
    const usersDB = await this.usersService.eadBoxUsersUniqueByName(userNames);
    usersDB
      .map(e => e.name)
      .forEach((e, i, ar) => {
        if (ar.indexOf(e) == ar.lastIndexOf(e)) {
          const m = usersNoname.get(e);

          m.externalId = usersDB[i].externalId;
          m.userId = usersDB[i].id;
          users.set(`${m.name}=${m.userAvatar}`, m);
          usersNoname.delete(e);
        }
      });
  }

  async fillByEnroll(users, usersNoname, courseId) {
    if (!usersNoname.length) return;
    const userNames = Array.from(usersNoname.values()).map((e: UserT) => e.name);
    const usersDB = await this.usersService.eadBoxUsersEnrollToCourseByName(userNames, courseId);
    usersDB
      .map(e => e.name)
      .forEach((e, i, ar) => {
        if (ar.indexOf(e) == ar.lastIndexOf(e)) {
          const m = usersNoname.get(e);

          m.externalId = usersDB[i].externalId;
          m.userId = usersDB[i].id;
          users.set(`${m.name}=${m.userAvatar}`, m);
          usersNoname.delete(e);
        }
      });
  }

  async createAvas(usersDB, usersMap) {
    const create = usersDB.filter(e => !e.avatar);
    if (!create.length) return;

    const files = create.map(e => {
      const user = usersMap.get(e.externalId);
      const parse = user.userAvatar.match(/(.*)\/(?<filename>(.+))\.(?<ext>(.*))/);
      return {
        url: user.userAvatar,
        title: parse.groups.filename,
        extension: parse.groups.ext,
        filename: parse.groups.filename,
        owner: { id: e.id },
      };
    });

    const avas = await this.filesService.saveMany(files);
    const avasMap = new Map(avas.map(e => [e.owner.id, e.id]));

    await this.usersService.getUsersRepository().save(
      create.map(e => {
        return {
          id: e.id,
          avatarId: avasMap.get(e.id),
        };
      }),
    );
  }

  async updateAvas(usersDB, usersMap) {
    const update = usersDB.filter(e => e.avatar && e.avatar.url !== usersMap.get(e.externalId).userAvatar);

    if (!update.length) return;

    const files = update.map(e => {
      const user = usersMap.get(e.externalId);
      const parse = user.userAvatar.match(/(.*)\/(?<filename>(.+))\.(?<ext>(.*))/);
      return {
        id: e.id,
        path: null,
        mimetype: null,
        size: null,
        url: user.userAvatar,
        title: parse.groups.filename,
        extension: parse.groups.ext,
        filename: parse.groups.filename,
      };
    });

    await this.filesService.saveMany(files);
  }

  async updateAvatarsFillUserId(users) {
    const usersMap: UserMapT = new Map<string, UserT>(Array.from(users.values()).map((e: UserT) => [e.externalId, e]));
    const usersDB: User[] = await this.usersService.eadBoxFindManyByExternalIds(Array.from(usersMap.keys()));

    usersDB.forEach(e => (usersMap.get(e.externalId).userId = e.id));

    await this.createAvas(usersDB, usersMap);
    await this.updateAvas(usersDB, usersMap);
  }

  async guessUsersId(topicsArray, courseId) {
    const [users, usersNoname] = this.sortUsers(topicsArray);

    await this.fillByUniqueNames(users, usersNoname);
    await this.fillByEnroll(users, usersNoname, courseId);

    await this.updateAvatarsFillUserId(users);

    return [users, usersNoname];
  }

  async getDBMessages(result: any) {
    // tree to line stucture
    const topicsArray = result
      .reduce((a, e) => a.concat(e.original_post, ...e.posts.reduce((a, e) => a.concat(e, ...e.replies), [])), [])
      .map(this.fillUserIdByAvatar);
    const externalIds = topicsArray.map(e => e.id);
    const topicDB = await this.forumService.findManyByExternalId(externalIds);
    const topicDBMap = new Map(topicDB.map(e => [e.externalId, e]));

    return [topicsArray, topicDBMap];
  }

  async upsertThread(courseId: string, topic: any, topicDBMap: any, users: UserMapT) {
    let entity;
    const message = topicDBMap.get(topic.original_post.id);
    const originalPost = topic.original_post;
    const user: UserT = users.get(`${originalPost.user_name}=${originalPost.user_avatar.url}`);
    let userId = user?.userId;
    if (!userId) {
      userId = this.fixedUsers.NONAME.id;
      originalPost.body += `<p>${originalPost.user_name}</p>`;
    }

    if (message) {
      //update
      const dto = new CreateMessageImportDto();
      const promises = [];
      if (message.title != topic.title) dto.title = topic.title;
      if (message.body != originalPost.body && dayjs(originalPost.updated_at).diff(message.modified)) dto.body = originalPost.body;
      if (message.score != originalPost.score) dto.score = originalPost.score;
      if (message.sticky != topic.sticky) dto.sticky = topic.sticky;

      if (dayjs(originalPost.created_at).diff(message.created)) {
        dto.created = originalPost.created_at;
      }
      if (dayjs(originalPost.updated_at).diff(message.modified)) {
        dto.modified = originalPost.updated_at;
      }

      if (message.userId != userId) {
        dto.userId = userId;
        promises.push(
          this.forumService.unsubscribe(message.id, message.userId),
          this.forumService.subscribe(message.id, userId, message.threadParent?.id || message.id),
        );
      }

      if (Object.keys(dto).length > 0) {
        dto.id = message.id;
        [entity] = await Promise.all([this.forumService.update(dto.id, dto), ...promises]);

        this.counter.updated++;
      } else {
        this.counter.skipped++;
        entity = message;
      }
    } else {
      //create
      const dto = new CreateMessageImportDto();

      dto.externalId = originalPost.id;
      dto.title = topic.title;
      dto.body = originalPost.body;

      dto.score = originalPost.score;
      dto.parentId = null;
      dto.sticky = topic.sticky;

      dto.created = originalPost.created_at;
      dto.modified = originalPost.updated_at;

      dto.lectureId = this.lectureMap.get(topic.lecture_id);
      dto.courseId = courseId;
      dto.userId = userId;

      entity = await this.forumService.createImport(dto, dto.userId);

      this.counter.created++;
    }
    return entity;
  }

  async upsertTopic(courseId: string, topic: any, topicDBMap: any, users: UserMapT, parentId: string, level = 1) {
    let entity;
    const message = topicDBMap.get(topic.id);

    const user: UserT = users.get(`${topic.user_name}=${topic.user_avatar.url}`);

    let userId = user?.userId;

    if (level == 2) topic.body += `<p>reply to @${topic.user_name}</p>`;

    if (!userId) {
      userId = this.fixedUsers.NONAME.id;
      topic.body += `<p>${topic.user_name}</p>`;
    }

    if (message) {
      //update
      const dto = new CreateMessageImportDto();
      const promises = [];
      if (message.title != topic.title) dto.title = topic.title;
      if (message.body != topic.body) dto.body = topic.body;
      if (message.score != topic.score) dto.score = topic.score;

      if (dayjs(topic.created_at).diff(message.created)) {
        dto.created = topic.created_at;
      }
      if (dayjs(topic.updated_at).diff(message.modified)) {
        dto.modified = topic.updated_at;
      }

      if (message.userId != userId) {
        dto.userId = userId;
        promises.push(
          this.forumService.unsubscribe(message.id, message.userId),
          this.forumService.subscribe(message.id, userId, message.threadParent?.id || message.id),
        );
      }

      if (Object.keys(dto).length > 0) {
        dto.id = message.id;
        dto.parentId = parentId;
        [entity] = await Promise.all([this.forumService.update(dto.id, dto), ...promises]);
        this.counter.updated++;
      } else {
        this.counter.skipped++;
        entity = message;
      }
    } else {
      //create
      const dto = new CreateMessageImportDto();
      dto.parentId = parentId;

      dto.externalId = topic.id;
      dto.title = topic.title;
      dto.body = topic.body;

      dto.score = topic.score;
      dto.parentId = parentId;
      dto.sticky = topic.sticky;

      dto.created = topic.created_at;
      dto.modified = topic.updated_at;

      dto.lectureId = this.lectureMap.get(topic.lecture_id);
      dto.courseId = courseId;
      dto.userId = userId;

      entity = await this.forumService.createImport(dto, dto.userId);

      this.counter.created++;
    }
    return entity;
  }

  async importForum(course: Course) {
    try {
      let start = Date.now();
      let result = [];
      result = await this.donwloadForumAll(course);

      this.debug && console.log(`${course.id} Load Forum ${Date.now() - start}ms threads = ${result.length}`);

      if (!result.length) return;
      start = Date.now();

      const [topicsArray, topicDBMap] = await this.getDBMessages(result);
      this.debug && console.log(`${course.id} messages = ${topicsArray.length}`);

      const [users] = await this.guessUsersId(topicsArray, course.id);

      // thread
      for (let index = 0; index < result.length; index++) {
        const topic = result[index];
        const thread = await this.upsertThread(course.id, topic, topicDBMap, users);

        // level 1
        if (topic.posts?.length) {
          for (let index = 0; index < topic.posts.length; index++) {
            const post = topic.posts[index];
            const topicLevel1 = await this.upsertTopic(course.id, post, topicDBMap, users, thread.id);

            // level 2
            if (post.replies?.length) {
              for (let index = 0; index < post.replies.length; index++) {
                const reply = post.replies[index];
                await this.upsertTopic(course.id, reply, topicDBMap, users, topicLevel1.id, 2);
              }
            }
          }
        }
      }
      this.debug && console.log(course.id, 'Save', Date.now() - start);
    } catch (error) {
      console.error(`import forum course ${course.id} ${course.title}`, error);
    }
  }
}
