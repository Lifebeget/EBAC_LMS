import { File } from '@entities/file.entity';
import { INestApplicationContext } from '@nestjs/common';
import { AnswersService } from 'src/assessments/answers.service';
import { FilesService } from 'src/files/files.service';
import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { promises as fs } from 'fs';

export class EadBoxDeleteFiles {
  private filesService: FilesService;
  private answersService: AnswersService;
  private eadboxSessionCookie: string;
  private debug: boolean;

  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate, size: 0 }) {
    this.filesService = context.get(FilesService);
    this.answersService = context.get(AnswersService);

    this.debug = true;
  }

  async import(concurrency = 1, debug = true) {
    this.debug = debug;
    const files = await this.filesService.findAllDeleted();
    const filesS3 = [];
    const filesLocal = [];
    const filesEadBox = [];
    const filesNone = [];

    files.forEach(f => {
      if (f.s3Key) filesS3.push(f);
      else if (f.url.includes('media.eadbox.com')) filesEadBox.push(f);
      else if (f.url.includes('/files/public/files/')) filesLocal.push(f);
      else filesNone.push(f);
    });

    await this.removeEadox(filesEadBox);
    await this.removeLocal(filesLocal, concurrency);
    await this.removeS3(filesS3);

    if (filesNone.length) {
      console.error(
        'wierd storage type of file',
        filesNone.map(e => e.id),
      );
    }

    return this.counter;
  }

  async removeEadox(files: File[]) {
    try {
      if (!files.length) return;

      await this.filesService.removePermanentMany(files.map(e => e.id));
    } catch (error) {
      console.error(`removeEadox:`, error);
    }
  }

  async removeLocal(files: File[], concurrency = 1) {
    try {
      if (!files.length) return;
      await promisePool(files, this.removeLocalOne.bind(this), concurrency);
    } catch (error) {
      console.error(`removeEadox:`, error);
    }
  }

  async removeLocalOne(file: File) {
    try {
      const name = file.path || file.url.split('/files/public/files/')[1];
      await fs.unlink(name);
      await this.filesService.removePermanentMany([file.id]);
    } catch (error) {
      console.error(`removeLocalOne:`, error);
    }
  }

  async removeS3(files: File[]) {
    if (!files.length) return;
    const count = files.length;
    const start = Date.now();

    do {
      const part = files.splice(0, 1000);
      try {
        await this.filesService.deleteManyS3(part);
      } catch (error) {
        console.error(`deleteManyS3:`, error);
      }
    } while (files.length);

    this.counter.updated += count;

    this.debug && console.log(`Deleted ${count} files`, Date.now() - start);
  }
}
