import { INestApplicationContext } from '@nestjs/common';
import { CoursesService } from 'src/courses/courses.service';
import { CreateLectureDto } from 'src/courses/dto/create-lecture.dto';
import { CreateModuleDto } from 'src/courses/dto/create-module.dto';
import { UpdateLectureDto } from 'src/courses/dto/update-lecture.dto';
import { LecturesService } from 'src/courses/lectures.service';
import { ModulesService } from 'src/courses/modules.service';
import { Course } from 'src/lib/entities/course.entity';
import { Lecture } from 'src/lib/entities/lecture.entity';
import { Module } from 'src/lib/entities/module.entity';
import { EadBoxAssesments } from './eadBox.assesments';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxLectures {
  private lecturesService: LecturesService;
  private modulesService: ModulesService;
  private coursesService: CoursesService;
  private counter: {
    created: number;
    updated: number;
    skipped: number;
  };
  private assesments: EadBoxAssesments;

  constructor(private context: INestApplicationContext, private download: EadBoxDownloadHelper) {
    this.lecturesService = this.context.get(LecturesService);
    this.modulesService = this.context.get(ModulesService);
    this.coursesService = this.context.get(CoursesService);
    this.counter = {
      created: 0,
      updated: 0,
      skipped: 0,
    };
  }

  async createTestModule(course: Course) {
    const moduleDto = new CreateModuleDto();

    moduleDto.title = `(Placeholder module)`;
    moduleDto.published = true;
    moduleDto.sort = 10;
    moduleDto.active = true;
    moduleDto.externalId = null;
    moduleDto.publishedAt = new Date();

    return this.modulesService.create(course.id, moduleDto);
  }

  async import(courses) {
    for (let i = 0, l = courses.length; i < l; i++) {
      const eadCourse = courses[i];
      const eadLectures = eadCourse.lectures;
      const dbCourse = await this.coursesService.findByExternalId(eadCourse.course_id);
      if (!dbCourse) {
        console.log('Course is missing, skipping');
        this.counter.skipped += eadCourse.lectures.length;
        continue;
      }
      let module: Module;
      if (dbCourse.modules.length == 0) {
        module = await this.createTestModule(dbCourse);
      } else {
        module = dbCourse.modules[0];
      }

      for (let idx = 0, k = eadLectures.length; idx < k; idx++) {
        const eadLecture = eadLectures[idx];
        const dbLectures = dbCourse.modules.reduce((acc, cur) => acc.concat(cur.lectures), []);
        const dbLecture = dbLectures.find(l => l.externalId == eadLecture.lecture_id);
        const sort = (idx + 1) * 100;

        if (dbLecture) {
          const result = await this.updateOne(dbLecture, eadLecture, sort);
          if (result.updated) {
            this.counter.updated++;
          } else {
            this.counter.skipped++;
          }
        } else {
          await this.saveOne(dbCourse.id, module.id, eadLecture, sort);
        }
      }
    }
    return this.counter;
  }

  async saveOne(courseId, moduleId, lecture, sort) {
    const lectureDto = new CreateLectureDto();

    lectureDto.slug = lecture.lecture_slug;
    lectureDto.free = lecture.is_free ? true : false;
    lectureDto.sort = sort;
    lectureDto.title = lecture.title;
    lectureDto.active = true;
    lectureDto.courseId = courseId;
    lectureDto.moduleId = moduleId;

    // New lectures will be created unpublished as of 20 Sep 2021
    // lectureDto.published = lecture.published ? true : false;
    lectureDto.published = false;
    lectureDto.externalId = lecture.lecture_id;
    lectureDto.description = lecture.description ?? '';
    lectureDto.publishedAt = lecture.published_at;

    let entity: Lecture;
    try {
      entity = await this.lecturesService.create(lectureDto);
    } catch (err) {
      console.error('Cannot create lecture, probably slug error. Retrying with unique slug');
      console.dir(err);
      lectureDto.slug = lectureDto.slug + '_' + lectureDto.externalId;
      try {
        entity = await this.lecturesService.create(lectureDto);
      } catch (err) {
        console.error('Failed to create lecture');
        console.dir(err);
      }
    }

    return {
      ...lecture,
      entity,
    };
  }

  async updateOne(dbLecture: Lecture, lecture, sort) {
    const lectureDto = new UpdateLectureDto();

    if (dbLecture.slug != lecture.lecture_slug) {
      lectureDto.slug = lecture.lecture_slug;
    }
    const free = lecture.is_free ? true : false;
    if (dbLecture.free != free) {
      lectureDto.free = free;
    }
    if (dbLecture.sort != sort) {
      lectureDto.sort = sort;
    }
    if (dbLecture.title != lecture.title) {
      lectureDto.title = lecture.title;
    }

    // Ignore changes of Lecture published state as of 20 Sep 2021
    // const published = lecture.published ? true : false;
    // if (dbLecture.published != published) {
    //   lectureDto.published = published;
    // }

    const description = lecture.description ?? '';
    if (description && dbLecture.description != description) {
      lectureDto.description = description;
    }
    const publishedAt = lecture.published_at ? new Date(lecture.published_at) : null;
    if (dbLecture.publishedAt?.getTime() != publishedAt?.getTime()) {
      lectureDto.publishedAt = publishedAt;
    }
    let entity;
    let updated = false;
    if (Object.keys(lectureDto).length > 0) {
      try {
        entity = await this.lecturesService.update(dbLecture.id, lectureDto);
        updated = true;
      } catch (err) {
        console.error('Cannot update lecture, probably slug error. Skipping');
        console.dir(err);
        entity = dbLecture;
      }
    } else {
      entity = dbLecture;
    }

    return {
      ...lecture,
      entity,
      updated,
    };
  }
}
