import { INestApplicationContext } from '@nestjs/common';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { counterTemplate } from '../lib/types/ImportCounters';
import { AnswersService } from 'src/assessments/answers.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import axios from 'axios';
import { Answer } from 'src/lib/entities/answer.entity';
import promisePool from 'src/_util/async-bottleneck';

export class EadBoxComments {
  private answerService: AnswersService;
  private submissionsService: SubmissionsService;
  private eadboxSessionCookie: string;
  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.answerService = context.get(AnswersService);
    this.submissionsService = context.get(SubmissionsService);
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async import(concurrency = 3) {
    await this.startSession();

    const answers = await this.answerService.findForEadbox();
    await promisePool(answers, this.importAnswer.bind(this), concurrency);

    return this.counter;
  }

  async importAnswer(it: Answer) {
    const assessmentLink = this.submissionsService.generateAssessmentLink(it.submission);
    if (!assessmentLink) {
      console.error('Invalid Answer in input');
      console.dir(it.submission);
      return;
    }
    const boxApiUrl = assessmentLink
      .replace('/ng/', '/ng/api/v2/')
      .replace(';userId=', '?&user_id=')
      .replace('corrections', 'user_assessments');

    const boxCourse = await axios
      .get<import('../assessments/types/box').Response.Init>(boxApiUrl, {
        headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
      })
      .catch(() => false as const)
      .then(it => {
        return it && it.data;
      });

    if (!boxCourse) {
      console.error('Failed to fetch import course data');
      return;
    }

    const tutorComment = boxCourse.included.find(
      it => it.type === 'userAssessment--answer--freeForm' || it.type === 'userAssessment--answer--file',
    )?.attributes?.correctionNote;

    if (tutorComment) {
      console.log(`Updated answer: ${it.id}`);
      this.counter.updated++;

      await this.answerService.update(it.id, { tutorComment });
    } else {
      console.log(`Empty comment ${it.id}`);
      this.counter.skipped++;

      await this.answerService.update(it.id, { tutorComment: '' });
    }
  }
}
