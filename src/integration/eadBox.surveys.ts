/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Lecture } from '@entities/lecture.entity';
import { User } from '@entities/user.entity';
import { ContentType } from '@enums/content-type.enum';
import { Assessment, SingleChoiceAnswerI } from '@lib/api/types';
import { AssessmentGradingType, AssessmentType, QuestionType, ShowCorrectAnswers, ShowResult, SubmissionStatus } from '@lib/types/enums';
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { AnswersService } from 'src/assessments/answers.service';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { CreateAnswerDto } from 'src/assessments/dto/create-answer.dto';
import { CreateAssessmentDto } from 'src/assessments/dto/create-assessment.dto';
import { CreateQuestionDto } from 'src/assessments/dto/create-question.dto';
import { CreateSubmissionDto } from 'src/assessments/dto/create-submission.dto';
import { ImportSurveyLinkService } from 'src/assessments/import-survey-link.service';
import { QuestionsService } from 'src/assessments/questions.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { ContentService } from 'src/courses/content.service';
import { CoursesService } from 'src/courses/courses.service';
import { CreateContentDto } from 'src/courses/dto/create-content.dto';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { LecturesService } from 'src/courses/lectures.service';
import { FilesService } from 'src/files/files.service';
import { Course } from 'src/lib/entities/course.entity';
import { UsersService } from 'src/users/users.service';
import promisePool from 'src/_util/async-bottleneck';
import { In } from 'typeorm';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { Response as SurveysTypes } from './types/surveys';
import { surveyapi } from './types/surveysapi';
import { v4 as uuidv4 } from 'uuid';

type surveyFormatted = {
  id: string;
  title: string;
  questionIds?: string;
  questions: [
    {
      id: string;
      type: string;
      title: string;
      answers?: string[];
    },
  ];
};

function removeHtml(text) {
  return text.replace(/(<([^>]+)>)/gi, '');
}

export class EadBoxSurveys {
  private assessmentsService: AssessmentsService;
  private coursesService: CoursesService;
  private submissionsService: SubmissionsService;
  private usersService: UsersService;
  private importSurveyLinkService: ImportSurveyLinkService;
  private filesService: FilesService;
  private userService: UsersService;
  private answersService: AnswersService;
  private contentService: ContentService;
  private questionsService: QuestionsService;
  private lecturesService: LecturesService;
  private eadboxSessionCookie: string;
  private courcesMap: Map<string, string>;
  private lectureMap: Map<string, string>;
  private moduleMap: Map<string, string>;
  private concurrency: number;
  private fixedUsers: {
    EBAC: User;
    NONAME: User;
  };
  private debug: boolean;
  enrollmentService: any;
  constructor(
    context: INestApplicationContext,
    private download: EadBoxDownloadHelper,
    private counter = {
      ...counterTemplate,
      globalSurvey: 0,
      survey: 0,
      answers: 0,
      userNotFound: 0,
      questionNotFound: 0,
      answersNotFound: 0,
    },
  ) {
    this.assessmentsService = context.get(AssessmentsService);
    this.coursesService = context.get(CoursesService);
    this.submissionsService = context.get(SubmissionsService);
    this.filesService = context.get(FilesService);
    this.userService = context.get(UsersService);
    this.answersService = context.get(AnswersService);
    this.questionsService = context.get(QuestionsService);
    this.usersService = context.get(UsersService);
    this.lecturesService = context.get(LecturesService);
    this.enrollmentService = context.get(EnrollmentsService);
    this.importSurveyLinkService = context.get(ImportSurveyLinkService);
    this.contentService = context.get(ContentService);
    this.fixedUsers = {
      EBAC: null,
      NONAME: null,
    };
    this.debug = true;
    this.concurrency = 1;
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async donwloadSurveyApi(params) {
    const boxApiUrl = `https://ebac-online.eadbox.com.br/ng/api/v2/teacher/courses/${params.course_slug}/surveys/${params.survey_id}`;

    try {
      const data = await axios
        .get<import('./types/surveysapi').surveyapi.RootObject>(boxApiUrl, {
          headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
        })
        .catch(err => {
          console.error('import survey', params.course_slug, err);
        })
        .then(it => it && it.data);

      return data;
    } catch (error) {
      console.error(error);
    }
  }

  convertQuestionType(txt: string) {
    switch (txt) {
      case 'survey--question--multipleChoice':
        return QuestionType.SingleChoice;
      case 'survey--question--freeForm':
        return QuestionType.ShortAnswer;
      default:
        console.error('Unknown question type ', txt);
    }
  }

  async createLocalSurveys(answers: SurveysTypes.SurveysResponse[], surveyApi: surveyFormatted) {
    const lectureId = this.lectureMap.get(answers[0].lecture.lecture_id);
    const surveyId = surveyApi.id;

    const assessmentNew: CreateAssessmentDto = {
      lectureId,
      title: surveyApi.title,
      description: '',
      score: 0,
      passingScore: 0,
      timeLimitSeconds: 0,
      gradingType: AssessmentGradingType.None,
      externalId: surveyId,
      type: AssessmentType.Survey,
      isGlobal: false,
      timeDelaytoRetestHours: 0,
      showResult: ShowResult.onFinish,
      showCorrectAnswers: ShowCorrectAnswers.noShow,
      shuffleAnswers: false,
      shuffleQuestions: false,
    };

    const assessment = await this.assessmentsService.create(assessmentNew);

    const promises = [];
    surveyApi.questions.map((question, index) => {
      const answers =
        question.type == 'survey--question--freeForm'
          ? [
              {
                id: uuidv4(),
                isCorrect: true,
                value: '',
                isUserInput: true,
              },
            ]
          : question.answers.reduce((a, ans) => {
              const answer: SingleChoiceAnswerI = {
                id: uuidv4(),
                value: removeHtml(ans),
                isUserInput: false,
              };
              a.push(answer);
              return a;
            }, []);

      const questionDto: CreateQuestionDto = {
        assessmentId: assessment.id,
        title: removeHtml(question.title),
        description: '',
        score: 0,
        sort: index * 10,
        passingScore: 0,
        gradable: false,
        type: this.convertQuestionType(question.type),
        externalId: question.id,
      };
      if (answers) {
        questionDto.data = { answers };
      }
      promises.push(this.questionsService.create(questionDto));
    });
    await Promise.all(promises);

    const createContent: CreateContentDto = {
      type: ContentType.survey,
      active: true,
      assessmentId: assessment.id,
    };
    await this.contentService.create(lectureId, createContent);
    return this.assessmentsService.findOne(assessment.id);
  }

  async createGlobalSurvey(surveyApi: surveyFormatted) {
    const surveyId = surveyApi.id;

    const assessmentNew: CreateAssessmentDto = {
      lectureId: null,
      title: surveyApi.title,
      description: '',
      score: 0,
      passingScore: 0,
      timeLimitSeconds: 0,
      gradingType: AssessmentGradingType.None,
      externalId: surveyId,
      type: AssessmentType.Survey,
      isGlobal: true,
      timeDelaytoRetestHours: 0,
      showResult: ShowResult.onFinish,
      showCorrectAnswers: ShowCorrectAnswers.noShow,
      shuffleAnswers: false,
      shuffleQuestions: false,
      levelId: '',
    };

    const assessment = await this.assessmentsService.create(assessmentNew);

    const promises = [];
    surveyApi.questions.map((question, index) => {
      const answers =
        question.type == 'survey--question--freeForm'
          ? [
              {
                id: uuidv4(),
                isCorrect: true,
                value: '',
                isUserInput: true,
              },
            ]
          : question.answers.reduce((a, ans) => {
              const answer: SingleChoiceAnswerI = {
                id: uuidv4(),
                value: removeHtml(ans),
                isUserInput: false,
              };
              a.push(answer);
              return a;
            }, []);

      const questionDto: CreateQuestionDto = {
        assessmentId: assessment.id,
        title: removeHtml(question.title),
        description: '',
        score: 0,
        sort: index * 10,
        passingScore: 0,
        gradable: false,
        type: this.convertQuestionType(question.type),
        externalId: question.id,
      };
      if (answers) {
        questionDto.data = { answers };
      }
      promises.push(this.questionsService.create(questionDto));
    });
    await Promise.all(promises);

    return this.assessmentsService.findOne(assessment.id);
  }

  buildAnswer(assessment: Assessment, answerChunk: SurveysTypes.Answer[], externalId: string) {
    const questionDb = assessment.questions;
    const result = answerChunk
      .filter(q => !(q.question.type == 'multiple_choice' && !q.choice))
      .map(q => {
        const dto: CreateAnswerDto = {
          externalId: externalId,
          data: null,
          questionId: null,
        };

        const qtext = removeHtml(q.question.body).toLowerCase();
        const qdb = questionDb.find(e => removeHtml(e.title).toLowerCase() == qtext);
        if (!qdb) {
          this.counter.questionNotFound++;
          console.error('not found question', q, assessment.id);
          return null;
        }
        dto.questionId = qdb.id;
        if (q.question.type == 'multiple_choice') {
          const atext = removeHtml(q.choice.body).toLowerCase();
          const adb = qdb.data.answers.find(e => removeHtml(e.value).toLowerCase() == atext);
          if (!adb) {
            this.counter.answersNotFound++;
            console.error('not found answer', q, assessment.id);
            return null;
          }

          dto.data = {
            id: adb.id,
            value: adb.value,
          };
        } else {
          dto.data = {
            id: qdb.data.answers[0].id,
            value: qdb.data.answers[0].value,
            userInput: q.body,
          };
        }

        return dto;
      });

    return result.filter(e => e);
  }

  async importGlobalSurveys(answers: Record<string, SurveysTypes.SurveysResponse[]>, surveyApis: surveyFormatted[]) {
    const links = new Map();
    // пытаемся угадать глобальный assessment для группы
    const surveyDbs = await this.importSurveyLinkService.importSurveyLinkRepository
      .createQueryBuilder('survey')
      .select('survey.assessmentId, count(1)')
      .where({ surveyId: In(surveyApis.map(e => e.id)) })
      .groupBy('survey.assessmentId')
      .orderBy('count(1)', 'DESC')
      .limit(1)
      .getRawMany();

    let assessment = null;
    if (surveyDbs.length) {
      assessment = await this.assessmentsService.findOne(surveyDbs[0].assessment_id);
    } else {
      assessment = await this.createGlobalSurvey(surveyApis[0]);
    }

    for (let index = 0; index < surveyApis.length; index++) {
      const survey = surveyApis[index];
      const answersArray = answers[survey.id];

      for (let index2 = 0; index2 < answersArray.length; index2++) {
        this.counter.answers += answersArray.length;
        console.log(`[global survey ${index + 1}/${surveyApis.length}] import answers ${index2 + 1} / ${answersArray.length}`);
        const answer = answersArray[index2];

        const courseId = this.courcesMap.get(answer.course.course_id);
        const lectureId = this.lectureMap.get(answer.lecture.lecture_id);
        const moduleId = this.moduleMap.get(lectureId);
        const surveyId = survey.id;

        let surveyDb = links.get(`${surveyId}=${courseId}=${lectureId}`);
        if (!surveyDb)
          surveyDb = await this.importSurveyLinkService.importSurveyLinkRepository.findOne({
            surveyId,
            courseId,
            lectureId,
          });

        if (!surveyDb) {
          const createContent: CreateContentDto = {
            type: ContentType.globalSurvey,
            active: true,
            assessmentId: assessment.id,
          };
          await this.contentService.create(lectureId, createContent);

          surveyDb = await this.importSurveyLinkService.importSurveyLinkRepository.save({
            surveyId,
            courseId,
            lectureId,
            isGlobal: true,
            assessment,
          });
          links.set(`${surveyId}=${courseId}=${lectureId}`, surveyDb);
        }

        if (!answer.user) {
          console.log('NO USER IN ANSWER');
          continue;
        }

        const userDb = await this.userService.findOneByExernalId(answer.user.user_id);
        if (!userDb) {
          this.counter.userNotFound++;
          console.error('import user answer - not found user in DB', answer.user);
          continue;
        }

        await this.submissionsService.removeListByExternalId(answer.user_survey_id);

        const answersChunks = this.split(answer.answers, survey.questions.length);
        const now = new Date();
        for (let index = 0; index < answersChunks.length; index++) {
          const answerChunk = answersChunks[index];
          const answersData: any[] = this.buildAnswer(assessment, answerChunk, answer.user_survey_id);

          const submission: CreateSubmissionDto = {
            userId: userDb.id,
            assessmentId: assessment.id,
            lectureId,
            moduleId,
            courseId,
            status: SubmissionStatus.Graded,
            answers: answersData,
            score: 0,
            attempt: index,
            current_attempt: answersChunks.length - index === 1,
            bindTutor: false,
            draftAt: now,
            submittedAt: now,
            gradedAt: now,
            externalId: answer.user_survey_id,
          };

          await this.submissionsService.create(submission);
        }
      }
    }
  }

  async importLocalSurveys(answers: SurveysTypes.SurveysResponse[], surveyApi: surveyFormatted) {
    if (!answers || !answers.length) {
      return;
    }
    let assessment = null;

    const courseId = this.courcesMap.get(answers[0].course.course_id);
    const lectureId = this.lectureMap.get(answers[0].lecture.lecture_id);
    const moduleId = this.moduleMap.get(lectureId);
    const surveyId = surveyApi.id;

    let surveyDb = await this.importSurveyLinkService.importSurveyLinkRepository.findOne({
      surveyId,
      courseId,
      lectureId,
    });
    if (!surveyDb) {
      assessment = await this.createLocalSurveys(answers, surveyApi);
      surveyDb = await this.importSurveyLinkService.importSurveyLinkRepository.save({
        surveyId,
        courseId,
        lectureId,
        isGlobal: false,
        assessment,
      });
    } else {
      assessment = await this.assessmentsService.findOne(surveyDb.assessmentId);
    }

    for (let index2 = 0; index2 < answers.length; index2++) {
      this.counter.answers += answers.length;
      console.log(`import ${index2 + 1} / ${answers.length}`);
      const answer = answers[index2];

      if (!answer.user) {
        console.log('NO USER IN ANSWER');
        continue;
      }

      const userDb = await this.userService.findOneByExernalId(answer.user.user_id);
      if (!userDb) {
        console.error('import user answer - not found user in DB', answer.user);
        continue;
      }

      await this.submissionsService.removeListByExternalId(answer.user_survey_id);

      const answersChunks = this.split(answer.answers, surveyApi.questions.length);
      const now = new Date();
      for (let index = 0; index < answersChunks.length; index++) {
        const answerChunk = answersChunks[index];
        const answersData: any[] = this.buildAnswer(assessment, answerChunk, answer.user_survey_id);

        const submission: CreateSubmissionDto = {
          userId: userDb.id,
          assessmentId: assessment.id,
          lectureId,
          moduleId,
          courseId,
          status: SubmissionStatus.Graded,
          answers: answersData,
          score: 0,
          attempt: index,
          current_attempt: answersChunks.length - index === 1,
          bindTutor: false,
          draftAt: now,
          submittedAt: now,
          gradedAt: now,
          externalId: answer.user_survey_id,
        };

        await this.submissionsService.create(submission);
      }
    }
  }

  groupsSurveys(surveys: surveyFormatted[]): [surveyFormatted[][], surveyFormatted[]] {
    const questions = new Set();
    surveys.forEach(s =>
      s.questions.forEach(q => {
        questions.add(JSON.stringify({ ...q, id: null }).toLocaleLowerCase());
      }),
    );
    const questionsArray = Array.from(questions);
    const surveysWithId = surveys.map(s => ({
      ...s,
      questionIds: s.questions
        .reduce((a, q) => a.concat(questionsArray.findIndex(e => e == JSON.stringify({ ...q, id: null }).toLocaleLowerCase())), [])
        .toString(),
    }));

    const surveyCount = {};
    surveysWithId.forEach(s => {
      surveyCount[s.questionIds] = -~surveyCount[s.questionIds];
    });

    const globalsIds = Object.entries(surveyCount)
      .sort((a: any, b: any) => b[1] - a[1])
      .splice(0, 2)
      .map(e => e[0]);
    const globals = globalsIds.map(id => {
      return surveysWithId.filter(e => e.questionIds == id);
    });

    const locals = surveysWithId.filter(s => !globalsIds.includes(s.questionIds));

    return [globals, locals];
  }

  async importSurveyApi(surveys) {
    const surveysIds = Object.keys(surveys).map(e => ({
      course_slug: surveys[e][0].course.course_slug,
      survey_id: e,
    }));

    const surveyApi = await promisePool(surveysIds, this.donwloadSurveyApi.bind(this), this.concurrency);

    const surveyApiFormatted = surveyApi
      .filter(e => e)
      .map((survey: surveyapi.RootObject) => {
        const questions = survey.data.relationships.questions.data.map(q => {
          const questionDetail = survey.included.find(a => a.id == q.id);
          let answers = null;
          if (questionDetail.type == 'survey--question--multipleChoice') {
            answers = questionDetail.relationships.choices.data.map(answer => {
              const answerDetail = survey.included.find(a => a.id == answer.id);
              return removeHtml(answerDetail.attributes.body);
            });
          }

          const question = {
            id: q.id,
            type: q.type,
            title: removeHtml(questionDetail.attributes.body) as string,
            answers: answers,
          };
          return question;
        });

        const result = {
          id: survey.data.id,
          title: removeHtml(survey.data.attributes.title) as string,
          questions,
        };
        return result;
      });
    // @ts-ignore TODO: need description
    return this.groupsSurveys(surveyApiFormatted);
  }

  async importAnswers(courses) {
    const answersLoad = await promisePool(courses, this.importSurveyAnswers.bind(this), this.concurrency);
    const answers = answersLoad
      .filter(e => Object.keys(e).filter(k => Object.keys(e[k]).length).length)
      .reduce((a: any, e: any) => ({ ...a, ...e }), {});

    const surveys = await this.groupSurvey(answers);
    return surveys;
  }

  async import(concurrency = 3, debug = true) {
    this.concurrency = concurrency;
    this.debug = debug;
    await this.startSession();

    const courses = await this.coursesService.findAll(false, true, null, null, true);

    this.courcesMap = new Map(courses.filter(e => e.externalId).map(cource => [cource.externalId, cource.id]));

    this.moduleMap = new Map(
      courses
        .reduce((a, cource) => (a.push(...cource.modules), a), [])
        .reduce((a, module) => (a.push(...module.lectures.map(e => [e.id, module.id])), a), []),
    );

    this.lectureMap = new Map(
      courses
        .reduce((a, cource) => (a.push(...cource.modules), a), [])
        .reduce((a, module) => (a.push(...module.lectures), a), [])
        .map(e => [e.externalId, e.id]),
    );

    const surveys = await this.importAnswers(courses);

    const [globalsSurvey, localsSurvey] = await this.importSurveyApi(surveys);

    this.counter.globalSurvey = globalsSurvey.length;
    this.counter.survey = localsSurvey.length;

    for (let index = 0; index < localsSurvey.length; index++) {
      await this.importLocalSurveys(surveys[localsSurvey[index].id], localsSurvey[index]);
    }

    for (let index = 0; index < globalsSurvey.length; index++) {
      await this.importGlobalSurveys(surveys, globalsSurvey[index]);
    }

    console.log(this.counter);

    return this.counter;
  }

  getQuizLength(answer: SurveysTypes.SurveysResponse): number {
    const questionStartId = answer.answers[0].question.question_id;
    for (let index = 1; index < answer.answers.length; index++) {
      if (answer.answers[index].question.question_id == questionStartId) {
        return index;
      }
    }
    return answer.answers.length;
  }

  split(array, n) {
    const [...arr] = array;
    const res = [];
    while (arr.length) {
      res.push(arr.splice(0, n));
    }
    return res;
  }

  async groupSurvey(answers) {
    const surveys = {};
    Object.keys(answers).map(course =>
      Object.keys(answers[course]).map(lecture => {
        answers[course][lecture].map((answer: SurveysTypes.SurveysResponse) => {
          if (answer.survey.survey_id in surveys) {
            surveys[answer.survey.survey_id].push(answer);
          } else {
            surveys[answer.survey.survey_id] = [answer];
          }
        });
      }),
    );

    return surveys;
  }

  async donwloadAnswers(it: Course, lecture: Lecture) {
    console.log(`start load course answer ${it.slug}/${lecture.slug}(${lecture.externalId})`);
    const boxApiUrl = `https://ebac-online.eadbox.com.br/api/admin/courses/${it.externalId}/lectures/${lecture.externalId}/user_surveys`;

    const result = [];
    let pageCounter = 1;
    while (true) {
      try {
        const data = await axios
          .get<import('./types/surveys').Response.SurveysResponse[]>(boxApiUrl, {
            params: {
              auth_token: 'Pv5UVW27fFK1f_zodaea',
              page: pageCounter++,
            },
          })
          .catch(err => {
            console.error(
              `import survey  ${it.slug}/${lecture.slug}(${lecture.externalId})`,
              `${err.response?.status || err} ${err.response?.statusText || ''}`,
            );
          })
          .then(it => it && it.data);

        if (!data || !data?.length) {
          break;
        }

        result.push(...data);
      } catch (error) {
        console.error(error);
      }
    }
    console.log(`   end load course answer ${it.slug}`);

    return result;
  }

  async importSurveyAnswers(course: Course) {
    const lectures = course.modules.reduce((a, module) => (a.push(...module.lectures), a), []);
    function load(lecture: Lecture) {
      return this.donwloadAnswers(course, lecture);
    }
    const results = await promisePool(lectures, load.bind(this), this.concurrency);
    const result = {
      [course.externalId]: results.filter((e: any) => e.length).reduce((a, e, i) => ((a[lectures[i].externalId] = e), a), {}),
    };
    return result;
  }
}
