import { INestApplicationContext } from '@nestjs/common';
import * as chalk from 'chalk';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { Submission } from 'src/lib/entities/submission.entity';
import promisePool from 'src/_util/async-bottleneck';
import { EntityManager } from 'typeorm';
import { EadBoxAssesments } from './eadBox.assesments';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxMutex {
  private readonly entityManager: EntityManager;
  private readonly submissionRepository;

  constructor(
    context: INestApplicationContext,
    private readonly downloadHelper: EadBoxDownloadHelper,
    private readonly assessments: EadBoxAssesments,
    private readonly submissionsService: SubmissionsService,
  ) {
    this.entityManager = context.get(EntityManager);
    this.downloadHelper = downloadHelper;
    this.assessments = assessments;
  }

  public async import({ concurrency }) {
    console.log('import');
    const pagesToDownload = await this.preparePagesToDownload();

    let index = 0;
    const importPage = async page => {
      this.printInfo(index, page, pagesToDownload.length);
      index++;

      const downloadedAssessments = await this.downloadPage(page);

      if (downloadedAssessments) {
        const pendingAssessments = downloadedAssessments.filter((a: any) => page.eadAssesments.includes(a.user_assessment_id));

        await this.assessments.save(pendingAssessments, page.pageKey.lectureId, page.pageKey.page);
      }
    };

    await promisePool(pagesToDownload, importPage.bind(this), concurrency);

    return this.assessments.getCounters();
  }

  private printInfo(index, page, length) {
    console.log(chalk.green(`[${index + 1}/${length}]`) + ` Downloading assesments page ${chalk.green(page.pageKey.page)}`);

    console.log(`of lecture ${chalk.blue(page.pageKey.courserTitle)} - ${chalk.yellow(page.pageKey.lectureTitle)}`);
  }

  private async downloadPage(page: any) {
    return this.downloadHelper.get(
      `/admin/courses/${page.pageKey.courseId}/lectures/${page.pageKey.lectureId}/user_assessments`,
      { page: page.pageKey.page },
      true, //silent
    );
  }

  private async getSubmissionsWithMutex() {
    const withMutex = await this.entityManager
      .createQueryBuilder(Submission, 'submission')
      .select(
        `
        submission.external_id as ead_assessment_id,
        lecture.external_id as ead_lecture_id,
        lecture.title as lecture_title,
        lecture.id as db_lecture_id,
        course.external_id as ead_course_id,
        course.title as course_title,
        submission.import_data
      `,
      )
      .innerJoin('submission.assessment', 'assessment')
      .innerJoin('assessment.lecture', 'lecture')
      .innerJoin('lecture.course', 'course')
      .where('submission.mutex is not null')
      .andWhere('submission.status = :status', { status: 'submitted' })
      .getRawMany();

    const filtered = withMutex.filter((s: { import_data: any; page_key: string }) => s.import_data);
    const sorted = filtered.sort(s => s.ead_lecture_id + s.import_data.page);

    return sorted;
  }

  private async preparePagesToDownload() {
    interface MutexData {
      ead_assessment_id: string;
      ead_lecture_id: string;
      lecture_title: string;
      db_lecture_id: string;
      ead_course_id: string;
      course_title: string;
      import_data: any;
    }

    const submisionsWithMutex = await this.getSubmissionsWithMutex();

    return submisionsWithMutex
      .filter((s: MutexData) => s.import_data)
      .map(s => ({
        ...s,
        pageKey: {
          lectureId: s.ead_lecture_id,
          courseId: s.ead_course_id,
          page: s.import_data.page,
          lectureTitle: s.lecture_title,
          courserTitle: s.course_title,
        },
      }))
      .reduce((pages, s) => this.mergePage(pages, s), []);
  }

  private mergePage(pages, s) {
    let page = pages.find(p => s.pageKey.lectureId === p.pageKey.lectureId && s.pageKey.page === p.pageKey.page);

    if (!page) {
      page = {
        pageKey: s.pageKey,
        eadAssesments: [],
      };

      pages.push(page);
    }

    page.eadAssesments.push(s.ead_assessment_id);
    return pages;
  }
}
