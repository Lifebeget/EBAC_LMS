import { Course } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { ContentType } from '@enums/content-type.enum';
import { AssessmentType, QuestionType } from '@lib/types/enums';
import { INestApplicationContext } from '@nestjs/common';
import * as chalk from 'chalk';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { CreateAssessmentDto } from 'src/assessments/dto/create-assessment.dto';
import { UpdateQuestionDto } from 'src/assessments/dto/update-question.dto';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { ContentService } from 'src/courses/content.service';
import { ModulesService } from 'src/courses/modules.service';
import { CreateContentDto } from 'src/courses/dto/create-content.dto';
import { CreateLectureDto } from 'src/courses/dto/create-lecture.dto';
import { LecturesService } from 'src/courses/lectures.service';
import { QuestionsService } from 'src/assessments/questions.service';
import { Submission } from 'src/lib/entities/submission.entity';
import promisePool from 'src/_util/async-bottleneck';
import { EntityManager } from 'typeorm';
import { Assessment } from '../lib/entities/assessment.entity';
import { downloadAssessment, downloadCourseModules, startSession } from './eadbox-api';
import { EadBoxAssesments } from './eadBox.assesments';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import * as courseapiParsed from './types/courseapi-parsed';
import { CreateQuestionDto } from 'src/assessments/dto/create-question.dto';
import { CreateModuleDto } from 'src/courses/dto/create-module.dto';

enum LogEnum {
  All = 4,
  Errors = 3,
  Double = 2,
  None = 1,
}
export class EadBoxFast {
  private readonly entityManager: EntityManager;
  private eadboxSessionCookie: string;
  private submissionsService: SubmissionsService;
  private lecturesService: LecturesService;
  private contentService: ContentService;
  private assessmentsService: AssessmentsService;
  private questionsService: QuestionsService;
  private modulesService: ModulesService;
  private debug: LogEnum;

  constructor(
    context: INestApplicationContext,
    private readonly downloadHelper: EadBoxDownloadHelper,
    private readonly assessments: EadBoxAssesments,
  ) {
    this.entityManager = context.get(EntityManager);
    this.downloadHelper = downloadHelper;
    this.assessments = assessments;
    this.submissionsService = context.get(SubmissionsService);
    this.lecturesService = context.get(LecturesService);
    this.contentService = context.get(ContentService);
    this.modulesService = context.get(ModulesService);
    this.assessmentsService = context.get(AssessmentsService);
    this.questionsService = context.get(QuestionsService);
    this.debug = LogEnum.All;
  }
  private log(text, logLevel = LogEnum.All) {
    if (this.debug >= logLevel) {
      logLevel === LogEnum.All ? console.log(chalk.green(text)) : console.error(chalk.red(text));
    }
  }
  private logError(text) {
    this.log(text, LogEnum.Errors);
  }

  private logDouble(text) {
    this.log(text, LogEnum.Double);
  }

  private createModule = (courseDb: Course, lectureEadbox) => {
    const dto = new CreateModuleDto();
    dto.sort = lectureEadbox.attributes.position * 10;
    dto.title = lectureEadbox.attributes.title;
    dto.active = true;
    dto.published = false;
    dto.externalId = lectureEadbox.id;
    dto.description = lectureEadbox.attributes.description ?? '';
    dto.publishedAt = lectureEadbox.attributes.published_at;

    return this.modulesService.create(courseDb.id, dto);
  };

  private validateModule = async (courseDb: Course, moduleEadbox) => {
    const moduleDb = courseDb.modules?.filter(e => e.externalId === moduleEadbox.attributes.slug);
    const moduleName = `${moduleEadbox.attributes.title}: ${moduleEadbox.attributes.description}`;
    const logMsg = `  module ${moduleEadbox.id} ${moduleName}`;

    if (!moduleDb || moduleDb?.length === 0) {
      this.logError(`${logMsg} | not exist`);
      this.log('    create module');
      return this.createModule(courseDb, moduleEadbox);
    } else if (moduleDb?.length === 1) {
      this.log(`${logMsg} | exist`);
      return moduleDb[0];
    } else if (moduleDb?.length > 1) {
      this.logDouble(`${logMsg} | MULTIPLE! count = ${moduleDb.length}`);
    }
  };

  private createLecture = (lectureEadbox, moduleDb) => {
    const lectureDto = new CreateLectureDto();
    lectureDto.slug = lectureEadbox.attributes.slug;
    lectureDto.free = lectureEadbox.attributes.free ? true : false;
    lectureDto.sort = lectureEadbox.attributes.position * 10;
    lectureDto.title = lectureEadbox.attributes.title;
    lectureDto.active = true;
    lectureDto.courseId = moduleDb.courseId;
    lectureDto.moduleId = moduleDb.id;
    lectureDto.published = false;
    lectureDto.externalId = lectureEadbox.id;
    lectureDto.description = lectureEadbox.attributes.description ?? '';
    lectureDto.publishedAt = lectureEadbox.attributes.published_at;

    return this.lecturesService.create(lectureDto);
  };

  private validateLecture = async (lecturesDb, lectureEadbox, moduleDb) => {
    const lectureDb = lecturesDb?.filter(e => e.externalId === lectureEadbox.id);
    const lectureName = `${lectureEadbox.attributes.title}: ${lectureEadbox.attributes.description}`;
    const logMsg = `    lecture ${lectureEadbox.id} ${lectureName}`;

    if (!lectureDb || lectureDb?.length === 0) {
      this.logError(`${logMsg} | not exist`);
      this.log('    create lecture');
      const lectureDb = await this.createLecture(lectureEadbox, moduleDb);
      lecturesDb.push(lectureDb);
    } else if (lectureDb?.length === 1) {
      this.log(`${logMsg} | exist`);
      return lectureDb[0];
    } else if (lectureDb?.length > 1) {
      this.logDouble(`${logMsg} | MULTIPLE! count = ${lectureDb.length}`);
    }
  };

  private createContent = (lectureId: string, contentEadbox: courseapiParsed.Content) => {
    const dto = new CreateContentDto();
    dto.type = ContentType.task;
    dto.active = true;
    dto.title = contentEadbox.attributes.title;
    dto.externalId = contentEadbox.id;
    return this.contentService.create(lectureId, dto);
  };

  async createAssesment(eadAssessment: any, lectureId: string): Promise<Assessment> {
    const assessmentDto = new CreateAssessmentDto();

    assessmentDto.title = eadAssessment.attributes.title;
    assessmentDto.lectureId = lectureId;
    assessmentDto.externalId = eadAssessment.id;

    const assessment = await this.assessmentsService.create(assessmentDto);

    // Creating questions
    assessment.questions = [];
    for (let i = 0, l = eadAssessment.questions.length; i < l; i++) {
      const questionDto = new CreateQuestionDto();
      questionDto.title = '';
      questionDto.assessmentId = assessment.id;
      questionDto.description = eadAssessment.questions[i].attributes.body;
      questionDto.externalId = eadAssessment.questions[i].id;
      questionDto.type = QuestionType.Upload; // For now no other types supported

      const question = await this.questionsService.create(questionDto);

      assessment.questions.push(question);
    }
    return assessment;
  }

  async updateAssesment(dbAssessment: Assessment, eadAssessment: any): Promise<Assessment> {
    const dto = new CreateAssessmentDto();
    let assessment = { ...dbAssessment };
    if (dbAssessment.title != eadAssessment.attributes.title) {
      dto.title = eadAssessment.attributes.title;
      assessment = await this.assessmentsService.update(dbAssessment.id, dto);
    }

    assessment.questions = [];
    for (let i = 0, l = eadAssessment.questions.length; i < l; i++) {
      const externalId = eadAssessment.questions[i].id;
      const dbQuestion = dbAssessment.questions.find(q => q.externalId == externalId);
      if (dbQuestion) {
        // Update
        const questionDto = new UpdateQuestionDto();
        const description = eadAssessment.questions[i].attributes.body;
        if (dbQuestion.description != description) {
          questionDto.description = description;
          questionDto.externalId = externalId; // passing to result
          await this.questionsService.update(dbQuestion.id, questionDto);
        }
      } else {
        const dbQuestionCheck = dbAssessment.questions.find(
          q => q.type === QuestionType.Upload && !q.externalId && q.description === eadAssessment.questions[i].attributes.body,
        );
        if (!dbQuestionCheck) {
          // create
          this.log('        create question');
          const questionDto = new CreateQuestionDto();
          questionDto.title = '';
          questionDto.assessmentId = assessment.id;
          questionDto.description = eadAssessment.questions[i].attributes.body;
          questionDto.externalId = externalId;
          questionDto.type = QuestionType.Upload; // For now no other types supported

          const question = await this.questionsService.create(questionDto);
          assessment.questions.push(question);
        } else {
          // bind externalId
          await this.questionsService.update(dbQuestionCheck.id, {
            externalId: eadAssessment.questions[i].id,
          });
        }
      }
    }
    return assessment;
  }

  private validateAssessment = async (courseDb: Course, lecture: Lecture, assessmentEadbox: courseapiParsed.Assessment) => {
    const assessmentName = `${assessmentEadbox.type}: ${assessmentEadbox.attributes.title}`;
    const logMsg = `        assessment ${assessmentEadbox.id} ${assessmentName}`;

    const assessmentsDb = await this.getAssessmentDb(assessmentEadbox.id);
    let assessment;

    const assessmentEadboxApi = await downloadAssessment(courseDb.slug, assessmentEadbox.id, this.eadboxSessionCookie);

    if (assessmentsDb.length > 0) {
      assessment = assessmentsDb[0];
      if (assessmentsDb.length > 1) {
        this.logDouble(`${logMsg} |  MULTIPLE assessment at DB! count = ${assessmentsDb.length}. use first`);
      } else {
        this.log(`${logMsg} |  exist`);
      }
      assessment = await this.updateAssesment(assessment, assessmentEadboxApi);
    } else {
      const assessmentCheck = lecture.contents.filter(
        c =>
          c.type === ContentType.task &&
          c.assessment &&
          !c.assessment?.externalId &&
          c.title === assessmentEadboxApi.attributes.title &&
          c.assessment.questions &&
          c.assessment.questions.length &&
          c.assessment.questions[0].description === assessmentEadboxApi.questions[0].attributes.body &&
          (!c.assessment.questions[0].externalId || c.assessment.questions[0].externalId === assessmentEadboxApi.questions[0].id),
      );
      if (assessmentCheck.length) {
        assessment = assessmentCheck[0].assessment;

        this.log(`      bind assessment ${assessment.id} to externalId ${assessmentEadboxApi.id}`);
        await this.assessmentsService.update(assessment.id, {
          externalId: assessmentEadboxApi.id,
        });
        if (!assessment.questions[0].externalId) {
          this.log(`      bind question ${assessment.questions[0].id} to externalId ${assessmentEadboxApi.questions[0].id}`);
          await this.questionsService.update(assessment.questions[0].id, {
            externalId: assessmentEadboxApi.questions[0].id,
          });
        }
      } else {
        this.log('      create assessment');
        assessment = await this.createAssesment(assessmentEadboxApi, lecture.id);
      }
    }

    return assessment;
  };

  private validateContent = async (courseDb, lectureDb, contentEadbox: courseapiParsed.Content) => {
    const contentDb = lectureDb?.contents?.filter(e => e.externalId === contentEadbox.id);
    const contentName = `${contentEadbox.type}: ${contentEadbox.attributes.title}`;
    const logMsg = `      content ${contentEadbox.id} ${contentName}`;
    let content;

    // print msg
    if (!contentDb || contentDb?.length === 0) {
      this.logError(`${logMsg} | not exist`);
    } else if (contentDb?.length === 1) {
      this.log(`${logMsg} | exist`);
    } else if (contentDb?.length > 1) {
      this.logDouble(`${logMsg} | MULTIPLE! count = ${contentDb.length}`);
    }

    if (contentEadbox.type === 'contents--assessment') {
      const isMultipleChoiceAssessment = contentEadbox?.assessment?.questions?.filter(
        e => e.type === 'assessment--question--multipleChoice',
      );
      if (isMultipleChoiceAssessment && isMultipleChoiceAssessment.length) {
        this.logError('      skip multipleChoice assessment');
        return;
      }
      const assessment = await this.validateAssessment(courseDb, lectureDb, contentEadbox.assessment);
      if (!contentDb || contentDb?.length === 0) {
        let contentDbCheck = lectureDb?.contents?.filter(
          e => e.type === ContentType.task && !e.externalId && e.sort === (contentEadbox.attributes.position + 1) * 10,
        );
        if (!contentDbCheck || !contentDbCheck.length) {
          const contentDbChecks = lectureDb?.contents?.filter(
            e => e.type === ContentType.task && !e.externalId && e?.assessment?.id === assessment.id,
          );
          if (contentDbChecks.length === 1) {
            contentDbCheck = contentDbChecks;
          }
        }
        if (!contentDbCheck || !contentDbCheck.length) {
          const contentDb = await this.createContent(lectureDb.id, contentEadbox);
          this.log('      create content block');
          content = await this.contentService.findOne(contentDb.id);
        } else {
          // bind externalId
          await this.contentService.update(contentDbCheck[0].id, {
            externalId: contentEadbox.id,
          });
          content = await this.contentService.findOne(contentDbCheck[0].id);
        }
      } else if (contentDb?.length === 1) {
        content = contentDb[0];
      } else if (contentDb?.length > 1) {
        const contentDBbyAssessment = contentDb.filter(e => e.assessment?.id === assessment.id);
        if (!contentDBbyAssessment) {
          content = await this.createContent(lectureDb.id, contentEadbox);
        } else {
          content = contentDBbyAssessment[0];
        }
      }

      if (content?.assessment?.id === assessment.id) {
        this.log(`${logMsg} |  same assessmentId in content`);
      } else {
        this.log(`${logMsg} |  update assessmentId in content`);
        await this.contentService.update(content.id, {
          assessmentId: assessment.id,
        });
      }
    }
  };

  public async validateCourse(courseId) {
    const courseDb = await this.getFromDbCourse(courseId);
    const lecturesDb = courseDb.modules.reduce((a, e) => a.concat(e.lectures), []);
    const courseEadboxModules = await downloadCourseModules(courseDb.externalId, this.eadboxSessionCookie);
    if (!courseEadboxModules) {
      console.error('cant get api data eadbox');
      return;
    }

    for (let index = 0; index < courseEadboxModules.length; index++) {
      const moduleEadbox = courseEadboxModules[index];
      const moduleDbOk = await this.validateModule(courseDb, moduleEadbox);
      if (!moduleDbOk || !moduleEadbox.lectures) {
        continue;
      }
      for (let index = 0; index < moduleEadbox.lectures.length; index++) {
        const lectureEadbox = moduleEadbox.lectures[index];
        const lectureDbOk = await this.validateLecture(lecturesDb, lectureEadbox, moduleDbOk);
        if (!lectureDbOk || !lectureEadbox.contents) {
          continue;
        }
        for (let index = 0; index < lectureEadbox.contents.length; index++) {
          const contentEadbox = lectureEadbox.contents[index];
          const _contentDbOk = await this.validateContent(courseDb, lectureDbOk, contentEadbox);
        }
      }
    }
  }

  public async import({ course, concurrency, log, validate }) {
    this.debug = log || LogEnum.Double;
    let lecturesToDownload;
    if (course) {
      lecturesToDownload = await this.getFromDbForCourse(course);
    } else {
      lecturesToDownload = await this.getFromDb();
    }

    if (validate) {
      this.eadboxSessionCookie = await startSession(this.submissionsService);

      const coursesIds = Array.from(new Set(lecturesToDownload.map(e => e.course_id)));

      await promisePool(coursesIds, this.validateCourse.bind(this), concurrency);
    }

    let index = 0;

    const importLecture = lecture => {
      this.printInfo(index, lecture, lecturesToDownload.length);
      index++;
      return this.downloadHelper.downloadByPage({
        name: 'assessments',
        fnGet: page => this.downloadPage(lecture, page),
        fnSave: async (assesments: any[], page: number) => this.assessments.save(assesments, lecture.db_lecture_id, page),
        silent: true,
      });
    };

    await promisePool(lecturesToDownload, importLecture.bind(this), concurrency);

    return this.assessments.getCounters();
  }

  private printInfo(index, lecture, length) {
    console.log(
      chalk.green(`[${index + 1}/${length}]`) +
        ` Importing assesments of lecture "${chalk.blue(lecture.course_title)}" - "${chalk.yellow(lecture.lecture_title)}"`,
    );
  }

  private async downloadPage(lecture: any, page: number) {
    return this.downloadHelper.get(
      `/admin/courses/${lecture.ead_course_id}/lectures/${lecture.ead_lecture_id}/user_assessments`,
      { page },
      true, //silent
    );
  }

  private async getFromDb() {
    const qb = this.entityManager
      .createQueryBuilder(Submission, 'submission')
      .select(
        `
        lecture.external_id as ead_lecture_id,
        lecture.title as lecture_title,
        lecture.id as db_lecture_id,
        course.external_id as ead_course_id,
        course.title as course_title,
        course.id as course_id
      `,
      )
      .distinct(true)
      .innerJoin('submission.assessment', 'assessment')
      .innerJoin('assessment.lecture', 'lecture')
      .innerJoin('lecture.course', 'course')
      .where('course.external_id is not null')
      .andWhere('lecture.external_id is not null')
      .andWhere('assessment.type = :type', { type: AssessmentType.Default });
    return qb.getRawMany();
  }

  private async getFromDbForCourse(courseId?: string) {
    const qb = this.entityManager
      .createQueryBuilder(Lecture, 'lecture')
      .select(
        `
        lecture.external_id as ead_lecture_id,
        lecture.title as lecture_title,
        lecture.id as db_lecture_id,
        course.external_id as ead_course_id,
        course.title as course_title,
        course.id as course_id
      `,
      )
      .innerJoin('lecture.course', 'course')
      .where('course.external_id is not null')
      .andWhere('lecture.external_id is not null')
      .andWhere('course.id = :courseId OR course.external_id = :courseId', {
        courseId,
      });
    return qb.getRawMany();
  }

  private async getFromDbCourse(courseId?: string) {
    const qb = this.entityManager
      .createQueryBuilder(Course, 'course')
      .leftJoinAndSelect('course.modules', 'modules')
      .leftJoinAndSelect('modules.lectures', 'lectures')
      .leftJoinAndSelect('lectures.contents', 'content')
      .leftJoinAndSelect('content.assessment', 'assessment')
      .leftJoinAndSelect('assessment.questions', 'question')
      .andWhere('course.id = :courseId OR course.external_id = :courseId', {
        courseId,
      });
    return qb.getOne();
  }

  private async getAssessmentDb(externalId?: string) {
    const qb = this.entityManager
      .createQueryBuilder(Assessment, 'assessment')
      .leftJoinAndSelect('assessment.lecture', 'lecture')
      .leftJoinAndSelect('assessment.questions', 'question')
      .andWhere('assessment.external_id = :externalId', {
        externalId,
      });
    return qb.getMany();
  }
}
