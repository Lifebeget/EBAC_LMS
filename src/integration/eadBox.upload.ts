import { INestApplicationContext } from '@nestjs/common';
import { AnswersService } from 'src/assessments/answers.service';
import { FilesService } from 'src/files/files.service';
import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate } from '../lib/types/ImportCounters';
export class EadBoxUploadS3 {
  private filesService: FilesService;
  private answersService: AnswersService;
  private debug: boolean;

  constructor(context: INestApplicationContext, private counter = { ...counterTemplate, size: 0 }) {
    this.filesService = context.get(FilesService);
    this.answersService = context.get(AnswersService);
    this.debug = true;
  }

  async import(concurrency = 1, debug = true) {
    this.debug = debug;
    const files = await this.answersService.findAllAnswersFiles2Upload();

    await promisePool(files, this.importData.bind(this), concurrency);

    return this.counter;
  }

  async importData(dbFile: any) {
    try {
      const start = Date.now();
      const file = {
        id: dbFile.files_id,
        title: dbFile.files_title,
        url: dbFile.files_url,
        extension: dbFile.files_extension,
        mimetype: dbFile.files_mimetype,
      };
      await this.filesService.uploadUrlToS3andUpdate(file);
      this.counter.size += dbFile.files_size || 0;
      this.counter.updated++;

      this.debug && console.log(dbFile.files_id, 'Save', Date.now() - start);
    } catch (error) {
      console.error(`upload file ${dbFile.files_id}`, error);
    }
  }
}
