import { INestApplicationContext } from '@nestjs/common';
import { EadBoxAssesments } from './eadBox.assesments';
import { EadBoxCourses } from './eadBox.courses';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import { countersTemplate, ImportCounters } from '../lib/types/ImportCounters';
import { EadBoxEnrollments } from './eadBox.enrollments';
import { EadBoxLectures } from './eadBox.lectures';
import { EadBoxUsers } from './eadBox.users';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { EadBoxMutex } from './eadBox.mutex';
import { EadBoxFast } from './eadBox.fast';
import axios from 'axios';
import { EadBoxComments } from './eadBox.comments';
import { EadBoxForums } from './eadBox.forums';
import { EadBoxModules } from './eadBox.modules';
import { EadBoxAvatars } from './eadBox.avatars';
import { EadBoxUploadS3 } from './eadBox.upload';
import { EadBoxDeleteFiles } from './eadBox.deletefiles';

import '../dayjs.config';
import { EadBoxSurveys } from './eadBox.surveys';

interface ImportOptions {
  downloadUsers?: boolean;
  downloadLectures?: boolean;
  downloadAssessments?: boolean;
  downloadEnrollments?: boolean;
  pagesLimit?: number;
  lecturesLimit?: number;
  perpage?: number;
  locked?: boolean;
  concurrency?: number;
  startDate?: string;
}
export const empyImportTemplate = {
  downloadUsers: false,
  downloadLectures: false,
  downloadAssessments: false,
  downloadEnrollments: false,
  pagesLimit: null,
  lecturesLimit: null,
} as ImportOptions;

export const fullImportTemplate = {
  downloadUsers: true,
  downloadLectures: true,
  downloadAssessments: true,
  downloadEnrollments: false,
  pagesLimit: null,
  lecturesLimit: null,
};

const notifyServer = async () => {
  try {
    await axios.post(`${process.env.API_URL}/signal/import-finish`, {
      challenge: process.env.INTERAPP_SECRET,
    });
    return true;
  } catch (err) {
    console.error('EadBox service failed to notify main server\n', err);
    return false;
  }
};

export class EadBox {
  private download: EadBoxDownloadHelper;
  private courses: EadBoxCourses;
  private users: EadBoxUsers;
  private lectures: EadBoxLectures;
  private assessments: EadBoxAssesments;
  private enrollments: EadBoxEnrollments;
  private mutex: EadBoxMutex;
  private fast: EadBoxFast;
  private comments: EadBoxComments;
  private forums: EadBoxForums;
  private surveys: EadBoxSurveys;
  private modules: EadBoxModules;
  private avatars: EadBoxAvatars;
  private uploadS3: EadBoxUploadS3;
  private deletefiles: EadBoxDeleteFiles;

  private submissionsService: SubmissionsService;

  constructor(
    public context: INestApplicationContext,
    url: string = process.env.EADBOX_API_URL,
    apiKey: string = process.env.EADBOX_API_KEY,
  ) {
    this.download = new EadBoxDownloadHelper(url, apiKey);

    this.users = new EadBoxUsers(context, this.download);
    this.courses = new EadBoxCourses(context, this.download);
    this.lectures = new EadBoxLectures(context, this.download);
    this.assessments = new EadBoxAssesments(context, this.download);
    this.enrollments = new EadBoxEnrollments(context, this.download);
    this.comments = new EadBoxComments(context, this.download);
    this.forums = new EadBoxForums(context, this.download);
    this.surveys = new EadBoxSurveys(context, this.download);
    this.modules = new EadBoxModules(context, this.download);
    this.avatars = new EadBoxAvatars(context, this.download);
    this.uploadS3 = new EadBoxUploadS3(context);
    this.deletefiles = new EadBoxDeleteFiles(context, this.download);

    this.submissionsService = context.get(SubmissionsService);

    this.mutex = new EadBoxMutex(context, this.download, this.assessments, this.submissionsService);
    this.fast = new EadBoxFast(context, this.download, this.assessments);
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(`Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`);
  }

  async fullImport(concurrency: number, perpage: number, startDate: string) {
    return this.import({
      ...fullImportTemplate,
      concurrency,
      perpage,
      startDate,
    });
  }

  async importComments(concurrency: number) {
    const result = await this.comments.import(concurrency);
    this.printStats(result);
  }

  async importForums(concurrency: number, debug: boolean) {
    const result = await this.forums.import(concurrency, debug);
    this.printStats(result);
  }

  async importSurveys(concurrency: number, debug: boolean) {
    const result = await this.surveys.import(concurrency, debug);
    this.printStats(result);
  }

  async importModules(concurrency: number, debug: boolean) {
    const result = await this.modules.import(concurrency, debug);
    this.printStats(result);
  }

  async importAvatars(concurrency: number, debug: boolean) {
    const result = await this.avatars.import(concurrency, debug);
    this.printStats(result);
  }

  async importUploadS3(concurrency: number, debug: boolean) {
    const result = await this.uploadS3.import(concurrency, debug);
    this.printStats(result);
  }

  async importDeleteFiles(concurrency: number, debug: boolean) {
    const result = await this.deletefiles.import(concurrency, debug);
    this.printStats(result);
  }

  async import(options: ImportOptions): Promise<ImportCounters> {
    const { pagesLimit, lecturesLimit, startDate } = options;

    let counters = countersTemplate;

    if (options.downloadUsers) {
      const start = Date.now();
      const result = await this.users.import(startDate, pagesLimit, options?.perpage, options?.locked);
      counters.users = result;
      console.log(`Imported user for ${Date.now() - start}ms`);
      this.printStats(result);
    }

    // This is 1 request, and we do need the result later
    // So no confirm here
    const { courses, stats } = await this.courses.import(null);
    this.printStats(stats);

    if (options.downloadLectures) {
      const lecturesStats = await this.lectures.import(courses);
      counters.lectures = lecturesStats;
      this.printStats(lecturesStats, 'Lectures');
    }

    if (options.downloadEnrollments) {
      const enrollmentsStats = await this.enrollments.import(startDate, pagesLimit, options.perpage);
      counters.enrollments = enrollmentsStats;
      this.printStats(enrollmentsStats);
    }

    if (options.downloadAssessments) {
      const assessmentsStats = await this.assessments.import({
        lecturesLimit,
        pagesLimit,
        concurrency: options.concurrency,
      });
      this.printStats(assessmentsStats.assessments, 'Assessments');
      this.printStats(assessmentsStats.questions, 'Questions');
      this.printStats(assessmentsStats.submissions, 'Submissions');
      this.printStats(assessmentsStats.answers, 'Answers');

      counters = { ...counters, ...assessmentsStats };
    }
    notifyServer();
    return counters;
  }

  async importMutex({ concurrency }) {
    const counters = await this.mutex.import({ concurrency });
    return counters;
  }

  async importFast({ course, concurrency, log, validate }: { course?: string; concurrency?: number; log?: string; validate?: string }) {
    const result = await this.fast.import({
      course,
      concurrency,
      log,
      validate,
    });
    notifyServer();
    return result;
  }
}
