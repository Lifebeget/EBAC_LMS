import { printheading } from 'src/cli';
import * as querystring from 'querystring';
import fetch, { Response } from 'node-fetch';
import { File } from 'src/lib/entities/file.entity';
import * as chalk from 'chalk';

export class EadBoxDownloadHelper {
  private url: string;
  private apiKey: string;

  constructor(url: string, apiKey: string) {
    this.url = url;
    this.apiKey = apiKey;
  }

  async get(path, params, silent = false) {
    let query = '';
    let json;
    const retry = 3;
    const RETRY_TIMEOUT = 10;
    let res;

    !silent && console.log(`Get path: ${chalk.yellow(path)}`, 'params', params);

    if (params) {
      query = '&' + querystring.encode(params);
    }
    for (let i = 1; i <= retry; i++) {
      try {
        const url = `${this.url}${path}?auth_token=${this.apiKey}${query}`;

        res = await fetch(url);

        if (res.status == 404) {
          console.log('Page not found. Skipping');
          return [];
        }

        json = await res.json();

        break;
      } catch (err) {
        console.log('Failed get data from API or to parse JSON');
        console.log(err);
        if (i <= retry) {
          console.log(`Retry in ${RETRY_TIMEOUT} seconds. Retry left: ${retry - i}`);
          await new Promise(r => setTimeout(r, RETRY_TIMEOUT * 1000));
        } else {
          const text = await res.text();
          console.log('Unexpected result in API call. Dump:');
          console.log(text);
          throw new Error('Unexpected result in API call');
        }
      }
    }
    return json;
  }
  async downloadByPage({ name, fnClear = null, fnGet, fnSave, firstPage = null, lastPage = null, silent = false }) {
    return this.download({
      name,
      fnClear,
      fnGet,
      fnSave,
      firstPage,
      lastPage,
      silent,
      startDate: undefined,
    });
  }

  async downloadByDate({ name, fnClear = null, fnGet, fnSave, startDate, perpage, lastPage = null, silent = false }) {
    return this.download({
      name,
      fnClear,
      fnGet,
      fnSave,
      startDate,
      perpage,
      lastPage,
      silent,
    });
  }

  async download({
    name,
    fnClear = null,
    fnGet,
    fnSave,
    startDate = null,
    perpage = null,
    firstPage = null,
    lastPage = null,
    silent = false,
  }): Promise<any> {
    let page = firstPage || 1;
    let datePage = 1;
    let date = startDate;
    let prevDate: Date = null;
    let items = [];
    let allItems = [];
    let counter = 0;
    let stop = false;
    !silent && printheading(`Getting ${name}. Pages limit: ${lastPage}`);

    if (fnClear) {
      !silent && console.log(`Cleaning up ${name} collection`);
      await fnClear();
    }
    do {
      if (lastPage && page > lastPage) {
        break;
      }
      const timer = new Date();

      if (date) {
        datePage = date == prevDate ? datePage + 1 : 1;

        console.log(chalk.blueBright('Date page: '), chalk.bold.magenta(datePage));

        items = await fnGet(date, datePage);
        stop = items.length < perpage;
      } else {
        items = await fnGet(page);
      }

      prevDate = date;

      if (!items || items.length == 0) {
        break;
      }

      const lastItem = items[items.length - 1];

      date = lastItem['updated_at'];

      const time = new Date().getTime() - timer.getTime();
      const sec = (time / 1000).toFixed(2);

      !silent && console.log(chalk.cyan(`Page ${chalk.green(page)} got ${chalk.green(items.length)} items in ${chalk.yellow(sec)} sec`));
      let inserted = await fnSave(items, page);

      if (!inserted) {
        inserted = [];
      }

      allItems = [...allItems, ...inserted];
      counter += inserted.length;
      page++;
    } while (items != [] && !stop);
    !silent && console.log(`${this.capitalizeFirstLetter(name)} download complete: ${counter} ${name}`);
    return allItems;
  }

  async fileInfo(path: string): Promise<Partial<File>> {
    let result: Response;
    const name = decodeURIComponent(path.substring(path.lastIndexOf('/') + 1));
    let extension = /[.]/.exec(name) ? /[^.]+$/.exec(name)?.pop() || '' : '';
    if (extension.length > 20) {
      extension = '';
    }
    const file: Partial<File> = { filename: name, extension };

    try {
      result = await fetch(path, { method: 'HEAD', timeout: 10000 });
      file.mimetype = result.headers.get('content-type');
      file.size = parseInt(result.headers.get('content-length')) || 0;
    } catch (e) {
      console.log('Unexpected result while fileInfo.');
      console.dir(e);
      file.size = 0;
      // throw new Error('Unexpected result in fileInfo');
    }

    return file;
  }
  capitalizeFirstLetter(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}
