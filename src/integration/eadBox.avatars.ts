import { User } from '@entities/user.entity';
import { FileFolder } from '@enums/file-folder.enum';
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { FilesService } from 'src/files/files.service';
import { UsersService } from 'src/users/users.service';
import promisePool from 'src/_util/async-bottleneck';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxAvatars {
  private usersService: UsersService;

  private submissionsService: SubmissionsService;
  private filesService: FilesService;
  private eadboxSessionCookie: string;
  private debug: boolean;
  enrollmentService: any;
  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.usersService = context.get(UsersService);
    this.filesService = context.get(FilesService);
    this.submissionsService = context.get(SubmissionsService);
    this.enrollmentService = context.get(EnrollmentsService);
    this.debug = true;
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async import(concurrency = 1, debug = true) {
    this.debug = debug;
    await this.startSession();

    const users = await this.usersService.eadBoxFindManyAvatars();

    await promisePool(users, this.importData.bind(this), concurrency);

    return this.counter;
  }

  async downloadAll(it: User) {
    const boxApiUrl = `https://ebac-online.eadbox.com.br/admin/users/${it.externalId}`;
    let counter = 0;
    const RETRY_TIMEOUT = 10;
    do {
      counter++;
      const result = await axios
        .get(boxApiUrl, {
          headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
        })

        .catch(err => {
          console.error('import avatar', it.externalId, err);
        })
        .then(res => res && res.status === 200 && res.data);

      if (result) return result;
      await new Promise(r => setTimeout(r, RETRY_TIMEOUT * 1000));
    } while (counter >= 5);
    console.error('import avatar cant download', it.externalId);
  }

  async createAvatar(usersDB, url) {
    const parse = url.match(/(.*)\/(?<filename>(.+))\.(?<ext>(.*))/);
    const obj = {
      url: url,
      title: parse.groups.filename,
      extension: parse.groups.ext,
      filename: parse.groups.filename,
      owner: { id: usersDB.id },
    };

    const avatar = await this.filesService.uploadUrlAndCreate(obj, FileFolder.avatars);

    await this.usersService.getUsersRepository().save({
      id: usersDB.id,
      avatarId: avatar.id,
    });
    this.counter.created++;
  }

  async updateAvatar(usersDB, url) {
    const parse = url.match(/(.*)\/(?<filename>(.+))\.(?<ext>(.*))/);
    const obj = {
      id: usersDB.avatar.id,
      path: null,
      mimetype: null,
      size: null,
      url: url,
      title: parse.groups.filename,
      extension: parse.groups.ext,
      filename: parse.groups.filename,
    };
    await this.filesService.uploadUrlToS3andUpdate(obj, FileFolder.avatars);
    this.counter.updated++;
  }

  async importData(dbUser: User) {
    try {
      let start = Date.now();
      const html = await this.downloadAll(dbUser);

      this.debug && console.log(`${dbUser.id} Load avatar ${Date.now() - start}ms`);

      if (!html) return;
      start = Date.now();
      const src = html.match(/<div class="user-avatar">\s*<img class="user-avatar" src="(?<url>.+?)"/i);
      const url = src?.groups?.url;
      if (url) {
        if (
          url == dbUser.avatar?.eadboxUrl ||
          url == 'https://cdn.eadbox.com/assets/avatar_g-add5fa3f23e0d2e44fcc089758e1fd27af5f19cceefc6d14696e299204e13e70.jpg' // default no avatar image
        )
          return this.counter.skipped++;

        if (dbUser.avatar) {
          await this.updateAvatar(dbUser, url);
        } else {
          await this.createAvatar(dbUser, url);
        }

        console.log(src?.groups?.url);
      } else {
        console.error(`${dbUser.id} Load avatar URL not found in html`);
      }

      this.debug && console.log(dbUser.id, 'Save', Date.now() - start);
    } catch (error) {
      console.error(`import avatar ${dbUser.id} ${dbUser.name}`, error);
    }
  }
}
