import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { CoursesService } from 'src/courses/courses.service';
import { CreateModuleDto } from 'src/courses/dto/create-module.dto';
import { UpdateLectureDto } from 'src/courses/dto/update-lecture.dto';
import { UpdateModuleDto } from 'src/courses/dto/update-module.dto';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { LecturesService } from 'src/courses/lectures.service';
import { ModulesService } from 'src/courses/modules.service';
import { Course } from 'src/lib/entities/course.entity';
import { Lecture } from 'src/lib/entities/lecture.entity';
import { Module } from 'src/lib/entities/module.entity';
import promisePool from 'src/_util/async-bottleneck';
import CourseResponse from './types/courses';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxModules {
  private coursesService: CoursesService;
  private modulesService: ModulesService;
  private submissionsService: SubmissionsService;
  private lecturesService: LecturesService;
  private eadboxSessionCookie: string;
  private courcesMap: Map<string, Course>;
  private modulesMap: Map<string, Module>;
  private lectureMap: Map<string, Lecture>;
  private debug: boolean;
  enrollmentService: any;
  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.coursesService = context.get(CoursesService);
    this.modulesService = context.get(ModulesService);
    this.lecturesService = context.get(LecturesService);
    this.submissionsService = context.get(SubmissionsService);
    this.enrollmentService = context.get(EnrollmentsService);
    this.debug = true;
  }

  async startSession() {
    if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
      throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
    }
    const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

    this.eadboxSessionCookie = await this.submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
  }

  async import(concurrency = 3, debug = true) {
    this.debug = debug;
    await this.startSession();

    const courses = await this.coursesService.findAll(false, true, null, null, true);
    this.courcesMap = new Map(courses.map(e => [e.externalId, e]));
    const modules = await this.modulesService.eaxBoxfindAll();
    this.modulesMap = new Map(modules.map(e => [`${e.course}=${e.externalId}`, e]));
    const lecture = await this.lecturesService.findAll(null, null, true);
    this.lectureMap = new Map(lecture.map(e => [e.externalId, e]));

    await promisePool(courses, this.importData.bind(this), concurrency);

    return this.counter;
  }

  async downloadAll(it: Course) {
    const boxApiUrl = `https://ebac-online.eadbox.com.br/ng/api/student/courses/${it.slug}.json`;
    const result = await axios
      .get<CourseResponse.CoursesData>(boxApiUrl, {
        headers: { Cookie: `_session_id=${this.eadboxSessionCookie};` },
      })
      .catch(err => {
        console.error('import courses', it.slug, err);
      })
      .then(it => it && it.data);

    return result;
  }

  async upsertModule(courseDB: Course, module: CourseResponse.Module2, index: number) {
    let entity;
    const moduleDB = this.modulesMap.get(`${courseDB.id}=${module.module_slug}`);

    if (moduleDB) {
      //update
      const dto = new UpdateModuleDto();
      if (!courseDB.autoNumbering) {
        if (moduleDB.sort != index * 10) dto.sort = index * 10;
        if (moduleDB.title != module.title) dto.title = module.title;
      }
      if (!moduleDB.active) dto.active = true;

      // Ignore changes in published state as of 20 Sep 2021
      // if (!moduleDB.published) dto.published = true;

      if (moduleDB.description != module.description) dto.description = module.description;
      if (moduleDB.externalId != module.module_slug) dto.externalId = module.module_slug;

      if (Object.keys(dto).length > 0) {
        entity = await this.modulesService.update(moduleDB.id, dto);
        this.counter.updated++;
      } else {
        this.counter.skipped++;
        entity = moduleDB;
      }
    } else {
      //create
      const dto = new CreateModuleDto();

      dto.sort = index * 10;
      dto.active = true;

      // New modules will be create as unpublished as of 20 Sep 2021
      dto.published = false;

      dto.title = module.title;
      dto.description = module.description;
      dto.externalId = module.module_slug;

      entity = await this.modulesService.create(courseDB.id, dto);

      this.counter.created++;
    }

    return entity;
  }

  async upsertLecture(courseDB: Course, lecture: CourseResponse.Lecture, _index: number) {
    let entity;
    if (!lecture.module) return courseDB;

    const lectureDB = this.lectureMap.get(lecture.lecture_id);
    const moduleDB = this.modulesMap.get(`${courseDB.id}=${lecture?.module?.module_id}`);
    if (!moduleDB) {
      console.error(`module with slug = ${lecture?.module?.module_id} in courseId=${courseDB.id} not found`);
    }

    if (lectureDB) {
      //update
      const dto = new UpdateLectureDto();

      if (!lectureDB?.module || lectureDB?.module?.id != moduleDB.id) dto.moduleId = moduleDB.id;
      if (lectureDB?.description != lecture.description && lecture.description) dto.description = moduleDB.description;

      if (Object.keys(dto).length > 0) {
        entity = await this.lecturesService.update(lectureDB.id, dto);
        this.counter.updated++;
      } else {
        this.counter.skipped++;
        entity = lectureDB;
      }
    } else {
      console.error(`lecture with externalId = ${lecture.lecture_id} not found`);
    }

    return entity;
  }

  async importData(courseDB: Course) {
    try {
      let start = Date.now();
      const course = await this.downloadAll(courseDB);

      this.debug && console.log(`${courseDB.id} Load module ${Date.now() - start}ms`);

      if (!course) return;
      start = Date.now();

      for (let index = 0; index < course.modules?.length; index++) {
        const module = course.modules[index];
        const moduleUpdated = await this.upsertModule(courseDB, module, index);
        this.modulesMap.set(`${courseDB.id}=${module.module_slug}`, moduleUpdated);
      }

      for (let index = 0; index < course.lectures?.length; index++) {
        const lecture = course.lectures[index];
        await this.upsertLecture(courseDB, lecture, index);
      }

      this.debug && console.log(courseDB.id, 'Save', Date.now() - start);
    } catch (error) {
      console.error(`import modules course ${courseDB.id} ${courseDB.title}`, error);
    }
  }
}
