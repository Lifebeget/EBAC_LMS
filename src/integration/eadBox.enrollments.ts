import { CourseRole } from '@lib/types/enums/course-role.enum';
import { INestApplicationContext } from '@nestjs/common';
import { CoursesService } from 'src/courses/courses.service';
import { CreateEnrollmentDto } from 'src/courses/dto/create-enrollment.dto';
import { UpdateEnrollmentDto } from 'src/courses/dto/update-enrollment.dto';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { Course } from 'src/lib/entities/course.entity';
import { Enrollment } from 'src/lib/entities/enrollment.entity';
import { SettingsService } from 'src/settings/settings.service';
import { UsersService } from 'src/users/users.service';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxEnrollments {
  private coursesService: CoursesService;
  private usersService: UsersService;
  private enrollmentsService: EnrollmentsService;
  private settingsService: SettingsService;
  private courses: Course[];

  constructor(context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.usersService = context.get(UsersService);
    this.coursesService = context.get(CoursesService);
    this.enrollmentsService = context.get(EnrollmentsService);
    this.settingsService = context.get(SettingsService);
  }

  async import(forcedStartDate?: string, pagesLimit: number = null, perpage?: number) {
    this.courses = await this.coursesService.findAll(false, false);

    let startDate = forcedStartDate;

    if (!startDate) {
      const settings = await this.settingsService.getSettings();
      startDate = settings.importSettings.enrollmentsLastDownload || '1970-01-01';
    }

    console.log('perpage: ', perpage);

    await this.download.downloadByDate({
      name: 'subscriptions',
      fnClear: null,
      fnGet: (updated_after: Date, page?: number) => {
        const params = {
          updated_after,
          per_page: perpage,
        };

        if (page) {
          params['page'] = page;
        }

        return this.download.get('/admin/subscriptions', params);
      },
      fnSave: async (enrollments: any[]) => {
        await this.save(enrollments);
        const enrollmentsLastDownload = enrollments[enrollments.length - 1]['updated_at'];

        this.settingsService.updateImport({ enrollmentsLastDownload });
      },
      lastPage: pagesLimit,
      startDate,
      perpage,
    });
    return this.counter;
  }

  async saveOne(eadEnrollment: any) {
    const dto = new CreateEnrollmentDto();

    const externalId = eadEnrollment.subscription_id;
    const userExternalId = eadEnrollment.user.user_id;
    const courseExternalId = eadEnrollment.course.course_id;

    const dbUser = await this.usersService.findOneByExernalId(userExternalId);
    if (!dbUser) {
      console.log(`User not found: ${eadEnrollment.user.email} [${userExternalId}]`);
      return;
    }

    const dbCourse = this.courses.find(c => c.externalId == courseExternalId);
    if (!dbCourse) {
      console.log(`Course not found: ${courseExternalId}`);
      return;
    }

    const active = eadEnrollment.approved && eadEnrollment.paid_and_valid;

    dto.userId = dbUser.id;
    dto.role = CourseRole.Student;
    dto.active = active;
    dto.dateFrom = eadEnrollment.started_at || eadEnrollment.created_at;
    dto.dateTo = eadEnrollment.expires_at;
    dto.externalId = externalId;

    const entity = await this.enrollmentsService.create(dbCourse.id, dto);

    return {
      ...eadEnrollment,
      entity,
    };
  }

  async updateOne(dbEnrollment: Enrollment, eadEnrollment: any) {
    const dto = new UpdateEnrollmentDto();

    const active = eadEnrollment.approved && eadEnrollment.paid_and_valid;
    const dateFrom = new Date(eadEnrollment.started_at || eadEnrollment.created_at)?.getTime();
    const dateTo = new Date(eadEnrollment.expires_at)?.getTime();

    if (dbEnrollment.active != active) {
      dto.active = active;
    }

    if (dbEnrollment.dateFrom?.getTime() != dateFrom) {
      dto.dateFrom = eadEnrollment.started_at || eadEnrollment.created_at;
    }

    if (dbEnrollment.dateTo?.getTime() != dateTo) {
      dto.dateTo = eadEnrollment.expires_at;
    }

    let entity = { ...dbEnrollment };
    let updated = false;
    if (Object.keys(dto).length > 0) {
      entity = await this.enrollmentsService.update(dbEnrollment.id, dto);
      updated = true;
    }

    return {
      ...eadEnrollment,
      entity,
      updated,
    };
  }

  async save(eadEnrollments: any[]) {
    const result = [];
    for (let i = 0, l = eadEnrollments.length; i < l; i++) {
      const eadEnrollment = eadEnrollments[i];
      const dbEnrollment = await this.enrollmentsService.findByExternalId(eadEnrollment.subscription_id);

      if (dbEnrollment) {
        const updated = await this.updateOne(dbEnrollment, eadEnrollment);
        result.push(updated);
        if (updated.updated) {
          this.counter.updated++;
        } else {
          this.counter.skipped++;
        }
      } else {
        const created = await this.saveOne(eadEnrollment);
        result.push(created);
        this.counter.created++;
      }
    }
    return result;
  }
}
