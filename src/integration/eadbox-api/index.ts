import axios from 'axios';
import * as courseapi from '../types/courseapi';
import * as courseapiParsed from '../types/courseapi-parsed';
import CourseResponse from '../types/courses';

export function buildTree(courseEadbox: courseapi.Root, courseStudent: any): courseapiParsed.Root | null {
  if (!courseEadbox) {
    return null;
  }

  if (courseStudent) {
    courseEadbox?.data?.forEach(module => {
      const moduleSlugObj = courseStudent?.modules.find(
        m => m.title === module.attributes.title && m.description === module.attributes.description,
      );
      if (moduleSlugObj) {
        module.attributes.slug = moduleSlugObj.module_slug;
      }
    });
  }

  const includedMap = courseEadbox?.included?.reduce((a, e) => ((a[e.id] = e), a), {});
  const build = node => {
    if (!node || !node?.relationships) return node;
    Object.keys(node?.relationships).forEach(key => {
      if (Array.isArray(node.relationships[key]?.data)) {
        node[key] = node.relationships[key]?.data?.map(node => {
          return build(includedMap[node.id]) || node;
        });
      } else {
        if (node.relationships[key]?.data) {
          node[key] = build(includedMap[node.relationships[key]?.data?.id]) || node.relationships[key]?.data;
        }
      }
    });
    delete node.relationships;
    return node;
  };
  const modules = Array.isArray(courseEadbox?.data)
    ? courseEadbox?.data?.map(module => {
        return build(module);
      })
    : build(courseEadbox?.data);
  return modules;
}

export async function downloadCourseModules(externalId, eadboxSessionCookie, getModuleSlug = true, DOWNLOAD_RETRY_COUNT = 3) {
  let data;
  let tryCount = 0;
  do {
    const url = `https://ebac-online.eadbox.com.br/ng/api/v2/teacher/courses/${externalId}/modules?&page[size]=100000000&page[number]=1`;
    console.log('start download from eadbox');
    data = await axios
      .get(url, {
        headers: { Cookie: `_session_id=${eadboxSessionCookie};` },
        timeout: 5 * 60000,
      })
      .then(res => {
        console.log('     finish download');
        return res.data;
      })
      .catch(() => {
        console.error('cant download course data from eadbox! externalId =', url);
      });
    tryCount++;
  } while (!data && tryCount < DOWNLOAD_RETRY_COUNT);

  if (!data) {
    return null;
  }

  if (getModuleSlug) {
    const courseStudent = await downloadCourse(externalId, eadboxSessionCookie);
    return buildTree(data, courseStudent);
  }

  return buildTree(data, null);
}

export async function downloadAssessment(courseSlug, assessmentId, eadboxSessionCookie, DOWNLOAD_RETRY_COUNT = 3): Promise<any> {
  let data;
  let tryCount = 0;
  do {
    const url = `https://ebac-online.eadbox.com.br/ng/api/v2/teacher/courses/${courseSlug}/assessments/${assessmentId}`;
    data = await axios
      .get(url, {
        headers: { Cookie: `_session_id=${eadboxSessionCookie};` },
        timeout: 5 * 60000,
      })
      .then(res => {
        return res.data;
      })
      .catch(() => {
        console.error('cant download assessments data from eadbox! externalId =', url);
      });
    tryCount++;
  } while (!data && tryCount < DOWNLOAD_RETRY_COUNT);

  return buildTree(data, null);
}

export async function startSession(submissionsService) {
  if (!process.env.EADBOX_ADMIN_LOGIN || !process.env.EADBOX_ADMIN_PASSWORD_BASE64) {
    throw new Error('Please provide EADBOX_ADMIN_LOGIN and EADBOX_ADMIN_PASSWORD_BASE64 env variables');
  }
  const EADBOX_PASSWORD = Buffer.from(String(process.env.EADBOX_ADMIN_PASSWORD_BASE64), 'base64').toString();

  return submissionsService.getBoxSession(process.env.EADBOX_ADMIN_LOGIN, EADBOX_PASSWORD);
}

export async function downloadCourse(courseSlug, eadboxSessionCookie) {
  const boxApiUrl = `https://ebac-online.eadbox.com.br/ng/api/student/courses/${courseSlug}.json`;
  const result = await axios
    .get<CourseResponse.CoursesData>(boxApiUrl, {
      headers: { Cookie: `_session_id=${eadboxSessionCookie};` },
    })
    .catch(err => {
      console.error('import courses', courseSlug, err);
    })
    .then(it => it && it.data);

  return result;
}
