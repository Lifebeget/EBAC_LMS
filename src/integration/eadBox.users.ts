import { SystemRole } from '@lib/api/types';
import { INestApplicationContext } from '@nestjs/common';
import { User } from 'src/lib/entities/user.entity';
import { SettingsService } from 'src/settings/settings.service';
import { AuthTriggerService } from 'src/trigger/services/auth-trigger.service';
import { CreateProfileDto } from 'src/users/dto/create-profile.dto';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UpdateProfileDto } from 'src/users/dto/update-profile.dto';
import { UpdateUserDto } from 'src/users/dto/update-user.dto';
import { ProfileService } from 'src/users/profile.service';
import { RolesService } from 'src/users/roles.service';
import { UsersService } from 'src/users/users.service';
import { counterTemplate } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';
import usersResponse from './types/users';

export class EadBoxUsers {
  private usersService: UsersService;
  private profileService: ProfileService;
  private rolesService: RolesService;
  private settingsService: SettingsService;
  private authTriggerService: AuthTriggerService;

  constructor(private context: INestApplicationContext, private download: EadBoxDownloadHelper, private counter = { ...counterTemplate }) {
    this.usersService = context.get(UsersService);
    this.profileService = context.get(ProfileService);
    this.rolesService = context.get(RolesService);
    this.settingsService = context.get(SettingsService);
    this.authTriggerService = context.get(AuthTriggerService);
  }

  async import(forcedStartDate?: string, pagesLimit: number = null, perpage?: number, locked?: boolean) {
    let startDate = forcedStartDate;

    if (!startDate) {
      const settings = await this.settingsService.getSettings();
      startDate = settings.importSettings.usersLastDownload || '1970-01-01';
    }

    await this.download.downloadByDate({
      name: 'users',
      fnClear: async () => this.clear(),
      fnGet: (updated_after: Date, page?: number) =>
        this.download.get('/admin/users', {
          locked,
          updated_after,
          per_page: perpage,
          page,
        }),
      fnSave: async (users: any[]) => {
        await this.save(users, locked);
        const usersLastDownload = users[users.length - 1]['updated_at'];
        this.settingsService.updateImport({ usersLastDownload });
      },
      lastPage: pagesLimit,
      startDate,
      perpage,
    });
    return this.counter;
  }

  async saveOne(eadUser: any, locked?: boolean) {
    const dto = new CreateUserDto();
    dto.externalId = eadUser.user_id;
    dto.name = eadUser.name;
    dto.email = eadUser.email;
    dto.eadboxToken = eadUser.auth_token;

    dto.active = !locked;

    // Generate password [a-z0-9]
    if (eadUser.group?.slug == 'teacher') {
      dto.password = Math.random().toString(36).substr(2, 8);

      // Fix from 27.04.2021, all the tutors will be active by default
      // And their active state will not be synced with EADBOX
      dto.active = true;
    }

    const entity = await this.usersService.create(dto);

    if (eadUser.group?.slug == 'teacher') {
      await this.rolesService.create(entity.id, { role: SystemRole.Tutor });
      this.authTriggerService.tutorWelcome(entity, dto.password);
    }

    return {
      ...eadUser,
      entity,
    };
  }

  async updateOne(dbUser: User, eadUser: any, locked?: boolean) {
    const dto = new UpdateUserDto();

    if (eadUser.name != dbUser.name) {
      dto.name = eadUser.name;
    }

    if (!dbUser.externalId) {
      dto.externalId = eadUser.user_id;
    }

    if (eadUser.email != dbUser.email) {
      dto.email = eadUser.email;
    }

    if (eadUser.auth_token != dbUser.eadboxToken) {
      dto.eadboxToken = eadUser.auth_token;
    }

    const active = !locked;
    if (dbUser.active !== active) {
      dto.active = active;
    }

    if (eadUser.group?.slug == 'teacher') {
      // Fix from 27.04.2021, all the tutors will be active by default
      // And their active state will not be synced with EADBOX
      delete dto.active;

      const roles = await this.rolesService.findByUser(dbUser.id);
      if (roles && roles.find(r => r.role == SystemRole.Tutor)) {
        // Do nothing
      } else {
        // Add tutor role
        await this.rolesService.create(dbUser.id, { role: SystemRole.Tutor });
      }

      if (!dbUser.password) {
        // Generate password [a-z0-9]
        dto.password = Math.random().toString(36).substr(2, 8);
        this.authTriggerService.tutorWelcome(dbUser, dto.password);
      }
    }

    if (Object.keys(dto).length > 0) {
      const entity = await this.usersService.update(dbUser.id, dto);
      return {
        ...eadUser,
        entity,
      };
    }
  }

  splitName(name): [string, string | null] {
    const words = name.match(/\S+/g);
    switch (words.length) {
      case 0:
        return ['', null];
      case 1:
        return [words.shift(), null];
      case 2:
        return [words.shift(), words.shift()];
      case 3:
        return [words.shift(), words.join(' ')];
      default:
        return [words.shift() + ' ' + words.shift(), words.join(' ')];
    }
  }

  async upsertProfile(dbUser: User, eadUser: usersResponse.User) {
    try {
      const dbProfile = await this.profileService.getProfileByUserId(dbUser.id);
      const [name, surname] = this.splitName(eadUser.name);

      if (dbProfile) {
        const dto = new UpdateProfileDto();

        if (dbProfile.name != name) dto.name = name;
        if (dbProfile.surname != surname) dto.surname = surname;
        if (dbProfile.cpf != eadUser.document_id) dto.cpf = eadUser.document_id;
        if (dbProfile.city != eadUser.city) dto.city = eadUser.city;
        if (eadUser.mobile_phone && dbProfile.phone != '' + eadUser.mobile_phone) dto.phone = '' + eadUser.mobile_phone;

        const eadUserSTR = JSON.stringify(eadUser);
        if (dbProfile.data != eadUserSTR) dbProfile.data = eadUserSTR;

        if (Object.keys(dto).length > 0) {
          await this.profileService.update(dbUser.id, dto);
        }
      } else {
        const dto = new CreateProfileDto();

        dto.name = name;
        dto.surname = surname;
        dto.cpf = eadUser.document_id;
        dto.city = eadUser.city;
        dto.phone = '' + eadUser.mobile_phone;
        dto.data = JSON.stringify(eadUser);
        await this.profileService.create(dbUser.id, dto);
      }
    } catch (error) {
      console.error(`upsertProfile userId=${dbUser?.id}`, error);
    }
  }

  async save(users: usersResponse.User[], locked?: boolean) {
    const start = Date.now();
    const result = [];

    for (let i = 0, l = users.length; i < l; i++) {
      const eadUser = users[i];
      let dbUser = await this.usersService.findOneByExernalId(eadUser.user_id);
      if (!dbUser) {
        dbUser = await this.usersService.findOneByEmail(eadUser.email);
      }

      if (dbUser) {
        let updated = false;
        await this.upsertProfile(dbUser, eadUser);
        try {
          updated = await this.updateOne(dbUser, eadUser, locked);
        } catch (err) {
          console.error('Cannot update user');
          console.dir(err);
        }
        if (updated) {
          this.counter.updated++;
          result.push(updated);
        } else {
          this.counter.skipped++;
        }
      } else {
        let created = false;
        try {
          created = await this.saveOne(eadUser, locked);
          await this.upsertProfile((created as any).entity, eadUser);
        } catch (err) {
          console.error('Cannot save user');
          console.dir(err);
        }
        if (created) {
          result.push(created);
          this.counter.created++;
        } else {
          this.counter.skipped++;
        }
      }
    }
    console.log(`save ${Date.now() - start}ms`);

    return result;
  }

  async clear() {
    if (process.env.EADBOX_CLEAR) {
      await this.usersService.purgeAll();
    }
  }
}
