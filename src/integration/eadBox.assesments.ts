import { SubmissionStatus } from '@lib/api/types';
import { INestApplicationContext } from '@nestjs/common';
import * as chalk from 'chalk';
import { AnswersService } from 'src/assessments/answers.service';
import { AssessmentsService } from 'src/assessments/assessments.service';
import { CreateAnswerDto } from 'src/assessments/dto/create-answer.dto';
import { CreateSubmissionDto } from 'src/assessments/dto/create-submission.dto';
import { UpdateAnswerDto } from 'src/assessments/dto/update-answer.dto';
import { UpdateSubmissionDto } from 'src/assessments/dto/update-submission.dto';
import { QuestionsService } from 'src/assessments/questions.service';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { printheading } from 'src/cli';
import { CoursesService } from 'src/courses/courses.service';
import { FilesService } from 'src/files/files.service';
import { Assessment } from 'src/lib/entities/assessment.entity';
import { ImportData, Submission } from 'src/lib/entities/submission.entity';
import { UsersService } from 'src/users/users.service';
import promisePool from 'src/_util/async-bottleneck';
import { countersTemplate, ImportCounters } from '../lib/types/ImportCounters';
import { EadBoxDownloadHelper } from './eadBox.download.helper';

export class EadBoxAssesments {
  private assessmentsService: AssessmentsService;
  private submissionService: SubmissionsService;
  private userService: UsersService;
  private answersService: AnswersService;
  private questionsService: QuestionsService;
  private coursesService: CoursesService;
  private filesService: FilesService;

  private counters = countersTemplate;

  constructor(private context: INestApplicationContext, private download: EadBoxDownloadHelper) {
    this.assessmentsService = context.get(AssessmentsService);
    this.submissionService = context.get(SubmissionsService);
    this.userService = context.get(UsersService);
    this.answersService = context.get(AnswersService);
    this.questionsService = context.get(QuestionsService);
    this.coursesService = context.get(CoursesService);
    this.filesService = context.get(FilesService);
  }

  getCounters(): ImportCounters {
    return this.counters;
  }

  async import({ lecturesLimit, pagesLimit, concurrency }) {
    const courses = await this.coursesService.findAll(true, true);
    for (let i = 0, l = courses.length; i < l; i++) {
      const course = courses[i];
      const lectures = course.modules.reduce((acc, cur) => acc.concat(cur.lectures), []);
      printheading(`Importing assessments for course: ${course.title} [${course.externalId}]`);
      if (!course.externalId) {
        console.log('Not imported course, skipping');
        continue;
      }

      if (lecturesLimit) {
        lectures.splice(lecturesLimit);
      }

      const importLecture = async lecture => {
        if (!lecture.externalId) {
          console.log('Not imported lecture, skipping');
          return;
        }
        await this.downloadForLecture({
          eadCourseId: course.externalId,
          eadLectureId: lecture.externalId,
          dbLectureId: lecture.id,
          lastPage: pagesLimit,
        });
      };

      await promisePool(lectures, importLecture.bind(this), concurrency);
    }
    return this.counters;
  }

  async saveAnswers(eadAnswers: any[], assessment: Assessment, submission: Submission): Promise<any> {
    const dbAnswers = await this.answersService.findForSubmission(submission.id);

    for (let i = 0, l = eadAnswers.length; i < l; i++) {
      const eadAnswer = eadAnswers[i];
      const eadQuestion = eadAnswer.question;
      if (!eadQuestion) {
        console.log('Weird answer. Skipping');
        console.dir(eadAnswers, i);
        this.counters.answers.skipped++;
        continue;
      }
      const question = assessment.questions.find(q => q.externalId == eadQuestion.question_id);
      if (!question) {
        console.log('Question not found. We look for ' + eadQuestion.question_id);
        console.dir(assessment);
        this.counters.answers.skipped++;
        continue;
      }

      const dbAnswer = dbAnswers.find(a => a.question.id == question.id);
      const score = Math.round(eadAnswer.grade) || 0;
      const comment = eadAnswer.body || null;

      const appendFile = async (url: string, answerDto: CreateAnswerDto | UpdateAnswerDto): Promise<void> => {
        if (url) {
          if (!Array.isArray(answerDto.files)) {
            answerDto.files = [];
          }
          const fileInfo = await this.download.fileInfo(url);
          const file = await this.filesService.create({
            title: fileInfo.filename,
            filename: fileInfo.filename,
            extension: fileInfo.extension,
            mimetype: fileInfo.mimetype,
            size: fileInfo.size,
            url: url,
            ownerId: submission.user.id,
          });
          answerDto.files.push(file);
        }
      };

      if (dbAnswer) {
        // Update
        const answerDto = new UpdateAnswerDto();
        if (dbAnswer.score != score) {
          answerDto.score = score;
        }

        if (comment && dbAnswer.comment != comment) {
          answerDto.comment = comment;
        }

        if (!dbAnswer.files.find(f => f.url == eadAnswer.file || f.eadboxUrl == eadAnswer.file)) {
          await Promise.all(dbAnswer.files.map(f => this.filesService.remove(f.id)));

          await appendFile(eadAnswer.file, answerDto);
        }

        if (Object.keys(answerDto).length > 0) {
          await this.answersService.update(dbAnswer.id, answerDto);
          this.counters.answers.updated++;
        } else {
          this.counters.answers.skipped++;
        }
      } else {
        const answerDto = {
          questionId: question.id,
          submissionId: submission.id,
          score: score,
          comment,
        } as CreateAnswerDto;

        await appendFile(eadAnswer.file, answerDto);

        await this.answersService.create(answerDto);
        this.counters.answers.created++;
      }
    }
  }

  async saveSubmission(eadAssessment: any, assessmentId: string, importData: ImportData): Promise<Submission> {
    if (!eadAssessment.user) {
      console.log('Submission without user. Skipping');
      this.counters.submissions.skipped++;
      return null;
    }
    const user = await this.userService.findOneByExernalId(eadAssessment.user.user_id);

    if (!user) {
      console.log('User not found. Skipping');
      this.counters.submissions.skipped++;
      return null;
    }
    const assessment = await this.assessmentsService.findOne(assessmentId);
    const lectureId = assessment.lecture?.id;
    const courseId = assessment.lecture?.course?.id;
    const moduleId = assessment.lecture?.module?.id;

    const dto = {
      userId: user.id,
      assessmentId,
      answers: [],
      status: eadAssessment.corrected ? SubmissionStatus.Graded : SubmissionStatus.Submitted,
      score: Math.round(eadAssessment.grade * 100),
      attempt: eadAssessment.attempt,
      externalId: eadAssessment.user_assessment_id,
      current_attempt: eadAssessment.current_attempt,
      submittedAt: new Date(),
      gradedAt: eadAssessment.corrected ? new Date() : null,
      importData,
      lectureId,
      courseId,
      moduleId,
    } as CreateSubmissionDto;

    const res = await this.submissionService.create(dto);
    this.counters.submissions.created++;
    return res;
  }

  async updateSubmission(dbSubmission: Submission, eadAssessment: any, importData: ImportData): Promise<Submission> {
    const dto = new UpdateSubmissionDto();

    const status = eadAssessment.corrected ? SubmissionStatus.Graded : SubmissionStatus.Submitted;

    if (dbSubmission.status != status && dbSubmission.status != SubmissionStatus.Graded) {
      dto.status = status;
      if (status == SubmissionStatus.Graded) {
        dto.gradedAt = new Date();
      }
    }

    const score = Math.round(eadAssessment.grade * 100);
    if (dbSubmission.score != score) {
      dto.score = score;
    }

    if (dbSubmission.attempt != eadAssessment.attempt) {
      dto.attempt = eadAssessment.attempt;
    }

    if (dbSubmission.current_attempt != eadAssessment.current_attempt) {
      dto.current_attempt = eadAssessment.current_attempt;
    }

    if (dbSubmission.importData === null) {
      dto.importData = importData;
    }

    if (!dbSubmission.lecture || !dbSubmission.course || !dbSubmission.module) {
      const assessment = await this.assessmentsService.findOne(dbSubmission.assessment.id);
      if (!assessment.lecture) {
        console.error(`ERROR: Assessment is not attached to lecture ${assessment.id}`);
      }
      dto.lectureId = assessment.lecture?.id;
      dto.courseId = assessment.lecture?.course?.id;
      dto.moduleId = assessment.lecture?.module?.id;
    }

    let submission = dbSubmission;
    if (Object.keys(dto).length > 0) {
      dto.externalId = dbSubmission.externalId; // Passing to result
      submission = await this.submissionService.update(dbSubmission.id, dto);
      this.counters.submissions.updated++;
    } else {
      this.counters.submissions.skipped++;
    }

    return submission;
  }

  filterUniqueAssessments(eadAssessments: any[]): any[] {
    return eadAssessments.reduce((acc, cur) => {
      const assessment_id = cur.assessment?.assessment_id;
      if (!assessment_id) {
        console.log('Weird assessment. Skipping');
        console.dir(cur);
        return acc;
      }
      // Skip submissions with empty answers, they are not actually submitted yet
      if (cur.answers.length == 0) {
        return acc;
      }
      // Add assessment to the array if not already present
      if (!acc.some(a => a.assessment_id == assessment_id)) {
        acc.push(cur);
      }
      return acc;
    }, []);
  }

  async save(eadAssessments: any[], lectureId: string, page: number) {
    if (!eadAssessments || eadAssessments.length == 0) {
      return;
    }

    console.log(chalk.cyan(`Saving ${eadAssessments.length} submissions`));

    // Handle actual assessments before saving submissions
    const uniqueAssessments = this.filterUniqueAssessments(eadAssessments);

    if (uniqueAssessments.length == 0) {
      console.log(`No valid assessments found for the lecture ${lectureId}. Skipping.`);
      return [];
    }

    const findDbAssessment = async validAssessment => {
      const assessment = await this.assessmentsService.findByExternalId(validAssessment.assessment.assessment_id);

      if (!assessment) {
        return;
      }
      validAssessment.dbAssessment = assessment; // Mutating
    };

    let index = 0;

    const saveAssessment = async eadAssessment => {
      const assessment = eadAssessment.dbAssessment;
      if (eadAssessment.answers?.length == 0) {
        // That's not yet submitted homework, skipping
        return;
      }
      if (!assessment) {
        console.log('No valid assessment for this submission, skipping');
        console.dir(eadAssessment);
        return;
      }

      const dbSubmission = await this.submissionService.findByExternalId(eadAssessment.user_assessment_id);
      let submission;
      const importData: ImportData = {
        page,
        index,
      };
      if (dbSubmission) {
        submission = await this.updateSubmission(dbSubmission, eadAssessment, importData);
      } else {
        submission = await this.saveSubmission(eadAssessment, assessment.id, importData);
      }
      if (!submission || !submission.user?.id) {
        return;
      }
      await this.saveAnswers(eadAssessment.answers, assessment, submission);
      index++;
    };

    await promisePool(uniqueAssessments, findDbAssessment.bind(this), 10);
    await promisePool(eadAssessments, saveAssessment.bind(this), 10);
  }

  async downloadForLecture({ eadCourseId, eadLectureId, dbLectureId, firstPage = 1, lastPage = null, alterGet = null }) {
    const defaultGet = page => {
      return this.download.get(
        `/admin/courses/${eadCourseId}/lectures/${eadLectureId}/user_assessments`,
        { page },
        false, //silent
      );
    };

    const fnGet = alterGet ?? defaultGet;

    return await this.download.downloadByPage({
      name: 'assessments',
      fnGet,
      fnSave: async (assessments: any[], page: number) => this.save(assessments, dbLectureId, page),
      firstPage,
      lastPage,
      silent: false,
    });
  }
}
