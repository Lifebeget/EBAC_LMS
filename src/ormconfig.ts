import { join } from 'path';
import { ConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

const CLIENT_MODE = process.env.CLIENT_MODE === 'true';

let partialConfig: ConnectionOptions;

if (process.env.DB_MASTER_HOST && process.env.DB_SLAVE_HOST) {
  partialConfig = {
    type: 'postgres',
    replication: {
      master: {
        host: process.env.DB_MASTER_HOST,
        port: parseInt(process.env.DB_MASTER_PORT),
        username: process.env.DB_MASTER_USER,
        password: process.env.DB_MASTER_PASSWORD,
        database: process.env.DB_MASTER_NAME,
      },
      slaves: [
        {
          host: process.env.DB_SLAVE_HOST,
          port: parseInt(process.env.DB_SLAVE_PORT),
          username: process.env.DB_SLAVE_USER,
          password: process.env.DB_SLAVE_PASSWORD,
          database: process.env.DB_SLAVE_NAME,
        },
      ],
    },
  };
} else {
  partialConfig = {
    type: 'postgres',
    host: process.env.DB_MASTER_HOST || process.env.DB_HOST,
    port: parseInt(process.env.DB_MASTER_PORT || process.env.DB_PORT) || 5432,
    username: process.env.DB_MASTER_USER || process.env.DB_USER,
    password: process.env.DB_MASTER_PASSWORD || process.env.DB_PASSWORD,
    database: process.env.DB_MASTER_NAME || process.env.DB_NAME,
  };
}

const connectionOptions: ConnectionOptions = {
  ...partialConfig,
  entities: [__dirname + '/**/*.entity{.ts,.js}'],
  namingStrategy: new SnakeNamingStrategy(),
  logging: ['error'],
  migrationsRun: false, // !CLIENT_MODE,
  migrations: [join(__dirname, 'migrations/*{.ts,.js}')],
  cli: {
    migrationsDir: 'src/migrations',
  },

  synchronize: false, // !CLIENT_MODE, // Set to false after deploy,
  cache: {
    duration: 60000, // 60 seconds
    type: 'redis',
    options: {
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
    },
    ignoreErrors: true,
  },
};

export = connectionOptions;
