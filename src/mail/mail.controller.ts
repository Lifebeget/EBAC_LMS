import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Interapp } from 'src/auth/interapp.decorator';
import { SendExpertSenderCustomEventDto } from 'src/expert-sender-api/custom-events-expert-sender/dto/send-expert-sender-custom-event.dto';
import { Pagination } from 'src/pagination.decorator';
import { PaginationDto } from 'src/pagination.dto';
import { QueryRetryMailDto } from './dto/query-retry-mail.dto';
import { RetryMailDto } from './dto/retry-mail.dto';
import { SendToExpertsenderDto } from './dto/send-to-expertsender.dto';
import { MailService } from './mail.service';

@ApiTags('mail')
@Controller('mail')
export class MailController {
  constructor(private mailService: MailService) {}

  @Get('get-failed')
  @ApiOperation({ summary: 'Get failed emails' })
  async getFailedEmails(@Pagination() paging: PaginationDto, @Query() query: QueryRetryMailDto) {
    return await this.mailService.getFailedEmails(paging, query);
  }

  @Post('retry')
  @ApiOperation({ summary: 'Add email to queue' })
  @ApiResponse({
    status: 201,
    description: 'Email added to queue',
  })
  async retryMail(@Body() dto: RetryMailDto) {
    return await this.mailService.retryMail(dto);
  }

  @Post('send-to-expertsender')
  @Interapp()
  @ApiOperation({ summary: 'Send client to Expertsender' })
  @ApiResponse({
    status: 201,
    description: 'Client added to Expertsender with transactional email',
  })
  async sendToExpertSender(@Body() body: SendToExpertsenderDto) {
    await this.mailService.sendToExpertsenderWithEmail(body);
    return {};
  }

  @Post('send-expert-sender-custom-event')
  @Interapp()
  @ApiOperation({ summary: 'Send ES custom event' })
  async sendExpertSenderCustomEvent(@Body() body: SendExpertSenderCustomEventDto) {
    await this.mailService.sendExpertSenderCustomEvent(body);
    return {};
  }
}
