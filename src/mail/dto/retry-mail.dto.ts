import { TriggerType } from '@enums/trigger-type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsOptional, IsString } from 'class-validator';

export class RetryMailDto {
  @ApiProperty({ description: 'email ID', required: false })
  @IsOptional()
  id?: string;

  @ApiProperty({ description: 'User ID' })
  @IsDefined()
  @IsString()
  userId: string;

  @ApiProperty({ description: 'email' })
  @IsDefined()
  @IsString()
  @IsEmail()
  to: string;

  @ApiProperty({ description: 'Trigger type' })
  triggerType: TriggerType;

  @ApiProperty({ description: 'email data' })
  data?: Record<string, any>;
}
