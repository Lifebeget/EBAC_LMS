import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsNumber, IsObject, IsOptional, IsString } from 'class-validator';
import { SnippetsTransactionalsExpertSenderInterface } from 'src/expert-sender-api/transactionals-expert-sender/interfaces/snippets-transactionals-expert-sender.interface';

export class SendToExpertsenderDto {
  @ApiProperty({ description: 'Client email' })
  @IsDefined()
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty({ description: 'Expertsender subscription list id' })
  @IsOptional()
  @IsNumber()
  esListId?: number;

  @ApiProperty({ description: 'Expertsender welcome transactional id' })
  @IsOptional()
  @IsNumber()
  esWelcomeTransactionalId?: number;

  @ApiProperty({ description: 'Expertsender snippets' })
  @IsOptional()
  @IsObject()
  snippets?: SnippetsTransactionalsExpertSenderInterface;
}
