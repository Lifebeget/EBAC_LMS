import { ApiProperty } from '@nestjs/swagger';

export class QueryRetryMailDto {
  @ApiProperty({ description: 'String to search', required: false })
  search?: string;
}