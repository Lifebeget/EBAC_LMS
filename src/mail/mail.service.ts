import { EmailQueue } from '@entities/email-queue.entity';
import { User } from '@entities/user.entity';
import { EmailStatus } from '@enums/email-status.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { InjectQueue, Process, Processor } from '@nestjs/bull';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Job, JobOptions, Queue } from 'bull';
import config from 'config';
import { CustomProcess } from 'src/decorators/custom-process.decorator';
import { SendExpertSenderCustomEventDto } from 'src/expert-sender-api/custom-events-expert-sender/dto/send-expert-sender-custom-event.dto';
import { ExpertSenderApiService } from 'src/expert-sender-api/expert-sender-api.service';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { Repository } from 'typeorm';
import { EXPERT_SENDER_API_SERVICE } from '../expert-sender-api/constants/constants';
import { QueryRetryMailDto } from './dto/query-retry-mail.dto';
import { RetryMailDto } from './dto/retry-mail.dto';
import { SendToExpertsenderDto } from './dto/send-to-expertsender.dto';

const mailToIds = config.mails;

@Processor('email')
@Injectable()
export class MailService {
  constructor(
    @InjectRepository(EmailQueue)
    private readonly emailQueueRepository: Repository<EmailQueue>,
    @Inject(EXPERT_SENDER_API_SERVICE)
    private expertSenderApiService: ExpertSenderApiService,
    @InjectQueue('email') private emailQueue: Queue,
  ) {}

  async addEmailToQueue(
    user: User | Partial<User>,
    triggerType: TriggerType,
    data: Record<string, any>,
    queueParams: JobOptions,
    emailId: string = null,
  ) {
    return await this.emailQueue.add(
      {
        user,
        triggerType,
        data,
        emailId,
      },
      queueParams,
    );
  }

  @CustomProcess()
  async consumeEmailQueue(
    job: Job<{
      user: User | Partial<User>;
      triggerType: TriggerType;
      data: Record<string, any>;
      emailId: string;
    }>,
  ) {
    return await this.sendEmail(job.data.user, job.data.triggerType, job.data.data, job.data.emailId);
  }

  async sendEmail(user: User | Partial<User>, triggerType: TriggerType, data: Record<string, any>, emailId: string) {
    if (!mailToIds[config.language][triggerType]) {
      return this.emailQueueRepository.save({
        status: EmailStatus.skipped,
        to: user.email,
        userId: user.id,
        triggerType,
        data,
      });
    }

    const enableEmails: boolean = process.env.ENABLE_EMAILS === 'ENABLE';

    if (!enableEmails) {
      const devEmails = process.env.EMAILS_ALLOWED.split(',').map(email => email.trim());
      if (!Array.isArray(devEmails) || (devEmails[0] === '' && devEmails.length === 1)) {
        throw new Error('You must define dev emails in config');
      }
      if (!devEmails.includes(user.email)) {
        return this.emailQueueRepository.save({
          status: EmailStatus.skipped,
          to: user.email,
          userId: user.id,
          triggerType,
          data,
        });
      }
    }

    let emailQueue = null;
    if (emailId) {
      emailQueue = await this.emailQueueRepository.findOne({ id: emailId });
    }
    if (!emailQueue) {
      emailQueue = this.emailQueueRepository.create({
        status: EmailStatus.sending,
        to: user.email,
        userId: user.id,
        triggerType,
        data,
      });
    } else {
      emailQueue.status = EmailStatus.sending;
    }

    const apiKey = process.env.EXPERTSENDER_API_KEY;
    const params = {
      apiKey,
      id: mailToIds[config.language][triggerType],
      returnGuid: true,
      receiver: {
        email: user.email,
      },
      snippets: data,
    };

    try {
      await this.expertSenderApiService.sendTransactionalToSubscriber(params);
      emailQueue.status = EmailStatus.sent;
    } catch (error) {
      try {
        await this.expertSenderApiService.upsertSubscriber({
          apiKey: '',
          verboseErrors: true,
          subscriber: {
            email: user.email,
            listId: parseInt(process.env.EXPERTSENDER_MAIL_LIST),
          },
        });
        await this.expertSenderApiService.sendTransactionalToSubscriber(params);
        emailQueue.status = EmailStatus.sent;
      } catch (error2) {
        emailQueue.status = EmailStatus.failed;
        emailQueue.statusCode = error2.response?.data?.ErrorMessage?.Code || error.response?.data?.ErrorMessage?.Code;
        emailQueue.statusText = error2.response?.data?.ErrorMessage?.Message || error.response?.data?.ErrorMessage?.Message;
      }
    }

    return this.emailQueueRepository.save(emailQueue);
  }

  async sendToExpertsenderWithEmail({ email, esWelcomeTransactionalId, snippets, esListId }: SendToExpertsenderDto): Promise<void> {
    // Add to primary list
    await this.expertSenderApiService.upsertSubscriber({
      verboseErrors: true,
      subscriber: {
        email,
        listId: parseInt(process.env.EXPERTSENDER_MAIL_LIST),
      },
    });

    // Add to additional list (if needed)
    if (esListId) {
      await this.expertSenderApiService.upsertSubscriber({
        verboseErrors: true,
        subscriber: {
          email,
          listId: esListId,
        },
      });
    }

    await this.expertSenderApiService.sendTransactionalToSubscriber({
      id: esWelcomeTransactionalId,
      returnGuid: true,
      receiver: {
        email,
      },
      snippets,
    });
  }

  async sendExpertSenderCustomEvent(dto: SendExpertSenderCustomEventDto) {
    return this.expertSenderApiService.sendCustomEvent(dto);
  }

  async getFailedEmails(paging: PaginationDto, query: QueryRetryMailDto): Promise<Paginated<EmailQueue[]>> {
    let queryBuilder = this.emailQueueRepository.createQueryBuilder('email_queue');

    queryBuilder = queryBuilder.andWhere('email_queue.status = :status', {
      status: 'Failed',
    });

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (query.search) {
      queryBuilder.andWhere(
        "LOWER(CONCAT(email_queue.id, ' ', email_queue.user_id, ' ', email_queue.to, ' ', email_queue.trigger_type)) like :like",
        {
          like: `%${query.search.toLowerCase()}%`,
        },
      );
    }
    queryBuilder.addOrderBy('email_queue.created', 'DESC');

    const [res, total] = await queryBuilder.getManyAndCount();

    return {
      results: res,
      meta: { ...paging, total },
    };
  }

  async retryMail(dto: RetryMailDto) {
    const user = new User();
    user.id = dto.userId;
    user.email = dto.to;

    await this.addEmailToQueue(user, dto.triggerType, dto.data, {}, dto.id);
  }
}
