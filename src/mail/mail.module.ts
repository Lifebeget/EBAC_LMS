import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { MailService } from './mail.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailQueue } from '@entities/email-queue.entity';
import { ExpertSenderApiModule } from 'src/expert-sender-api/expert-sender-api.module';
import { BullModule } from '@nestjs/bull';
import { MailController } from './mail.controller';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: {
          service: process.env.MAIL_SERVICE,
          host: process.env.MAIL_HOST,
          auth: {
            user: process.env.MAIL_USER,
            pass: Buffer.from(String(process.env.MAIL_PASSWORD), 'base64').toString(),
          },
        },
        defaults: {
          from: process.env.MAIL_FROM,
        },
      }),
    }),
    TypeOrmModule.forFeature([EmailQueue]),
    ExpertSenderApiModule,
    BullModule.registerQueue({
      name: 'email',
    }),
  ],
  controllers: [MailController],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
