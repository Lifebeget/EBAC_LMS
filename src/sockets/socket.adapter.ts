import { IoAdapter } from '@nestjs/platform-socket.io';
import { RedisClient } from 'redis';
import { ServerOptions } from 'socket.io';
import * as redisIoAdapter from 'socket.io-redis';

const pubClient = new RedisClient({
  host: process.env.REDIS_HOST || '127.0.0.1',
  port: +process.env.REDIS_PORT || 6379,
});
const subClient = pubClient.duplicate();
const redisAdapter = redisIoAdapter({ pubClient, subClient });
export class SocketAdapter extends IoAdapter {
  createIOServer(port: number, options?: ServerOptions): any {
    const server = super.createIOServer(port, options);
    server.adapter(redisAdapter);
    return server;
  }
}
