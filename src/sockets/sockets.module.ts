import { forwardRef, Module } from '@nestjs/common';
import { SocketsGateway } from './sockets.gateway';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/lib/entities/user.entity';
import { Role } from 'src/lib/entities/role.entity';
import { UserActivity } from 'src/lib/entities/user-activity.entity';
import { FilesModule } from 'src/files/files.module';
import { Hierarchy } from '@entities/hierarchy.entity';
import { Webinar } from '@entities/webinars.entity';
import { Trigger } from '@entities/trigger.entity';
import { SettingsModule } from 'src/settings/settings.module';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { Tag } from '@entities/tag.entity';
import { TagCategory } from '@entities/tag-category.entity';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { Segment } from '@entities/segment.entity';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    FilesModule,
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Role]),
    TypeOrmModule.forFeature([UserActivity]),
    TypeOrmModule.forFeature([Hierarchy]),
    TypeOrmModule.forFeature([Webinar]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([FeatureFlag]),
    TypeOrmModule.forFeature([TagCategory]),
    TypeOrmModule.forFeature([Segment]),
    SettingsModule,
    forwardRef(() => AssessmentsModule),
  ],
  providers: [SocketsGateway],
  exports: [SocketsGateway],
})
export class SocketsModule {}
