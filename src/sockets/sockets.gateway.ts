import { Nps } from '@entities/nps.entity';
import { SystemNotification } from '@entities/system-notification.entity';
import { Logger } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { appPromise } from 'src/app';
import { LecturesService } from 'src/courses/lectures.service';
import { Notification } from 'src/lib/entities/notification.entity';
import { KalturaCallback } from 'src/lib/types/KalturaCallback';
import { AuthService } from '../auth/auth.service';

type SocketUser = Socket & { handshake: { userId: string } };

@WebSocketGateway({
  path: '/socket.io',
  transports: ['websocket'],
})
export class SocketsGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  @WebSocketServer()
  server: Server;
  usersCount = 0;
  userIdToSocketId: Record<string, string[]> = {};
  anonymousSocketIds: string[] = [];
  updateUsers: null;
  updateUsersDate: any = 0;

  constructor(private authService: AuthService) {}

  afterInit() {
    // @ts-ignore TODO: need description
    this.server.of('/').adapter.customHook = (request, cb) => {
      const type = request.type;
      if (type === 'getUsers') {
        cb({
          authorized: JSON.stringify(this.userIdToSocketId),
          unauthorized: this.anonymousSocketIds.length,
        });
        return;
      }
      cb();
    };
  }

  private logger: Logger = new Logger('SocketIO');

  getUsers() {
    return new Promise(res => {
      this.server
        .of('/')
        // @ts-ignore TODO: need description
        .adapter.customRequest({ type: 'getUsers' }, function (error, replies) {
          const result: any = {
            authorized: {},
            unauthorized: 0,
          };

          if (error) {
            this.logger.error(error);
            result.error = true;
            res(result);
          }

          try {
            replies
              .filter(e => e)
              .forEach(res => {
                result.unauthorized += res.unauthorized;
                res.authorized = JSON.parse(res.authorized);
                Object.keys(res.authorized).forEach(k => {
                  if (!result.authorized[k]) result.authorized[k] = [];
                  result.authorized[k] = result.authorized[k].concat(res.authorized[k]);
                });
              });
            res(result);
          } catch (error) {
            this.logger.error(error);
            res({
              authorized: {},
              unauthorized: 0,
              error: true,
            });
          }
        });
    });
  }

  handleDisconnect(client: SocketUser) {
    client.leaveAll();
    this.usersCount--;
    this.removeUser(client.handshake.userId, client.id);
  }

  async handleConnection(client: SocketUser) {
    if (!client.handshake?.query?.token || ['null', 'undefined'].includes(client.handshake?.query?.token)) {
      client.send('Missing token.').disconnect();
      return;
    }

    this.usersCount++;
    try {
      const payload = await this.authService.verifyUser(client.handshake.query.token);
      client.join(payload.sub);
      client.handshake.userId = payload.sub;
      this.setUser(payload.sub, client.id);
    } catch (e) {
      this.setAnonymousUser(client.id);
      this.logger.log(`JWT parse err: ${e}`);
    }
  }

  setUser(userId, socketId) {
    if (!this.userIdToSocketId[userId]) {
      this.userIdToSocketId[userId] = [];
    }
    this.userIdToSocketId[userId].push(socketId);
    this.emitUsers();
  }

  setAnonymousUser(socketId) {
    if (this.anonymousSocketIds && !this.anonymousSocketIds.includes(socketId)) {
      this.anonymousSocketIds.push(socketId);
      this.emitUsers();
    }
  }

  removeAnonymousUser(socketId) {
    this.anonymousSocketIds = this.anonymousSocketIds.filter(item => item !== socketId);
  }

  loginUser(userId, socketId) {
    this.setUser(userId, socketId);
    this.removeAnonymousUser(socketId);
  }

  logoutUser(userId, socketId) {
    this.removeUserSessions(userId);
    this.setAnonymousUser(socketId);
  }

  getSocketUser(userId) {
    return this.userIdToSocketId[userId];
  }

  getUserBySocket(socketId) {
    return Object.keys(this.userIdToSocketId).find(key => this.userIdToSocketId[key].includes(socketId));
  }

  removeUser(userId, socketId) {
    if (this.userIdToSocketId[userId]) {
      this.userIdToSocketId[userId] = this.userIdToSocketId[userId].filter(item => item !== socketId);
      if (this.userIdToSocketId[userId].length === 0) {
        delete this.userIdToSocketId[userId];
      }
    } else {
      this.removeAnonymousUser(socketId);
    }
    this.emitUsers();
  }

  removeUserSessions(userId) {
    if (this.userIdToSocketId[userId]) {
      this.userIdToSocketId[userId].forEach(socketUser => {
        this.server.sockets.to(socketUser).emit('triggerLogout');
        this.setAnonymousUser(socketUser);
      });
      this.userIdToSocketId[userId] = [];
      delete this.userIdToSocketId[userId];
    }
    this.emitUsers();
  }

  emitUsers(client?: SocketUser) {
    if (!client) {
      return;
    }
    this.getUsers()
      .then(res => {
        if (!client) {
          this.serverEmit('usersChange', res);
        } else {
          client.emit('usersChange', res);
        }
      })
      .catch(error => {
        this.logger.error(error);
      });
  }

  serverEmit(eventName: string, payload) {
    try {
      if (this.server) {
        this.server.sockets.emit(eventName, payload);
      } else {
        this.logger.log(`Server is null`);
      }
    } catch (e) {
      this.logger.log(`Server emit err: ${e}`);
    }
  }

  dataUpdate(payload: string) {
    this.serverEmit('dataUpdate', payload);
  }

  dataUpdateForUser(payload: string, userId) {
    this.server.sockets.to(userId).emit('dataUpdate', payload);
  }

  sendNotification(notification: Notification) {
    this.server.sockets.to(notification.recipient.id).emit('notification', notification);
  }

  submissionChange(data) {
    this.server.sockets.emit('submissionChange', data);
  }

  sendNpsAvailable(nps: Nps) {
    this.server.sockets.to(nps.userId).emit('npsAvailable', nps);
  }

  sendKalturaCallback(room: string, payload: KalturaCallback) {
    const [socketRoom, embedUUID] = room.split(':');
    return this.server.sockets.to(socketRoom).emit('kalturaCallback', { ...payload, embedUUID });
  }

  sendSystemNotification(rooms: string[], event: string, payload: SystemNotification | string) {
    if (!rooms.length) {
      return false;
    }
    const socketRooms = rooms.reduce((acc, room) => acc.to(room), this.server.sockets);

    return socketRooms.emit(event, payload);
  }

  @SubscribeMessage('sendMessage')
  handleMessage(client: Socket, payload: string): void {
    this.serverEmit('receiveMessage', payload);
  }

  @SubscribeMessage('getUsers')
  handleGetUsers(client: SocketUser): void {
    this.emitUsers(client);
  }

  // unsecure!!!
  // @SubscribeMessage('login')
  // handleLogin(client: Socket, payload: string): void {
  //   this.loginUser(payload, client.id);
  //   this.usersService.saveActivity(payload, { lastLogin: new Date() });
  // }

  @SubscribeMessage('logout')
  handleLogout(client: SocketUser, payload: string): void {
    this.logoutUser(payload, client.id);
  }

  @SubscribeMessage('visitLecture')
  async visitLecture(client: SocketUser, payload: { lectureId: string }) {
    const app = await appPromise;
    const lectureService = app.get(LecturesService);

    const userId = client.handshake.userId;

    if (userId && payload.lectureId) {
      await lectureService.saveLectureProgress(userId, payload.lectureId, {
        viewed: new Date(),
      });
      const lectureProgress = await lectureService.findOneLecturesProgress({
        userId,
        lectureId: payload.lectureId,
      });
      this.server.sockets.to(userId).emit('updateLectureProgress', { payload: lectureProgress });
    }
  }

  @SubscribeMessage('endsLecture')
  async endsLecture(client: SocketUser, payload: { lectureId: string }) {
    const app = await appPromise;
    const lectureService = app.get(LecturesService);

    const userId = client.handshake.userId;

    if (userId && payload.lectureId) {
      lectureService.saveLectureProgress(userId, payload.lectureId, {
        ended: new Date(),
        progress: 100,
      });
    }
  }

  @SubscribeMessage('progressLecture')
  async saveLectureProgress(
    client: SocketUser,
    payload: {
      lectureId: string;
      progress: number;
    },
  ) {
    const app = await appPromise;
    const lectureService = app.get(LecturesService);

    const userId = client.handshake.userId;

    if (userId && payload.lectureId) {
      lectureService.saveLectureProgress(userId, payload.lectureId, {
        progress: payload.progress,
      });
    }
  }

  @SubscribeMessage('subscribeToTypeRoom')
  addSocketToTypeRoom(socket: Socket, typeRoom: string) {
    socket.join(typeRoom);
  }
}
