import { Content } from '@entities/content.entity';
import { Lecture } from '@entities/lecture.entity';
import { Trigger } from '@entities/trigger.entity';
import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { MailModule } from 'src/mail/mail.module';
import { UsersModule } from 'src/users/users.module';
import { AmoModule } from 'src/amo/amo.module';
import { CertListeners } from './listeners/cert.listeners';
import { CourseListeners } from './listeners/course.listeners';
import { FeedbackListeners } from './listeners/feedback.listeners';
import { HomeworkListeners } from './listeners/homework.listeners';
import { ModuleCompletedListeners } from './listeners/module-completed.listeners';
import { ModulePublishedListeners } from './listeners/module-published.listeners';
import { SurveyListeners } from './listeners/survey.listeners';
import { WelcomeListeners } from './listeners/welcome.listeners';
import { WelcomeTriggerService } from './services/welcome-trigger.service';
import { TriggerService } from './trigger.service';
import { Module as ModuleEntity } from '../lib/entities/module.entity';
import { Course, CourseToSatellite } from '@entities/course.entity';
import { Tag } from '@entities/tag.entity';
import { File } from '@entities/file.entity';
import { CertTriggerService } from './services/cert-trigger.service';
import { Webinar } from '@entities/webinars.entity';
import { SubmissionTriggerService } from './services/submission-trigger.service';
import { SubmissionListeners } from './listeners/submission.listeners';
import { ModuleTriggerService } from './services/module-trigger.service';
import { User } from '@entities/user.entity';
import { Enrollment } from '@entities/enrollment.entity';
import { SettingsModule } from 'src/settings/settings.module';
import { AuthTriggerService } from './services/auth-trigger.service';
import { AuthListeners } from './listeners/auth.listeners';
import { EnrollmentTriggerService } from './services/enrollment-trigger.service';
import { EnrollmentListeners } from './listeners/enrollment.listeners';
import { LectureProgressTriggerService } from './services/lecture-progress-trigger.service';
import { LectureProgressListeners } from './listeners/lecture-progress.listeners';
import { CoursesModule } from '../courses/courses.module';
import { LectureProgress } from '@entities/lecture-progress.entity';
import { SupportMessagesTriggerService } from './services/support-messages-trigger.service';
import { SupportMessagesListeners } from './listeners/support-messages.listeners';
import { ContentTriggerService } from './services/content-trigger.service';
import { ContentListeners } from './listeners/content.listener';
import { CoversModule } from 'src/covers/covers.module';
import { NpsModule } from 'src/nps/nps.module';
import { ForumListener } from './listeners/forum.listener';
import { ForumTriggerService } from './services/forum-trigger.service';
import { TagsModule } from 'src/tags/tags.module';
import { TagCategory } from '@entities/tag-category.entity';
import { Segment } from '@entities/segment.entity';
import { ProfileListeners } from './listeners/profile.listeners';
import { AuthModule } from 'src/auth/auth.module';
import { WebinarModule } from 'src/webinar/webinar.module';
import { ProfileTriggerService } from './services/profile-trigger.service';
import { CioEnrollmentListeners } from './listeners/cio-enrollment.listeners';
import { KafkaModule } from 'src/kafka/kafka.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([Course]),
    TypeOrmModule.forFeature([CourseToSatellite]),
    TypeOrmModule.forFeature([Lecture]),
    TypeOrmModule.forFeature([LectureProgress]),
    TypeOrmModule.forFeature([Content]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([ModuleEntity]),
    TypeOrmModule.forFeature([File]),
    TypeOrmModule.forFeature([Webinar]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Enrollment]),
    TypeOrmModule.forFeature([TagCategory]),
    TypeOrmModule.forFeature([Segment]),
    MailModule,
    SettingsModule,
    forwardRef(() => CoursesModule),
    forwardRef(() => AuthModule),
    forwardRef(() => UsersModule),
    forwardRef(() => WebinarModule),
    AmoModule,
    CoversModule,
    TagsModule,
    forwardRef(() => AssessmentsModule),
    forwardRef(() => NpsModule),
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.JWT_TOKEN_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRATION_TIME,
        },
      }),
    }),
    KafkaModule,
    ConfigModule,
  ],
  providers: [
    // listeners
    ModuleCompletedListeners,
    WelcomeListeners,
    FeedbackListeners,
    SurveyListeners,
    CourseListeners,
    ModulePublishedListeners,
    HomeworkListeners,
    CertListeners,
    SubmissionListeners,
    AuthListeners,
    EnrollmentListeners,
    CioEnrollmentListeners,
    LectureProgressListeners,
    SupportMessagesListeners,
    ContentListeners,
    ForumListener,
    ProfileListeners,
    //triggers
    TriggerService,
    WelcomeTriggerService,
    CertTriggerService,
    SubmissionTriggerService,
    ModuleTriggerService,
    AuthTriggerService,
    EnrollmentTriggerService,
    LectureProgressTriggerService,
    SupportMessagesTriggerService,
    ContentTriggerService,
    ForumTriggerService,
    ProfileTriggerService,
  ],
  exports: [
    TriggerService,
    WelcomeTriggerService,
    CertTriggerService,
    SubmissionTriggerService,
    ModuleTriggerService,
    AuthTriggerService,
    EnrollmentTriggerService,
    LectureProgressTriggerService,
    SupportMessagesTriggerService,
    ContentTriggerService,
    ForumTriggerService,
    ProfileTriggerService,
  ],
})
export class TriggerModule {}
