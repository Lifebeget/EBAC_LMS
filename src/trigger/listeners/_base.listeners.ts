import { Injectable } from '@nestjs/common';
import { MailService } from '../../mail/mail.service';
import { UsersService } from '../../users/users.service';

@Injectable()
export class BaseListeners {
  constructor(protected readonly usersService: UsersService, protected readonly mailService: MailService) {}

  protected async getUserById(id: string) {
    return await this.usersService.findOne(id);
  }
}
