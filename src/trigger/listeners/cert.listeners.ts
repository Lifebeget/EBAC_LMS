import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { WebinarCertCreated } from '../events/webinar-cert-created.event';
import { CourseCertCreated } from '../events/course-cert-created.event';
import { BaseListeners } from './_base.listeners';
import { MailService } from '../../mail/mail.service';
import { UsersService } from '../../users/users.service';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

@Injectable()
export class CertListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    protected readonly configService: ConfigService,
  ) {
    super(usersService, mailService);
  }

  async callCustomerIo(data: Record<string, any>) {
    data.variables = {
      cioAppDomain: this.configService.get('CIO_APP_DOMAIN'),
      cioBearer: this.configService.get('CIO_BEARER'),
      cioTrackDomain: this.configService.get('CIO_TRACK_DOMAIN'),
      cioSiteId: this.configService.get('CIO_SITE_ID'),
      cioApiKey: this.configService.get('CIO_API_KEY'),
      lmsApiUrl: this.configService.get('LMS_API_URl'),
      lmsInterappToken: this.configService.get('LMS_INTERAPP_TOKEN'),
    };
    return axios({
      url: this.configService.get('CIO_TRIGGER_CERT_URL'),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: JSON.stringify(data),
    });
  }

  @OnEvent(TriggerType.webinarCertCreated, { async: true })
  async webinarCertCreated(event: WebinarCertCreated) {
    const user = await this.getUserById(event.studentId);
    this.callCustomerIo({
      lms_user: user,
      course_cert: {
        data: {
          webinarName: event.webinarName,
          certLink: event.certLink,
          student: user.name.trim().split(' ')[0],
        },
        name: 'webinar_certificate_sent',
      },
    });
    if (event.sendEmail) {
      return await this.mailService.addEmailToQueue(
        user,
        TriggerType.webinarCertCreated,
        {
          webinarName: event.webinarName,
          certLink: event.certLink,
          student: user.name.trim().split(' ')[0],
        },
        {
          priority: TriggerPriority.low,
        },
      );
    }
  }

  @OnEvent(TriggerType.courseCertCreated, { async: true })
  async courseCertCreated(event: CourseCertCreated) {
    const user = await this.getUserById(event.studentId);
    this.callCustomerIo({
      lms_user: user,
      course_cert: {
        data: {
          courseName: event.courseName,
          certLink: event.certLink,
          student: user.name.trim().split(' ')[0],
        },
        name: 'course_certificate_sent',
      },
    });
    if (event.sendEmail) {
      const user = await this.getUserById(event.studentId);
      return await this.mailService.addEmailToQueue(
        user,
        TriggerType.courseCertCreated,
        {
          courseName: event.courseName,
          certLink: event.certLink,
          student: user.name.trim().split(' ')[0],
        },
        {
          priority: TriggerPriority.low,
        },
      );
    }
  }
}
