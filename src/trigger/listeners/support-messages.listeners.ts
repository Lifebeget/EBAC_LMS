import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { BaseListeners } from './_base.listeners';
import { UsersService } from 'src/users/users.service';
import { MailService } from 'src/mail/mail.service';
import { AmoService } from 'src/amo/amo.service';
import { OnboardingCallEvent } from '../events/enrollment-created.event';
import { AmoEventsService } from '../../amo/amo-events.service';

@Injectable()
export class SupportMessagesListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    private readonly amoService: AmoService,
    private readonly amoEventsService: AmoEventsService,
  ) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.supportMessageSent, { async: true, promisify: true })
  async onSupportMessageSent(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.processSupportMessage(event.amoData);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }
}
