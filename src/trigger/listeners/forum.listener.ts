import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import axios from 'axios';
import { ForumMessageSentEvent } from '../events/forumMessageSent.event';
import { BaseListeners } from './_base.listeners';
import config from 'config';

@Injectable()
export class ForumListener extends BaseListeners {
  readonly targetMap = {
    'lms-production': process.env.LAMBDA_NEGATIVE_FORUM_MSG_URL,
    'lms-production-mx': process.env.LAMBDA_NEGATIVE_FORUM_MSG_MX_URL,
  };
  @OnEvent(TriggerType.forumMessageSent, { async: true })
  async forumMessageSent(event: ForumMessageSentEvent) {
    if (process.env.ENVIRONMENT in this.targetMap && !event.skipResolve) {
      try {
        const threadId = event.threadParentId || event.id;
        let courseTitle = event.course?.title;
        if (courseTitle && event.course.version > 1) courseTitle += ' v' + event.course.version;
        await axios.post(
          this.targetMap[process.env.ENVIRONMENT],
          {
            threadId,
            courseId: event.courseId,
            courseTitle,
            lectureTitle: event.lecture?.title,
            userId: event.userId,
            userName: event.user.name,
            tutorLmsUrl: config.setUser(event.user).get('TUTOR_LMS_URL'),
            studentLmsUrl: config.setUser(event.user).get('STUDENT_LMS_URL'),
            message: event.body,
          },
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: process.env.LAMBDA_NEGATIVE_FORUM_MSG_AUTH,
            },
            timeout: 7000,
          },
        );
      } catch {}
    }
  }
}
