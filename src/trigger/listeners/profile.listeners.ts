import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { BaseListeners } from './_base.listeners';
import axios from 'axios';
import { ProfileUpdatedEvent } from '../events/profile-updated.event';

@Injectable()
export class ProfileListeners extends BaseListeners {
  @OnEvent(TriggerType.userProfileUpdated, { async: true })
  async onUserProfileUpdate(event: Required<ProfileUpdatedEvent>) {
    await axios.post(process.env.LAMBDA_UPDATE_CIO_USER_ON_PROFILE_UPDATE, {
      lmsDomain: process.env.COOKIE_DOMAIN,
      userId: event.userId,
      profile: event.profile,
    });
  }
}
