import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { LastModuleSurveyEvent, MidCourseSurveyEvent } from '../events/survey.event';
import { BaseListeners } from './_base.listeners';

const getSurvey = (event: MidCourseSurveyEvent | LastModuleSurveyEvent, triggerType: TriggerType) =>
  triggerType === TriggerType.midCourseSurvey
    ? ['mid_course_survey', (event as MidCourseSurveyEvent).midCourseSurvey]
    : ['last_module_survey', (event as LastModuleSurveyEvent).lastModuleSurvey];

export class SurveyListeners extends BaseListeners {
  @OnEvent(TriggerType.midCourseSurvey, { async: true })
  onMidCourseSurvey(event: MidCourseSurveyEvent) {
    this.commonSurvey(event, TriggerType.midCourseSurvey);
  }

  @OnEvent(TriggerType.lastModuleSurvey, { async: true })
  onLastModuleSurvey(event: MidCourseSurveyEvent) {
    this.commonSurvey(event, TriggerType.lastModuleSurvey);
  }

  async commonSurvey(event: MidCourseSurveyEvent | LastModuleSurveyEvent, triggerType: TriggerType) {
    const user = await this.getUserById(event.studentId);
    const [surveyKey, surveyValue] = getSurvey(event, triggerType);

    this.mailService.addEmailToQueue(
      user,
      triggerType,
      {
        course: event.courseName,
        course_link: event.courseLink,
        student: user.name,
        support_link: event.supportLink,
        [surveyKey]: surveyValue,
      },
      {},
    );
  }
}
