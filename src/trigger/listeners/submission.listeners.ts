import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { MailService } from 'src/mail/mail.service';
import { UsersService } from 'src/users/users.service';
import { SubmissionGradedEvent } from '../events/submission-graded.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class SubmissionListeners extends BaseListeners {
  constructor(protected readonly usersService: UsersService, protected readonly mailService: MailService) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.submissionGradedSuccess, { async: true })
  onSubmissionGradedSuccess(event: SubmissionGradedEvent) {
    this.onSubmissionGraded(event, TriggerType.submissionGradedSuccess);
  }

  @OnEvent(TriggerType.submissionGradedFailed, { async: true })
  onSubmissionGradedFail(event: SubmissionGradedEvent) {
    this.onSubmissionGraded(event, TriggerType.submissionGradedFailed);
  }

  @OnEvent(TriggerType.submissionGradedQuestion, { async: true })
  onSubmissionGradedQuestion(event: SubmissionGradedEvent) {
    this.onSubmissionGraded(event, TriggerType.submissionGradedQuestion);
  }

  async onSubmissionGraded(event: SubmissionGradedEvent, type: TriggerType) {
    const user = await this.getUserById(event.studentId);

    this.mailService.addEmailToQueue(
      user,
      type,
      {
        student: user.name.trim().split(' ')[0],
        course: event.course,
        activity_score: event.activityScore,
        graded_activity_link: event.gradedActivityLink,
      },
      {},
    );
  }
}
