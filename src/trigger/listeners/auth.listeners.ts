import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { BaseListeners } from './_base.listeners';
import { MailService } from 'src/mail/mail.service';
import { UsersService } from 'src/users/users.service';
import { ForgotPasswordEvent, TutorWelcomeEvent } from '../events/auth.event';
import axios from 'axios';
import config from 'config';

@Injectable()
export class AuthListeners extends BaseListeners {
  constructor(protected readonly usersService: UsersService, protected readonly mailService: MailService) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.forgotPassword, { async: true })
  async onForgotPassword(event: ForgotPasswordEvent) {
    const user = await this.getUserById(event.studentId);
    await this.mailService.addEmailToQueue(
      user,
      TriggerType.forgotPassword,
      {
        student: user.name.trim().split(' ')[0],
        reset_password_link: event.resetPasswordLink,
      },
      {},
    );
  }

  @OnEvent(TriggerType.tutorWelcome, { async: true })
  async onTutorWelcome(event: TutorWelcomeEvent) {
    const user = await this.getUserById(event.studentId);
    const payload = {
      email: user.email,
      name: user.name,
      password: event.password,
      url: config.setUser(user).get('TUTOR_LMS_URL'),
    };
    await this.mailService.addEmailToQueue(user, TriggerType.tutorWelcome, payload, {});
  }

  // TODO remove when reactivation program is over or
  // consider further use cases
  @OnEvent(TriggerType.reactivateMoveLead, { async: true })
  async reactivateMoveLead({ user_id }: { user_id: string }) {
    if (process.env.ENVIRONMENT !== 'lms-production') return;
    const logger = new Logger(AuthListeners.name);
    for (let i = 0; i < 5; i++) {
      try {
        const { data } = await axios.post(
          'https://9esvsewqrl.execute-api.sa-east-1.amazonaws.com/Prod/move',
          {
            student_id: user_id,
          },
          {
            headers: { 'X-Custom-Auth': 'X!nzDfH&HDD2*h%j' },
            timeout: 60000,
          },
        );
        logger.log(`'Reactivation program for ${user_id}' attempt (${i + 1}) succeeded with: ${JSON.stringify(data)}`);
        break;
      } catch (err) {
        const message = err.response?.data || err.message || "'unknown'";
        const statusCode = err.response?.status || "'status unknown'";
        logger.error(`'Reactivation program for ${user_id}' attempt (${i + 1}) failed with: ${message}, status: ${statusCode}`);
        if (statusCode == 404) break;
        await new Promise(res => setTimeout(res, 5000));
      }
    }
  }
}
