import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { BaseListeners } from './_base.listeners';
import { UsersService } from 'src/users/users.service';
import { MailService } from 'src/mail/mail.service';
import { EnrollmentTriggerData, OnboardingCallEvent } from '../events/enrollment-created.event';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import axios from 'axios';
import { ignoredCourseTitles, paidEndrollmentTypes } from '../services/paid-enrollment-types';
import * as dayjs from 'dayjs';
import { AmoEventsService } from 'src/amo/amo-events.service';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';
import { TriggerService } from '../trigger.service';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { AmoService } from 'src/amo/amo.service';

@Injectable()
export class CioEnrollmentListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    protected readonly amoEventsService: AmoEventsService,
    protected readonly triggerService: TriggerService,
    protected readonly amoService: AmoService,
  ) {
    super(usersService, mailService);
  }

  async cioTriggerEnrollmentHandler(event: EnrollmentTriggerData) {
    if (process.env.CIO_TRIGGER_ENROLLMENTS_URL && ['lms-production'].includes(process.env.ENVIRONMENT)) {
      try {
        await axios({
          url: process.env.CIO_TRIGGER_ENROLLMENTS_URL,
          method: 'POST',
          data: {
            lms_user: event.user,
            lang_code: 'pt-BR',
            cio_event: {
              enrollment: {
                ...event.enrollment,
                course: event.course,
              },
            },
          },
        });
        console.log(`${event.user.id} | CIO_TRIGGER_ENROLLMENTS_URL Activated`);
      } catch (e) {
        console.log(`${event.user.id} | Failed to start step function execution | ${e.stack}`);
      }
    }
  }

  async amoTriggerEnrollmentHandler(event: EnrollmentTriggerData) {
    const enrollment = event.enrollment;
    const user = event.user;
    const course = event.course;
    const profile = event.profile;

    // Not create internal and trial leads in AMO
    if (!paidEndrollmentTypes.includes(enrollment.type)) {
      console.log(`${user.id} | Internal or trial enrollment. Exiting`);
      return;
    }

    // hack for ignoring some courses that are "free" despite the enrollment having type "Buy"
    if (ignoredCourseTitles.includes(course.title)) {
      console.log(`${user.id} | Course is free. Exiting`);
      return;
    }

    //Do not create lead if phone number is empty
    if (!profile?.phone) {
      console.log(`${user.id} | User has no phone number. Exiting`);
      return;
    }

    // Date check
    if (!dayjs(enrollment.dateFrom).isAfter(dayjs().subtract(2, 'days'))) {
      console.log(`${user.id} | Enrollment date is less than 2 days ago`);
      return;
    }

    // Activity check
    if (enrollment.hasOwnProperty('active') && enrollment.active !== true) {
      console.log(`${user.id} | Enrollment inactive or enrollment role is not student`);
      return;
    }

    const amoData = {
      lead: `Onboarding call ${user.name}`,
      name: user.name,
      phone: profile?.phone ? profile.phone : '',
      email: user.email,
      courseName: course?.title || 'Course name not set',
      countMonths: course?.duration || 0,
      countModules: course?.modules || 0,
    };
    console.log(`${user.id} | Data of the lead to create: ${JSON.stringify(amoData)}`);

    try {
      const amoEvent = await this.amoEventsService.create({
        userId: user.id,
        courseId: course.id,
        eventType: AmoUserEventType.courseOnboarding,
        data: amoData,
      });

      const result = await this.triggerService.addTrigger(
        TriggerType.onboardingCallTrigger,
        { studentId: user.id, amoData, eventId: amoEvent.id, enrollmentId: enrollment.id },
        {
          priority: TriggerPriority.normal,
          onceHash: `${user.id}`,
        },
      );

      if (result.skipped) {
        console.log(`${user.id} | User already have onboarding lead`);
      }
    } catch (e) {
      console.error(`${user.id} | Error with AmoCRM lead creating: ${e.stack}`);
    }

    const amoDataNotStarted = { ...amoData };
    amoDataNotStarted.lead = `Not started learning ${user.name}`;

    const amoEvent = await this.amoEventsService.create({
      userId: user.id,
      courseId: course.id,
      eventType: AmoUserEventType.courseNotStarted,
      data: amoDataNotStarted,
    });
    this.triggerService.addTrigger(
      TriggerType.userCourseNotStarted,
      { studentId: user.id, amoData: amoDataNotStarted, eventId: amoEvent.id, enrollmentId: enrollment.id },
      {
        priority: TriggerPriority.normal,
        onceHash: `${user.id}${course.id}`,
        delay: { days: 30 },
      },
    );
  }

  @OnEvent(TriggerType.enrollmentCreated, { async: true })
  async onEnrollmentCreated(event: EnrollmentTriggerData) {
    // This handler is interested only in student enrollments
    if (event.enrollment.role != CourseRole.Student) {
      return;
    }

    console.log(`${event.user.id} | Processing userEnrollCreate`);
    console.log(`${event.user.id} | Enrollment info: ${JSON.stringify(event.enrollment)}`);

    await this.cioTriggerEnrollmentHandler(event);
    await this.amoTriggerEnrollmentHandler(event);
  }

  @OnEvent(TriggerType.onboardingCallTrigger, { async: true })
  async OnboardingCallTrigger(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.createOnboardCallLead(event);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.userCourseNotStarted, { async: true, promisify: true })
  async onUserCourseNotStarted(event: Required<OnboardingCallEvent>) {
    // hack for ignoring some courses that are "free" despite the enrollment having type "Buy"
    if (ignoredCourseTitles.includes(event.amoData.courseName)) {
      return;
    }
    const result = await this.amoService.createCourseNotStartedCallLead(event);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }
}
