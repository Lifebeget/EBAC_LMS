import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { CourseEndWithoutSertificateEvent, CourseEndWithCertificateEvent } from '../events/course.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class CourseListeners extends BaseListeners {
  @OnEvent(TriggerType.endCourseWithCertificate, { async: true })
  onCourseEndWithCertificate(event: CourseEndWithCertificateEvent) {
    this.commonCourseEnd(event, TriggerType.endCourseWithCertificate);
  }

  @OnEvent(TriggerType.endCourseWithoutCertificate, { async: true })
  onCourseEndWithoutCertificate(event: CourseEndWithCertificateEvent) {
    this.commonCourseEnd(event, TriggerType.endCourseWithoutCertificate);
  }

  async commonCourseEnd(event: CourseEndWithoutSertificateEvent | CourseEndWithCertificateEvent, triggerType: TriggerType) {
    const user = await this.getUserById(event.studentId);
    this.mailService.addEmailToQueue(
      user,
      triggerType,
      {
        course: event.courseName,
        course_link: event.courseLink,
        student: user.name,
        support_link: event.supportLink,
        on: event.platformName,
        ...(triggerType === TriggerType.endCourseWithCertificate && {
          certificate_link: (event as CourseEndWithCertificateEvent).certificateLink,
          add_to_linkedin_profile: (event as CourseEndWithCertificateEvent).addToLinkedIn,
        }),
      },
      {},
    );
  }
}
