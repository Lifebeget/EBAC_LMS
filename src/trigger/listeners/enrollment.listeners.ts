import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { BaseListeners } from './_base.listeners';
import { UsersService } from 'src/users/users.service';
import { MailService } from 'src/mail/mail.service';
import { AmoService } from 'src/amo/amo.service';
import {
  EnrollmentCreatedGivePresentEvent,
  EnrollmentTriggerData,
  OnboardingCallEvent,
  UserEnrollmentCheckPresentEvent,
} from '../events/enrollment-created.event';
import { AmoEventsService } from '../../amo/amo-events.service';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { CreateEnrollmentDto } from 'src/courses/dto/create-enrollment.dto';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { CoursesService } from 'src/courses/courses.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { EnrollmentTriggerService } from '../services/enrollment-trigger.service';
import * as dayjs from 'dayjs';
import config from 'config';

@Injectable()
export class EnrollmentListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    protected readonly coursesService: CoursesService,
    protected readonly enrollmentService: EnrollmentsService,
    private readonly amoService: AmoService,
    private readonly amoEventsService: AmoEventsService,
    private readonly enrollmentTriggerService: EnrollmentTriggerService,
  ) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.enrollmentCreated, { async: true })
  async onEnrollmentCreated(event: EnrollmentTriggerData) {
    const user = event.user;
    const enrollments = event.enrollments;

    if (event.sendEmail) {
      // Welcome Emails
      this.enrollmentTriggerService.userEnrolledWelcomeEmail(event);

      // Promo Emails
      if (this.enrollmentTriggerService.checkEnglishFirstCriteria(enrollments)) {
        this.enrollmentTriggerService.englishFirst(user);
      }
    }
  }

  @OnEvent(TriggerType.userEnrollmentUpdated, { async: true })
  async onUserEnrollmentUpdated(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.removeOnboardCallLead(event.amoData);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.userEnrollmentDeleted, { async: true })
  async onUserEnrollmentDeleted(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.removeOnboardCallLead(event.amoData);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.userEnrollmentCreatedGivePresent, { async: true })
  async onUserEnrollmentCreatedGivePresent(event: Required<EnrollmentCreatedGivePresentEvent>) {
    const baseEnrollment = event.enrollment;
    const userId = baseEnrollment.userId || baseEnrollment.user?.id;

    if (!userId) {
      return console.error('ERRORS.TRIGGER.userEnrollmentCreatedGivePresent.CANT_FIND_USER');
    }

    // If add new enrollment for student - give him all presents course
    if (baseEnrollment.role === CourseRole.Student) {
      // get all appendage courses
      const courses = await this.coursesService.getAutoEnrollCourses();
      // current user enrollemnts
      const currentUserEnrollments = await this.enrollmentService.findAll(userId);
      // we need only course ids
      const currentUserEnrollmentsCourseIds = currentUserEnrollments.map(en => en.courseId);

      //active all presents
      courses
        .filter(c => currentUserEnrollmentsCourseIds.includes(c.id))
        .forEach(c => {
          const en = currentUserEnrollments.filter(en => en.courseId === c.id)[0];
          this.enrollmentService.rawSave({ ...en, active: true });
        });

      // filter appendage courses, we dont need twins presents
      courses
        .filter(c => !currentUserEnrollmentsCourseIds.includes(c.id))
        .forEach(async c => {
          const AppendageEnrollement: CreateEnrollmentDto = {
            userId,
            role: CourseRole.Student,
            type: EnrollmentType.Present,
            sendEmail: false,
            active: true,
            serviceEndsAt: dayjs().add(config.defaultCourseSupportDuration, 'day').toDate(),
            rights: {
              canWatchFreeLessons: true,
              canWatchAllLessons: true,
              canDownloadFiles: true,
              canReadForum: true,
              canWriteForum: true,
              canSubmitQuiz: true,
              canSubmitSurvey: true,
              canSubmitAssignment: true,
              canGraduate: true,
            },
            reason: {
              message: 'Gift.',
              authorId: null,
            },
          };

          //save
          await this.enrollmentService.rawSave({
            ...AppendageEnrollement,
            course: { id: c.id },
            user: { id: userId },
          });
        });
    }
  }

  @OnEvent(TriggerType.userEnrollmentCheckPresent, { async: true })
  async onUserEnrollmentCheckPresent(event: Required<UserEnrollmentCheckPresentEvent>) {
    const baseEnrollment = event.enrollment;
    const userId = baseEnrollment.userId || baseEnrollment.user?.id;

    if (!userId) {
      return console.error('ERRORS.TRIGGER.userEnrollmentCreatedGivePresent.CANT_FIND_USER');
    }

    //get all users enrollments
    const userEnrollments = await this.enrollmentService.findAll(userId);
    const countOfNotPresents = userEnrollments.filter(en => {
      const enrollmentsValidForPresent = en.course.isAutoEnroll !== true && en.type !== EnrollmentType.Trial && en.active === true;
      return enrollmentsValidForPresent;
    }).length;

    //disable all presents
    if (countOfNotPresents === 0) {
      userEnrollments
        .filter(en => en.course.isAutoEnroll === true)
        .forEach(en => {
          this.enrollmentService.update(en.id, { active: false });
        });
      //else enable all previous presents
    } else {
      userEnrollments
        .filter(en => en.course.isAutoEnroll === true)
        .forEach(en => {
          this.enrollmentService.update(en.id, { active: true });
        });
    }
  }
}
