import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { ModuleFinishedEvent, TrialModuleFinishedEvent } from '../events/module-finished.event';
import { BaseListeners } from './_base.listeners';

export class ModuleCompletedListeners extends BaseListeners {
  @OnEvent(TriggerType.firstModuleFinished, { async: true })
  onFirstModuleFinished(event: Required<ModuleFinishedEvent>) {
    this.commonModuleFinished(event, TriggerType.firstModuleFinished);
  }

  @OnEvent(TriggerType.lastModuleFinished, { async: true })
  onLastModuleFinished(event: Omit<ModuleFinishedEvent, 'pendingActivityLink'>) {
    this.commonModuleFinished(event, TriggerType.lastModuleFinished);
  }

  @OnEvent(TriggerType.trialModuleFinished, { async: true })
  onTrialModuleFinished(event: TrialModuleFinishedEvent) {
    this.commonModuleFinished(event, TriggerType.trialModuleFinished);
  }

  async commonModuleFinished(event: ModuleFinishedEvent | TrialModuleFinishedEvent, triggerType: TriggerType) {
    const user = await this.getUserById(event.studentId);

    const payload = {
      course: event.courseName,
      course_link: event.courseLink,
      student: user.name,
      support_link: event.supportLink,
      ...(triggerType === TriggerType.firstModuleFinished && {
        pending_activity_link: (event as ModuleFinishedEvent).pendingActivityLink,
      }),
      ...(triggerType === TriggerType.trialModuleFinished && {
        payment_link: (event as TrialModuleFinishedEvent).paymentLink,
      }),
    };

    this.mailService.addEmailToQueue(user, triggerType, payload, {});
  }
}
