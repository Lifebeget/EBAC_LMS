import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { TaskFeedbackEvent } from '../events/task-feedback.event';
import { BaseListeners } from './_base.listeners';

export class FeedbackListeners extends BaseListeners {
  @OnEvent(TriggerType.taskFeedback, { async: true })
  onTaskFeedback(event: Required<TaskFeedbackEvent>) {
    this.commonOnTaskFeedback(event, TriggerType.taskFeedback);
  }

  @OnEvent(TriggerType.taskFeedbackWithCorrection, { async: true })
  onTaskFeedbackWithCorrection(event: Omit<TaskFeedbackEvent, 'activityScore'>) {
    this.commonOnTaskFeedback(event, TriggerType.taskFeedbackWithCorrection);
  }

  private async commonOnTaskFeedback(event: TaskFeedbackEvent, triggerType: TriggerType) {
    const user = await this.getUserById(event.studentId);
    const payload = {
      course: event.courseName,
      course_link: event.courseLink,
      student: event.courseLink,
      support_link: event.supportLink,
      graded_activity_link: event.gradedActivityLink,
      activity_score: event.activityScore,
    };

    if (payload.activity_score !== undefined) {
      delete payload.activity_score;
    }

    this.mailService.addEmailToQueue(user, triggerType, payload, {});
  }
}
