import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { ModulePublishedEvent } from '../events/module-published.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class ModulePublishedListeners extends BaseListeners {
  @OnEvent(TriggerType.modulePublished, { async: true })
  async onModulePublished(event: ModulePublishedEvent) {
    if (event.sendEmail) {
      const user = await this.getUserById(event.studentId);
      this.mailService.addEmailToQueue(
        user,
        TriggerType.modulePublished,
        {
          course: event.courseName,
          course_link: event.courseLink,
          student: user.name,
        },
        {},
      );
    }
  }
}
