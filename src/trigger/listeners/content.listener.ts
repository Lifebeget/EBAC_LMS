import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { ContentService } from 'src/courses/content.service';
import { MailService } from 'src/mail/mail.service';
import { UsersService } from 'src/users/users.service';
import { ContentKalturaCreatedEvent } from '../events/content.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class ContentListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    readonly contentService: ContentService,
  ) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.contentKalturaUpdated, { async: true })
  onContentKalturaUpdated(event: ContentKalturaCreatedEvent) {
    this.contentService.validateKalturaContent(event.content, event.inform);
  }
}
