import { TriggerType } from '@enums/trigger-type.enum';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable, Logger } from '@nestjs/common';
import { BaseListeners } from './_base.listeners';
import { UsersService } from 'src/users/users.service';
import { MailService } from 'src/mail/mail.service';
import { AmoService } from 'src/amo/amo.service';
import { OnboardingCallEvent } from '../events/enrollment-created.event';
import { AmoEventsService } from 'src/amo/amo-events.service';
import { LectureProgressEvent } from '../events/lecture-progress.event';
import { NpsService } from 'src/nps/nps.service';
import { strict } from 'assert';
import { TriggerService } from '../trigger.service';
import axios from 'axios';
import { User } from '@entities/user.entity';
import { Course } from '@entities/course.entity';

@Injectable()
export class LectureProgressListeners extends BaseListeners {
  private readonly logger = new Logger(LectureProgressListeners.name);

  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    readonly amoEventsService: AmoEventsService,
    private readonly amoService: AmoService,
    private readonly npsService: NpsService,
    private readonly triggerService: TriggerService,
  ) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.userProgressChanged, { async: true })
  async onUserProgressChangedCheckNps(event: Required<LectureProgressEvent>) {
    this.npsService.checkAndCreate(event.userId, event.courseId);
  }

  @OnEvent(TriggerType.lectureProgressUpdated, { async: true })
  async onLectureProgressUpdated(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.successOnboardCallLead(event.amoData);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.moveTrialLead, { async: true })
  async onMoveTrialLead(event: { user: User; course: Course }) {
    await axios.post(process.env.LAMBDA_MOVE_LEAD_TO_TRIAL_ACCESS_GRANTED_BR_URL, {
      lmsDomain: process.env.COOKIE_DOMAIN,
      user: {
        email: event.user.email,
      },
      course: {
        cmsId: event.course.cmsId,
      },
    });
  }

  @OnEvent(TriggerType.userCourseAlmostCompleted, { async: true })
  async onUserCourseAlmostCompleted(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.createAlmostCompletedCallLead(event);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.userCourseCompleted, { async: true })
  async onUserCourseCompleted(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.createCompletedCallLead(event);
    if (event.eventId) {
      // find amo user event
      const amoUserEvent = await this.amoEventsService.findById(event.eventId);
      strict.ok(amoUserEvent);
      const { userId, courseId } = amoUserEvent;
      strict.ok(userId);
      strict.ok(courseId);

      // cancel delayed "stopped learning" trigger
      const stoppedLearningOnceHash = `${userId}${courseId}`;
      this.logger.log(`Cancelling trigger ${stoppedLearningOnceHash} (${TriggerType.userStoppedLearning})`);
      await this.triggerService.cancelTrigger({
        triggerType: TriggerType.userStoppedLearning,
        onceHash: stoppedLearningOnceHash,
      });

      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }

  @OnEvent(TriggerType.userStoppedLearning, { async: true, promisify: true })
  async onUserStoppedLearning(event: Required<OnboardingCallEvent>) {
    const result = await this.amoService.createStopLearningCallLead(event);
    if (event.eventId) {
      const { status, ...responseData } = result;
      await this.amoEventsService.update(event.eventId, {
        sendTime: new Date(),
        success: status === 'success',
        responseData,
      });
    }
  }
}
