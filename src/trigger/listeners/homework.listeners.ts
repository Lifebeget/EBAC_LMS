import { TriggerType } from '@enums/trigger-type.enum';
import { SubmissionStatus } from '@lib/types/enums';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { MailService } from 'src/mail/mail.service';
import { UsersService } from 'src/users/users.service';
import { HomeworkReminderEvent } from '../events/homework.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class HomeworkListeners extends BaseListeners {
  constructor(
    protected readonly usersService: UsersService,
    protected readonly mailService: MailService,
    private readonly submissionsService: SubmissionsService,
  ) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.homeworkReminder, { async: true })
  async onHomeworkReminder(event: HomeworkReminderEvent) {
    const submissions = await this.submissionsService.findForAssessmentAndUser(event.assessmentId, event.studentId);

    if (submissions.every(submission => submission.status === SubmissionStatus.Graded)) {
      return;
    }

    const student = await this.getUserById(event.studentId);
    this.mailService.addEmailToQueue(
      student,
      TriggerType.homeworkReminder,
      {
        course: event.courseName,
        course_link: event.courseLink,
        student: student.name,
        support_link: event.supportLink,
        current_module: event.currentModule,
        pending_activity_link: event.pendingActivityLink,
      },
      {},
    );
  }
}
