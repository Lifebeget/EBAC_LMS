import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { MailService } from 'src/mail/mail.service';
import { UsersService } from 'src/users/users.service';
import { StudentCreatedEvent } from '../events/student-created.event';
import { BaseListeners } from './_base.listeners';

@Injectable()
export class WelcomeListeners extends BaseListeners {
  constructor(protected readonly usersService: UsersService, protected readonly mailService: MailService) {
    super(usersService, mailService);
  }

  @OnEvent(TriggerType.registration, { async: true })
  async onStudentCreated(event: StudentCreatedEvent) {
    if (event.sendEmail) {
      const user = await this.getUserById(event.studentId);
      this.mailService.addEmailToQueue(
        user,
        TriggerType.registration,
        {
          student: user.name.trim().split(' ')[0],
          login: event.login,
          password: event.password,
        },
        {},
      );
    }
  }
}
