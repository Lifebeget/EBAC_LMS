import { Trigger } from '@entities/trigger.entity';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { LessThanOrEqual, Repository } from 'typeorm';
import { BaseEvent } from './events/_base.event';
import * as dayjs from 'dayjs';
import { DurationUnitsObjectType } from 'dayjs/plugin/duration';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { KAFKA_ENABLED, KAFKA_PREFIX } from 'src/helpers';
import { KafkaClientService } from 'src/kafka/kafka-client.service';

@Injectable()
export class TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {}

  skippedTrigger(trigger: Trigger) {
    return {
      ...trigger,
      skipped: true,
    } as Trigger;
  }

  sendToEventBus(trigger: Trigger) {
    if (KAFKA_ENABLED && this.kafkaClient) {
      this.kafkaClient.emit(`log.triggers.0`, JSON.stringify(trigger));
    }
  }

  async addTrigger(
    triggerType: TriggerType,
    payload: Record<string, any> & Partial<BaseEvent>,
    options?: {
      delay?: DurationUnitsObjectType;
      onceHash?: string;
      refresh?: boolean;
      priority?: TriggerPriority | null;
    },
  ) {
    let trigger: Trigger;
    if (options.onceHash) {
      trigger = await this.triggerRepository
        .createQueryBuilder('trigger')
        .where('once_hash = :onceHash', {
          // Investigate. Why there is no triggerType condition but triggertype is in the data?
          onceHash: options.onceHash,
          triggerType,
        })
        .getOne();
    }

    if (trigger) {
      if (options?.refresh && !trigger.executed) {
        if (options?.delay) {
          trigger.dateToExecute = dayjs().add(dayjs.duration(options.delay)).toDate();
        }
        const updatedTrigger = await this.triggerRepository.save(trigger);
        return this.skippedTrigger(updatedTrigger);
      }
      return this.skippedTrigger(trigger);
    } else {
      const newTrigger = this.triggerRepository.create({
        user: payload.studentId ? { id: payload.studentId } : null,
        triggerType,
        data: payload,
        onceHash: options?.onceHash || null,
        priority: options?.priority,
        dateToExecute: null,
      });

      //adding priority
      if (newTrigger.priority === TriggerPriority.instantly) {
        newTrigger.dateToExecute = new Date();
        newTrigger.executed = true;
        this.emitTrigger(newTrigger);
        newTrigger.executed = true;
        this.sendToEventBus(newTrigger);
      } else if (options.delay) {
        newTrigger.dateToExecute = dayjs().add(dayjs.duration(options.delay)).toDate();
      } else {
        newTrigger.dateToExecute = new Date();
        newTrigger.executed = true;
        this.emitTrigger(newTrigger);
        newTrigger.executed = true;
        this.sendToEventBus(newTrigger);
      }
      return await this.triggerRepository.save(newTrigger);
    }
  }

  async removeTrigger(removeData: { id?: string; onceHash?: string; triggerType?: TriggerType }) {
    if (!removeData.id && !removeData.onceHash) {
      return;
    }
    if (removeData.id) {
      return this.triggerRepository.delete(removeData.id);
    }
    if (removeData.onceHash && removeData.triggerType) {
      return this.triggerRepository.delete({
        onceHash: removeData.onceHash,
        triggerType: removeData.triggerType,
      });
    }
  }

  async cancelTrigger(removeData: { id?: string; onceHash?: string; triggerType?: TriggerType }) {
    if (!removeData.id && !removeData.onceHash) {
      return;
    }
    if (removeData.id) {
      const trigger = await this.triggerRepository
        .createQueryBuilder('trigger')
        .where('id = :id', { id: removeData.id })
        .andWhere('canceled = false')
        .getOne();
      if (trigger) {
        trigger.canceled = true;
        return this.triggerRepository.save(trigger);
      }
    }
    if (removeData.onceHash && removeData.triggerType) {
      const trigger = await this.triggerRepository
        .createQueryBuilder('trigger')
        .where('once_hash = :onceHash', { onceHash: removeData.onceHash })
        .andWhere('trigger_type = :triggerType', {
          triggerType: removeData.triggerType,
        })
        .andWhere('canceled = false')
        .getOne();
      if (trigger) {
        trigger.canceled = true;
        return this.triggerRepository.save(trigger);
      }
    }
  }

  async executeTriggers(count = 50) {
    const triggers = await this.triggerRepository.find({
      where: {
        executed: false,
        canceled: false,
        dateToExecute: LessThanOrEqual(new Date()),
      },
      order: {
        priority: 'ASC',
      },
      relations: ['user'],
      take: count,
    });

    for (const trigger of triggers) {
      try {
        await this.asyncEmitTrigger(trigger);
        trigger.executed = true;
        await this.triggerRepository.save(trigger);
      } catch (e) {
        console.error('Trigger execution error', trigger);
        console.error(e);
      } finally {
        this.sendToEventBus(trigger);
      }
    }
  }

  private emitTrigger(trigger: Trigger) {
    this.eventEmitter.emit(trigger.triggerType, trigger.data);
  }

  private async asyncEmitTrigger(trigger: Trigger) {
    await this.eventEmitter.emitAsync(trigger.triggerType, trigger.data);
  }
}
