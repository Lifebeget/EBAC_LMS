import { BaseEvent } from './_base.event';

export interface SubmissionGradedEvent extends BaseEvent {
  course: string;
  activityScore: number;
  gradedActivityLink: string;
}
