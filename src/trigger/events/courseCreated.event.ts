import { BaseEvent } from './_base.event';

export interface CourseCreatedEvent extends BaseEvent {
  student: string;
  course: string;
  courseMonths: string | number;
  courseModules: string | number;
  courseDate: string;
  zeroModule: string;
  newSurveyLink: string;
  sendEmail: boolean;
}
