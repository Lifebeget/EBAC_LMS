import { Content } from '@entities/content.entity';
import { BaseEvent } from './_base.event';

export interface ContentKalturaCreatedEvent extends BaseEvent {
  content: Content;
  inform: boolean;
}
