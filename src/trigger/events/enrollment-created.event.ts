import { User } from '@entities/user.entity';
import { CourseStatus } from '@enums/course-status.enum';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { CreateEnrollmentDto } from 'src/courses/dto/create-enrollment.dto';
import { BaseEvent } from './_base.event';

export interface EnrollmentTriggerData extends BaseEvent {
  enrollment: {
    id: string;
    type: EnrollmentType;
    role: CourseRole;
    active: boolean;
    dateFrom: Date | null;
    dateTo: Date | null;
    reason: Record<string, any>;
    options: Record<string, any> | null;
  };
  user: Pick<User, 'id' | 'name' | 'email' | 'domain'>;
  profile: {
    phone: string | null;
  };
  course: {
    id: string;
    title: string;
    version: number;
    cmsId: string;
    duration: number;
    modules: number;
    startAt: Date;
    status: CourseStatus;
  };
  links: {
    course: string;
    survey: string;
  };
  enrollments: {
    id: string;
    courseId: string;
    type: EnrollmentType;
    role: CourseRole;
    active: boolean;
    courseNotForSale: boolean;
  }[];
  studentId: string;
  sendEmail?: boolean;
}

export interface OnboardingCallEvent extends BaseEvent {
  studentId: string;
  eventId: string;
  enrollmentId?: string;
  amoData: {
    lead: string;
    name?: string;
    phone?: string;
    email?: string;
    courseName: string;
    countMonths?: string;
    countModules?: string;
  };
}

export interface extendedCreateEnrollmentDto extends CreateEnrollmentDto {
  user?: User;
}

export interface EnrollmentCreatedGivePresentEvent extends BaseEvent {
  studentId: string;
  courseId: string;
  enrollment: extendedCreateEnrollmentDto;
}

export interface UserEnrollmentCheckPresentEvent extends BaseEvent {
  studentId: string;
  courseId: string;
  enrollment: extendedCreateEnrollmentDto;
}
