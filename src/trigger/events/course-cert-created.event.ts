import { BaseEvent } from './_base.event';

export interface CourseCertCreated extends BaseEvent {
  courseName: string;
  certLink: string;
  sendEmail: boolean;
}
