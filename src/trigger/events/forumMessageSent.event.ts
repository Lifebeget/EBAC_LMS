import { ForumMessages } from '@entities/forum-messages.entity';
import { BaseEvent } from './_base.event';

export interface ForumMessageSentEvent extends ForumMessages, BaseEvent {}
