import { BaseEvent } from './_base.event';

export interface ModuleFinishedEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  pendingActivityLink?: string;
}

export interface TrialModuleFinishedEvent extends Omit<ModuleFinishedEvent, 'pendingActivityLink'> {
  paymentLink: string;
}
