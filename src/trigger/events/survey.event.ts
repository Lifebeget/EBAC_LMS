import { BaseEvent } from './_base.event';

interface SurveyEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
}

export interface MidCourseSurveyEvent extends SurveyEvent {
  midCourseSurvey: string;
}

export interface LastModuleSurveyEvent extends SurveyEvent {
  lastModuleSurvey: string;
}
