import { BaseEvent } from './_base.event';

export interface StudentCreatedEvent extends Omit<BaseEvent, 'supportLink'> {
  student: string;
  login: string;
  password: string;
  sendEmail?: boolean;
}
