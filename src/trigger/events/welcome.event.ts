import { BaseEvent } from './_base.event';

export interface WelcomeEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  supportLink: string;
  courseDuration: string | number;
  courseModulesQuantity: string | number;
  courseStartDate: string;
  zeroModuleLink: string;
  newSurveyLink: string;
}
