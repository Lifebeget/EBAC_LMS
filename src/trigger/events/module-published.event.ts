import { BaseEvent } from './_base.event';

export interface ModulePublishedEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  sendEmail?: boolean;
}
