import { BaseEvent } from './_base.event';

export interface CourseEndWithoutSertificateEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  platformName: string;
}

export interface CourseEndWithCertificateEvent extends CourseEndWithoutSertificateEvent {
  certificateLink: string;
  addToLinkedIn: string;
}
