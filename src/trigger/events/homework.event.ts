import { BaseEvent } from './_base.event';

export interface HomeworkReminderEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  currentModule: number;
  pendingActivityLink: string;
  assessmentId: string;
}
