import { BaseEvent } from './_base.event';

export interface CourseUncompletedEvent extends BaseEvent {
  student: string;
  course: string;
  courseMonths: string | number;
  courseModules: string | number;
  courseLink: string;
  newSurveyLink: string;
  sendEmail: boolean;
}
