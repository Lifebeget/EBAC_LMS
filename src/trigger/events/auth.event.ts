import { BaseEvent } from './_base.event';

export interface ForgotPasswordEvent extends BaseEvent {
  resetPasswordLink: string;
}

export interface TutorWelcomeEvent extends BaseEvent {
  password: string;
}
