import { LectureStatus } from '@enums/lecture-status.enum';
import { BaseEvent } from './_base.event';

export interface LectureProgressEvent extends BaseEvent {
  userId: string;
  courseId: string;
  lectureId: string;
  status: LectureStatus;
}
