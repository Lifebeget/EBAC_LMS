import { BaseEvent } from './_base.event';

export interface TaskFeedbackEvent extends BaseEvent {
  courseName: string;
  courseLink: string;
  gradedActivityLink: string;
  activityScore?: string | number;
}
