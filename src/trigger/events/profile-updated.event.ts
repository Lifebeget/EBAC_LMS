import { BaseEvent } from './_base.event';

export interface ProfileUpdatedEvent extends BaseEvent {
  userId: string;
  profile: Record<string, any>;
}
