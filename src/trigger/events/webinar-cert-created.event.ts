import { BaseEvent } from './_base.event';

export interface WebinarCertCreated extends BaseEvent {
  webinarName: string;
  certLink: string;
  sendEmail: boolean;
}
