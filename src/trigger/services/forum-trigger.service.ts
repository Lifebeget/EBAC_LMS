import { ForumMessages } from '@entities/forum-messages.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { Injectable } from '@nestjs/common';
import { TriggerService } from '../trigger.service';

@Injectable()
export class ForumTriggerService extends TriggerService {
  async messageSent(message: ForumMessages) {
    await this.addTrigger(TriggerType.forumMessageSent, { ...message }, { priority: TriggerPriority.normal });
  }
}
