import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';

/**
 * Enrollment types to trigger paid course events, creating Onboarding/Completed/Almost completed leads
 */
export const paidEndrollmentTypes = [
  //
  EnrollmentType.Buy,
  EnrollmentType.Transfer,
  EnrollmentType.Present,
  EnrollmentType.Promo,
];

/**
 * Titles of the courses that are "free" despite the enrollment having type "Buy"
 * Would NOT trigger paid course events, thus NOT creating Onboarding/Completed/Almost completed leads
 */
export const ignoredCourseTitles = [
  //
  'Introdução à Programação',
  'Plano de Carreira',
];
