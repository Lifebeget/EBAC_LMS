import { Trigger } from '@entities/trigger.entity';
import { User } from '@entities/user.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';

@Injectable()
export class WelcomeTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @InjectRepository(User)
    readonly usersRepository: Repository<User>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async studentCreated(user: Partial<User>, password: string, sendEmail: boolean) {
    await this.addTrigger(
      TriggerType.registration,
      {
        studentId: user.id,
        student: user.name,
        login: user.email,
        password: password,
        sendEmail: sendEmail,
      },
      {
        priority: TriggerPriority.instantly,
      },
    );
  }
}
