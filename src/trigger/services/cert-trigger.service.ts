import { Trigger } from '@entities/trigger.entity';
import { User } from '@entities/user.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { WebinarService } from 'src/webinar/webinar.service';
import { CoursesService } from 'src/courses/courses.service';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import config from 'config';

@Injectable()
export class CertTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @InjectRepository(Trigger)
    readonly usersRepository: Repository<User>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
    private readonly webinarService: WebinarService,
    private readonly coursesService: CoursesService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async webinarCertCreated(webinarId: string, userId: string, certUniqueId: string, sendEmail: boolean) {
    const webinar = await this.webinarService.findOne(webinarId);
    const user = await this.usersRepository.findOne(userId);

    await this.addTrigger(
      TriggerType.webinarCertCreated,
      {
        studentId: userId,
        webinarName: webinar.name,
        certLink: `${config.setUser(user).get('STUDENT_LMS_URL')}/certs/validate/${certUniqueId}`,
        sendEmail: sendEmail,
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }

  async courseCertCreated(courseId: string, userId: string, certUniqueId: string, sendEmail: boolean) {
    const course = await this.coursesService.findOne(courseId);
    const user = await this.usersRepository.findOne(userId);

    await this.addTrigger(
      TriggerType.courseCertCreated,
      {
        studentId: userId,
        courseName: course.title,
        certLink: `${config.setUser(user).get('STUDENT_LMS_URL')}/certs/validate/${certUniqueId}`,
        sendEmail: sendEmail,
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }
}
