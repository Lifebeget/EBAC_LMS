import { Trigger } from '@entities/trigger.entity';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { In, Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { UsersService } from 'src/users/users.service';
import { TriggerType } from '@enums/trigger-type.enum';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { AmoService } from 'src/amo/amo.service';
import { Lecture } from '@entities/lecture.entity';
import { LectureProgress } from '@entities/lecture-progress.entity';
import { LecturesService } from '../../courses/lectures.service';
import { Course } from '@entities/course.entity';
import { CourseStatus } from '@enums/course-status.enum';
import { AmoEventsService } from '../../amo/amo-events.service';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';
import { Submission } from '@entities/submission.entity';
import { Assessment } from '@entities/assessment.entity';
import { AssessmentType } from '@lib/types/enums';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { Enrollment } from '@entities/enrollment.entity';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { LectureStatus } from '@enums/lecture-status.enum';
import { strict } from 'assert';
import { AmoUserEvent } from '@entities/amo-user-event.entity';
import { User } from '@entities/user.entity';
import { ignoredCourseTitles, paidEndrollmentTypes } from './paid-enrollment-types';
import { KafkaClientService } from 'src/kafka/kafka-client.service';

@Injectable()
export class LectureProgressTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => UsersService))
    readonly usersService: UsersService,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
    readonly amoService: AmoService,
    readonly amoEventsService: AmoEventsService,
    @Inject(forwardRef(() => LecturesService))
    readonly lecturesService: LecturesService,
    @InjectRepository(Lecture)
    private lecturesRepository: Repository<Lecture>,
    @InjectRepository(LectureProgress)
    private lectureProgressRepository: Repository<LectureProgress>,
    @InjectRepository(Course)
    private coursesRepository: Repository<Course>,
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,
    @InjectRepository(Enrollment)
    private enrollmentsRepository: Repository<Enrollment>,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async lectureProgressUpdate(userId: string, lectureParam: Lecture | string, status: LectureStatus) {
    let lecture: Lecture;

    if (typeof lectureParam === 'string') {
      lecture = await this.lecturesService.findOne(lectureParam, true);
    } else {
      lecture = lectureParam;
    }

    if (!lecture) {
      return false;
    }

    const courseId = lecture.course.id;

    // Triggers if update includes status
    if (status) {
      this.addTrigger(
        TriggerType.userProgressChanged,
        { userId, courseId, lectureId: lecture.id, status },
        { priority: TriggerPriority.normal },
      );
    }

    // Not create internal leads in AMO
    const enrollment = await this.enrollmentsRepository
      .createQueryBuilder('enrollment')
      .andWhere('enrollment.user.id = :userId', { userId })
      .andWhere('enrollment.course.id = :courseId', { courseId })
      .andWhere('enrollment.role = :role', { role: CourseRole.Student })
      .getOne();

    if (!enrollment || enrollment.type === EnrollmentType.Inner) {
      return;
    }

    const lp = await this.lectureProgressRepository
      .createQueryBuilder('lecture_progress')
      .leftJoin('lecture_progress.lecture', 'lecture')
      .andWhere('lecture.active = true')
      .andWhere('lecture.published = true')
      .andWhere('lecture.course.id = :courseId', { courseId })
      .andWhere('lecture_progress.user.id = :userId', { userId })
      .getMany();

    const lectures = await this.lecturesRepository
      .createQueryBuilder('lecture')
      .andWhere('lecture.active = true')
      .andWhere('lecture.published = true')
      .andWhere('lecture.course.id = :courseId', { courseId })
      .getMany();

    const course = await this.coursesRepository.findOne(courseId, {
      relations: ['modules'],
    });

    const countViewsLectures = lp.length;

    const countCompletedLectures = lp.filter(l => l.status === 'success').length;

    const user = await this.usersService.findOne(userId);
    strict.ok(user);

    //Отрабатываем пока только на Бразилии
    if (['lms-production'].includes(process.env.ENVIRONMENT)) {
      this.addTrigger(
        TriggerType.moveTrialLead,
        {
          user: { email: user.email },
          course: { cmsId: course.cmsId },
        },
        {
          priority: TriggerPriority.normal,
          onceHash: `progress:${user.id}:${course.id}'`,
        },
      );
    }

    // create delayed trigger for checking students who stopped learning
    if (enrollment.type === EnrollmentType.Buy && !ignoredCourseTitles.includes(course.title)) {
      // find or create amo event
      const amoEvent = await this.findOrCreateStoppedLearningAmoEvent({ user, lecture });
      strict.ok(amoEvent);

      // add or refresh delayed trigger
      // do we need to "await"?
      this.addTrigger(
        TriggerType.userStoppedLearning,
        { studentId: user.id, amoData: amoEvent.data, eventId: amoEvent.id, enrollmentId: enrollment.id },
        {
          priority: TriggerPriority.normal,
          onceHash: `${userId}${courseId}`,
          delay: { days: 30 },
          refresh: true,
        },
      );
    }

    // remove course not started trigger
    this.cancelTrigger({
      onceHash: `${userId}${courseId}`,
      triggerType: TriggerType.userCourseNotStarted,
    });

    // check if need to cancel onboarding
    if ((countViewsLectures * 100) / lectures.length > 5) {
      // if has event no need to create trigger
      const events = await this.amoEventsService.findAll({
        userId,
        courseId,
        eventType: AmoUserEventType.courseOnboardingSuccess,
      });
      if (events.length === 0) {
        const amoData = {
          lead: `Onboarding call ${user.name}`,
          courseName: lecture.course.title,
        };
        const event = await this.amoEventsService.create({
          userId,
          courseId,
          eventType: AmoUserEventType.courseOnboardingSuccess,
          data: amoData,
        });
        this.addTrigger(
          TriggerType.lectureProgressUpdated,
          { studentId: user.id, amoData, eventId: event.id },
          { priority: TriggerPriority.normal },
        );
      }
    }

    // check if need to trigger almost complete
    if (
      paidEndrollmentTypes.includes(enrollment.type) &&
      course.status === CourseStatus.completed &&
      (countCompletedLectures * 100) / lectures.length >= 80 &&
      // hack for ignoring some courses that are "free" despite the enrollment having type "Buy"
      !ignoredCourseTitles.includes(course.title)
    ) {
      // if has event no need to create trigger
      const events = await this.amoEventsService.findAll({
        userId,
        courseId,
        eventType: AmoUserEventType.courseAlmostCompleted,
      });
      if (events.length === 0) {
        const modules = course.modules;
        const modulesData = await (async () => {
          // Module has assessments(type=default) && student has no submissions?
          // OR should we count contents with "task" type
          const needsExercise = [];
          // Lecture count > lecture progress count && progress.progress == 100?
          const notWatchedVideo = [];
          // Has submissions with score < assessment(type=default) passing score?
          const notPassed = [];

          for (const module of modules) {
            const [lectures, submissions] = await Promise.all([
              this.lecturesRepository.find({
                where: { module },
              }),
              this.submissionsRepository.find({
                where: { module, current_attempt: true },
                relations: ['assessment'],
              }),
            ]);
            const lectureIds = lectures.map(lecture => lecture.id);
            const [assessments, lectureProgresses] = await Promise.all([
              // @ts-ignore TODO: need description
              this.assessmentsRepository.find({
                where: {
                  lecture: In(lectureIds),
                  type: AssessmentType.Default,
                },
              }),
              // @ts-ignore TODO: need description
              this.lectureProgressRepository.find({
                where: { lecture: In(lectureIds) },
              }),
            ]);

            if (assessments.length > submissions.length) {
              needsExercise.push(module.title);
            }
            if (lectures.length > lectureProgresses.length) {
              notWatchedVideo.push(module.title);
            }
            const notPassedSubmissions = submissions.filter(
              s => s.assessment.type === AssessmentType.Default && s.score < s.assessment.passingScore,
            );
            if (notPassedSubmissions.length) {
              notPassed.push(module.title);
            }
          }

          return { needsExercise, notWatchedVideo, notPassed };
        })();
        const amoData = {
          lead: `Almost finished course ${user.name}`,
          courseName: lecture.course.title,
          phone: user.profile?.phone ? user.profile.phone : '',
          email: user.email,
          ...modulesData,
        };
        const event = await this.amoEventsService.create({
          userId,
          courseId,
          eventType: AmoUserEventType.courseAlmostCompleted,
          data: amoData,
        });
        this.addTrigger(
          TriggerType.userCourseAlmostCompleted,
          { studentId: user.id, amoData, eventId: event.id, enrollmentId: enrollment.id },
          {
            priority: TriggerPriority.normal,
            onceHash: `${userId}${courseId}`,
          },
        );
      }
    }

    // check if trigger complete
    if (
      paidEndrollmentTypes.includes(enrollment.type) &&
      course.status === CourseStatus.completed &&
      countCompletedLectures >= lectures.length &&
      // hack for ignoring some courses that are "free" despite the enrollment having type "Buy"
      !ignoredCourseTitles.includes(course.title)
    ) {
      // if has event no need to create trigger
      const events = await this.amoEventsService.findAll({
        userId,
        courseId,
        eventType: AmoUserEventType.courseCompleted,
      });
      if (events.length === 0) {
        const amoData = {
          lead: `Finished course ${user.name}`,
          courseName: lecture.course.title,
          phone: user.profile?.phone ? user.profile.phone : '',
          email: user.email,
        };
        const event = await this.amoEventsService.create({
          userId,
          courseId,
          eventType: AmoUserEventType.courseCompleted,
          data: amoData,
        });
        this.addTrigger(
          TriggerType.userCourseCompleted,
          { studentId: user.id, amoData, eventId: event.id, enrollmentId: enrollment.id },
          {
            priority: TriggerPriority.normal,
            onceHash: `${userId}${courseId}`,
          },
        );
      }
    }
  }

  async findOrCreateStoppedLearningAmoEvent({ user, lecture }: { user: User; lecture: Lecture }): Promise<AmoUserEvent> {
    const existingAmoEvent = await this.amoEventsService.findOne({
      userId: user.id,
      courseId: lecture.courseId,
      eventType: AmoUserEventType.courseStoppedStudying,
    });
    if (existingAmoEvent) {
      return existingAmoEvent;
    }

    return this.amoEventsService.create({
      userId: user.id,
      courseId: lecture.course.id,
      eventType: AmoUserEventType.courseStoppedStudying,
      data: {
        lead: `${user.name} stopped learning`,
        courseName: lecture.course.title,
        name: user.name,
        phone: user.profile?.phone ? user.profile.phone : '',
        email: user.email,
        lastAccessed: new Date().toISOString(),
      },
    });
  }
}
