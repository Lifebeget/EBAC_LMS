import { Trigger } from '@entities/trigger.entity';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { UsersService } from 'src/users/users.service';
import { TriggerType } from '@enums/trigger-type.enum';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { CoursesService } from 'src/courses/courses.service';
import { Enrollment } from '@entities/enrollment.entity';
import { EnrollmentTriggerData } from '../events/enrollment-created.event';
import { UpdateEnrollmentDto } from 'src/courses/dto/update-enrollment.dto';
import { MailService } from 'src/mail/mail.service';
import * as dayjs from 'dayjs';
import { CourseStatus } from '@enums/course-status.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import config from 'config';
import { AuthService } from 'src/auth/auth.service';
import { KafkaClientService } from 'src/kafka/kafka-client.service';

@Injectable()
export class EnrollmentTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => UsersService))
    readonly usersService: UsersService,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
    @Inject(forwardRef(() => CoursesService))
    readonly coursesService: CoursesService,
    @Inject(forwardRef(() => MailService))
    readonly mailService: MailService,
    @Inject(forwardRef(() => AuthService))
    readonly authService: AuthService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async prepareTriggerData(courseId: string, enrollment: Enrollment, sendEmail?: boolean) {
    const course = await this.coursesService.findOne(courseId);
    const user = await this.usersService.findOne(enrollment.user.id);

    const newSurveyLink = await this.coursesService.generateNewSurveyLinkByCourse(course, user);
    const courseLink = await this.coursesService.generateCourseLink(courseId, user);

    const triggerData = {
      enrollment: {
        id: enrollment.id,
        type: enrollment.type,
        role: enrollment.role,
        active: enrollment.active,
        dateFrom: enrollment.dateFrom,
        dateTo: enrollment.dateTo,
        reason: enrollment.reason,
        options: enrollment.options,
      },
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
        domain: user.domain,
      },
      profile: {
        phone: user.profile?.phone || null,
      },
      course: {
        id: course.id,
        title: course.title,
        version: course.version,
        cmsId: course.cmsId,
        duration: course.duration,
        modules: course.modules.length,
        startAt: course.startAt,
        status: course.status,
      },
      links: {
        course: courseLink,
        survey: newSurveyLink,
      },
      enrollments: user.enrollments.map(e => ({
        id: e.id,
        courseId: e.courseId,
        type: e.type,
        role: e.role,
        active: e.active,
        courseNotForSale: e.course.notForSale,
      })),
      studentId: user.id,
      sendEmail,
    } as EnrollmentTriggerData;

    return triggerData;
  }

  async enrollmentCreated(courseId: string, enrollment: Enrollment, sendEmail?: boolean) {
    const triggerData = await this.prepareTriggerData(courseId, enrollment, sendEmail);
    this.addTrigger(TriggerType.enrollmentCreated, triggerData, { priority: TriggerPriority.normal });
  }

  getStudentName(user: EnrollmentTriggerData['user']) {
    return user.name.trim().split(' ')[0];
  }

  generateCourseDate(course: EnrollmentTriggerData['course']) {
    return course.startAt ? dayjs(course.startAt).format('DD/MM/YYYY') : '...';
  }

  async userEnrolledWelcomeEmail(event: EnrollmentTriggerData) {
    const enrollment = event.enrollment;
    if (enrollment.type === EnrollmentType.Trial) {
      this.userEnrolledTrialWelcomeEmail(event.user, event.course, event.links);
    } else if (enrollment.type !== EnrollmentType.Satellite) {
      this.userEnrolledGeneralWelcomeEmail(event.user, event.course, event.links);
    }
  }

  async userEnrolledTrialWelcomeEmail(
    user: EnrollmentTriggerData['user'],
    course: EnrollmentTriggerData['course'],
    links: EnrollmentTriggerData['links'],
  ) {
    // Generate AuthLink
    const date = new Date();
    date.setDate(date.getDate() + config.authLinkExpired.userEnrolledTrial);
    const authLink = await this.authService.loginLink(user, {
      expires: date,
      email: user.email,
      backUrl: links.course,
    });

    this.mailService.addEmailToQueue(
      user,
      TriggerType.userEnrolledTrial,
      {
        student: this.getStudentName(user),
        studentId: user.id,
        courseName: course.title,
        courseLink: authLink.url,
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }

  async userEnrolledGeneralWelcomeEmail(
    user: EnrollmentTriggerData['user'],
    course: EnrollmentTriggerData['course'],
    links: EnrollmentTriggerData['links'],
  ) {
    let triggerType: TriggerType;
    if (course.status == CourseStatus.completed) {
      triggerType = TriggerType.courseFullyPublished;
    } else if (course.status == CourseStatus.uncompleted) {
      triggerType = TriggerType.courseNotCompleted;
    } else if (course.status == CourseStatus.created) {
      triggerType = TriggerType.courseNotStarted;
    }
    if (!triggerType) {
      return;
    }

    this.mailService.addEmailToQueue(
      user,
      triggerType,
      {
        course: course.title,
        student: this.getStudentName(user),
        support_link: links.course,
        course_link: links.course,
        course_months: course.duration,
        course_modules: course.modules,
        course_date: this.generateCourseDate(course),
        zero_module: links.course,
        new_survey_link: links.survey,
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }

  checkEnglishFirstCriteria(enrollments: EnrollmentTriggerData['enrollments']) {
    const paidLength = enrollments.filter(enrollment => enrollment.type === EnrollmentType.Buy).length;
    const notForSaleLength = enrollments.filter(enrollment => enrollment.courseNotForSale).length;
    const sendEnglishFirstEmail = (paidLength === 0 && notForSaleLength === 1) || (paidLength === 1 && notForSaleLength === 0);
    return sendEnglishFirstEmail;
  }

  async englishFirst(user: EnrollmentTriggerData['user']) {
    this.mailService.addEmailToQueue(
      user,
      TriggerType.englishFirst,
      {
        student: this.getStudentName(user),
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }

  async userEnrollUpdate(updateEnrollment: UpdateEnrollmentDto & { id: string }) {
    return;
  }

  async userEnrollDelete(enrollment: Enrollment) {
    return;
  }
}
