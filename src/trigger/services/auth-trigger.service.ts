import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { TriggerService } from '../trigger.service';
import { User } from '@entities/user.entity';
import { Token } from '@entities/token.entity';
import { TriggerType } from '@enums/trigger-type.enum';
import { InjectRepository } from '@nestjs/typeorm';
import { Trigger } from '@entities/trigger.entity';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Repository } from 'typeorm';
import { PlatformType } from '@enums/platform-type.enum';
import axios from 'axios';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import config from 'config';

@Injectable()
export class AuthTriggerService extends TriggerService {
  private logger = new Logger(AuthTriggerService.name);
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async forgotPassword(user: User, token: Token, platform: PlatformType) {
    const baseUrl =
      platform === PlatformType.student
        ? config.setUser(user).get('STUDENT_LMS_URL')
        : config.setUser(user).get('TUTOR_LMS_URL');
    await this.addTrigger(
      TriggerType.forgotPassword,
      {
        studentId: user.id,
        resetPasswordLink: `${baseUrl}/reset-password/${token.token}`,
      },
      {},
    );
  }

  async tutorWelcome(user: User, password: string) {
    await this.addTrigger(
      TriggerType.tutorWelcome,
      {
        password,
        studentId: user.id,
        url: config.setUser(user).get('TUTOR_LMS_URL'),
      },
      {},
    );
  }

  async userLogin(user: User) {
    /**
     * Update (upsert) attributes on login in Customer.io
     * Only on Brazil production for now!
     *  */
    if (process.env.CIO_TRIGGER_AUTH_URL && ['lms-production'].includes(process.env.ENVIRONMENT))
      await axios({
        url: process.env.CIO_TRIGGER_AUTH_URL,
        method: 'POST',
        data: {
          lang_code: 'pt-BR',
          lms_user: user,
          last_login: Date.now(),
        },
      });
    // Cancel all active "userCourseNotStarted" triggers for this user
    const userId = user.id;
    const courseNotStartedTriggers = await this.triggerRepository.find({
      where: {
        triggerType: TriggerType.userCourseNotStarted,
        user: { id: userId },
        canceled: false,
        executed: false,
      },
    });
    this.logger.log(`Cancelling ${courseNotStartedTriggers.length} "userCourseNotStarted" triggers for user "${userId}"`);
    await Promise.all(courseNotStartedTriggers.map(t => this.cancelTrigger({ id: t.id })));
  }

  // TODO remove when reactivation program is over or
  // consider further use cases
  async reactivateMoveLead(user: User) {
    await this.addTrigger(
      TriggerType.reactivateMoveLead,
      {
        user_id: user.id,
      },
      {},
    );
  }
}
