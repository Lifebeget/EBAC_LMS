import { Submission } from '@entities/submission.entity';
import { Trigger } from '@entities/trigger.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import config from 'config';

@Injectable()
export class SubmissionTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @InjectRepository(Submission)
    readonly submissionRepository: Repository<Submission>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async submissionGraded(submissionId, score) {
    const submission = await this.submissionRepository.findOne(submissionId, {
      relations: ['assessment', 'user', 'course', 'lecture'],
    });

    let type: TriggerType;

    if (submission.isQuestion) {
      type = TriggerType.submissionGradedQuestion;
    } else if (score >= submission.assessment.passingScore) {
      type = TriggerType.submissionGradedSuccess;
    } else {
      type = TriggerType.submissionGradedFailed;
    }

    await this.addTrigger(
      type,
      {
        studentId: submission.user.id,
        activityScore: score,
        course: submission.course.title,
        gradedActivityLink: `${config.setUser(submission.user).get('STUDENT_LMS_URL')}/lesson/${submission.lecture.id}`,
      },
      {
        priority: TriggerPriority.low,
      },
    );
  }
}
