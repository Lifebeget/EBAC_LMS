import { Content } from '@entities/content.entity';
import { Trigger } from '@entities/trigger.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';

@Injectable()
export class ContentTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async kalturaBlockUpdated(content: Content) {
    // Check block after one hour
    this.addTrigger(TriggerType.contentKalturaUpdated, { content, inform: false }, { priority: TriggerPriority.low, delay: { hours: 1 } });
    // Check block after one day
    this.addTrigger(TriggerType.contentKalturaUpdated, { content, inform: true }, { priority: TriggerPriority.low, delay: { days: 1 } });
  }
}
