import { Trigger } from '@entities/trigger.entity';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { TriggerType } from '@enums/trigger-type.enum';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { UpdateProfileDto } from '../../users/dto/update-profile.dto';
import { KafkaClientService } from 'src/kafka/kafka-client.service';

@Injectable()
export class ProfileTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async userProfileUpdate(userId: string, profile: UpdateProfileDto) {
    this.addTrigger(
      TriggerType.userProfileUpdated,
      { userId, profile },
      {
        priority: TriggerPriority.normal,
      },
    );
  }
}
