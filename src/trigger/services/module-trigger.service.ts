import { Trigger } from '@entities/trigger.entity';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { Module } from '@entities/module.entity';
import { UsersService } from 'src/users/users.service';
import { TriggerType } from '@enums/trigger-type.enum';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { KafkaClientService } from 'src/kafka/kafka-client.service';
import config from 'config';

@Injectable()
export class ModuleTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @InjectRepository(Module)
    readonly moduleRepository: Repository<Module>,
    readonly usersService: UsersService,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async modulePublished(module: string | Module, sendEmail: boolean) {
    if (typeof module === 'string') {
      module = await this.moduleRepository.findOne(module, {
        relations: ['course'],
      });
    }

    if (!module.course) {
      module = await this.moduleRepository.findOne(module.id, {
        relations: ['course'],
      });
    }

    const users = await this.usersService.findAll(
      { page: 1, itemsPerPage: 20, showAll: true },
      {
        studentCourseId: module.course.id,
      },
    );

    users.results.forEach(u => {
      this.addTrigger(
        TriggerType.modulePublished,
        {
          studentId: u.id,
          // @ts-ignore TODO: need description
          courseName: module.course.title,
          // @ts-ignore TODO: need description
          courseLink: `${config.setUser(u).get('STUDENT_LMS_URL')}/courses/${module.course.id}`,
          sendEmail,
        },
        {
          priority: TriggerPriority.low,
        },
      );
    });
  }
}
