import { Trigger } from '@entities/trigger.entity';
import { TriggerPriority } from '@enums/trigger-priority.enum';
import { TriggerType } from '@enums/trigger-type.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from 'eventemitter2';
import { Repository } from 'typeorm';
import { TriggerService } from '../trigger.service';
import { AmoUserEventType } from '@enums/amo-user-event-type.enum';
import { SupportMessage } from '@entities/support-message.entity';
import { AmoEventsService } from '../../amo/amo-events.service';
import { KafkaClientService } from 'src/kafka/kafka-client.service';

@Injectable()
export class SupportMessagesTriggerService extends TriggerService {
  constructor(
    readonly eventEmitter: EventEmitter2,
    @InjectRepository(Trigger)
    readonly triggerRepository: Repository<Trigger>,
    @Inject(forwardRef(() => AmoEventsService))
    readonly amoEventsService: AmoEventsService,
    @Inject(forwardRef(() => KafkaClientService))
    readonly kafkaClient: KafkaClientService,
  ) {
    super(eventEmitter, triggerRepository, kafkaClient);
  }

  async supportMessageSent(supportMessage: SupportMessage) {
    const event = await this.amoEventsService.create({
      userId: supportMessage.user?.id || null,
      eventType: AmoUserEventType.supportMessageCreated,
      data: supportMessage as any,
    });

    this.addTrigger(
      TriggerType.supportMessageSent,
      {
        studentId: supportMessage.user?.id || null,
        amoData: supportMessage,
        eventId: event.id,
      },
      { priority: TriggerPriority.normal },
    );
  }
}
