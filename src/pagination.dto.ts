import { ApiOkResponse, ApiProperty, getSchemaPath } from '@nestjs/swagger';
import { applyDecorators, Type } from '@nestjs/common';

export class PaginationDto {
  @ApiProperty({
    description: 'Current page (starting with 1)',
    default: 1,
    minimum: 1,
    required: false,
  })
  page: number;

  @ApiProperty({
    description: 'Page size',
    default: 100,
    minimum: 1,
    required: false,
  })
  itemsPerPage: number;

  @ApiProperty({
    description: 'Ignore pagination defaults and fetch all results. Disregard all other pagination params',
    default: false,
    required: false,
  })
  showAll: boolean;
}

export const ApiPaginatedResponse = <TModel extends Type<any>>(model: TModel) => {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        properties: {
          meta: {
            properties: {
              page: { type: 'number' },
              itemsPerPage: { type: 'number' },
              total: { type: 'number', nullable: true },
            },
          },
          results: {
            type: 'array',
            items: { $ref: getSchemaPath(model) },
          },
        },
      },
    }),
  );
};

export class PaginationResponse {
  page: number;
  itemsPerPage: number;
  total?: number;
  showAll: boolean;
  sortField?: string;
  sortDirection?: 'ASC' | 'DESC';
}

export type Paginated<T> = {
  results: T;
  meta: PaginationResponse;
  raw?: any;
};
