import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateFaqDto {
  @ApiProperty({ description: 'Sorting' })
  @IsOptional()
  sort?: number;

  @ApiProperty({ description: 'Question body' })
  @IsNotEmpty()
  question: string;

  @ApiProperty({ description: 'Answer for the question' })
  @IsNotEmpty()
  answer?: string;

  @ApiProperty({
    required: false,
    description: 'Connected courses',
  })
  courses: Array<{ id: string }>;
}
