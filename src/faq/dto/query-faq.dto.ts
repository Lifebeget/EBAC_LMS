import { ApiProperty } from '@nestjs/swagger';
import { PaginationDto } from 'src/pagination.dto';

export class QueryFaqByCourse {
  @ApiProperty({ description: 'Including not active in query, default false.' })
  includeHidden?: boolean;
}

export class QueryFaq extends PaginationDto {
  @ApiProperty({ description: 'Including not active in query, default false.' })
  includeHidden?: boolean;

  @ApiProperty({ description: 'Faqs without course.' })
  general?: boolean;
}
