import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateFaqDto } from './dto/create-faq.dto';
import { QueryFaqByCourse } from './dto/query-faq.dto';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { Faq } from './entities/faq.entity';

@Injectable()
export class FaqService {
  constructor(
    @InjectRepository(Faq)
    private readonly faqRepository: Repository<Faq>,
  ) {}

  @Transactional()
  async findOne(id: string) {
    return await this.faqRepository.findOne(id, { relations: ['courses'] });
  }

  async findAll(query) {
    const res = this.faqRepository
      .createQueryBuilder('faq')
      .leftJoinAndSelect('faq.courses', 'courses')
      .leftJoinAndSelect('courses.image', 'image');

    if (!query.showAll) {
      res.skip((query.page - 1) * query.itemsPerPage).take(query.itemsPerPage);
    }

    if (!query.includeHidden) {
      res.andWhere('faq.active = :active', { active: true });
    }

    if (query.general && query.general !== 'false') {
      res.andWhere('courses.id IS NULL');
    }

    const [results, total] = await res.getManyAndCount();
    return {
      results,
      meta: { page: query.page, itemsPerPage: query.itemsPerPage, total },
    };
  }

  async findByCourse(courseId: string, query: QueryFaqByCourse) {
    const res = this.faqRepository
      .createQueryBuilder('faq')
      .leftJoin('faq.courses', 'filtercourse')
      .andWhere('filtercourse.id = :courseId', { courseId })
      .leftJoinAndSelect('faq.courses', 'courses');

    //if is not active flag = !true, show only active faqs
    if (!query.includeHidden) {
      res.andWhere('faq.active = :active', { active: true });
    }

    return res.getMany();
  }

  async create(faq: CreateFaqDto) {
    return await this.faqRepository.save(faq);
  }

  async update(id: string, faq: UpdateFaqDto) {
    await this.faqRepository.save({ id, ...faq });
    return await this.faqRepository.findOne(id, { relations: ['courses'] });
  }

  async delete(id: string) {
    return this.faqRepository.delete(id);
  }
}
