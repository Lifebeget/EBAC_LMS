import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateFaqDto } from './dto/create-faq.dto';
import { FaqService } from './faq.service';
import { Permission } from '@enums/permission.enum';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { QueryFaqByCourse } from './dto/query-faq.dto';

@ApiTags('faq')
@ApiBearerAuth()
@Controller('faq')
export class FaqController {
  constructor(private readonly faqService: FaqService) {}

  @Get(':id')
  @ApiOperation({ summary: 'Find one faq' })
  @ApiQuery({
    name: 'id',
    description: 'Faq id',
    required: true,
  })
  async findById(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.faqService.findOne(id);
  }

  @Get('')
  @ApiOperation({ summary: 'Find many faq' })
  @ApiQuery({
    name: 'page',
    description: 'Page number',
    required: true,
    type: Number,
  })
  @ApiQuery({
    name: 'itemsPerPage',
    description: 'Items per page',
    required: true,
    type: Number,
  })
  @ApiQuery({
    name: 'includeHidden',
    description: 'Show not active',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'general',
    description: 'Only without course',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'showAll',
    description: 'Show all(paging)',
    required: false,
    type: Boolean,
  })
  async findAll(@Query() query: QueryFaqByCourse, @Req() req: Request) {
    //if user doesn't have FAQ_VIEW permission -> remove includeHidden Faq from query
    if (!req.user?.permissions.includes(Permission.FAQ_VIEW)) query.includeHidden = undefined;

    return this.faqService.findAll(query);
  }

  @Get('course/:id')
  @ApiOperation({ summary: 'Find faqs by course id' })
  @ApiParam({
    name: 'id',
    description: 'Course id',
    required: true,
  })
  async findByCourseId(@Param('id', new ParseUUIDPipe()) id: string, @Query() query: QueryFaqByCourse, @Req() req: Request) {
    //if user doesn't have FAQ_VIEW permission -> remove includeHidden Faq from query
    if (!req.user?.permissions.includes(Permission.FAQ_VIEW)) query.includeHidden = undefined;

    return this.faqService.findByCourse(id, query);
  }

  @Post()
  @ApiOperation({ summary: 'Create new Faq' })
  @RequirePermissions(Permission.FAQ_MANAGE)
  async create(@Body() createFaqDto: CreateFaqDto) {
    return this.faqService.create(createFaqDto);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update Faq' })
  @RequirePermissions(Permission.FAQ_MANAGE)
  @ApiParam({
    name: 'id',
    description: 'Faq id',
    required: true,
  })
  async update(@Body() createFaqDto: UpdateFaqDto, @Param('id', new ParseUUIDPipe()) id: string) {
    return this.faqService.update(id, createFaqDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete faq, permanent. Use active for disable' })
  @RequirePermissions(Permission.FAQ_MANAGE)
  async delete(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.faqService.delete(id);
  }
}
