import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Field, ObjectType } from '@nestjs/graphql';
import { Course } from '@entities/course.entity';

@ObjectType()
@Entity()
export class Faq {
  @Field()
  @ApiProperty({ description: 'UUID' })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @ApiProperty({ description: 'Sorting. Min is high priority' })
  @Column({ type: 'int', default: 500 })
  sort: number;

  @Field()
  @ApiProperty({ description: 'Active' })
  @Column({ type: 'boolean', default: true })
  active: boolean;

  @Field({ nullable: false })
  @ApiProperty({ description: 'Question' })
  @Column({ type: 'varchar', nullable: false })
  question: string;

  @Field({ nullable: true })
  @ApiProperty({ description: 'Answer' })
  @Column({ type: 'varchar', nullable: true })
  answer?: string;

  @Field()
  @ApiProperty({ description: 'Creation date' })
  @CreateDateColumn()
  created?: Date;

  @Field()
  @ApiProperty({ description: 'Modified date' })
  @UpdateDateColumn()
  modified?: Date;

  @Field(type => [Course])
  @ApiProperty({ type: () => [Course] })
  @ManyToMany(() => Course)
  @JoinTable()
  courses: Course[];
}
