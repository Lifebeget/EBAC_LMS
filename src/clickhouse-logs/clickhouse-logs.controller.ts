import { Permission } from '@lib/types/enums';
import { Controller, Get, Param, Query, Req } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Pagination } from 'src/pagination.decorator';
import { PaginationDto } from 'src/pagination.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { QueryLogsDto } from './dto/query-log.dto';
import { LogsService } from './clickhouse-logs.service';

@Controller()
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get('logs')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'List all submissions' })
  //@ApiPaginatedResponse(Submission)
  findAll(@Query() query: QueryLogsDto, @Pagination() paging: PaginationDto, @Req() _req: Express.Request) {
    return this.logsService.findAll(query, paging);
  }

  @Get('logs/:id')
  @RequirePermissions(Permission.GRANT_ADMIN_ROLE)
  @ApiOperation({ summary: 'Get one submissions' })
  findOne(@Param('id') id: string) {
    return this.logsService.findOne(+id);
  }
}
