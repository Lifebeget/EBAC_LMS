import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString, ValidateIf } from 'class-validator';
import { LogsSortByEnums } from '@lib/api/logs/types';

export class QueryLogsDto {
  @ApiProperty({ description: 'User name' })
  @ValidateIf(o => o.hasOwnProperty('userName'))
  @IsString()
  userName?: string;

  @ApiProperty({ description: 'Text search body' })
  @ValidateIf(o => o.hasOwnProperty('bodySearch'))
  @IsString()
  bodySearch?: string;

  @ApiProperty({ description: 'Controller method name' })
  @ValidateIf(o => o.hasOwnProperty('handlerTag'))
  @IsString()
  handlerTag?: string;

  @ApiProperty({ description: 'Start date for filter' })
  periodStart?: Date;

  @ApiProperty({ description: 'End date for filter' })
  periodEnd?: Date;

  @ApiProperty({
    description: 'Sort by field',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('sortField'))
  @IsEnum(LogsSortByEnums)
  sortField?: LogsSortByEnums;

  @ApiProperty({
    description: 'Sort direction',
    required: false,
  })
  sortDirection?: 'ASC' | 'DESC';
}
