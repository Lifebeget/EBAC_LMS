import { Module } from '@nestjs/common';
import { LogsService } from './clickhouse-logs.service';
import { LogsController } from './clickhouse-logs.controller';

@Module({
  controllers: [LogsController],
  providers: [LogsService],
})
export class LogsModule {}
