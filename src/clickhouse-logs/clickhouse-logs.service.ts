import { Injectable } from '@nestjs/common';
import { PaginationDto } from 'src/pagination.dto';
import { QueryLogsDto } from './dto/query-log.dto';
import { query as clickhouseQuery, toClickhouseTime } from 'src/service/_common/clickhouse/db';
import * as dayjs from 'dayjs';

@Injectable()
export class LogsService {
  async findAll(query: QueryLogsDto, paging: PaginationDto) {
    let sql = `SELECT * FROM ${process.env.CLICKHOUSE_DB_NAME}.log `;
    const where = [];
    if (query.bodySearch) {
      where.push(`ilike(Body, '%${query.bodySearch.replace(/'/gi, "''")}%')`);
    }
    if (query.handlerTag) {
      where.push(`ilike(HandlerTag, '%${query.handlerTag.replace(/'/gi, "''")}%')`);
    }
    if (query.userName) {
      where.push(`ilike(UserName, '%${query.userName.replace(/'/gi, "''")}%')`);
    }
    if (query.periodStart) {
      where.push(`Time >= ${toClickhouseTime(dayjs(query.periodStart, 'YYYY-MM-DD HH:mm').toDate())}`);
    }
    if (query.periodEnd) {
      where.push(`Time <= ${toClickhouseTime(dayjs(query.periodEnd, 'YYYY-MM-DD HH:mm').toDate())}`);
    }
    if (where.length) {
      sql += where.reduce((a, e, i) => (i > 0 ? `${a} AND ${e} ` : `${a} ${e}`), ' WHERE ');
    }

    sql += ` ORDER BY ${query.sortField || 'Time'} ${query.sortDirection || 'DESC'}`;

    sql += ` LIMIT ${paging.itemsPerPage} OFFSET ${(paging.page - 1) * paging.itemsPerPage} `;
    sql += ` FORMAT JSON`;

    console.error(sql);

    const results = await clickhouseQuery(sql);

    return {
      ...results,
      //meta: { ...paging, total },
    };
  }

  findOne(id: number) {
    return `This action returns a #${id} log`;
  }
}
