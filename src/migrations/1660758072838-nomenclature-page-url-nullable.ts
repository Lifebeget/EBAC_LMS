import {MigrationInterface, QueryRunner} from "typeorm";

export class nomenclaturePageUrlNullable1660758072838 implements MigrationInterface {
    name = 'nomenclaturePageUrlNullable1660758072838'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nomenclature" ALTER COLUMN "page_url" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nomenclature" ALTER COLUMN "page_url" SET NOT NULL`);
    }

}
