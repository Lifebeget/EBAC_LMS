import { MigrationInterface, QueryRunner } from "typeorm";

export class moreIndexesForEnrollmentAndFile1657896900970 implements MigrationInterface {
    name = 'moreIndexesForEnrollmentAndFile1657896900970';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_d6f7b5a9700ff483d5814e4641" ON "enrollment" ("type") `);
        await queryRunner.query(`CREATE INDEX "IDX_8445dd90d16d812c6f3955b16c" ON "file" ("deleted") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_8445dd90d16d812c6f3955b16c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d6f7b5a9700ff483d5814e4641"`);
    }

}
