import { Settings } from '@entities/settings.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class forumuserebac1636520637655 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const setting = await queryRunner.manager.findOne(Settings);
    if (!setting) {
      console.error('settings not found at DB!');
      return;
    }
    await queryRunner.manager.update(Settings, setting.id, {
      mainSettings: {
        ...setting.mainSettings,
        forumEbacUserEmail: 'online@ebac.art.br',
      },
    });
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
