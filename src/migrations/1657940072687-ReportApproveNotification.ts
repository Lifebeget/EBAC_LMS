import { MigrationInterface, QueryRunner } from 'typeorm';

export class ReportApproveNotification1657940072687 implements MigrationInterface {
  name = 'ReportApproveNotification1657940072687';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "system_notification" ADD "to" character varying`);
    await queryRunner.query(`ALTER TABLE "system_notification" ADD "for_role" character varying`);
    await queryRunner.query(`ALTER TABLE "system_notification" ADD "translate" boolean`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "system_notification" DROP COLUMN "translate"`);
    await queryRunner.query(`ALTER TABLE "system_notification" DROP COLUMN "for_role"`);
    await queryRunner.query(`ALTER TABLE "system_notification" DROP COLUMN "to"`);
  }
}
