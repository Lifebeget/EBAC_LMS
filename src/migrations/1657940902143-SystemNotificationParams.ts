import { MigrationInterface, QueryRunner } from 'typeorm';

export class SystemNotificationParams1657940902143 implements MigrationInterface {
  name = 'SystemNotificationParams1657940902143';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "system_notification" ADD "params" jsonb`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "system_notification" DROP COLUMN "params"`);
  }
}
