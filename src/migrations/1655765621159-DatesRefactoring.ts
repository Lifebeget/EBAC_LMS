import { MigrationInterface, QueryRunner } from 'typeorm';

const TZ = process.env.ENVIRONMENT == 'local' ? 'UTC' : 'ADT';
// ADT	-03:00	Atlantic Daylight Time -- Sao Paulo
// ACT	-05:00	Atlantic/Porto Acre Standard Time -- Mexico
// Dev, Review and Prod database uses Brazil dump so has dates incorrectly stored as -03 relative to UTC
// Mexico DB runs at Brazil time since transition to Kube cluster
//   and probably will have all the dates prior to 02.06.2022 misinterpreted

export class DatesRefactoring1655765621159 implements MigrationInterface {
  tables = [
    ['role', ['date_from', 'date_to']],
    ['trigger', ['created', 'date_to_execute']],
    ['module', ['created', 'modified', 'published_at']],
    ['user', ['created', 'modified', 'last_accessed']],
    ['webinar', ['created', 'modified', 'deleted', 'start_date_utc', 'final_start_date_utc', 'survey_open_date', 'survey_close_date']],
    ['amo_user_event', ['created', 'modified']],
    ['amo_user', ['created', 'modified']],
    ['amo', ['modified']],
    ['answer', ['created', 'modified']],
    ['assessment', ['created', 'modified', 'deleted']],
    ['base_entity', ['created', 'modified', 'deleted']],
    ['assessment_criterion_category', ['created', 'modified', 'deleted']],
    ['assessment_criterion', ['created', 'modified', 'deleted']],
    ['blacklist', ['created', 'modified']],
    ['bonus_config', ['active_from', 'active_to']],
    ['cert', ['created', 'modified']],
    ['certs_generation_queue', ['created', 'modified']],
    ['client', ['created', 'modified']],
    ['content', ['created', 'modified', 'deleted']],
    ['course', ['created', 'modified', 'published_at', 'start_at']],
    ['cover', ['created', 'modified']],
    ['distribute_log', ['created']],
    ['email_queue', ['created']],
    ['enrollment', ['created', 'modified', 'date_from', 'date_to', 'graduated_at']],
    ['feature_flag', ['created', 'modified', 'deleted']],
    ['file', ['created', 'modified', 'deleted']],
    ['forum_complaints', ['created', 'modified']],
    ['forum_likes', ['created', 'modified']],
    ['forum_messages_lock', ['created', 'modified', 'mutex']],
    ['forum_messages', ['created', 'modified', 'edited', 'deleted', 'resolved']],
    ['course_forum_stat', ['stat_date']],
    ['user_forum_stat', ['stat_date']],
    ['forum_subscribe', ['created']],
    ['forum_message_tariff', ['urgent_date', 'outdated_date']],
    ['forum_viewed', ['modified']],
    ['import_info', ['started_at', 'expired_at', 'finished_at']],
    ['import_survey_link', ['created', 'modified']],
    ['lecture_evaluation', ['created']],
    ['lecture_progress', ['created', 'modified', 'deleted', 'viewed', 'ended']],
    ['lecture', ['created', 'modified', 'deleted', 'published_at']],
    ['nomenclature', ['created', 'modified']],
    ['note', ['created', 'modified', 'deleted']],
    ['notification', ['created', 'read_at']],
    ['nps', ['created', 'modified', 'deleted', 'suspended', 'declined', 'completed']],
    // ['payment_correction', ['date']],
    ['persona', ['created', 'modified', 'deleted']],
    ['question', ['created', 'modified', 'deleted']],
    ['read_status', ['created', 'modified']],
    ['section', ['created', 'modified', 'published_at']],
    ['assessment_level_tariff', ['created', 'modified', 'deleted', 'active_from', 'active_to']],
    ['assessment_level', ['created', 'modified', 'deleted']],
    ['source', ['created', 'modified']],
    // ['stored_bonus', ['date']],
    // ['stored_payment', ['date']],
    ['course_statistic', ['stat_date']],
    ['tutor_statistic', ['stat_date']],
    ['submission_tariff', ['urgent_date', 'outdated_date']],
    ['submission', ['created', 'modified', 'deleted', 'draft_at', 'submitted_at', 'graded_at', 'due', 'mutex']],
    ['subscription', ['created', 'modified']],
    ['support_message', ['created', 'updated']],
    ['system_notification', ['created', 'modified']],
    ['topic', ['created', 'modified', 'deleted', 'topic_date_utc', 'topic_date_brazil']],
    ['user_activity', ['modified', 'last_login', 'last_submission_view', 'first_lecture_view']],
    ['banner', ['created', 'modified', 'active_from', 'active_to']],
  ];

  viewOrder = [
    'view_lecture',
    'view_assessment',
    'view_forum_thread',
    'view_module_count',
    'view_assessment_count',
    'view_lecture_count',
    'view_submission',
    'view_submission_count',
    'view_course_count',
    'view_user_module_count',
    'view_user_progress_count',
    'view_user_progress_count_materialized',
  ];

  async getViews(queryRunner: QueryRunner): Promise<any> {
    console.log('Getting views definition...');
    const views = await queryRunner.query('SELECT * from typeorm_metadata');
    if (views.length == this.viewOrder.length) {
      console.log(`View count OK: ${views.length}`);
    } else {
      throw new Error(
        `Invalid views count, something will probably go wrong. Must be: ${this.viewOrder.length}, instead got: ${views.length}`,
      );
    }
    return views;
  }

  async deleteViews(queryRunner: QueryRunner, views: any): Promise<void> {
    // Sorting in order of deletion
    views.sort((a, b) => this.viewOrder.indexOf(b.name) - this.viewOrder.indexOf(a.name));
    console.log('Deleting views');

    for (let i = 0, l = views.length; i < l; i++) {
      const viewName = views[i].name;
      const materialized = views[i].type == 'MATERIALIZED_VIEW' ? 'MATERIALIZED' : '';
      const query = `DROP ${materialized} VIEW "${viewName}"`;
      console.log(query);
      await queryRunner.query(query);
    }

    console.log('Deleting views complete');
  }

  async createViews(queryRunner: QueryRunner, views: any): Promise<void> {
    console.log('Recreating views');

    // Sorting in order of deletion
    views.sort((a, b) => this.viewOrder.indexOf(a.name) - this.viewOrder.indexOf(b.name));

    for (let i = 0, l = views.length; i < l; i++) {
      const viewName = views[i].name;
      const materialized = views[i].type == 'MATERIALIZED_VIEW' ? 'MATERIALIZED' : '';
      const select = views[i].value;
      const query = `CREATE ${materialized} VIEW "${viewName}" AS ${select}`;
      console.log(viewName);
      await queryRunner.query(query);
    }

    console.log('Recreating views complete');
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Migration DatesRefactoring1655765621159 running...');

    const views = await this.getViews(queryRunner);

    await this.deleteViews(queryRunner, views);

    for (let i = 0, l = this.tables.length; i < l; i++) {
      const t = this.tables[i];
      const tableName = t[0];
      const columns = t[1];
      console.log(`[${i + 1}/${l}] ${tableName}`);
      for (let j = 0, k = columns.length; j < k; j++) {
        const column = columns[j];
        const query = `ALTER TABLE "${tableName}" ALTER COLUMN "${column}" TYPE TIMESTAMP WITH TIME ZONE USING "${column}" AT TIME ZONE '${TZ}'`;
        console.log(query);
        await queryRunner.query(query);
      }
    }

    await this.createViews(queryRunner, views);

    console.log('Migration DatesRefactoring1655765621159 done');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    console.log('ROLLBACK Migration DatesRefactoring1655765621159 running...');

    const views = await this.getViews(queryRunner);

    await this.deleteViews(queryRunner, views);

    for (let i = 0, l = this.tables.length; i < l; i++) {
      const t = this.tables[i];
      const tableName = t[0];
      const columns = t[1];
      console.log(`[${i + 1}/${l}] ${tableName}`);
      for (let j = 0, k = columns.length; j < k; j++) {
        const column = columns[j];
        const query = `ALTER TABLE "${tableName}" ALTER COLUMN "${column}" TYPE TIMESTAMP`;
        console.log(query);
        await queryRunner.query(query);
      }
    }

    await this.createViews(queryRunner, views);

    console.log('ROLLBACK Migration DatesRefactoring1655765621159 done');
  }
}
