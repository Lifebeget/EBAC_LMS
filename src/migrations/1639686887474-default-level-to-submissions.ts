import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { Submission } from '@entities/submission.entity';
import { IsNull, MigrationInterface, Not, QueryRunner } from 'typeorm';
import * as dayjs from 'dayjs';
import { DateCalculator } from '@lib/helpers/date';
import { Calendars } from '@lib/helpers/date/calendar';

export class defaultLevelToSubmissions1639686887474 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Migration 1639686887474-default-level-to-submissions running...');
    const tariff = await queryRunner.manager.getRepository(ServiceLevelTariff).findOne({});

    const submissions = await queryRunner.manager.find(Submission, {
      where: { submittedAt: Not(IsNull()) },
    });

    const calculator = new DateCalculator([0, 6], dayjs).setCalendars(new Calendars([], dayjs));

    for (const s of submissions) {
      await queryRunner.manager.insert(SubmissionTariff, {
        submissionId: s.id,
        createTariffId: tariff.id,
        gradeTariffId: tariff.id,
        urgentDate: calculator.addBusinessHours(s.submittedAt, tariff.urgentHours),
        outdatedDate: calculator.addBusinessHours(s.submittedAt, tariff.outdatedHours),
      });
    }

    console.log('Migration done');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.getRepository(SubmissionTariff).delete({});
  }
}
