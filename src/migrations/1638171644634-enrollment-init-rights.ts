import { MigrationInterface, QueryRunner } from 'typeorm';
import { Enrollment } from '@entities/enrollment.entity';

export class enrollmentInitRights1638171644634 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.update(
      Enrollment,
      { role: 'student' },
      {
        rights: {
          canWatchFreeLessons: true,
          canWatchAllLessons: true,
          canDownloadFiles: true,
          canReadForum: true,
          canWriteForum: true,
          canSubmitQuiz: true,
          canSubmitSurvey: true,
          canSubmitAssignment: true,
          canGraduate: true,
        },
      },
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
