import { MigrationInterface, QueryRunner } from 'typeorm';

export class QuizEntities1659625283529 implements MigrationInterface {
  name = 'QuizEntities1659625283529';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "quiz_submission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "status" character varying(40) NOT NULL DEFAULT 'new', "attempt" integer NOT NULL DEFAULT '0', "current_attempt" boolean NOT NULL DEFAULT true, "draft_at" TIMESTAMP WITH TIME ZONE, "submitted_at" TIMESTAMP WITH TIME ZONE, "graded_at" TIMESTAMP WITH TIME ZONE, "success" boolean, "score" integer, "correct_answer_count" integer NOT NULL DEFAULT '0', "incorrect_answer_count" integer NOT NULL DEFAULT '0', "user_id" uuid, "quiz_id" uuid, "course_id" uuid, "lecture_id" uuid, "module_id" uuid, CONSTRAINT "PK_af730e984e8f6f25b5667a5d7be" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_1013e19bb827b96f23a2ac8692" ON "quiz_submission" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_49a996b3dc7d91c94f0615e593" ON "quiz_submission" ("user_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_9dea7f6862d7e967c3d37623c2" ON "quiz_submission" ("status") `);
    await queryRunner.query(`CREATE INDEX "IDX_24327b55bf3b9f6a9ddaad9e2b" ON "quiz_submission" ("submitted_at") `);
    await queryRunner.query(`CREATE INDEX "IDX_3cdafe461c43e6566b11d6fe0c" ON "quiz_submission" ("graded_at") `);
    await queryRunner.query(`CREATE INDEX "IDX_d2a834af8b7ae35d4f7c3eda9a" ON "quiz_submission" ("quiz_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_6b049c944b7b4d1a7aece9ad39" ON "quiz_submission" ("course_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_2344c523b0bc2bd4b7ce9ed900" ON "quiz_submission" ("lecture_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_ad5e9588cdefa1da80ba05bd2f" ON "quiz_submission" ("module_id") `);
    await queryRunner.query(
      `CREATE TABLE "quiz" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "title" character varying NOT NULL, "description" text, "current_version" boolean NOT NULL DEFAULT true, "common_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "score" integer NOT NULL DEFAULT '100', "passing_score" integer NOT NULL DEFAULT '70', "time_limit_seconds" integer NOT NULL DEFAULT '0', "time_seconds" integer NOT NULL DEFAULT '0', "max_attempts" integer NOT NULL DEFAULT '3', "time_delayto_retest_hours" integer NOT NULL DEFAULT '24', "show_result" integer NOT NULL DEFAULT '1', "show_correct_answers" integer NOT NULL DEFAULT '1', "can_change_answer" boolean NOT NULL DEFAULT true, "shuffle_questions" boolean NOT NULL DEFAULT true, "shuffle_answers" boolean NOT NULL DEFAULT true, "type" character varying NOT NULL DEFAULT 'default', "count_limit" integer NOT NULL DEFAULT '0', "lecture_id" uuid, CONSTRAINT "PK_422d974e7217414e029b3e641d0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_4c092dd4f7268f548eae617b23" ON "quiz" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_faaf9ae694133cbc3de0a55b6f" ON "quiz" ("passing_score") `);
    await queryRunner.query(`CREATE INDEX "IDX_545f0069c450b4b9164e3b9374" ON "quiz" ("type") `);
    await queryRunner.query(
      `CREATE TABLE "quiz_question" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "sort" integer NOT NULL DEFAULT '500', "title" character varying NOT NULL, "description" text NOT NULL, "type" character varying(80) NOT NULL, "text_mode" character varying(80), "text_mode_min" integer NOT NULL DEFAULT '0', "text_mode_max" integer NOT NULL DEFAULT '0', "is_required" boolean NOT NULL DEFAULT true, "gradable" boolean NOT NULL DEFAULT true, "data" jsonb, "feedback_common" text NOT NULL DEFAULT '', "feedback_correct" text NOT NULL DEFAULT '', "feedback_incorrect" text NOT NULL DEFAULT '', "quiz_id" uuid, CONSTRAINT "PK_0bab74c2a71b9b3f8a941104083" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_8e68a1cb15626c580e2847d053" ON "quiz_question" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_77e8e87d9e707fabdb82bf227f" ON "quiz_question" ("quiz_id") `);
    await queryRunner.query(
      `CREATE TABLE "quiz_answer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "comment" text, "question_id" uuid, "data" jsonb, "submission_id" uuid, CONSTRAINT "PK_926d49bc4559c8200b6c6c2c22f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_55ad494081be7f81b56ba9dd1a" ON "quiz_answer" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_608c6b43835cc75367fcbc501c" ON "quiz_answer" ("submission_id") `);
    await queryRunner.query(
      `ALTER TABLE "quiz_submission" ADD CONSTRAINT "FK_49a996b3dc7d91c94f0615e5935" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_submission" ADD CONSTRAINT "FK_d2a834af8b7ae35d4f7c3eda9af" FOREIGN KEY ("quiz_id") REFERENCES "quiz"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_submission" ADD CONSTRAINT "FK_6b049c944b7b4d1a7aece9ad39d" FOREIGN KEY ("course_id") REFERENCES "course"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_submission" ADD CONSTRAINT "FK_2344c523b0bc2bd4b7ce9ed9004" FOREIGN KEY ("lecture_id") REFERENCES "lecture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_submission" ADD CONSTRAINT "FK_ad5e9588cdefa1da80ba05bd2f9" FOREIGN KEY ("module_id") REFERENCES "module"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz" ADD CONSTRAINT "FK_4fb61d12cd2b38ee03a4d299821" FOREIGN KEY ("lecture_id") REFERENCES "lecture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_question" ADD CONSTRAINT "FK_77e8e87d9e707fabdb82bf227fc" FOREIGN KEY ("quiz_id") REFERENCES "quiz"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_answer" ADD CONSTRAINT "FK_a0c7f7e7800ddf6d12a16aaf67f" FOREIGN KEY ("question_id") REFERENCES "quiz_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "quiz_answer" ADD CONSTRAINT "FK_608c6b43835cc75367fcbc501c7" FOREIGN KEY ("submission_id") REFERENCES "quiz_submission"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "quiz_answer" DROP CONSTRAINT "FK_608c6b43835cc75367fcbc501c7"`);
    await queryRunner.query(`ALTER TABLE "quiz_answer" DROP CONSTRAINT "FK_a0c7f7e7800ddf6d12a16aaf67f"`);
    await queryRunner.query(`ALTER TABLE "quiz_question" DROP CONSTRAINT "FK_77e8e87d9e707fabdb82bf227fc"`);
    await queryRunner.query(`ALTER TABLE "quiz" DROP CONSTRAINT "FK_4fb61d12cd2b38ee03a4d299821"`);
    await queryRunner.query(`ALTER TABLE "quiz_submission" DROP CONSTRAINT "FK_ad5e9588cdefa1da80ba05bd2f9"`);
    await queryRunner.query(`ALTER TABLE "quiz_submission" DROP CONSTRAINT "FK_2344c523b0bc2bd4b7ce9ed9004"`);
    await queryRunner.query(`ALTER TABLE "quiz_submission" DROP CONSTRAINT "FK_6b049c944b7b4d1a7aece9ad39d"`);
    await queryRunner.query(`ALTER TABLE "quiz_submission" DROP CONSTRAINT "FK_d2a834af8b7ae35d4f7c3eda9af"`);
    await queryRunner.query(`ALTER TABLE "quiz_submission" DROP CONSTRAINT "FK_49a996b3dc7d91c94f0615e5935"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_608c6b43835cc75367fcbc501c"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_55ad494081be7f81b56ba9dd1a"`);
    await queryRunner.query(`DROP TABLE "quiz_answer"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_77e8e87d9e707fabdb82bf227f"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_8e68a1cb15626c580e2847d053"`);
    await queryRunner.query(`DROP TABLE "quiz_question"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_545f0069c450b4b9164e3b9374"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_faaf9ae694133cbc3de0a55b6f"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_4c092dd4f7268f548eae617b23"`);
    await queryRunner.query(`DROP TABLE "quiz"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_ad5e9588cdefa1da80ba05bd2f"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_2344c523b0bc2bd4b7ce9ed900"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_6b049c944b7b4d1a7aece9ad39"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d2a834af8b7ae35d4f7c3eda9a"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_3cdafe461c43e6566b11d6fe0c"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_24327b55bf3b9f6a9ddaad9e2b"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_9dea7f6862d7e967c3d37623c2"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_49a996b3dc7d91c94f0615e593"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_1013e19bb827b96f23a2ac8692"`);
    await queryRunner.query(`DROP TABLE "quiz_submission"`);
  }
}
