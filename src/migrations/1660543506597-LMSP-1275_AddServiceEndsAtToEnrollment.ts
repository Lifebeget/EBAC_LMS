import { MigrationInterface, QueryRunner } from 'typeorm';

export class LMSP1275AddServiceEndsAtToEnrollment1660543506597 implements MigrationInterface {
  name = 'LMSP1275AddServiceEndsAtToEnrollment1660543506597';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "enrollment" ADD "service_ends_at" TIMESTAMP WITH TIME ZONE`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "enrollment" DROP COLUMN "service_ends_at"`);
  }

}
