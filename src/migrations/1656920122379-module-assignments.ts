import { MigrationInterface, QueryRunner } from 'typeorm';

export class moduleAssignments1656920122379 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            INSERT INTO module_assignment (exclusive, user_id, module_id) SELECT true as exclusive, user_id, module_id FROM user_modules_module
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
