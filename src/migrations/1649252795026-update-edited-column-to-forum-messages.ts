import { MigrationInterface, QueryRunner } from 'typeorm';
// import {ForumMessages} from '@entities/forum-messages.entity';

export class updateEditedColumnToForumMessages1649252795026 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // обновляет поле modified, так не надо, поэтому ниже обычный квери
    /*await queryRunner.manager
      .getRepository(ForumMessages)
      .createQueryBuilder()
      .update()
      .set({ edited: () => `modified` })
      .where(`created <> modified`)
      .execute();*/

    await queryRunner.manager.query(`UPDATE "public"."forum_messages"
                                     SET "edited" = "modified"
                                     WHERE "created" <> "modified"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.query(`UPDATE "public"."forum_messages"
                                     SET "edited" = NULL`);
  }
}
