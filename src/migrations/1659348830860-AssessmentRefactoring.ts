import { MigrationInterface, QueryRunner } from 'typeorm';

export class AssessmentRefactoring1659348830860 implements MigrationInterface {
  name = 'AssessmentRefactoring1659348830860';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, [
      'MATERIALIZED_VIEW',
      'public',
      'view_user_progress_count_materialized',
    ]);
    await queryRunner.query(`DROP MATERIALIZED VIEW "view_user_progress_count_materialized"`);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, [
      'VIEW',
      'public',
      'view_user_progress_count',
    ]);
    await queryRunner.query(`DROP VIEW "view_user_progress_count"`);
    await queryRunner.query(`CREATE TYPE "public"."homework_criterion_category_display_enum" AS ENUM('toggle', 'toggleWithNeutral')`);
    await queryRunner.query(
      `CREATE TABLE "homework_criterion_category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "title" character varying(128) NOT NULL, "description" text, "default_weight" integer NOT NULL DEFAULT '1', "display" "public"."homework_criterion_category_display_enum" NOT NULL DEFAULT 'toggle', "sort" integer NOT NULL DEFAULT '500', "gradable" boolean NOT NULL DEFAULT true, CONSTRAINT "UQ_26d0810bb6b3096a07af56ab5d1" UNIQUE ("title"), CONSTRAINT "PK_8f744d29b089f5a42d4b7886173" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "homework_submission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "status" character varying(40) NOT NULL DEFAULT 'new', "attempt" integer NOT NULL DEFAULT '0', "current_attempt" boolean NOT NULL DEFAULT true, "draft_at" TIMESTAMP WITH TIME ZONE, "submitted_at" TIMESTAMP WITH TIME ZONE, "graded_at" TIMESTAMP WITH TIME ZONE, "success" boolean, "score" integer, "mutex" TIMESTAMP WITH TIME ZONE, "is_mutex_auto" boolean NOT NULL DEFAULT false, "bind_tutor" boolean NOT NULL DEFAULT false, "is_question" boolean NOT NULL DEFAULT false, "user_id" uuid, "tutor_id" uuid, "homework_id" uuid, "course_id" uuid, "lecture_id" uuid, "module_id" uuid, CONSTRAINT "PK_77748553ef7235189268bfa5e3b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_b720c44f8870b691162798bacb" ON "homework_submission" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_71ddd64c04965d8aeb62fbac3e" ON "homework_submission" ("user_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_549118807cc62a7c1d59330ea3" ON "homework_submission" ("status") `);
    await queryRunner.query(`CREATE INDEX "IDX_0161bc260a09346af70a31ef09" ON "homework_submission" ("submitted_at") `);
    await queryRunner.query(`CREATE INDEX "IDX_247bc0b8c17b54fd4032c51b43" ON "homework_submission" ("graded_at") `);
    await queryRunner.query(`CREATE INDEX "IDX_d1761b69e88ad8890da047d05e" ON "homework_submission" ("is_question") `);
    await queryRunner.query(`CREATE INDEX "IDX_88d3838d1296f5bec41649e847" ON "homework_submission" ("tutor_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_ea42144388e4f52a623cb9dded" ON "homework_submission" ("homework_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_98b0c19b06ec6c00c8fcab0e29" ON "homework_submission" ("course_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_800b4eb3e4c7f6232189dded4e" ON "homework_submission" ("lecture_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_749fce831de2978043f82bbeda" ON "homework_submission" ("module_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "title" character varying NOT NULL, "description" text, "current_version" boolean NOT NULL DEFAULT true, "common_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "tutor_message" text, "score" integer NOT NULL DEFAULT '100', "passing_score" integer NOT NULL DEFAULT '70', "grading_type" character varying(80) NOT NULL DEFAULT 'overall_should_pass', "level_id" uuid, "lecture_id" uuid, CONSTRAINT "PK_90dbf463ef94040ed137c4fd38d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_d95a66bec1f67e42b4782f0049" ON "homework" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_15507d96dbfe486c5183c173a3" ON "homework" ("passing_score") `);
    await queryRunner.query(`CREATE INDEX "IDX_0e2e8edf74302f531a45075042" ON "homework" ("level_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework_question" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "sort" integer NOT NULL DEFAULT '500', "title" character varying NOT NULL, "description" text NOT NULL, "type" character varying(80) NOT NULL, "text_mode" character varying(80), "text_mode_min" integer NOT NULL DEFAULT '0', "text_mode_max" integer NOT NULL DEFAULT '0', "is_required" boolean NOT NULL DEFAULT true, "gradable" boolean NOT NULL DEFAULT true, "score" integer NOT NULL DEFAULT '100', "passing_score" integer NOT NULL DEFAULT '70', "allow_multiple_files" boolean NOT NULL DEFAULT true, "allowed_file_types" character varying(20) NOT NULL DEFAULT 'images', "max_files" integer NOT NULL DEFAULT '10', "homework_id" uuid, CONSTRAINT "PK_21a7299281ba209a82e9f16d586" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_540d84fc2376ac4904282184e8" ON "homework_question" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_015e4983fbf2ef1506f82c4123" ON "homework_question" ("homework_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework_criterion" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "category_id" uuid NOT NULL, "title" text NOT NULL, "weight" integer NOT NULL DEFAULT '1', "author_id" uuid, "sort" integer NOT NULL DEFAULT '500', "question_id" uuid NOT NULL, CONSTRAINT "PK_03346473041e85a2584a20c9277" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_8f744d29b089f5a42d4b788617" ON "homework_criterion" ("category_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_63be1eef58e6d7d4eda35bffbb" ON "homework_criterion" ("author_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_a9b5c09935de1e8d686624f559" ON "homework_criterion" ("question_id") `);
    await queryRunner.query(`CREATE TYPE "public"."homework_criterion_entry_state_enum" AS ENUM('yes', 'no')`);
    await queryRunner.query(
      `CREATE TABLE "homework_criterion_entry" ("state" "public"."homework_criterion_entry_state_enum" NOT NULL, "answer_id" uuid NOT NULL, "criteria_id" uuid NOT NULL, CONSTRAINT "UNIQUE_HOMEWORK_ANSWER_AND_HOMEWORK_CRITERION" UNIQUE ("answer_id", "criteria_id"), CONSTRAINT "PK_77762ca69b48ebe813baed1157f" PRIMARY KEY ("answer_id", "criteria_id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "homework_answer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT true, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted" TIMESTAMP WITH TIME ZONE, "external_id" character varying, "comment" text, "score" integer NOT NULL DEFAULT '0', "tutor_comment" text, "submission_id" uuid, "question_id" uuid, CONSTRAINT "PK_2c4c5fab20ca57ee5ba34738144" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_d371b2e3d2ca56760fc83d7d13" ON "homework_answer" ("external_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_b8cb17eff0c3a1b053868259ea" ON "homework_answer" ("submission_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework_question_files_file" ("homework_question_id" uuid NOT NULL, "file_id" uuid NOT NULL, CONSTRAINT "PK_b05a3e570b363c1b49aa50de49e" PRIMARY KEY ("homework_question_id", "file_id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_20aa4695af59eab4a555280919" ON "homework_question_files_file" ("homework_question_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_91ac3e1e592e225aab131b83a4" ON "homework_question_files_file" ("file_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework_answer_files_file" ("homework_answer_id" uuid NOT NULL, "file_id" uuid NOT NULL, CONSTRAINT "PK_38c5c62d48dcfd64f0780f2221b" PRIMARY KEY ("homework_answer_id", "file_id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_be5f2c9001af4873e950e3b6f2" ON "homework_answer_files_file" ("homework_answer_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_29583572c7fa9a127d8b9d9202" ON "homework_answer_files_file" ("file_id") `);
    await queryRunner.query(
      `CREATE TABLE "homework_answer_tutor_files_file" ("homework_answer_id" uuid NOT NULL, "file_id" uuid NOT NULL, CONSTRAINT "PK_8dada7d66ea563a3c8edb663a33" PRIMARY KEY ("homework_answer_id", "file_id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_744e454a72f238f8f0fc0fafaf" ON "homework_answer_tutor_files_file" ("homework_answer_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_4e9842c2909304bc0986d82771" ON "homework_answer_tutor_files_file" ("file_id") `);
    await queryRunner.query(`ALTER TABLE "answer" ADD "active" boolean NOT NULL DEFAULT true`);
    await queryRunner.query(`ALTER TABLE "answer" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "submission" ADD "active" boolean NOT NULL DEFAULT true`);
    await queryRunner.query(`ALTER TABLE "question" ADD "active" boolean NOT NULL DEFAULT true`);
    await queryRunner.query(`ALTER TABLE "assessment" ADD "active" boolean NOT NULL DEFAULT true`);
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_71ddd64c04965d8aeb62fbac3ea" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_88d3838d1296f5bec41649e8478" FOREIGN KEY ("tutor_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_ea42144388e4f52a623cb9ddedd" FOREIGN KEY ("homework_id") REFERENCES "homework"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_98b0c19b06ec6c00c8fcab0e29c" FOREIGN KEY ("course_id") REFERENCES "course"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_800b4eb3e4c7f6232189dded4e6" FOREIGN KEY ("lecture_id") REFERENCES "lecture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_submission" ADD CONSTRAINT "FK_749fce831de2978043f82bbeda0" FOREIGN KEY ("module_id") REFERENCES "module"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework" ADD CONSTRAINT "FK_0e2e8edf74302f531a45075042c" FOREIGN KEY ("level_id") REFERENCES "assessment_level"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework" ADD CONSTRAINT "FK_48d20d43d3671a56d28b8ec59d5" FOREIGN KEY ("lecture_id") REFERENCES "lecture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_question" ADD CONSTRAINT "FK_015e4983fbf2ef1506f82c4123c" FOREIGN KEY ("homework_id") REFERENCES "homework"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_criterion" ADD CONSTRAINT "FK_63be1eef58e6d7d4eda35bffbbd" FOREIGN KEY ("author_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_criterion" ADD CONSTRAINT "FK_8f744d29b089f5a42d4b7886173" FOREIGN KEY ("category_id") REFERENCES "homework_criterion_category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_criterion" ADD CONSTRAINT "FK_a9b5c09935de1e8d686624f5599" FOREIGN KEY ("question_id") REFERENCES "homework_question"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_criterion_entry" ADD CONSTRAINT "FK_87a973d67a7106558a6398fd91b" FOREIGN KEY ("answer_id") REFERENCES "homework_answer"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_criterion_entry" ADD CONSTRAINT "FK_6a1923f32e6e82c51f3d35227b1" FOREIGN KEY ("criteria_id") REFERENCES "homework_criterion"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer" ADD CONSTRAINT "FK_b8cb17eff0c3a1b053868259ea4" FOREIGN KEY ("submission_id") REFERENCES "homework_submission"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer" ADD CONSTRAINT "FK_a3a9c8ca2dd7802688dc2b6cd7e" FOREIGN KEY ("question_id") REFERENCES "homework_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_question_files_file" ADD CONSTRAINT "FK_20aa4695af59eab4a555280919b" FOREIGN KEY ("homework_question_id") REFERENCES "homework_question"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_question_files_file" ADD CONSTRAINT "FK_91ac3e1e592e225aab131b83a4b" FOREIGN KEY ("file_id") REFERENCES "file"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer_files_file" ADD CONSTRAINT "FK_be5f2c9001af4873e950e3b6f2d" FOREIGN KEY ("homework_answer_id") REFERENCES "homework_answer"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer_files_file" ADD CONSTRAINT "FK_29583572c7fa9a127d8b9d9202b" FOREIGN KEY ("file_id") REFERENCES "file"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer_tutor_files_file" ADD CONSTRAINT "FK_744e454a72f238f8f0fc0fafafd" FOREIGN KEY ("homework_answer_id") REFERENCES "homework_answer"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "homework_answer_tutor_files_file" ADD CONSTRAINT "FK_4e9842c2909304bc0986d82771c" FOREIGN KEY ("file_id") REFERENCES "file"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(`CREATE VIEW "view_user_progress_count" AS 
    select
      e.user_id,
      e.course_id,
      vcc.status as course_status,
      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,
      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,
      count(1) filter (
        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required
      ) as modules_required_completed,
      count(1) filter (
        where vumc.lectures_completed >= vumc.lectures_total
      ) as modules_completed,
      vcc.lectures as lectures_total,
      vcc.lectures_required as lectures_required_total,
      vcc.modules as modules_total,
      vcc.modules_required as modules_required_total,
      vcc.homeworks as homeworks_total,
      vcc.homeworks_required as homeworks_required_total,
      vcc.quizes as quizes_total,
      vcc.quizes_required as quizes_required_total,
      vcc.surveys as surveys_total,
      vcc.surveys_required as surveys_required_total,
      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,
      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,
      coalesce(vsc.quizes_completed, 0) as quizes_completed,
      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,
      coalesce(vsc.surveys_completed, 0) as surveys_completed,
      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,
      case when vcc.lectures_required > 0 then
        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)
      else 0 end as progress,
      coalesce(sum(vumc.lectures_required_completed), 0) = vcc.lectures_required and vcc.status = 'completed' as graduated
    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e
    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id
    left join view_course_count vcc on vcc.id = e.course_id
    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id
    group by e.user_id, e.course_id,
      vcc.lectures, vcc.lectures_required,
      vcc.modules, vcc.modules_required,
      vcc.homeworks, vcc.homeworks_required,
      vcc.quizes, vcc.quizes_required,
      vcc.surveys, vcc.surveys_required,
      vsc.homeworks_completed, vsc.homeworks_completed_required,
      vsc.quizes_completed, vsc.quizes_completed_required,
      vsc.surveys_completed, vsc.surveys_completed_required,
      vcc.status
  `);
    await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, [
      'VIEW',
      'public',
      'view_user_progress_count',
      "select\n      e.user_id,\n      e.course_id,\n      vcc.status as course_status,\n      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,\n      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,\n      count(1) filter (\n        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required\n      ) as modules_required_completed,\n      count(1) filter (\n        where vumc.lectures_completed >= vumc.lectures_total\n      ) as modules_completed,\n      vcc.lectures as lectures_total,\n      vcc.lectures_required as lectures_required_total,\n      vcc.modules as modules_total,\n      vcc.modules_required as modules_required_total,\n      vcc.homeworks as homeworks_total,\n      vcc.homeworks_required as homeworks_required_total,\n      vcc.quizes as quizes_total,\n      vcc.quizes_required as quizes_required_total,\n      vcc.surveys as surveys_total,\n      vcc.surveys_required as surveys_required_total,\n      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,\n      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,\n      coalesce(vsc.quizes_completed, 0) as quizes_completed,\n      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,\n      coalesce(vsc.surveys_completed, 0) as surveys_completed,\n      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,\n      case when vcc.lectures_required > 0 then\n        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)\n      else 0 end as progress,\n      coalesce(sum(vumc.lectures_required_completed), 0) = vcc.lectures_required and vcc.status = 'completed' as graduated\n    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e\n    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id\n    left join view_course_count vcc on vcc.id = e.course_id\n    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id\n    group by e.user_id, e.course_id,\n      vcc.lectures, vcc.lectures_required,\n      vcc.modules, vcc.modules_required,\n      vcc.homeworks, vcc.homeworks_required,\n      vcc.quizes, vcc.quizes_required,\n      vcc.surveys, vcc.surveys_required,\n      vsc.homeworks_completed, vsc.homeworks_completed_required,\n      vsc.quizes_completed, vsc.quizes_completed_required,\n      vsc.surveys_completed, vsc.surveys_completed_required,\n      vcc.status",
    ]);
    await queryRunner.query(`CREATE MATERIALIZED VIEW "view_user_progress_count_materialized" AS 
    select * from view_user_progress_count
  `);
    await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, [
      'MATERIALIZED_VIEW',
      'public',
      'view_user_progress_count_materialized',
      'select * from view_user_progress_count',
    ]);
    await queryRunner.query(`DROP INDEX "public"."IDX_c077fef479e5307ce8362d16bd"`);
    await queryRunner.query(`ALTER TABLE "answer" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "question" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "homework_question" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "homework_answer" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "assessment_criterion_entry" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "homework_criterion_entry" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`CREATE INDEX "IDX_ab2b317501aaf10c047a12683a" ON "assessment_criterion_category" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_8802ec7140705862107573eef9" ON "assessment_criterion" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_acb6e7759d55bce78905ac5937" ON "assessment_criterion_entry" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_e55f10ca58a02e15df06f12142" ON "homework_criterion_category" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_915ca61fd2f5211d5b676cda92" ON "homework_submission" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_d656d86c3460a650f9091241b0" ON "homework" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_3201bafac9d592eb1fbe36cbd0" ON "homework_criterion" ("deleted") `);
    await queryRunner.query(`CREATE INDEX "IDX_67199afc402526f4f21b07f3fb" ON "homework_criterion_entry" ("deleted") `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, [
      'MATERIALIZED_VIEW',
      'public',
      'view_user_progress_count_materialized',
    ]);
    await queryRunner.query(`DROP MATERIALIZED VIEW "view_user_progress_count_materialized"`);
    await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, [
      'VIEW',
      'public',
      'view_user_progress_count',
    ]);
    await queryRunner.query(`DROP VIEW "view_user_progress_count"`);
    await queryRunner.query(`ALTER TABLE "homework_answer_tutor_files_file" DROP CONSTRAINT "FK_4e9842c2909304bc0986d82771c"`);
    await queryRunner.query(`ALTER TABLE "homework_answer_tutor_files_file" DROP CONSTRAINT "FK_744e454a72f238f8f0fc0fafafd"`);
    await queryRunner.query(`ALTER TABLE "homework_answer_files_file" DROP CONSTRAINT "FK_29583572c7fa9a127d8b9d9202b"`);
    await queryRunner.query(`ALTER TABLE "homework_answer_files_file" DROP CONSTRAINT "FK_be5f2c9001af4873e950e3b6f2d"`);
    await queryRunner.query(`ALTER TABLE "homework_question_files_file" DROP CONSTRAINT "FK_91ac3e1e592e225aab131b83a4b"`);
    await queryRunner.query(`ALTER TABLE "homework_question_files_file" DROP CONSTRAINT "FK_20aa4695af59eab4a555280919b"`);
    await queryRunner.query(`ALTER TABLE "homework_answer" DROP CONSTRAINT "FK_a3a9c8ca2dd7802688dc2b6cd7e"`);
    await queryRunner.query(`ALTER TABLE "homework_answer" DROP CONSTRAINT "FK_b8cb17eff0c3a1b053868259ea4"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion_entry" DROP CONSTRAINT "FK_6a1923f32e6e82c51f3d35227b1"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion_entry" DROP CONSTRAINT "FK_87a973d67a7106558a6398fd91b"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion" DROP CONSTRAINT "FK_a9b5c09935de1e8d686624f5599"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion" DROP CONSTRAINT "FK_8f744d29b089f5a42d4b7886173"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion" DROP CONSTRAINT "FK_63be1eef58e6d7d4eda35bffbbd"`);
    await queryRunner.query(`ALTER TABLE "homework_question" DROP CONSTRAINT "FK_015e4983fbf2ef1506f82c4123c"`);
    await queryRunner.query(`ALTER TABLE "homework" DROP CONSTRAINT "FK_48d20d43d3671a56d28b8ec59d5"`);
    await queryRunner.query(`ALTER TABLE "homework" DROP CONSTRAINT "FK_0e2e8edf74302f531a45075042c"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_749fce831de2978043f82bbeda0"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_800b4eb3e4c7f6232189dded4e6"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_98b0c19b06ec6c00c8fcab0e29c"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_ea42144388e4f52a623cb9ddedd"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_88d3838d1296f5bec41649e8478"`);
    await queryRunner.query(`ALTER TABLE "homework_submission" DROP CONSTRAINT "FK_71ddd64c04965d8aeb62fbac3ea"`);
    await queryRunner.query(`ALTER TABLE "assessment" DROP COLUMN "active"`);
    await queryRunner.query(`ALTER TABLE "question" DROP COLUMN "active"`);
    await queryRunner.query(`ALTER TABLE "submission" DROP COLUMN "active"`);
    await queryRunner.query(`ALTER TABLE "answer" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "answer" DROP COLUMN "active"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_4e9842c2909304bc0986d82771"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_744e454a72f238f8f0fc0fafaf"`);
    await queryRunner.query(`DROP TABLE "homework_answer_tutor_files_file"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_29583572c7fa9a127d8b9d9202"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_be5f2c9001af4873e950e3b6f2"`);
    await queryRunner.query(`DROP TABLE "homework_answer_files_file"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_91ac3e1e592e225aab131b83a4"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_20aa4695af59eab4a555280919"`);
    await queryRunner.query(`DROP TABLE "homework_question_files_file"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_b8cb17eff0c3a1b053868259ea"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d371b2e3d2ca56760fc83d7d13"`);
    await queryRunner.query(`DROP TABLE "homework_answer"`);
    await queryRunner.query(`DROP TABLE "homework_criterion_entry"`);
    await queryRunner.query(`DROP TYPE "public"."homework_criterion_entry_state_enum"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_a9b5c09935de1e8d686624f559"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_63be1eef58e6d7d4eda35bffbb"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_8f744d29b089f5a42d4b788617"`);
    await queryRunner.query(`DROP TABLE "homework_criterion"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_015e4983fbf2ef1506f82c4123"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_540d84fc2376ac4904282184e8"`);
    await queryRunner.query(`DROP TABLE "homework_question"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_0e2e8edf74302f531a45075042"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_15507d96dbfe486c5183c173a3"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d95a66bec1f67e42b4782f0049"`);
    await queryRunner.query(`DROP TABLE "homework"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_749fce831de2978043f82bbeda"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_800b4eb3e4c7f6232189dded4e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_98b0c19b06ec6c00c8fcab0e29"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_ea42144388e4f52a623cb9dded"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_88d3838d1296f5bec41649e847"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d1761b69e88ad8890da047d05e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_247bc0b8c17b54fd4032c51b43"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_0161bc260a09346af70a31ef09"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_549118807cc62a7c1d59330ea3"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_71ddd64c04965d8aeb62fbac3e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_b720c44f8870b691162798bacb"`);
    await queryRunner.query(`DROP TABLE "homework_submission"`);
    await queryRunner.query(`DROP TABLE "homework_criterion_category"`);
    await queryRunner.query(`DROP TYPE "public"."homework_criterion_category_display_enum"`);
    await queryRunner.query(`CREATE VIEW "view_user_progress_count" AS select
      e.user_id,
      e.course_id,
      vcc.status as course_status,
      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,
      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,
      count(1) filter (
        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required
      ) as modules_required_completed,
      count(1) filter (
        where vumc.lectures_completed >= vumc.lectures_total
      ) as modules_completed,
      vcc.lectures as lectures_total,
      vcc.lectures_required as lectures_required_total,
      vcc.modules as modules_total,
      vcc.modules_required as modules_required_total,
      vcc.homeworks as homeworks_total,
      vcc.homeworks_required as homeworks_required_total,
      vcc.quizes as quizes_total,
      vcc.quizes_required as quizes_required_total,
      vcc.surveys as surveys_total,
      vcc.surveys_required as surveys_required_total,
      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,
      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,
      coalesce(vsc.quizes_completed, 0) as quizes_completed,
      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,
      coalesce(vsc.surveys_completed, 0) as surveys_completed,
      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,
      case when vcc.lectures_required > 0 then
        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)
      else 0 end as progress,
      CASE
        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric
        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric
      END AS graduated
    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e
    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id
    left join view_course_count vcc on vcc.id = e.course_id
    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id
    group by e.user_id, e.course_id,
      vcc.lectures, vcc.lectures_required,
      vcc.modules, vcc.modules_required,
      vcc.homeworks, vcc.homeworks_required,
      vcc.quizes, vcc.quizes_required,
      vcc.surveys, vcc.surveys_required,
      vsc.homeworks_completed, vsc.homeworks_completed_required,
      vsc.quizes_completed, vsc.quizes_completed_required,
      vsc.surveys_completed, vsc.surveys_completed_required,
      vcc.status`);
    await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, [
      'VIEW',
      'public',
      'view_user_progress_count',
      "select\n      e.user_id,\n      e.course_id,\n      vcc.status as course_status,\n      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,\n      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,\n      count(1) filter (\n        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required\n      ) as modules_required_completed,\n      count(1) filter (\n        where vumc.lectures_completed >= vumc.lectures_total\n      ) as modules_completed,\n      vcc.lectures as lectures_total,\n      vcc.lectures_required as lectures_required_total,\n      vcc.modules as modules_total,\n      vcc.modules_required as modules_required_total,\n      vcc.homeworks as homeworks_total,\n      vcc.homeworks_required as homeworks_required_total,\n      vcc.quizes as quizes_total,\n      vcc.quizes_required as quizes_required_total,\n      vcc.surveys as surveys_total,\n      vcc.surveys_required as surveys_required_total,\n      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,\n      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,\n      coalesce(vsc.quizes_completed, 0) as quizes_completed,\n      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,\n      coalesce(vsc.surveys_completed, 0) as surveys_completed,\n      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,\n      case when vcc.lectures_required > 0 then\n        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)\n      else 0 end as progress,\n      CASE\n        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric\n        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric\n      END AS graduated\n    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e\n    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id\n    left join view_course_count vcc on vcc.id = e.course_id\n    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id\n    group by e.user_id, e.course_id,\n      vcc.lectures, vcc.lectures_required,\n      vcc.modules, vcc.modules_required,\n      vcc.homeworks, vcc.homeworks_required,\n      vcc.quizes, vcc.quizes_required,\n      vcc.surveys, vcc.surveys_required,\n      vsc.homeworks_completed, vsc.homeworks_completed_required,\n      vsc.quizes_completed, vsc.quizes_completed_required,\n      vsc.surveys_completed, vsc.surveys_completed_required,\n      vcc.status",
    ]);
    await queryRunner.query(`CREATE MATERIALIZED VIEW "view_user_progress_count_materialized" AS select * from view_user_progress_count`);
    await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, [
      'MATERIALIZED_VIEW',
      'public',
      'view_user_progress_count_materialized',
      'select * from view_user_progress_count',
    ]);
    await queryRunner.query(`DROP INDEX "public"."IDX_67199afc402526f4f21b07f3fb"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_3201bafac9d592eb1fbe36cbd0"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d656d86c3460a650f9091241b0"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_915ca61fd2f5211d5b676cda92"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_e55f10ca58a02e15df06f12142"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_acb6e7759d55bce78905ac5937"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_8802ec7140705862107573eef9"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_ab2b317501aaf10c047a12683a"`);
    await queryRunner.query(`ALTER TABLE "homework_criterion_entry" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "assessment_criterion_entry" DROP COLUMN "deleted"`);
    await queryRunner.query(`ALTER TABLE "homework_answer" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "homework_question" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "question" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "answer" ADD "deleted" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`CREATE INDEX "IDX_c077fef479e5307ce8362d16bd" ON "question" ("deleted") `);
  }
}
