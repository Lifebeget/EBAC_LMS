import { BonusConfig } from '@entities/bonus-config.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class defaultBonusConfig1649915191729 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    let bonusConfig: BonusConfig = {
      activeFrom: new Date(0),
      activeTo: new Date('2022-03-31'),
      checkpoints: [0, 4],
      bonusPercent: 10,
    };

    await queryRunner.manager.getRepository(BonusConfig).insert(bonusConfig);

    bonusConfig = {
      activeFrom: new Date('2022-04-01'),
      activeTo: new Date('9999-12-30'),
      checkpoints: [1, 4],
      bonusPercent: 10,
    };

    await queryRunner.manager.getRepository(BonusConfig).insert(bonusConfig);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
