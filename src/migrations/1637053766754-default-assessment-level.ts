import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { ServiceLevel } from '@entities/service-level.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { Assessment } from '@entities/assessment.entity';

const defaultTariff = {
  activeFrom: '1970-01-01',
  activeTo: '9999-12-31',
  urgentHours: 24,
  outdatedHours: 72,
  value: 15,
};

export class defaultServiceLevel1637053766754 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const levelsCount = await queryRunner.manager.count(ServiceLevel);

    if (levelsCount > 0) {
      return;
    }

    const result = await queryRunner.manager.insert(ServiceLevel, {
      title: 'Normal',
      isDefault: true,
    });

    const levelId = result.identifiers[0].id;

    await queryRunner.manager.insert(ServiceLevelTariff, {
      ...defaultTariff,
      levelId,
    });

    await queryRunner.manager.update(Assessment, {}, { levelId });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.update(Assessment, {}, { levelId: null });
    await queryRunner.manager.delete(ServiceLevelTariff, {});
    await queryRunner.manager.delete(ServiceLevel, {});
  }
}
