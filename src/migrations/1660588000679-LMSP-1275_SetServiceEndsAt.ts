import { MigrationInterface, QueryRunner } from 'typeorm';
import config from '../../config';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';

export class LMSP1275SetServiceEndsAt1660588000679 implements MigrationInterface {
  name = 'LMSP1275SetServiceEndsAt1660588000679';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`UPDATE "enrollment" AS e
     SET service_ends_at = CASE
           WHEN e.type = '${EnrollmentType.Satellite}' THEN LEAST(e.date_from, e.created) + INTERVAL '${config.defaultSatelliteCourseSupportDuration} DAY'
           WHEN e.type = '${EnrollmentType.Trial}' THEN e.date_to
           ELSE GREATEST(LEAST(e.date_from, e.created) + INTERVAL '${config.defaultCourseSupportDuration} DAY','2023-01-01')
       END
     WHERE e.service_ends_at IS NULL AND e.role = '${CourseRole.Student}';`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`UPDATE "enrollment" SET service_ends_at = NULL`);
  }
}
