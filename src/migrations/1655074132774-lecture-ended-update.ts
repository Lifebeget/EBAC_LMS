import { MigrationInterface, QueryRunner } from 'typeorm';

export class lectureEndedUpdate1655074132774 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Migration 1655074132774-lecture-ended-update running...');
    await queryRunner.query(`
            UPDATE  "lecture_progress"
            SET     ended = modified
            WHERE   "status" = 'success' AND "ended" IS NULL
        `);
    console.log('Migration done');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}

}
