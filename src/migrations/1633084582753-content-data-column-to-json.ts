import { MigrationInterface, QueryRunner } from 'typeorm';

export class ContentDataColumnToJSON1633084582753 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.query(`
    UPDATE "content" AS b
    SET DATA = T.DATA :: json
    FROM
        ( SELECT ID, DATA #>> '{}' AS DATA FROM "content" WHERE DATA IS NOT NULL ) AS T
    WHERE
        b.DATA IS NOT NULL
        AND b.ID = T.ID;`);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
