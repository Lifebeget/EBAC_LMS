import { MigrationInterface, QueryRunner } from 'typeorm';

export class ImportantNotification1657997573019 implements MigrationInterface {
  name = 'ImportantNotification1657997573019';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notification" ADD "important" boolean`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "important"`);
  }
}
