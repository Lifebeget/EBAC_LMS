import { MigrationInterface, QueryRunner } from 'typeorm';

export class TriggerDefaultUpdate1657797644540 implements MigrationInterface {
  name = 'TriggerDefaultUpdate1657797644540';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "trigger" ALTER COLUMN "date_to_execute" SET DEFAULT NOW()`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // Do nothing
  }
}
