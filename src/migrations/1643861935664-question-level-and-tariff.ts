import { ServiceLevelTariff } from '@entities/service-level-tariff.entity';
import { ServiceLevel } from '@entities/service-level.entity';
import { ServiceLevelTarget } from '@lib/api/directory/service-levels';
import { Calendars, ICalendar } from '@lib/helpers/date/calendar';
import { DateCalculator } from '@lib/helpers/date';
import { IsNull, MigrationInterface, MoreThan, QueryRunner } from 'typeorm';
import * as dayjs from 'dayjs';
import { Calendar } from '@entities/calendar.entity';
import { Submission } from '@entities/submission.entity';
import promisePool from 'src/_util/async-bottleneck';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { ForumMessageTariff } from '@entities/forum-tariff.entity';
import { SubmissionStatus } from '@lib/api/types';

const questionTariff = {
  activeFrom: '1970-01-01',
  activeTo: '9999-12-31',
  urgentHours: 24,
  outdatedHours: 72,
  value: 5,
};

const calendar: ICalendar = {
  year: 2021,
  vacationDays: ['01-01', '02-28', '03-01', '04-15', '04-21', '05-01', '06-16', '09-07', '10-12', '11-02', '11-15', '12-25'],
  workingDays: [],
};

export class questionLevelAndTariff1643861935664 implements MigrationInterface {
  private async migrationRequired(queryRunner: QueryRunner) {
    const levelsCount = await queryRunner.manager.count(ServiceLevel, {
      where: { target: ServiceLevelTarget.Question },
    });

    return levelsCount === 0;
  }
  private async createServiceLevel(queryRunner: QueryRunner, target: ServiceLevelTarget, title: string) {
    console.log('Createing question level and tariff');
    let result = await queryRunner.manager.insert(ServiceLevel, {
      title,
      isDefault: false,
      target,
    });

    const levelId = result.identifiers[0].id;

    result = await queryRunner.manager.insert(ServiceLevelTariff, {
      ...questionTariff,
      levelId,
    });

    return result.identifiers[0].id;
  }

  private async updateSubmissions(queryRunner: QueryRunner, calculator: DateCalculator, questionTariffId: string) {
    console.log('Updating submission');
    const submissions = await queryRunner.manager.find(Submission, {
      where: {
        status: SubmissionStatus.Submitted,
        submittedAt: MoreThan(new Date(2022, 1, 1)),
      },
      relations: ['tariff', 'tariff.createTariff'],
    });

    const updateSubmission = async (submission: Submission) => {
      if (!submission.isQuestion && submission.tariff?.createTariff) {
        await queryRunner.manager.update(
          SubmissionTariff,
          {
            submissionId: submission.id,
          },
          {
            urgentDate: calculator.addBusinessHours(submission.submittedAt, submission.tariff.createTariff.urgentHours),
            outdatedDate: calculator.addBusinessHours(submission.submittedAt, submission.tariff.createTariff.outdatedHours),
          },
        );
      } else {
        await queryRunner.manager.insert(SubmissionTariff, {
          submissionId: submission.id,
          createTariffId: questionTariffId,
          gradeTariffId: questionTariffId,
          urgentDate: calculator.addBusinessHours(submission.submittedAt, questionTariff.urgentHours),
          outdatedDate: calculator.addBusinessHours(submission.submittedAt, questionTariff.outdatedHours),
        });
      }
    };

    await promisePool(submissions, updateSubmission.bind(this), 10);
  }

  async updateForum(queryRunner: QueryRunner, calculator: DateCalculator, forumTariffId: string) {
    console.log('Updating forum');
    const messages = await queryRunner.manager.find(ForumMessages, {
      where: {
        deleted: IsNull(),
      },
    });

    const updateMessage = async (message: ForumMessages) => {
      await queryRunner.manager.insert(ForumMessageTariff, {
        messageId: message.id,
        createTariffId: forumTariffId,
        resolveTariffId: forumTariffId,
        urgentDate: calculator.addBusinessHours(message.created, questionTariff.urgentHours),
        outdatedDate: calculator.addBusinessHours(message.created, questionTariff.outdatedHours),
      });
    };

    await promisePool(messages, updateMessage.bind(this), 10);
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Migration 1643861935664-question-level-and-tariff running...');
    try {
      if (!this.migrationRequired(queryRunner)) {
        return;
      }

      await queryRunner.manager.insert(Calendar, calendar);
      const calculator = new DateCalculator([0, 6], dayjs).setCalendars(new Calendars([calendar], dayjs));

      const questionTariffId = await this.createServiceLevel(queryRunner, ServiceLevelTarget.Question, 'Question');
      await this.updateSubmissions(queryRunner, calculator, questionTariffId);

      const forumTariffId = await this.createServiceLevel(queryRunner, ServiceLevelTarget.Forum, 'Forum');
      await this.updateForum(queryRunner, calculator, forumTariffId);

      console.log('Migration done');
    } catch (e) {
      console.error('!!! ERROR !!!', e);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete(Calendar, {});
    await queryRunner.manager.delete(ForumMessageTariff, {});
  }
}
