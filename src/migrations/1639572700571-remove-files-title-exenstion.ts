import { MigrationInterface, QueryRunner } from 'typeorm';
import { File } from '@entities/file.entity';

export class removeFilesTitleExenstion1639572700571 implements MigrationInterface {
  name = 'removeFilesTitleExenstion1639572700571';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.update(
      File,
      {},
      {
        title: () => "REPLACE(title, CONCAT('.', extension), '')",
      },
    );
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
