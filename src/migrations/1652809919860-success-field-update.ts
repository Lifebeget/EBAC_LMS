import { MigrationInterface, QueryRunner } from 'typeorm';

export class successFieldUpdate1652809919860 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Migration 1652809919860-success-field-update running...');
    const result = await queryRunner.query(`
            WITH cte AS (
                SELECT submission.id,
                    (submission.status in ('graded') AND submission.score >= a.passing_score) as result,
                    submission.status as status
                FROM   submission
                    LEFT JOIN assessment a on submission.assessment_id = a.id
                WHERE submission.status in ('graded')
            )
            
            UPDATE submission
            SET    success = cte.result
            FROM   cte
            WHERE  cte.id = submission.id
        `);
    console.log('Migration done');

    return result;
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
