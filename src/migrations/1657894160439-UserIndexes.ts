import { MigrationInterface, QueryRunner } from "typeorm";

export class UserIndexes1657894160439 implements MigrationInterface {
    name = 'UserIndexes1657894160439';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_e12875dfb3b1d92d7d7c5377e2" ON "user" ("email") `);
        await queryRunner.query(`CREATE INDEX "IDX_843f46779f60b45032874be95b" ON "user" ("active") `);
        await queryRunner.query(`CREATE INDEX "IDX_64f07386a6620dcecf54653ca2" ON "user" ("pause") `);
        await queryRunner.query(`CREATE INDEX "IDX_4b64f121599c9ae5166e0a5004" ON "user" ("interapp_token") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_4b64f121599c9ae5166e0a5004"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_64f07386a6620dcecf54653ca2"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_843f46779f60b45032874be95b"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e12875dfb3b1d92d7d7c5377e2"`);
    }

}
