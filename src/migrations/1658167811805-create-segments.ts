import { MigrationInterface, QueryRunner } from "typeorm";

export class createSegments1658167811805 implements MigrationInterface {
  name = 'createSegments1658167811805'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "segment" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, "condition" jsonb, "deleted_at" TIMESTAMP WITH TIME ZONE, "created" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "modified" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_9e0406598d248857fe96f5e929d" UNIQUE ("name"), CONSTRAINT "PK_d648ac58d8e0532689dfb8ad7ef" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "webinar" ADD "segment_id" uuid`);
    await queryRunner.query(`ALTER TABLE "banner" ADD "segment_id" uuid`);
    await queryRunner.query(
      `ALTER TABLE "webinar" ADD CONSTRAINT "FK_6203ed9bc13fb1d0a83abff9e60" FOREIGN KEY ("segment_id") REFERENCES "segment"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "banner" ADD CONSTRAINT "FK_8732df581e1a7042fedeec06132" FOREIGN KEY ("segment_id") REFERENCES "segment"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "banner" DROP CONSTRAINT "FK_8732df581e1a7042fedeec06132"`);
    await queryRunner.query(`ALTER TABLE "webinar" DROP CONSTRAINT "FK_6203ed9bc13fb1d0a83abff9e60"`);
    await queryRunner.query(`ALTER TABLE "banner" DROP COLUMN "segment_id"`);
    await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "segment_id"`);
    await queryRunner.query(`DROP TABLE "segment"`);
  }

}
