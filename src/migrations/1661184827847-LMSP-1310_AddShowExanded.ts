import { MigrationInterface, QueryRunner } from 'typeorm';

export class LMSP1310AddShowExanded1661184827847 implements MigrationInterface {
  name = 'LMSP1310AddShowExanded1661184827847';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "file" ADD "show_expanded" boolean`);
    await queryRunner.query(`UPDATE public.file
    SET show_expanded=true
    WHERE id in (SELECT id
          FROM (
              SELECT file.id as id, ROW_NUMBER() OVER (PARTITION BY content.id ORDER BY file.created) as current
                  FROM file
                  LEFT JOIN content_files_file ON content_files_file.file_id = file.id 
                  LEFT JOIN content ON content_files_file.content_id = content.id 
              WHERE file.extension = 'pdf' AND content.lecture_id IN (SELECT DISTINCT lecture_id FROM content WHERE type = 'task' and active = true)
              ORDER BY file.id
              ) as file
          WHERE current = 1);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "file" DROP COLUMN "show_expanded"`);
  }
}
