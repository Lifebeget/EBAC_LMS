import { MigrationInterface, QueryRunner } from 'typeorm';

export class sectionsActiveAndPublished1657750200758 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        UPDATE  section
        SET     active = true, published = true, published_at = coalesce(published_at, now());
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
