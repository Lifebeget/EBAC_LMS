import {MigrationInterface, QueryRunner} from "typeorm";

export class addCourseStatusCriteriaToGraduatedProgressView1659365291453 implements MigrationInterface {
    name = 'addCourseStatusCriteriaToGraduatedProgressView1659365291453'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, ["MATERIALIZED_VIEW","public","view_user_progress_count_materialized"]);
        await queryRunner.query(`DROP MATERIALIZED VIEW "view_user_progress_count_materialized"`);
        await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, ["VIEW","public","view_user_progress_count"]);
        await queryRunner.query(`DROP VIEW "view_user_progress_count"`);
        await queryRunner.query(`CREATE VIEW "view_user_progress_count" AS 
    select
      e.user_id,
      e.course_id,
      vcc.status as course_status,
      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,
      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,
      count(1) filter (
        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required
      ) as modules_required_completed,
      count(1) filter (
        where vumc.lectures_completed >= vumc.lectures_total
      ) as modules_completed,
      vcc.lectures as lectures_total,
      vcc.lectures_required as lectures_required_total,
      vcc.modules as modules_total,
      vcc.modules_required as modules_required_total,
      vcc.homeworks as homeworks_total,
      vcc.homeworks_required as homeworks_required_total,
      vcc.quizes as quizes_total,
      vcc.quizes_required as quizes_required_total,
      vcc.surveys as surveys_total,
      vcc.surveys_required as surveys_required_total,
      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,
      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,
      coalesce(vsc.quizes_completed, 0) as quizes_completed,
      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,
      coalesce(vsc.surveys_completed, 0) as surveys_completed,
      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,
      case when vcc.lectures_required > 0 then
        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)
      else 0 end as progress,
      CASE
        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric AND vcc.status = 'completed'
        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric AND vcc.status = 'completed'
      END AS graduated
    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e
    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id
    left join view_course_count vcc on vcc.id = e.course_id
    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id
    group by e.user_id, e.course_id,
      vcc.lectures, vcc.lectures_required,
      vcc.modules, vcc.modules_required,
      vcc.homeworks, vcc.homeworks_required,
      vcc.quizes, vcc.quizes_required,
      vcc.surveys, vcc.surveys_required,
      vsc.homeworks_completed, vsc.homeworks_completed_required,
      vsc.quizes_completed, vsc.quizes_completed_required,
      vsc.surveys_completed, vsc.surveys_completed_required,
      vcc.status
  `);
        await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, ["VIEW","public","view_user_progress_count","select\n      e.user_id,\n      e.course_id,\n      vcc.status as course_status,\n      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,\n      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,\n      count(1) filter (\n        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required\n      ) as modules_required_completed,\n      count(1) filter (\n        where vumc.lectures_completed >= vumc.lectures_total\n      ) as modules_completed,\n      vcc.lectures as lectures_total,\n      vcc.lectures_required as lectures_required_total,\n      vcc.modules as modules_total,\n      vcc.modules_required as modules_required_total,\n      vcc.homeworks as homeworks_total,\n      vcc.homeworks_required as homeworks_required_total,\n      vcc.quizes as quizes_total,\n      vcc.quizes_required as quizes_required_total,\n      vcc.surveys as surveys_total,\n      vcc.surveys_required as surveys_required_total,\n      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,\n      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,\n      coalesce(vsc.quizes_completed, 0) as quizes_completed,\n      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,\n      coalesce(vsc.surveys_completed, 0) as surveys_completed,\n      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,\n      case when vcc.lectures_required > 0 then\n        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)\n      else 0 end as progress,\n      CASE\n        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric AND vcc.status = 'completed'\n        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric AND vcc.status = 'completed'\n      END AS graduated\n    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e\n    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id\n    left join view_course_count vcc on vcc.id = e.course_id\n    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id\n    group by e.user_id, e.course_id,\n      vcc.lectures, vcc.lectures_required,\n      vcc.modules, vcc.modules_required,\n      vcc.homeworks, vcc.homeworks_required,\n      vcc.quizes, vcc.quizes_required,\n      vcc.surveys, vcc.surveys_required,\n      vsc.homeworks_completed, vsc.homeworks_completed_required,\n      vsc.quizes_completed, vsc.quizes_completed_required,\n      vsc.surveys_completed, vsc.surveys_completed_required,\n      vcc.status"]);
        await queryRunner.query(`CREATE MATERIALIZED VIEW "view_user_progress_count_materialized" AS 
    select * from view_user_progress_count
  `);
        await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, ["MATERIALIZED_VIEW","public","view_user_progress_count_materialized","select * from view_user_progress_count"]);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, ["MATERIALIZED_VIEW","public","view_user_progress_count_materialized"]);
        await queryRunner.query(`DROP MATERIALIZED VIEW "view_user_progress_count_materialized"`);
        await queryRunner.query(`DELETE FROM "typeorm_metadata" WHERE "type" = $1 AND "schema" = $2 AND "name" = $3`, ["VIEW","public","view_user_progress_count"]);
        await queryRunner.query(`DROP VIEW "view_user_progress_count"`);
        await queryRunner.query(`CREATE VIEW "view_user_progress_count" AS select
      e.user_id,
      e.course_id,
      vcc.status as course_status,
      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,
      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,
      count(1) filter (
        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required
      ) as modules_required_completed,
      count(1) filter (
        where vumc.lectures_completed >= vumc.lectures_total
      ) as modules_completed,
      vcc.lectures as lectures_total,
      vcc.lectures_required as lectures_required_total,
      vcc.modules as modules_total,
      vcc.modules_required as modules_required_total,
      vcc.homeworks as homeworks_total,
      vcc.homeworks_required as homeworks_required_total,
      vcc.quizes as quizes_total,
      vcc.quizes_required as quizes_required_total,
      vcc.surveys as surveys_total,
      vcc.surveys_required as surveys_required_total,
      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,
      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,
      coalesce(vsc.quizes_completed, 0) as quizes_completed,
      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,
      coalesce(vsc.surveys_completed, 0) as surveys_completed,
      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,
      case when vcc.lectures_required > 0 then
        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)
      else 0 end as progress,
      CASE
        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric
        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric
      END AS graduated
    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e
    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id
    left join view_course_count vcc on vcc.id = e.course_id
    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id
    group by e.user_id, e.course_id,
      vcc.lectures, vcc.lectures_required,
      vcc.modules, vcc.modules_required,
      vcc.homeworks, vcc.homeworks_required,
      vcc.quizes, vcc.quizes_required,
      vcc.surveys, vcc.surveys_required,
      vsc.homeworks_completed, vsc.homeworks_completed_required,
      vsc.quizes_completed, vsc.quizes_completed_required,
      vsc.surveys_completed, vsc.surveys_completed_required,
      vcc.status`);
        await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, ["VIEW","public","view_user_progress_count","select\n      e.user_id,\n      e.course_id,\n      vcc.status as course_status,\n      coalesce(sum(vumc.lectures_completed), 0) as lectures_completed,\n      coalesce(sum(vumc.lectures_required_completed), 0) as lectures_required_completed,\n      count(1) filter (\n        where vumc.required and vumc.lectures_required_completed >= vumc.lectures_required\n      ) as modules_required_completed,\n      count(1) filter (\n        where vumc.lectures_completed >= vumc.lectures_total\n      ) as modules_completed,\n      vcc.lectures as lectures_total,\n      vcc.lectures_required as lectures_required_total,\n      vcc.modules as modules_total,\n      vcc.modules_required as modules_required_total,\n      vcc.homeworks as homeworks_total,\n      vcc.homeworks_required as homeworks_required_total,\n      vcc.quizes as quizes_total,\n      vcc.quizes_required as quizes_required_total,\n      vcc.surveys as surveys_total,\n      vcc.surveys_required as surveys_required_total,\n      coalesce(vsc.homeworks_completed, 0) as homeworks_completed,\n      coalesce(vsc.homeworks_completed_required, 0) as homeworks_completed_required,\n      coalesce(vsc.quizes_completed, 0) as quizes_completed,\n      coalesce(vsc.quizes_completed_required, 0) as quizes_completed_required,\n      coalesce(vsc.surveys_completed, 0) as surveys_completed,\n      coalesce(vsc.surveys_completed_required, 0) as surveys_completed_required,\n      case when vcc.lectures_required > 0 then\n        coalesce(round(sum(vumc.lectures_required_completed)::float * 100 / vcc.lectures_required::float), 0)\n      else 0 end as progress,\n      CASE\n        WHEN vcc.homeworks_required = 0 THEN COALESCE(sum(vumc.lectures_required_completed), 0::numeric) = vcc.lectures_required::numeric\n        ELSE COALESCE(vsc.homeworks_completed_required, 0::numeric) = vcc.homeworks_required::numeric\n      END AS graduated\n    from (select user_id, course_id from enrollment where role = 'student' group by user_id, course_id) e\n    left join view_user_module_count vumc on e.user_id = vumc.user_id and e.course_id = vumc.course_id\n    left join view_course_count vcc on vcc.id = e.course_id\n    left join view_submission_count vsc on vsc.course_id = e.course_id and vsc.user_id = e.user_id\n    group by e.user_id, e.course_id,\n      vcc.lectures, vcc.lectures_required,\n      vcc.modules, vcc.modules_required,\n      vcc.homeworks, vcc.homeworks_required,\n      vcc.quizes, vcc.quizes_required,\n      vcc.surveys, vcc.surveys_required,\n      vsc.homeworks_completed, vsc.homeworks_completed_required,\n      vsc.quizes_completed, vsc.quizes_completed_required,\n      vsc.surveys_completed, vsc.surveys_completed_required,\n      vcc.status"]);
        await queryRunner.query(`CREATE MATERIALIZED VIEW "view_user_progress_count_materialized" AS select * from view_user_progress_count`);
        await queryRunner.query(`INSERT INTO "typeorm_metadata"("type", "schema", "name", "value") VALUES ($1, $2, $3, $4)`, ["MATERIALIZED_VIEW","public","view_user_progress_count_materialized","select * from view_user_progress_count"]);
    }

}
