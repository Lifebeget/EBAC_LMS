import { MigrationInterface, QueryRunner } from 'typeorm';

export class forumTutorsPipeline1649176714082 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      update forum_messages set pipeline = 'tutor' where id in (
        select fm.id
        from forum_messages fm
        inner join forum_messages_lock fml on fm.lock_id = fml.id
        where thread_parent_id is null
          and pipeline = 'support'
          and deleted is null
          and fml.user_id IN (
            select distinct e.user_id
            from enrollment e
            inner join "user" u on e.user_id = u.id and u.active = true
            where e.role IN ('tutor', 'teamlead')
              and e.active = true
        )
      )
    `);
    console.log('forumTutorsPipeline1649176714082 migration - OK');
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
