import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc';

dayjs.extend(utc);

import { MigrationInterface, QueryRunner } from 'typeorm';

const _sla = {
  urgentHours: 24,
  outdatedHours: 72,
};

export class newForumStatistics1641892360463 implements MigrationInterface {
  public async up(_queryRunner: QueryRunner): Promise<void> {
    console.log('Migration 1641892360463-new-forum-statistics running...');
    console.log(`
      Skipped. Please delete user_forum_statistic and course_forum_statistic tables manually.
      Then run node bin/manage stat forum for each date`);
    // console.log('Dropping old tables...');

    // try {
    //   await queryRunner.dropTable('user_forum_statistic');
    //   await queryRunner.dropTable('course_forum_statistic');
    // } catch (er) {
    //   console.log('er');
    // }

    // console.log('Deleteing data...');

    // await queryRunner.manager.getRepository(CourseForumStatistic).delete({});
    // await queryRunner.manager.getRepository(UserForumStatistic).delete({});

    // const messagesByUser = new UserForumStatService(false, sli);
    // const threadsByUser = new UserForumStatService(true, sli);

    // const messagesByCourse = new CourseForumStatService(false, sli);
    // const threadsByCourse = new CourseForumStatService(true, sli);

    // const now = dayjs();
    // const start = dayjs('2021-11-10');

    // console.log('Calculating data...');

    // for (let d = dayjs(start); d.isBefore(now); d = d.add(1, 'days')) {
    //   const dt = d.format('YYYY-MM-DD');
    //   await messagesByUser.save(dt);
    //   await threadsByUser.save(dt);
    //   await messagesByCourse.save(dt);
    //   await threadsByCourse.save(dt);

    //   console.log(`${dt} done`);
    // }

    console.log('Migration done');
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
