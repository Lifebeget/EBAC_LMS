import { MigrationInterface, QueryRunner } from 'typeorm';

export class DomainsUppercase1658844634402 implements MigrationInterface {
  name = 'DomainsUppercase1658844634402';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
          UPDATE "user" SET domain = UPPER(domain) WHERE domain IS NOT NULL;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
          UPDATE "user" SET domain = LOWER(domain) WHERE domain IS NOT NULL;
    `);
  }
}
