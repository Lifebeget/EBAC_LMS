import { MigrationInterface, QueryRunner } from 'typeorm';

export class AttemptLimits1658155385261 implements MigrationInterface {
  name = 'AttemptLimits1658155385261';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "attempt" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "additional_attempts" integer NOT NULL DEFAULT '0', "created" TIMESTAMP NOT NULL DEFAULT now(), "modified" TIMESTAMP NOT NULL DEFAULT now(), "user_id" uuid, "course_id" uuid, "lecture_id" uuid, "assessment_id" uuid, CONSTRAINT "PK_5f822b29b3128d1c65d3d6c193d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "assessment" ADD "max_attempts" integer NOT NULL DEFAULT '3'`);
    await queryRunner.query(
      `ALTER TABLE "attempt" ADD CONSTRAINT "FK_c976ed6445a23f1138e948bc20a" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "attempt" ADD CONSTRAINT "FK_130760bdc2125798b5f3aec79ca" FOREIGN KEY ("course_id") REFERENCES "course"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "attempt" ADD CONSTRAINT "FK_23bffd95fa191ac249b4c639796" FOREIGN KEY ("lecture_id") REFERENCES "lecture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "attempt" ADD CONSTRAINT "FK_bfbc23330cb027d626fd64a8de4" FOREIGN KEY ("assessment_id") REFERENCES "assessment"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_bfbc23330cb027d626fd64a8de4"`);
    await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_23bffd95fa191ac249b4c639796"`);
    await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_130760bdc2125798b5f3aec79ca"`);
    await queryRunner.query(`ALTER TABLE "attempt" DROP CONSTRAINT "FK_c976ed6445a23f1138e948bc20a"`);
    await queryRunner.query(`ALTER TABLE "assessment" DROP COLUMN "max_attempts"`);
    await queryRunner.query(`DROP TABLE "attempt"`);
  }
}
