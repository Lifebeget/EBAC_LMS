import {MigrationInterface, QueryRunner} from "typeorm";

export class DropSocialGroupLinkColumn1658233613701 implements MigrationInterface {
    name = 'DropSocialGroupLinkColumn1658233613701'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "course" DROP COLUMN "social_group_link"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "course" ADD "social_group_link" character varying(120)`);
    }

}
