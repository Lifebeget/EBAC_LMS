import { Settings } from '@entities/settings.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import defaultSettings from 'src/settings/defaultSettings';

export class settingsInit1633087715467 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const settingCount = await queryRunner.manager.count(Settings);
    if (settingCount) {
      return;
    }

    await queryRunner.manager.insert(Settings, defaultSettings);
  }

  public async down(_queryRunner: QueryRunner): Promise<void> {}
}
