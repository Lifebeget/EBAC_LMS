// import { Course } from '@entities/course.entity';
import { Section } from '@entities/section.entity';
import { from } from 'form-data';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class CheckCourseSection1656601077818 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const newSection = {
      id: undefined,
      externalId: null,
      description: '',
      title: null,
      published: true,
      publishedAt: new Date(),
      active: true,
      sort: -1,
    };

    const course = await queryRunner.query(`
        select course.id, course.title from course
    `);
    const sectionsRepository = await queryRunner.connection.createEntityManager().getRepository('section');
    const moduleRepository = await queryRunner.connection.createEntityManager().getRepository('module');
    course.forEach(async item => {
      const modules = await queryRunner.query(
        `select id, title, sort, active, section_id, course_id from module where course_id = '${item.id}' order by sort`,
      );
      const sections = (await sectionsRepository
        .createQueryBuilder('section')
        .andWhere('section.course_id = :id', {
          id: item.id,
        })
        .orderBy('sort', 'ASC')
        .getMany()) as Section[];

      if (modules.length > 0 && sections.length > 0) {
        if (modules[0].sort < sections[0].sort) {
          console.log(`Course: ${item.title}, first module above section: ${modules[0].title}`);
          newSection.title = item.title;
          newSection.sort = modules[0].sort - 10;
          const newFirstSection = await sectionsRepository.save({
            ...newSection,
            course: { id: item.id },
          });

          // update section_id in Modules by sort
          modules.forEach(module => {
            if (module.sort >= newFirstSection.sort) {
              console.log(`module title: ${module.title}, old section: ${module.section_id} new section: ${newFirstSection.id}`);
              module.section_id = newFirstSection.id;
            }
          });

          for (let index = 0; index < sections.length; index++) {
            modules.forEach(module => {
              if (module.sort >= sections[index].sort) {
                console.log(`module title: ${module.title}, old section: ${module.section_id} new section: ${sections[index].id}`);
                module.section_id = sections[index].id;
              }
            });
          }

          const promises = [];
          modules.forEach(module => promises.push(moduleRepository.save(module)));
          await Promise.all(promises);
          console.log(`[UPDATE] modules section id [DONE]`);
        }
      }
    });
    return;
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
