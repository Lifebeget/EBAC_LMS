import {MigrationInterface, QueryRunner} from "typeorm";

export class LMSP12451659972261941 implements MigrationInterface {
    name = 'LMSP12451659972261941'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "banner_courses_course" ("banner_id" uuid NOT NULL, "course_id" uuid NOT NULL, CONSTRAINT "PK_24df6e19f9c333fcc9db7e25cb2" PRIMARY KEY ("banner_id", "course_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_71c58f2c5a0f9ee0c83d2f89e5" ON "banner_courses_course" ("banner_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_ecbaa146b4410f849c6a58161c" ON "banner_courses_course" ("course_id") `);
        await queryRunner.query(`ALTER TABLE "banner_courses_course" ADD CONSTRAINT "FK_71c58f2c5a0f9ee0c83d2f89e53" FOREIGN KEY ("banner_id") REFERENCES "banner"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "banner_courses_course" ADD CONSTRAINT "FK_ecbaa146b4410f849c6a58161c1" FOREIGN KEY ("course_id") REFERENCES "course"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "banner_courses_course" DROP CONSTRAINT "FK_ecbaa146b4410f849c6a58161c1"`);
        await queryRunner.query(`ALTER TABLE "banner_courses_course" DROP CONSTRAINT "FK_71c58f2c5a0f9ee0c83d2f89e53"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ecbaa146b4410f849c6a58161c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_71c58f2c5a0f9ee0c83d2f89e5"`);
        await queryRunner.query(`DROP TABLE "banner_courses_course"`);
    }

}
