import { MigrationInterface, QueryRunner } from 'typeorm';

export class multipleCorrections1649382880072 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        insert
            into
            correction (user_id,
            "date",
            value,
            reason)
        select
            user_id,
            "date",
            value,
            reason
        from
            payment_correction
    `);

    await queryRunner.dropTable('payment_correction');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        insert
            into
            payment_correction (user_id,
            "date",
            value,
            reason)
        select
            user_id,
            "date",
            value,
            reason
        from
            correction
    `);

    await queryRunner.dropTable('correction');
  }
}
