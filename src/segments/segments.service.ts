import { Conditions, Segment } from '@entities/segment.entity';
import { User } from '@entities/user.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { CreateSegmentDto } from './dto/create-segment.dto';
import { UpdateSegmentDto } from './dto/update-segment.dto';
import { buildConditionQuery } from '@utils/conditions';

@Injectable()
export class SegmentsService {
  constructor(
    @InjectRepository(Segment)
    private readonly segmentRespository: Repository<Segment>,
  ) {}

  create(createSegmentDto: CreateSegmentDto) {
    createSegmentDto.name = createSegmentDto?.name.trim();
    createSegmentDto.description = createSegmentDto?.description.trim();
    return this.segmentRespository.save(createSegmentDto).catch((e: Error) => {
      throw new BadRequestException('This name has already been used');
    });
  }

  async findAll(pagination, query) {
    const qb = this.segmentRespository.createQueryBuilder('segment').orderBy('id', 'ASC');

    if (query.onlyWithCondition) {
      qb.andWhere('segment.condition is not null');
    }

    if (query.search) {
      qb.andWhere('segment.name iLIKE :search', { search: `%${query.search}%` });
    }

    if (!pagination.showAll) {
      qb.skip((pagination.page - 1) * pagination.itemsPerPage);
      qb.take(pagination.itemsPerPage);
    }

    if (query.orderByName) {
      qb.orderBy('segment.name');
    } else {
      qb.orderBy('segment.created', 'DESC');
    }

    const [result, total] = await qb.getManyAndCount();

    return {
      result,
      meta: {
        ...pagination,
        total,
      },
    };
  }

  async findOne(id: string) {
    const segment = await this.segmentRespository.findOne(id);
    if (!segment) {
      throw new BadRequestException('Bad segment id');
    }
    return segment;
  }

  update(id: string, updateSegmentDto: UpdateSegmentDto) {
    updateSegmentDto.name = updateSegmentDto?.name.trim();
    updateSegmentDto.description = updateSegmentDto?.description.trim();
    return this.segmentRespository.update(id, updateSegmentDto).catch((e: Error) => {
      throw new BadRequestException('This name has already been used');
    });
  }

  remove(id: string) {
    return this.segmentRespository.softDelete(id);
  }

  async buildCondition(condition: Conditions, user: User = null) {
    const db = getConnection();
    const startAt = new Date();
    const count = (await db.query(buildConditionQuery(condition, true, 'filter', 0, user) as string))[0]?.count;
    const endAt = new Date();
    return {
      executionTime: +endAt - +startAt,
      startAt,
      endAt,
      count,
    };
  }

  async validateCondition(condition: Conditions) {
    return await this.buildCondition(condition);
  }

  async validateConditionBySegmentForUser(id: string, user: User) {
    const result = await this.segmentRespository.findOne(id);
    if (!result) {
      throw new BadRequestException('Bad segment id');
    } else {
      return +(await this.buildCondition(result.condition, user)).count === 0 ? false : true;
    }
  }

  async validateConditionForUser(condition: Conditions, user: User) {
    return +(await this.buildCondition(condition, user)).count === 0 ? false : true;
  }
}
