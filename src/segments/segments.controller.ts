import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Query, ParseUUIDPipe, BadRequestException } from '@nestjs/common';
import { SegmentsService } from './segments.service';
import { CreateSegmentDto } from './dto/create-segment.dto';
import { UpdateSegmentDto } from './dto/update-segment.dto';
import { Conditions } from '@entities/segment.entity';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ValidateConditionDto } from './dto/validate-user.dto';
import { Pagination } from 'src/pagination.decorator';
import { PaginationDto } from 'src/pagination.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { ConditionDto } from './dto/condition.dto';
import { QueryDto } from './dto/query.dto';
import { IsUUID } from 'class-validator';

@ApiTags('segments')
@Controller('segments')
export class SegmentsController {
  constructor(private readonly segmentsService: SegmentsService) {}

  @Post()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create a new segment' })
  @RequirePermissions(Permission.SEGMENTS_MANAGE)
  create(@Body() createSegmentDto: CreateSegmentDto) {
    return this.segmentsService.create(createSegmentDto);
  }

  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Find all segments' })
  @RequirePermissions(Permission.SEGMENTS_VIEW)
  findAll(@Pagination() pagination: PaginationDto, @Query() query: QueryDto) {
    return this.segmentsService.findAll(pagination, query);
  }

  @Post('/condition')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Validate condition' })
  @RequirePermissions(Permission.SEGMENTS_VIEW)
  validateCondition(@Body() body: ConditionDto) {
    const condition = body.condition.all || body.condition.any;
    if (!condition || condition.filter(c => !c.fact && !c.any && !c.all).length > 0) {
      throw new BadRequestException('Bad condition');
    }
    return this.segmentsService.validateCondition(body.condition);
  }

  @Get('/condition-user/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Validate condition for current user' })
  @RequirePermissions(Permission.SEGMENTS_VIEW)
  validateConditionForCurrentUser(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: any) {
    return this.segmentsService.validateConditionBySegmentForUser(id, req.user);
  }

  @Post('/condition-user/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Validate condition for user' })
  @RequirePermissions(Permission.SEGMENTS_VIEW)
  validateConditionForUser(@Param('id', new ParseUUIDPipe()) id: string, @Body() body: ValidateConditionDto) {
    return this.segmentsService.validateConditionBySegmentForUser(id, body.user);
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Find segment by id' })
  @RequirePermissions(Permission.SEGMENTS_VIEW)
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.segmentsService.findOne(id);
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update segment' })
  @RequirePermissions(Permission.SEGMENTS_MANAGE)
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateSegmentDto: UpdateSegmentDto) {
    return this.segmentsService.update(id, updateSegmentDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Soft delete segment' })
  @RequirePermissions(Permission.SEGMENTS_MANAGE)
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.segmentsService.remove(id);
  }
}
