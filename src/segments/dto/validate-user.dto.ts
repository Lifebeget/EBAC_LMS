import { User } from '@entities/user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsUUID, ValidateIf, ValidateNested } from 'class-validator';

class UserDTO extends User {
  @IsUUID()
  id: string;
}

export class ValidateConditionDto {
  @ApiProperty({ description: 'Segment description' })
  @ValidateIf(o => o.hasOwnProperty('user'))
  @ValidateNested({ each: true })
  @Type(() => UserDTO)
  user: UserDTO;
}
