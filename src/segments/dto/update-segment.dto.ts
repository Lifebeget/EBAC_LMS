import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';
import { CreateSegmentDto } from './create-segment.dto';

export class UpdateSegmentDto extends PartialType(CreateSegmentDto) {
  @ApiProperty({ description: 'Segment name' })
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiProperty({ description: 'Segment description' })
  @IsOptional()
  @IsString()
  description?: string;
}
