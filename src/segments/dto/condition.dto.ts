import { Conditions } from '@entities/segment.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ValidateIf } from 'class-validator';

export class ConditionDto {
  @ApiProperty({ description: 'Segment condition' })
  @ValidateIf(o => o.hasOwnProperty('condition'))
  condition?: Conditions;
}
