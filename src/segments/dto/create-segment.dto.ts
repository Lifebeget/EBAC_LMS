import { Conditions } from '@entities/segment.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Transform, TransformFnParams } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateIf } from 'class-validator';

export class CreateSegmentDto {
  @ApiProperty({ description: 'Segment name' })
  @Transform(({ value }: TransformFnParams) => value?.trim())
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({ description: 'Segment description' })
  @Transform(({ value }: TransformFnParams) => value?.trim())
  @IsString()
  description?: string;

  @ApiProperty({ description: 'Segment condition' })
  @ValidateIf(o => o.hasOwnProperty('condition'))
  condition?: Conditions;
}
