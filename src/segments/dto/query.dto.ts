import { ApiProperty } from '@nestjs/swagger';

export class QueryDto {
  @ApiProperty({ description: 'Segments only with condition' })
  onlyWithCondition?: boolean;

  @ApiProperty({ description: 'Segments order by name' })
  orderByName?: boolean;

  @ApiProperty({ description: 'Search by name' })
  search?: string;
}
