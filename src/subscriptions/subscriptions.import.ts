import { Client } from '@entities/client.entity';
import { Source } from '@entities/source.entity';
import { Subscription } from '@entities/subscription.entity';
import { User } from '@entities/user.entity';
import { Webinar } from '@entities/webinars.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import { Repository } from 'typeorm';
import { GDDSubscription } from '../lib/types/GddSubscription';
import { ImportOptions } from '../lib/types/ImportOptions';

export class ImportSubscriptions {
  private subscriptionsRepository: Repository<Subscription>;
  private clientsRepository: Repository<Client>;
  private sourcesRepository: Repository<Source>;
  private webinarsRepository: Repository<Webinar>;
  private usersRepository: Repository<User>;
  private token: string;
  private url: string;

  constructor(context: INestApplicationContext) {
    this.subscriptionsRepository = context.get(getRepositoryToken(Subscription));
    this.clientsRepository = context.get(getRepositoryToken(Client));
    this.sourcesRepository = context.get(getRepositoryToken(Source));
    this.webinarsRepository = context.get(getRepositoryToken(Webinar));
    this.usersRepository = context.get(getRepositoryToken(User));
    this.token = process.env.GDD_TOKEN;
    this.url = process.env.GDD_SUBSCRIPTIONS_URL;
    strict.ok(this.token);
    strict.ok(this.url);
  }

  async import({ pageSize = 100, chill = false }: ImportOptions) {
    let page = 0;
    let created = 0;
    let updated = 0;

    console.log(`➡️ Importing subscriptions`, { pageSize });

    while (true) {
      // Fetch GDD subscriptions
      const res = await axios.get<{ subscriptions: GDDSubscription[] }>(this.url, {
        params: {
          offset: page * pageSize,
          limit: pageSize,
          token: this.token,
        },
      });

      // Create or update each LMS subscription
      for (const gddSubscription of res.data.subscriptions) {
        const client = gddSubscription.client_id
          ? await this.clientsRepository.findOne({ externalId: gddSubscription.client_id }, { select: ['id', 'email'], withDeleted: true })
          : null;
        const source = await this.sourcesRepository.findOne(
          { externalId: gddSubscription.source_id },
          { select: ['id'], withDeleted: true },
        );
        const webinar = await this.webinarsRepository.findOne(
          { externalId: gddSubscription.source_id },
          { select: ['id'], withDeleted: true },
        );
        const user = client
          ? await this.usersRepository.findOne({
              where: { email: client.email },
              select: ['id'],
            })
          : null;

        if (!chill) {
          strict.ok(client);
          strict.ok(source);
          strict.ok(webinar);
          strict.ok(user);
        }

        const lmsSubscription: Partial<Subscription> = {
          client,
          source,
          webinar,
          user,
          externalId: gddSubscription.id,
          reference: gddSubscription.reference,
          created: new Date(gddSubscription.created),
          modified: new Date(gddSubscription.modified),
          options: gddSubscription.options,
        };
        const existingLmsSubscription = await this.subscriptionsRepository.findOne(
          { externalId: lmsSubscription.externalId },
          { withDeleted: true },
        );
        if (existingLmsSubscription) {
          await this.subscriptionsRepository.save({
            ...existingLmsSubscription,
            ...lmsSubscription,
          });
          updated += 1;
        } else {
          await this.subscriptionsRepository.save(lmsSubscription);
          created += 1;
        }
      }

      // Quit or go to the next page
      if (res.data.subscriptions.length < pageSize) {
        console.log('✔️ done', { created, updated, total: created + updated });
        break;
      } else {
        page += 1;
        console.log({ page, created, updated });
      }
    }
  }
}
