import { Source } from '@entities/source.entity';
import { Subscription } from '@entities/subscription.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { strict } from 'assert';
import { ClientsService } from 'src/clients/clients.service';
import { UsersService } from 'src/users/users.service';
import { WebinarService } from 'src/webinar/webinar.service';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { SubscriptionFormat } from './subscriptions.controller';

@Injectable()
export class SubscriptionsService {
  constructor(
    private readonly clientsService: ClientsService,
    private readonly webinarsService: WebinarService,
    @InjectRepository(Subscription)
    private subscriptionRepository: Repository<Subscription>,
    @InjectRepository(Source)
    private sourceRepository: Repository<Source>,
    private usersService: UsersService,
  ) {}

  async createSubscription({ clientId, sourceId, webinarId, ...dto }: CreateSubscriptionDto) {
    const client = await this.clientsService.findClientById(clientId);
    strict.ok(client);
    strict.ok(client.email);
    const user = await this.usersService.findOneByEmail(client.email);
    const source = await this.sourceRepository.findOne(sourceId);
    let webinar;
    if (webinarId) {
      webinar = await this.webinarsService.findOne(webinarId);
      strict.ok(webinar);
    }

    strict.ok(source);

    return this.subscriptionRepository.save({
      ...dto,
      client,
      source,
      webinar,
      user,
    });
  }

  async findSubscriptions(options: FindManyOptions<Subscription>): Promise<{ data: Subscription[]; count: number }> {
    const [data, count] = await this.subscriptionRepository.findAndCount(options);
    return { count, data };
  }

  @Transactional()
  async findSubscription(options: FindOneOptions<Subscription>): Promise<Subscription | null> {
    return this.subscriptionRepository.findOne(options);
  }

  public static formatSubscription(format: SubscriptionFormat, subscription: Subscription) {
    switch (format) {
      case SubscriptionFormat.DEFAULT:
        return { ...subscription };
      case SubscriptionFormat.BILLING: {
        const pick = (keys: (keyof Subscription)[]) => keys.reduce((acc, k) => ({ ...acc, [k]: subscription[k] }), {});

        const { user, webinar, options } = subscription;

        return {
          ...pick(['id', 'created', 'modified']),
          // user
          userId: user && user.id,
          userName: user && user.name,
          userEmail: user && user.email,
          // event
          eventCode: webinar && webinar.slug,
          eventId: webinar && webinar.id,
          eventType: webinar && webinar.type,
          eventName: webinar && webinar.name,
          eventStartDateUtc: webinar && webinar.startDateUtc,
          // options
          ga_cid: options.ga_cid,
          pageUrl: options.PageUrl,
          window_width: options.window_width,
          window_height: options.window_height,
          useragent: options.UserAgent,
          referrer: options.referrer_url,
          utm_term: options.utm_term,
          utm_medium: options.utm_medium,
          utm_source: options.utm_source,
          utm_content: options.utm_content,
          utm_campaign: options.utm_campaign,
          visits_count: options.visits_count,
          entrance_point: options.entrance_point,
        };
      }
      default:
        throw new Error(`Unsupported subscription format: ${format}`);
    }
  }
}
