import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsDefined, IsNumber, IsOptional, IsString, ValidateIf } from 'class-validator';
import { Product } from 'src/lib/types/Product';
import { ProductDto } from './product.dto';

export class CreateSubscriptionDto {
  @ApiProperty()
  @IsDefined()
  sourceId: string;

  @ApiProperty()
  @IsDefined()
  clientId: string;

  @ApiProperty({ required: false })
  webinarId?: string;

  // Необязательные поля
  @ApiProperty({
    required: false,
    description: 'Client window height',
  })
  @IsOptional()
  @IsNumber()
  window_height?: number;

  @ApiProperty({
    required: false,
    description: 'Client window width',
  })
  @IsOptional()
  @IsNumber()
  window_width?: number;

  @ApiProperty({
    required: false,
    description: 'Client referrer',
  })
  @IsOptional()
  @IsString()
  Referrer?: string;

  @ApiProperty({
    required: false,
    description: 'Client page url',
  })
  @IsOptional()
  @IsString()
  PageUrl?: string;

  @ApiProperty({
    required: false,
    description: 'Client timestamp',
  })
  @IsOptional()
  @IsString()
  UserTimestamp?: string;

  @ApiProperty({
    required: false,
    description: 'Client date',
  })
  @IsOptional()
  @IsString()
  UserDate?: string;

  @ApiProperty({
    required: false,
    description: 'Client user agent',
  })
  @IsOptional()
  @IsString()
  UserAgent?: string;

  @ApiProperty({
    required: false,
    description: 'Client gacid',
  })
  @IsOptional()
  @IsNumber()
  ga_cid?: number;

  @ApiProperty({
    required: false,
    description: 'Client has no gasid',
  })
  @IsOptional()
  @IsBoolean()
  no_ga?: boolean;

  @ApiProperty({
    required: false,
    description: 'Experments',
  })
  @IsOptional()
  @IsString()
  experiments?: string;

  @ApiProperty({
    required: false,
    description: 'Last schedule event',
  })
  @IsOptional()
  @IsString()
  last_schedule_event?: string;

  @ApiProperty({
    required: false,
    description: 'Client UTM source',
  })
  @IsOptional()
  @IsString()
  utm_source?: string;

  @ApiProperty({
    required: false,
    description: 'Client UTM medium',
  })
  @IsOptional()
  @IsString()
  utm_medium?: string;

  @ApiProperty({
    required: false,
    description: 'Client UTM campaign',
  })
  @IsOptional()
  @IsString()
  utm_campaign?: string;

  @ApiProperty({
    required: false,
    description: 'Client UTM content',
  })
  @IsOptional()
  @IsString()
  utm_content?: string;

  @ApiProperty({
    required: false,
    description: 'Client UTM term',
  })
  @IsOptional()
  @IsString()
  utm_term?: string;

  @ApiProperty({
    required: false,
    description: 'Client current page',
  })
  @IsOptional()
  @IsString()
  current_page?: string;

  @ApiProperty({
    required: false,
    description: 'Client referrer url',
  })
  @IsOptional()
  @IsString()
  referrer_url?: string;

  @ApiProperty({
    required: false,
    description: 'Client entrance point',
  })
  @IsOptional()
  @IsString()
  entrance_point?: string;

  @ApiProperty({
    required: false,
    description: 'Client visits count',
  })
  @IsOptional()
  @IsNumber()
  visits_count?: number;

  @ApiProperty({
    required: false,
    description: 'Client IP',
  })
  @IsOptional()
  @IsString()
  ClientIP?: string;

  // Поля конкретных типов заявок:
  // - лендинги курсов
  @ApiProperty({
    required: false,
    description: 'Course promocode',
  })
  @ValidateIf(o => o.hasOwnProperty('promo_code'))
  @IsOptional()
  @IsString()
  promo_code?: string;

  @ApiProperty({
    required: false,
    description: 'Client contact method',
  })
  @ValidateIf(o => o.hasOwnProperty('contact_with'))
  @IsOptional()
  @IsString()
  contact_with?: string;

  // - лендинги вебинаров и скидок для участников вебинаров
  @ApiProperty({
    required: false,
    description: 'Webinar schedule code',
  })
  @ValidateIf(o => o.hasOwnProperty('schedule_code'))
  @IsDefined()
  @IsString()
  schedule_code: string;

  // - лендинги распродаж
  @ApiProperty({
    required: false,
    description: 'Sale id',
  })
  @ValidateIf(o => o.hasOwnProperty('sale_id'))
  @IsDefined()
  sale_id: string;

  @ApiProperty({
    required: false,
    description: 'Sale products',
    type: () => [ProductDto],
  })
  @ValidateIf(o => o.hasOwnProperty('products'))
  @IsDefined()
  @IsArray()
  products: Product[];

  // Прочие поля (в доке не указано, но с лендинга отправляется)
  @ApiProperty({
    required: false,
    description: 'Checkbox',
  })
  @IsOptional()
  @IsString()
  Checkbox?: string;

  @ApiProperty()
  @IsOptional()
  country?: string;
}
