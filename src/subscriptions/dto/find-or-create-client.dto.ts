import { ApiProperty } from '@nestjs/swagger';
import { IsDefined } from 'class-validator';

export class FindOrCreateClientDto {
  @ApiProperty()
  @IsDefined()
  name: string;

  @ApiProperty({ required: false })
  phone?: string;

  @ApiProperty()
  email?: string;

  @ApiProperty()
  country?: string;
}
