import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNumber, IsString } from 'class-validator';

export class ProductDto {
  @ApiProperty({ description: 'Product id' })
  @IsDefined()
  @IsNumber()
  id: number;

  @ApiProperty({ description: 'Product price' })
  @IsDefined()
  @IsNumber()
  price: number;

  @ApiProperty({ description: 'Product discount percent' })
  @IsDefined()
  @IsNumber()
  percent: number;

  @ApiProperty({ description: 'Product short name' })
  @IsDefined()
  @IsString()
  shortName: string;

  @ApiProperty({ description: 'Product name' })
  @IsDefined()
  @IsString()
  name: string;
}
