import { Source } from '@entities/source.entity';
import { Subscription } from '@entities/subscription.entity';
import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule } from 'src/clients/clients.module';
import { WebinarModule } from 'src/webinar/webinar.module';
import { UsersModule } from 'src/users/users.module';
import { MailModule } from 'src/mail/mail.module';
import { SubscriptionsController } from './subscriptions.controller';
import { SubscriptionsService } from './subscriptions.service';
import { SourceModule } from '../source/source.module';
import { SegmentsModule } from 'src/segments/segments.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Subscription]),
    TypeOrmModule.forFeature([Source]),
    ClientsModule,
    WebinarModule,
    UsersModule,
    MailModule,
    SourceModule,
    HttpModule,
    SegmentsModule,
  ],
  controllers: [SubscriptionsController],
  providers: [SubscriptionsService],
  exports: [SubscriptionsService],
})
export class SubscriptionsModule {}
