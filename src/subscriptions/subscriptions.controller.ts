import { Subscription } from '@entities/subscription.entity';
import { Permission } from '@enums/permission.enum';
import { BadRequestException, Body, Controller, Get, HttpService, InternalServerErrorException, Post, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiProperty, ApiResponse, ApiTags } from '@nestjs/swagger';
import { strict } from 'assert';
import { IsDateString, IsDefined, IsEmail, IsOptional } from 'class-validator';
import { Interapp } from 'src/auth/interapp.decorator';
import { ClientsService } from 'src/clients/clients.service';
import { PaginationDto } from 'src/pagination.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { SegmentsService } from 'src/segments/segments.service';
import { WebinarService } from 'src/webinar/webinar.service';
import { FindManyOptions, MoreThan } from 'typeorm';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { SubscriptionsService } from './subscriptions.service';

export enum SubscriptionFormat {
  DEFAULT = 'default',
  BILLING = 'billing',
}
export class FindSubscriptionsQuery extends PaginationDto {
  @ApiProperty({ required: false })
  @IsEmail()
  @IsOptional()
  userEmail?: string;

  @ApiProperty({ required: false })
  @IsDateString()
  @IsOptional()
  modifiedAfter?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  format?: SubscriptionFormat;
}

export class isMySubscriptionQuery {
  @ApiProperty()
  @IsDefined()
  webinarSlug: string;
}

@ApiTags('subscriptions')
@Controller('subscriptions')
export class SubscriptionsController {
  constructor(
    //
    private subscriptionsService: SubscriptionsService,
    private clientsService: ClientsService,
    private webinarsService: WebinarService,
    private httpService: HttpService,
    private segmentsService: SegmentsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Subscribe client' })
  @Interapp()
  @ApiResponse({
    status: 201,
    description: 'User subscribed',
  })
  @RequirePermissions(Permission.SUBSCRIPTION_MANAGE)
  async createSubscription(@Body() body: CreateSubscriptionDto) {
    return this.subscriptionsService.createSubscription(body);
  }

  @Get()
  @ApiOperation({ summary: 'Find subscriptions' })
  @ApiBearerAuth()
  @Interapp()
  @RequirePermissions(Permission.SUBSCRIPTION_VIEW)
  async findSubscriptions(@Query() query: FindSubscriptionsQuery) {
    const { userEmail, modifiedAfter, page = 1, itemsPerPage = 100, format = SubscriptionFormat.DEFAULT } = query;

    const where: FindManyOptions<Subscription>['where'] = {};
    if (userEmail) {
      const client = await this.clientsService.findClientByEmail({
        email: userEmail,
      });
      strict.ok(client);
      where.client = client;
    }
    if (modifiedAfter) {
      where.modified = MoreThan(new Date(modifiedAfter));
    }

    const { count, data } = await this.subscriptionsService.findSubscriptions({
      order: { id: 'ASC' },
      where,
      take: itemsPerPage,
      skip: (page - 1) * itemsPerPage,
      withDeleted: true,
      relations: ['user', 'webinar'],
    });
    return {
      count,
      data: data.map(SubscriptionsService.formatSubscription.bind(null, format)),
    };
  }

  @Get('is-mine')
  @ApiOperation({
    summary: 'Checks if current user has a specific subscription',
  })
  @ApiBearerAuth()
  async isMySubscription(@Req() req: any, @Query() query: isMySubscriptionQuery) {
    const webinar = await this.webinarsService.findOneBySlug(query.webinarSlug);

    if (!webinar) {
      throw new BadRequestException('Webinar not found');
    }

    const result: any = {
      webinarId: webinar.id,
      public: webinar.public,
    };

    if (webinar.public === false) {
      result.canSubscribe = !!webinar.segment.condition
        ? await this.segmentsService.validateConditionForUser(webinar.segment.condition, req.user)
        : false;
    } else {
      result.canSubscribe = true;
    }

    const { subscriptionId, isGddSubscription } = await (async (): Promise<{
      subscriptionId: string | null;
      isGddSubscription: boolean | null;
    }> => {
      // try to find local subscription
      const subscription = await this.subscriptionsService.findSubscription({
        where: { webinar, user: req.user },
      });
      if (subscription) {
        return { subscriptionId: subscription.id, isGddSubscription: false };
      }

      // fallback to CMS subscriptions
      const gddSubscriptionsRes = await this.httpService
        .post(process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL, {
          token: process.env.WEBINAR_ACCESS_KEY,
          email: req.user.email,
          timeout: 10000,
        })
        .toPromise();

      // find CMS subscription by webinar external id
      const gddSubscription = gddSubscriptionsRes.data.subscriptions.find(s => s.schedule_event_id === webinar.externalId);
      if (gddSubscription) {
        return { subscriptionId: gddSubscription.subscription_id, isGddSubscription: true };
      }

      // if nothing found
      return { subscriptionId: null, isGddSubscription: null };
    })();

    result.subscriptionId = subscriptionId;
    result.isMine = !!subscriptionId;
    result.isGddSubscription = isGddSubscription;

    return result;
  }
}
