import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Hierarchy } from 'src/lib/entities/hierarchy.entity';
import { Permission } from 'src/lib/enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateHierarchyDto } from './dto/create-hierarchy.dto';
import { UpdateHierarchyDto } from './dto/update-hierarchy.dto';
import { HierarchysService } from './hierarchy.service';

@ApiTags('hierarchy')
@ApiBearerAuth()
@Controller('hierarchy')
export class HierarchysController {
  constructor(private readonly hierarchyService: HierarchysService) {}

  @Post()
  @RequirePermissions(Permission.HIERARCHY_MANAGE)
  @ApiOperation({ summary: 'Create hierarchy' })
  @ApiResponse({
    status: 200,
    type: Hierarchy,
    description: 'Created hierarchy',
  })
  findOrCreate(@Body() createHierarchyDto: CreateHierarchyDto) {
    return this.hierarchyService.create(createHierarchyDto);
  }

  @Get('teamlead/:teamleadId')
  @RequirePermissions(Permission.HIERARCHY_VIEW)
  @ApiOperation({ summary: 'Get hierarchy of teamlead' })
  findTeamleadTutors(@Param('teamleadId', new ParseUUIDPipe()) teamleadId: string) {
    return this.hierarchyService.findTeamleadTutors(teamleadId);
  }

  @Post('teamlead/:teamleadId/:tutorId')
  @RequirePermissions(Permission.HIERARCHY_MANAGE)
  @ApiOperation({ summary: 'Assign teamlead for tutor' })
  assignTeamlead(@Param('teamleadId', new ParseUUIDPipe()) teamleadId: string, @Param('tutorId', new ParseUUIDPipe()) tutorId: string) {
    return this.hierarchyService.assignTeamlead(teamleadId, tutorId);
  }

  @Delete('teamlead/:teamleadId/:tutorId')
  @RequirePermissions(Permission.HIERARCHY_MANAGE)
  @ApiOperation({ summary: 'Release tutor from teamlead' })
  releaseTeamleadTutor(
    @Param('teamleadId', new ParseUUIDPipe()) teamleadId: string,
    @Param('tutorId', new ParseUUIDPipe()) tutorId: string,
  ) {
    return this.hierarchyService.releaseTeamleadTutor(teamleadId, tutorId);
  }

  @Delete('teamlead/:teamleadId')
  @RequirePermissions(Permission.HIERARCHY_MANAGE)
  @ApiOperation({ summary: 'Release teamlead from all tutors' })
  releaseTeamlead(@Param('teamleadId', new ParseUUIDPipe()) teamleadId: string) {
    return this.hierarchyService.releaseTeamlead(teamleadId);
  }

  @Get()
  @ApiOperation({ summary: 'Get all hierarchy' })
  @ApiResponse({
    status: 200,
    type: [Hierarchy],
    description: 'Find all hierarchy',
  })
  findAll() {
    return this.hierarchyService.findAll();
  }

  @Put(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Update hierarchy' })
  @ApiResponse({
    status: 200,
    type: Hierarchy,
    description: 'Updated hierarchy',
  })
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateHierarchyDto: UpdateHierarchyDto) {
    return this.hierarchyService.update(id, updateHierarchyDto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Delete hierarchy' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  delete(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.hierarchyService.remove(id);
  }
}
