import { SystemRole } from '@lib/api/types';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { WelcomeTriggerService } from 'src/trigger/services/welcome-trigger.service';
import { Interapp } from '../auth/interapp.decorator';
import { User } from '../lib/entities/user.entity';
import { mapPermissionsForRoles, Permission } from '../lib/enums/permission.enum';
import { Permissions } from '../permissions.decorator';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { CreateAvatarDto } from './dto/create-avatar.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { QuerySearchUsersDTO } from './dto/query-search.users.dto';
import { QueryUserDto } from './dto/query-user.dto';
import { QueryWebinarUsersDTO } from './dto/query-webinar-users.dto';
import { UpdateModulesDto } from './dto/update-modules.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { SupportService } from './support.service';
import { UsersService } from './users.service';
import { UserSettings } from '@lib/types/settings';
import { checkRoleset } from 'src/utils/user.helper';
import { QueryUsersCompletedSurveyDto } from './dto/query-users-completed-survey.dto';
import { Tag } from 'src/lib/entities/tag.entity';
import { UpdateDomainDto } from './dto/update-domain.dto';

@ApiTags('users')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly welcomeTriggerService: WelcomeTriggerService,
    private readonly supportService: SupportService,
  ) {}

  @Post()
  @Interapp()
  @ApiOperation({ summary: 'Create a new user' })
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiResponse({
    status: 201,
    description: 'Created user',
    type: User,
  })
  @PgConstraints()
  async create(@Body() createUserDto: CreateUserDto, @Req() req: Express.Request) {
    const roles = req.user.parsedRoles;

    // Superadmin role cannot be added through API
    if (createUserDto.roles?.some(role => role.role == SystemRole.Superadmin)) {
      throw new UnauthorizedException("You can't set that role");
    }

    // Check if current user role allows to set provided roles
    if (
      createUserDto.roles &&
      !checkRoleset(
        roles,
        createUserDto.roles.map(r => r.role),
      )
    ) {
      throw new UnauthorizedException("You can't set that role");
    }

    const user = await this.usersService.create(createUserDto);

    this.welcomeTriggerService.studentCreated(user, createUserDto.password, createUserDto.sendEmail);

    return user;
  }

  @Get()
  @Interapp()
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'List all users' })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @ApiPaginatedResponse(User)
  findAll(@Pagination() paging: PaginationDto, @Query() query: QueryUserDto, @Req() req: Request) {
    if (query.includeProfile && !req.user.permissions.includes(Permission.USERS_MANAGE)) {
      throw new ForbiddenException('FORBIDDEN');
    }
    return this.usersService.findAll(paging, query);
  }

  @Get('webinar/:id')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Users which subscribed on webinar' })
  findAllWebinarUsers(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query(
      new ValidationPipe({
        transform: true,
      }),
    )
    query: QueryWebinarUsersDTO,
  ) {
    return this.usersService.webinarSubscribers(id, query);
  }

  @Get('/search')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Search users by email, name or id' })
  @Interapp()
  searchMany(@Query() query: QuerySearchUsersDTO) {
    return this.usersService.searchAll(query.search);
  }

  @Get('/responsibles')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Tutors' })
  @Interapp()
  findTutors() {
    return this.usersService.findResponsibles();
  }

  @Get('status')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get users count' })
  @ApiResponse({
    status: 200,
    type: Number,
  })
  status() {
    return this.usersService.status();
  }

  @Get(':id')
  @Interapp()
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get one user by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a user by id',
    type: User,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    const user = await this.usersService.findOne(id);
    const rolesArray = user.roles.map(el => el.role);
    const permissions = mapPermissionsForRoles(rolesArray);
    return { ...user, permissions };
  }

  @Get('webinar/:id/survey-completed')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get users completed the webinar survey' })
  findAllCompletedWebinarSurvey(
    @Param('id', new ParseUUIDPipe()) webinarId: string,
    @Query(
      new ValidationPipe({
        transform: true,
      }),
    )
    query: QueryUsersCompletedSurveyDto,
  ) {
    return this.usersService.findAllCompletedWebinarSurvey(webinarId, query);
  }

  @Patch('update-settings')
  @ApiOperation({ summary: 'Updates user settings' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  updateSettings(@Req() req: Request, @Body() settings: UserSettings) {
    const userId = req.user.id;
    return this.usersService.updateSettings(userId, settings);
  }

  @Patch('self/domain')
  @ApiOperation({ summary: 'User updates domain' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async updateSelfDomain(@Req() req: Request, @Body() updateDomainDto: UpdateDomainDto) {
    const userId = req.user.id;
    const user = await this.usersService.findOne(userId);
    if (user.domain) throw new BadRequestException('Domain already specified');
    return this.usersService.update(userId, { domain: updateDomainDto.domain });
  }

  @Patch(':id')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Updates a user' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateUserDto: UpdateUserDto,
    @Permissions() perms: Permission[],
    @Req() req: Request,
  ) {
    // Cannot patch roles
    if (updateUserDto.hasOwnProperty('roles')) {
      throw new BadRequestException("You can't update roles through this endpoint");
    }

    // Cannot update users with higher roles
    if (req.user.id != id) {
      const user = await this.usersService.findOne(id);
      if (
        !checkRoleset(
          req.user.parsedRoles,
          user.roles.map(r => r.role),
        )
      ) {
        throw new UnauthorizedException("You can't edit users with higher roles");
      }
    }
    return this.usersService.update(id, updateUserDto);
  }

  @Post(':id/reset-password')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE, Permission.USERS_PASSWORD_RESET)
  @ApiOperation({ summary: 'Resets user password' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async resetPassword(@Param('id', new ParseUUIDPipe()) userId: string, @Permissions() perms: Permission[], @Req() req: Request) {
    const user = await this.usersService.findOne(userId);
    // Cannot update admins if not superadmin or self
    if (req.user.id != userId && !perms.includes(Permission.GRANT_ADMIN_ROLE)) {
      if (user.roles.some(role => role.role == SystemRole.Admin || role.role == SystemRole.Superadmin)) {
        throw new UnauthorizedException("You can't edit admin users");
      }
    }
    const newPassword = this.usersService.generatePassword();
    await this.usersService.update(userId, { password: newPassword });
    await this.welcomeTriggerService.studentCreated(user, newPassword, true);
  }

  @Delete('self/avatar')
  @ApiOperation({ summary: 'Delete self avatar' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async removeSelfAvatar(@Req() req: Request) {
    return this.usersService.removeSelfAvatar(req.user.id);
  }

  @Post('self/avatar')
  @ApiOperation({ summary: 'Upload self avatar' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async uploadSelfAvatar(
    @Body()
    createAvatarDto: CreateAvatarDto,
    @Req() req: Request,
  ) {
    const match = createAvatarDto?.data?.match(/data:(?<contentType>.*?);base64,(?<base64>.+)/);
    if (!match) throw new BadRequestException('Wrong base64 format');

    return this.usersService.uploadSelfAvatar(req.user.id, match.groups?.base64, match.groups?.contentType);
  }

  @Delete(':id')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE)
  @ApiOperation({ summary: 'Deletes a user' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    // Cannot delete admins if not superadmin
    const user = await this.usersService.findOne(id);
    if (user.roles.some(role => role.role == SystemRole.Admin)) {
      if (!perms.includes(Permission.GRANT_ADMIN_ROLE)) {
        throw new UnauthorizedException("You can't delete admin users");
      }
    }
    // Cannot delete superadmins
    if (user.roles.some(role => role.role == SystemRole.Superadmin)) {
      throw new UnauthorizedException("You can't delete superadmin users");
    }
    // Otherwise it's ok
    return this.usersService.remove(id);
  }

  @Patch(':id/modules')
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  )
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Updates modules in the user' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated user',
  })
  @PgConstraints()
  async updateModels(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateModulesDto: UpdateModulesDto) {
    return await this.usersService.updateModules(id, updateModulesDto);
  }

  @Get('support/:id')
  @Interapp()
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get one user for support by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a user for support by id',
    type: User,
  })
  @ApiResponse({
    status: 404,
    description: 'User not found',
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid user id',
  })
  @ApiResponse({
    status: 401,
    description: 'Unauthorized',
  })
  async findOneForSupport(@Param('id', new ParseUUIDPipe()) userId: string) {
    const data = await this.supportService.getProfile(userId);
    if (!data.user) {
      throw new NotFoundException(`User not found: ${userId}`);
    }
    return data;
  }

  @Post(':userId/tag/:tagId')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({
    summary: 'Attach tag to user',
  })
  @ApiResponse({
    status: 200,
    type: Tag,
    description: 'Attached tag to user',
  })
  attachTag(@Param('userId', new ParseUUIDPipe()) userId: string, @Param('tagId', new ParseUUIDPipe()) tagId: string) {
    return this.usersService.attachTag(userId, tagId);
  }

  @Delete(':userId/tag/:tagId')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({
    summary: 'Detach tag from user',
  })
  @ApiResponse({
    status: 200,
    type: [Tag],
    description: 'List of tags that remains in user',
  })
  detachTag(@Param('userId', new ParseUUIDPipe()) userId: string, @Param('tagId', new ParseUUIDPipe()) tagId: string) {
    return this.usersService.detachTag(userId, tagId);
  }
}
