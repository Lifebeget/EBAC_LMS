import { BlacklistItem } from '../lib/entities/blacklist.entity';
import { Permission } from '@enums/permission.enum';
import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { BlacklistService } from './blacklist.service';
import { CreateBlacklistItemDto } from './dto/create-blacklist-item.dto';
import { QueryBlacklistItemDto } from './dto/query-blacklist-item.dto';
import { UpdateBlacklistItemDto } from './dto/update-blacklist-item.dto';
import { PgConstraints } from 'src/pg.decorators';

@ApiTags('users')
@Controller()
export class BlacklistController {
  constructor(private readonly blacklistService: BlacklistService) {}

  @Post('blacklist')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Adds one user to another user blacklist' })
  @ApiResponse({
    status: 201,
    description: 'Created role',
    type: BlacklistItem,
  })
  @PgConstraints()
  create(@Body() createBlacklistItemDto: CreateBlacklistItemDto) {
    return this.blacklistService.create(createBlacklistItemDto);
  }

  @Patch('blacklist')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Update an item in blacklist' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Body() updateBlacklistItemDto: UpdateBlacklistItemDto) {
    return this.blacklistService.update(updateBlacklistItemDto);
  }

  @Get('blacklist')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'All blacklist' })
  @ApiResponse({
    status: 200,
    type: [BlacklistItem],
  })
  @ApiQuery({
    name: 'tutorId',
    description: 'Filter by tutor',
    required: false,
  })
  @ApiQuery({
    name: 'studentId',
    description: 'Filter by student',
    required: false,
  })
  findAll(@Query() query: QueryBlacklistItemDto) {
    return this.blacklistService.findAll(query);
  }

  @Delete('blacklist/:id')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Delete from blacklist' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.blacklistService.remove(id);
  }
}
