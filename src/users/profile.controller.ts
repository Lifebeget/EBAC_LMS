import { Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe, Req, Query } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { ProfileService } from './profile.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { PgConstraints } from '../pg.decorators';
import { Profile } from '../lib/entities/profile.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { QueryProfileDto } from './dto/query-profile.dto';
import { Request } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../lib/entities/user.entity';
import { Repository } from 'typeorm';
import { UpdateUserDto } from './dto/update-user.dto';
import { Interapp } from 'src/auth/interapp.decorator';

@ApiTags('profile')
@Controller('')
export class ProfileController {
  constructor(
    private readonly profileService: ProfileService,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  @Post('users/:userId/profile')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE)
  @ApiOperation({ summary: 'Create a new profile for particular user' })
  @ApiResponse({
    status: 201,
    description: 'Created profile',
    type: Profile,
  })
  @PgConstraints()
  create(@Param('userId') userId: string, @Body() createProfileDto: CreateProfileDto) {
    return this.profileService.create(userId, createProfileDto);
  }

  @Get('users/:userId/profile')
  @Interapp()
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get user profile by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a user profile by id',
    type: Profile,
  })
  findOne(@Param('userId', new ParseUUIDPipe()) userId: string) {
    return this.profileService.getProfileByUserId(userId);
  }

  @Patch('users/:userId/profile')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE)
  @ApiOperation({ summary: 'Updates user profile' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('userId', new ParseUUIDPipe()) userId: string, @Body() updateProfileDto: UpdateProfileDto) {
    return this.profileService.update(userId, updateProfileDto);
  }

  @Delete('users/:userId/profile')
  @Interapp()
  @RequirePermissions(Permission.USERS_MANAGE)
  @ApiOperation({ summary: 'Deletes user profile' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) userId: string) {
    return this.profileService.remove(userId);
  }

  @Get('user/profile')
  @ApiOperation({ summary: 'Get requested user profile' })
  @ApiResponse({
    status: 200,
    description: 'Get requested user profile',
    type: Profile,
  })
  async findSelfProfile(@Query() query: QueryProfileDto, @Req() req: Request) {
    let profile = await this.profileService.getProfileByUserId(req.user.id);
    if (!profile) {
      const [name, surname] = req.user.name?.split(' ');
      const createProfileDto: CreateProfileDto = {
        name: name || '',
        surname: surname || '',
      };
      profile = await this.profileService.create(req.user.id, createProfileDto);
    }
    return profile;
  }

  @Patch('user/profile')
  @ApiOperation({ summary: 'Updates requested user profile' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async updateSelfProfile(@Req() req: Request, @Body() updateProfileDto: UpdateProfileDto) {
    const updateUsernameDto: UpdateUserDto = {
      name: `${updateProfileDto.name} ${updateProfileDto.surname}`,
    };
    await this.usersRepository.update(req.user.id, updateUsernameDto);
    return this.profileService.update(req.user.id, updateProfileDto);
  }

  //todo probably remove after test
  @Get('profile/phone-search')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Search users by profile phone' })
  searchOne(@Query() query: { phone: string }) {
    return this.profileService.getProfileAndUserBySearch(query?.phone);
  }

  @Post('profile/modify-all')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({
    summary: 'Updates all profiles (for creating consistent hashes)',
  })
  modifyAll() {
    return this.profileService.modifyAll();
  }
}
