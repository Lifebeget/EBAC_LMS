/* eslint-disable @typescript-eslint/no-unused-vars */
import { Hierarchy } from '@entities/hierarchy.entity';
import { mapGradeForRoles } from '@enums/grade-role.enum';
import { HierarchyRole } from '@enums/hierarchy-role.enum';
import { AppRole, MultiDomainsEnum, SubmissionStatus, SystemRole } from '@lib/api/types';
import { BadRequestException, forwardRef, Inject, Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import * as assert from 'assert';
import { FilesService } from 'src/files/files.service';
import { Role } from 'src/lib/entities/role.entity';
import { UserActivity } from 'src/lib/entities/user-activity.entity';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { WebinarService } from 'src/webinar/webinar.service';
import { Brackets, getConnection, getManager, In, IsNull, Not, Repository, SelectQueryBuilder } from 'typeorm';
import { generatePassword, generatePasswordHash } from '../helpers';
import { User } from '../lib/entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { QueryUserDto } from './dto/query-user.dto';
import { UpdateModulesDto } from './dto/update-modules.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserSettings } from '@lib/types/settings';
import { TagsService } from 'src/tags/tags.service';
import { getTeamleadCourseIds, getTeamleadTutorIds } from 'src/query-helpers/teamlead.helper';
import { EnrollmentType } from '@lib/types/enums/enrollment-type.enum';
import { QueryUsersCompletedSurveyDto } from './dto/query-users-completed-survey.dto';
import { TagType } from '@lib/types/enums/tag-type.enum';
import { Cert } from '@entities/cert.entity';
import { QueryWebinarUsersDTO } from './dto/query-webinar-users.dto';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { FeatureStrategy } from '@lib/types/enums/feature-flags/feature-strategy.enum';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateFeatureEntryByUserFilterDto } from '../feature-flags/dto/create-entry-by-user-filter.dto';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { CreateCMSEnrollmentDto } from 'src/courses/dto/create-cms-enrollment.dto';
import config from 'config';
import { CreateProfileDto } from './dto/create-profile.dto';
import { ProfileService } from './profile.service';
import { WelcomeTriggerService } from 'src/trigger/services/welcome-trigger.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
    @InjectRepository(UserActivity)
    private usersActivityRepository: Repository<UserActivity>,
    @InjectRepository(FeatureFlag)
    private featureFlagRepository: Repository<FeatureFlag>,
    private readonly filesService: FilesService,
    @Inject(forwardRef(() => WebinarService))
    private readonly webinarService: WebinarService,
    private readonly tagsService: TagsService,
    @Inject(forwardRef(() => ProfileService))
    private readonly profileService: ProfileService,
    @Inject(forwardRef(() => WelcomeTriggerService))
    private readonly welcomeTriggerService: WelcomeTriggerService,
  ) {}

  getUsersRepository() {
    return this.usersRepository;
  }

  async saveActivity(userId: string, arg: Omit<Partial<UserActivity>, 'user'>) {
    const user = await this.usersRepository.findOne(userId);
    // required to correctly update modified column
    const activity = await this.usersActivityRepository.findOne({
      where: { user },
    });

    if (activity) {
      const { firstLectureView } = activity;
      const activityUpdate = {
        ...arg,
        // if current activity is 'lectureViews' and firstLectureView not initialized - add it to activity update
        ...(arg.activity === 'lectureViews' && firstLectureView == null ? { firstLectureView: new Date() } : {}),
      };
      return this.usersActivityRepository.update({ user }, activityUpdate);
    }

    return this.usersActivityRepository.save({ user, activity: arg.activity });
  }

  async create(createUserDto: CreateUserDto) {
    const entity = { ...createUserDto };

    if (createUserDto.generatePassword) {
      createUserDto.password = this.generatePassword();
    }
    if (createUserDto.password) {
      entity.password = generatePasswordHash(createUserDto.password);
    }
    entity.email = entity.email.toLowerCase();
    const { password, ...user } = await this.usersRepository.save(entity);

    return user as User;
  }

  // Create user and profile from CMS Enrollment if user is not exists
  async createCMS(createCMSEnrollmentDto: CreateCMSEnrollmentDto): Promise<User> {
    let sendEmail = true;
    if (createCMSEnrollmentDto.sendEmail !== undefined) {
      sendEmail = createCMSEnrollmentDto.sendEmail;
    }

    // find user
    let user = await this.findOneByEmail(createCMSEnrollmentDto.email);

    // create user if not exists
    if (!user) {
      const defaultDomainCode = (config.defaultDomainCode as MultiDomainsEnum) || null;

      user = await this.create({
        email: createCMSEnrollmentDto.email,
        name: createCMSEnrollmentDto.name,
        generatePassword: sendEmail,
        externalId: createCMSEnrollmentDto.eadboxUserId,
        sendEmail: sendEmail,
        domain: createCMSEnrollmentDto.domain || defaultDomainCode,
      });

      const profile = {
        name: createCMSEnrollmentDto.name,
      } as CreateProfileDto;

      if (createCMSEnrollmentDto.phone) {
        profile.phone = createCMSEnrollmentDto.phone;
      }

      await this.profileService.create(user.id, profile);
    }

    if (!user.hasPassword && sendEmail) {
      // generate password and send welcome email
      user.password = generatePassword(12);
      await this.update(user.id, user);
      this.welcomeTriggerService.studentCreated(user, user.password, sendEmail);
    }

    return user;
  }

  generatePassword() {
    return generatePassword(12);
  }

  enrollmentsCondition(includeInactiveEnrollments?: boolean) {
    if (!includeInactiveEnrollments) {
      return (
        'enrollments.active = TRUE' +
        ' AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())' +
        ' AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW())'
      );
    }
    return '';
  }

  async userIsStudent(userId: string) {
    const student = await this.usersRepository
      .createQueryBuilder('user')
      .where('user.id = :userId', { userId })
      .leftJoin('user.enrollments', 'enrollments', this.enrollmentsCondition(false))
      .andWhere('enrollments.type != :trial', { trial: EnrollmentType.Trial })
      .andWhere('enrollments.role = :student', { student: AppRole.Student })
      .getOne();

    return !!student;
  }

  joinUserRelations<T>(queryBuilder: SelectQueryBuilder<T>, query: QueryUserDto) {
    queryBuilder
      .select('user.id')
      .leftJoin(
        'user.roles',
        'roles',
        '(roles.date_from IS NULL OR roles.date_from <= NOW())' + 'AND (roles.date_to IS NULL OR roles.date_to > NOW())',
      )
      .leftJoin('user.modules', 'modules')
      .leftJoin('modules.course', 'course')
      .leftJoin('user.activity', 'activity')
      .leftJoin('user.enrollments', 'enrollments', this.enrollmentsCondition(query.includeInactiveEnrollments))
      .leftJoin('user.tags', 'tags')
      .leftJoin('tags.category', 'tag_categories')
      .leftJoin('user.blacklist', 'blacklist');
  }

  joinAndSelectUserRelations<T>(queryBuilder: SelectQueryBuilder<T>, query: QueryUserDto) {
    queryBuilder
      .leftJoinAndSelect(
        'user.roles',
        'roles',
        '(roles.date_from IS NULL OR roles.date_from <= NOW())' + 'AND (roles.date_to IS NULL OR roles.date_to > NOW())',
      )
      .leftJoinAndSelect('user.modules', 'modules')
      .leftJoinAndSelect('modules.course', 'course')
      .leftJoinAndSelect('user.activity', 'activity')
      .leftJoinAndSelect('user.enrollments', 'enrollments', this.enrollmentsCondition(query.includeInactiveEnrollments))
      .leftJoinAndSelect('user.tags', 'tags')
      .leftJoinAndSelect('tags.category', 'tag_categories')
      .leftJoinAndSelect('user.blacklistedStudents', 'blacklistedStudents');
  }

  async applyUserFilters<T>(queryBuilder: SelectQueryBuilder<T>, query: QueryUserDto) {
    if (query.role) {
      queryBuilder = queryBuilder.leftJoin('user.roles', 'rolesSubSelect');
      if (query.role === AppRole.Student) {
        queryBuilder = queryBuilder
          .andWhere('enrollments.type != :type', { type: EnrollmentType.Trial })
          .andWhere('enrollments.role = :role', { role: AppRole.Student });
      } else {
        queryBuilder = queryBuilder.andWhere('rolesSubSelect.role = :role', {
          role: query.role,
        });
        queryBuilder = queryBuilder.andWhere('(rolesSubSelect.date_to IS NULL OR rolesSubSelect.date_to > NOW())');
        queryBuilder = queryBuilder.andWhere('(rolesSubSelect.date_from IS NULL OR rolesSubSelect.date_from <= NOW())');
      }
    }
    if (query.roles) {
      queryBuilder = queryBuilder.leftJoin('user.roles', 'rolesInSubSelect');
      queryBuilder = queryBuilder.andWhere('rolesInSubSelect.role IN (:...roles)', {
        roles: query.roles,
      });
      queryBuilder = queryBuilder.andWhere('(rolesInSubSelect.date_to IS NULL OR rolesInSubSelect.date_to > NOW())');
      queryBuilder = queryBuilder.andWhere('(rolesInSubSelect.date_from IS NULL OR rolesInSubSelect.date_from <= NOW())');
    }
    if (query.name) {
      queryBuilder = queryBuilder.andWhere('LOWER(user.name) LIKE LOWER(:name)', {
        name: `%${query.name}%`,
      });
    }
    if (query.email) {
      queryBuilder = queryBuilder.andWhere('user.email LIKE :email', {
        email: `%${query.email.toLowerCase()}%`,
      });
    }
    if (query.exactEmail) {
      queryBuilder = queryBuilder.andWhere('user.email = :exactEmail', {
        exactEmail: `${query.exactEmail.toLowerCase()}`,
      });
    }
    if (query.active !== undefined) {
      queryBuilder = queryBuilder.andWhere('user.active = :active', {
        active: query.active,
      });
    }
    if (query.moduleId) {
      queryBuilder = queryBuilder.andWhere('modules.id = :moduleId', {
        moduleId: query.moduleId,
      });
    }
    if (query.noActivity) {
      queryBuilder = queryBuilder.andWhere('activity.modified IS NULL');
    }
    if (query.activityDateUp) {
      queryBuilder = queryBuilder.andWhere('activity.modified >= :activityDateUp', query);
    }
    if (query.activityDateDown) {
      queryBuilder = queryBuilder.andWhere('activity.modified <= :activityDateDown', query);
    }
    if (query.noLastSubmissionView) {
      queryBuilder = queryBuilder.andWhere('activity.lastSubmissionView IS NULL');
    }
    if (query.lastSubmissionViewUp) {
      queryBuilder = queryBuilder.andWhere('activity.lastSubmissionView >= :lastSubmissionViewUp', query);
    }
    if (query.lastSubmissionViewDown) {
      queryBuilder = queryBuilder.andWhere('activity.lastSubmissionView <= :lastSubmissionViewDown', query);
    }
    if (query.externalId) {
      queryBuilder = queryBuilder.andWhere('user.externalId = :externalId', {
        externalId: query.externalId,
      });
    }

    if (query.ids) {
      queryBuilder = queryBuilder.andWhere('user.id = ANY (:ids)', {
        ids: query.ids,
      });
    }

    if (query.searchString) {
      queryBuilder.andWhere("LOWER(CONCAT(user.name, ' ', user.email, ' ', user.id)) like :like", {
        like: `%${query.searchString.toLowerCase()}%`,
      });
    }

    if (query.studentCourseId) {
      queryBuilder
        .andWhere('enrollments.course_id = :studentCourseId', {
          studentCourseId: query.studentCourseId,
        })
        .andWhere('enrollments.role = :roleStudent', {
          roleStudent: CourseRole.Student,
        });
    }

    if (query.tutorCourseIds?.length) {
      queryBuilder
        .andWhere('enrollments.course_id in (:...tutorCourseIds)', {
          tutorCourseIds: query.tutorCourseIds,
        })
        .andWhere('enrollments.role = :roleTutor', {
          roleTutor: CourseRole.Tutor,
        });
    }

    if (query.includeProfile === 'true') {
      queryBuilder = queryBuilder.leftJoinAndSelect('user.profile', 'profile');
    }

    if (query.tagIds?.length) {
      queryBuilder.andWhere('tags.id in (:...tagIds)', {
        tagIds: query.tagIds,
      });
    }

    if (query.graduated === 'true') {
      queryBuilder.andWhere('enrollments.graduated_at is not null');
    }

    if (query.withCert === 'false' && query.studentCourseId) {
      queryBuilder.leftJoin('user.certs', 'cert', 'cert.course_id = :studentCourseId');
      queryBuilder.andWhere('cert.user_id is null');
    } else if (query.withCert && query.studentCourseId) {
      queryBuilder.innerJoin('user.certs', 'cert', 'cert.course_id = :studentCourseId');
    }

    if (query.webinarId) {
      const webinarSubscribers = (await this.webinarSubscribers(query.webinarId, { takeAll: true })) || [];
      const webinarSubscribersIds = webinarSubscribers.map(subscriber => subscriber.id);
      queryBuilder.andWhereInIds(webinarSubscribersIds);
    }
  }

  async findAll(paging: PaginationDto, query: QueryUserDto): Promise<Paginated<User[]>> {
    const queryBuilder = this.usersRepository.createQueryBuilder('user');
    this.joinAndSelectUserRelations(queryBuilder, query);
    await this.applyUserFilters(queryBuilder, query);

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    const [results, total] = await queryBuilder.getManyAndCount();
    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findAllWithoutSelectedRelations(dto: CreateFeatureEntryByUserFilterDto) {
    const queryBuilder = this.usersRepository.createQueryBuilder('user');
    this.joinUserRelations(queryBuilder, dto);
    await this.applyUserFilters(queryBuilder, dto);
    return queryBuilder.getMany();
  }

  async findOne(id: string, includeExpiredEnrollments = false, includeInactiveEnrollments = false): Promise<User> {
    let enrollmentWhere = includeInactiveEnrollments ? '' : 'enrollments.active = TRUE';

    if (!includeExpiredEnrollments)
      enrollmentWhere +=
        ' AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())' +
        ' AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW())';

    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.modules', 'modules')
      .leftJoinAndSelect('user.certs', 'certs')
      .leftJoinAndSelect('certs.file', 'file')
      .leftJoinAndSelect('user.profile', 'profile')
      // .leftJoinAndSelect('modules.course', 'course') course is undefined
      .leftJoinAndSelect('user.avatar', 'avatar')
      .leftJoinAndSelect('user.enrollments', 'enrollments', enrollmentWhere)
      .leftJoinAndSelect('enrollments.course', 'course')
      .leftJoinAndSelect('user.tags', 'tags')
      .leftJoinAndSelect('tags.category', 'tag_categories')
      .leftJoinAndSelect('user.blacklistedStudents', 'blacklistedStudents')
      .where('user.id = :userId', { userId: id });

    let user;

    // Run on Master
    const masterQueryRunner = getConnection().createQueryRunner('master');
    try {
      queryBuilder.setQueryRunner(masterQueryRunner);
      user = await queryBuilder.cache(`db:user:${id}:${includeExpiredEnrollments}`).getOne();
    } finally {
      await masterQueryRunner.release();
    }

    const features = await this.findUserFeatureFlags(id);

    if (!user) {
      throw new BadRequestException('User not found');
    }

    user.features = features.map(f => f.name);

    if (user && user.roles.some(r => r.role === SystemRole.Teamlead)) {
      user.teamleadTutorIds = await getTeamleadTutorIds(user.id, new Date());
      user.teamleadCourseIds = await getTeamleadCourseIds(user.id, new Date());
    }

    const { password, ...userWithoutPassword } = user;

    return userWithoutPassword as User;
  }

  async findUserFeatureFlags(userId: string) {
    const appRoles: AppRole[] & SystemRole[] = [];
    const systemRoles = await this.rolesRepository
      .createQueryBuilder('role')
      .leftJoinAndSelect('role.user', 'user')
      .leftJoinAndSelect('user.enrollments', 'enrollment')
      .where('user.id = :userId', { userId })
      .andWhere('(role.dateTo IS NULL OR role.dateTo > NOW())')
      .andWhere('(role.dateFrom IS NULL OR role.dateFrom <= NOW())')
      .getMany();

    const systemRolesNames = systemRoles.map(value => value.role);

    const hasStudentRole = await this.userIsStudent(userId);
    if (hasStudentRole) {
      appRoles.push(AppRole.Student);
    }
    appRoles.push(...systemRolesNames);

    return this.featureFlagRepository
      .createQueryBuilder('feature')
      .leftJoin('feature.entries', 'entry', 'entry.userId = :userId AND entry.enabled', { userId })
      .leftJoin('feature.roleEntries', 'roleEntry', 'feature.name = roleEntry.featureName')
      .where('feature.enabled = TRUE')
      .andWhere(
        new Brackets(qb => {
          qb.andWhere('entry.userId = :userId AND feature.strategy = :userIdsStrategy', {
            userId,
            userIdsStrategy: FeatureStrategy.UserIds,
          });
          if (appRoles.length > 0) {
            qb.orWhere('roleEntry.role IN (:...userRoles) AND feature.strategy = :userRolesStrategy', {
              userRoles: appRoles,
              userRolesStrategy: FeatureStrategy.UserRoles,
            });
          }
          qb.orWhere('feature.strategy = :allUsersStrategy', {
            allUsersStrategy: FeatureStrategy.AllUsers,
          });
        }),
      )
      .getMany();
  }

  async hasFeatureAccess(userId: string, featureName: FeatureName): Promise<boolean> {
    const userFeatureFlags = await this.findUserFeatureFlags(userId);
    return userFeatureFlags.some(feature => feature.name === featureName);
  }

  findByRole(role: SystemRole, courseId = '', limit = null): Promise<User[]> {
    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.modules', 'modules')
      .leftJoinAndSelect('modules.course', 'course')
      .where('roles.role = :role', { role })
      .andWhere('(roles.date_from IS NULL OR roles.date_from <= NOW())')
      .andWhere('(roles.date_to IS NULL OR roles.date_to > NOW())')
      .andWhere('user.active = true')
      .leftJoinAndSelect(
        'user.enrollments',
        'enrollments',
        'enrollments.active = TRUE' +
          ' AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())' +
          ' AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW())',
      )
      .orderBy('user.name');
    if (courseId) {
      queryBuilder.andWhere('enrollments.courseId = :courseId', { courseId });
    }
    if (limit) {
      queryBuilder.limit(limit);
    }

    return queryBuilder.getMany();
  }

  findOneByRole(role: SystemRole) {
    return this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .where('roles.role=:role', { role })
      .andWhere('(roles.date_from IS NULL OR roles.date_from <= NOW())')
      .andWhere('(roles.date_to IS NULL OR roles.date_to > NOW())')
      .andWhere('user.active = true')
      .getOne();
  }

  async findOneByInterappToken(interappToken: string): Promise<User> {
    const interappUser = await this.usersRepository.findOne({ interappToken });
    if (!interappUser?.id) {
      return;
    }
    const user = await this.findOne(interappUser.id);

    return user;
  }

  // выбираем минимум полей и только АКТИВНЫХ туторов
  getTutorsLoadFindByRole(courseId?: string): Promise<User[]> {
    let queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.modules', 'modules')
      .leftJoinAndSelect('user.moduleAssignments', 'moduleAssignments')
      .leftJoinAndSelect('user.sectionAssignments', 'sectionAssignments')
      .leftJoinAndSelect('sectionAssignments.section', 'assignedSection')
      .leftJoinAndSelect('assignedSection.modules', 'assignedModulesBySection')
      .leftJoinAndSelect('modules.course', 'course')
      .leftJoinAndSelect('user.blacklistedStudents', 'blacklistedStudents')
      .leftJoinAndSelect('moduleAssignments.module', 'assignedModule')
      .where('user.active = TRUE')
      // .andWhere('user.pause = FALSE')
      .andWhere('roles.role = :role', { role: SystemRole.Tutor })
      .andWhere('(roles.date_from IS NULL OR roles.date_from <= NOW())')
      .andWhere('(roles.date_to IS NULL OR roles.date_to > NOW())')
      .leftJoinAndSelect(
        'user.enrollments',
        'enrollments',
        'enrollments.active = TRUE' +
          ' AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())' +
          ' AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW())' +
          ' AND enrollments.role = :roleEnrollment',
        { roleEnrollment: CourseRole.Tutor },
      )
      .orderBy('user.name')
      .select([
        'user.id',
        'user.name',
        'user.pause',
        'enrollments.courseId',
        'modules.id',
        'course.id',
        'blacklistedStudents.studentId',
        'moduleAssignments.moduleId',
        'moduleAssignments.exclusive',
        'sectionAssignments.sectionId',
        'sectionAssignments.exclusive',
        'assignedModule.courseId',
        'assignedSection.courseId',
      ]);

    if (courseId) {
      queryBuilder = queryBuilder.andWhere('enrollments.courseId = :courseId', { courseId });
    }

    return queryBuilder.getMany();
  }

  /** This one returns password along for AuthService */
  @Transactional()
  async findOneByEmail(email: string): Promise<User> {
    const user = await this.usersRepository.findOne({
      email: email.toLowerCase(),
    });
    if (!user && email == process.env.DEFAULT_ADMIN_EMAIL) {
      const usersCount = await this.rolesRepository.count({
        role: SystemRole.Superadmin,
      });
      if (usersCount === 0) {
        const defaultAdmin = new CreateUserDto();
        defaultAdmin.name = 'Default admin';
        defaultAdmin.active = true;
        defaultAdmin.email = process.env.DEFAULT_ADMIN_EMAIL;
        defaultAdmin.password = generatePasswordHash(process.env.DEFAULT_ADMIN_PASSWORD);
        const role = new Role();
        role.role = SystemRole.Superadmin;
        defaultAdmin.roles = [role];
        return this.usersRepository.save(defaultAdmin);
      }
    }
    return user;
  }

  @Transactional()
  async findOneByExernalId(externalId: string) {
    return this.usersRepository.findOne({ externalId });
  }

  async findManyByExernalId(externalIds: string[]) {
    return this.usersRepository.find({ externalId: In(externalIds) });
  }

  status(): Promise<number> {
    return getManager().query('select now()');
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    await this.clearCache(id);

    const entity = { ...updateUserDto, id };
    if (updateUserDto.password) {
      entity.password = generatePasswordHash(updateUserDto.password);
    }
    if (updateUserDto.email) {
      entity.email = entity.email.toLowerCase();
    }
    return this.usersRepository.save(entity);
  }

  updateAccessDate(userId: string) {
    return this.usersRepository.update(userId, { lastAccessed: new Date() });
  }

  async updateSettings(userId, settings: UserSettings) {
    await this.clearCache(userId);
    const entity = { settings };
    return this.usersRepository.update(userId, entity);
  }

  async setPause(id: string, pause: boolean) {
    await this.clearCache(id);
    return this.usersRepository.update(id, { pause });
  }

  remove(id: string) {
    return this.usersRepository.delete(id);
  }

  removeList(users: User[]) {
    return this.usersRepository.remove(users);
  }

  purgeAll() {
    return this.usersRepository.clear();
  }

  eadBoxFindManyByExternalIds(externalIds: string[]) {
    return this.usersRepository.find({
      relations: ['avatar'],
      where: {
        externalId: In(externalIds),
      },
    });
  }

  eadBoxUsersEnrollToCourseByName(userNames, courseId): Promise<User[]> {
    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .setParameter('courseId', courseId)
      .innerJoinAndSelect('user.enrollments', 'enrollments', 'enrollments.courseId = :courseId', { courseId })
      .where('user.name  in (:...userNames)', { userNames })
      .select(['user.id', 'user.externalId', 'user.name', 'user.avatar_id as avatarId'])
      .groupBy('user.id, user.externalId, user.name, user.avatar_id');

    return queryBuilder.getMany();
  }

  eadBoxUsersUniqueByName(userNames): Promise<User[]> {
    const subquery = this.usersRepository
      .createQueryBuilder('user')
      .select('user.name')
      .where('user.name  in (:...userNames)', { userNames })
      .groupBy('user.name')
      .andHaving('COUNT(user.name) = 1');

    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .setParameter('userNames', userNames)
      .where(`user.name in (${subquery.getQuery()})`);

    return queryBuilder.getMany();
  }

  eadBoxFindManyAvatars() {
    return this.usersRepository.find({
      relations: ['avatar'],
      where: {
        externalId: Not(IsNull()),
      },
    });
  }

  eadBoxFindManyWithExternalId() {
    return this.usersRepository.find({
      where: {
        externalId: Not(IsNull()),
      },
    });
  }

  async removeSelfAvatar(id) {
    const user = await this.usersRepository.findOne(id, {
      relations: ['avatar'],
    });
    if (user.avatar) {
      await this.usersRepository.save({
        id,
        avatarId: null,
      });
      return this.filesService.deleteOneStorage(user.avatar);
    }
  }

  async uploadSelfAvatar(id, base64, contentType) {
    const user = await this.usersRepository.findOne(id, {
      relations: ['avatar'],
    });

    let uploadedFile;

    try {
      uploadedFile = await this.filesService.storage.uploadBase64(base64, contentType, null, 'avatars');
    } catch (error) {
      throw new UnprocessableEntityException('USERS.AVATAR_UPDATE.CANT_UPLOAD_STORAGE');
    }

    const dbFile = await this.filesService.create({
      ...uploadedFile,
      ownerId: id,
    });

    await this.usersRepository.save({
      id,
      avatar: dbFile,
    });

    if (user.avatar) {
      try {
        await this.filesService.deleteOneStorage(user.avatar);
      } catch (error) {
        console.log('Cant delete avatar:', error);
      }
    }

    return dbFile;
  }

  async updateModules(id, updateModulesDto: UpdateModulesDto) {
    return await this.usersRepository.save({
      id,
      modules: updateModulesDto.modules,
    });
  }

  async compareGradeUsers(user1: User, user2: User) {
    const gradeUser1 = await this.getGradeUser(user1);
    const gradeUser2 = await this.getGradeUser(user2);
    return gradeUser1 - gradeUser2;
  }

  async getGradeUser(user: User) {
    if (user?.roles.length > 0) {
      return mapGradeForRoles(user.roles);
    } else return 0;
  }
  async findAllWithNotS3Avatar() {
    return this.usersRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.avatar', 'avatar')
      .where('avatar.s3key is null')
      .getMany();
  }

  async findAllWithS3Avatar() {
    return this.usersRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.avatar', 'avatar')
      .where('avatar.s3key is not null')
      .getMany();
  }

  async webinarSubscribers(webinarId: string, query: QueryWebinarUsersDTO) {
    const webinar = await this.webinarService.findOne(webinarId);

    const params = {
      token: process.env.WEBINAR_ACCESS_KEY,
      eventId: webinar.externalId,
      limit: query.take,
      offset: query.skip,
      email: query.email,
      takeAll: query.takeAll,
    };

    const totalSubscribers = [];

    while (true) {
      const subscribers = await axios
        .post(`https://gdd.ebaconline.com.br/api/get_event_subscriptions`, {
          ...params,
        })
        .then(it => {
          return it.data.subscriptions;
        })
        .catch(err => {
          if (['invalid token', 'jwt malformed'].includes(err.response.data.error)) {
            console.warn(
              `${process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL} errored on request. Seems like auth token error. Check env WEBINAR_ACCESS_KEY value`,
            );
          } else {
            console.log(err);
            console.error(`${process.env.WEBINAR_GDD_SUBSCRIPTIONS_URL} errored on request.`);
          }

          console.log('Assuming empty response\n\n');

          return [];
        });

      totalSubscribers.push(...subscribers);

      if (process.env.EMAILS_ALLOWED) {
        process.env.EMAILS_ALLOWED.split(',').map(e => {
          totalSubscribers.push({ user_email: e.trim() });
        });
      }

      if (subscribers.length < params.limit || 1000) {
        break;
      } else if (params.takeAll) {
        params.offset += 1000;
      } else {
        params.offset += params.limit;
      }
    }

    if (totalSubscribers.length === 0) return [];

    const queryEmails: Array<{ email: string }> = totalSubscribers.map(s => {
      return {
        email: s.user_email,
      };
    });

    const userQb = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.submissions', 'submissions', 'submissions.webinarId = :webinarId', { webinarId })
      .leftJoinAndSelect('submissions.assessment', 'assessment', 'assessment.isWebinarSurvey = TRUE')
      .where(queryEmails);

    this.applyPrintedCertsFilter(userQb, query, webinarId);
    return userQb.getMany();
  }

  findAllCompletedWebinarSurvey(webinarId: string, query: QueryUsersCompletedSurveyDto) {
    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.submissions', 'submissions')
      .where('submissions.webinarId = :webinarId', { webinarId })
      .andWhere('submissions.status = :submittedStatus', {
        submittedStatus: SubmissionStatus.Graded,
      })
      .leftJoinAndSelect('submissions.assessment', 'assessment', 'assessment.isWebinarSurvey = TRUE');

    this.applyPrintedCertsFilter(queryBuilder, query, webinarId);
    return queryBuilder.getMany();
  }

  applyPrintedCertsFilter(queryBuilder: SelectQueryBuilder<User>, query, webinarId: string) {
    // Если у всех сертификатов есть файл
    if (query.printedCertificates === true) {
      queryBuilder
        .innerJoinAndSelect('user.certs', 'cert')
        .leftJoinAndSelect('cert.file', 'file')
        .andWhere(qb => `NOT EXISTS ${this.certsWithoutFileSubQuery(qb)}`);
      // Если хоть у одного сертификата отсутствует файл, либо нет сертификата
    } else if (query.printedCertificates === false) {
      queryBuilder
        .leftJoinAndSelect('user.certs', 'cert')
        .leftJoinAndSelect('cert.file', 'file')
        .andWhere(qb => `(NOT EXISTS ${this.certsWithFileSubQuery(qb, webinarId)} OR cert.id IS NULL)`);
    } else {
      queryBuilder.leftJoinAndSelect('user.certs', 'cert').leftJoinAndSelect('cert.file', 'file');
    }
  }

  certsWithoutFileSubQuery(queryBuilder: SelectQueryBuilder<User>) {
    return queryBuilder
      .subQuery()
      .select()
      .from(Cert, 'cert')
      .where('cert.user_id = user.id')
      .leftJoin('cert.file', 'file')
      .andWhere('file IS NULL')
      .getQuery();
  }

  certsWithFileSubQuery(queryBuilder: SelectQueryBuilder<User>, webinarId: string) {
    return queryBuilder
      .subQuery()
      .select()
      .from(Cert, 'cert')
      .where('cert.user_id = user.id')
      .leftJoin('cert.file', 'file')
      .andWhere('file.id IS NOT NULL')
      .andWhere('cert.webinar_id = :webinarId', { webinarId })
      .getQuery();
  }

  async searchAll(search: string) {
    return this.usersRepository
      .createQueryBuilder('users')
      .orWhere("LOWER(CONCAT(name, ' ', email, ' ', id)) like :nameLike", {
        nameLike: `%${search.toLowerCase()}%`,
      })
      .getMany();
  }

  async findResponsibles() {
    return this.usersRepository
      .createQueryBuilder('users')
      .leftJoinAndSelect('users.roles', 'roles')
      .where('roles.role in (:...responibleRoles)', { responibleRoles: [AppRole.Admin, AppRole.Support, AppRole.Tutor] })
      .orderBy('users.name', 'ASC')
      .getMany();
  }

  async findOneForSupport(id: string): Promise<User> {
    // todo remove this
    const maybeIncludeHidden = (qb: SelectQueryBuilder<any>) => qb;

    const queryBuilder = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.modules', 'modules')
      .leftJoinAndSelect('user.profile', 'profile')
      .leftJoinAndSelect('modules.course', 'course')
      .leftJoinAndSelect('user.activity', 'activity')
      .leftJoinAndSelect('user.lectureProgress', 'lectureProgress')
      .leftJoinAndSelect('user.enrollments', 'enrollments', 'enrollments.active = TRUE')
      .where('user.id = :userId', { userId: id })
      .leftJoinAndSelect('enrollments.course', 'enrollmentsCourse')
      .leftJoinAndSelect('enrollmentsCourse.lectures', 'lectures')
      .andWhere('(roles.date_from IS NULL OR roles.date_from <= NOW())')
      .andWhere('(roles.date_to IS NULL OR roles.date_to > NOW())');

    return await queryBuilder.getOne();
  }

  // список курсов к которым имеют доступ туторы тимлида
  async getCoursesForTutorsTeamlead(teamleadId: string): Promise<any> {
    const queryBuilder = getConnection()
      .createQueryBuilder()
      .select('distinct enrollments.course')
      .from(Hierarchy, 'hierarchy')
      .where('hierarchy.parent = :teamleadId AND hierarchy.role = :role', {
        teamleadId,
        role: HierarchyRole.Teamlead,
      })
      .leftJoin('hierarchy.child', 'user')
      .innerJoin(
        'user.enrollments',
        'enrollments',
        'enrollments.active = TRUE AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW()) AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW())',
      );

    const result = await queryBuilder.getRawMany();
    return result.map(e => e.course_id);
  }

  async clearCache(userId: string) {
    try {
      await getConnection().queryResultCache.remove([`db:user:${userId}:false`]);
      await getConnection().queryResultCache.remove([`db:user:${userId}:true`]);
    } catch (error) {
      console.error(error);
    }
  }

  async attachTag(userId: string, tagId: string) {
    return this.findUserAndRun(userId, ['tags'], user => this.attachTagToUser(user, tagId));
  }

  async detachTag(userId: string, tagId: string) {
    return this.findUserAndRun(userId, ['tags'], user => this.detachTagFromUser(user, tagId));
  }

  async findUserAndRun(userId: string, relations: string[], func: any) {
    const user = await this.usersRepository.findOne(userId, {
      relations,
    });

    assert(user, new NotFoundException('User is not found'));

    return func(user);
  }

  async attachTagToUser(user: User, tagId: string) {
    const existedTag = user.tags.find(tag => tag.id === tagId);

    if (existedTag) {
      return existedTag;
    }

    const tag = await this.tagsService.findOne(tagId);

    assert(tag, new NotFoundException('Tag is not found'));

    if (tag && tag.category.type !== TagType.UserGroup) {
      throw new BadRequestException('The tag could not be assigned to the user');
    }

    user.tags.push(tag);

    await this.usersRepository.save(user);

    return tag;
  }

  async detachTagFromUser(user: User, tagId: string) {
    user.tags = user.tags.filter(tag => tag.id !== tagId);
    await this.usersRepository.save(user);
    return user.tags;
  }

  async checkUser(userId: string) {
    const user = await this.usersRepository.findOne(userId);
    if (!user) {
      throw new NotFoundException(`User ${userId} not found`);
    }
    return user;
  }
}
