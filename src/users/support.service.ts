import { Assessment } from '@entities/assessment.entity';
import { Enrollment } from '@entities/enrollment.entity';
import { LectureProgress } from '@entities/lecture-progress.entity';
import { Lecture } from '@entities/lecture.entity';
import { Module } from '@entities/module.entity';
import { Question } from '@entities/question.entity';
import { Section } from '@entities/section.entity';
import { Submission } from '@entities/submission.entity';
import { User } from '@entities/user.entity';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CoursesService } from 'src/courses/courses.service';
import { In, Repository } from 'typeorm';

@Injectable()
export class SupportService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Enrollment)
    private enrollmentRepository: Repository<Enrollment>,
    @InjectRepository(Module)
    private moduleRepository: Repository<Module>,
    @InjectRepository(Lecture)
    private lectureRepository: Repository<Lecture>,
    @InjectRepository(Section)
    private sectionRepository: Repository<Section>,
    @InjectRepository(Assessment)
    private assessmentRepository: Repository<Assessment>,
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,
    @InjectRepository(LectureProgress)
    private lectureProgressRepository: Repository<LectureProgress>,
    @Inject(forwardRef(() => CoursesService))
    private readonly coursesService: CoursesService,
  ) {}

  async getProfile(userId: string, isImmediate = true): Promise<any> {
    // Get user & their enrollments
    const [user, enrollments] = await Promise.all([this.getUser(userId), this.getEnrollments(userId)]);

    // Get assessments, submissions & lecture progress by lectures
    const courseIds = enrollments.reduce((acc, e) => acc.concat(e.course.id), []).filter((id, idx, self) => self.indexOf(id) === idx); // Unique ids

    const [assessments, submissions, lectureProgress] = !courseIds.length
      ? [[], [], []]
      : await Promise.all([this.getAssessments(courseIds), this.getSubmissions(userId), this.getLectureProgress(userId)]);

    // User progress counters
    let coursesProgress;

    if (isImmediate) {
      coursesProgress = await this.coursesService.viewUserProgressCount(userId);
    } else {
      coursesProgress = await this.coursesService.viewUserProgressCountMaterialized(userId);
    }

    return {
      user,
      enrollments,
      assessments,
      submissions,
      lectureProgress,
      coursesProgress,
    };
  }

  async getUser(userId: string) {
    const q = this.usersRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.activity', 'activity')
      .leftJoinAndSelect('user.certs', 'certs')
      .leftJoinAndSelect('certs.file', 'file')
      .loadRelationCountAndMap('user.viewedLecturesCount', 'user.lectureProgress')
      .loadRelationCountAndMap('user.notesCount', 'user.notes')
      .loadRelationCountAndMap('user.forumMessagesCount', 'user.forumMessages')
      .where('user.id=:userId', { userId });
    const res = await q.getOne();
    return res;
  }

  async getEnrollments(userId: string) {
    const enrollments = await this.enrollmentRepository.find({
      relations: ['course', 'course.image'],
      where: { userId, role: CourseRole.Student },
    });

    const courseIds = enrollments.map(e => e.course.id);

    const sections = await this.sectionRepository.find({
      where: { course: { id: In(courseIds) } },
    });

    const lectures = await this.lectureRepository.find({
      where: { course: { id: In(courseIds) } },
    });

    const modules = await this.moduleRepository.find({
      where: { course: { id: In(courseIds) } },
    });

    return enrollments.map(e => ({
      ...e,
      course: {
        ...e.course,
        lectures: lectures.filter(l => l.courseId === e.course.id),
        modules: modules.filter(m => m.courseId === e.course.id),
        sections: sections.filter(s => s.courseId === e.course.id),
      },
    }));
  }

  async getAssessments(courseIds: string[]) {
    const result = await this.assessmentRepository
      .createQueryBuilder('assessment')
      .select([
        'assessment.id "id"',
        'assessment.type "type"',
        'assessment.passing_score "passing_score"',
        `json_build_object(
          'id', assessment.lecture_id
        ) "lecture"`,
        'assessment.current_version "current_version"',
        'assessment.common_id "common_id"',
        'assessment.created "created"',
        '(lecture.required AND lecture.active AND module.active) as required',
      ])
      .innerJoin('content', 'content', 'content.lecture_id = assessment.lecture_id and content.assessment_id = assessment.id')
      .leftJoin('lecture', 'lecture', 'lecture.id = assessment.lecture_id')
      .leftJoin('module', 'module', 'lecture.module_id = module.id')
      .where('lecture.course_id IN (:...courseIds)', { courseIds })
      .orderBy('assessment.lecture_id')
      .getRawMany();

    // Snake case to camelCase
    return result.map((a: any) => ({
      id: a.id,
      type: a.type,
      passingScore: a.passing_score,
      lecture: { id: a.lecture.id },
      currentVersion: a.current_version,
      commonId: a.common_id,
      created: a.created,
      required: a.required,
    }));
  }

  async getSubmissions(userId: string) {
    return this.submissionsRepository
      .createQueryBuilder('submission')
      .select([
        'submission.id "id"',
        'submission.score "score"',
        'submission.status "status"',
        'submission.success "success"',
        'submission.current_attempt "current_attempt"',
        'submission.correct_answer_count "correct_answer_count"',
        'submission.created "created"',
        'submission.graded_at "gradedAt"',
        'submission.attempt "attempt"',
        'submission.is_question "isQuestion"',
        `json_build_object(
          'id', submission.course_id
        ) "course"`,
        `json_build_object(
          'id', submission.module_id
        ) "module"`,
        `json_build_object(
          'id', submission.lecture_id
        ) "lecture"`,
        `json_build_object(
          'id', assessment.id,
          'type', assessment.type,
          'questionCount', question.count,
          'commonId', assessment.common_id
        ) "assessment"`,
        `json_build_object(
          'id', tutor.id,
          'name', tutor.name
        ) "tutor"`,
      ])
      .leftJoin('assessment', 'assessment', 'assessment.id=submission.assessment_id')
      .leftJoin(
        qb =>
          qb
            .select(['COUNT(questionsub) count', 'questionsub.assessment_id as assessment_id'])
            .from(Question, 'questionsub')
            .groupBy('questionsub.assessment_id'),
        'question',
        'question.assessment_id=submission.assessment_id',
      )
      .leftJoin('user', 'tutor', 'tutor.id = submission.tutor_id')
      .where('submission.user_id=:userId', { userId })
      .orderBy('submission.course_id, submission.module_id, submission.lecture_id, submission.attempt')
      .getRawMany<Submission>();
  }

  async getLectureProgress(userId: string) {
    return this.lectureProgressRepository.find({
      select: ['status', 'progress', 'viewed', 'lectureId'],
      where: { userId },
    });
  }
}
