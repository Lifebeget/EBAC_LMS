import { Hierarchy } from '@entities/hierarchy.entity';
import { HierarchyRole } from '@enums/hierarchy-role.enum';
import { SystemRole } from '@lib/api/types';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as assert from 'assert';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { Repository } from 'typeorm';
import { CreateHierarchyDto } from './dto/create-hierarchy.dto';
import { UpdateHierarchyDto } from './dto/update-hierarchy.dto';
import { RolesService } from './roles.service';
import { UsersService } from './users.service';

@Injectable()
export class HierarchysService {
  constructor(
    @InjectRepository(Hierarchy)
    private readonly hierarchyRepository: Repository<Hierarchy>,
    private readonly usersService: UsersService,
    private readonly rolesService: RolesService,
    private readonly enrollmentsService: EnrollmentsService,
  ) {}

  findAll() {
    return this.hierarchyRepository.find();
  }

  findOne(id: string) {
    return this.hierarchyRepository.findOne(id);
  }

  async create(dto: CreateHierarchyDto) {
    return this.hierarchyRepository.save({
      ...dto,
      parent: { id: dto.parentId },
      child: { id: dto.childId },
    });
  }

  // This uses Tutor-to-teamlead relation table
  findTeamleadTutors(teamleadId) {
    return this.hierarchyRepository.find({
      where: { parent: { id: teamleadId } },
      relations: ['child'],
    });
  }

  assign(parentId: string, childId: string, role: HierarchyRole) {
    return this.create({ parentId, childId, role });
  }

  async assignTeamlead(teamleadId: string, tutorId: string) {
    const existed = await this.hierarchyRepository.findOne({
      where: {
        parent: { id: teamleadId },
        child: { id: tutorId },
        role: HierarchyRole.Teamlead,
      },
    });

    assert(!existed, 'Hierarchy allready exists');

    const tutor = await this.usersService.findOne(tutorId);

    assert(
      tutor.roles.some(r => r.role === SystemRole.Tutor),
      'Teamlead can be assigned only for tutor',
    );

    const teamlead = await this.usersService.findOne(teamleadId);

    const allreadyTeamlead = teamlead.roles.some(r => r.role === SystemRole.Teamlead);

    if (!allreadyTeamlead) {
      await this.rolesService.create(teamleadId, { role: SystemRole.Teamlead });
    }

    return this.assign(teamleadId, tutorId, HierarchyRole.Teamlead);
  }

  async releaseTeamlead(teamleadId: string) {
    const teamlead = await this.usersService.findOne(teamleadId);

    assert(
      teamlead.roles.some(r => r.role === SystemRole.Teamlead),
      'User is not a teamlead',
    );

    const teamleadRole = teamlead.roles.find(r => r.role === SystemRole.Teamlead);

    this.rolesService.remove(teamleadRole.id);
    return this.hierarchyRepository.delete({ parent: { id: teamleadId } });
  }

  async releaseTeamleadTutor(teamleadId: string, tutorId: string) {
    return this.hierarchyRepository.delete({
      parent: { id: teamleadId },
      child: { id: tutorId },
    });
  }

  update(id: string, updateHierarchyDto: UpdateHierarchyDto) {
    return this.hierarchyRepository.save({ ...updateHierarchyDto, id });
  }

  remove(id: string) {
    return this.hierarchyRepository.delete(id);
  }
}
