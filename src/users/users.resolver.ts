import { Permission } from '@enums/permission.enum';
import { Resolver, Args, Query, ResolveField, Parent } from '@nestjs/graphql';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { User } from '../lib/entities/user.entity';
import { UsersService } from './users.service';

import { EnrollmentsService } from 'src/courses/enrollments.service';

import { RolesService } from './roles.service';

@Resolver(of => User)
export class UsersResolver {
  constructor(
    private readonly usersService: UsersService,
    private readonly enrollmentsService: EnrollmentsService,
    private readonly rolesService: RolesService,
  ) {}

  @RequirePermissions(Permission.USERS_VIEW)
  @Query(() => User)
  async user(@Args('id') id: string): Promise<User> {
    return await this.usersService.findOne(id);
  }

  @ResolveField()
  async enrollments(@Parent() user: User) {
    const { id } = user;
    return await this.enrollmentsService.findAll(id);
  }

  @ResolveField()
  async roles(@Parent() user: User) {
    const { id } = user;
    return await this.rolesService.findByUser(id);
  }
}
