import { SystemRole } from '@lib/api/types';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  UnauthorizedException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { checkRoleset } from 'src/utils/user.helper';
import { Role } from '../lib/entities/role.entity';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { CreateRoleDto } from './dto/create-role.dto';
import { QueryRoleDto } from './dto/query-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RolesService } from './roles.service';

@ApiTags('users')
@ApiBearerAuth()
@Controller('')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Post('users/:userId/roles')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Grant a role to user' })
  @ApiResponse({
    status: 201,
    description: 'Created role',
    type: Role,
  })
  @PgConstraints()
  create(@Param('userId') userId: string, @Body() createRoleDto: CreateRoleDto, @Req() req: Express.Request) {
    // Limitations on adding roles
    if (!checkRoleset(req.user.parsedRoles, [createRoleDto.role])) {
      throw new UnauthorizedException("You can't set that role");
    }
    return this.rolesService.create(userId, createRoleDto);
  }

  @Get('users/:userId/roles')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get roles for user' })
  @ApiResponse({
    status: 201,
    description: 'Roles',
    type: [Role],
  })
  @PgConstraints()
  findByUser(@Param('userId') userId: string) {
    return this.rolesService.findByUser(userId);
  }

  @Get('roles')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'List all roles' })
  @ApiResponse({
    status: 200,
    type: [Role],
  })
  @ApiQuery({
    name: 'userId',
    description: 'Filter by user',
    required: false,
  })
  @ApiQuery({
    name: 'role',
    description: 'Filter by role',
    enum: SystemRole,
    required: false,
  })
  @ApiQuery({
    name: 'showInactive',
    description: 'Include inactive roles',
    enum: ['true', 'false'],
    required: false,
  })
  findAll(@Query() query: QueryRoleDto) {
    return this.rolesService.findAll(query);
  }

  @Get('roles/:id')
  @RequirePermissions(Permission.USERS_VIEW)
  @ApiOperation({ summary: 'Get one role by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a role by id',
    type: Role,
  })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.rolesService.findOne(id);
  }

  @Patch('roles/:id')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Updates a role' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateRoleDto: UpdateRoleDto, @Req() req: Express.Request) {
    // Check that role type doesn't change
    if (updateRoleDto.hasOwnProperty('role')) {
      throw new BadRequestException("You can't change existent role type");
    }
    // Check the role first, may be the user is not allowed to edit it
    const role = await this.rolesService.findOne(id);
    const roles = await this.rolesService.findByUser(role.user.id);
    if (
      !checkRoleset(
        req.user.parsedRoles,
        roles.map(r => r.role),
      )
    ) {
      throw new UnauthorizedException("You can't change that role");
    }

    // Otherwise, it's ok to update
    return this.rolesService.update(id, updateRoleDto);
  }

  @Delete('roles/:id')
  @RequirePermissions(Permission.USERS_MANAGE, Permission.SUPPORT_MANAGE)
  @ApiOperation({ summary: 'Deletes a role' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Express.Request) {
    // Check the role first, only superadmins can remove admins
    const role = await this.rolesService.findOne(id);
    const roles = await this.rolesService.findByUser(role.user.id);
    if (
      !checkRoleset(
        req.user.parsedRoles,
        roles.map(r => r.role),
      )
    ) {
      throw new UnauthorizedException("You can't remove that role");
    }

    // Otherwise it's ok to remove
    return this.rolesService.remove(id);
  }
}
