import { Hierarchy } from '@entities/hierarchy.entity';
import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FilesModule } from 'src/files/files.module';
import { Profile } from 'src/lib/entities/profile.entity';
import { Role } from 'src/lib/entities/role.entity';
import { Token } from 'src/lib/entities/token.entity';
import { UserActivity } from 'src/lib/entities/user-activity.entity';
import { User } from 'src/lib/entities/user.entity';
import { BlacklistItem } from '@entities/blacklist.entity';
import { HierarchysController } from './hierarchy.controller';
import { HierarchysService } from './hierarchy.service';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { TokensService } from './tokens.service';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { BlacklistService } from './blacklist.service';
import { BlacklistController } from './blacklist.controller';
import { ModuleAssignment } from 'src/lib/entities/module-assignment.entity';
import { ModuleAssignmentController } from './module-assignment.controller';
import { ModuleAssignmentService } from './module-assignment.service';
import { SectionAssignment } from 'src/lib/entities/section-assignment.entity';
import { SectionAssignmentController } from './section-assignment.controller';
import { SectionAssignmentService } from './section-assignment.service';
import { Module as ModuleEntity } from '../lib/entities/module.entity';
import { UsersResolver } from './users.resolver';
import { Enrollment } from '@entities/enrollment.entity';
import { Webinar } from '@entities/webinars.entity';
import { Trigger } from '@entities/trigger.entity';
import { Course, CourseToSatellite } from '@entities/course.entity';
import { Lecture } from '@entities/lecture.entity';
import { Tag } from '@entities/tag.entity';
import { Content } from '@entities/content.entity';
import { JwtModule } from '@nestjs/jwt';
import { AmoModule } from '../amo/amo.module';
import { SettingsModule } from 'src/settings/settings.module';
import { AssessmentsModule } from 'src/assessments/assessments.module';
import { SupportService } from './support.service';
import { LectureProgress } from '@entities/lecture-progress.entity';
import { File } from '@entities/file.entity';
import { Cover } from '@entities/cover.entity';
import { Section } from '@entities/section.entity';
import { CoursesModule } from 'src/courses/courses.module';
import { TagsModule } from 'src/tags/tags.module';
import { TagCategory } from '@entities/tag-category.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { FeatureFlagEntry } from '@entities/feature-flag-entry.entity';
import { FeatureFlag } from '@entities/feature-flag.entity';
import { Segment } from '@entities/segment.entity';
import { WebinarModule } from 'src/webinar/webinar.module';
import { TriggerModule } from 'src/trigger/trigger.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Role]),
    TypeOrmModule.forFeature([UserActivity]),
    TypeOrmModule.forFeature([Token]),
    TypeOrmModule.forFeature([Profile]),
    TypeOrmModule.forFeature([Hierarchy]),
    TypeOrmModule.forFeature([Enrollment]),
    TypeOrmModule.forFeature([Webinar]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([Course]),
    TypeOrmModule.forFeature([CourseToSatellite]),
    TypeOrmModule.forFeature([ModuleEntity]),
    TypeOrmModule.forFeature([Lecture]),
    TypeOrmModule.forFeature([LectureProgress]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([Content]),
    TypeOrmModule.forFeature([File]),
    TypeOrmModule.forFeature([Cover]),
    TypeOrmModule.forFeature([Section]),
    TypeOrmModule.forFeature([TagCategory]),
    TypeOrmModule.forFeature([ForumMessages]),
    TypeOrmModule.forFeature([FeatureFlagEntry]),
    TypeOrmModule.forFeature([FeatureFlag]),
    TypeOrmModule.forFeature([BlacklistItem]),
    TypeOrmModule.forFeature([Segment]),
    TypeOrmModule.forFeature([ModuleAssignment]),
    TypeOrmModule.forFeature([SectionAssignment]),
    FilesModule,
    CoursesModule,
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.JWT_TOKEN_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRATION_TIME,
        },
      }),
    }),
    forwardRef(() => SettingsModule),
    forwardRef(() => WebinarModule),
    forwardRef(() => AmoModule),
    forwardRef(() => TriggerModule),
    forwardRef(() => CoursesModule),
    AssessmentsModule,
    TagsModule,
  ],
  controllers: [
    UsersController,
    RolesController,
    ProfileController,
    HierarchysController,
    BlacklistController,
    ModuleAssignmentController,
    SectionAssignmentController,
  ],
  providers: [
    UsersService,
    RolesService,
    TokensService,
    ProfileService,
    HierarchysService,
    UsersResolver,
    BlacklistService,
    SupportService,
    ModuleAssignmentService,
    SectionAssignmentService,
  ],
  exports: [
    UsersService,
    TokensService,
    ProfileService,
    SupportService,
    HierarchysService,
    ModuleAssignmentService,
    SectionAssignmentService,
  ],
})
export class UsersModule {}
