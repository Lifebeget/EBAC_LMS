import { ConflictException, Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { ModuleAssignmentService } from './module-assignment.service';
import { CreateModuleAssignmentDto } from './dto/create-module-assignment.dto';

@ApiTags('enrollments')
@ApiBearerAuth()
@Controller('')
export class ModuleAssignmentController {
  constructor(private readonly moduleAssignmentService: ModuleAssignmentService) {}

  @Post('module-assignment')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Create module assignment' })
  @ApiResponse({
    status: 201,
    description: 'Create module assignment',
  })
  @PgConstraints()
  async createModuleAssignment(
    @Body()
    body: CreateModuleAssignmentDto,
  ) {
    if (await this.moduleAssignmentService.isEntryExists(body)) {
      throw new ConflictException(`Entry already exists`);
    }
    if (await this.moduleAssignmentService.isSectionForThisModuleAlreadyAssigned(body)) {
      throw new ConflictException(`Section for this module with same exclusive flag or exclusively already exists`);
    }
    if (!(await this.moduleAssignmentService.hasEnrollmentForModule(body))) {
      throw new ConflictException(`Current user hasn't enrollment for course with this module`);
    }
    this.moduleAssignmentService.hasEnrollmentForModule(body);
    return this.moduleAssignmentService.assignModule(body);
  }

  @Get('users/:id/module-assignments')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Get module assignments' })
  @ApiResponse({
    status: 200,
    description: 'Get module assignments',
  })
  @PgConstraints()
  async getModuleAssignments(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.moduleAssignmentService.getByUserId(id);
  }

  @Delete('module-assignments/:id')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Delete module assignment' })
  @ApiResponse({
    status: 200,
    description: 'Delete module assignment',
  })
  @PgConstraints()
  async deleteSectionAssignment(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.moduleAssignmentService.deleteById(id);
  }
}
