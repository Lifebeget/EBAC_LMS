import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString, IsUUID, ValidateIf } from 'class-validator';

export class CreateBlacklistItemDto {
  @ApiProperty({ description: 'Tutor id' })
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId: string;

  @ApiProperty({ description: 'Student id' })
  @ValidateIf(o => o.hasOwnProperty('studentId'))
  @IsUUID()
  studentId: string;

  @ApiProperty({ description: 'Reason' })
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => value?.trim())
  reason?: string;
}
