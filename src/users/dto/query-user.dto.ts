import { AppRole } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBooleanString, IsEmail, IsEnum, IsOptional, IsPhoneNumber, IsString, IsUUID, ValidateIf } from 'class-validator';

export class QueryUserDto {
  @ApiProperty({ description: 'Filter by name', required: false })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    description: 'Filter by email (substring search)',
    required: false,
  })
  @ValidateIf(o => !!o.email)
  @IsString()
  @IsOptional()
  email?: string;

  @ApiProperty({
    description: 'Filter by email (exact search)',
    required: false,
  })
  @IsEmail()
  @IsOptional()
  exactEmail?: string;

  @ApiProperty({
    description: 'Filter by phone (smart, but heavy search)',
    required: false,
  })
  @IsPhoneNumber()
  @IsOptional()
  phone?: string;

  @ApiProperty({ description: 'Filter by External ID', required: false })
  @IsString()
  @IsOptional()
  externalId?: string;

  @ApiProperty({ description: 'Filter by system role', required: false })
  @IsEnum(AppRole)
  @IsOptional()
  role?: AppRole;

  @ApiProperty({ description: 'Filter by system roles list', required: false })
  @IsEnum(AppRole, { each: true })
  @IsOptional()
  roles?: AppRole[];

  activityDateUp?: Date;
  activityDateDown?: Date;
  lastSubmissionViewUp?: Date;
  lastSubmissionViewDown?: Date;
  noActivity?: boolean;
  noLastSubmissionView?: boolean;

  @ApiProperty({ description: 'Filter by user ids (array)', required: false })
  @IsUUID(4, { each: true })
  @IsOptional()
  ids?: string[];

  @ApiProperty({ description: 'Filter by active state', required: false })
  @Transform(
    ({ value }) => {
      if (typeof value === 'boolean') {
        return value;
      }
      return value.toLowerCase() === 'true';
    },
    {
      toClassOnly: true,
    },
  )
  @IsOptional()
  active?: boolean;

  @ApiProperty({ description: 'Include inactive enrollments', required: false })
  @Transform(flag => flag.value?.toLowerCase() === 'true', {
    toClassOnly: true,
  })
  @IsOptional()
  includeInactiveEnrollments?: boolean;

  @ApiProperty({
    description: 'Filter by module id (for tutors)',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('moduleId'))
  @IsUUID()
  @IsOptional()
  moduleId?: string;

  @ApiProperty({ description: 'String to search', required: false })
  @IsString()
  @IsOptional()
  searchString?: string;

  @ApiProperty({ description: 'Search by webinar id', required: false })
  @IsUUID()
  @IsOptional()
  webinarId?: string;

  @ApiProperty({
    description: 'Enrollment to the course as a student',
    required: false,
  })
  @IsUUID()
  @IsOptional()
  studentCourseId?: string;

  @ApiProperty({
    description: 'Enrollment to the courses as a tutor',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  tutorCourseIds?: string[];

  @ApiProperty({
    description: 'Filter by tags',
    required: false,
  })
  @IsOptional()
  @IsUUID(4, { each: true })
  tagIds?: string[];

  @ApiProperty({ description: 'Include user profile', required: false })
  @IsBooleanString()
  @IsOptional()
  includeProfile?: string;

  @ApiProperty({ description: 'Search only graduated students', required: false })
  @IsString()
  @IsOptional()
  graduated?: string;

  @ApiProperty({ description: 'Search by existing cert', required: false })
  @IsString()
  @IsOptional()
  withCert?: string;
}
