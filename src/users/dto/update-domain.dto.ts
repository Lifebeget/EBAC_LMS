import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { MultiDomainsEnum } from '@lib/types/enums';

export class UpdateDomainDto {
  @ApiProperty({ description: 'User domain', required: true })
  @IsEnum(MultiDomainsEnum)
  domain: MultiDomainsEnum;
}
