import { SystemRole } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateRoleDto {
  @ApiProperty({
    enum: SystemRole,
    description: `User system role. There are several available roles:\n
      superadmin -- Can do anything\n
      admin -- Can do almost anything except adding/removing admin role\n
      observer -- Full Read-only access\n
      designer -- Course designer. Can create and edit courses\n
      manager -- Tutor manager. Can manage tutors`,
  })
  @IsNotEmpty()
  role: SystemRole;

  @ApiProperty({ description: 'Role starts at' })
  dateFrom?: Date;

  @ApiProperty({ description: 'Role ends at' })
  dateTo?: Date;
}
