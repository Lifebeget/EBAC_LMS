import { IsNotEmpty, IsUUID, ValidateIf } from 'class-validator';
import { User } from '../../lib/entities/user.entity';

export class QueryProfileDto {
  @ValidateIf(o => o.hasOwnProperty('id'))
  @IsUUID()
  id?: string;

  user?: User;

  @ValidateIf(o => o.hasOwnProperty('name'))
  @IsNotEmpty()
  name?: string;

  surname?: string;

  birthdate?: Date;

  cpf?: string;

  city?: string;

  phone?: string;
}
