import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ExcludeBase64FromLog } from 'src/logger';

export class CreateAvatarDto {
  @ApiProperty({ description: 'Avatar base64' })
  @IsNotEmpty()
  @ExcludeBase64FromLog()
  data: string;
}
