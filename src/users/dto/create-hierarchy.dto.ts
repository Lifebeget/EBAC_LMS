import { HierarchyContext } from '@enums/hierarchy-context';
import { HierarchyRole } from '@enums/hierarchy-role.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class CreateHierarchyDto {
  @ApiProperty({ description: 'Parent user ID' })
  @IsUUID()
  parentId: string;

  @ApiProperty({ description: 'Child user ID' })
  @IsUUID()
  childId: string;

  @ApiProperty({
    description: 'Role of hierarchy (teamlead, tutor, etc...)',
    enum: HierarchyRole,
  })
  role: HierarchyRole;

  @ApiProperty({
    description: 'Context of hierarchy (course, module, etc... or nothing). null - nothing. ',
    enum: HierarchyContext,
  })
  context?: HierarchyContext;

  @ApiProperty({
    description: 'ID of context item if context is not null',
    enum: HierarchyContext,
  })
  contextId?: string;
}
