import { ApiProperty } from '@nestjs/swagger';

export class CreateProfileDto {
  @ApiProperty({ description: 'User first name' })
  name: string;

  @ApiProperty({ description: 'User surname' })
  surname?: string;

  @ApiProperty({ description: 'User birthdate' })
  birthdate?: string;

  @ApiProperty({ description: 'User cpf' })
  cpf?: string;

  @ApiProperty({ description: 'User city' })
  city?: string;

  @ApiProperty({ description: 'User phone' })
  phone?: string;

  @ApiProperty({
    description: 'User phone numbers for search (should be automatic)',
  })
  phoneClean?: string;

  @ApiProperty({ description: 'User data' })
  data?: string;
}
