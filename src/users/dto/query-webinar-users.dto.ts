import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEmail, IsInt, IsOptional, IsPositive } from 'class-validator';

export class QueryWebinarUsersDTO {
  @ApiPropertyOptional({ description: 'Take', required: false })
  @IsInt()
  @IsPositive()
  @IsOptional()
  take?: number;

  @ApiPropertyOptional({ description: 'Skip', required: false })
  @IsInt()
  @IsPositive()
  @IsOptional()
  skip?: number;

  @ApiPropertyOptional({ description: 'Search by email', required: false })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({
    description: 'Take All filter',
    required: false,
  })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  takeAll?: boolean;

  @ApiProperty({
    description: 'Filter by the presence of printed certificates',
    required: false,
  })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  printedCertificates?: boolean;
}
