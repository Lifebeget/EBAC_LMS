import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean } from 'class-validator';

export class CreateModuleAssignmentDto {
  @ApiProperty({ description: 'User id' })
  @IsNotEmpty()
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'Module id' })
  @IsNotEmpty()
  @IsUUID()
  moduleId: string;

  @ApiProperty({ description: 'Is exclusive' })
  @IsNotEmpty()
  @IsBoolean()
  exclusive: boolean;
}
