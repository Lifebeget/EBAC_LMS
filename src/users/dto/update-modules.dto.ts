import { PickType } from '@nestjs/swagger';
import { CreateUserDto } from './create-user.dto';

export class UpdateModulesDto extends PickType(CreateUserDto, ['modules'] as const) {}
