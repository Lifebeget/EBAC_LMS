import { IsBoolean, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class QueryUsersCompletedSurveyDto {
  @ApiProperty({
    description: 'Filter by the presence of printed certificates',
    required: false,
  })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  printedCertificates?: boolean;
}
