import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID, IsBoolean } from 'class-validator';

export class CreateSectionAssignmentDto {
  @ApiProperty({ description: 'User id' })
  @IsNotEmpty()
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'Section id' })
  @IsNotEmpty()
  @IsUUID()
  sectionId: string;

  @ApiProperty({ description: 'Is exclusive' })
  @IsNotEmpty()
  @IsBoolean()
  exclusive: boolean;
}
