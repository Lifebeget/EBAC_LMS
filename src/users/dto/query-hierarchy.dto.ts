import { HierarchyRole } from '@enums/hierarchy-role.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, ValidateIf } from 'class-validator';

export class QueryHierarchyDto {
  @ApiProperty({
    description: 'Filter by parent',
    enum: HierarchyRole,
  })
  @ValidateIf(o => o.hasOwnProperty('parentId'))
  @IsUUID()
  parentId?: string;

  @ApiProperty({
    description: 'Filter by child',
    enum: HierarchyRole,
  })
  @ValidateIf(o => o.hasOwnProperty('childId'))
  @IsUUID()
  childId?: string;

  @ApiProperty({
    description: 'Filter by hierarchy role',
    enum: HierarchyRole,
  })
  role: HierarchyRole;
}
