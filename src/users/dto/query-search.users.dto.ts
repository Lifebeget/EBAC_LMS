import { ApiPropertyOptional } from '@nestjs/swagger';

export class QuerySearchUsersDTO {
  @ApiPropertyOptional({ description: 'Search by email, name, id', required: true })
  search: string;
}
