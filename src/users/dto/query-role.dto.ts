import { IsUUID, ValidateIf } from 'class-validator';
import { SystemRole } from '@lib/api/types';

export class QueryRoleDto {
  showInactive?: boolean;
  role?: SystemRole;

  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;
}
