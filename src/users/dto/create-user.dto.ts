import { IsArray, IsEmail, IsEnum, IsNotEmpty, IsOptional, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from 'src/lib/entities/role.entity';
import { CreateRoleDto } from './create-role.dto';
import { Module } from 'src/lib/entities/module.entity';
import { Profile } from 'src/lib/entities/profile.entity';
import { MultiDomainsEnum } from '@lib/types/enums';

export class CreateUserDto {
  @ApiProperty({ description: 'User name' })
  @IsNotEmpty()
  name: string;

  @ApiProperty({ description: 'User email. Must be unique' })
  @IsEmail()
  email: string;

  @ApiProperty({ description: 'User domain' })
  @IsEnum(MultiDomainsEnum)
  @IsOptional()
  domain?: MultiDomainsEnum;

  @ApiProperty({ description: 'User Password' })
  password?: string;

  @ApiProperty({
    required: false,
    default: true,
    description: 'User activity flag',
  })
  active?: boolean;

  @ApiProperty({
    required: false,
    description: 'Last accessed time (for round robin distribution)',
  })
  lastAccessed?: Date;

  @ApiProperty({
    required: false,
    default: true,
    description: 'Is email confirmed',
  })
  emailConfirmed?: boolean;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'Auth token for EADBOX', required: false })
  eadboxToken?: string;

  @ApiProperty({
    description: 'User roles',
    required: false,
    type: () => [CreateRoleDto],
  })
  roles?: Role[];

  @ApiProperty({
    description: 'User profile',
    required: false,
    type: () => Profile,
  })
  profile?: Profile;

  @ApiProperty({
    description: 'User modules (for tutors)',
    required: false,
    type: () => [Module],
  })
  @ValidateIf(o => o.hasOwnProperty('modules'))
  @IsArray()
  modules?: Module[];

  @ApiProperty({ description: 'Generate password for user?' })
  generatePassword?: boolean;

  @ApiProperty({ description: 'Send email to new user' })
  sendEmail?: boolean;
}
