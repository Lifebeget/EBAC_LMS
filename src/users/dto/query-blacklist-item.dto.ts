import { IsUUID, ValidateIf } from 'class-validator';

export class QueryBlacklistItemDto {
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId?: string;

  @ValidateIf(o => o.hasOwnProperty('studentId'))
  @IsUUID()
  studentId?: string;
}
