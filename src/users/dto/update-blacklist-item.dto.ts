import { PartialType } from '@nestjs/swagger';
import { CreateBlacklistItemDto } from './create-blacklist-item.dto';

export class UpdateBlacklistItemDto extends PartialType(CreateBlacklistItemDto) {}
