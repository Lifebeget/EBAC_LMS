/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ModuleAssignment } from 'src/lib/entities/module-assignment.entity';
import { SectionAssignment } from 'src/lib/entities/section-assignment.entity';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { CreateModuleAssignmentDto } from './dto/create-module-assignment.dto';
import { ModulesService } from 'src/courses/modules.service';
import { CourseRole } from '@lib/types/enums/course-role.enum';

@Injectable()
export class ModuleAssignmentService {
  constructor(
    @InjectRepository(ModuleAssignment)
    private moduleAssignmentRepository: Repository<ModuleAssignment>,
    @InjectRepository(SectionAssignment)
    private sectionAssignmentRepository: Repository<SectionAssignment>,
    private enrollmentsService: EnrollmentsService,
    private modulesService: ModulesService,
  ) {}

  assignModule(dto: CreateModuleAssignmentDto) {
    return this.moduleAssignmentRepository.save({ ...dto, user: { id: dto.userId }, module: { id: dto.moduleId } });
  }

  getByUserId(userId: string) {
    return this.moduleAssignmentRepository
      .createQueryBuilder('moduleAssignment')
      .leftJoin('moduleAssignment.user', 'user')
      .leftJoin('moduleAssignment.module', 'module')
      .leftJoin('module.course', 'course')
      .leftJoin('module.section', 'section')
      .select([
        'moduleAssignment.id',
        'moduleAssignment.exclusive',
        'user.id',
        'user.name',
        'module.id',
        'module.number',
        'module.customNumber',
        'module.title',
        'module.sort',
        'section.id',
        'section.title',
        'section.sort',
        'course.id',
        'course.title',
        'course.version',
      ])
      .where('user.id = :userId', { userId })
      .orderBy('course.title')
      .addOrderBy('section.sort', 'DESC')
      .addOrderBy('module.sort', 'DESC')
      .getMany();
  }

  deleteById(id: string) {
    return this.moduleAssignmentRepository.delete({ id });
  }

  isEntryExists(dto: CreateModuleAssignmentDto) {
    const { userId, moduleId } = dto;
    return this.moduleAssignmentRepository.findOne({ userId, moduleId });
  }

  async isSectionForThisModuleAlreadyAssigned(dto: CreateModuleAssignmentDto) {
    const module = await this.modulesService.findOne(dto.moduleId, true);

    if (!module.section_id) return false;

    const qb = this.sectionAssignmentRepository
      .createQueryBuilder('sectionAssignment')
      .where('sectionAssignment.section_id = :sectionId', { sectionId: module.section_id })
      .andWhere('sectionAssignment.user_id = :userId', { userId: dto.userId });

    if (dto.exclusive) {
      qb.andWhere('sectionAssignment.exclusive = :exclusive', { exclusive: dto.exclusive });
    }

    const sectionEntry = await qb.getOne();

    return Boolean(sectionEntry);
  }

  async deleteBySectionId(sectionId: string, exclusive: boolean) {
    const modules = await this.modulesService.findBySectionIds([sectionId]);
    const moduleIds = modules.map(module => module.id);

    const qb = this.moduleAssignmentRepository
      .createQueryBuilder('module_assignment')
      .delete()
      .where('module_assignment.module_id in (:...moduleIds)', { moduleIds });

    if (!exclusive) {
      qb.andWhere('module_assignment.exclusive = :exclusive', { exclusive });
    }

    return qb.execute();
  }

  async hasEnrollmentForModule(dto: CreateModuleAssignmentDto) {
    const module = await this.modulesService.findOne(dto.moduleId, true);
    return this.enrollmentsService.hasEnrollment(dto.userId, module.courseId, CourseRole.Tutor);
  }
}
