import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Token } from 'src/lib/entities/token.entity';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';

import { generatePassword } from '../helpers';

@Injectable()
export class TokensService {
  constructor(
    @InjectRepository(Token)
    private tokenRepository: Repository<Token>,
  ) {}

  async create(userId: string): Promise<Token> {
    let tries = 0;
    let token = '';
    let dbToken: Token;

    do {
      if (tries > 10) throw new Error('Token generation problem');
      token = generatePassword(64);
      dbToken = await this.tokenRepository.findOne({ token });
      tries += 1;
    } while (dbToken);

    return this.tokenRepository.save({ token, user: { id: userId } });
  }

  useToken(id: string): Promise<Token> {
    const token = { id, used: new Date() };
    return this.tokenRepository.save(token);
  }

  @Transactional()
  async findOneByToken(token: string) {
    const dbToken = await this.tokenRepository.findOne({ token }, { relations: ['user'] });
    return dbToken;
  }
}
