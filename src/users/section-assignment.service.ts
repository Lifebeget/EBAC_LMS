/* eslint-disable @typescript-eslint/no-unused-vars */
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SectionAssignment } from 'src/lib/entities/section-assignment.entity';
import { ModuleAssignmentService } from './module-assignment.service';
import { CreateSectionAssignmentDto } from './dto/create-section-assignment.dto';
import { SectionsService } from 'src/courses/sections.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { CourseRole } from '@lib/types/enums/course-role.enum';

@Injectable()
export class SectionAssignmentService {
  constructor(
    @InjectRepository(SectionAssignment)
    private sectionAssignmentRepository: Repository<SectionAssignment>,
    private moduleAssignmentService: ModuleAssignmentService,
    @Inject(forwardRef(() => SectionsService))
    private sectionsService: SectionsService,
    private enrollmentsService: EnrollmentsService,
  ) {}

  async assignSection(dto: CreateSectionAssignmentDto) {
    await this.moduleAssignmentService.deleteBySectionId(dto.sectionId, dto.exclusive);
    return this.sectionAssignmentRepository.save({ ...dto, user: { id: dto.userId }, section: { id: dto.sectionId } });
  }

  getByUserId(userId: string) {
    return this.sectionAssignmentRepository
      .createQueryBuilder('sectionAssignment')
      .leftJoin('sectionAssignment.user', 'user')
      .leftJoin('sectionAssignment.section', 'section')
      .leftJoin('section.course', 'course')
      .select([
        'sectionAssignment.id',
        'sectionAssignment.exclusive',
        'user.id',
        'user.name',
        'section.id',
        'section.title',
        'section.sort',
        'course.id',
        'course.title',
        'course.version',
      ])
      .where('user.id = :userId', { userId })
      .orderBy('course.title')
      .addOrderBy('section.sort', 'DESC')
      .getMany();
  }

  getBySectionId(sectionId: string, exclusive: boolean) {
    return this.sectionAssignmentRepository
      .createQueryBuilder('sectionAssignment')
      .select(['section_id'])
      .where('sectionAssignment.sectionId = :sectionId', { sectionId })
      .andWhere('sectionAssignment.exclusive = :exclusive', { exclusive })
      .getOne();
  }

  deleteById(id: string) {
    return this.sectionAssignmentRepository.delete({ id });
  }

  isEntryExists(dto: CreateSectionAssignmentDto) {
    const { userId, sectionId } = dto;
    return this.sectionAssignmentRepository.findOne({ userId, sectionId });
  }

  async hasEnrollmentForSection(dto: CreateSectionAssignmentDto) {
    const section = await this.sectionsService.findOne(dto.sectionId, true);
    return this.enrollmentsService.hasEnrollment(dto.userId, section.courseId, CourseRole.Tutor);
  }
}
