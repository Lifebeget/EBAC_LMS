/* eslint-disable @typescript-eslint/no-unused-vars */
import { Hierarchy } from '@entities/hierarchy.entity';
import { SystemRole } from '@lib/api/types';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { Role } from '../lib/entities/role.entity';
import { CreateRoleDto } from './dto/create-role.dto';
import { QueryRoleDto } from './dto/query-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
    @InjectRepository(Hierarchy)
    private hierarchyRepository: Repository<Hierarchy>,
  ) {}

  create(userId: string, createRoleDto: CreateRoleDto) {
    const entity = { ...createRoleDto, user: { id: userId } };
    return this.rolesRepository.save(entity);
  }

  findAll(query: QueryRoleDto): Promise<Role[]> {
    let queryBuilder = this.rolesRepository.createQueryBuilder('role');
    queryBuilder = queryBuilder.innerJoinAndSelect('role.user', 'user');
    if (query.role) {
      queryBuilder = queryBuilder.andWhere('role = :role', {
        role: query.role,
      });
    }
    if (query.userId) {
      queryBuilder = queryBuilder.andWhere('user_id = :userId', {
        userId: query.userId,
      });
    }
    if (!query.showInactive) {
      queryBuilder = queryBuilder.andWhere('(date_to IS NULL OR date_to > NOW())');
      queryBuilder = queryBuilder.andWhere('(date_from IS NULL OR date_from <= NOW())');
    }
    return queryBuilder.getMany();
  }

  @Transactional()
  findOne(id: string): Promise<Role> {
    return this.rolesRepository.findOne(id, { relations: ['user'] });
  }

  /** This one returns password along for AuthService */
  findByUser(userId: string): Promise<Role[]> {
    let queryBuilder = this.rolesRepository.createQueryBuilder('role');
    queryBuilder = queryBuilder.where('user_id = :userId', { userId });
    queryBuilder = queryBuilder.andWhere('(date_to IS NULL OR date_to > NOW())');
    queryBuilder = queryBuilder.andWhere('(date_from IS NULL OR date_from <= NOW())');
    return queryBuilder.getMany();
  }

  update(id: string, updateRoleDto: UpdateRoleDto) {
    const entity = { ...updateRoleDto, id };
    return this.rolesRepository.save(entity);
  }

  async remove(id: string) {
    const role = await this.rolesRepository.findOne(id, {
      relations: ['user', 'user.roles', 'user.hierarchyChilds'],
    });

    if (role.role === SystemRole.Teamlead) {
      if (role.user.roles.filter(r => r.role === SystemRole.Teamlead).length === 1) {
        role.user.hierarchyChilds.forEach(h => {
          this.hierarchyRepository.delete(h.id);
        });
      }
    }

    return this.rolesRepository.delete(id);
  }

  purgeAll() {
    return this.rolesRepository.clear();
  }
}
