import { ConflictException, Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { SectionAssignmentService } from './section-assignment.service';
import { CreateSectionAssignmentDto } from './dto/create-section-assignment.dto';

@ApiTags('enrollments')
@ApiBearerAuth()
@Controller('')
export class SectionAssignmentController {
  constructor(private readonly sectionAssignmentService: SectionAssignmentService) {}

  @Post('section-assignment')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Create section assignment' })
  @ApiResponse({
    status: 201,
    description: 'Create section assignment',
  })
  @PgConstraints()
  async createSectionAssignment(
    @Body()
    body: CreateSectionAssignmentDto,
  ) {
    if (await this.sectionAssignmentService.isEntryExists(body)) {
      throw new ConflictException(`Entry already exists`);
    }
    if (!(await this.sectionAssignmentService.hasEnrollmentForSection(body))) {
      throw new ConflictException(`Current user hasn't enrollment for course with this section`);
    }
    return await this.sectionAssignmentService.assignSection(body);
  }

  @Get('users/:id/section-assignments')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Get section assignments' })
  @ApiResponse({
    status: 200,
    description: 'Get section assignments',
  })
  @PgConstraints()
  getSectionAssignments(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectionAssignmentService.getByUserId(id);
  }

  @Delete('section-assignments/:id')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Delete section assignment' })
  @ApiResponse({
    status: 200,
    description: 'Delete section assignment',
  })
  @PgConstraints()
  async deleteSectionAssignment(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.sectionAssignmentService.deleteById(id);
  }
}
