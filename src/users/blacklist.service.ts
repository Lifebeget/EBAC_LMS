import { Injectable, ConflictException, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BlacklistItem } from '@entities/blacklist.entity';
import { CreateBlacklistItemDto } from './dto/create-blacklist-item.dto';
import { UpdateBlacklistItemDto } from './dto/update-blacklist-item.dto';
import { QueryBlacklistItemDto } from './dto/query-blacklist-item.dto';
import { SubmissionsService } from 'src/assessments/submissions.service';

@Injectable()
export class BlacklistService {
  constructor(
    @InjectRepository(BlacklistItem)
    private blacklistRepository: Repository<BlacklistItem>,
    @Inject(forwardRef(() => SubmissionsService))
    private submissionsService: SubmissionsService,
  ) {}

  async create(createBlacklistItemDto: CreateBlacklistItemDto) {
    const { tutorId, studentId, reason } = createBlacklistItemDto;

    const item = await this.blacklistRepository.findOne({ tutorId, studentId });

    if (item) {
      throw new ConflictException('Blacklist entry already exist');
    }

    const result = await this.blacklistRepository.save({
      tutor: { id: tutorId },
      student: { id: studentId },
      reason: reason.trim(),
    });

    await this.submissionsService.unbindTutorFromSubmissionsByUserId(tutorId, studentId);
    await this.submissionsService.distribute();

    return result;
  }

  async update(updateBlacklistItemDto: UpdateBlacklistItemDto) {
    const { tutorId, studentId, reason } = updateBlacklistItemDto;

    return await this.blacklistRepository.update({ tutorId, studentId }, { reason: reason.trim() });
  }

  async remove(id: string) {
    return await this.blacklistRepository.delete(id);
  }

  async findAll(query: QueryBlacklistItemDto) {
    const qb = this.blacklistRepository
      .createQueryBuilder('blacklist')
      .leftJoinAndSelect('blacklist.tutor', 'tutor')
      .leftJoinAndSelect('blacklist.student', 'student');

    if (query.tutorId) {
      qb.andWhere('tutor_id = :tutorId', { tutorId: query.tutorId });
    }

    if (query.studentId) {
      qb.andWhere('student_id = :studentId', { studentId: query.studentId });
    }

    return await qb.getMany();
  }
}
