import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Profile } from 'src/lib/entities/profile.entity';
import { Repository } from 'typeorm';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { insertIntoString, removeFromString } from '../helpers';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { ProfileTriggerService } from '../trigger/services/profile-trigger.service';

@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(Profile)
    private profileRepository: Repository<Profile>,
    @Inject(forwardRef(() => ProfileTriggerService))
    private profileTriggerService: ProfileTriggerService,
  ) {}

  async create(userId: string, dto: CreateProfileDto) {
    const entity = { ...dto, user: { id: userId } };
    if (entity.phone) {
      entity.phoneClean = entity.phone.replace(/[^0-9]/g, '');
    }
    return this.profileRepository.save(entity);
  }

  async update(userId: string, updateProfileDto: UpdateProfileDto) {
    const entity = { ...updateProfileDto, user: { id: userId } };
    if (entity.phone) {
      entity.phoneClean = entity.phone.replace(/[^0-9]/g, '');
    }
    //Отрабатываем пока только на Бразилии
    if (['lms-production'].includes(process.env.ENVIRONMENT)) {
      this.profileTriggerService.userProfileUpdate(userId, updateProfileDto);
    }

    return this.profileRepository.save(entity);
  }

  @Transactional()
  async getProfileByUserId(userId: string) {
    return this.profileRepository.findOne(userId);
  }

  async getProfileAndUserBySearch(phone: string): Promise<Profile> {
    if (phone) {
      const cleanPhone = phone.replace(/[^0-9]/g, '');
      let findRes = await this.profileRepository.findOne({
        relations: ['user'],
        where: {
          phoneClean: cleanPhone,
        },
      });
      if (!findRes) {
        // todo make international, this is specifically for Brazil
        if (cleanPhone.length === 12) {
          const otherCleanPhone = insertIntoString(cleanPhone, 4, '9');
          findRes = await this.profileRepository.findOne({
            relations: ['user'],
            where: {
              phoneClean: otherCleanPhone,
            },
          });
        }
        if (cleanPhone.length === 13) {
          const otherCleanPhone = removeFromString(cleanPhone, 4, 1);
          findRes = await this.profileRepository.findOne({
            relations: ['user'],
            where: {
              phoneClean: otherCleanPhone,
            },
          });
        }
      }
      return findRes;
    }
    return;
  }

  async modifyAll() {
    const findRes = await this.profileRepository.find({ relations: ['user'] });
    for (const profile of findRes) {
      const { user, ...entity } = profile;
      await this.update(user.id, entity);
    }
    return true;
  }

  remove(id: string) {
    return this.profileRepository.delete(id);
  }
}
