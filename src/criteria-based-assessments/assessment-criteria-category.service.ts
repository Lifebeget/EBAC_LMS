import { Injectable, NotFoundException } from '@nestjs/common';
import { UpdateAssessmentCriterionDto } from './dto/update-assessment-criterion.dto';
import { CreateAssessmentCriterionCategoryDto } from './dto/create-assessment-criterion-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { Repository } from 'typeorm';
import { queryAssessmentCriterionCategoryDto } from './dto/query-assessment-criterion-category.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { ResortDto } from './dto/resort.dto';

@Injectable()
export class AssessmentCriteriaCategoryService {
  constructor(
    @InjectRepository(AssessmentCriterionCategory)
    private criterionCategoryRepository: Repository<AssessmentCriterionCategory>,
  ) {}

  create(dto: CreateAssessmentCriterionCategoryDto) {
    return this.criterionCategoryRepository.save(dto);
  }

  findAll(query: queryAssessmentCriterionCategoryDto) {
    const qb = this.criterionCategoryRepository
      .createQueryBuilder('assessmentCriterionCategory')
      .orderBy('assessmentCriterionCategory.sort', 'ASC');

    if (query.active === true) {
      qb.andWhere('assessmentCriterionCategory.active = TRUE');
    } else if (query.active === false) {
      qb.andWhere('assessmentCriterionCategory.active = FALSE');
    }
    if (query.withDeleted === true) {
      qb.withDeleted();
    }
    return qb.getMany();
  }

  @Transactional()
  async findOne(id: string, withDeleted = false) {
    const category = await this.criterionCategoryRepository.findOne(id, {
      withDeleted,
    });
    if (!category) {
      throw new NotFoundException(`Assessment criterion category ${id} not found`);
    }
    return category;
  }

  async update(id: string, dto: UpdateAssessmentCriterionDto) {
    await this.findOne(id);
    const entity: any = { id, ...dto };
    return this.criterionCategoryRepository.save(entity);
  }

  resort(dto: ResortDto) {
    return this.criterionCategoryRepository.save(dto.order);
  }

  async remove(id: string) {
    await this.findOne(id);
    return this.criterionCategoryRepository.softDelete(id);
  }

  async restore(id: string) {
    await this.findOne(id, true);
    return this.criterionCategoryRepository.restore(id);
  }
}
