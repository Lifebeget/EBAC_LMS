import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  ValidationPipe,
} from '@nestjs/common';
import { AssessmentCriteriaService } from './assessment-criteria.service';
import { CreateAssessmentCriterionDto } from './dto/create-assessment-criterion.dto';
import { UpdateAssessmentCriterionDto } from './dto/update-assessment-criterion.dto';
import { QueryAssessmentCriterionDto } from './dto/query-assessment-criterion.dto';
import { Pagination } from '../pagination.decorator';
import { PaginationDto } from '../pagination.dto';
import { PgConstraints } from '../pg.decorators';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { Request } from 'express';
import { ResortDto } from './dto/resort.dto';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { QueryGroupingByCategoryDto } from './dto/query-grouping-by-category.dto';

@Controller('assessment-criteria')
@ApiBearerAuth()
@ApiTags('assessment-criteria')
export class AssessmentCriteriaController {
  constructor(private readonly assessmentCriteriaService: AssessmentCriteriaService) {}

  @Post()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_CREATE, Permission.ASSESSMENT_CRITERIA_MANAGE)
  @PgConstraints()
  create(@Body() dto: CreateAssessmentCriterionDto, @Req() request: Request) {
    return this.assessmentCriteriaService.create(dto, request.user.id);
  }

  @Get()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findAll(
    @Query(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    query: QueryAssessmentCriterionDto,
    @Pagination() paging: PaginationDto,
  ) {
    return this.assessmentCriteriaService.findAll(paging, query);
  }

  @Get('/questions/:questionId/grouping-by-category')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findAllGroupingByCategory(
    @Param('questionId', new ParseUUIDPipe()) questionId: string,
    @Query(
      new ValidationPipe({ transform: true })
    ) query: QueryGroupingByCategoryDto,
  ) {
    return this.assessmentCriteriaService.findAllGroupingByCategory(questionId, query);
  }

  @Get(':id')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findOne(@Param('id') id: string) {
    return this.assessmentCriteriaService.findOne(id);
  }

  @Patch(':id')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  @PgConstraints()
  update(@Param('id') id: string, @Body() updateCriteriaBasedAssessmentDto: UpdateAssessmentCriterionDto) {
    return this.assessmentCriteriaService.update(id, updateCriteriaBasedAssessmentDto);
  }

  @Patch('resort')
  @ApiOperation({ summary: 'Resort criteria' })
  @ApiResponse({
    status: 200,
    description: 'Sorted criteria',
    type: [AssessmentCriterion],
  })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  resort(@Body() dto: ResortDto) {
    return this.assessmentCriteriaService.resort(dto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  @PgConstraints()
  remove(@Param('id') id: string) {
    return this.assessmentCriteriaService.remove(id);
  }

  @Patch(':id/restore')
  @ApiOperation({ summary: 'Restore criterion category' })
  @ApiResponse({
    status: 200,
    description: 'Restored criterion category',
    type: AssessmentCriterionCategory,
  })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  restore(@Param('id') id: string) {
    return this.assessmentCriteriaService.restore(id);
  }
}
