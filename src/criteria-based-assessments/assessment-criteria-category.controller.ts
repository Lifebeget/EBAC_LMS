import { Controller, Get, Post, Body, Patch, Param, Delete, Query, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { AssessmentCriteriaCategoryService } from './assessment-criteria-category.service';
import { CreateAssessmentCriterionCategoryDto } from './dto/create-assessment-criterion-category.dto';
import { UpdateAssessmentCriterionCategoryDto } from './dto/update-assessment-criterion-category.dto';
import { queryAssessmentCriterionCategoryDto } from './dto/query-assessment-criterion-category.dto';
import { ResortDto } from './dto/resort.dto';

@Controller('assessment-criterion-categories')
@ApiBearerAuth()
@ApiTags('criteria-based-assessments')
export class AssessmentCriteriaCategoryController {
  constructor(private readonly assessmentCriteriaCategoryService: AssessmentCriteriaCategoryService) {}

  @Post()
  @ApiOperation({ summary: 'Create assessment criterion category' })
  @ApiResponse({
    status: 201,
    description: 'Assessment criterion category created',
    type: AssessmentCriterionCategory,
  })
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_CREATE, Permission.ASSESSMENT_CRITERIA_MANAGE)
  @PgConstraints()
  create(@Body() dto: CreateAssessmentCriterionCategoryDto) {
    return this.assessmentCriteriaCategoryService.create(dto);
  }

  @Get()
  @PgConstraints()
  @ApiOperation({ summary: 'Get all assessment criteria categories' })
  @ApiResponse({
    status: 200,
    description: 'Assessment criteria category list',
    type: [AssessmentCriterionCategory],
  })
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  findAll(
    @Query(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    query: queryAssessmentCriterionCategoryDto,
  ) {
    return this.assessmentCriteriaCategoryService.findAll(query);
  }

  @Get(':id')
  @PgConstraints()
  @ApiOperation({ summary: 'Get one criterion category by id' })
  @ApiResponse({
    status: 200,
    description: 'Assessment criterion category',
    type: AssessmentCriterionCategory,
  })
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  findOne(@Param('id') id: string) {
    return this.assessmentCriteriaCategoryService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update criterion category' })
  @ApiResponse({
    status: 200,
    description: 'Updated criterion category',
    type: AssessmentCriterionCategory,
  })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  update(@Param('id') id: string, @Body() dto: UpdateAssessmentCriterionCategoryDto) {
    return this.assessmentCriteriaCategoryService.update(id, dto);
  }

  @Patch('update-many/resort')
  @ApiOperation({ summary: 'Sort criteria categories' })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  resort(@Body() dto: ResortDto) {
    return this.assessmentCriteriaCategoryService.resort(dto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove criterion category' })
  @ApiResponse({
    status: 200,
    description: 'Removed criterion category',
    type: AssessmentCriterionCategory,
  })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  remove(@Param('id') id: string) {
    return this.assessmentCriteriaCategoryService.remove(id);
  }

  @Patch(':id/restore')
  @ApiOperation({ summary: 'Restore criterion category' })
  @ApiResponse({
    status: 200,
    description: 'Restored criterion category',
    type: AssessmentCriterionCategory,
  })
  @PgConstraints()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  restore(@Param('id') id: string) {
    return this.assessmentCriteriaCategoryService.restore(id);
  }
}
