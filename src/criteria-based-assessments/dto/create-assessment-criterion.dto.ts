import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsString, IsUUID, Matches, Max, Min, MinLength } from 'class-validator';

export class CreateAssessmentCriterionDto {
  @ApiProperty({ description: 'Criterion category ID' })
  @IsNotEmpty()
  @IsUUID()
  categoryId: string;

  @ApiProperty({ description: 'Criterion name' })
  @IsNotEmpty()
  @IsString()
  @Matches(/\w+/)
  @MinLength(1)
  title: string;

  @ApiProperty({
    description: 'Criterion weight',
    required: false,
    default: 1,
  })
  @IsInt()
  @Min(1)
  @Max(10)
  @IsOptional()
  weight?: number;

  @ApiProperty({ description: 'Criterion author ID' })
  @IsUUID()
  @IsOptional()
  authorId?: string;

  @ApiProperty({ description: 'Criterion question ID' })
  @IsNotEmpty()
  @IsUUID()
  questionId: string;

  @ApiProperty({
    description: 'Sort',
    required: false,
    default: 500,
  })
  @IsInt()
  @IsOptional()
  sort?: number;
}
