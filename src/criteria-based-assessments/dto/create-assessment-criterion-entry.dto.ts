import { CriterionAnswer } from '@lib/types/enums/criterion-answer.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsUUID } from 'class-validator';

export class CreateAssessmentCriterionEntryDto {
  @ApiProperty({
    description: 'Criterion criterion ID',
    required: true,
  })
  @IsUUID()
  @IsNotEmpty()
  criteriaId: string;

  @ApiProperty({
    description: 'Criterion answer result (yes/no)',
    required: true,
    enum: CriterionAnswer,
  })
  @IsEnum(CriterionAnswer)
  @IsNotEmpty()
  state: CriterionAnswer;
}

export class ParamAssessmentCriterionEntry extends CreateAssessmentCriterionEntryDto {}
