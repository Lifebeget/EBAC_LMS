import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';

export class QueryAssessmentCriterionDto {
  @ApiProperty({ description: 'Is active filter', required: false })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @ApiProperty({ description: 'Category ID filter', required: false })
  @IsUUID()
  @IsOptional()
  categoryId?: string;

  @ApiProperty({ description: 'Author ID filter', required: false })
  @IsUUID()
  @IsOptional()
  authorId?: string;

  @ApiProperty({ description: 'Question ID filter', required: false })
  @IsUUID()
  @IsOptional()
  questionId?: string;

  @ApiProperty({ description: 'Criterion name filter', required: false })
  @IsString()
  @IsOptional()
  title?: string;

  @ApiProperty({ description: 'Whether to show deleted criteria' })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  withDeleted?: boolean;
}
