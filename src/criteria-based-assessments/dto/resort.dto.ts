import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsInt, IsNotEmpty, IsNumber, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateLectureContentCbaDto } from './create-lecture-content-cba.dto';

export class OrderDto {
  @ApiProperty({ description: 'Entity id' })
  @IsNotEmpty()
  @IsUUID()
  id: string;

  @ApiProperty({ description: 'Sort value' })
  @IsInt()
  sort: number;
}

export class ResortDto {
  @ApiProperty({
    description: 'New order array',
  })
  @IsArray()
  order: OrderDto[];
}
