import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class queryAssessmentCriterionCategoryDto {
  @ApiProperty({ description: 'Is active' })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @ApiProperty({ description: 'Whether to show deleted categories' })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  withDeleted?: boolean;
}
