import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class QueryAssessmentCriterionEntryDto {
  @ApiProperty({ description: 'Answer ID filter', required: false })
  @IsUUID()
  @IsOptional()
  answerId?: string;

  @ApiProperty({ description: 'Criterion ID filter', required: false })
  @IsUUID()
  @IsOptional()
  criteriaId?: string;
}
