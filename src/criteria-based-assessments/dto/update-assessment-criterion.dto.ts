import { PartialType } from '@nestjs/swagger';
import { CreateAssessmentCriterionDto } from './create-assessment-criterion.dto';

export class UpdateAssessmentCriterionDto extends PartialType(CreateAssessmentCriterionDto) {}
