import { ApiProperty, OmitType } from '@nestjs/swagger';
import { CreateAssessmentCriterionDto } from './create-assessment-criterion.dto';
import { IsOptional, IsUUID } from 'class-validator';

export class CreateLectureContentCbaDto
  extends OmitType(CreateAssessmentCriterionDto, ['questionId']) {
  @ApiProperty({ description: 'CBA id' })
  @IsOptional()
  @IsUUID()
  id?: string;
}
