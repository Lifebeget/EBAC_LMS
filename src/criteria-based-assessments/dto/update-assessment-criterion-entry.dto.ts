import { PartialType } from '@nestjs/swagger';
import { CreateAssessmentCriterionEntryDto } from './create-assessment-criterion-entry.dto';

export class UpdateAssessmentCriterionEntryDto extends PartialType(CreateAssessmentCriterionEntryDto) {}
