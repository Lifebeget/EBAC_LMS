import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsBooleanString, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class QueryGroupingByCategoryDto {
  @ApiProperty({ description: 'Show empty categories', required: false })
  @IsBooleanString()
  @IsOptional()
  showEmptyCategories?: boolean;

  @ApiProperty({
    description: 'Show active or inactive criteria in categories',
    required: false,
  })
  @Transform(({ value }) => value.toLowerCase() === 'true')
  @IsBoolean()
  @IsOptional()
  active?: boolean;
}
