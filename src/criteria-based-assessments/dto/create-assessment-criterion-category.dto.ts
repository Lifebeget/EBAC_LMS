import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString, Matches, Max, Min } from 'class-validator';
import { CriterionSelector } from '@lib/types/enums/criterion-selector.enum';

export class CreateAssessmentCriterionCategoryDto {
  @ApiProperty({ description: 'Criterion category name' })
  @IsNotEmpty()
  @Matches(/\w+/)
  @IsString()
  title: string;

  @ApiProperty({ description: 'Criterion category description' })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    description: 'Sort',
    required: false,
    default: 500,
  })
  @IsInt()
  @IsOptional()
  sort?: number;

  @ApiProperty({ description: 'Criterion default weight' })
  @IsInt()
  @Min(1)
  @Max(10)
  @IsOptional()
  defaultWeight?: number;

  @ApiProperty({
    enum: CriterionSelector,
    description: 'Criterion toggle element type',
    required: false,
    default: CriterionSelector.Checkbox,
  })
  @IsEnum(CriterionSelector)
  @IsOptional()
  display?: CriterionSelector;

  @ApiProperty({ description: 'Is active', default: true })
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @ApiProperty({ description: 'Is gradable', default: true })
  @IsBoolean()
  @IsOptional()
  gradable?: boolean;
}
