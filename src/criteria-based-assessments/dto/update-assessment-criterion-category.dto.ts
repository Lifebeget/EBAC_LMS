import { CreateAssessmentCriterionCategoryDto } from './create-assessment-criterion-category.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateAssessmentCriterionCategoryDto extends PartialType(CreateAssessmentCriterionCategoryDto) {}
