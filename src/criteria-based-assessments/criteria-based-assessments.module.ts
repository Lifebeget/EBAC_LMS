import { forwardRef, Module } from '@nestjs/common';
import { AssessmentCriteriaService } from './assessment-criteria.service';
import { AssessmentCriteriaController } from './assessment-criteria.controller';
import { AssessmentCriteriaCategoryController } from './assessment-criteria-category.controller';
import { AssessmentCriteriaEntryController } from './assessment-criteria-entry.controller';
import { AssessmentCriteriaCategoryService } from './assessment-criteria-category.service';
import { AssessmentCriteriaEntryService } from './assessment-criteria-entry.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';
import { User } from '@entities/user.entity';
import { Question } from '@entities/question.entity';
import { Answer } from '@entities/answer.entity';
import { AssessmentsModule } from '../assessments/assessments.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([AssessmentCriterion]),
    TypeOrmModule.forFeature([AssessmentCriterionCategory]),
    TypeOrmModule.forFeature([AssessmentCriterionEntry]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Question]),
    TypeOrmModule.forFeature([Answer]),
    forwardRef(() => AssessmentsModule),
  ],
  exports: [AssessmentCriteriaService, AssessmentCriteriaCategoryService, AssessmentCriteriaEntryService],
  controllers: [AssessmentCriteriaController, AssessmentCriteriaCategoryController, AssessmentCriteriaEntryController],
  providers: [AssessmentCriteriaService, AssessmentCriteriaCategoryService, AssessmentCriteriaEntryService],
})
export class CriteriaBasedAssessmentsModule {}
