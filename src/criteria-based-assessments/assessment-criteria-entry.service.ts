import { ConflictException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateAssessmentCriterionEntryDto } from './dto/create-assessment-criterion-entry.dto';
import { UpdateAssessmentCriterionEntryDto } from './dto/update-assessment-criterion-entry.dto';
import { QueryAssessmentCriterionEntryDto } from './dto/query-assessment-criterion-entry.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';
import { Answer } from '@entities/answer.entity';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { Question } from '@entities/question.entity';
import { AssessmentCriteriaService } from './assessment-criteria.service';

@Injectable()
export class AssessmentCriteriaEntryService {
  constructor(
    @InjectRepository(AssessmentCriterionEntry)
    private criterionEntriesRepository: Repository<AssessmentCriterionEntry>,
    @InjectRepository(Answer)
    private answersRepository: Repository<Answer>,
    @InjectRepository(AssessmentCriterion)
    private criteriaRepository: Repository<AssessmentCriterion>,
    @InjectRepository(AssessmentCriterionCategory)
    private criteriaCategoriesRepository: Repository<AssessmentCriterionCategory>,
    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,
    @Inject(AssessmentCriteriaService)
    private criteriaService: AssessmentCriteriaService,
  ) {}

  async create(answerId: string, dto: CreateAssessmentCriterionEntryDto[]) {
    const promises = [];
    for (const entity of dto) {
      promises.push(this.checkCriterionExistence(entity.criteriaId));
    }
    await Promise.all([this.checkAnswerExistence(answerId), ...promises]);
    const createdFields = dto.map(entry => ({ answerId, ...entry }));
    return this.criterionEntriesRepository.save(createdFields);
  }

  async findAll(query: QueryAssessmentCriterionEntryDto) {
    const criteriaEntryQb = this.criteriaEntryQb();
    if (query.criteriaId) {
      criteriaEntryQb.andWhere('entry.criteriaId = :criteriaId', {
        criteriaId: query.criteriaId,
      });
    }
    if (query.answerId) {
      criteriaEntryQb.andWhere('entry.answerId = :answerId', {
        answerId: query.answerId,
      });
    }
    return criteriaEntryQb.getMany();
  }

  async findAllGroupingByCategory(answerId: string) {
    const answer = await this.answersRepository.findOne(answerId);
    if (!answer) {
      throw new NotFoundException(`Answer ${answerId} not found`);
    }
    return this.criteriaCategoriesRepository
      .createQueryBuilder('category')
      .where('category.active = TRUE')
      .leftJoinAndSelect('category.criteria', 'criteria', 'criteria.active = TRUE')
      .leftJoinAndMapOne(
        'criteria.entry',
        AssessmentCriterionEntry,
        'entry',
        'entry.answer_id = :answerId AND entry.criteria_id = criteria.id',
        { answerId },
      )
      .leftJoin('criteria.question', 'question')
      .andWhere('question.id = :questionId', { questionId: answer.questionId })
      .orderBy('category.sort', 'ASC')
      .addOrderBy('criteria.sort', 'ASC')
      .getMany();
  }

  async findOne(answerId: string, criteriaId: string) {
    await Promise.all([this.checkAnswerExistence(answerId), this.checkCriterionExistence(criteriaId)]);
    const entry = await this.criterionEntriesRepository.findOne({
      answerId,
      criteriaId,
    });
    if (!entry) {
      throw new NotFoundException(`Assessment criterion entry not found`);
    }
    return entry;
  }

  async update(answerId: string, dto: UpdateAssessmentCriterionEntryDto[]) {
    const promises = [];
    for (const entry of dto) {
      promises.push(this.findOne(answerId, entry.criteriaId));
      promises.push(this.checkCriterionExistence(entry.criteriaId));
    }
    await Promise.all([this.checkAnswerExistence(answerId), ...promises]);
    const updatedFields = dto.map(entry => ({ answerId, ...entry }));
    return this.criterionEntriesRepository.save(updatedFields);
  }

  async remove(answerId: string, criteriaId: string) {
    await this.findOne(answerId, criteriaId);
    return this.criterionEntriesRepository.delete({
      answerId,
      criteriaId,
    });
  }

  criteriaEntryQb() {
    return this.criterionEntriesRepository
      .createQueryBuilder('entry')
      .leftJoinAndSelect('entry.answer', 'answer')
      .leftJoinAndSelect('entry.criteria', 'criteria')
      .leftJoinAndSelect('criteria.category', 'category');
  }

  async checkAnswerExistence(answerId: string) {
    const answer = await this.answersRepository.findOne(answerId);
    if (!answer) {
      throw new NotFoundException(`Answer ${answerId} not found`);
    }
    return answer;
  }

  async checkCriterionExistence(criteriaId: string) {
    const criterion = await this.criteriaRepository.findOne(criteriaId);
    if (!criterion) {
      throw new NotFoundException(`Criterion ${criteriaId} not found`);
    }
    return criterion;
  }

  async checkEntryNotExistence(answerId: string, criteriaId: string) {
    const entry = await this.criterionEntriesRepository.findOne({
      answerId,
      criteriaId,
    });
    if (entry) {
      throw new ConflictException(`Entry with answer ${answerId} and criteria ${criteriaId} already exists`);
    }
  }
}
