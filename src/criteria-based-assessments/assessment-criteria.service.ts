import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAssessmentCriterionDto } from './dto/create-assessment-criterion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { QueryAssessmentCriterionDto } from './dto/query-assessment-criterion.dto';
import { PaginationDto } from '../pagination.dto';
import { UpdateAssessmentCriterionDto } from './dto/update-assessment-criterion.dto';
import { Question } from '@entities/question.entity';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { User } from '@entities/user.entity';
import { ResortDto } from './dto/resort.dto';
import { QueryGroupingByCategoryDto } from './dto/query-grouping-by-category.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class AssessmentCriteriaService {
  constructor(
    @InjectRepository(AssessmentCriterion)
    private criteriaRepository: Repository<AssessmentCriterion>,
    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,
    @InjectRepository(AssessmentCriterionCategory)
    private criterionCategoriesRepository: Repository<AssessmentCriterionCategory>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(AssessmentCriterionCategory)
    private criteriaCategoriesRepository: Repository<AssessmentCriterionCategory>,
  ) {}
  async create(dto: CreateAssessmentCriterionDto, authorId?: string) {
    await Promise.all([
      this.checkCriterionCategoryExistence(dto.categoryId),
      this.checkUserExistence(dto.authorId),
      this.checkQuestionExistence(dto.questionId),
    ]);
    if (!dto.authorId && authorId) {
      dto.authorId = authorId;
    }
    return this.criteriaRepository.save(dto);
  }

  async findAll(paging: PaginationDto, query: QueryAssessmentCriterionDto) {
    const criteriaQb = this.criteriaQueryBuilder();
    if (query.questionId) {
      criteriaQb.andWhere('criterion.questionId = :questionId', {
        questionId: query.questionId,
      });
    }
    if (query.active === true) {
      criteriaQb.andWhere('criterion.active = TRUE');
    } else if (query.active === false) {
      criteriaQb.andWhere('criterion.active = FALSE');
    }
    if (query.authorId) {
      criteriaQb.andWhere('criterion.authorId = :authorId', {
        authorId: query.authorId,
      });
    }
    if (query.categoryId) {
      criteriaQb.andWhere('criterion.categoryId = :categoryId', {
        categoryId: query.categoryId,
      });
    }
    if (query.title) {
      criteriaQb.andWhere('LOWER(criterion.title) LIKE LOWER(:title)', {
        title: `%${query.title}%`,
      });
    }
    if (query.withDeleted === true) {
      criteriaQb.withDeleted();
    }
    if (!paging.showAll) {
      criteriaQb.skip((paging.page - 1) * paging.itemsPerPage);
      criteriaQb.take(paging.itemsPerPage);
    }
    const [results, total] = await criteriaQb.getManyAndCount();

    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findAllGroupingByCategory(questionId: string, query?: QueryGroupingByCategoryDto) {
    const queryBuilder = this.criteriaCategoriesRepository
      .createQueryBuilder('category');

    const question = await this.questionsRepository.findOne(questionId);
    // Если вопрос не найден, то считаем что questionId создан на фронте и нужно
    // вернуть просто список категорий с пустыми критериями
    if (!question) {
      return queryBuilder
        .orderBy('category.sort', 'ASC')
        .leftJoinAndSelect('category.criteria', 'criteria', 'criteria.id IS NULL')
        .getMany();
    }

    let activeCondition = ''
    if (query.active === true) {
      activeCondition = ' AND criteria.active = TRUE';
    } else if (query.active === false) {
      activeCondition = ' AND criteria.active = FALSE';
    }

    if (query.showEmptyCategories) {
      queryBuilder
        .leftJoinAndSelect(
          'category.criteria',
          'criteria',
          `criteria.questionId = :questionId${activeCondition}`,
          { questionId },
        );
    } else {
      if (query.active === true) {
        queryBuilder
          .leftJoinAndSelect(
            'category.criteria',
            'criteria',
            'criteria.active = TRUE'
          );
      } else if (query.active === false) {
        queryBuilder
          .leftJoinAndSelect(
            'category.criteria',
            'criteria',
            'criteria.active = FALSE'
          );
      } else {
        queryBuilder
          .leftJoinAndSelect('category.criteria', 'criteria');
      }
      queryBuilder
        .andWhere('criteria.questionId = :questionId', {
          questionId,
        });
    }

    return queryBuilder
      .orderBy('category.sort', 'ASC')
      .addOrderBy('criteria.sort', 'ASC')
      .andWhere('category.active = TRUE')
      .getMany();
  }

  @Transactional()
  async findOne(id: string) {
    const criterion = await this.criteriaQueryBuilder().andWhere('criterion.id = :id', { id }).getOne();

    if (!criterion) {
      throw new NotFoundException(`Assessment criterion ${id} not found`);
    }
    return criterion;
  }

  async update(id: string, dto: UpdateAssessmentCriterionDto) {
    await Promise.all([
      this.checkCriterionExistence(id),
      this.checkCriterionCategoryExistence(dto.categoryId),
      this.checkUserExistence(dto.authorId),
      this.checkQuestionExistence(dto.questionId),
    ]);
    const updateFields: any = { id, ...dto };
    if (dto.authorId) {
      updateFields.author = { id: dto.authorId };
    }
    if (dto.categoryId) {
      updateFields.category = { id: dto.categoryId };
    }
    if (dto.questionId) {
      updateFields.question = { id: dto.questionId };
    }
    return this.criteriaRepository.save(updateFields);
  }

  resort(dto: ResortDto) {
    return this.criteriaRepository.save(dto.order);
  }

  async remove(id: string) {
    await this.checkCriterionExistence(id);
    return this.criteriaRepository.softDelete(id);
  }

  async restore(id: string) {
    await this.checkCriterionExistence(id, true);
    return this.criteriaRepository.restore(id);
  }

  async checkCriterionExistence(criterionId: string, withDeleted = false) {
    const criterion = await this.criteriaRepository.findOne(criterionId, {
      withDeleted,
    });
    if (!criterion) {
      throw new NotFoundException(`Assessment criterion ${criterionId} not found`);
    }
  }

  async checkQuestionExistence(questionId: string) {
    if (!questionId) return;
    const question = await this.questionsRepository.findOne(questionId);
    if (!question) {
      throw new NotFoundException(`Question ${questionId} not found`);
    }
    return question;
  }

  async checkCriterionCategoryExistence(categoryId: string) {
    if (!categoryId) return;
    const category = await this.criterionCategoriesRepository.findOne(categoryId);
    if (!category) {
      throw new NotFoundException(`Criterion category ${categoryId} not found`);
    }
    return category;
  }

  async checkUserExistence(userId: string) {
    if (!userId) return;
    const author = await this.usersRepository.findOne(userId);
    if (!author) {
      throw new NotFoundException(`User ${userId} not found`);
    }
    return author;
  }

  criteriaQueryBuilder() {
    return this.criteriaRepository
      .createQueryBuilder('criterion')
      .leftJoinAndSelect('criterion.category', 'category')
      .leftJoinAndSelect('criterion.author', 'author')
      .leftJoinAndSelect('criterion.question', 'question');
  }
}
