import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Req,
  NotFoundException,
  ForbiddenException,
  ParseUUIDPipe,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { Repository } from 'typeorm';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';
import { AssessmentCriteriaEntryService } from './assessment-criteria-entry.service';
import { QueryAssessmentCriterionEntryDto } from './dto/query-assessment-criterion-entry.dto';
import { CreateAssessmentCriterionEntryDto } from './dto/create-assessment-criterion-entry.dto';
import { UpdateAssessmentCriterionEntryDto } from './dto/update-assessment-criterion-entry.dto';
import { Request } from 'express';
import { Answer } from '@entities/answer.entity';
import { AssessmentType } from '@lib/types/enums';
import { Permission } from '@enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from '../require-permissions.decorator';

@Controller('assessment-criteria-entries')
@ApiBearerAuth()
@ApiTags('assessment-criteria-entries')
export class AssessmentCriteriaEntryController {
  constructor(
    @InjectRepository(AssessmentCriterion)
    private criteriaRepository: Repository<AssessmentCriterion>,
    @InjectRepository(Answer)
    private answersRepository: Repository<Answer>,
    @InjectRepository(AssessmentCriterionEntry)
    private criteriaEntryRepository: Repository<AssessmentCriterionEntry>,

    private readonly criteriaEntryService: AssessmentCriteriaEntryService,
  ) {}

  @Post('answers/:answerId')
  @PgConstraints()
  async create(
    @Param('answerId', new ParseUUIDPipe()) answerId: string,
    @Body() dto: CreateAssessmentCriterionEntryDto[],
    @Req() req: Request,
  ) {
    await this.checkPermissions(answerId, req.user);
    return this.criteriaEntryService.create(answerId, dto);
  }

  @Get()
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findAll(@Query() query: QueryAssessmentCriterionEntryDto) {
    return this.criteriaEntryService.findAll(query);
  }

  @Get('answers/:answerId/grouping-by-category')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findAllGroupingByCategory(@Param('answerId', new ParseUUIDPipe()) answerId: string) {
    return this.criteriaEntryService.findAllGroupingByCategory(answerId);
  }

  @Get('answers/:answerId/criteria/:criteriaId')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_VIEW)
  @PgConstraints()
  findOne(@Param('answerId', new ParseUUIDPipe()) answerId: string, @Param('criteriaId', new ParseUUIDPipe()) criteriaId: string) {
    return this.criteriaEntryService.findOne(answerId, criteriaId);
  }

  @Patch('answers/:answerId')
  @PgConstraints()
  async update(
    @Param('answerId', new ParseUUIDPipe()) answerId: string,
    @Body() dto: UpdateAssessmentCriterionEntryDto[],
    @Req() req: Request,
  ) {
    await this.checkPermissions(answerId, req.user);
    return this.criteriaEntryService.update(answerId, dto);
  }

  @Delete('answers/:answerId/criteria/:criteriaId')
  @RequirePermissions(Permission.ASSESSMENT_CRITERIA_MANAGE)
  @PgConstraints()
  remove(@Param('answerId', new ParseUUIDPipe()) answerId: string, @Param('criteriaId', new ParseUUIDPipe()) criteriaId: string) {
    return this.criteriaEntryService.remove(answerId, criteriaId);
  }

  checkRights(courseId: string, assessmentType: AssessmentType, user: Express.User) {
    const hasViewPerm = user.hasPermission(Permission.COURSE_VIEW);
    const hasEnrollment =
      user.enrolledAsSupport(courseId) ||
      (user.enrolledAsStudent(courseId) &&
        assessmentType === AssessmentType.Default &&
        user.coursePermissions[courseId].canSubmitAssignment) ||
      user.enrolledAsTutor(courseId) ||
      user.enrolledAsTeamlead(courseId) ||
      user.enrolledAsModerator(courseId);

    return !!(hasViewPerm || hasEnrollment);
  }

  async checkPermissions(answerId: string, user: Express.User) {
    const answer = await this.answersRepository
      .createQueryBuilder('answer')
      .leftJoin('answer.submission', 'submission')
      .addSelect('submission.id')
      .leftJoin('submission.assessment', 'assessment')
      .addSelect('assessment.type')
      .leftJoin('submission.course', 'course')
      .addSelect('course.id')
      .andWhere('answer.id = :answerId', { answerId })
      .getOne();

    if (!answer) {
      throw new NotFoundException(`Answer ${answerId} not found`);
    }
    if (answer.submission?.assessment?.type && answer.submission?.course?.id) {
      const assessmentType = answer.submission.assessment.type;
      const courseId = answer.submission.assessment.type;
      const hasRights = this.checkRights(courseId, assessmentType, user);
      if (!hasRights) {
        throw new ForbiddenException('ERRORS.SUBMISSION_FORBIDDEN');
      }
    }
  }
}
