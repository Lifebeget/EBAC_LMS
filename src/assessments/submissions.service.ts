import { AttemptsService } from './attempts.service';
import { Answer } from '@entities/answer.entity';
import { AssessmentCriterionCategory } from '@entities/assessment-criterion-category.entity';
import { AssessmentCriterionEntry } from '@entities/assessment-criterion-entry.entity';
import { Assessment } from '@entities/assessment.entity';
import { DistributeLog } from '@entities/distribute-log.entity';
import { Enrollment } from '@entities/enrollment.entity';
import { LectureProgress } from '@entities/lecture-progress.entity';
import { Lecture } from '@entities/lecture.entity';
import { Module } from '@entities/module.entity';
import { Role } from '@entities/role.entity';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { Submission } from '@entities/submission.entity';
import { SubmissionState } from '@enums/submission-type.enum';
import { AssessmentType, SubmissionStatus, SubmissionType, SystemRole } from '@lib/api/types';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { DatePart } from '@lib/types/enums/date-part.enum';
import { BadRequestException, forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Brackets } from 'typeorm';
import axios from 'axios';
import * as dayjs from 'dayjs';
import { AssessmentLevelsService } from 'src/directory/service-level/assessment-levels.service';
import { User } from 'src/lib/entities/user.entity';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { addTeamleadFilter, getTeamleadTutors } from 'src/query-helpers/teamlead.helper';
import { getTutors } from 'src/query-helpers/tutor.helper';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { UsersService } from 'src/users/users.service';
import { ModulesService } from 'src/courses/modules.service';
import { DateService } from 'src/utils/date.service';
import { EntityManager, getConnection, Repository, SelectQueryBuilder } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { AssessmentsService } from './assessments.service';
import { CreateSubmissionDto } from './dto/create-submission.dto';
import { CreateWebinarSubmissionDto } from './dto/create-webinar-submission.dto';
import { QuerySubmissionIds } from './dto/query-submission-ids.dto';
import { QuerySubmissionListDto, QuerySubmissionListForAdminPanelDto } from './dto/query-submission-list.dto';
import { QuerySubmissionOneDto } from './dto/query-submission-one.dto';
import { QuerySubmissionDto } from './dto/query-submission.dto';
import { QueryTutorReportDto } from './dto/query-tutor-report.dto';
import { SubmissionSummaryDto } from './dto/submission-summary.dto';
import { TutorUpdateSubmissionDto } from './dto/tutor-update-submission.dto';
import { UpdateSubmissionGradeDto } from './dto/update-submission-grade.dto';
import { UpdateSubmissionDto } from './dto/update-submission.dto';
import { QuizService } from './quiz.service';
import { ModuleAssignment } from '@entities/module-assignment.entity';
import config from 'config';
import { FeatureName } from '@lib/types/enums/feature-flags/feature-name.enum';
import { HaveAttemptsDto } from './dto/have-attempts.dto';
import { TariffsService } from 'src/directory/service-level/tariffs.service';
import { StoreService } from 'src/store/store.service';
import { CRUD } from 'src/lib/interfaces/CRUD';

const NEXT_ATTEMPT_OUTDATED_HOURS = 48;
const DISTRIBUTE_LOCK_TIMEOUT = 5 * 60 * 1000;
const DISTRIBUTE_MUTEX_NAME = 'tutor-distribute-mutex';

export interface TutorsLoad {
  id: string;
  name: string;
  pause: boolean;
  courses: string[];
  moduleAssignments: ModuleAssignment[];
  counter: number;
  submissions: string[];
  blacklistedStudents: string[];
}

interface MutexCount {
  submissions_count: number;
  tutor_id: string;
}

interface SubmissionToDistribute {
  id: string;
  course_id: string;
  module_id: string;
  user_id: string;
}

export interface AssessmentStatus {
  assessment_id: string;
  type: AssessmentType;
  passing_score: number;
  lecture_id: string;
  score: number | null;
  success: boolean | null;
  in_progress: boolean | null;
}

@Injectable()
export class SubmissionsService implements CRUD {
  constructor(
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,
    @InjectRepository(DistributeLog)
    private distributeLogRepository: Repository<DistributeLog>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
    @InjectRepository(SubmissionTariff)
    private submissionTariffRepository: Repository<SubmissionTariff>,
    @InjectEntityManager()
    private entityManager: EntityManager,
    private usersService: UsersService,
    private readonly modulesService: ModulesService,
    @Inject(forwardRef(() => QuizService))
    private quizService: QuizService,
    @Inject(forwardRef(() => AssessmentsService))
    private assessmentsService: AssessmentsService,
    private levelService: AssessmentLevelsService,
    private tariffService: TariffsService,
    private socketsGateway: SocketsGateway,
    private readonly dateService: DateService,
    @InjectRepository(Answer)
    private answerRepository: Repository<Answer>,
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,
    @Inject(forwardRef(() => AttemptsService))
    private attemptsService: AttemptsService,
    protected readonly store: StoreService,
  ) {}

  async create(dto: CreateSubmissionDto, assignTutor = false) {
    await this.checkHaveAttempts({ userId: dto.userId, assessmentId: dto.assessmentId });

    const courseSubmissions = await this.checkCourseSubmisionExisting(dto);

    if (courseSubmissions) {
      return {
        ...courseSubmissions,
        ...dto,
      };
    }
    let tutor: { id: string } | null = null;

    // Стандартный поиск подходящего тьютора
    const getStandartTutor = async () => {
      const selectedTutor = await this.getTutorForNewSubmission(dto);
      if (selectedTutor) {
        return { id: selectedTutor.id };
      } else {
        return null;
      }
    };

    if (assignTutor) {
      if (dto.attempt === 1) {
        if (dto.bindTutor) {
          // TODO: Берём конкретного тьютора для этого студента
        } else {
          tutor = await getStandartTutor();
        }
      } else {
        // Если это не первая попытка сдачи решения, то берем тьютора из предыдущей попытки
        const previousAttemptsWithTutor = await this.submissionsRepository
          .createQueryBuilder('submission')
          .select('submission.tutor_id as id')
          .innerJoin('submission.tutor', 'tutor')
          .where('tutor.active = TRUE')
          .innerJoin('tutor.enrollments', 'enrollment')
          .where('enrollment.course_id = submission.course_id')
          .andWhere('enrollment.role = :role', { role: SystemRole.Tutor })
          .andWhere('enrollment.active = TRUE')
          .andWhere('(enrollment.date_from is null OR enrollment.date_from < NOW())')
          .andWhere('(enrollment.date_to is null OR enrollment.date_to > NOW())')
          .andWhere('assessment_id = :assessmentId', { assessmentId: dto.assessmentId })
          .andWhere('submission.user_id = :userId', { userId: dto.userId })
          .andWhere('tutor_id is not null')
          .andWhere('status = :status', { status: SubmissionStatus.Graded })
          .orderBy('attempt', 'DESC')
          .getRawMany();

        const lastAttemptTutor = previousAttemptsWithTutor.shift();
        tutor = lastAttemptTutor?.id ? lastAttemptTutor : await getStandartTutor();
      }
    }

    // const isDefaultAssessment =
    //   this.quizService.isDefaultAssessment(assessment);
    if (dto.current_attempt !== false) {
      dto.current_attempt = true;
    }

    // Remove "current_attempt" status from all other submissions
    if (dto.current_attempt === true && dto.userId && dto.assessmentId && dto.lectureId) {
      const assessment = await this.assessmentsRepository.findOne(dto.assessmentId);
      const versionIds = (
        await this.assessmentsRepository.find({
          where: {
            commonId: assessment.commonId,
          },
        })
      ).map((v: Assessment) => v.id);

      this.submissionsRepository
        .createQueryBuilder('submission')
        .update()
        .set({ current_attempt: false })
        .where('assessment_id in (:...assessmentIds)', {
          assessmentIds: versionIds,
        })
        .andWhere('user_id = :userId', { userId: dto.userId })
        .andWhere('lecture_id = :lectureId', {
          lectureId: dto.lectureId,
        })
        .execute();
    }

    let tariff = null;
    if (dto.status == SubmissionStatus.Submitted) {
      tariff = await this.createSubmissionTariff({ ...dto });
    }

    const result = await this.submissionsRepository.save({
      ...dto,
      user: { id: dto.userId },
      assessment: { id: dto.assessmentId },
      lecture: { id: dto.lectureId },
      module: { id: dto.moduleId },
      course: { id: dto.courseId },
      tutor,
      tariff,
    });

    if (assignTutor && tutor?.id) {
      // Дёрнуть сокет тьютора
      this.socketsGateway.dataUpdateForUser('tutorSubmissions', tutor.id);
    }
    return result;
  }

  async checkHaveAttempts(dto: HaveAttemptsDto): Promise<void> {
    const hasFeatureAccess = await this.usersService.hasFeatureAccess(dto.userId, FeatureName.AssessmentAttemptLimits);

    if (hasFeatureAccess) {
      const haveAttempts = await this.attemptsService.haveAttempts({ userId: dto.userId, assessmentId: dto.assessmentId });
      if (!haveAttempts) {
        throw new BadRequestException('Attempts are over');
      }
    }
  }

  async createSubmissionTariff(dto: UpdateSubmissionDto) {
    const assessment = await this.quizService.canStartQuiz(dto);
    let tariff = null;

    if (assessment.type === AssessmentType.Default) {
      const allAttempts = !dto.isQuestion
        ? await this.submissionsRepository
            .createQueryBuilder('submission')
            .andWhere('assessment_id = :assessmentId', { assessmentId: dto.assessmentId })
            .andWhere('user_id = :userId', { userId: dto.userId })
            .andWhere('is_question = :isQuestion', { isQuestion: dto.isQuestion })
            .getMany()
        : null;

      const createTariff = await this.levelService.getCreateTariff(dto.assessmentId, dto.submittedAt, dto.isQuestion);

      const outdatedHours = allAttempts?.length > 1 ? createTariff.outdatedHours + NEXT_ATTEMPT_OUTDATED_HOURS : createTariff.outdatedHours;

      tariff = {
        createTariff,
        urgentDate: await this.dateService.addBusinessHours(dto.submittedAt, createTariff.urgentHours),
        outdatedDate: await this.dateService.addBusinessHours(dto.submittedAt, outdatedHours),
      };
    }

    return tariff;
  }

  async createWebinarSubmision(dto: CreateWebinarSubmissionDto) {
    await this.quizService.canStartQuiz(dto);

    const webinarSubmissions = await this.checkWebinarSubmisionExisting(dto);

    if (webinarSubmissions) {
      return {
        ...webinarSubmissions,
        ...dto,
      };
    }

    const result = await this.submissionsRepository.save({
      ...dto,
      user: { id: dto.userId },
      assessment: { id: dto.assessmentId },
      webinar: { id: dto.webinarId },
    });
    return result;
  }

  async checkCourseSubmisionExisting(dto: CreateSubmissionDto) {
    return await this.submissionsRepository
      .createQueryBuilder('submission')
      .where('assessment_id = :assessmentId', { assessmentId: dto.assessmentId })
      .andWhere('user_id = :userId', { userId: dto.userId })
      .andWhere('lecture_id = :lectureId', { lectureId: dto.lectureId })
      .andWhere('status = :status', { status: SubmissionStatus.New })
      .getOne();
  }

  async checkWebinarSubmisionExisting(dto: CreateWebinarSubmissionDto) {
    return await this.submissionsRepository
      .createQueryBuilder('submission')
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.user', 'user')
      .leftJoinAndSelect('submission.webinar', 'webinar')
      .where('assessment.id = :assessmentId', { assessmentId: dto.assessmentId })
      .andWhere('user.id = :userId', { userId: dto.userId })
      .andWhere('webinar.id = :webinarId', { webinarId: dto.webinarId })
      .andWhere('submission.status = :status', { status: SubmissionStatus.New })
      .getOne();
  }

  calcData(submission: Submission) {
    const now = new Date();
    const dayEnd = this.dateService.endOfDay(now);
    const dayStart = this.dateService.startOfDay(now);

    let sli = { isUrgent: false, isOutdated: false };

    if (submission.tariff) {
      sli = {
        isUrgent: (submission.tariff.urgentDate <= now && submission.tariff.outdatedDate > now) || undefined,
        isOutdated: (submission.tariff.outdatedDate < now && submission.status == SubmissionStatus.Submitted) || undefined,
      };
    }

    return {
      ...sli,
      isGradedToday: (submission.gradedAt >= dayStart && submission.gradedAt <= dayEnd) || undefined,
      isLocked: submission.mutex && new Date(submission.mutex) > new Date(),
    };
  }

  async prepareForHomeworksPage(submissions: Submission[]) {
    for (const submission of submissions) {
      submission.userSubmissionsCount = await this.userSubmissionsCount(submission.user.id);
      // await this.quizService.addSubmissionResult(submission);

      const { isUrgent, isOutdated, isGradedToday, isLocked } = this.calcData(submission);

      submission.isUrgent = isUrgent;
      submission.isOutdated = isOutdated;
      submission.isGradedToday = isGradedToday;
      submission.isLocked = isLocked;

      if (submission.tariff) {
        submission.tariff.levelTitle = submission.tariff.createTariff.level.title;
      }
    }

    return submissions;
  }

  async findAll(paging: PaginationDto, query: QuerySubmissionDto): Promise<Paginated<Submission[]>> {
    const queryBuilder = await this.createQueryBuilderWithFilter(query, paging);

    if (query.sortField) {
      queryBuilder.orderBy(query.sortField, query.sortDirection);
    }

    const [results, total] = await queryBuilder.getManyAndCount();

    const submissions = await this.prepareForHomeworksPage(results);

    return {
      results: submissions,
      meta: { ...paging, total },
    };
  }

  async findAllOptimized(paging: PaginationDto, query: QuerySubmissionDto): Promise<Paginated<Submission[]>> {
    const queryBuilder = await this.createQueryBuilderWithFilterOptimized(query, paging);

    if (query.sortField) {
      queryBuilder.orderBy(query.sortField, query.sortDirection);
    }

    const [results, total] = await queryBuilder.getManyAndCount();

    const submissions = await this.prepareForHomeworksPage(results);

    return {
      results: submissions,
      meta: { ...paging, total },
    };
  }

  async findByIds(paging: PaginationDto, query: QuerySubmissionIds) {
    const queryBuilder = this.createQueryBuilder();

    if (query.ids) {
      queryBuilder.whereInIds(query.ids);
    }

    if (query.searchString) {
      this.applySearch(queryBuilder, query.searchString, 'user.name, tutor.name, course.title, assessment.title');
    }

    const [results, total] = await queryBuilder.getManyAndCount();

    const submissions = await this.prepareForHomeworksPage(results);

    return {
      results: submissions,
      meta: { ...paging, total },
    };
  }

  async findAllList(paging: PaginationDto, query: QuerySubmissionListDto): Promise<Paginated<Submission[]>> {
    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.lecture', 'lecture')
      .leftJoinAndSelect('submission.course', 'course')
      .leftJoinAndSelect('submission.webinar', 'webinar')
      .andWhere('assessment.id is NOT NULL')
      .leftJoinAndSelect('submission.user', 'user');

    this.applyFilter(queryBuilder, query, 'user.name, webinar.name, course.title, assessment.title, submission.id', paging);

    if (query.sortField) {
      queryBuilder.orderBy(query.sortField, query.sortDirection);
    }

    const [results, total] = await queryBuilder.getManyAndCount();

    return {
      results,
      meta: {
        ...paging,
        total,
        sortField: query.sortField,
        sortDirection: query.sortDirection,
      },
    };
  }

  async submissionCount(query: QuerySubmissionDto) {
    const queryBuilder = await this.createQueryBuilderWithFilterOptimized(query);
    queryBuilder.select('count(distinct submission.id) as cnt');

    const { cnt } = await queryBuilder.getRawOne();

    return cnt;
  }

  urgentCount(query: QuerySubmissionDto) {
    query.submissionState = SubmissionState.Urgent;
    return this.submissionCount(query);
  }

  outdatedCount(query: QuerySubmissionDto) {
    query.submissionState = SubmissionState.Outdated;
    return this.submissionCount(query);
  }

  todayCount(query: QuerySubmissionDto) {
    query.submissionState = SubmissionState.Today;
    return this.submissionCount(query);
  }

  allCount(query: QuerySubmissionDto) {
    query.submissionState = SubmissionState.Main;
    return this.submissionCount(query);
  }

  async getTutorsForFilter(query: QuerySubmissionDto) {
    if (query.asRole !== SystemRole.Teamlead) {
      delete query.tutorId;
    }

    if (query.teamleadId) {
      return getTeamleadTutors(query.teamleadId, new Date());
    }
    return getTutors();
  }

  async getCoursesForFilter(query: QuerySubmissionDto) {
    delete query.courseId;

    const queryBuilder = await this.createQueryBuilderWithFilterOptimized(query);
    queryBuilder.select('distinct course.id as id, course.title as name, course.version as version ').orderBy('course.title');
    return queryBuilder.getRawMany();
  }

  async getModulesForFilter(query: QuerySubmissionDto) {
    delete query.moduleId;

    const queryBuilder = await this.createQueryBuilderWithFilterOptimized(query);
    queryBuilder.select('distinct module.id as id, module.title as name ').orderBy('module.title');

    return queryBuilder.getRawMany();
  }

  async getStudentsForFilter(query: QuerySubmissionDto) {
    delete query.userIds;

    const queryBuilder = await this.createQueryBuilderWithFilterOptimized(query);
    queryBuilder.select('distinct user.id as id, user.name as name ').orderBy('user.name');

    return queryBuilder.getRawMany();
  }

  async getFilterData(query: QuerySubmissionDto) {
    const [tutors, courses, modules, students] = await Promise.all([
      this.getTutorsForFilter({ ...query }),
      this.getCoursesForFilter({ ...query }),
      this.getModulesForFilter({ ...query }),
      this.getStudentsForFilter({ ...query }),
    ]);

    return {
      tutors,
      courses,
      modules,
      students,
    };
  }

  async summary(query: QuerySubmissionDto): Promise<SubmissionSummaryDto> {
    const [allCount, urgentCount, outdatedCount, todayCount] = await Promise.all([
      this.allCount(query),
      this.urgentCount(query),
      this.outdatedCount(query),
      this.todayCount(query),
    ]);

    return {
      allCount,
      urgentCount,
      outdatedCount,
      todayCount,
    };
  }

  createQueryBuilderOptimized() {
    return this.submissionsRepository
      .createQueryBuilder('submission')
      .withDeleted()
      .innerJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.lecture', 'lecture')
      .leftJoinAndSelect('submission.course', 'course')
      .leftJoinAndSelect('submission.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .andWhere('assessment.id is NOT NULL')
      .andWhere('submission.deleted is NULL')
      .leftJoinAndSelect('submission.user', 'user')
      .leftJoinAndSelect('submission.tutor', 'tutor')
      .leftJoinAndSelect('submission.tariff', 'tariff')
      .leftJoinAndSelect('tariff.createTariff', 'createTariff')
      .leftJoinAndSelect('createTariff.level', 'level');
  }

  createQueryBuilder() {
    return this.submissionsRepository
      .createQueryBuilder('submission')
      .withDeleted()
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('assessment.questions', 'assessmentquestions')
      .leftJoinAndSelect('submission.answers', 'answers')
      .leftJoinAndSelect('answers.files', 'files')
      .leftJoinAndSelect('answers.tutorFiles', 'tutorFiles')
      .leftJoinAndSelect('answers.question', 'questions')
      .leftJoinAndSelect('submission.lecture', 'lecture')
      .leftJoinAndSelect('submission.course', 'course')
      .leftJoinAndSelect('submission.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .andWhere('assessment.id is NOT NULL')
      .andWhere('submission.deleted is NULL')
      .leftJoinAndSelect('submission.user', 'user')
      .leftJoinAndSelect('submission.tutor', 'tutor')
      .leftJoinAndSelect('user.avatar', 'avatar1')
      .leftJoinAndSelect('tutor.avatar', 'avatar2')
      .leftJoinAndSelect('submission.tariff', 'tariff')
      .leftJoinAndSelect('tariff.createTariff', 'createTariff')
      .leftJoinAndSelect('createTariff.level', 'level');
  }

  // User for student interface
  async createQueryBuilderWithFilter(query: QuerySubmissionDto, paging?: PaginationDto): Promise<SelectQueryBuilder<Submission>> {
    if (query.submissionState == SubmissionState.Today) {
      query.status = SubmissionStatus.Graded;
    }

    const queryBuilder = this.createQueryBuilder();

    this.applyFilter(queryBuilder, query, 'user.name, tutor.name, course.title, assessment.title', paging);

    return queryBuilder;
  }

  // User for tutor interface
  async createQueryBuilderWithFilterOptimized(query: QuerySubmissionDto, paging?: PaginationDto): Promise<SelectQueryBuilder<Submission>> {
    if (query.submissionState == SubmissionState.Today) {
      query.status = SubmissionStatus.Graded;
    }

    const queryBuilder = this.createQueryBuilderOptimized();

    this.applyFilter(queryBuilder, query, 'user.name, tutor.name, course.title, assessment.title', paging);

    return queryBuilder;
  }

  async fixSubmission(userId: string, assessmentId: string) {
    let count = 0;
    const result = await this.submissionsRepository
      .createQueryBuilder('submission')
      .select(`id, score, status, submitted_at, attempt, current_attempt, created`)
      .where('user_id = :userId and assessment_id = :assessmentId', {
        userId,
        assessmentId,
      })
      .getRawMany();

    result.sort((a, b) => {
      const dateA = a.submitted_at ? a.submitted_at : a.created;
      const dateB = b.submitted_at ? b.submitted_at : b.created;
      const dateDiff = dateB - dateA;
      if (dateDiff == 0) {
        const diff = b.attempt - a.attempt;
        if (diff == 0) {
          return b.score - a.score;
        }
        return diff;
      }
      return dateDiff;
    });

    for (let idx = 0; idx < result.length; idx++) {
      const s = result[idx];
      const update = new UpdateSubmissionDto();

      // Current attempt state
      if (idx == 0 && s.current_attempt !== true) {
        update.current_attempt = true;
      } else if (idx != 0 && s.current_attempt === true) {
        update.current_attempt = false;
      }
      // Attempt number
      if (s.attempt != result.length - idx) {
        update.attempt = result.length - idx;
      }
      // TODO: Success/failed state

      if (Object.keys(update).length > 0) {
        // Update;
        await this.update(s.id, update);
        count++;
      }
    }

    return count;
  }

  async fixSubmissions() {
    let counter = 0;
    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .select([
        'user_id',
        'count(1) as cnt',
        'bool_or(current_attempt) as has_current_attempt',
        'max(submitted_at) as latest_submitted_at',
        'assessment_id',
        `json_agg(
          json_build_object(
            'id', id,
            'score', score,
            'status', status,
            'submitted_at', submitted_at,
            'attempt', attempt,
            'current_attempt', current_attempt,
            'created', created
          )
        ) as attempts`,
      ])
      .groupBy('user_id')
      .addGroupBy('assessment_id')
      .orderBy('cnt', 'DESC');

    const result = await queryBuilder.getRawMany();

    for (let i = 0, l = result.length; i < l; i++) {
      const sub = result[i];
      const numOfAttempts = sub.attempts.length;

      // Sorting: Date-Attempt-Score DESC
      sub.attempts.sort((a, b) => {
        const dateA = a.submitted_at ? a.submitted_at : a.created;
        const dateB = b.submitted_at ? b.submitted_at : b.created;
        const dateDiff = dateB.localeCompare(dateA);
        if (dateDiff == 0) {
          const diff = b.attempt - a.attempt;
          if (diff == 0) {
            return b.score - a.score;
          }
          return diff;
        }
        return dateDiff;
      });

      for (let idx = 0; idx < numOfAttempts; idx++) {
        const s = sub.attempts[idx];
        const update = new UpdateSubmissionDto();

        // Current attempt state
        if (idx == 0 && s.current_attempt !== true) {
          update.current_attempt = true;
        } else if (idx != 0 && s.current_attempt === true) {
          update.current_attempt = false;
        }
        // Attempt number
        if (s.attempt != numOfAttempts - idx) {
          update.attempt = numOfAttempts - idx;
        }
        // TODO: Success/failed state

        if (Object.keys(update).length > 0) {
          // Update;
          await this.update(s.id, update);
          counter++;
        }
      }
      if (i % 100 == 0) {
        console.log(`[${i}/${l}] ${counter} submissions fixed`);
      }
    }
    console.log(`Complete. ${counter} submissions fixed`);
  }

  applyFilter(queryBuilder: SelectQueryBuilder<Submission>, query: QuerySubmissionDto, searchFields: string, paging?: PaginationDto) {
    if (query.onlyDefault) {
      queryBuilder.andWhere('assessment.type = :task', {
        task: AssessmentType.Default,
      });
    }

    if (query.assessmentType) {
      queryBuilder.andWhere('assessment.type IN (:...types)', {
        types: query.assessmentType,
      });
    }

    if (query.webinarId) {
      queryBuilder.andWhere('submission.webinarId = :webinarId', query);
    }

    if (query.submissionId) {
      queryBuilder.andWhere('submission.id = :submissionId', query);
    }
    if (query.courseId) {
      queryBuilder.andWhere('course.id = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.moduleId) {
      queryBuilder.andWhere('module.id = :moduleId', {
        moduleId: query.moduleId,
      });
    }
    if (query.lectureId) {
      queryBuilder.andWhere('lecture.id = :lectureId', {
        lectureId: query.lectureId,
      });
    }
    if (query.assessmentId) {
      queryBuilder.andWhere('assessment.id = :assessmentId', {
        assessmentId: query.assessmentId,
      });
    }

    if (query.userId) {
      queryBuilder.andWhere('user.id = :userId', {
        userId: query.userId,
      });
    } else if (query.userIds?.length > 0) {
      queryBuilder.andWhere('user.id IN(:...userIds)', {
        userIds: query.userIds,
      });
    }
    if (query.status) {
      queryBuilder.andWhere('submission.status = :status', {
        status: query.status,
      });
    } else if (query.statuses) {
      queryBuilder.andWhere('submission.status IN(:...statuses)', {
        statuses: query.statuses,
      });
    }
    if (query.teamleadId) {
      addTeamleadFilter(queryBuilder, query.teamleadId, new Date(), 'submission.course_id = teamlead_enrollment.course_id');
    }
    if (query.tutorId) {
      queryBuilder.andWhere('tutor_id = :tutorId', {
        tutorId: query.tutorId,
      });
    }
    if (query.type === SubmissionType.Homework) {
      queryBuilder.andWhere('not is_question');
    }
    if (query.type === SubmissionType.Question) {
      queryBuilder.andWhere('is_question');
    }
    if (query.isWebinarResult !== undefined) {
      queryBuilder.andWhere('assessment.isWebinarSurvey = :isWebinarResult', {
        isWebinarResult: query.isWebinarResult,
      });
    }

    if (query.submittedStartDate) {
      queryBuilder.andWhere('submitted_at >= :submittedStartDate', {
        submittedStartDate: query.submittedStartDate,
      });
    }

    if (query.submittedEndDate) {
      queryBuilder.andWhere('submitted_at <= :submittedEndDate', {
        submittedEndDate: query.submittedEndDate,
      });
    }

    if (query.gradedStartDate) {
      queryBuilder.andWhere('graded_at >= :gradedStartDate', {
        gradedStartDate: query.gradedStartDate,
      });
    }

    if (query.gradedEndDate) {
      queryBuilder.andWhere('graded_at <= :gradedEndDate', {
        gradedEndDate: query.gradedEndDate,
      });
    }

    const now = new Date();
    const dayEnd = this.dateService.endOfDay(now);
    const dayStart = this.dateService.startOfDay(now);

    if (query.submissionState) {
      if (query.submissionState === SubmissionState.Urgent) {
        queryBuilder
          .andWhere(':now > tariff.urgent_date')
          .andWhere(':now <= tariff.outdated_date', { now })
          .andWhere('submission.status = :status', {
            status: SubmissionStatus.Submitted,
          });
      } else if (query.submissionState === SubmissionState.Outdated) {
        queryBuilder.andWhere(':now > tariff.outdated_date', { now }).andWhere('submission.status = :status', {
          status: SubmissionStatus.Submitted,
        });
      } else if (query.submissionState === SubmissionState.Today) {
        queryBuilder.andWhere('submission.graded_at between :day_start and :day_end', {
          day_start: dayStart,
          day_end: dayEnd,
        });
      }
    }

    if (query.searchString) {
      this.applySearch(queryBuilder, query.searchString, searchFields);
    }

    // Disabled due to high load
    if (false && query.showCriteriaEntries === 'true') {
      queryBuilder
        .leftJoinAndMapMany('answers.assessmentCriteriaEntries', AssessmentCriterionCategory, 'category', 'category.id IS NOT NULL')
        .leftJoinAndSelect('category.criteria', 'criteria', 'criteria.questionId = answers.questionId')
        .leftJoinAndMapOne(
          'criteria.entry',
          AssessmentCriterionEntry,
          'entry',
          'entry.answer_id = answers.id AND entry.criteria_id = criteria.id',
        );
    }

    if (paging && !paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (query.levelId) {
      queryBuilder
        .innerJoinAndSelect('tariff.gradeTariff', 'gradeTariff')
        .innerJoinAndSelect('gradeTariff.level', 'gradeLevel')
        .andWhere('gradeLevel.id = :levelId', {
          levelId: query.levelId,
        });
    }
  }

  applySearch(queryBuilder: SelectQueryBuilder<Submission>, searchString: string, searchFields: string) {
    queryBuilder.andWhere(`LOWER(CONCAT(${searchFields})) like :like`, {
      like: `%${searchString.toLowerCase()}%`,
    });
  }

  findForAssessment(assessmentId: string): Promise<Submission[]> {
    return this.submissionsRepository.createQueryBuilder('submission').where('assessment_id = :assessmentId', { assessmentId }).getMany();
  }

  findForUser(userId: string): Promise<Submission[]> {
    return this.submissionsRepository.createQueryBuilder('submission').where('user_id = :userId', { userId }).getMany();
  }

  findForAssessmentAndUser(assessmentId: string, userId: string): Promise<Submission[]> {
    return this.submissionsRepository
      .createQueryBuilder('submission')
      .where('assessment_id = :assessmentId', { assessmentId })
      .andWhere('user_id = :userId', { userId })
      .getMany();
  }

  @Transactional()
  findOne(id: string, query?: QuerySubmissionOneDto): Promise<Submission> {
    // const relations = [
    //   'assessment',
    //   'user',
    //   'user.avatar',
    //   'answers',
    //   'answers.question',
    //   'answers.files',
    //   'answers.tutorFiles',
    //   'assessment.lecture',
    //   'assessment.lecture.course',
    //   'assessment.lecture.module',
    //   'tutor',
    //   'tutor.avatar',
    //   'lecture',
    //   'course',
    //   'module',
    //   'module.section',
    //   'tariff',
    //   'tariff.createTariff',
    //   'tariff.createTariff.level',
    //   'tariff.gradeTariff',
    //   'tariff.gradeTariff.level',
    // ];
    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .where('submission.id = :id', { id })
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.user', 'user')
      .leftJoinAndSelect('user.avatar', 'avatar')
      .leftJoinAndSelect('submission.answers', 'answer')
      .leftJoinAndSelect('answer.question', 'question')
      .leftJoinAndSelect('answer.files', 'files')
      .leftJoinAndSelect('answer.tutorFiles', 'tutorFiles')
      .leftJoinAndSelect('assessment.lecture', 'lecture')
      .leftJoinAndSelect('lecture.course', 'course')
      .leftJoinAndSelect('lecture.module', 'lectureModule')
      .leftJoinAndSelect('submission.tutor', 'tutor')
      .leftJoinAndSelect('tutor.avatar', 'tutorAvatar')
      .leftJoinAndSelect('submission.lecture', 'submissionLecture')
      .leftJoinAndSelect('submission.course', 'submissionCourse')
      .leftJoinAndSelect('submission.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoinAndSelect('submission.tariff', 'tariff')
      .leftJoinAndSelect('tariff.createTariff', 'createTariff')
      .leftJoinAndSelect('createTariff.level', 'createTariffLevel')
      .leftJoinAndSelect('tariff.gradeTariff', 'gradeTariff')
      .leftJoinAndSelect('gradeTariff.level', 'gradeTariffLevel');

    if (query?.withQuestions) {
      // relations.push('assessment.questions');
      queryBuilder.leftJoinAndSelect('assessment.questions', 'assessmentQuestion');
    }
    if (query?.showCriteriaEntries === 'true') {
      queryBuilder
        .leftJoinAndMapMany('answer.assessmentCriteriaEntries', AssessmentCriterionCategory, 'category', 'category.id IS NOT NULL')
        .leftJoinAndSelect('category.criteria', 'criteria', 'criteria.questionId = answer.questionId')
        .leftJoinAndMapOne(
          'criteria.entry',
          AssessmentCriterionEntry,
          'entry',
          'entry.answer_id = answer.id AND entry.criteria_id = criteria.id',
        );
    }

    // return this.submissionsRepository.findOne(id, {
    //   relations,
    // });
    return queryBuilder.getOne();
  }

  async findOneForTutor(id: string, query?: QuerySubmissionOneDto): Promise<Submission> {
    const submission = await this.findOne(id, query);
    if (!submission) {
      return;
    }
    if (query.showCriteriaEntries === 'true') {
      this.normalizeCriteriaEntries(submission);
    }
    // [LMSP-994] check could we assign home form to tutor in _slug.vue[Message] page
    submission.isLocked = submission.mutex && new Date(submission.mutex) > new Date();
    return {
      ...submission,
      link: this.generateAssessmentLink(submission),
    };
  }

  normalizeCriteriaEntries(submission: Submission) {
    if (submission.answers) {
      for (const answer of submission.answers) {
        answer.assessmentCriteriaEntries = answer.assessmentCriteriaEntries.filter((e: any) => {
          return e.criteria.length > 0;
        });
        if (answer.assessmentCriteriaEntries.length === 0) {
          answer.assessmentCriteriaEntries = null;
        }
      }
    }
  }

  generateAssessmentLink(submission: Submission) {
    // Only generate link for external submissions
    if (!submission.externalId || submission.assessment?.type !== AssessmentType.Default || !submission.course) {
      return '';
    }
    let link = `https://ebac-online.eadbox.com.br/ng/teacher/courses/`;
    link += submission.course.slug;
    link += '/assessments/';
    link += submission.assessment.externalId;
    link += '/corrections/';
    link += submission.externalId;
    link += ';userId=';
    link += submission.user.externalId;
    return link;
  }

  async getBoxSession(email: string, pass: string, updateAuthToken?: boolean): Promise<string | undefined> {
    const loginData = await axios.post('https://ebac-online.eadbox.com/api/login', { email, password: pass });

    if (loginData.data.valid === true) {
      if (updateAuthToken) {
        const user = await this.usersService.findOneByExernalId(loginData.data.user.user_id);

        if (user) {
          await this.usersService.update(user.id, {
            eadboxToken: loginData.data.authentication_token,
          });
        }
      }
      return (
        await axios.get(
          `https://ebac-online.eadbox.com.br/login?${loginData.data.token_authentication_key}=${loginData.data.authentication_token}`,
          {
            maxRedirects: 0,
            validateStatus: function (status) {
              return status >= 200 && status <= 303;
            },
          },
        )
      )?.headers['set-cookie']
        ?.join(';')
        .split(';')
        .find(it => it.startsWith('_session_id'))
        ?.split('=')[1];
    }
    return null;
  }

  async getBoxSessionByToken(token: string) {
    try {
      return (
        await axios.get(`https://ebac-online.eadbox.com.br/login?auth_token=${token}`, {
          maxRedirects: 0,
          validateStatus: function (status) {
            return status >= 200 && status <= 303;
          },
        })
      )?.headers['set-cookie']
        ?.join(';')
        .split(';')
        .find(it => it.startsWith('_session_id'))
        ?.split('=')[1];
    } catch (e) {
      return false;
    }
  }

  async findForTutor(paging: PaginationDto, tutorId: string, query: QuerySubmissionDto) {
    if (query.asRole !== SystemRole.Teamlead) {
      query.tutorId = tutorId;
    }

    const submissions = await this.findAllOptimized(paging, query);
    const results = submissions.results.map(submission => ({
      ...submission,
      link: this.generateAssessmentLink(submission),
    }));

    // Disabled due to high load
    if (false && query.showCriteriaEntries === 'true') {
      for (const submission of results) {
        this.normalizeCriteriaEntries(submission);
      }
    }

    return { ...submissions, results };
  }

  findByExternalId(externalId: string): Promise<Submission> {
    return this.submissionsRepository.findOne(
      { externalId },
      {
        relations: [
          'assessment',
          'user',
          'answers',
          'answers.question',
          'answers.files',
          'answers.tutorFiles',
          'tutor',
          'lecture',
          'module',
          'course',
        ],
      },
    );
  }

  getMutexDate(): Date {
    const MUTEX_TIME = process.env.SUBMISSION_LOCK_SECONDS ?? '3600';
    const mutexTimeInt = parseInt(MUTEX_TIME, 10) * 1000;
    return new Date(Date.now() + mutexTimeInt);
  }

  getSubmissionsToDistribute(): Promise<SubmissionToDistribute[]> {
    // Сортируем все непроверенные и незаблокированные задания по возрастанию даты создания
    const qb = this.submissionsRepository
      .createQueryBuilder('submissions')
      .leftJoin('submissions.assessment', 'assessment')
      .leftJoin('submissions.lecture', 'lecture')
      .leftJoin('submissions.course', 'course')
      .leftJoin('submissions.module', 'module')
      .leftJoin('submissions.tariff', 'tariff')
      .select(['submissions.id AS id', 'course.id AS course_id', 'module.id AS module_id', 'submissions.user_id AS user_id'])
      .where('(mutex IS NULL OR mutex < NOW())')
      .andWhere('submissions.status = :status', {
        status: SubmissionStatus.Submitted,
      })
      .andWhere('submissions.bind_tutor = :bindTutor', { bindTutor: false })
      .andWhere(
        new Brackets(qb => {
          qb.andWhere('submissions.attempt = 1').orWhere('tariff.outdated_date < NOW()').orWhere('submissions.tutor_id is null');
        }),
      )
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .orderBy('submitted_at', 'ASC');
    return qb.getRawMany();
  }

  async getActiveSubmissionsCountGroupedByTutor(tutorIds: string[]): Promise<MutexCount[]> {
    let qb = this.submissionsRepository
      .createQueryBuilder('submissions')
      .leftJoin('submissions.assessment', 'assessment')
      .select('count(submissions.id) as submissions_count, tutor_id')
      .where('status = :status', { status: SubmissionStatus.Submitted })
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .groupBy('tutor_id');

    if (!tutorIds || tutorIds.length == 0) {
      return [];
    }

    qb = qb.andWhere('tutor_id IN (:...tutorIds)', { tutorIds });
    const res = await qb.execute();
    return res.map((r: { tutor_id: string; submissions_count?: string | null }) => ({
      tutor_id: r.tutor_id,
      submissions_count: r.submissions_count ? parseInt(r.submissions_count, 10) : 0,
    }));
  }

  async getActiveMutexCountGroupedByTutor(): Promise<MutexCount[]> {
    const mutexDate = new Date();
    const qb = this.submissionsRepository
      .createQueryBuilder('submissions')
      .select('count(id) as submissions_count, tutor_id')
      .where('mutex > :mutexDate', { mutexDate })
      .groupBy('tutor_id');
    const res = await qb.execute();
    return res.map((r: { tutor_id: string; submissions_count?: string | null }) => ({
      tutor_id: r.tutor_id,
      submissions_count: r.submissions_count ? parseInt(r.submissions_count, 10) : 0,
    }));
  }

  tutorIsEnrolledToModule(tutor: User, moduleId: string): boolean {
    return tutor.modules.some(m => m.id == moduleId);
  }
  tutorIsEnrolledToCourse(tutor: User, courseId: string): boolean {
    return tutor.enrollments.some(e => e.course.id == courseId && e.role == CourseRole.Tutor);
  }
  tutorIsActive(tutor: User): boolean {
    return tutor.active && !tutor.pause;
  }

  async getTutorForNewSubmission(dto: CreateSubmissionDto) {
    const tutorsLoad = await this.getTutorsLoadForNewSubmission(dto);
    return this.getMostAvailableTutor(tutorsLoad, dto.courseId, dto.moduleId, dto.userId);
  }

  async mutateTutorsAddModuleAssignmentsBySection(activeTutors: User[]) {
    for (let i = 0; i < activeTutors.length; i++) {
      const currentTutor = activeTutors[i];
      currentTutor.moduleAssignments = currentTutor.moduleAssignments.map(a => ({
        moduleId: a.moduleId,
        exclusive: a.exclusive,
        courseId: a.module.courseId,
      }));
      if (currentTutor.sectionAssignments) {
        for (let j = 0; j < currentTutor.sectionAssignments.length; j++) {
          const modules = await this.modulesService.findBySectionIds([currentTutor.sectionAssignments[j].sectionId]);
          const mappedModules = modules.map(el => ({
            moduleId: el.id,
            exclusive: currentTutor.sectionAssignments[j].exclusive,
            courseId: el.courseId,
          }));
          currentTutor.moduleAssignments.push(...mappedModules);
        }
      }
    }
  }

  mapTutorsLoad(activeTutors: User[], currentLoad: MutexCount[]) {
    return activeTutors.map(tutor => {
      const count = currentLoad.find(s => s.tutor_id === tutor.id)?.submissions_count ?? 0;
      return {
        id: tutor.id,
        name: tutor.name,
        pause: tutor.pause,
        courses: tutor.enrollments.map(e => e.courseId),
        moduleAssignments: tutor.moduleAssignments,
        blacklistedStudents: tutor.blacklistedStudents?.map(e => e.studentId),
        counter: count,
        submissions: <string[]>[],
      };
    });
  }

  async getTutorsLoad(): Promise<TutorsLoad[]> {
    const promises: [Promise<User[]>, Promise<MutexCount[]>] = [
      this.usersService.getTutorsLoadFindByRole(),
      this.getActiveMutexCountGroupedByTutor(),
    ];

    const [activeTutors, currentLoad]: [User[], MutexCount[]] = await Promise.all(promises);

    await this.mutateTutorsAddModuleAssignmentsBySection(activeTutors);

    const result = this.mapTutorsLoad(activeTutors, currentLoad);
    return result;
  }

  async getTutorsLoadForNewSubmission(dto: CreateSubmissionDto): Promise<TutorsLoad[]> {
    const activeTutors = await this.usersService.getTutorsLoadFindByRole(dto.courseId);
    if (!activeTutors) {
      return [];
    }

    const currentLoad = await this.getActiveSubmissionsCountGroupedByTutor(activeTutors.map(t => t.id));

    await this.mutateTutorsAddModuleAssignmentsBySection(activeTutors);

    const result = this.mapTutorsLoad(activeTutors, currentLoad);
    return result;
  }

  getMostAvailableTutor(tutors: TutorsLoad[], courseId: string, moduleId: string, submissionAuthorId: string): TutorsLoad {
    let courseTutors: TutorsLoad[];

    // Exclude tutors whose submission author is blacklisted
    const tutorsFilteredByBlacklist = tutors.filter(tutor => !tutor.blacklistedStudents.includes(submissionAuthorId));

    // Tutors responsible for particular modules of the course
    const exclusiveTutors = tutorsFilteredByBlacklist.filter(tutor =>
      tutor.moduleAssignments.some(assignment => assignment.courseId === courseId && assignment.exclusive),
    );

    // Check that this module has active exclusive tutor
    const moduleHasActiveExclusiveTutor = exclusiveTutors.some(tutor =>
      tutor.moduleAssignments.some(assignment => assignment.moduleId === moduleId),
    );

    if (moduleHasActiveExclusiveTutor) {
      // Active tutors with assignment for current module
      courseTutors = tutorsFilteredByBlacklist.filter(
        tutor => tutor.moduleAssignments.some(assignment => assignment.moduleId === moduleId) && !tutor.pause,
      );
    } else {
      // Active tutors responsible to the whole course or tutors with assignment for current module
      courseTutors = tutorsFilteredByBlacklist.filter(tutor => {
        const hasCurrentCourseModuleAssignments = tutor.moduleAssignments?.some(a => a.courseId === courseId);
        const tutorHasOnlyCourseEnrollment = tutor.courses.some(id => id === courseId) && !hasCurrentCourseModuleAssignments;
        const tutorHasCourseEnrollmentAndModuleAssignment =
          tutor.courses.some(id => id === courseId) && tutor.moduleAssignments.some(assignment => assignment.moduleId === moduleId);

        return (tutorHasOnlyCourseEnrollment || tutorHasCourseEnrollmentAndModuleAssignment) && !tutor.pause;
      });
    }

    if (courseTutors.length == 0) {
      return null;
    }

    courseTutors.sort((a, b) => a.counter - b.counter);

    // Select random tutor from available list
    const minCounter = courseTutors[0].counter;
    courseTutors = courseTutors.filter(a => a.counter == minCounter);
    return courseTutors[Math.floor(Math.random() * courseTutors.length)];
  }

  /** Distribute submissions among the tutors */
  async distribute() {
    if (await this.store.hasMutex(DISTRIBUTE_MUTEX_NAME)) {
      return []; // Distribute is already in progress, just ignore
    }

    await this.store.setMutex(DISTRIBUTE_MUTEX_NAME, DISTRIBUTE_LOCK_TIMEOUT);

    // Сортируем все непроверенные и незаблокированные задания по возрастанию даты создания

    // Устанавливаем счётчики по тьюторам исходя из открытых Mutex по заданиям
    // (чтобы уже проверенные задания случайно не распределились другому тьютору)
    const [submissions, tutors]: [SubmissionToDistribute[], TutorsLoad[]] = await Promise.all([
      this.getSubmissionsToDistribute(),
      this.getTutorsLoad(),
    ]);

    const unassigned: string[] = [];
    // Распределение сохраняется на 10 минут
    // const DISTRIBUTE_MUTEX = 10 * 60000;
    // const mutex = new Date(Date.now() + DISTRIBUTE_MUTEX);
    // console.log(mutex);

    // Распределяем по имеющимся тьюторам исходя из принципа:
    // - Тьютор подписан на курс
    // - Среди всех тьюторов на курсе на этом тьюторе меньшее кол-во заданий (счётчик учитывает все курсы)
    for (let i = 0, l = submissions.length; i < l; i++) {
      const submission = submissions[i];
      const tutor = this.getMostAvailableTutor(tutors, submission.course_id, submission.module_id, submission.user_id);
      if (tutor) {
        tutor.counter++;
        tutor.submissions.push(submission.id);
      } else {
        unassigned.push(submission.id);
      }
    }

    const distributeLogRepositoryArray = [];

    // Сохраняем по каждому тьютору
    for (let i = 0, l = tutors.length; i < l; i++) {
      const tutor = tutors[i];
      const submissions = tutor.submissions;
      if (!submissions) {
        continue;
      }
      await this.submissionsRepository
        .createQueryBuilder()
        .update(Submission)
        .set({
          tutor: { id: tutor.id },
          isMutexAuto: true,
          mutex: () => `CURRENT_TIMESTAMP + (10 * interval '1 minute')`,
        })
        .where('id = ANY (:submissions)', { submissions })
        .execute();

      // Log distribution
      distributeLogRepositoryArray.concat(
        submissions.map(e => ({
          submissionId: e,
          tutorId: tutor.id,
        })),
      );
    }
    // Log distribution
    distributeLogRepositoryArray.concat(
      unassigned.map(e => ({
        submissionId: e,
        tutorId: null,
      })),
    );

    // Сбрасываем тьютора у заданий без назначения
    // Log distribution save
    await Promise.all([
      this.submissionsRepository
        .createQueryBuilder()
        .update(Submission)
        .set({ tutor: null })
        .where('id = ANY (:unassigned)', { unassigned })
        .execute(),
      this.distributeLogRepository.save(distributeLogRepositoryArray),
    ]);

    // Обновляем клиенты
    this.socketsGateway.dataUpdate('tutorSubmissions');

    await this.store.clearMutex(DISTRIBUTE_MUTEX_NAME);

    return tutors;
  }

  async update(id: string, updateSubmissionDto: UpdateSubmissionDto | TutorUpdateSubmissionDto | UpdateSubmissionGradeDto) {
    const updateFields = { ...updateSubmissionDto, id };
    return this.submissionsRepository.save(updateFields);
  }

  async remove(id: string) {
    const submission = await this.submissionsRepository.findOne(id, {
      relations: ['user', 'assessment'],
    });

    if (!submission) {
      throw new NotFoundException('Submission is not found');
    }

    const deletedAnswers = await this.answerRepository
      .createQueryBuilder('answer')
      .delete()
      .where('submission_id = :submissionId', { submissionId: id })
      .execute();

    const deletedTariffs = await this.submissionTariffRepository
      .createQueryBuilder('submission_tariff')
      .delete()
      .where('submission_id = :submissionId', { submissionId: id })
      .execute();

    const deletedSubmission = await this.submissionsRepository.delete(id);

    const fixedCount = await this.fixSubmission(submission.user.id, submission.assessment.id);

    this.socketsGateway.submissionChange({ ...submission, action: 'remove' });

    return {
      deletedTariffs,
      deletedAnswers,
      deletedSubmission,
      fixedCount,
    };
  }

  async removeList(submissions: Submission[]) {
    return this.submissionsRepository.remove(submissions);
  }

  async removeListByExternalId(externalId: string) {
    return this.submissionsRepository.delete({ externalId });
  }

  async clear() {
    const submissions = await this.submissionsRepository.find();

    if (submissions) {
      await this.submissionsRepository.remove(submissions);
    }
  }

  @Transactional()
  async setMutex(submissionId: string, tutorId: string) {
    const mutexDate = this.getMutexDate();
    const saveData = {
      id: submissionId,
      isMutexAuto: false,
      mutex: mutexDate,
      tutor: { id: tutorId },
    };
    const submissionSave = await this.submissionsRepository.save(saveData);
    const submission = await this.submissionsRepository.findOne(submissionId, {
      relations: ['tutor'],
    });
    this.socketsGateway.submissionChange({ ...submission, action: 'setMutex' });

    return submissionSave;
  }

  unsetMutex(submissionId: string) {
    return this.submissionsRepository.save({
      id: submissionId,
      mutex: null,
    });
  }

  unsetTutorMutexes(tutorId: string) {
    return this.submissionsRepository
      .createQueryBuilder()
      .update()
      .set({ mutex: null })
      .where('tutor_id = :tutorId and mutex is not null', { tutorId })
      .andWhere('mutex > :mutexDate', { mutexDate: new Date() })
      .execute();
  }

  async tutorsPauseIfIdle() {
    const pauseTime = this.dateService.getPauseTime();

    const qb = this.rolesRepository
      .createQueryBuilder('r')
      .select('distinct r.user_id')
      .leftJoin('user_activity', 'a', 'r.user_id = a.user_id')
      .where('(a.modified < :pauseTime or a.modified is null)', { pauseTime })
      .andWhere('r.role = :role', { role: SystemRole.Tutor })
      .andWhere('(r.date_from IS NULL OR r.date_from <= NOW())')
      .andWhere('(r.date_to IS NULL OR r.date_to > NOW())');

    const idledTutorIds = await qb.getRawMany();

    for (const rec of idledTutorIds) {
      await this.tutorPause(rec.user_id);
    }
  }

  async tutorPause(tutorId: string, distribute = false) {
    const tutor = await this.usersService.setPause(tutorId, true);

    // await this.unsetTutorMutexes(tutorId);
    // no longer release mutexes in prepare for new distribution system

    if (distribute) {
      this.distribute(); // Don't await this
    }

    return tutor;
  }

  async tutorUnpause(tutorId: string) {
    const tutor = await this.usersService.setPause(tutorId, false);
    await this.usersService.saveActivity(tutorId, {});
    this.distribute(); // Don't await this
    return tutor;
  }

  async activeStudents(tutorId?: string) {
    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .leftJoin('submission.assessment', 'assessment')
      .innerJoin('submission.user', 'user')
      .select('user')
      .where('submission.status = :status', {
        status: SubmissionStatus.Submitted,
      })
      .andWhere('assessment.type = :type', { type: AssessmentType.Default });

    if (tutorId) {
      queryBuilder.andWhere('submission.tutor_id = :tutorId', { tutorId });
    }

    return queryBuilder.getRawMany();
  }

  async userSubmissionsCount(userId: string) {
    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .leftJoin('submission.assessment', 'assessment')
      .select('count(submission) as cnt')
      .where('submission.status = :status', {
        status: SubmissionStatus.Submitted,
      })
      .andWhere('assessment.type = :type', { type: AssessmentType.Default })
      .andWhere('submission.user_id = :userId', { userId });

    const { cnt } = await queryBuilder.getRawOne();
    return cnt;
  }

  tutorReport(query: QueryTutorReportDto, groupBy: DatePart) {
    const qb = this.entityManager.createQueryBuilder().from(subQuery => {
      subQuery
        .from(Submission, 'submission')
        .innerJoin('submission.assessment', 'assessment')
        .withDeleted()
        .innerJoin('submission.tariff', 'tariff')
        .innerJoin('tariff.gradeTariff', 'grade_tariff')
        .select(
          `date_trunc('${groupBy}', graded_at) as dt, count(submission.id) as cnt,
           count(submission.id) * grade_tariff.value as amount`,
        )
        .andWhere('graded_at is not null')
        .andWhere('assessment.type = :type', { type: AssessmentType.Default })
        .andWhere('not is_question')
        .groupBy('dt, grade_tariff.id')
        .orderBy('dt', query.sortDirection);

      if (query.tutorId) {
        subQuery.andWhere('tutor_id = :tutorId', { tutorId: query.tutorId });
      }

      if (query.periodStart) {
        subQuery.andWhere('graded_at >= :periodStart', {
          periodStart: query.periodStart,
        });
      }

      if (query.periodEnd) {
        subQuery.andWhere('graded_at <= :periodEnd', {
          periodEnd: query.periodEnd,
        });
      }

      if (groupBy === DatePart.Day) {
        subQuery.addSelect('level.title', 'level');
        subQuery.innerJoin('grade_tariff.level', 'level');
        subQuery.addGroupBy('level.id');
      }

      return subQuery;
    }, 'inner');

    if (groupBy === DatePart.Day) {
      return qb.select('*').getRawMany();
    }

    qb.select('dt, sum(cnt) as cnt, sum(amount) as amount');
    qb.groupBy('dt');

    return qb.getRawMany();
  }

  purgeAll() {
    return this.submissionsRepository.clear();
  }

  countSubmission(assessmentId: string, userId: string): Promise<number> {
    return this.submissionsRepository.count({
      assessment: { id: assessmentId },
      user: { id: userId },
    });
  }

  async isSubmissionAuthorBlacklistedByTutor(submissionId: string, tutorId: string): Promise<boolean> {
    const tutor = await this.usersService.findOne(tutorId);
    const submission = await this.submissionsRepository.findOne(submissionId, {
      relations: ['user'],
    });
    return tutor.blacklistedStudents.map(e => e.studentId).includes(submission.user.id);
  }

  async bindTutor(submissionId: string, tutorId: string) {
    if (await this.isSubmissionAuthorBlacklistedByTutor(submissionId, tutorId)) {
      throw new BadRequestException('submission author blacklisted by tutor');
    }
    const resp = await this.submissionsRepository.save({
      id: submissionId,
      tutor: { id: tutorId },
      bindTutor: true,
    });
    const submission = await this.submissionsRepository.findOne(submissionId, {
      relations: ['tutor'],
    });
    this.socketsGateway.submissionChange({ ...submission, action: 'bind' });

    return resp;
  }

  async unbindTutorFromSubmissionsByUserId(tutorId: string, userId: string) {
    return await this.submissionsRepository
      .createQueryBuilder('submission')
      .update()
      .set({
        tutor: null,
        bindTutor: false,
      })
      .where('submission.status = :status', {
        status: SubmissionStatus.Submitted,
      })
      .andWhere('submission.tutor_id = :tutorId', { tutorId })
      .andWhere('submission.user_id = :userId', { userId })
      .execute();
  }

  getSubmissionForFillProgress() {
    return getConnection()
      .createQueryBuilder()
      .select([
        'submission2.score >= assessment.passing_score as ok',
        'submission2.status as status',
        'submission2.submitted_at',
        'sub.user_id',
        'sub.assessment_id',
        'assessment.lecture_id',
      ])
      .from(qb => {
        return qb
          .from(Submission, 'submission')
          .select(['MAX(submission.attempt) as attempt', 'submission.assessment_id', 'submission.user_id'])
          .where('submission.is_question=FALSE')
          .groupBy('submission.assessment_id, submission.user_id');
      }, 'sub')
      .leftJoin(Assessment, 'assessment', 'assessment.id=sub.assessment_id')
      .leftJoin(
        Submission,
        'submission2',
        'submission2.user_id=sub.user_id AND submission2.assessment_id=sub.assessment_id AND submission2.attempt=sub.attempt',
      )
      .leftJoin(
        LectureProgress,
        'lectureProgress',
        'lectureProgress.lecture_id=assessment.lecture_id AND lectureProgress.user_id=sub.user_id',
      )
      .where('submission2.status is not null and assessment.lecture_id is not null')
      .execute();
  }

  updateSubmissionMoveLecture(lecture: Lecture, oldModuleId: string, newModuleId: string) {
    return this.submissionsRepository.update(
      {
        lecture: lecture,
        module: { id: oldModuleId },
      },
      {
        module: { id: newModuleId },
      },
    );
  }

  updateSubmissionMoveModule(module: Module, oldCourseId: string, newCourseId: string) {
    return this.submissionsRepository.update(
      {
        module: module,
        course: { id: oldCourseId },
      },
      {
        course: { id: newCourseId },
      },
    );
  }

  async findForLectureProgress(userId: string, lectureId: string) {
    return getConnection()
      .createQueryBuilder()
      .select(['submission2.score >= assessment.passing_score as ok', 'submission2.status as status'])
      .from(qb => {
        return qb
          .from(Submission, 'submission')
          .select(['MAX(submission.submitted_at) as submitted_at', 'submission.assessment_id', 'submission.user_id'])
          .where('submission.user_id=:userId and submission.lecture_id=:lectureId', {
            userId,
            lectureId,
          })
          .groupBy('submission.assessment_id, submission.user_id');
      }, 'sub')
      .leftJoin(Assessment, 'assessment', 'assessment.id=sub.assessment_id')
      .leftJoin(
        Submission,
        'submission2',
        'submission2.user_id=sub.user_id AND submission2.assessment_id=sub.assessment_id AND submission2.submitted_at=sub.submitted_at',
      )
      .execute();
  }

  async findForLectureProgressWithoutSubmissionAndViewed() {
    return getConnection()
      .createQueryBuilder()
      .select([
        'enrollment.user_id',
        'lecture.id as lecture_id',
        'LectureProgress.viewed as viewed',
        'LectureProgress.ended as ended',
        'LectureProgress.progress as progress',
      ])
      .from(Enrollment, 'enrollment')
      .leftJoin(Lecture, 'lecture', 'lecture.course_id = enrollment.course_id')
      .innerJoin(Assessment, 'assessment', 'assessment.lecture_id = lecture.id AND assessment."type"=\'default\'')
      .leftJoin(Submission, 'submission', 'submission.user_id=enrollment.user_id AND submission.assessment_id=assessment.id')
      .leftJoin(
        LectureProgress,
        'LectureProgress',
        'LectureProgress.lecture_id = lecture.id AND LectureProgress.user_id = enrollment.user_id',
      )
      .where(
        "enrollment.role='student' AND LectureProgress.viewed is not null AND submission.id is null AND LectureProgress.status is null",
      )
      .execute();
  }

  async setGradeTariff(submission: Submission, date = new Date()) {
    if (submission.isQuestion) {
      const level = (await this.tariffService.getTariffForQuestion(date)).level;
      return this.setGradeLevel(submission, level.id, date);
    } else {
      const level = (await this.levelService.getTariffForAssessment(submission.assessment.id, date)).level;
      return this.setGradeLevel(submission, level.id, date);
    }
  }

  async setGradeLevel(submission: Submission, newLevelId: string, date = new Date()) {
    const newTariff = await this.tariffService.getTariff(newLevelId, date);

    const submissionTariff = {
      submissionId: submission.id,
      gradeTariffId: newTariff.id,
    } as any;

    if (!submission.tariff?.createTariff) {
      submissionTariff.createTariff = newTariff;
      submissionTariff.urgentDate = await this.dateService.addBusinessHours(submission.submittedAt, newTariff.urgentHours);
      submissionTariff.outdatedDate = await this.dateService.addBusinessHours(submission.submittedAt, newTariff.outdatedHours);
    }

    return this.submissionTariffRepository.save(submissionTariff);
  }

  /**
   * This will return the list of all the assessments in the lecture with
   * respected submission status for the user
   * @param userId The user
   * @param lectureId The lecture
   * @returns Object
   */
  async getSubmissionsViewByLecture(userId: string, lectureId: string): Promise<AssessmentStatus[]> {
    const raw = await getConnection()
      .createQueryBuilder()
      .select([
        'va.id as assessment_id',
        'va.type as type',
        'va.passing_score as passing_score',
        'va.lecture_id as lecture_id',
        'vs.score as score',
        'vs.success as success',
        'vs.in_progress as in_progress',
      ])
      .from('view_assessment', 'va')
      .leftJoin('view_submission', 'vs', 'va.common_id = vs.common_id')
      .where('vs.user_id = :userId', { userId })
      .andWhere('va.lecture_id = :lectureId', { lectureId })
      .getRawMany();
    return raw.map(
      (r: any) =>
        ({
          ...r,
        } as AssessmentStatus),
    );
  }

  async findAllForAdminPanel(paging: PaginationDto, query: QuerySubmissionListForAdminPanelDto): Promise<Paginated<any[]>> {
    const selectedCourses = query.checkedCourseElements?.filter((el: any) => el.type === 'course').map((el: any) => el.id);
    const selectedSections = query.checkedCourseElements?.filter((el: any) => el.type === 'section').map((el: any) => el.id);
    const selectedModules = query.checkedCourseElements?.filter((el: any) => el.type === 'module').map((el: any) => el.id);
    const selectedLectures = query.checkedCourseElements?.filter((el: any) => el.type === 'lecture').map((el: any) => el.id);

    const queryBuilder = this.submissionsRepository
      .createQueryBuilder('submission')
      .leftJoinAndSelect('submission.course', 'course')
      .leftJoinAndSelect('submission.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoinAndSelect('submission.lecture', 'lecture')
      .leftJoinAndSelect('submission.tariff', 'tariff')
      .leftJoinAndSelect('submission.tutor', 'tutor')
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('assessment.level', 'level')
      .leftJoinAndSelect('tariff.gradeTariff', 'gradeTariff')
      .leftJoinAndSelect('gradeTariff.level', 'gradeLevel')
      .leftJoinAndSelect('tariff.createTariff', 'createTariff')
      .leftJoinAndSelect('createTariff.level', 'createLevel')
      .where("assessment.type = 'default'");

    if (typeof query.isQuestion === 'boolean') {
      queryBuilder.andWhere('submission.is_question = :isQuestion', {
        isQuestion: query.isQuestion,
      });
    }
    if (query.tutors?.length) {
      queryBuilder.andWhere(
        new Brackets(qb => {
          const tutorIds = query.tutors?.filter(Boolean);
          const withoutTutor = query.tutors?.some(el => !el);
          if (tutorIds?.length) {
            qb.orWhere('submission.tutor.id IN (:...tutors)', {
              tutors: tutorIds,
            });
          }
          if (withoutTutor) {
            qb.orWhere('submission.tutor_id is NULL');
          }
        }),
      );
    }
    if (selectedCourses?.length || selectedSections?.length || selectedModules?.length || selectedLectures?.length) {
      queryBuilder.andWhere(
        new Brackets(qb => {
          if (selectedCourses.length) {
            qb.orWhere('course.id IN (:...selectedCourses)', {
              selectedCourses,
            });
          }
          if (selectedSections.length) {
            qb.orWhere('section.id IN (:...selectedSections)', {
              selectedSections,
            });
          }
          if (selectedModules.length) {
            qb.orWhere('module.id IN (:...selectedModules)', {
              selectedModules,
            });
          }
          if (selectedLectures.length) {
            qb.orWhere('lecture.id IN (:...selectedLectures)', {
              selectedLectures,
            });
          }
        }),
      );
    }
    if (query.submittedFrom) {
      queryBuilder.andWhere('submission.submitted_at >= :submittedFrom', {
        submittedFrom: query.submittedFrom,
      });
    }
    if (query.submittedTo) {
      queryBuilder.andWhere('submission.submitted_at <= :submittedTo', {
        submittedTo: query.submittedTo,
      });
    }
    if (query.gradedFrom) {
      queryBuilder.andWhere('submission.graded_at >= :gradedFrom', {
        gradedFrom: query.gradedFrom,
      });
    }
    if (query.gradedTo) {
      queryBuilder.andWhere('submission.graded_at <= :gradedTo', {
        gradedTo: query.gradedTo,
      });
    }
    if (query.scoreMin) {
      queryBuilder.andWhere('submission.score >= :scoreMin', {
        scoreMin: query.scoreMin,
      });
    }
    if (query.scoreMax) {
      queryBuilder.andWhere('submission.score <= :scoreMax', {
        scoreMax: query.scoreMax,
      });
    }
    if (query.attempt) {
      queryBuilder.andWhere('submission.attempt = :attempt', {
        attempt: query.attempt,
      });
    }
    if (query.levels?.length) {
      queryBuilder.andWhere(
        '((gradeLevel.id IN (:...levels) AND submission.status = :statusGraded) OR (createLevel.id IN (:...levels) AND submission.status != :statusGraded))',
        {
          levels: query.levels,
          statusGraded: SubmissionStatus.Graded,
        },
      );
    }
    if (query.statuses?.length) {
      queryBuilder.andWhere('submission.status IN (:...statuses)', {
        statuses: query.statuses,
      });
    }
    if (query.isGradedAfterOutdated) {
      queryBuilder.andWhere('submission.graded_at is not NULL');
      queryBuilder.andWhere('tariff.outdated_date is not NULL');
      queryBuilder.andWhere('submission.graded_at > tariff.outdated_date');
    }
    if (query.isGradedAfterOutdated === false) {
      queryBuilder.andWhere('submission.graded_at is not NULL');
      queryBuilder.andWhere('tariff.outdated_date is not NULL');
      queryBuilder.andWhere('submission.graded_at <= tariff.outdated_date');
    }

    if (paging && !paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (query.sortField) {
      let sortField;
      if (query.sortField === 'submittedAt') {
        sortField = 'submission.submittedAt';
      } else if (query.sortField === 'gradedAt') {
        sortField = 'submission.gradedAt';
      } else if (query.sortField === 'deadline') {
        sortField = 'tariff.outdatedDate';
      } else if (query.sortField === 'type') {
        sortField = 'submission.isQuestion';
      } else if (query.sortField === 'tutor') {
        sortField = 'tutor.name';
      } else if (query.sortField === 'course') {
        sortField = 'course.title';
      } else if (query.sortField === 'section') {
        sortField = 'section.title';
      } else if (query.sortField === 'module') {
        sortField = 'module.sort';
      } else if (query.sortField === 'lecture') {
        sortField = 'lecture.sort';
      } else if (query.sortField === 'status') {
        sortField = 'submission.status';
      } else if (query.sortField === 'attempt') {
        sortField = 'submission.attempt';
      } else if (query.sortField === 'score') {
        sortField = 'submission.score';
      } else {
        sortField = 'submission.submittedAt';
      }
      queryBuilder.orderBy(sortField, query.sortDirection);
    } else {
      queryBuilder.orderBy('submission.submittedAt', 'DESC');
    }

    const [submissions, total] = await queryBuilder.getManyAndCount();
    const results = submissions.map((el, idx) => {
      const convertHoursToDaysAndHours = (hours: number | string, zeroDiff?: string): string => {
        if (!hours) return zeroDiff;
        const days = Math.floor(Number(hours) / 24);
        hours = Number(hours) - days * 24;
        return days && hours ? `${days}d ${hours}h` : days && !hours ? `${days}d` : `${hours}h`;
      };

      const outdatedDiff =
        el.status === SubmissionStatus.Graded && el.tariff?.outdatedDate && dayjs(el.gradedAt).isAfter(el.tariff.outdatedDate)
          ? convertHoursToDaysAndHours(dayjs(el.gradedAt).diff(dayjs(el.tariff.outdatedDate), 'hours'), '< 1h')
          : el.tariff?.outdatedDate && dayjs().isAfter(el.tariff.outdatedDate) && !el.score && el.status !== SubmissionStatus.Graded
          ? convertHoursToDaysAndHours(dayjs().diff(dayjs(el.tariff.outdatedDate), 'hours'), '< 1h')
          : '';

      const gradedAt = el.gradedAt ? dayjs(el.gradedAt).format('YYYY-MM-DD HH:mm') : '';
      const deadline = el.tariff?.outdatedDate ? dayjs(el.tariff.outdatedDate).format('YYYY-MM-DD HH:mm') : '';
      const submittedAt = el.submittedAt ? dayjs(el.submittedAt).format('YYYY-MM-DD HH:mm') : '';

      const correctionOverallTime = dayjs(el.gradedAt).diff(dayjs(el.submittedAt), 'hours');
      const overallCorrection = el.submittedAt && el.gradedAt ? convertHoursToDaysAndHours(correctionOverallTime, '< 1h') : '';
      const courseTitle = `${el.course?.title}${el.course?.version > 1 ? ' v' + el.course.version : ''}`;
      const level = el.status === SubmissionStatus.Graded ? el.tariff?.gradeTariff?.level?.title : el.tariff?.createTariff?.level?.title;
      const moduleNumber = el.module?.customNumber || el.module?.number;
      const moduleTitle = moduleNumber !== null ? `${moduleNumber} - ${el.module?.title}` : `${el.module?.title}`;
      const lectureNumber = el.lecture?.customNumber || el.lecture?.number;
      const lectureTitle = lectureNumber !== null ? `${lectureNumber} - ${el.lecture?.title}` : `${el.lecture?.title}`;

      return {
        id: el.id,
        attempt: el.attempt,
        score: el.score,
        status: el.status,
        course: courseTitle,
        section: el.module?.section?.title,
        module: moduleTitle,
        lecture: lectureTitle,
        submittedAt,
        deadline,
        gradedAt,
        overallCorrection,
        outdatedDiff,
        tutor: el.tutor?.name,
        level,
        isQuestion: el.isQuestion,
        type: el.isQuestion ? 'Question' : 'Homework',
        link: `${config.get('TUTOR_LMS_URL')}/submission/${el.id}`,
      };
    });

    return {
      results,
      meta: {
        ...paging,
        total,
        sortField: query.sortField,
        sortDirection: query.sortDirection,
      },
    };
  }
}
