import { LectureStatus, ShowResult, SubmissionStatus } from '@lib/api/types';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Request,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as assert from 'assert';
import { LecturesService } from 'src/courses/lectures.service';
import { populateLogBySubmission, PopulateLogWith, SetLogTag } from 'src/service/log/logging';
import { Answer } from '../lib/entities/answer.entity';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { AnswersService } from './answers.service';
import { AssessmentsService } from './assessments.service';
import { CreateAnswerDto } from './dto/create-answer.dto';
import { TutorUpdateAnswerDto } from './dto/tutor-update-answer.dto';
import { UpdateAnswerDto } from './dto/update-answer.dto';
import { QuizService } from './quiz.service';
import { SubmissionsService } from './submissions.service';
import { QuerySurveyAnswersDto } from './dto/query-survey-answers.dto';
import { Pagination } from '../pagination.decorator';
import { PaginationDto } from '../pagination.dto';
import { WebinarService } from 'src/webinar/webinar.service';
import { Permissions } from 'src/permissions.decorator';
import { AssessmentCriteriaEntryService } from '../criteria-based-assessments/assessment-criteria-entry.service';
import { AssessmentType, SystemRole } from '@lib/types/enums';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { User } from '@entities/user.entity';
import { CRUD } from 'src/lib/interfaces/CRUD';

@ApiTags('answers')
@ApiBearerAuth()
@Controller('')
export class AnswersController implements CRUD {
  constructor(
    private readonly submissionsService: SubmissionsService,
    private readonly answersService: AnswersService,
    private readonly lecturesService: LecturesService,
    private readonly webinarService: WebinarService,
    private quizService: QuizService,
    private assessmentsService: AssessmentsService,
    private readonly assessmentCriteriaEntryService: AssessmentCriteriaEntryService,
  ) {}

  @Get('surveys/rate')
  @RequirePermissions(Permission.SURVEY_VIEW, Permission.WEBINAR_SURVEY_VIEW)
  surveyRateAnswers(@Query() query: QuerySurveyAnswersDto, @Permissions() perms: Permission[]) {
    if (!perms.includes(Permission.SURVEY_VIEW) && query.lectureId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_SURVEY_VIEW_PERMISSIONS');
    }
    if (!perms.includes(Permission.WEBINAR_SURVEY_VIEW) && query.webinarId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_SURVEY_VIEW_PERMISSIONS');
    }
    return this.answersService.surveyRateAnswers(query);
  }

  @Get('surveys/text-answers')
  @RequirePermissions(Permission.SURVEY_VIEW, Permission.WEBINAR_SURVEY_VIEW)
  surveyAnswers(@Query() query: QuerySurveyAnswersDto, @Pagination() paging: PaginationDto, @Permissions() perms: Permission[]) {
    if (!perms.includes(Permission.SURVEY_VIEW) && query.lectureId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_SURVEY_VIEW_PERMISSIONS');
    }
    if (!perms.includes(Permission.WEBINAR_SURVEY_VIEW) && query.webinarId) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_SURVEY_VIEW_PERMISSIONS');
    }
    return this.answersService.surveyAnswers(query, paging);
  }

  @Post('submissions/:submissionId/answers')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Create a new answer for submission' })
  @ApiResponse({
    status: 201,
    description: 'Created answer',
    type: Answer,
  })
  @PgConstraints()
  @PopulateLogWith(async (log, context) => {
    const submissionId = log.url.slice(1).split('/')[2];
    return populateLogBySubmission(submissionId, context);
  })
  @SetLogTag('AnswersController.create')
  create(@Param('submissionId', new ParseUUIDPipe()) submissionId: string, @Body() createAnswerDto: CreateAnswerDto) {
    return this.answersService.create({ submissionId, ...createAnswerDto });
  }

  @Get('submissions/:submissionId/answers')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'List all answers for submission' })
  @ApiResponse({
    status: 200,
    type: [Answer],
  })
  @PopulateLogWith(async (log, context) => {
    const submissionId = log.url.slice(1).split('/')[2];
    return populateLogBySubmission(submissionId, context);
  })
  @SetLogTag('AnswersController.findAll')
  findAll(@Param('submissionId', new ParseUUIDPipe()) submissionId: string) {
    return this.answersService.findForSubmission(submissionId);
  }

  @Get('answers/:id')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'Get one answer by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a answer by id',
    type: Answer,
  })
  @PopulateLogWith(async (log, context) => {
    const answerService = context.get(AnswersService);
    const answerId = log.url.slice(1).split('/')[2];
    const answer = await answerService.findOne(answerId);
    return populateLogBySubmission(answer.submission.id, context);
  })
  @SetLogTag('AnswersController.findOne')
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.answersService.findOne(id);
  }

  @Patch('answers/:id')
  @ApiOperation({ summary: 'Updates an answer' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateAnswerDto: UpdateAnswerDto, @Request() req) {
    const answer = await this.answersService.find({ id }, [
      'question',
      'submission',
      'submission.user',
      'submission.assessment',
      'submission.assessment.questions',
    ]);

    if (
      !(
        (req.user as Express.User).hasPermission(Permission.SUBMISSIONS_MANAGE) ||
        (answer.length && answer[0].submission && answer[0].submission.user.id == req.user.id) ||
        !answer.length
      )
    ) {
      return new UnauthorizedException('SUBMISSIONS_MANAGE or owner can change answer');
    }

    if (answer.length && answer[0].submission) {
      await this.quizService.canUpdateAnswer(answer[0]);
    }
    const answerUpdated = await this.answersService.update(id, updateAnswerDto);
    const result = this.quizService.addAnswerResult(
      { ...answer[0], data: updateAnswerDto.data },
      answer[0].submission.assessment,
      req.user.id,
    );

    return { ...answerUpdated, result };
  }

  @Delete('answers/:id')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Deletes an answer' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  async remove(@Param('id', new ParseUUIDPipe()) id: string) {
    const answer = await this.answersService.findOne(id);

    if (answer && answer.submission) {
      await this.quizService.canDeleteAnswer(answer);
    }
    return this.answersService.remove(id);
  }

  @Post('user/answers/:assessmentId')
  @ApiOperation({ summary: 'Creates an answer for assessment' })
  @ApiResponse({
    status: 200,
    description: 'Returns created answer',
  })
  @PgConstraints()
  async createAnswer(
    @Param('assessmentId', new ParseUUIDPipe()) assessmentId: string,
    @Body() createAnswerDto: CreateAnswerDto,
    @Request() req,
  ) {
    const assessment = await this.assessmentsService.findOne(assessmentId);

    if (assessment.type === AssessmentType.Default) {
      if (!createAnswerDto.comment && !createAnswerDto?.files.length) {
        throw new BadRequestException('no comment or an file');
      }
    }

    if (this.quizService.isQuizAssessment(assessment) || this.quizService.isSurveyAssessment(assessment)) {
      if (createAnswerDto.webinarId) {
        const { soon, closed } = await this.webinarService.findOneSurvey(createAnswerDto.webinarId);
        if (soon || closed) {
          throw new BadRequestException('survey closed');
        }
      }
      await this.quizService.canSaveAnswer(createAnswerDto, assessment, req.user.id);
      const answer = await this.answersService.find({
        submission: { id: createAnswerDto.submissionId },
        question: { id: createAnswerDto.questionId },
      });

      let answerResult;
      if (answer.length) {
        if (!assessment.canChangeAnswer) {
          throw new BadRequestException('cant change answer');
        }
        answerResult = await this.answersService.update(answer[0].id, createAnswerDto);
      } else {
        answerResult = await this.answersService.create(createAnswerDto);
      }
      if (assessment.showResult === ShowResult.onQuestion) {
        const answer = await this.answersService.find({
          submission: { id: createAnswerDto.submissionId },
          question: { id: createAnswerDto.questionId },
        });
        answerResult.result = await this.quizService.addAnswerResult(answer[0], assessment, req.user.id);
      }

      return answerResult;
    }

    const submissions = await this.submissionsService.findForAssessmentAndUser(assessmentId, req.user.id);

    let submission = submissions.find(s => s.status == SubmissionStatus.New || s.status == SubmissionStatus.Draft);
    if (!submission) {
      if (submissions.find(s => s.status == SubmissionStatus.Submitted)) {
        return null;
      }

      submission = await this.submissionsService.create(
        {
          userId: req.user.id,
          assessmentId: assessmentId,
          courseId: assessment.lecture.course.id,
          lectureId: assessment.lecture.id,
          moduleId: assessment.lecture.module.id,
          status: SubmissionStatus.New,
          answers: [],
          attempt: submissions.length + 1,
          current_attempt: true,
          isQuestion: createAnswerDto.isQuestion,
        },
        true,
      );
    } else {
      this.submissionsService.update(submission.id, {
        draftAt: null,
        submittedAt: new Date(),
        isQuestion: createAnswerDto.isQuestion,
      });
    }

    let videoBlockData = {};
    if (createAnswerDto.kalturaEntryId) {
      videoBlockData = {
        kalturaEntryId: createAnswerDto.kalturaEntryId,
      };
    }

    const answers = await this.answersService.find({
      submission: { id: submission.id },
      question: { id: createAnswerDto.questionId },
    });

    const payload = {
      submissionId: submission.id,
      ...createAnswerDto,
      data: {
        ...createAnswerDto.data,
        ...videoBlockData,
      },
    };

    const result = answers?.length ? await this.answersService.update(answers[0].id, payload) : await this.answersService.create(payload);

    if (result) {
      const tariff = await this.submissionsService.createSubmissionTariff({
        assessmentId: assessmentId,
        submittedAt: new Date(),
        isQuestion: createAnswerDto.isQuestion,
        userId: req.user.id,
      });

      this.submissionsService.update(submission.id, {
        status: SubmissionStatus.Submitted,
        submittedAt: new Date(),
        tariff,
      });
    }

    if (!createAnswerDto.isQuestion) {
      await this.lecturesService.saveLectureProgress(req.user.id, assessment.lecture.id, {
        status: LectureStatus.inProgress,
      });
    }

    return result;
  }

  @Patch('tutor/answers/:id')
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  )
  @RequirePermissions(Permission.SUBMISSIONS_TUTOR_REVIEW, Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Updates an answer after review' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  @Transactional()
  async review(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateAnswerDto: TutorUpdateAnswerDto, @Request() req) {
    if (typeof updateAnswerDto.tutorComment === 'string' && !updateAnswerDto.tutorComment) {
      throw new BadRequestException('no comment');
    }
    const answer = await this.answersService.findOne(id);
    assert(answer, new NotFoundException('Answer is not found'));

    const { submission } = answer;

    const canUserReview =
      (req.user.isTutor() && submission.tutor?.id == req.user.id) ||
      (req.user.isTeamlead() && submission.tutor?.id && req.user.teamleadTutorIds.includes(submission.tutor.id)) ||
      req.user.hasPermission(Permission.SUBMISSIONS_MANAGE);
    assert(canUserReview, new UnauthorizedException('This answer does not belong to submission available to this tutor'));
    if (updateAnswerDto.entries) {
      await this.assessmentCriteriaEntryService.create(id, updateAnswerDto.entries);
    }

    const { serviceLevelId: serviceLevelId } = updateAnswerDto;

    if (serviceLevelId) {
      const user: User = req.user;
      assert(
        [SystemRole.Admin, SystemRole.Superadmin, SystemRole.Manager].some(role => user.roles.some(userRole => userRole.role === role)),
        new ForbiddenException('This user has no permissions to change service level'),
      );
      await this.submissionsService.setGradeLevel(submission, serviceLevelId, submission.gradedAt);
    } else {
      submission.isQuestion = updateAnswerDto.isQuestion;
      await this.submissionsService.setGradeTariff(submission);
    }

    return this.answersService.update(id, updateAnswerDto);
  }
}
