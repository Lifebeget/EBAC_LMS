/* eslint-disable @typescript-eslint/no-namespace */

export namespace Request {
  export interface Init {
    data: PokedexData;
  }

  interface PokedexData {
    type: string;
    id: string;
    relationships: DataRelationships;
  }

  interface DataRelationships {
    answers: Answers;
    user: User;
  }

  interface Answers {
    data: Datum[];
  }

  interface Datum {
    type: string;
    attributes: Attributes;
    relationships: DatumRelationships;
    id: string;
  }

  interface Attributes {
    correctionNote: string;
    grade: number;
  }

  interface DatumRelationships {
    question: User;
  }

  interface User {
    data: UserData;
  }

  interface UserData {
    id: string;
  }
}

export namespace Response {
  export interface Init {
    data: Data;
    included: Included[];
  }

  interface Data {
    id: string;
    type: string;
    attributes: DataAttributes;
    relationships: DataRelationships;
  }

  interface DataAttributes {
    startedAt: Date;
    endedAt: Date;
    chances: number;
    attempt: number;
    humanChances: number;
    approved: boolean;
    finished: boolean;
    corrected: boolean;
    current: boolean;
    maximumGrade: boolean;
    persisted: boolean;
    grade: number;
  }

  interface DataRelationships {
    lecture: Assessment;
    user: Assessment;
    assessment: Assessment;
    answers: Answers;
    questions: Answers;
    subscription: Assessment;
  }

  interface Answers {
    data: DAT[] | null;
  }

  interface DAT {
    id: string;
    type: string;
  }

  interface Assessment {
    data: DAT;
  }

  interface Included {
    id: string;
    type: string;
    attributes: IncludedAttributes;
    relationships?: IncludedRelationships;
  }

  interface IncludedAttributes {
    createdAt?: Date | null;
    slug?: string;
    description?: null | string;
    position?: number;
    title?: string;
    updatedAt?: Date;
    extra?: boolean | null;
    minimumGrade?: number;
    publishedAt?: Date;
    alwaysReleased?: boolean;
    releasesAt?: null;
    releasesAtFuture?: null;
    releasesIn?: null;
    showForum?: boolean;
    free?: boolean;
    private?: boolean;
    amountOfContents?: number;
    price?: null;
    tagsArray?: any[];
    youtubeVideoLink?: null;
    timeAvailable?: null;
    publishedUntil?: null;
    unlisted?: boolean;
    subscribable?: boolean;
    subscriptionsLimit?: null;
    requestable?: boolean;
    requestNote?: boolean;
    workload?: null;
    topics?: null;
    objective?: null;
    targetAudience?: null;
    certification?: null;
    methodology?: null;
    teachers?: null;
    pagePart?: null;
    requireLastLectureToBeFinished?: null;
    requireLastLectureToBeApproved?: null;
    estimatedTimes?: boolean;
    defaultEstimatedTime?: null;
    skipCourseHome?: boolean;
    dontSendCompleted?: boolean;
    disableCertificate?: boolean;
    customCertificate?: boolean;
    videosViewLimit?: null;
    hideVideoProgressBar?: boolean;
    seoTitle?: null;
    seoDescription?: null;
    published?: boolean;
    paid?: boolean;
    logo?: Logo;
    banner?: Banner;
    commentRating?: null;
    lecturesCount?: number;
    subscribersCount?: number;
    tutor?: boolean;
    author?: boolean;
    videoLimitEnabled?: boolean;
    originalPrice?: null;
    minInstallmentMonthlyPrice?: number;
    maxInstallmentMonths?: number;
    showOnHome?: boolean;
    homePosition?: number;
    categoryPosition?: number;
    installmentInterest?: number;
    freeInstallmentMonths?: number;
    name?: string;
    state?: string;
    email?: string;
    betas?: any[];
    nameAndEmail?: string;
    campaignID?: null;
    pagarmeRecipient?: null;
    avatarThumb?: string;
    locale?: string;
    manual?: boolean;
    randomQuestions?: number;
    deadline?: number;
    redos?: number;
    showMultipleChoiceResult?: string;
    mediated?: null;
    startsAt?: null;
    endsAt?: null;
    weight?: number;
    maximumGrade?: boolean;
    neverRequireCorrection?: boolean;
    canMediate?: boolean;
    body?: string;
    needsCorrection?: boolean;
    grade?: number;
    answered?: boolean;
    correct?: boolean;
    corrected?: boolean;
    correctionNote?: string;
  }

  interface Banner {
    banner: null;
  }

  interface Logo {
    thumb: string;
    normal: string;
    medium: string;
    big: string;
    card: string;
  }

  interface IncludedRelationships {
    attendances?: Answers;
    contents?: Answers;
    simpleContents?: Answers;
    subscriptions?: Answers;
    teams?: Answers;
    course?: Assessment;
    module?: Answers;
    lectureStatus?: Answers;
    permissions?: Answers;
    category?: Assessment;
    plan?: Answers;
    owner?: Assessment;
    modules?: Answers;
    dependencies?: Answers;
    rows?: Answers;
    draftRows?: Answers;
    certificate?: Answers;
    questions?: Answers;
    question?: Assessment;
  }
}
