import { Version } from './../lib/interfaces/Version';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Res,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { VersionUpdateDTO } from 'src/courses/dto/update-content-version-update.dto';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { populateLogByAssessment, PopulateLogWith, SetLogTag } from 'src/service/log/logging';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { Assessment } from '../lib/entities/assessment.entity';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { AssessmentsService } from './assessments.service';
import { CreateAssessmentDto } from './dto/create-assessment.dto';
import { QuerySurveyCourseListDto } from './dto/query-assessment-course-list.dto';
import { QuerySurveyListDto } from './dto/query-assessment-list.dto';
import { QueryAssessmentUserInputDto } from './dto/query-assessment-user-input.dto';
import { QueryAssessmentVersionListDto } from './dto/query-assessment-version-list.dto';
import { QueryAssessmentVersionWebinarListDto } from './dto/query-assessment-version-webinar-list.dto';
import { QuerySurveyWebinarListDto } from './dto/query-assessment-webinar-list.dto';
import { QueryAssessmentDto } from './dto/query-assessment.dto';
import { UpdateAssessmentDto } from './dto/update-assessment.dto';
import { Readable } from 'stream';
import { Permissions } from 'src/permissions.decorator';
import { CRUD } from 'src/lib/interfaces/CRUD';

@ApiTags('assessments')
@ApiBearerAuth()
@Controller()
export class AssessmentsController implements CRUD, Version {
  constructor(private readonly assessmentsService: AssessmentsService) {}

  @Post('assessments')
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new assessment' })
  @ApiResponse({
    status: 201,
    description: 'Created assessment',
    type: Assessment,
  })
  @PgConstraints()
  create(@Body() createAssessmentDto: CreateAssessmentDto) {
    return this.assessmentsService.create(createAssessmentDto);
  }

  @Get('assessments')
  @RequirePermissions(Permission.COURSE_VIEW)
  @ApiOperation({ summary: 'List all assessments' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'moduleId',
    description: 'Filter by module',
    required: false,
  })
  @ApiQuery({
    name: 'globalSurvey',
    enum: ['true', 'false'],
    description: 'Filter by globalSurvey',
    required: false,
  })
  @ApiQuery({
    name: 'webinarSurvey',
    enum: ['true', 'false'],
    description: 'Filter by webinarSurvey',
    required: false,
  })
  @ApiResponse({
    status: 200,
    type: [Assessment],
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  findAll(@Query() query: QueryAssessmentDto) {
    return this.assessmentsService.findAll(query);
  }

  @Get('assessments/:id')
  @RequirePermissions(Permission.COURSE_VIEW, Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'Get one assessment by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a assessment by id',
    type: Assessment,
  })
  @PopulateLogWith(async (log, context) => {
    const assessmentId = log.url.slice(1).split('/')[1];
    return populateLogByAssessment(assessmentId, context);
  })
  @SetLogTag('AssessmentsController.findOne')
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Permissions() perms: Permission[]) {
    const res = await this.assessmentsService.findOne(id);
    if (!perms.includes(Permission.COURSE_VIEW) && !res.isWebinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_VIEW_PERMISSION');
    }
    if (!perms.includes(Permission.WEBINAR_SURVEY_VIEW) && res.isWebinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_SURVEY_VIEW_PERMISSION');
    }
    return res;
  }

  @Patch('assessments/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a assessment' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateAssessmentDto: UpdateAssessmentDto) {
    return this.assessmentsService.update(id, updateAssessmentDto);
  }

  @Delete('assessments/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Deletes a assessment' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.assessmentsService.remove(id);
  }

  @Transactional()
  @Post('assessments/versionUpdate')
  @RequirePermissions(Permission.COURSE_EDIT, Permission.WEBINAR_SURVEY_MANAGE)
  @ApiOperation({ summary: 'Create/Update global assessment' })
  @ApiResponse({
    status: 201,
    description: 'Created/updated assessment',
    type: Assessment,
  })
  @PgConstraints()
  versionUpdate(@Body() dto: VersionUpdateDTO, @Permissions() perms: Permission[]) {
    if (!dto.create && !dto.update && !dto.update) return {};
    if (!perms.includes(Permission.COURSE_EDIT)) {
      // Webinar survey create / update
      ['create', 'delete', 'update'].forEach(key => {
        if (dto[key] && dto[key].filter(o => !o.assessment?.isWebinarSurvey).length) {
          throw new UnauthorizedException('ERRORS.MUST_HAVE_COURSE_EDIT_PERMISSION');
        }
      });
    }
    return this.assessmentsService.versionUpdate(dto);
  }

  @Get('surveys/:id/report')
  @RequirePermissions(Permission.SURVEY_VIEW)
  @ApiOperation({ summary: 'Get result report of survey' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Filter by lecture',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'report',
    type: Assessment,
  })
  @PgConstraints()
  surveyReport(@Param('id', new ParseUUIDPipe()) id: string, @Query() query: QueryAssessmentVersionListDto) {
    return this.assessmentsService.surveyReport(id, query);
  }

  @Get('surveys/:id/report-file')
  @RequirePermissions(Permission.SURVEY_VIEW)
  @ApiOperation({ summary: 'Get result report of survey' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Filter by lecture',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'report',
    type: Assessment,
  })
  @PgConstraints()
  async surveyReportFile(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query() query: QueryAssessmentVersionListDto,
    @Res() response: Response,
  ) {
    const file = await this.assessmentsService.surveyReportFile(id, query);
    if (file) {
      response.setHeader('Content-type', `text/csv`);
      response.attachment(`attachment;filename=${id}-${query.courseId}-${query.lectureId}.csv`);
      const stream = new Readable();
      stream.push(file, 'utf-8');
      stream.push(null);
      stream.pipe(response);
    }
  }

  @Get('surveys/:id/webinar-report')
  @RequirePermissions(Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'Get result report of survey' })
  @ApiQuery({
    name: 'webinarId',
    description: 'Filter by webinarId',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'report',
    type: Assessment,
  })
  @PgConstraints()
  surveyWebinarReport(@Param('id', new ParseUUIDPipe()) id: string, @Query() query: QueryAssessmentVersionWebinarListDto) {
    return this.assessmentsService.surveyWebinarReport(id, query);
  }

  @Get('surveys/:id/webinar-report-file')
  @RequirePermissions(Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'Get result report of survey' })
  @ApiQuery({
    name: 'webinarId',
    description: 'Filter by webinarId',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'file',
  })
  @PgConstraints()
  async surveyWebinarReportFile(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query() query: QueryAssessmentVersionWebinarListDto,
    @Res() response: Response,
  ) {
    const file = await this.assessmentsService.surveyWebinarReportFile(id, query);
    if (file) {
      response.setHeader('Content-type', `text/csv`);
      response.attachment(`attachment;filename=${id}-${query.webinarId}.csv`);
      const stream = new Readable();
      stream.push(file, 'utf-8');
      stream.push(null);
      stream.pipe(response);
    }
    return;
  }

  @Get('surveys/:id/user-input')
  @RequirePermissions(Permission.SURVEY_VIEW)
  @ApiOperation({ summary: 'Get user input report' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: true,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Filter by lecture',
    required: true,
  })
  @ApiQuery({
    name: 'questionId',
    description: 'Filter by questionId',
    required: true,
  })
  @ApiQuery({
    name: 'answerId',
    description: 'Filter by answerId',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: '',
    type: Assessment,
  })
  @PgConstraints()
  surveyReportUserInput(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Pagination() paging: PaginationDto,
    @Query() query: QueryAssessmentUserInputDto,
  ) {
    return this.assessmentsService.surveyReportUserInput(id, query, paging);
  }

  // @Get('surveys/:assessmentId/result')
  // @RequirePermissions(Permission.SURVEY_VIEW)
  // surveyResult(
  //   @Param('assessmentId', new ParseUUIDPipe()) assessmentId: string,
  //   @Query() query: QueryAssessmentUserInputDto,
  // ) {
  //   return 'survey result';
  // }

  @Get('surveys')
  @RequirePermissions(Permission.SURVEY_VIEW)
  @ApiOperation({ summary: 'List surveys for datatable' })
  @ApiPaginatedResponse(Assessment)
  findAllSurveyList(@Pagination() paging: PaginationDto, @Query() query: QuerySurveyCourseListDto) {
    return this.assessmentsService.findAllSurveyList(query, paging);
  }

  @Get('surveys-webinar')
  @RequirePermissions(Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'List webinar surveys for datatable' })
  @ApiPaginatedResponse(Assessment)
  findAllWebinarSurveyList(@Pagination() paging: PaginationDto, @Query() query: QuerySurveyWebinarListDto) {
    return this.assessmentsService.findAllWebinarSurveyList(query, paging);
  }

  @Get('surveys/list')
  @RequirePermissions(Permission.SURVEY_VIEW, Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'List assessments for datatable' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'moduleId',
    description: 'Filter by module',
    required: false,
  })
  @ApiQuery({
    name: 'globalSurvey',
    enum: ['true', 'false'],
    description: 'Filter by globalSurvey',
    required: false,
  })
  @ApiQuery({
    name: 'webinarSurvey',
    enum: ['true', 'false'],
    description: 'Filter by webinarSurvey',
    required: false,
  })
  @ApiQuery({
    name: 'questionCount',
    enum: ['true', 'false'],
    description: 'add question count to result',
    required: false,
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  findAll2(@Pagination() paging: PaginationDto, @Query() query: QuerySurveyListDto, @Permissions() perms: Permission[]) {
    if (!perms.includes(Permission.WEBINAR_SURVEY_VIEW) && query.webinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_SURVEY_PERMISSIONS');
    }
    if (!perms.includes(Permission.SURVEY_VIEW) && !query.webinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_SURVEY_PERMISSIONS');
    }
    return this.assessmentsService.getList(query, paging);
  }

  @Get('surveys/:id/versions')
  @RequirePermissions(Permission.SURVEY_VIEW, Permission.WEBINAR_SURVEY_VIEW)
  @ApiOperation({ summary: 'List assessments version' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Filter by lecture',
    required: false,
  })
  @ApiResponse({
    status: 200,
    type: [Assessment],
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  async findVersions(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query() query: QueryAssessmentVersionListDto,
    @Permissions() perms: Permission[],
  ) {
    const res = await this.assessmentsService.findVersions(id, query);
    const isWebinarSurvey = res[0]?.isWebinarSurvey === true;
    if (!perms.includes(Permission.SURVEY_VIEW) && !isWebinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_COURSE_SURVEY_VIEW_PERMISSION');
    }
    if (!perms.includes(Permission.WEBINAR_SURVEY_VIEW) && isWebinarSurvey) {
      throw new UnauthorizedException('ERRORS.REQUIRE_WEBINAR_SURVEY_VIEW_PERMISSION');
    }
    return res;
  }
}
