import { Test, TestingModule } from '@nestjs/testing';
import { SubmissionsService, TutorsLoad } from './submissions.service';
import { Answer } from '@entities/answer.entity';
import { Assessment } from '@entities/assessment.entity';
import { DistributeLog } from '@entities/distribute-log.entity';
import { Role } from '@entities/role.entity';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { Submission } from '@entities/submission.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AssessmentLevelsService } from 'src/directory/service-level/assessment-levels.service';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { UsersService } from 'src/users/users.service';
import { DateService } from 'src/utils/date.service';
import { EntityManager } from 'typeorm';
import { AssessmentsService } from './assessments.service';
import { QuizService } from './quiz.service';
import { ModulesService } from 'src/courses/modules.service';
import { v4 as uuid } from 'uuid';

const tutorLoadTemplate = (): TutorsLoad => ({
  id: uuid(),
  name: 'Tutor',
  pause: false,
  courses: [],
  moduleAssignments: [],
  blacklistedStudents: [],
  counter: 0,
  submissions: [],
});

describe('SubmissionsService.getMostAvailableTutor test suite', () => {
  let submissionsSevice: SubmissionsService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SubmissionsService,
        {
          provide: ModulesService,
          useValue: {},
        },
        {
          provide: EntityManager,
          useValue: {},
        },
        {
          provide: UsersService,
          useValue: {},
        },
        {
          provide: QuizService,
          useValue: {},
        },
        {
          provide: AssessmentsService,
          useValue: {},
        },
        {
          provide: AssessmentLevelsService,
          useValue: {},
        },
        {
          provide: SocketsGateway,
          useValue: {},
        },
        {
          provide: DateService,
          useValue: {},
        },
        {
          provide: getRepositoryToken(Submission),
          useValue: {},
        },
        {
          provide: getRepositoryToken(DistributeLog),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Role),
          useValue: {},
        },
        {
          provide: getRepositoryToken(SubmissionTariff),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Answer),
          useValue: {},
        },
        {
          provide: getRepositoryToken(Assessment),
          useValue: {},
        },
      ],
    }).compile();

    submissionsSevice = module.get<SubmissionsService>(SubmissionsService);
  });

  it('SubmissionsService should be defined', () => {
    expect(submissionsSevice).toBeDefined();
  });

  it('getMostAvailableTutor should be defined', () => {
    expect(submissionsSevice.getMostAvailableTutor).toBeDefined();
  });

  it('call with empty tutorLoads, expect null', async () => {
    const TUTORS: TutorsLoad[] = [];
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const EXPECTED = null;

    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
  });

  it('tutor has exclusive moduleAssignment, expect tutors with moduleAssignment for this module (exclusive or not exclusive)', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        courses: [COURSE_ID, uuid()],
        moduleAssignments: [
          {
            moduleId: MODULE_ID,
            courseId: COURSE_ID,
            exclusive: true,
          },
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: false,
          },
        ],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        courses: [COURSE_ID, uuid()],
        moduleAssignments: [
          {
            moduleId: MODULE_ID,
            courseId: COURSE_ID,
            exclusive: false,
          },
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: true,
          },
        ],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        courses: [COURSE_ID, uuid()],
        moduleAssignments: [
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: false,
          },
        ],
      },
    ];
    const EXPECTED = [TUTORS[0], TUTORS[1]];

    expect(EXPECTED).toContain(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID));
  });

  it('call with all tutors paused, expect null', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        pause: true,
        courses: [COURSE_ID, uuid()],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        pause: true,
        courses: [COURSE_ID, uuid()],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        pause: true,
        courses: [COURSE_ID, uuid()],
      },
    ];
    const EXPECTED = null;
    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
  });

  it('call with tutors who did not have exclusive moduleAssignment for current module, expect tutors with course enrollment', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        courses: [COURSE_ID, uuid()],
        moduleAssignments: [
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: false,
          },
        ],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        courses: [uuid()],
        moduleAssignments: [
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: true,
          },
        ],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        courses: [COURSE_ID, uuid()],
        moduleAssignments: [
          {
            moduleId: uuid(),
            courseId: uuid(),
            exclusive: false,
          },
        ],
      },
    ];
    const EXPECTED = [TUTORS[0], TUTORS[2]];

    expect(EXPECTED).toContain(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID));
  });

  it('test counter, expect tutor with minimum counter', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        courses: [COURSE_ID, uuid()],
        counter: 3,
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        courses: [COURSE_ID, uuid()],
        counter: 0,
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        courses: [COURSE_ID, uuid()],
        counter: 3,
      },
    ];
    const EXPECTED = TUTORS[1];

    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
  });

  it('call with all tutors has submission author id in blacklist, expect null', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [SUBMISSION_AUTHOR_ID],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [SUBMISSION_AUTHOR_ID],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [SUBMISSION_AUTHOR_ID],
      },
    ];
    const EXPECTED = null;

    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
  });

  it('call with some tutors has submission author id in blacklist, expect tutor without entry in blacklist with current submission author id', async () => {
    const COURSE_ID = uuid();
    const MODULE_ID = uuid();
    const SUBMISSION_AUTHOR_ID = uuid();
    const TUTORS: TutorsLoad[] = [
      {
        ...tutorLoadTemplate(),
        name: 'Tutor1',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [SUBMISSION_AUTHOR_ID, uuid()],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor2',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [SUBMISSION_AUTHOR_ID],
      },
      {
        ...tutorLoadTemplate(),
        name: 'Tutor3',
        courses: [COURSE_ID, uuid()],
        blacklistedStudents: [uuid()],
      },
    ];
    const EXPECTED = TUTORS[2];

    expect(submissionsSevice.getMostAvailableTutor(TUTORS, COURSE_ID, MODULE_ID, SUBMISSION_AUTHOR_ID)).toEqual(EXPECTED);
  });
});
