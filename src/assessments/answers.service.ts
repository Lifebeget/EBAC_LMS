import { AssessmentType, QuestionType, SubmissionStatus } from '@lib/types/enums';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FilesService } from 'src/files/files.service';
import { FindConditions, IsNull, Repository } from 'typeorm';
import { Answer } from '../lib/entities/answer.entity';
import { CreateAnswerDto } from './dto/create-answer.dto';
import { TutorUpdateAnswerDto } from './dto/tutor-update-answer.dto';
import { UpdateAnswerDto } from './dto/update-answer.dto';
import { QuerySurveyAnswersDto } from './dto/query-survey-answers.dto';
import { PaginationDto } from '../pagination.dto';
import { textFromHTML } from '@lib/helpers/text-helpers';
import { QuestionsService } from './questions.service';
import { AnswerWithResult, ProfileUserName } from '@lib/api/types';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { SubmissionsService } from './submissions.service';
import { CRUD } from 'src/lib/interfaces/CRUD';

@Injectable()
export class AnswersService implements CRUD {
  constructor(
    @InjectRepository(Answer)
    private answersRepository: Repository<Answer>,
    private readonly filesService: FilesService,
    private readonly questionsService: QuestionsService,
  ) {}

  prepareUserInputData(answers: AnswerWithResult[]) {
    const result = [];
    answers.forEach(answer => {
      if (!Array.isArray(answer.data)) {
        switch (answer.question.type) {
          case QuestionType.CollectStudentName: {
            const userName: ProfileUserName = JSON.parse(answer.data.userInput);
            answer.data.userInput = userName.surname ? `${userName.name} ${userName.surname}` : userName.name;
            result.push(answer);
            break;
          }
          case QuestionType.Upload: {
            let input = '-';
            if (answer.data.userInput) {
              input = textFromHTML(answer.data.userInput);
            }
            answer.data.files = answer.files.map(file => {
              return {
                input,
                filename: file.filename,
                url: file.url,
              };
            });
            result.push(answer);
            break;
          }
          case QuestionType.SingleChoice: {
            if (answer.data.userInput) {
              answer.data.userInput = textFromHTML(answer.data.userInput);
              result.push(answer);
            }
            break;
          }
          default: {
            if (answer.data.userInput) {
              answer.data.userInput = textFromHTML(answer.data.userInput);
              result.push(answer);
            }
          }
        }
      } else {
        if (answer.question.type === QuestionType.MultiChoice) {
          answer.data.forEach(ans => {
            if (ans.userInput) {
              ans.userInput = textFromHTML(ans.userInput);
            }
          });
          result.push(answer);
        }
      }
    });
    return result;
  }

  distinctAnswersText(answers: any) {
    const answersList = {};
    const arrayAnswerList = [];
    const preparedResults = this.prepareUserInputData(answers);
    for (const answer of preparedResults) {
      if (!Array.isArray(answer.data)) {
        const value = answer.data?.files ? answer.data?.files : answer.data?.userInput;
        if (Array.isArray(value)) {
          arrayAnswerList.push(value);
        } else {
          answersList[value] = (answersList[value] || 0) + 1;
        }
      } else {
        arrayAnswerList.push(answer.data);
      }
    }
    const results = Object.entries(answersList).map(([value, count]: [string, number]) => {
      return { value, count };
    });
    arrayAnswerList.forEach(answer => {
      results.push({
        value: answer,
        count: answer.length,
      });
    });
    results.sort((a, b) => b.count - a.count);
    return results;
  }

  surveyAnswersQueryBuilder(query: QuerySurveyAnswersDto) {
    const selects = ['answer.id', 'question.type', 'answer.data'];
    let queryBuilder = this.answersRepository
      .createQueryBuilder('answer')
      .leftJoin('answer.question', 'question')
      .select(selects)
      .leftJoin('answer.submission', 'submission')
      .leftJoinAndSelect('answer.files', 'files')
      .where('answer.data IS NOT NULL')
      .andWhere('answer.questionId = :questionId', {
        questionId: query.questionId,
      });

    if (query.webinarId) {
      queryBuilder = queryBuilder.andWhere('submission.webinarId = :webinarId', { webinarId: query.webinarId });
    }
    if (query.lectureId) {
      queryBuilder = queryBuilder.andWhere('submission.lectureId = :lectureId', { lectureId: query.lectureId });
    }
    if (query.courseId) {
      queryBuilder = queryBuilder.leftJoin('submission.course', 'course').andWhere('course.id = :courseId', { courseId: query.courseId });
    }
    if (query.userInputOnly) {
      queryBuilder = queryBuilder.andWhere("answer.data->>'isUserInput' = 'true'");
    }
    return queryBuilder;
  }

  async surveyRateAnswers(query: QuerySurveyAnswersDto) {
    const queryBuilder = this.surveyAnswersQueryBuilder(query);
    queryBuilder.andWhere('question.type = :questionType', {
      questionType: QuestionType.Rate,
    });

    const usersAnswers = await queryBuilder.getMany();
    const question = await this.questionsService.surveyRateQuestionResults(query);
    const questionDataAnswers = question.data?.answers;

    return questionDataAnswers.map(q => {
      const answersCount = usersAnswers.filter(a => {
        if (!Array.isArray(a.data)) {
          return a.data?.id === q.id;
        }
      }).length;

      const divider = usersAnswers.length || 1;
      const percent = (answersCount / divider) * 100;

      return {
        value: q.value,
        title: q.title,
        answersCount,
        percent: percent === 0 ? percent : +percent.toFixed(1),
      };
    });
  }

  async surveyAnswers(query: QuerySurveyAnswersDto, paging?: PaginationDto) {
    const queryBuilder = this.surveyAnswersQueryBuilder(query);
    const totalPromise = queryBuilder.getMany();
    const answersPromise = queryBuilder
      .skip((paging.page - 1) * paging.itemsPerPage)
      .take(paging.itemsPerPage)
      .getMany();

    const [results, total] = await Promise.all([answersPromise, totalPromise]);
    return {
      results: this.distinctAnswersText(results),
      meta: {
        ...paging,
        total: this.distinctAnswersText(total).length,
      },
    };
  }

  async create(createAnswerDto: CreateAnswerDto) {
    if (createAnswerDto.tutorComment) {
      createAnswerDto.tutorComment = await this.filesService.replaceAndUploadBase64ImgS3(createAnswerDto.tutorComment);
    }
    if (createAnswerDto.comment) {
      createAnswerDto.comment = await this.filesService.replaceAndUploadBase64ImgS3(createAnswerDto.comment);
    }
    return this.answersRepository.save({
      ...createAnswerDto,
      submission: { id: createAnswerDto.submissionId },
      question: { id: createAnswerDto.questionId },
    });
  }

  findForEadbox() {
    return this.answersRepository.find({
      relations: ['submission', 'submission.user', 'submission.assessment', 'submission.lecture', 'submission.course'],
      where: {
        submission: {
          status: SubmissionStatus.Graded,
          assessment: {
            type: AssessmentType.Default,
          },
        },
        tutorComment: IsNull(),
      },
    });
  }

  findForSubmission(submissionId: string): Promise<Answer[]> {
    return this.answersRepository
      .createQueryBuilder('answer')
      .leftJoinAndSelect('answer.question', 'question')
      .leftJoinAndSelect('answer.files', 'files')
      .leftJoinAndSelect('answer.tutorFiles', 'tutorFiles')
      .where('submission_id = :submissionId', { submissionId })
      .getMany();
  }

  @Transactional()
  findOne(id: string): Promise<Answer> {
    return this.answersRepository.findOne(id, {
      relations: [
        'submission',
        'submission.tutor',
        'submission.assessment',
        'submission.tariff',
        'submission.tariff.createTariff',
        'question',
        'files',
        'tutorFiles',
        'assessmentCriteriaEntries',
      ],
    });
  }

  find(where: FindConditions<Answer>, relations = null): Promise<Answer[]> {
    return this.answersRepository.find({
      where,
      relations: relations || ['submission', 'submission.tutor', 'question', 'files', 'tutorFiles'],
    });
  }

  async update(id: string, updateAnswerDto: UpdateAnswerDto | TutorUpdateAnswerDto) {
    const updateFields = { ...updateAnswerDto, id };
    if (updateAnswerDto.tutorComment) {
      updateFields.tutorComment = await this.filesService.replaceAndUploadBase64ImgS3(updateFields.tutorComment);
    }

    return this.answersRepository.save(updateFields);
  }

  remove(id: string) {
    return this.answersRepository.delete(id);
  }

  async clear() {
    await this.answersRepository.clear();
  }

  purgeAll() {
    return this.answersRepository.clear();
  }

  findAllAnswersFiles2Upload() {
    return this.answersRepository
      .createQueryBuilder('answer')
      .leftJoinAndSelect('answer.files', 'files')
      .where('files.eadboxUrl is NULL AND files.id is not NULL')
      .select('files')
      .getRawMany();
  }

  findAllWithBase64Img() {
    return this.answersRepository
      .createQueryBuilder('answer')
      .where('answer.comment like :b64 OR answer.tutorComment like :b64', {
        b64: `%src="data:%`,
      })
      .select(['answer.id', 'answer.comment', 'answer.tutorComment'])
      .getMany();
  }
}
