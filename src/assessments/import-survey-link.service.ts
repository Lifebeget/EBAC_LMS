import { ImportSurveyLink } from '@entities/import-survey-link.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ImportSurveyLinkService {
  constructor(
    @InjectRepository(ImportSurveyLink)
    public importSurveyLinkRepository: Repository<ImportSurveyLink>,
  ) {}
}
