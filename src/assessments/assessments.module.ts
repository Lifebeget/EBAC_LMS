import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssessmentsService } from './assessments.service';
import { Assessment } from '../lib/entities/assessment.entity';
import { QuestionsService } from './questions.service';
import { QuestionsController } from './questions.controller';
import { Question } from '../lib/entities/question.entity';
import { SubmissionsController } from './submissions.controller';
import { AnswersController } from './answers.controller';
import { SubmissionsService } from './submissions.service';
import { AnswersService } from './answers.service';
import { Submission } from '../lib/entities/submission.entity';
import { Answer } from '../lib/entities/answer.entity';
import { SocketsModule } from 'src/sockets/sockets.module';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { FilesModule } from 'src/files/files.module';
import { QuizService } from './quiz.service';
import { DistributeLog } from '@entities/distribute-log.entity';
import { Role } from '@entities/role.entity';
import { Content } from '@entities/content.entity';
import { ImportSurveyLinkService } from './import-survey-link.service';
import { ImportSurveyLink } from '@entities/import-survey-link.entity';
import { Trigger } from '@entities/trigger.entity';
import { CoursesModule } from 'src/courses/courses.module';
import { UsersModule } from 'src/users/users.module';
import { SubmissionTariff } from '@entities/submission-tariff.entity';
import { ViewAssessment } from 'src/lib/views/view-assessment.entity';
import { DirectoryModule } from 'src/directory/directory.module';
import { AssessmentsController } from './assessments.controller';
import { WebinarModule } from 'src/webinar/webinar.module';
import { CriteriaBasedAssessmentsModule } from '../criteria-based-assessments/criteria-based-assessments.module';
import { EnrollmentsHelper } from '@utils/enrollments.helper';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { Attempt } from './../lib/entities/attempt.entity';
import { AttemptsService } from './attempts.service';
import { AttemptsController } from './attempts.controller';
import { FeatureFlagsModule } from 'src/feature-flags/feature-flags.module';
import { StoreModule } from 'src/store/store.module';
import { TriggerModule } from 'src/trigger/trigger.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Assessment]),
    TypeOrmModule.forFeature([Question]),
    TypeOrmModule.forFeature([Submission]),
    TypeOrmModule.forFeature([SubmissionTariff]),
    TypeOrmModule.forFeature([Answer]),
    TypeOrmModule.forFeature([DistributeLog]),
    TypeOrmModule.forFeature([Role]),
    TypeOrmModule.forFeature([Content]),
    TypeOrmModule.forFeature([ImportSurveyLink]),
    TypeOrmModule.forFeature([Trigger]),
    TypeOrmModule.forFeature([ViewAssessment]),
    TypeOrmModule.forFeature([AssessmentCriterion]),
    TypeOrmModule.forFeature([Attempt]),
    forwardRef(() => UsersModule),
    SocketsModule,
    NotificationsModule,
    FilesModule,
    forwardRef(() => CoursesModule),
    DirectoryModule,
    WebinarModule,
    forwardRef(() => CriteriaBasedAssessmentsModule),
    forwardRef(() => FeatureFlagsModule),
    forwardRef(() => TriggerModule),
    StoreModule,
  ],
  controllers: [AssessmentsController, QuestionsController, SubmissionsController, AnswersController, AttemptsController],
  providers: [
    AssessmentsService,
    QuestionsService,
    SubmissionsService,
    AnswersService,
    QuizService,
    ImportSurveyLinkService,
    EnrollmentsHelper,
    AttemptsService,
  ],
  exports: [
    QuizService,
    QuestionsService,
    SubmissionsService,
    AnswersService,
    TypeOrmModule,
    AssessmentsService,
    DirectoryModule,
    AttemptsService,
  ],
})
export class AssessmentsModule {}
