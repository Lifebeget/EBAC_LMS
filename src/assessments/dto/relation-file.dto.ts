import { ApiProperty } from '@nestjs/swagger';

export class RelationFileDto {
  @ApiProperty({ description: 'File ID' })
  id: string;
}
