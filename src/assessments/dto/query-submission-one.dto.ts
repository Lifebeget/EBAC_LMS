import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsOptional } from 'class-validator';

export class QuerySubmissionOneDto {
  @ApiProperty({
    description: 'Get obly default submissions(not quiz, survey)',
    required: false,
  })
  withQuestions?: boolean;

  @ApiProperty({
    description: 'Whether to show criteria entries',
    required: false,
  })
  @IsBooleanString()
  @IsOptional()
  showCriteriaEntries?: string;
}
