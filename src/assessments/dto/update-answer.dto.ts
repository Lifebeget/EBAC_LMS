import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsOptional, IsUUID, ValidateIf } from 'class-validator';

import { CreateAnswerDto } from './create-answer.dto';

export class UpdateAnswerDto extends PartialType(OmitType(CreateAnswerDto, ['questionId', 'submissionId'] as const)) {
  @ApiProperty({ description: 'New service level id' })
  @IsOptional()
  @ValidateIf(o => o.hasOwnProperty('serviceLevelId'))
  @IsUUID()
  serviceLevelId?: string;
}
