import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class HaveAttemptsDto {
  @ApiProperty({ description: 'User ID' })
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'Assessment ID' })
  @IsUUID()
  assessmentId: string;
}
