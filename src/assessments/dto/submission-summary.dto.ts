import { ApiProperty } from '@nestjs/swagger';

export class SubmissionSummaryDto {
  @ApiProperty({ description: 'Count of all submission' })
  allCount: number;

  @ApiProperty({ description: 'Count of urgent submission' })
  urgentCount: number;

  @ApiProperty({ description: 'Count of outdated submission' })
  outdatedCount: number;

  @ApiProperty({ description: 'Count of submission graded today' })
  todayCount: number;
}
