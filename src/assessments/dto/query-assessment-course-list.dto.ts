import { Transform } from 'class-transformer';
import { IsBoolean, IsString, IsUUID, ValidateIf } from 'class-validator';
import { QuerySorted } from './query-sorted';

export class QuerySurveyCourseListDto extends QuerySorted {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  course?: string;

  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  lecture?: string;

  @ValidateIf(o => o.hasOwnProperty('globalSurvey'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  globalSurvey?: boolean;

  @ValidateIf(o => o.hasOwnProperty('webinarSurvey'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  webinarSurvey?: boolean;

  @ValidateIf(o => o.hasOwnProperty('survey'))
  @IsString()
  survey?: string;

  @ValidateIf(o => o.hasOwnProperty('questionCount'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  questionCount?: boolean;
}
