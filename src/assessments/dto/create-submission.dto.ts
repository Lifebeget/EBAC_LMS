import { SubmissionStatus } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsUUID, Max, Min, ValidateIf } from 'class-validator';
import { Answer } from '../../lib/entities/answer.entity';
import { ImportData } from '../../lib/entities/submission.entity';

export class CreateSubmissionDto {
  @ApiProperty({ description: 'User ID' })
  @IsUUID()
  userId?: string;

  @ApiProperty({ description: 'Question ID' })
  @IsUUID()
  assessmentId: string;

  @ApiProperty({ description: 'Lecture ID' })
  @IsUUID()
  lectureId: string;

  @ApiProperty({ description: 'Module ID' })
  @IsUUID()
  moduleId: string;

  @ApiProperty({ description: 'Course ID' })
  @IsUUID()
  courseId: string;

  @ValidateIf(o => o.hasOwnProperty('isQuestion'))
  @IsBoolean()
  @ApiProperty({ description: 'Is it just a question' })
  isQuestion?: boolean;

  @IsOptional()
  @ApiProperty({
    description: 'Status / Stage of the submission',
    enum: SubmissionStatus,
    default: SubmissionStatus.New,
  })
  status?: SubmissionStatus;

  @ApiProperty({ description: 'Answers', type: () => [Answer] })
  answers: Answer[];

  @ApiProperty({ description: 'Submission comment text' })
  comment?: string;

  @ValidateIf(o => o.hasOwnProperty('score'))
  @IsInt()
  @Min(0)
  @Max(100)
  @ApiProperty({ description: 'Grade of the submission' })
  score?: number;

  @ApiProperty({ description: 'Submission due date' })
  due?: Date;

  @ApiProperty({ default: 0, description: 'Attempt number. Starts with 0' })
  attempt?: number;

  @ApiProperty({
    default: true,
    description: 'If many attempts on one assignment, only one of them should be true',
  })
  current_attempt?: boolean;

  @ApiProperty({
    default: false,
    description: 'Submission should be checked by specific tutor',
  })
  bindTutor?: boolean;

  @ApiProperty({ description: 'Date of last draft save' })
  draftAt?: Date;

  @ApiProperty({ description: 'Date of submission' })
  submittedAt?: Date;

  @IsOptional()
  @ApiProperty({ description: 'Date of grading' })
  gradedAt?: Date;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'Additional import data', required: false })
  importData?: ImportData;

  @ValidateIf(o => o.hasOwnProperty('success'))
  @ApiProperty({
    default: null,
    description: 'Submission state',
  })
  success?: boolean;
}
