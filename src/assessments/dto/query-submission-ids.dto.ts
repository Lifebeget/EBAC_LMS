import { Ids } from '@lib/types/Ids';
import { ApiProperty } from '@nestjs/swagger';
export class QuerySubmissionIds implements Ids {
  @ApiProperty({ description: 'Submission identifiers to load' })
  ids: string[];

  @ApiProperty({
    description: 'Text to search',
    required: false,
  })
  searchString?: string;
}
