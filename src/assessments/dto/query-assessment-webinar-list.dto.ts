import { IsString, ValidateIf } from 'class-validator';
import { QuerySorted } from './query-sorted';

export class QuerySurveyWebinarListDto extends QuerySorted {
  @ValidateIf(o => o.hasOwnProperty('webinar'))
  @IsString()
  webinar?: string;

  @ValidateIf(o => o.hasOwnProperty('survey'))
  @IsString()
  survey?: string;
}
