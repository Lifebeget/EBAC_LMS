import { IsOptional, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QuerySurveyAnswersDto {
  @ApiProperty({ description: 'Question ID' })
  @IsUUID()
  questionId: string;

  @ApiProperty({ description: 'Webinar ID' })
  @IsUUID()
  @IsOptional()
  webinarId?: string;

  @ApiProperty({ description: 'Lecture ID' })
  @IsUUID()
  @IsOptional()
  lectureId?: string;

  @ApiProperty({ description: 'Course ID' })
  @IsUUID()
  @IsOptional()
  courseId?: string;

  @ApiProperty({
    description: 'Get only User input answer',
    required: false,
  })
  @IsOptional()
  userInputOnly?: boolean;
}
