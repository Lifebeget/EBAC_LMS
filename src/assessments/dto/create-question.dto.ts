import { MultiChoiceAnswerI, QuestionType, RateAnswerI, SingleChoiceAnswerI, TextMode, TrueFalseAnswerI } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsInt, IsNotEmpty, IsOptional, ValidateIf, ValidateNested } from 'class-validator';
import { RelationFileDto } from './relation-file.dto';
import { Type } from 'class-transformer';
import { CreateLectureContentCbaDto } from '../../criteria-based-assessments/dto/create-lecture-content-cba.dto';

export class CreateQuestionDto {
  assessmentId?: string;

  @ApiProperty({ description: 'Question title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Question text' })
  description?: string;

  @ValidateIf(o => o.hasOwnProperty('score'))
  @IsInt()
  @ApiProperty({ description: 'Maximum score', default: 100 })
  score?: number;

  @ValidateIf(o => o.hasOwnProperty('passingScore'))
  @IsInt()
  @ApiProperty({ description: 'Score to pass', default: 70 })
  passingScore?: number;

  @ApiProperty({
    description: "Does this question's grade get into grading book",
    enum: ['true', 'false'],
    default: true,
  })
  gradable?: boolean;

  @ApiProperty({
    description: 'Question type',
    enum: QuestionType,
    default: QuestionType.Upload,
  })
  type: QuestionType;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({
    description: 'Question files',
    required: false,
    type: () => [RelationFileDto],
  })
  files?: RelationFileDto[];

  @ApiProperty({ description: 'config answers', required: false, type: Object })
  data?: {
    answers: SingleChoiceAnswerI[] | MultiChoiceAnswerI[] | TrueFalseAnswerI[] | RateAnswerI[];
  };

  @ValidateIf(o => o.hasOwnProperty('sort'))
  @IsInt()
  @ApiProperty({ description: 'sort', default: 0 })
  sort?: number;

  @ValidateIf(o => o.hasOwnProperty('textMode'))
  @ApiProperty({ enum: TextMode, default: TextMode.OneLine })
  textMode?: TextMode;

  @ValidateIf(o => o.hasOwnProperty('textModeMin'))
  @IsInt()
  @ApiProperty({ description: 'Min text', default: 0 })
  textModeMin?: number;

  @ValidateIf(o => o.hasOwnProperty('textModeMax'))
  @IsInt()
  @ApiProperty({ description: 'Max text', default: 0 })
  textModeMax?: number;

  @ApiProperty({
    description: 'isRequired',
    default: true,
  })
  @IsBoolean()
  @IsOptional()
  isRequired?: boolean;

  @ApiProperty({
    description: 'Assessment criteria list',
    required: false,
    type: () => [CreateLectureContentCbaDto],
  })
  @IsArray()
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateLectureContentCbaDto)
  assessmentCriteria?: CreateLectureContentCbaDto[];
}
