import { PartialType, ApiProperty } from '@nestjs/swagger';
import { CreateSubmissionDto } from './create-submission.dto';

export class UpdateSubmissionDto extends PartialType(CreateSubmissionDto) {
  @ApiProperty({
    default: {},
    description: 'submission tariff',
  })
  tariff?: any;
}
