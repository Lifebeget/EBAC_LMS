import { SubmissionState } from '@enums/submission-type.enum';
import { SystemRole } from '@lib/api/types';
import { AssessmentType, SubmissionStatus, SubmissionType } from '@lib/types/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsOptional, IsUUID, ValidateIf } from 'class-validator';
import { QuerySorted } from './query-sorted';

export class QuerySubmissionDto extends QuerySorted {
  @ApiProperty({ description: 'Find by id', required: false })
  @ValidateIf(o => o.hasOwnProperty('submissionId'))
  @IsUUID()
  submissionId?: string;

  @ApiProperty({ description: 'Filter by course', required: false })
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ApiProperty({ description: 'Filter by module', required: false })
  @ValidateIf(o => o.hasOwnProperty('moduleId'))
  @IsUUID()
  moduleId?: string;

  @ApiProperty({ description: 'Filter by lecture', required: false })
  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ApiProperty({ description: 'Filter by assesment', required: false })
  @ValidateIf(o => o.hasOwnProperty('assessmentId'))
  @IsUUID()
  assessmentId?: string;

  @ApiProperty({ description: 'Filter by user', required: false })
  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ApiProperty({ description: 'Filter by tutor', required: false })
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId?: string;

  @ApiProperty({ description: 'Filter by teamlead', required: false })
  @ValidateIf(o => o.hasOwnProperty('teamleadId'))
  @IsUUID()
  teamleadId?: string;

  @ApiProperty({ description: 'Filter by status', required: false })
  @ValidateIf(o => o.hasOwnProperty('status'))
  status?: SubmissionStatus;

  @ApiProperty({ description: 'Filter by status list', required: false })
  statuses?: SubmissionStatus[];

  @ApiProperty({
    description: 'Filter by received period start date',
    required: false,
  })
  submittedStartDate?: Date;

  @ApiProperty({
    description: 'Filter by received period end date',
    required: false,
  })
  submittedEndDate?: Date;

  @ApiProperty({
    description: 'Filter by graded period start date',
    required: false,
  })
  gradedStartDate?: Date;

  @ApiProperty({
    description: 'Filter by grade period end date',
    required: false,
  })
  gradedEndDate?: Date;

  @ApiProperty({ description: 'Filter by users list', required: false })
  userIds?: string[];

  @ApiProperty({
    description: 'Filter by submission type list',
    required: false,
  })
  submissionState?: SubmissionState;

  @ApiProperty({
    description: 'Text to search',
    required: false,
  })
  searchString?: string;

  @ApiProperty({
    description: 'Get submissions for role',
    required: false,
  })
  asRole?: SystemRole;

  @ApiProperty({
    description: 'Get obly default submissions(not quiz, survey)',
    required: false,
  })
  onlyDefault?: boolean;

  @ApiProperty({
    description: 'Assessment Type',
    required: false,
  })
  assessmentType?: AssessmentType[];

  @ApiProperty({ description: 'Find by webinarId', required: false })
  @ValidateIf(o => o.hasOwnProperty('webinarId'))
  @IsUUID()
  webinarId?: string;

  @ApiProperty({
    description: 'Get only/not webinar survey results',
    required: false,
  })
  isWebinarResult?: boolean;

  @ApiProperty({ description: 'Filter by type', required: false })
  @ValidateIf(o => o.hasOwnProperty('type'))
  type?: SubmissionType;

  @ApiProperty({
    description: 'Whether to show criteria entries',
    required: false,
  })
  @IsBooleanString()
  @IsOptional()
  showCriteriaEntries?: string;

  @ApiProperty({
    description: 'Id of service level',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('levelId'))
  @IsUUID()
  @IsOptional()
  levelId?: string;
}
