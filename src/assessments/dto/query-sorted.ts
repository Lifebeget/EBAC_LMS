import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString, ValidateIf } from 'class-validator';

export enum ColumnSortI {
  'ASC' = 'ASC',
  'DESC' = 'DESC',
}

export class QuerySorted {
  @ApiProperty({
    description: 'Sort by field',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('sortField'))
  @IsString()
  sortField?: string;

  @ApiProperty({
    description: 'Sort direction',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('sortDirection'))
  @IsEnum(ColumnSortI)
  sortDirection?: ColumnSortI;
}
