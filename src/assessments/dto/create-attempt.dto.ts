import { ApiProperty, PartialType, OmitType } from '@nestjs/swagger';
import { IsInt, IsUUID } from 'class-validator';

export class CreateAttemptDto {
  @ApiProperty({ description: 'User ID' })
  @IsUUID()
  userId: string;

  @ApiProperty({ description: 'Course ID' })
  @IsUUID()
  courseId: string;

  @ApiProperty({ description: 'Lecture ID' })
  @IsUUID()
  lectureId: string;

  @ApiProperty({ description: 'Assessment ID' })
  @IsUUID()
  assessmentId: string;

  @ApiProperty({ description: 'Additional attempts', default: 0 })
  @IsInt()
  additionalAttempts: number;
}

export class CreateOneAdditionalAttemptDto extends OmitType(CreateAttemptDto, ['additionalAttempts'] as const) {}
