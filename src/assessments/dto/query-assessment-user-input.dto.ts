import { IsUUID, ValidateIf } from 'class-validator';

export class QueryAssessmentUserInputDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ValidateIf(o => o.hasOwnProperty('questionId'))
  @IsUUID()
  questionId?: string;

  @ValidateIf(o => o.hasOwnProperty('answerId'))
  @IsUUID()
  answerId?: string;
}
