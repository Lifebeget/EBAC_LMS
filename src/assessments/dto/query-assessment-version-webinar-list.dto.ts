import { IsUUID, ValidateIf } from 'class-validator';

export class QueryAssessmentVersionWebinarListDto {
  @ValidateIf(o => o.hasOwnProperty('webinarId'))
  @IsUUID()
  webinarId?: string;
}
