import { ApiProperty, PickType } from '@nestjs/swagger';
import { CreateSubmissionDto } from './create-submission.dto';

export class TutorUpdateSubmissionDto extends PickType(CreateSubmissionDto, [
  'status',
  'score',
  'gradedAt',
  'current_attempt',
  'isQuestion',
  'success',
] as const) {
  @ApiProperty({
    default: {},
    description: 'submission tutor',
  })
  tutor?: any;
}
