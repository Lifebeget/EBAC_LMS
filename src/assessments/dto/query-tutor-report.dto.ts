import { SortDirection } from '@enums/sort-direction.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, ValidateIf } from 'class-validator';

export class QueryTutorReportDto {
  @ApiProperty({ description: 'Filter by tutor', required: false })
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId?: string;

  @ApiProperty({ description: 'Start date for filter' })
  periodStart?: Date;

  @ApiProperty({ description: 'End date for filter' })
  periodEnd?: Date;

  @ApiProperty({
    description: 'Grading method',
    default: SortDirection.Desc,
    enum: SortDirection,
  })
  sortDirection?: SortDirection;
}
