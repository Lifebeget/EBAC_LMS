import { IsBoolean, IsInt, IsOptional, IsUUID, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { File } from '../../lib/entities/file.entity';
import { RelationFileDto } from './relation-file.dto';
import { AnswerDataI } from '@lib/api/types';
import { ExcludeBase64FromLog } from 'src/logger';

export class CreateAnswerDto {
  @ValidateIf(o => o.hasOwnProperty('submissionId'))
  @IsUUID()
  @ApiProperty({ description: 'Submission ID' })
  submissionId?: string;

  @IsUUID()
  @ApiProperty({ description: 'Question ID' })
  questionId: string;

  @ApiProperty({ description: 'Student comment for answer or answer text' })
  @ExcludeBase64FromLog()
  comment?: string;

  @IsOptional()
  @ApiProperty({ description: 'Tutor comment for answer' })
  @ExcludeBase64FromLog()
  tutorComment?: string;

  @ValidateIf(o => o.hasOwnProperty('score'))
  @IsInt()
  @ApiProperty({ description: 'Graded score', default: 0 })
  score?: number;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({
    description: 'Files',
    required: false,
    type: () => [RelationFileDto],
  })
  files?: File[];

  @ValidateIf(o => o.hasOwnProperty('tutorFiles'))
  @ApiProperty({
    description: 'Tutor files',
    required: false,
    type: () => [RelationFileDto],
  })
  tutorFiles?: File[];

  @ApiProperty({
    description: 'Answer data quiz/survey',
    required: false,
    type: Object,
  })
  data?: AnswerDataI | AnswerDataI[];

  @ValidateIf(o => o.hasOwnProperty('isQuestion'))
  @IsBoolean()
  @ApiProperty({ description: 'Is it just a question' })
  isQuestion?: boolean;

  @ApiProperty({
    description: 'Kaltura entry id',
    required: false,
    type: String,
  })
  kalturaEntryId?: string;

  @ApiProperty({ description: 'Webinar ID', required: false })
  webinarId?: string;
}
