import { SubmissionState } from '@enums/submission-type.enum';
import { SystemRole } from '@lib/api/types';
import { AssessmentType, SubmissionStatus } from '@lib/types/enums';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, ValidateIf } from 'class-validator';
import { QuerySorted } from './query-sorted';

export class QuerySubmissionListDto extends QuerySorted {
  @ApiProperty({ description: 'Find by id', required: false })
  @ValidateIf(o => o.hasOwnProperty('submissionId'))
  @IsUUID()
  submissionId?: string;

  @ApiProperty({ description: 'Filter by course', required: false })
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ApiProperty({ description: 'Filter by module', required: false })
  @ValidateIf(o => o.hasOwnProperty('moduleId'))
  @IsUUID()
  moduleId?: string;

  @ApiProperty({ description: 'Filter by lecture', required: false })
  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ApiProperty({ description: 'Filter by assesment', required: false })
  @ValidateIf(o => o.hasOwnProperty('assessmentId'))
  @IsUUID()
  assessmentId?: string;

  @ApiProperty({ description: 'Filter by user', required: false })
  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ApiProperty({ description: 'Filter by tutor', required: false })
  @ValidateIf(o => o.hasOwnProperty('tutorId'))
  @IsUUID()
  tutorId?: string;

  @ApiProperty({ description: 'Filter by teamlead', required: false })
  @ValidateIf(o => o.hasOwnProperty('teamleadId'))
  @IsUUID()
  teamleadId?: string;

  @ApiProperty({ description: 'Filter by status', required: false })
  @ValidateIf(o => o.hasOwnProperty('status'))
  status?: SubmissionStatus;

  @ApiProperty({ description: 'Filter by status list', required: false })
  statuses?: SubmissionStatus[];

  @ApiProperty({
    description: 'Filter by received period start date',
    required: false,
  })
  submittedStartDate?: Date;

  @ApiProperty({
    description: 'Filter by received period end date',
    required: false,
  })
  submittedEndDate?: Date;

  @ApiProperty({
    description: 'Filter by graded period start date',
    required: false,
  })
  gradedStartDate?: Date;

  @ApiProperty({
    description: 'Filter by grade period end date',
    required: false,
  })
  gradedEndDate?: Date;

  @ApiProperty({ description: 'Filter by users list', required: false })
  userIds?: string[];

  @ApiProperty({
    description: 'Filter by submission type list',
    required: false,
  })
  submissionState?: SubmissionState;

  @ApiProperty({
    description: 'Text to search',
    required: false,
  })
  searchString?: string;

  @ApiProperty({
    description: 'Get submissions for role',
    required: false,
  })
  asRole?: SystemRole;

  @ApiProperty({
    description: 'Get obly default submissions(not quiz, survey)',
    required: false,
  })
  onlyDefault?: boolean;

  @ApiProperty({
    description: 'Get simple list for datatable',
    required: false,
  })
  assessmentType?: AssessmentType[];
}

export class QuerySubmissionListForAdminPanelDto extends QuerySorted {
  @ApiProperty({ description: 'Find where graded date after outdated date', required: false })
  @ValidateIf(o => o.hasOwnProperty('isGradedAfterOutdated'))
  isGradedAfterOutdated?: boolean;

  @ApiProperty({ description: 'Find by type', required: false })
  @ValidateIf(o => o.hasOwnProperty('isQuestion'))
  isQuestion?: boolean;

  @ApiProperty({ description: 'Filter by courses, sections, modules, lectures', required: false })
  @ValidateIf(o => o.hasOwnProperty('checkedCourseElements'))
  checkedCourseElements?: string[];

  @ApiProperty({ description: 'Filter by tutor', required: false })
  @ValidateIf(o => o.hasOwnProperty('tutors'))
  tutors?: string[];

  @ApiProperty({ description: 'Filter by status list', required: false })
  @ValidateIf(o => o.hasOwnProperty('statuses'))
  statuses?: SubmissionStatus[];

  @ApiProperty({
    description: 'Filter by submit period start date',
    required: false,
  })
  submittedFrom?: Date;

  @ApiProperty({
    description: 'Filter by submit period end date',
    required: false,
  })
  submittedTo?: Date;

  @ApiProperty({
    description: 'Filter by correction period start date',
    required: false,
  })
  gradedFrom?: Date;

  @ApiProperty({
    description: 'Filter by correction period end date',
    required: false,
  })
  gradedTo?: Date;

  @ApiProperty({
    description: 'Filter by time difference by submitted and graded dates',
    required: false,
  })
  correctionOverallTime?: number;

  @ApiProperty({
    description: 'Filter by score (min)',
    required: false,
  })
  scoreMin?: number;

  @ApiProperty({
    description: 'Filter by score (max)',
    required: false,
  })
  scoreMax?: number;

  @ApiProperty({
    description: 'Filter by difficulty',
    required: false,
  })
  levels?: string;

  @ApiProperty({
    description: 'Filter by attempt',
    required: false,
  })
  attempt?: number;
}
