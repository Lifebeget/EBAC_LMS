import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsArray, IsOptional, IsUUID, ValidateIf, ValidateNested } from 'class-validator';

import { CreateAnswerDto } from './create-answer.dto';
import { ParamAssessmentCriterionEntry } from '../../criteria-based-assessments/dto/create-assessment-criterion-entry.dto';
import { Type } from 'class-transformer';

export class TutorUpdateAnswerDto extends PickType(CreateAnswerDto, ['tutorComment', 'score', 'tutorFiles', 'isQuestion'] as const) {
  @ApiProperty({ description: 'CBA entries' })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ParamAssessmentCriterionEntry)
  @IsOptional()
  entries?: ParamAssessmentCriterionEntry[];

  @ApiProperty({ description: 'New service level id' })
  @IsOptional()
  @ValidateIf(o => o.hasOwnProperty('serviceLevelId'))
  @IsUUID()
  serviceLevelId?: string;
}
