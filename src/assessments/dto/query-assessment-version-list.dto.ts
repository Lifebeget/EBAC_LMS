import { IsUUID, ValidateIf } from 'class-validator';

export class QueryAssessmentVersionListDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;
}
