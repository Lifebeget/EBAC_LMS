import { AssessmentGradingType, AssessmentType, ShowCorrectAnswers, ShowResult } from '@lib/api/types';
import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsInt, IsNotEmpty, IsUUID, Min, ValidateIf } from 'class-validator';
import { Question } from 'src/lib/entities/question.entity';

export class CreateAssessmentDto {
  @ApiProperty({ description: 'Lecture ID' })
  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ApiProperty({ description: 'Webinar ID' })
  @ValidateIf(o => o.hasOwnProperty('webinarId'))
  @IsUUID()
  webinarId?: string;

  @ApiProperty({ description: 'Assessment title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Assessment description' })
  description?: string;

  @ValidateIf(o => o.hasOwnProperty('score'))
  @IsInt()
  @ApiProperty({ description: 'Maximum score', default: 100 })
  score?: number;

  @ValidateIf(o => o.hasOwnProperty('passingScore'))
  @IsInt()
  @ApiProperty({ description: 'Score to pass', default: 70 })
  passingScore?: number;

  @ValidateIf(o => o.hasOwnProperty('timeLimitSeconds'))
  @IsInt()
  @ApiProperty({
    description: 'Time limit in seconds. Set 0 for no limit',
    default: 0,
  })
  timeLimitSeconds?: number;

  @ValidateIf(o => o.hasOwnProperty('timeDelaytoRetestHours'))
  @IsInt()
  @ApiProperty({
    description: 'Delay to restart',
    default: 0,
  })
  timeDelaytoRetestHours?: number;

  @ApiProperty({
    description: 'Grading method',
    default: AssessmentGradingType.OverallShouldPass,
    enum: AssessmentGradingType,
  })
  gradingType?: AssessmentGradingType;

  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  externalId?: string;

  @ApiProperty({ description: 'type' })
  type?: AssessmentType;

  @ApiProperty({ description: 'isGlobal', required: false })
  isGlobal?: boolean;

  @ApiProperty({ description: 'CommonId', required: false })
  commonId?: string;

  @ApiProperty({
    description: 'ShowResult',
    enum: ShowResult,
  })
  showResult?: ShowResult;

  @ApiProperty({
    description: 'ShowCorrectAnswers',
    enum: ShowCorrectAnswers,
  })
  showCorrectAnswers?: ShowCorrectAnswers;

  @ValidateIf(o => o.hasOwnProperty('shuffleAnswers'))
  @IsBooleanString()
  shuffleAnswers: boolean;

  @ValidateIf(o => o.hasOwnProperty('shuffleQuestions'))
  @IsBooleanString()
  shuffleQuestions: boolean;

  @ValidateIf(o => o.hasOwnProperty('levelId'))
  @IsUUID()
  levelId?: string;

  @ApiProperty({
    description: 'Max attempts',
    default: 3,
  })
  @ValidateIf(o => o.hasOwnProperty('maxAttempts'))
  @IsInt()
  @Min(1)
  maxAttempts?: number;
}

export class CreateAssessmentForContentDto extends CreateAssessmentDto {
  @ApiProperty({ description: 'External ID (EADBOX)', required: false })
  questions?: Array<Question>;

  @ValidateIf(o => o.hasOwnProperty('id'))
  @IsUUID()
  @ApiProperty({ description: 'id' })
  id?: string;
}
