import { PartialType } from '@nestjs/swagger';
import { CreateSubmissionDto } from './create-submission.dto';

export class UpdateSubmissionGradeDto extends PartialType(CreateSubmissionDto) {
  correctAnswerCount?: number;

  incorrectAnswerCount?: number;
}
