import { LectureStatus, SubmissionStatus, SystemRole } from '../../../lib/src/api/types';
import { AssessmentType } from '@lib/types/enums';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { DatePart } from '@lib/types/enums/date-part.enum';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiProperty, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { EnrollmentsHelper } from '@utils/enrollments.helper';
import * as assert from 'assert';
import axios from 'axios';
import { Type } from 'class-transformer';
import { IsArray, IsOptional, IsUUID, ValidateNested } from 'class-validator';
import { Request } from 'express';
import { LecturesService } from 'src/courses/lectures.service';
import { NotificationType } from 'src/lib/enums/notification-type.enum';
import { UserWithPermissions } from 'src/lib/types/UserWithPermissions';
import { NotificationsService } from 'src/notifications/notifications.service';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { populateLogBySubmission, PopulateLogWith, SetLogTag } from 'src/service/log/logging';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { SubmissionTriggerService } from 'src/trigger/services/submission-trigger.service';
import { UsersService } from 'src/users/users.service';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { AssessmentCriteriaEntryService } from '../criteria-based-assessments/assessment-criteria-entry.service';
import { ParamAssessmentCriterionEntry } from '../criteria-based-assessments/dto/create-assessment-criterion-entry.dto';
import { Assessment } from '../lib/entities/assessment.entity';
import { Submission } from '../lib/entities/submission.entity';
import { Permission } from '../lib/enums/permission.enum';
import { PgConstraints } from '../pg.decorators';
import { RequirePermissions } from '../require-permissions.decorator';
import { AnswersService } from './answers.service';
import { AssessmentsService } from './assessments.service';
import { CreateSubmissionDto } from './dto/create-submission.dto';
import { CreateWebinarSubmissionDto } from './dto/create-webinar-submission.dto';
import { QuerySubmissionListDto, QuerySubmissionListForAdminPanelDto } from './dto/query-submission-list.dto';
import { QuerySubmissionIds } from './dto/query-submission-ids.dto';
import { QuerySubmissionOneDto } from './dto/query-submission-one.dto';
import { QuerySubmissionDto } from './dto/query-submission.dto';
import { QueryTutorReportDto } from './dto/query-tutor-report.dto';
import { TutorUpdateSubmissionDto } from './dto/tutor-update-submission.dto';
import { UpdateSubmissionDto } from './dto/update-submission.dto';
import { QuizService } from './quiz.service';
import { SubmissionsService, TutorsLoad } from './submissions.service';
import { CRUD } from 'src/lib/interfaces/CRUD';

class LoginInfoDto {
  @ApiProperty()
  password: string;
  @ApiProperty({
    description: 'Optional, if not passed will be the same as tutor login',
    required: false,
  })
  email?: string;

  @ApiProperty({
    description: 'If passed will ignore user eadbox token',
    required: false,
  })
  forceLoginPass?: boolean;
}

class ReviewBodyDto {
  @ApiProperty()
  @Type(() => LoginInfoDto)
  tutor: LoginInfoDto;

  @IsUUID()
  @ApiProperty()
  answerId: string;

  @ApiProperty({ description: 'CBA entries' })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ParamAssessmentCriterionEntry)
  @IsOptional()
  entries?: ParamAssessmentCriterionEntry[];
}

class TestPassDto {
  @ApiProperty()
  email: string;
  @ApiProperty()
  password: string;
}

@ApiTags('submissions')
@ApiBearerAuth()
@Controller()
export class SubmissionsController implements CRUD {
  private MUTEX_TIME: number;

  constructor(
    private readonly submissionsService: SubmissionsService,
    private readonly answerService: AnswersService,
    private readonly usersService: UsersService,
    private readonly assessmentService: AssessmentsService,
    private readonly notificationsService: NotificationsService,
    private readonly socketsGateway: SocketsGateway,
    private quizService: QuizService,
    private readonly submissionTriggerService: SubmissionTriggerService,
    private readonly lecturesService: LecturesService,
    private readonly assessmentCriteriaEntryService: AssessmentCriteriaEntryService,
    private readonly enrollmentsHelper: EnrollmentsHelper,
  ) {
    const mutexTimeSeconds = process.env.SUBMISSION_LOCK_SECONDS ?? '3600';
    this.MUTEX_TIME = 1000 * parseInt(mutexTimeSeconds, 10);
  }

  checkRights(courseId: string, assessment: Assessment, user: Express.User) {
    const hasViewPerm = user.hasPermission(Permission.COURSE_VIEW);
    const hasEnrollment =
      user.enrolledAsSupport(courseId) ||
      (user.enrolledAsStudent(courseId) &&
        ((assessment.type == AssessmentType.Default && user.coursePermissions[courseId].canSubmitAssignment) ||
          (assessment.type == AssessmentType.Quiz && user.coursePermissions[courseId].canSubmitQuiz) ||
          (assessment.type == AssessmentType.Survey && user.coursePermissions[courseId].canSubmitSurvey))) ||
      user.enrolledAsTutor(courseId) ||
      user.enrolledAsTeamlead(courseId) ||
      user.enrolledAsModerator(courseId);
    return !!(hasViewPerm || hasEnrollment);
  }

  @Post('submissions')
  @ApiOperation({ summary: 'Create a new submission' })
  @ApiResponse({
    status: 201,
    description: 'Created submission',
    type: Submission,
  })
  @PgConstraints()
  async create(@Body() createSubmissionDto: CreateSubmissionDto, @Req() req: Request) {
    createSubmissionDto.userId = req.user.id;
    const assessment = await this.assessmentService.findOne(createSubmissionDto.assessmentId);
    if (!this.checkRights(createSubmissionDto.courseId, assessment, req.user)) {
      throw new ForbiddenException('ERRORS.SUBMISSION_FORBIDDEN');
    }

    const submission = await this.submissionsService.create(createSubmissionDto, false);

    if ((assessment.type === AssessmentType.Quiz || assessment.type === AssessmentType.Survey) && !assessment.isWebinarSurvey) {
      await this.lecturesService.saveLectureProgress(submission.user.id, submission.lecture.id, {
        status: LectureStatus.inProgress,
      });
    }

    return submission;
  }

  @Post('submissions/webinar')
  @ApiOperation({ summary: 'Create a new webinar submission' })
  @ApiResponse({
    status: 201,
    description: 'Created webinar submission',
    type: Submission,
  })
  @PgConstraints()
  createWebinarSubmission(@Body() createSubmissionDto: CreateWebinarSubmissionDto, @Req() req: Request) {
    createSubmissionDto.userId = req.user.id;
    return this.submissionsService.createWebinarSubmision(createSubmissionDto);
  }

  @Get('submissions')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW, Permission.SUBMISSIONS_TUTOR_VIEW)
  @ApiOperation({ summary: 'List all submissions' })
  @ApiPaginatedResponse(Submission)
  findAll(@Query() query: QuerySubmissionDto, @Pagination() paging: PaginationDto, @Req() req: Request) {
    query = this.checkSubmissionViewPermissions(req.user, query);
    return this.submissionsService.findAll(paging, query);
  }

  @Get('submissions/byids')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW, Permission.SUBMISSIONS_TUTOR_VIEW)
  @ApiOperation({ summary: 'List all submissions' })
  @ApiPaginatedResponse(Submission)
  findByIds(@Query() query: QuerySubmissionIds, @Pagination() paging: PaginationDto) {
    return this.submissionsService.findByIds(paging, query);
  }

  @Get('submissions/list')
  @RequirePermissions(
    Permission.SUBMISSIONS_VIEW,
    Permission.SUBMISSIONS_TUTOR_VIEW,
    Permission.SURVEY_VIEW,
    Permission.WEBINAR_SURVEY_VIEW,
  )
  @ApiOperation({ summary: 'List all submissions for datatable' })
  @ApiPaginatedResponse(Submission)
  findAllList(@Query() query: QuerySubmissionListDto, @Pagination() paging: PaginationDto, @Req() req: Request) {
    query = this.checkSubmissionViewPermissions(req.user, query);
    return this.submissionsService.findAllList(paging, query);
  }

  @Post('submissions/for-admin-panel')
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @RequirePermissions(Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'List submissions for admin panel' })
  @ApiPaginatedResponse(Submission)
  findAllAdminPanel(@Body() body: QuerySubmissionListForAdminPanelDto, @Pagination() paging: PaginationDto, @Req() req: Request) {
    return this.submissionsService.findAllForAdminPanel(paging, body);
  }

  @Get('submissions/summary')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW, Permission.SUBMISSIONS_TUTOR_VIEW)
  @ApiOperation({ summary: 'List all submissions' })
  summary(@Query() query: QuerySubmissionDto, @Req() req: Request) {
    query = this.checkSubmissionViewPermissions(req.user, query);
    if (query.asRole == SystemRole.Tutor) {
      query.tutorId = req.user.id;
    }
    return this.submissionsService.summary(query);
  }

  @Get('submissions/filterData')
  @RequirePermissions(Permission.SUBMISSIONS_VIEW, Permission.SUBMISSIONS_TUTOR_VIEW)
  @ApiOperation({ summary: 'List all submissions' })
  getFilterData(@Query() query: QuerySubmissionDto, @Req() req: Request) {
    query = this.checkSubmissionViewPermissions(req.user, query);
    return this.submissionsService.getFilterData(query);
  }

  checkSubmissionViewPermissions(user: UserWithPermissions, query: QuerySubmissionDto) {
    const newQuery = { ...query };
    if (!user.permissions.includes(Permission.SUBMISSIONS_VIEW)) {
      if (query.courseId) {
        assert(
          this.tutorHasAccessToTheCourse(user as UserWithPermissions, query.courseId),
          new UnauthorizedException('Must be enrolled to the course as a tutor'),
        );
      } else if (query.assessmentType?.length === 1 && query.assessmentType[0] == AssessmentType.Survey) {
        if (!user.permissions.includes(Permission.SURVEY_VIEW)) {
          newQuery.isWebinarResult = true;
        }
        if (!user.permissions.includes(Permission.WEBINAR_SURVEY_VIEW)) {
          newQuery.isWebinarResult = false;
        }
      } else {
        if (query.asRole == SystemRole.Teamlead) {
          newQuery.teamleadId = user.id;
        } else if (query.asRole == SystemRole.Tutor) {
          newQuery.tutorId = user.id;
        }
      }
    }

    return newQuery;
  }

  @Get('user/submissions')
  @ApiOperation({ summary: 'List all submissions for user' })
  @ApiQuery({
    name: 'courseId',
    description: 'Filter by course',
    required: false,
  })
  @ApiQuery({
    name: 'moduleId',
    description: 'Filter by module',
    required: false,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Filter by lecture',
    required: false,
  })
  @ApiQuery({
    name: 'status',
    description: 'Filter by status',
    required: false,
  })
  @ApiQuery({
    name: 'tutorId',
    description: 'Filter by tutor',
    required: false,
  })
  @ApiQuery({
    name: 'assessmentId',
    description: 'Filter by assessment',
    required: false,
  })
  @ApiPaginatedResponse(Submission)
  findAllForUser(@Query() query: QuerySubmissionDto, @Pagination() paging: PaginationDto, @Req() req: Request) {
    return this.submissionsService.findAll(paging, {
      ...query,
      userId: req.user.id,
    });
  }

  @Get('submissions/:id')
  @ApiOperation({ summary: 'Get one submission by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a submission by id',
    type: Submission,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Express.Request, @Query() query: QuerySubmissionOneDto) {
    const result = await this.submissionsService.findOne(id, query);
    if (!result) {
      throw new NotFoundException('ERRORS.SUBMISSION_NOT_FOUND');
    }
    if (!req.user.permissions.includes(Permission.SUBMISSIONS_VIEW) && result.user.id != req.user.id) {
      throw new ForbiddenException('ERRORS.SUBMISSION_FORBIDDEN');
    }
    return result;
  }

  @Transactional()
  @Patch('submissions/:id/submit')
  @ApiOperation({ summary: 'Submit submission' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async submit(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const submission = await this.quizService.canSubmitSubmisssion(id, req.user);

    if (this.quizService.isQuizAssessment(submission.assessment)) {
      const quizResult = await this.quizService.gradeQuiz(submission);

      if (submission?.lecture?.id) {
        // Delayed to make sure all the tables and views are updated
        setTimeout(() => {
          this.lecturesService.updateLectureProgressAfterReview(submission.user.id, submission.lecture.id);
        }, 1000);
      }
      return quizResult;
    }

    if (this.quizService.isSurveyAssessment(submission.assessment)) {
      const surveyResult = await this.quizService.gradeSurvey(submission);
      if (submission?.lecture?.id) {
        // Delayed to make sure all the tables and views are updated
        setTimeout(() => {
          this.lecturesService.updateLectureProgressAfterReview(submission.user.id, submission.lecture.id);
        }, 1000);
      }
      return surveyResult;
    }

    return this.submissionsService.update(id, {
      status: SubmissionStatus.Submitted,
      submittedAt: new Date(),
    });
  }

  @Transactional()
  @Patch('submissions/:id/draft')
  @ApiOperation({ summary: 'Draft submission' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async draft(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.quizService.canDraftSubmisssion(id, req.user);

    const result = this.submissionsService.update(id, {
      status: SubmissionStatus.Draft,
      draftAt: new Date(),
    });

    return result;
  }

  @Transactional()
  @Patch('submissions/:id')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Updates a submission' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateSubmissionDto: UpdateSubmissionDto) {
    const submission = await this.submissionsService.update(id, updateSubmissionDto);

    return submission;
  }

  @Transactional()
  @Delete('submissions/:id')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Deletes a submission' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.submissionsService.remove(id);
  }

  @Get('tutor/submissions')
  @RequirePermissions(Permission.SUBMISSIONS_TUTOR_VIEW, Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'List all submissions' })
  @ApiPaginatedResponse(Submission)
  async findForTutor(@Query() query: QuerySubmissionDto, @Req() req: Request, @Pagination() paging: PaginationDto) {
    const submissions = await this.submissionsService.findForTutor(paging, req.user.id, query);

    return submissions;
  }

  @Get('tutor/submissions/:id')
  @RequirePermissions(Permission.SUBMISSIONS_TUTOR_VIEW, Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'Get one submission by id for tutor' })
  @ApiResponse({
    status: 200,
    description: 'Get a submission by id',
    type: Submission,
  })
  async findOneForTutor(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request, @Query() query: QuerySubmissionOneDto) {
    const submission = await this.submissionsService.findOneForTutor(id, query);
    if (submission && submission.course && this.tutorHasAccessToTheCourse(req.user, submission.course.id)) {
      return submission;
    } else if (submission) {
      throw new UnauthorizedException('Tutor is not enrolled to the course of that submission');
    } else {
      throw new NotFoundException('Submission is not found');
    }
  }

  @Transactional()
  @Patch('tutor/submissions/:id')
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  )
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE, Permission.SUBMISSIONS_TUTOR_REVIEW)
  @ApiOperation({ summary: 'Updates a submission after tutor review' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async updateForTutor(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateSubmissionDto: TutorUpdateSubmissionDto,
    @Req() req: Request,
  ) {
    const submission = await this.submissionsService.findOne(id);
    const canUserReview =
      (req.user.isTutor() && submission.tutor?.id == req.user.id) ||
      (req.user.isTeamlead() && submission.tutor?.id && req.user.teamleadTutorIds.includes(submission.tutor.id)) ||
      req.user.hasPermission(Permission.SUBMISSIONS_MANAGE);
    assert(canUserReview, new UnauthorizedException('Submission is not assigned to that tutor'));

    const result = await this.submissionsService.update(id, {
      ...updateSubmissionDto,
      tutor: { id: req.user.id },
    });

    submission.isQuestion = result.isQuestion;

    if (!submission.isQuestion) {
      setTimeout(async () => {
        // Must be delayed to be sure all the views are updated
        await this.lecturesService.updateLectureProgressAfterReview(submission.user.id, submission.lecture.id);
        await this.enrollmentsHelper.setGraduatedDate(submission.user.id, submission.course.id);
      }, 1000);
    }

    if (submission.status == SubmissionStatus.Submitted && updateSubmissionDto.status == SubmissionStatus.Graded) {
      await this.submissionsService.setGradeTariff(submission);
      if (!submission.externalId) {
        await this.submissionTriggerService.submissionGraded(submission.id, updateSubmissionDto.score);
      }

      if (!submission.isQuestion) {
        await this.notificationsService.create({
          title: 'Your submission is corrected',
          body: 'The tutor gave a feedback on your submission',
          notificationType: NotificationType.SubmissionGraded,
          notificationData: {
            submissionId: submission.id,
            courseId: submission.course.id,
            courseSlug: submission.course.slug,
            lectureId: submission.lecture.id,
          },
          authorId: req.user.id,
          recipientId: submission.user.id,
        });
      } else {
        await this.notificationsService.create({
          title: 'New response to submission',
          body: 'Tutor answered to your question',
          notificationType: NotificationType.SubmissionAnswered,
          notificationData: {
            submissionId: submission.id,
            courseId: submission.course.id,
            courseSlug: submission.course.slug,
            lectureId: submission.lecture.id,
          },
          authorId: req.user.id,
          recipientId: submission.user.id,
        });
      }
      this.socketsGateway.submissionChange({ ...(await this.submissionsService.findOne(submission.id)), action: 'graded' });
    } else if (submission.status == SubmissionStatus.Submitted && updateSubmissionDto.status == SubmissionStatus.Draft) {
      this.socketsGateway.submissionChange({ ...result, action: 'studentCancel' });
    }

    this.socketsGateway.dataUpdateForUser('updateLecture', submission.user.id);

    return result;
  }

  @Post('tutor/submissions/:submissionId/lock')
  @ApiTags('tutor')
  @RequirePermissions(Permission.SUBMISSIONS_LOCK)
  @ApiOperation({ summary: 'Lock submission to current tutor' })
  @ApiResponse({
    status: 201,
    description: 'Created submission',
    type: Submission,
  })
  @PgConstraints()
  @PopulateLogWith(async (log, context) => {
    const submissionId = log.url.slice(1).split('/')[2];
    return populateLogBySubmission(submissionId, context);
  })
  @SetLogTag('SubmissionsController.lock')
  async lock(@Param('submissionId') submissionId: string, @Req() req: Request) {
    const submission = await this.submissionsService.findOne(submissionId);
    if (!submission) {
      throw new NotFoundException('Submission not found');
    }

    if (submission.mutex && submission.tutor && submission.tutor.id != req.user.id && submission.mutex.getTime() > new Date().getTime()) {
      throw new UnauthorizedException('This submission is locked for another tutor');
    }
    if (submission.bindTutor && submission.tutor.id != req.user.id) {
      throw new UnauthorizedException('This submission is binded for another tutor');
    }

    await this.usersService.saveActivity(req.user.id, {
      lastSubmissionView: new Date(),
    });
    const result = await this.submissionsService.setMutex(submissionId, req.user.id);
    this.socketsGateway.dataUpdateForUser('updateLecture', req.user.id);

    return result;
  }

  @Post('tutor/box/test-password')
  @ApiTags('tutor')
  @ApiOperation({ summary: 'Test tutor password' })
  async testPass(@Body() body: TestPassDto) {
    try {
      return this.submissionsService.getBoxSession(body.email, body.password);
    } catch (err) {
      return null;
    }
  }

  @Post('tutor/box/submissions/review-answer')
  @ApiTags('tutor')
  @RequirePermissions(Permission.SUBMISSIONS_TUTOR_REVIEW)
  @ApiOperation({ summary: 'Send submission answer review from tutor' })
  // @ApiResponse({
  //   status: 201,
  //   description: 'Created submission',
  //   type: Submission,
  // })
  @PgConstraints()
  @PopulateLogWith<ReviewBodyDto>(async (log, context) => {
    const answerService = context.get(AnswersService);
    const answer = await answerService.findOne(log.body.answerId);

    if (answer) {
      return populateLogBySubmission(answer.submission.id, context);
    } else {
      return {};
    }
  })
  @SetLogTag('SubmissionsController.review')
  async review(@Req() req: Request, @Body() body: ReviewBodyDto) {
    // #todo> infer login

    const useLoginPass = !req.user.eadboxToken || body.tutor.forceLoginPass;

    if (!(req.user.isTutor() || req.user.isManager() || req.user.isTeamlead())) {
      throw new BadRequestException("User doesn't have Tutor role");
    }

    const answer = await this.answerService.findOne(body.answerId);
    const tutorEmail = body.tutor.email || req.user.email;
    const tutorPass = body.tutor.password;

    assert(tutorEmail, new BadRequestException('Tutor email not found'));
    if (useLoginPass) {
      assert(tutorPass, new BadRequestException('Tutor password not found'));
    }
    assert(answer, new NotFoundException('Answer not found'));
    assert(answer.tutorComment, new NotFoundException('Answer tutor comment is empty'));

    const submission = await this.submissionsService.findOne(answer.submission.id);

    if (!submission.externalId) {
      // Doesn't require sending to EADBOX
      return true;
    }

    assert(submission, new NotFoundException('Submission not found'));

    // add files to tutor comment on eadbox
    let comment = answer.tutorComment;
    if (answer.tutorFiles.length > 0) {
      answer.tutorFiles.forEach(file => {
        comment += `<p><a href="${file.url}" title="${file.filename}" target="_blank">${file.filename}</a><br></p>`;
      });
    }

    const boxApiUrl = this.submissionsService
      .generateAssessmentLink(submission)
      .replace('/ng/', '/ng/api/v2/')
      .replace(';userId=', '?&user_id=')
      .replace('corrections', 'user_assessments');

    let sessionCookie;
    if (useLoginPass) {
      sessionCookie = await this.submissionsService.getBoxSession(tutorEmail, tutorPass, true);
    } else {
      sessionCookie = await this.submissionsService.getBoxSessionByToken(req.user.eadboxToken);
    }

    assert(sessionCookie, new BadRequestException('Tutor credentials are incorrect'));

    // can throw
    const { data: boxCourse } = await axios.get<import('./types/box').Response.Init>(boxApiUrl, {
      headers: { Cookie: `_session_id=${sessionCookie};` },
    });

    const type = answer.question.type == 'upload' ? 'file' : 'freeForm';

    const boxPutData: import('./types/box').Request.Init = {
      data: {
        type: 'userAssessment',
        id: boxCourse.data.id,
        relationships: {
          answers: {
            data: [
              {
                id: boxCourse.data.relationships.answers.data[0].id,
                type: `userAssessment--answer--${type}`,
                attributes: {
                  correctionNote: comment,
                  grade: answer.score / 100,
                },
                relationships: {
                  question: {
                    data: {
                      id: boxCourse.data.relationships.questions.data[0].id,
                    },
                  },
                },
              },
            ],
          },
          user: { data: { id: boxCourse.data.relationships.user.data.id } },
        },
      },
    };

    await axios.put(boxApiUrl, boxPutData, {
      headers: { Cookie: `_session_id=${sessionCookie};` },
    });

    if (body.entries) {
      await this.assessmentCriteriaEntryService.create(body.answerId, body.entries);
    }

    return true;
  }

  @Post('tutor/submissions/:submissionId/unlock')
  @ApiTags('tutor')
  @RequirePermissions(Permission.SUBMISSIONS_LOCK)
  @ApiOperation({ summary: 'Unlock submission' })
  @ApiResponse({
    status: 201,
    description: 'Created submission',
    type: Submission,
  })
  @PgConstraints()
  @PopulateLogWith((log, context) => {
    const submissionId = log.url.slice(1).split('/')[2];
    return populateLogBySubmission(submissionId, context);
  })
  @SetLogTag('SubmissionsController.unlock')
  async unlock(@Param('submissionId') submissionId: string, @Req() req: Request) {
    const submission = await this.submissionsService.findOne(submissionId);
    if (!submission) {
      throw new NotFoundException('Submission not found');
    }
    if (req.user.id != submission.tutor?.id && !req.user.permissions.includes(Permission.SUBMISSIONS_MANAGE)) {
      throw new UnauthorizedException("Can't unlock submission you don't own");
    }
    return this.submissionsService.unsetMutex(submissionId);
  }

  @Post('tutor/distribute')
  @ApiTags('tutor')
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  @ApiOperation({ summary: 'Distribute submissions between tutors' })
  @PgConstraints()
  async distribute(): Promise<TutorsLoad[]> {
    return this.submissionsService.distribute();
  }

  @Post('tutor/pause')
  @Transactional()
  @ApiTags('tutor')
  @ApiOperation({ summary: 'Pause tutor activity (current user)' })
  @PgConstraints()
  pause(@Req() req: Request) {
    return this.submissionsService.tutorPause(req.user.id, true);
  }

  @Post('tutor/:tutorId/pause')
  @Transactional()
  @ApiTags('tutor')
  @ApiOperation({ summary: 'Pause tutor activity' })
  @PgConstraints()
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  pauseTutor(@Param('tutorId', new ParseUUIDPipe()) tutorId?: string) {
    return this.submissionsService.tutorPause(tutorId, true);
  }

  @Post('tutor/unpause')
  @Transactional()
  @ApiTags('tutor')
  @ApiOperation({ summary: 'Unpause tutor activity (current user)' })
  @PgConstraints()
  unpause(@Req() req: Request) {
    return this.submissionsService.tutorUnpause(req.user.id);
  }

  @Post('tutor/:tutorId/unpause')
  @Transactional()
  @ApiTags('tutor')
  @ApiOperation({ summary: 'Unpause tutor activity' })
  @PgConstraints()
  @RequirePermissions(Permission.SUBMISSIONS_MANAGE)
  unpauseTutor(@Param('tutorId', new ParseUUIDPipe()) tutorId?: string) {
    return this.submissionsService.tutorUnpause(tutorId);
  }

  @Post('submissions/bind/:submissionId/:tutorId')
  @ApiTags('submissions')
  @ApiOperation({ summary: 'Bind tutor to submission' })
  @PgConstraints()
  @RequirePermissions(Permission.SUBMISSIONS_BIND)
  bindTutor(
    @Param('submissionId', new ParseUUIDPipe()) submissionId?: string,
    @Param('tutorId', new ParseUUIDPipe()) tutorId?: string,
    @Req() req?: Request,
  ) {
    return this.submissionsService.bindTutor(submissionId, tutorId);
  }

  @Get('tutor/students')
  @ApiTags('students')
  @ApiOperation({ summary: 'Get students with submessions' })
  @PgConstraints()
  tutorStudent(@Req() req: Request) {
    return this.submissionsService.activeStudents(req.user.id);
  }

  @Get('tutor/report/month')
  @ApiTags('report')
  @ApiOperation({ summary: 'Get tutor report, grouped by month' })
  @PgConstraints()
  tutorReportByMonth(@Query() query: QueryTutorReportDto, @Req() req: Request) {
    if (!req.user.hasPermission(Permission.REPORT_VIEW)) {
      query.tutorId = req.user.id;
    }

    return this.submissionsService.tutorReport(query, DatePart.Month);
  }

  @Get('tutor/report/day')
  @ApiTags('report')
  @ApiOperation({ summary: 'Get tutor report, grouped by day' })
  @PgConstraints()
  tutorReportByDay(@Query() query: QueryTutorReportDto, @Req() req: Request) {
    if (!req.user.hasPermission(Permission.REPORT_VIEW)) {
      query.tutorId = req.user.id;
    }
    return this.submissionsService.tutorReport(query, DatePart.Day);
  }

  @Get('students/active')
  @ApiTags('students')
  @ApiOperation({ summary: 'Get students with submessions' })
  @PgConstraints()
  activeStudent() {
    return this.submissionsService.activeStudents();
  }

  tutorHasAccessToTheCourse(tutor: UserWithPermissions, courseId: string): boolean {
    if (tutor.permissions.includes(Permission.SUBMISSIONS_VIEW)) {
      return true;
    }
    const isEnrolled = tutor.enrollments.some(e => e.courseId == courseId && e.role == CourseRole.Tutor);
    const isPartiallyEnrolled = tutor.modules.some(m => m.courseId == courseId);
    return isEnrolled || isPartiallyEnrolled;
  }
}
