import { QueryAttemptsDto } from './dto/query-attempts.dto';
import { Controller, Get, Post, Body, Param, Delete, Query, ParseUUIDPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AttemptsService } from './attempts.service';
import { CreateAttemptDto, CreateOneAdditionalAttemptDto } from './dto/create-attempt.dto';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Permission } from '@enums/permission.enum';
import { PgConstraints } from 'src/pg.decorators';

@ApiTags('attempts')
@ApiBearerAuth()
@Controller('attempts')
export class AttemptsController {
  constructor(private readonly attemptsService: AttemptsService) {}

  @Post('/one')
  @RequirePermissions(Permission.ASSESSMENT_ATTEMPTS_MANAGE)
  @ApiOperation({ summary: 'Create one additional attempt for the user to the assessment' })
  @ApiResponse({
    status: 201,
    description: 'Created entity',
  })
  @PgConstraints()
  createOneAdditionalAttempt(@Body() createOneAdditionalAttemptDto: CreateOneAdditionalAttemptDto) {
    return this.attemptsService.createOneAdditionalAttempt(createOneAdditionalAttemptDto);
  }

  @Get()
  @RequirePermissions(Permission.SUBMISSIONS_VIEW)
  @ApiOperation({ summary: 'Get additional attempts for the user to the assessment' })
  @ApiResponse({
    status: 200,
  })
  @ApiQuery({
    name: 'userId',
    description: 'Filter by user',
    required: true,
  })
  @ApiQuery({
    name: 'assessmentId',
    description: 'Filter by assessment',
    required: true,
  })
  findAll(@Query() query: QueryAttemptsDto) {
    return this.attemptsService.findAll(query);
  }

  @Delete(':id')
  @RequirePermissions(Permission.ASSESSMENT_ATTEMPTS_MANAGE)
  @ApiOperation({ summary: 'Delete additional attempts' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.attemptsService.remove(id);
  }
}
