import { Answer } from '@entities/answer.entity';
import { Question } from '@entities/question.entity';
import {
  AnswerDataI,
  MultiChoiceAnswerI,
  QuestionType,
  RateAnswerI,
  ShortAnswerI,
  ShowCorrectAnswers,
  SingleChoiceAnswerI,
  TrueFalseAnswerI,
} from '@lib/api/types';
import { BadRequestException } from '@nestjs/common';

//
// из нескольких фиксированных ответов выбрать 1
// если правильно возвращем score
//
export class SingleChoice {
  static async isNewVersion(questionOld: Question, questionNew: Partial<Question>): Promise<boolean> {
    const answersOld = <SingleChoiceAnswerI[]>(<any>questionOld.data)?.answers || [];
    const answersNew = <SingleChoiceAnswerI[]>(<any>questionNew.data)?.answers || [];

    if (answersOld.length !== answersNew.length) return true;

    // проверяем id вопросов до и после( например: если один удалили и один добавили )
    const answersNewMap = answersNew.reduce((a, e) => ({ ...a, [e.id]: e }), {});
    const sameAnswersCount = answersOld.reduce((a, e) => (answersNewMap[e.id] ? a + 1 : a), 0);
    if (sameAnswersCount != answersOld.length) return true;

    const isNewAnyAnswer = answersOld.some(a => {
      return ['isCorrect'].some(attr => {
        if (a[attr] !== answersNewMap[a.id][attr]) return true;
        return false;
      });
    });
    if (isNewAnyAnswer) return true;

    return false;
  }

  static grade(answer: Answer) {
    const data = <SingleChoiceAnswerI>(<unknown>answer.data);
    const answersConfig = <SingleChoiceAnswerI[]>(<any>answer.question.data).answers;

    const answerBase = answersConfig.find(e => e.id === data.id);
    if (!answerBase) throw new BadRequestException('gradeSingleChoice: answerBase not found');
    if (answerBase.isCorrect) return { score: answer.question.score, isCorrect: true };
    return { score: 0, isCorrect: false };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersConfig = <SingleChoiceAnswerI[]>answer.question.data?.answers;
    const answersUser = <AnswerDataI>answer?.data;

    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showOnlyIncorrect) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: answersUser.id == e.id,
          };
          if (e.isUserInput) {
            res.userInput = answersUser.userInput;
          }
          return res;
        });
    }

    if (showCorrectAnswers === ShowCorrectAnswers.showAll) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: answersUser.id == e.id,
          };
          if (answersUser.id == e.id) {
            res.userAnswerCorrect = e?.isCorrect;
          } else {
            res.userNotSelectCorrect = e?.isCorrect;
          }
          return res;
        });
    }
    return result;
  }
}

//
// из нескольких фиксированных ответов выбрать - несколько
// если все правильные возвращем score
//
export class MultiChoice {
  static async isNewVersion(questionOld: Question, questionNew: Partial<Question>): Promise<boolean> {
    const answersOld = <MultiChoiceAnswerI[]>(<any>questionOld.data)?.answers || [];
    const answersNew = <MultiChoiceAnswerI[]>(<any>questionNew.data)?.answers || [];

    if (answersOld.length !== answersNew.length) return true;

    // проверяем id вопросов до и после( например: если один удалили и один добавили )
    const answersNewMap = answersNew.reduce((a, e) => ({ ...a, [e.id]: e }), {});
    const sameAnswersCount = answersOld.reduce((a, e) => (answersNewMap[e.id] ? a + 1 : a), 0);
    if (sameAnswersCount != answersOld.length) return true;

    const isNewAnyAnswer = answersOld.some(a => {
      return ['isCorrect'].some(attr => {
        if (a[attr] !== answersNewMap[a.id][attr]) return true;
        return false;
      });
    });
    if (isNewAnyAnswer) return true;

    return false;
  }

  static grade(answer: Answer) {
    const data = <AnswerDataI[]>answer.data || [];
    const answersConfig = <MultiChoiceAnswerI[]>(<any>answer.question.data).answers;

    const correctAnswerCount = answersConfig
      .map(answerCfg => {
        const answer = data.find(e => e.id == answerCfg.id);
        if (answer && answerCfg.isCorrect) {
          return true;
        }
        if (!answer && !answerCfg.isCorrect) {
          return true;
        }
        return false;
      })
      .reduce((a, e) => a + +e, 0);

    if (correctAnswerCount === answersConfig.length) {
      return {
        score: answer.question.score,
        isCorrect: true,
        correctAnswerCount,
      };
    }
    return { score: 0, isCorrect: false };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersUser = <AnswerDataI[]>answer?.data || [];

    const answersConfig = <MultiChoiceAnswerI[]>answer.question.data?.answers;
    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showOnlyIncorrect) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const answer = answersUser.find(a => e.id === a.id);
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: !!answer,
          };
          if (e.isUserInput && answer) {
            res.userInput = answer.userInput;
          }
          return res;
        });
    }

    if (showCorrectAnswers == ShowCorrectAnswers.showAll) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const answer = answersUser.find(a => e.id === a.id);
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: !!answer,
          };
          if (answer) {
            res.userAnswerCorrect = e?.isCorrect;
          } else {
            res.userNotSelectCorrect = e?.isCorrect;
          }
          return res;
        });
    }
    return result;
  }
}

//
// из 2х фиксированных ответов Да\Нет выбрать 1
// если правильно возвращем score
//
export class TrueFalse {
  static async isNewVersion(questionOld: Question, questionNew: Partial<Question>): Promise<boolean> {
    const answersOld = <TrueFalseAnswerI[]>(<any>questionOld.data)?.answers || [];
    const answersNew = <TrueFalseAnswerI[]>(<any>questionNew.data)?.answers || [];

    if (answersOld.length !== answersNew.length) return true;

    // проверяем id вопросов до и после( например: если один удалили и один добавили )
    const answersNewMap = answersNew.reduce((a, e) => ({ ...a, [e.id]: e }), {});
    const sameAnswersCount = answersOld.reduce((a, e) => (answersNewMap[e.id] ? a + 1 : a), 0);
    if (sameAnswersCount != answersOld.length) return true;

    const isNewAnyAnswer = answersOld.some(a => {
      return ['isCorrect'].some(attr => {
        if (a[attr] !== answersNewMap[a.id][attr]) return true;
        return false;
      });
    });
    if (isNewAnyAnswer) return true;

    return false;
  }

  static grade(answer: Answer) {
    const data = <TrueFalseAnswerI>(<unknown>answer.data);
    const answersConfig = <TrueFalseAnswerI[]>(<any>answer.question.data).answers;

    const answerBase = answersConfig.find(e => e.id === data.id);
    if (!answerBase) throw new BadRequestException('gradeTrueFalse: answerBase not found');
    if (answerBase.isCorrect) return { score: answer.question.score, isCorrect: true };
    return { score: 0, isCorrect: false };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersConfig = <TrueFalseAnswerI[]>answer.question.data?.answers;
    const answersUser = <AnswerDataI>answer?.data;

    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showOnlyIncorrect) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: answersUser.id == e.id,
          };
          if (e.isUserInput && answer) {
            res.userInput = answersUser.userInput;
          }
          return res;
        });
    }

    if (showCorrectAnswers === ShowCorrectAnswers.showAll) {
      result.answers = answersConfig
        .map(e => ({ ...e }))
        .map((e: any) => {
          const res: any = {
            id: e.id,
            value: e.value,
            userSelectIt: answersUser.id == e.id,
          };
          if (answersUser.id == e.id) {
            res.userAnswerCorrect = e?.isCorrect;
          } else {
            res.userNotSelectCorrect = e?.isCorrect;
          }
          return res;
        });
    }
    return result;
  }
}

//
// выбор ползунком - не оцениваем
// всегда возвращем score
//
export class Rate {
  static async isNewVersion(questionOld: Question, questionNew: Partial<Question>): Promise<boolean> {
    const answersOld = <RateAnswerI[]>(<any>questionOld.data)?.answers || [];
    const answersNew = <RateAnswerI[]>(<any>questionNew.data)?.answers || [];

    if (answersOld.length !== answersNew.length) return true;

    // проверяем id вопросов до и после( например: если один удалили и один добавили )
    const answersNewMap = answersNew.reduce((a, e) => ({ ...a, [e.id]: e }), {});
    const sameAnswersCount = answersOld.reduce((a, e) => (answersNewMap[e.id] ? a + 1 : a), 0);
    if (sameAnswersCount != answersOld.length) return true;

    return false;
  }

  static grade(answer: Answer) {
    const data = <RateAnswerI>(<unknown>answer.data);
    const answersConfig = <RateAnswerI[]>(<any>answer.question.data).answers;

    const answerBase = answersConfig.find(e => e.id === data.id);
    if (!answerBase) throw new BadRequestException('gradeRate: answerBase not found');
    if (answerBase.isCorrect) return { score: answer.question.score, isCorrect: true };
    return { score: 0, isCorrect: false };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersConfig = <RateAnswerI[]>answer.question.data?.answers;
    const answersUser = <RateAnswerI>answer?.data;
    const isCorrectAnswer = answersConfig?.find(e => e.id === answersUser.id);

    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showAll) {
      result.answers.push({
        id: answersUser.id,
        value: isCorrectAnswer.value,
        isCorrect: isCorrectAnswer?.isCorrect,
      });
      if (!isCorrectAnswer.isCorrect) {
        answersConfig.forEach(e => {
          if (e.isCorrect) {
            result.answers.push({
              id: e.id,
              value: e.value,
              isCorrect: e?.isCorrect,
            });
          }
        });
      }
    }
    return result;
  }
}

//
// текстовый ответ
//
export class ShortAnswer {
  static async isNewVersion(_questionOld: Question, _questionNew: Partial<Question>): Promise<boolean> {
    return false;
  }

  static grade(answer: Answer) {
    return { score: answer.question.score, isCorrect: true };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersConfig = <ShortAnswerI[]>answer.question.data?.answers;
    const answersUser = <AnswerDataI>answer?.data;
    const isCorrectAnswer = answersConfig?.find(e => e.id === answersUser.id);

    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showOnlyIncorrect) {
      result.answers.push({
        id: answersUser.id,
        value: isCorrectAnswer.value,
        isCorrect: isCorrectAnswer?.isCorrect,
      });
    }

    if (showCorrectAnswers === ShowCorrectAnswers.showAll) {
      result.answers.push({
        id: answersUser.id,
        value: isCorrectAnswer.value,
        isCorrect: isCorrectAnswer?.isCorrect,
      });
      if (!isCorrectAnswer.isCorrect) {
        answersConfig.forEach(e => {
          if (e.isCorrect) {
            result.answers.push({
              id: e.id,
              value: e.value,
              isCorrect: e?.isCorrect,
            });
          }
        });
      }
    }
    return result;
  }
}

//
// загрузка файла
//
export class FileUploadAnswer {
  static async isNewVersion(_questionOld: Question, _questionNew: Partial<Question>): Promise<boolean> {
    return false;
  }

  static grade(answer: Answer) {
    return { score: answer.question.score, isCorrect: true };
  }

  static addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    if (showCorrectAnswers === ShowCorrectAnswers.noShow) return {};
    const answersConfig = <ShortAnswerI[]>answer.question.data?.answers;
    const answersUser = <AnswerDataI>answer?.data;
    const isCorrectAnswer = answersConfig?.find(e => e.id === answersUser.id);

    const grade = this.grade(answer);
    const result = {
      answers: [],
      grade,
    };

    if (showCorrectAnswers === ShowCorrectAnswers.showOnlyIncorrect) {
      result.answers.push({
        id: answersUser.id,
        value: isCorrectAnswer.value,
        isCorrect: isCorrectAnswer?.isCorrect,
      });
    }

    if (showCorrectAnswers === ShowCorrectAnswers.showAll) {
      result.answers.push({
        id: answersUser.id,
        value: isCorrectAnswer.value,
        isCorrect: isCorrectAnswer?.isCorrect,
      });
      if (!isCorrectAnswer.isCorrect) {
        answersConfig.forEach(e => {
          if (e.isCorrect) {
            result.answers.push({
              id: e.id,
              value: e.value,
              isCorrect: e?.isCorrect,
            });
          }
        });
      }
    }
    return result;
  }
}
export class CollectUserNameAnswer {
  static async isNewVersion(_questionOld: Question, _questionNew: Partial<Question>): Promise<boolean> {
    return false;
  }

  static grade(answer: Answer) {
    return { score: answer.question.score, isCorrect: true };
  }

  static addResult(answer: Answer) {
    const answersUser = <RateAnswerI>answer?.data;
    const answersConfig = <ShortAnswerI[]>answer.question.data?.answers;
    const userAnswer = answersConfig?.find(e => e.id === answersUser.id);
    const grade = this.grade(answer);

    return {
      answers: [
        {
          id: answersUser.id,
          value: userAnswer.value,
          isCorrect: userAnswer?.isCorrect,
        },
      ],
      grade,
    };
  }
}

export default class QuizQuestions {
  async grade(answer: Answer) {
    switch (answer.question.type) {
      case QuestionType.SingleChoice:
        return SingleChoice.grade(answer);
      case QuestionType.MultiChoice:
        return MultiChoice.grade(answer);
      case QuestionType.TrueFalse:
        return TrueFalse.grade(answer);
      case QuestionType.Rate:
        return Rate.grade(answer);
      case QuestionType.ShortAnswer:
        return ShortAnswer.grade(answer);
      case QuestionType.Upload:
        return FileUploadAnswer.grade(answer);
      case QuestionType.CollectStudentName:
        return CollectUserNameAnswer.grade(answer);

      default:
        throw new BadRequestException(`grade: cant check answer with type ${answer.question.type}`);
    }
  }

  isUpdatedQuestionAttr(questionOld: Question, questionNew: Partial<Question>): boolean {
    const isUpdated = ['gradable', 'score', 'passing_score', 'type'].some(attr => {
      if (attr in questionNew && questionOld[attr] != questionNew[attr]) {
        return true;
      }
      return false;
    });
    if (isUpdated) {
      return true;
    }
    return false;
  }

  async isNewVersion(questionOld: Question, questionNew: Partial<Question>): Promise<boolean> {
    if (this.isUpdatedQuestionAttr(questionOld, questionNew)) {
      return true;
    }

    switch (questionOld.type) {
      case QuestionType.SingleChoice:
        return SingleChoice.isNewVersion(questionOld, questionNew);
      case QuestionType.MultiChoice:
        return MultiChoice.isNewVersion(questionOld, questionNew);
      case QuestionType.TrueFalse:
        return TrueFalse.isNewVersion(questionOld, questionNew);
      case QuestionType.Rate:
        return Rate.isNewVersion(questionOld, questionNew);
      case QuestionType.ShortAnswer:
        return ShortAnswer.isNewVersion(questionOld, questionNew);
      case QuestionType.Upload:
        return FileUploadAnswer.isNewVersion(questionOld, questionNew);
      case QuestionType.CollectStudentName:
        return CollectUserNameAnswer.isNewVersion(questionOld, questionNew);

      default:
        throw new BadRequestException(`isNewVersion: cant check answer with type ${questionOld.type}`);
    }
  }

  //
  // добавляем ответ для вывода на фронте
  //
  addResult(answer: Answer, showCorrectAnswers: ShowCorrectAnswers) {
    switch (answer.question.type) {
      case QuestionType.SingleChoice:
        return SingleChoice.addResult(answer, showCorrectAnswers);
      case QuestionType.MultiChoice:
        return MultiChoice.addResult(answer, showCorrectAnswers);
      case QuestionType.TrueFalse:
        return TrueFalse.addResult(answer, showCorrectAnswers);
      case QuestionType.Rate:
        return Rate.addResult(answer, showCorrectAnswers);
      case QuestionType.ShortAnswer:
        return ShortAnswer.addResult(answer, showCorrectAnswers);
      case QuestionType.Upload:
        return FileUploadAnswer.addResult(answer, showCorrectAnswers);
      case QuestionType.CollectStudentName:
        return CollectUserNameAnswer.addResult(answer);

      default:
        throw new BadRequestException(`addResult: cant check answer with type ${answer.question.type}`);
    }
  }
}
