import { Answer } from '@entities/answer.entity';
import { Submission } from '@entities/submission.entity';
import { AssessmentType, ProfileUserName, QuestionType, SubmissionStatus } from '@lib/api/types';
import { BadRequestException, forwardRef, Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { Assessment } from '../lib/entities/assessment.entity';
import { CreateAnswerDto } from './dto/create-answer.dto';
import { CreateSubmissionDto } from './dto/create-submission.dto';
import { UpdateSubmissionDto } from './dto/update-submission.dto';
import { CreateWebinarSubmissionDto } from './dto/create-webinar-submission.dto';
import QuizQuestions from './quiz.questions';
import { SubmissionsService } from './submissions.service';
import { UsersService } from '../users/users.service';
import { ProfileService } from '../users/profile.service';

interface AnswerWithResult extends Answer {
  result?: {
    answers?: Record<string, unknown>;
    grade?: {
      isCorrect?: boolean;
      score?: number;
    };
  };
}
interface SubmissionWithResult extends Submission {
  result?: {
    isCorrect?: boolean;
    answers?: Record<string, unknown>;
  };
}
@Injectable()
export class QuizService {
  public readonly quizQuestions: QuizQuestions;
  constructor(
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,

    @Inject(forwardRef(() => SubmissionsService))
    private submissionsService: SubmissionsService,
    private usersService: UsersService,
    private userProfileService: ProfileService,
  ) {
    this.quizQuestions = new QuizQuestions();
  }

  isDefaultAssessment(assessment: Assessment) {
    return assessment.type === AssessmentType.Default;
  }

  isQuizAssessment(assessment: Assessment) {
    return assessment.type === AssessmentType.Quiz;
  }

  isSurveyAssessment(assessment: Assessment) {
    return assessment.type === AssessmentType.Survey;
  }
  timeFromStart(submission: Submission) {
    return dayjs().diff(submission.created, 'seconds');
  }

  isActiveNow(assessment: Assessment, submissions: Submission[]) {
    const currentAttempt = submissions.filter(e => e.current_attempt && e.status !== SubmissionStatus.Graded);
    if (currentAttempt.length > 1) {
      console.error('canStartQuiz: multiple currentAttempt!!!', currentAttempt[0].assessment);
    }

    if (currentAttempt.length) {
      const currentAttemptObj = currentAttempt[0];
      if (this.isActiveTimeLimit(assessment, currentAttemptObj)) {
        throw new BadRequestException('exist active submission', currentAttemptObj.id);
      }
    }
  }

  checkTimeDelayToRetest(assessment: Assessment, submissions: Submission[]) {
    if (!assessment.timeDelaytoRetestHours || !submissions.length) return;

    const submissionsSorted = submissions.filter(e => e.gradedAt).sort((a, b) => +a.gradedAt - +b.gradedAt);

    if (!submissionsSorted.length) return;

    const timeFromPrevious = dayjs().diff(submissionsSorted[0].gradedAt, 'hours');

    if (timeFromPrevious < assessment.timeDelaytoRetestHours) {
      throw new BadRequestException(`can restart after ${assessment.timeDelaytoRetestHours - timeFromPrevious} hours`);
    }
  }

  isActiveTimeLimit(assessment: Assessment, submission: Submission) {
    return !assessment.timeLimitSeconds || (assessment.timeLimitSeconds && assessment.timeLimitSeconds < this.timeFromStart(submission));
  }

  async isCountLimit(assessment: Assessment, userId: string) {
    if (assessment.countLimit) {
      const submissions = await this.submissionsService.countSubmission(assessment.id, userId);
      return submissions < assessment.countLimit;
    }
    return true;
  }

  async canStartQuiz(dto: CreateSubmissionDto | UpdateSubmissionDto | CreateWebinarSubmissionDto) {
    const assessment = await this.assessmentsRepository.findOne(dto.assessmentId);
    if (!this.isQuizAssessment(assessment)) return assessment;

    const submissions = await this.submissionsService.findForAssessmentAndUser(dto.assessmentId, dto.userId);

    this.isActiveNow(assessment, submissions);

    if (assessment.countLimit && submissions.length >= assessment.countLimit) {
      throw new BadRequestException('no attempts left');
    }

    this.checkTimeDelayToRetest(assessment, submissions);

    return assessment;
  }

  async canSaveAnswer(dto: CreateAnswerDto, assessment: Assessment, userId: string) {
    if (!dto.submissionId) throw new BadRequestException('submissionId empty');

    const submission = await this.submissionsService.findOne(dto.submissionId);

    if (submission.user.id !== userId) {
      throw new UnauthorizedException('Only submission owner can save answer');
    }

    if (submission.status !== SubmissionStatus.New) {
      throw new BadRequestException('submission must be in status NEW');
    }

    if (!this.isActiveTimeLimit(assessment, submission)) {
      throw new BadRequestException('submission time limit');
    }
  }

  async canDeleteAnswer(answer: Answer) {
    const assessment = await this.assessmentsRepository.findOne(answer.submission.assessment.id);
    if (!this.isQuizAssessment(assessment)) return;

    if (answer.submission.status !== SubmissionStatus.New) {
      throw new BadRequestException('submission must be in status NEW');
    }

    if (!this.isActiveTimeLimit(assessment, answer.submission)) {
      throw new BadRequestException('submission time limit');
    }
  }

  async canUpdateAnswer(answer: Answer) {
    const assessment = await this.assessmentsRepository.findOne(answer.submission.assessment.id);
    if (!assessment) {
      throw new NotFoundException('Not found assessment');
    }
    if (!this.isQuizAssessment(assessment)) return;

    if (answer.submission.status !== SubmissionStatus.New) {
      throw new BadRequestException('submission must be in status NEW');
    }

    if (!this.isActiveTimeLimit(assessment, answer.submission)) {
      throw new BadRequestException('submission time limit');
    }

    if (!assessment.canChangeAnswer) {
      throw new BadRequestException('cant change answer');
    }
  }

  async canSubmitSubmisssion(id: string, user: Express.User) {
    const submission = await this.submissionsService.findOne(id, {
      withQuestions: true,
    });
    if (!submission) {
      throw new NotFoundException('Not found submission');
    }
    if (submission.user.id !== user.id) {
      throw new UnauthorizedException('Only owner can submit');
    }
    if (submission.status !== SubmissionStatus.New && submission.status !== SubmissionStatus.Draft) {
      throw new BadRequestException('submission must be in status NEW or DRAFT');
    }
    return submission;
  }

  async canDraftSubmisssion(id: string, user: Express.User) {
    const submission = await this.submissionsService.findOne(id, {
      withQuestions: true,
    });
    if (!submission) {
      throw new NotFoundException('Not found submission');
    }
    if (submission.user.id !== user.id) {
      throw new UnauthorizedException('Only owner can submit');
    }
    if (submission.status !== SubmissionStatus.Submitted) {
      throw new BadRequestException('Submission must be in status SUBMITTED');
    }
    if (submission.mutex && new Date(submission.mutex) > new Date() && !submission.isMutexAuto) {
      throw new BadRequestException('The mentor has already started checking your homework and it is not possible to edit it');
    }
    return submission;
  }

  // ставим оценку за тест
  async gradeQuiz(submission: Submission) {
    if (submission.status !== SubmissionStatus.New) throw new BadRequestException('submission must be in status New');
    const { assessment, answers } = submission;
    let score = 0;
    let correctAnswerCount = 0;
    let incorrectAnswerCount = 0;

    for (const answer of answers) {
      const result = await this.quizQuestions.grade(answer);
      if (result.isCorrect) {
        correctAnswerCount++;
      } else {
        incorrectAnswerCount++;
      }
    }
    score = Math.round((correctAnswerCount / assessment.questions.length) * 100);

    const updatedSubmission = await this.submissionsService.update(submission.id, {
      success: score >= submission.assessment.passingScore,
      status: SubmissionStatus.Graded,
      gradedAt: new Date(),
      submittedAt: new Date(),
      score,
      comment: 'autochecked',
      correctAnswerCount,
      incorrectAnswerCount,
    });

    const submissionResult: SubmissionWithResult = await this.submissionsService.findOne(submission.id, {
      withQuestions: true,
    });
    await this.addSubmissionResult(submissionResult);
    return submissionResult;
  }

  async gradeSurvey(submission: Submission) {
    if (submission.status !== SubmissionStatus.New) throw new BadRequestException('submission must be in status New');
    const { assessment, answers } = submission;
    let score = 0;
    let correctAnswerCount = 0;
    const incorrectAnswerCount = 0;

    for (const _answer of answers) {
      correctAnswerCount++;
    }

    score = Math.round((correctAnswerCount / assessment.questions.length) * 100);

    const updatedSubmission = await this.submissionsService.update(submission.id, {
      success: true,
      status: SubmissionStatus.Graded,
      gradedAt: new Date(),
      submittedAt: new Date(),
      score,
      comment: 'autochecked',
      correctAnswerCount,
      incorrectAnswerCount,
    });

    const submissionResult: SubmissionWithResult = await this.submissionsService.findOne(submission.id, {
      withQuestions: true,
    });
    await this.addSubmissionResult(submissionResult);
    return submissionResult;
  }

  async addAnswerResult(answer: AnswerWithResult, assessment: Assessment, userId?: string) {
    if (!this.isQuizAssessment(assessment) && !this.isSurveyAssessment(assessment)) {
      return;
    }
    if (answer.question.type === QuestionType.CollectStudentName) {
      const [user, profile] = await Promise.all([this.usersService.findOne(userId), this.userProfileService.getProfileByUserId(userId)]);
      if (answer.data && user && profile && !Array.isArray(answer.data) && user.name !== answer.data.userInput) {
        const userName: ProfileUserName = JSON.parse(answer.data.userInput);
        const name = userName.name.trim();
        const surname = userName.surname.trim();
        const updateUserNamePromise = this.usersService.update(userId, {
          name: `${name} ${surname}`,
        });
        const updateProfileNamePromise = this.userProfileService.update(userId, {
          name,
          surname,
        });
        await Promise.all([updateUserNamePromise, updateProfileNamePromise]);
      }
    }
    return this.quizQuestions.addResult(answer, assessment.showCorrectAnswers);
  }

  async addSubmissionResult(submission: Submission) {
    if (!this.isQuizAssessment(submission.assessment) && !this.isSurveyAssessment(submission.assessment)) {
      return;
    }

    const collectStudentNameAnswers = submission.answers.filter(answer => {
      return answer.question.type === QuestionType.CollectStudentName;
    });
    if (collectStudentNameAnswers.length > 1) {
      throw new BadRequestException('“Collect student’s name” question should be unique in the survey');
    }
    for (const answer of submission.answers) {
      (answer as AnswerWithResult).result = await this.addAnswerResult(answer, submission.assessment, submission.user.id);
      (<any>answer.question.data).answers.forEach(a => {
        delete a.isCorrect;
      });
    }
    return submission;
  }
}
