import { Content } from '@entities/content.entity';
import { Question } from '@entities/question.entity';
import { Submission } from '@entities/submission.entity';
import { Webinar } from '@entities/webinars.entity';
import * as json2csv from 'json2csv';
import {
  AnswerDataI,
  QuerySurveyListResult,
  QuerySurveyTableResultDTO,
  QueryWebinarSurveyTableResultDTO,
  QuerySurveyVersionsDTO,
  SubmissionStatus,
} from '@lib/api/types';
import { AssessmentType, QuestionType } from '@lib/types/enums';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VersionUpdateDTO, VersionUpdateDTOResult } from 'src/courses/dto/update-content-version-update.dto';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { Repository } from 'typeorm';
import { Assessment } from '../lib/entities/assessment.entity';
import { CreateAssessmentDto, CreateAssessmentForContentDto } from './dto/create-assessment.dto';
import { QuerySurveyCourseListDto } from './dto/query-assessment-course-list.dto';
import { QuerySurveyListDto } from './dto/query-assessment-list.dto';
import { QueryAssessmentUserInputDto } from './dto/query-assessment-user-input.dto';
import { QueryAssessmentVersionListDto } from './dto/query-assessment-version-list.dto';
import { QueryAssessmentVersionWebinarListDto } from './dto/query-assessment-version-webinar-list.dto';
import { QuerySurveyWebinarListDto } from './dto/query-assessment-webinar-list.dto';
import { QueryAssessmentDto } from './dto/query-assessment.dto';
import { UpdateAssessmentDto } from './dto/update-assessment.dto';
import { QuizService } from './quiz.service';
import { convert } from 'html-to-text';
import { AssessmentLevelsService } from 'src/directory/service-level/assessment-levels.service';
import { QuestionsService } from './questions.service';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CRUD } from 'src/lib/interfaces/CRUD';
import { External } from './../lib/interfaces/External';
import { Version } from '../lib/interfaces/Version';

@Injectable()
export class AssessmentsService implements CRUD, Version, External {
  constructor(
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,

    @InjectRepository(Submission)
    private submissionRepository: Repository<Submission>,

    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,

    @InjectRepository(Content)
    private contentRepository: Repository<Content>,

    private quizService: QuizService,

    private assessmentLevelsService: AssessmentLevelsService,

    private questionsService: QuestionsService,
  ) {}

  async create(createAssessmentDto: CreateAssessmentDto) {
    if (!createAssessmentDto.levelId) {
      const defaultLevel = await this.assessmentLevelsService.getDefault();
      createAssessmentDto.levelId = defaultLevel.id;
    }
    const assessment = await this.assessmentsRepository.save({
      ...createAssessmentDto,
      lecture: { id: createAssessmentDto.lectureId },
      webinar: { webinarId: createAssessmentDto.webinarId },
    });

    return assessment;
  }

  findAll(query: QueryAssessmentDto): Promise<Assessment[]> {
    let queryBuilder = this.assessmentsRepository
      .createQueryBuilder('assessment')
      .innerJoinAndSelect('assessment.lecture', 'lecture')
      .innerJoinAndSelect('lecture.course', 'course')
      .innerJoinAndSelect('lecture.module', 'module')
      .innerJoinAndSelect('assessment.level', 'level')
      .leftJoinAndSelect('assessment.questions', 'questions');
    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('course.id = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.moduleId) {
      queryBuilder = queryBuilder.andWhere('module.id = :moduleId', {
        moduleId: query.moduleId,
      });
    }

    if (query.globalSurvey && query.webinarSurvey) {
      queryBuilder = this.assessmentsRepository
        .createQueryBuilder('assessment')
        .leftJoinAndSelect('assessment.questions', 'questions')
        .andWhere('assessment.isGlobal = TRUE AND assessment.current_version = TRUE');
    } else if (query.globalSurvey) {
      queryBuilder = this.assessmentsRepository
        .createQueryBuilder('assessment')
        .leftJoinAndSelect('assessment.questions', 'questions')
        .andWhere('assessment.isGlobal = TRUE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = FALSE');
    } else if (query.webinarSurvey) {
      queryBuilder = this.assessmentsRepository
        .createQueryBuilder('assessment')
        .leftJoinAndSelect('assessment.questions', 'questions')
        .andWhere('assessment.isGlobal = TRUE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = TRUE');
    }

    return queryBuilder.getMany();
  }

  @Transactional()
  findOne(id: string): Promise<Assessment> {
    return this.assessmentsRepository.findOne(id, {
      relations: ['lecture', 'lecture.module', 'lecture.course', 'questions', 'level'],
    });
  }

  findByExternalId(externalId: string): Promise<Assessment> {
    return this.assessmentsRepository.findOne(
      { externalId },
      {
        relations: ['lecture', 'questions'],
      },
    );
  }

  async update(id: string, updateAssessmentDto: UpdateAssessmentDto) {
    const updateFields = { ...updateAssessmentDto, id };
    return this.assessmentsRepository.save(updateFields);
  }

  remove(id: string) {
    return this.assessmentsRepository.delete(id);
  }

  async removeList(assessments: Assessment[]) {
    return this.assessmentsRepository.remove(assessments);
  }

  purgeAll() {
    return this.assessmentsRepository.clear();
  }

  getSubmissinCountForAssessment(assesment: Assessment) {
    return this.submissionRepository.count({
      assessment: { id: assesment.id },
    });
  }

  async isNewVersionAssessment(dto: CreateAssessmentForContentDto, assessmentOld: Assessment) {
    // It is not new if there is no old
    if (!assessmentOld) {
      return false;
    }
    // нет смысла новой версии, если для текущей нет submissions - просто обновляем
    const countSubmissionForAssessment = await this.getSubmissinCountForAssessment(assessmentOld);
    if (!countSubmissionForAssessment) return false;

    const changedAssessmentAttr = ['gradingType', 'passingScore', 'score', 'type'].some(attr => {
      if (attr in dto && dto[attr] !== assessmentOld[attr]) return true;
      return false;
    });
    if (changedAssessmentAttr) {
      return true;
    }

    if (assessmentOld.questions.length !== dto?.questions?.length) {
      return true;
    }

    // проверяем id вопросов до и после( например: если один удалили и один добавили )
    const newQuestionMap = dto?.questions?.reduce((a, e) => ({ ...a, [e.id]: e }), []);
    const sameQuestionCount = assessmentOld.questions.reduce((a, e) => (newQuestionMap[e.id] ? a + 1 : a), 0);
    if (sameQuestionCount != assessmentOld.questions.length) {
      return true;
    }

    // сравниваем вопросы
    for (const q of assessmentOld.questions) {
      const notSame = await this.quizService.quizQuestions.isNewVersion(q, newQuestionMap[q.id]);
      if (notSame) {
        return true;
      }
    }

    return false;
  }

  async updateAssessment(dto: CreateAssessmentForContentDto, lectureId?: string) {
    let assessmentOld = null;
    let assessment = null;
    assessmentOld = await this.findOne(dto.id);

    if (assessmentOld && dto.levelId !== assessmentOld.levelId) {
      await this.assessmentLevelsService.changeLevelOfAssessment(assessmentOld.levelId, dto.levelId, assessmentOld.id);
    }

    const isNewVersion = dto.type == AssessmentType.Default ? false : await this.isNewVersionAssessment(dto, assessmentOld);

    if (isNewVersion) {
      await this.assessmentsRepository.update(
        {
          commonId: assessmentOld.commonId,
        },
        {
          currentVersion: false,
        },
      );
      assessment = await this.assessmentsRepository.save({
        lecture: {
          id: lectureId,
        },
        ...{
          ...dto,
          id: undefined,
          questions: undefined,
          created: undefined,
          modified: undefined,
          currentVersion: true,
        },
      });

      if (dto.questions.length) {
        const promises = [];
        dto.questions.forEach((q: Question) => {
          q.assessmentId = assessment.id;
          delete q.id;
          promises.push(this.questionsRepository.save(q));
        });
        await Promise.all(promises);
      }

      await this.contentRepository.update(
        {
          assessment: { id: assessmentOld.id },
        },
        {
          assessment: { id: assessment.id },
        },
      );
    } else {
      if (dto.questions) {
        const promises = [];
        dto.questions.forEach(q => {
          promises.push(this.questionsService.update(q.id, q));
        });
        await Promise.all(promises);
      }

      assessment = await this.assessmentsRepository.save({
        lecture: {
          id: lectureId,
        },
        ...dto,
        level: {
          id: dto.levelId,
        },
      });
    }

    return [isNewVersion, assessment];
  }

  async createWithQuestion(createAssessmentDto?: CreateAssessmentForContentDto) {
    if (createAssessmentDto.questions) {
      const promises = [];
      createAssessmentDto.questions.forEach(q => {
        promises.push(this.questionsRepository.save(q));
      });
      await Promise.all(promises);
    }
    const assessment = await this.create(createAssessmentDto);
    return assessment;
  }

  async versionUpdate(dto: VersionUpdateDTO) {
    let promisesCreate = null;
    let promisesDelete = null;
    let promisesUpdate = null;
    const result: VersionUpdateDTOResult = {
      create: null,
      update: null,
      delete: null,
    };

    if (dto.create && dto.create.length) {
      promisesCreate = dto.create.reduce((a, e) => a.concat([this.createWithQuestion(e.assessment)]), []);
      result.create = await Promise.all(promisesCreate);
    }

    if (dto.delete && dto.delete.length) {
      promisesDelete = dto.delete.reduce((a, e) => a.concat([this.remove('' + e)]), []);
      result.delete = await Promise.all(promisesDelete);
    }

    if (dto.update && dto.update.length) {
      promisesUpdate = dto.update.reduce((a, e) => a.concat([this.updateAssessment(e.assessment)]), []);
      result.update = await Promise.all(promisesUpdate);
    }

    return result;
  }

  async surveyReport(id: string, query: QueryAssessmentVersionListDto) {
    const assessment = await this.findOne(id);
    const questionUsed = await this.submissionRepository
      .createQueryBuilder('submission')
      .select('COUNT( 1 )*100 / sub.cx as procent, answers.question_id')
      .leftJoin('submission.answers', 'answers')
      .leftJoin(
        qb =>
          qb
            .select('count(1) as cx')
            .from(Submission, 'submissionsub')
            .where(
              'submissionsub.assessment_id = :id AND submissionsub.status = :status AND submissionsub.course_id = :courseId AND submissionsub.lecture_id = :lectureId ',
              {
                id,
                status: 'graded',
                courseId: query.courseId,
                lectureId: query.lectureId,
              },
            ),
        'sub',
        '1=1',
      )
      .where(
        'submission.assessment_id = :id AND submission.status = :status AND submission.course_id = :courseId AND submission.lecture_id = :lectureId ',
        {
          id,
          status: 'graded',
          courseId: query.courseId,
          lectureId: query.lectureId,
        },
      )
      .groupBy('answers.question_id')
      .addGroupBy('sub.cx')
      .getRawMany();

    const totalStats = await this.submissionRepository
      .createQueryBuilder('submission')
      .where('submission.assessment_id = :id AND submission.course_id = :courseId AND submission.lecture_id = :lectureId ', {
        id,
        courseId: query.courseId,
        lectureId: query.lectureId,
      })
      .select([
        'count(submission.id) as submission_cx',
        'COUNT ( DISTINCT submission."user_id" ) AS uniq_user_cx',
        "SUM( CASE WHEN submission.status = 'graded' THEN submission.score ELSE 0 END ) / \"sum\" ( CASE WHEN submission.status = 'graded' THEN 1 ELSE 0 END ) AS avg_graded_score",
        "SUM( CASE WHEN submission.status = 'graded' THEN 1 ELSE 0 END ) AS graded_cx",
        "SUM( CASE WHEN submission.status = 'submitted' THEN 1 ELSE 0 END ) AS submitted_cx",
        "SUM( CASE WHEN submission.status = 'new' THEN 1 ELSE 0 END ) AS new_cx",
      ])
      .getRawMany();

    // TODO to query builder
    const answers = await this.submissionRepository.query(
      `
      SELECT
      n.question_id,
      n.answer_id,
      n."value",
      n.cx,
      kk.cx as total,
      n.cx*100/kk.cx as percent
      from
      (
      select
      k.question_id,
      k.answer_id,
      k."value",
      count(1) as cx
      from (
      SELECT T
        .ID,
        t.user_id,
        A.question_id,
        x.id as answer_id,
        x.value

      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_record(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
        T.assessment_id = $1
        AND T.course_id = $2
        AND T.lecture_id = $3
        AND T.status='graded'
        AND A.DATA IS NOT NULL
        AND jsonb_typeof(a.data)='object'
        AND T.deleted IS NULL

      UNION

      SELECT T
        .ID,
        t.user_id,
        A.question_id,
        x.id as answer_id,
        x.value

      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_recordset(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
        T.assessment_id = $1
        AND T.course_id = $2
        AND T.lecture_id = $3
        AND T.status='graded'
        AND A.DATA IS NOT NULL
        AND T.deleted IS NULL
        AND jsonb_typeof(a.data)='array') as k

        GROUP BY k.question_id, k.answer_id, k."value"
        ) as n
        left join
        (
        select count(aa.question_id) as cx, aa.question_id
        from submission as t
        left join answer as aa
        on t.id=aa.submission_id
        where
        T.assessment_id = $1
        AND T.course_id = $2
        AND T.lecture_id = $3
        AND T.status='graded'
        AND T.deleted IS NULL
          GROUP BY aa.question_id
        ) as kk

        on kk.question_id=n.question_id
        ORDER BY n.question_id
    `,
      [id, query.courseId, query.lectureId],
    );

    return {
      assessment,
      answers,
      totalStats,
      questionUsed,
    };
  }

  async surveyReportFile(id: string, query: QueryAssessmentVersionListDto) {
    const assessment = await this.findOne(id);
    const questions = assessment.questions.sort((a, b) => a.sort - b.sort);

    const questionsMap = questions.reduce((a, e) => {
      e.data.answers.forEach(k => (a[e.id + k.id] = k));
      return a;
    }, {});

    const result = await this.submissionRepository
      .createQueryBuilder('submission')
      .leftJoinAndSelect('submission.answers', 'answers')
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.course', 'course')
      .leftJoinAndSelect('submission.lecture', 'lecture')
      .innerJoinAndSelect('submission.user', 'user')
      .where(
        `submission.assessment_id = :id AND submission.course_id = :courseId AND submission.lecture_id = :lectureId AND submission.status = 'graded'`,
        {
          id,
          courseId: query.courseId,
          lectureId: query.lectureId,
        },
      )
      .getMany();

    const resultJSON = result.map(e => {
      const result = {
        submissionId: e.id,
        submissionCreated: e.created,
        submissionStatus: e.status,

        userId: e.user.id,
        userName: e.user.name,

        courseName: e.course.title,
        lectureName: e.lecture.title,
      };

      const answers = questions.reduce((acc, q, index) => {
        const ans = e.answers.find(a => a.questionId === q.id);
        let text = '';
        if (ans) {
          if (q.type === QuestionType.MultiChoice) {
            text = (<AnswerDataI[]>ans.data).reduce((a, e) => {
              if (a) {
              }
              a += e.value;
              return a;
            }, '');
          } else {
            text = (<AnswerDataI>ans.data).isUserInput
              ? (<AnswerDataI>ans.data).userInput
              : questionsMap[q.id + (<AnswerDataI>ans.data).id].value;
          }
        }
        acc[`question${index + 1}`] = text;
        return acc;
      }, {});

      return { ...result, ...answers };
    });

    const fields = [
      'submissionId',
      'submissionCreated',
      'submissionStatus',

      'userId',
      'userName',

      'courseName',
      'lectureName',
      ...questions.reduce((a, e, i) => a.concat(`question${i + 1}`), []),
    ];

    try {
      const parser = new json2csv.Parser({ fields });
      return parser.parse(resultJSON);
    } catch (err) {
      console.error(err);
    }
  }

  async surveyWebinarReport(id: string, query: QueryAssessmentVersionWebinarListDto) {
    const assessment = await this.findOne(id);

    const questionUsed = await this.submissionRepository
      .createQueryBuilder('submission')
      .select('COUNT( 1 )*100 / sub.cx as procent, answers.question_id')
      .leftJoin('submission.answers', 'answers')
      .leftJoin(
        qb =>
          qb
            .select('count(1) as cx')
            .from(Submission, 'submissionsub')
            .where('submissionsub.assessment_id = :id AND submissionsub.status = :status', {
              id,
              status: 'graded',
            }),
        'sub',
        '1=1',
      )
      .where('submission.assessment_id = :id AND submission.status = :status', {
        id,
        status: 'graded',
      })
      .groupBy('answers.question_id')
      .addGroupBy('sub.cx')
      .getRawMany();

    const totalStats = await this.submissionRepository
      .createQueryBuilder('submission')
      .where('submission.assessment_id = :id AND submission.webinar_id = :webinarId', {
        id,
        webinarId: query.webinarId,
      })
      .select([
        'count(submission.id) as submission_cx',
        'COUNT ( DISTINCT submission."user_id" ) AS uniq_user_cx',
        "AVG( submission.score ) FILTER( WHERE submission.status = 'graded' ) AS avg_graded_score",
        "SUM( CASE WHEN submission.status = 'graded' THEN 1 ELSE 0 END ) AS graded_cx",
        "SUM( CASE WHEN submission.status = 'submitted' THEN 1 ELSE 0 END ) AS submitted_cx",
        "SUM( CASE WHEN submission.status = 'new' THEN 1 ELSE 0 END ) AS new_cx",
      ])
      .getRawMany();

    // TODO to query builder
    const answers = await this.submissionRepository.query(
      `
      SELECT
        n.question_id,
        n.answer_id,
        n."value",
        n.cx,
        kk.cx AS total,
        n.cx * 100 / kk.cx AS percent
      FROM
        (
        SELECT K
          .question_id,
          K.answer_id,
          K."value",
          COUNT ( 1 ) AS cx
        FROM
          (
          SELECT
            T.ID,
            T.user_id,
            A.question_id,
            x.ID AS answer_id,
            x.VALUE

          FROM
            submission
            AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
            jsonb_to_record ( A.DATA ) AS x ( ID VARCHAR, VALUE VARCHAR, "isUserInput" bool, "userInput" VARCHAR )
          WHERE
            T.assessment_id = $1
            AND T.webinar_id = $2
            AND T.status = 'graded'
            AND A.DATA IS NOT NULL
            AND T.deleted IS NULL
            AND jsonb_typeof ( A.DATA ) = 'object' UNION
          SELECT
            T.ID,
            T.user_id,
            A.question_id,
            x.ID AS answer_id,
            x.VALUE

          FROM
            submission
            AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
            jsonb_to_recordset ( A.DATA ) AS x ( ID VARCHAR, VALUE VARCHAR, "isUserInput" bool, "userInput" VARCHAR )
          WHERE
            T.assessment_id = $1
            AND T.webinar_id = $2
            AND T.status = 'graded'
            AND A.DATA IS NOT NULL
            AND T.deleted IS NULL
            AND jsonb_typeof ( A.DATA ) = 'array'
          ) AS K
        GROUP BY
          K.question_id,
          K.answer_id,
          K."value"
        ) AS n
        LEFT JOIN (
        SELECT COUNT
          ( aa.question_id ) AS cx,
          aa.question_id
        FROM
          submission
          AS T LEFT JOIN answer AS aa ON T.ID = aa.submission_id
        WHERE
          T.assessment_id = $1
          AND T.webinar_id = $2
          AND T.status = 'graded'
          AND T.deleted IS NULL
        GROUP BY
          aa.question_id
        ) AS kk ON kk.question_id = n.question_id
      ORDER BY
        n.question_id
    `,
      [id, query.webinarId],
    );

    return {
      assessment,
      answers,
      totalStats,
      questionUsed,
    };
  }

  async surveyWebinarReportFile(id: string, query: QueryAssessmentVersionWebinarListDto) {
    const assessment = await this.findOne(id);
    const questions = assessment.questions.sort((a, b) => a.sort - b.sort);

    const questionsMap = questions.reduce((a, e) => {
      e.data.answers.forEach((k, i) => (a[e.id + k.id] = { ...k, sort: i }));
      return a;
    }, {});

    const result = await this.submissionRepository
      .createQueryBuilder('submission')
      .leftJoinAndSelect('submission.answers', 'answers')
      .leftJoinAndSelect('answers.files', 'files')
      .leftJoinAndSelect('submission.assessment', 'assessment')
      .leftJoinAndSelect('submission.webinar', 'webinar')
      .innerJoinAndSelect('submission.user', 'user')
      .where(`submission.assessment_id = :id AND submission.webinar_id = :webinarId AND submission.status = 'graded'`, {
        id,
        webinarId: query.webinarId,
      })
      .getMany();

    const resultJSON = result.map(e => {
      const result = {
        submissionId: e.id,
        submissionCreated: e.created,
        submissionStatus: e.status,

        userId: e.user.id,
        userName: e.user.name,
        email: e.user.email,

        webinarName: e.webinar.name,
      };

      const answers = questions.reduce((acc, q) => {
        const ans = e.answers.find(a => a.questionId === q.id);
        let text = '';
        if (ans) {
          if (q.type === QuestionType.MultiChoice) {
            text = (<AnswerDataI[]>ans.data)
              .sort((a, b) => questionsMap[q.id + (<AnswerDataI>a).id].sort - questionsMap[q.id + (<AnswerDataI>b).id].sort)
              .reduce((a, e) => {
                if (a) {
                  a += ', ';
                }
                a += e.userInput ? `${e.value}: ${e.userInput}` : e.value;
                return a;
              }, '');
          } else if (q.type === QuestionType.Upload) {
            text = ans.files.reduce((a, e) => {
              if (a) {
                a += ', ';
              }
              a += e.url;
              return a;
            }, '');
            let userInput = '';
            if (!Array.isArray(ans.data) && ans.data.userInput) {
              userInput = ans.data.userInput;
            }
            text = userInput !== '' ? `${userInput}, ${text}` : text;
          } else if (q.type === QuestionType.Rate) {
            text = (<AnswerDataI>ans.data).title;
          } else {
            text = (<AnswerDataI>ans.data).isUserInput
              ? convert((<AnswerDataI>ans.data).userInput)
              : convert(questionsMap[q.id + (<AnswerDataI>ans.data).id].value);
          }
        }
        acc[q.title] = text;
        return acc;
      }, {});

      return { ...result, ...answers };
    });

    const fields = [
      'submissionId',
      'submissionCreated',
      'submissionStatus',

      'userId',
      'userName',
      'email',

      'webinarName',
      ...questions.reduce((a, question) => a.concat(`${question.title}`), []),
    ];

    try {
      const parser = new json2csv.Parser({ fields });
      return parser.parse(resultJSON);
    } catch (err) {
      console.error(err);
    }
  }

  async surveyUserInputResult(_assessmentId: string, _query: QueryAssessmentUserInputDto, _paging?: PaginationDto) {
    return;
  }

  async surveyReportUserInput(id: string, query: QueryAssessmentUserInputDto, paging: PaginationDto) {
    const sql1 = `
    SELECT
      n.question_id,
      n.answer_id,
      n."value",
      n."userInput",
      n.cx,
      kk.cx as total,
      n.cx*100/kk.cx as percent
      from
      (
      select
      k.question_id,
      k.answer_id,
      k."value",
      k."userInput",
      count(1) as cx
      from (
      SELECT T
        .ID,
        t.user_id,
        --a.id,
        A.question_id,
        --A.DATA,
        x.id as answer_id,
        x.value,
        x."userInput"


      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_record(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
        T.assessment_id = $1
        AND T.course_id = $2
        AND T.lecture_id = $3
        AND A.question_id = $4
        AND x.id=$5
        AND T.status='graded'
        AND A.DATA IS NOT NULL
        AND T.deleted IS NULL
        and jsonb_typeof(a.data)='object'

      UNION

      SELECT T
        .ID,
        t.user_id,
        --a.id,
        A.question_id,
        --A.DATA,
        x.id as answer_id,
        x.value,
        x."userInput"

      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_recordset(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
      T.assessment_id = $1
      AND T.course_id = $2
      AND T.lecture_id = $3
      AND A.question_id = $4
      AND x.id=$5
      AND T.status='graded'
      AND A.DATA IS NOT NULL
      AND T.deleted IS NULL

        and jsonb_typeof(a.data)='array') as k

        GROUP BY k.question_id, k.answer_id, k."value", k."userInput"
        ) as n
        left join
        (
        select count(aa.question_id) as cx, aa.question_id
        from submission as t
        left join answer as aa
        on t.id=aa.submission_id
        where
        T.assessment_id = $1
        AND t.deleted IS NULL
          GROUP BY aa.question_id
        ) as kk

        on kk.question_id=n.question_id
        ORDER BY n.cx DESC
        offset $6
        limit $7
  `;

    const sql2 = `
    SELECT
      count(1) as total
      from
      (
      select
      k.question_id,
      k.answer_id,
      k."value",
      k."userInput",
      count(1) as cx
      from (
      SELECT T
        .ID,
        t.user_id,
        A.question_id,
        x.id as answer_id,
        x.value,
        x."userInput"

      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_record(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
        T.assessment_id = $1
        AND T.course_id = $2
        AND T.lecture_id = $3
        AND A.question_id = $4
        AND x.id=$5
        AND T.status='graded'
        AND A.DATA IS NOT NULL
        AND T.deleted IS NULL
        and jsonb_typeof(a.data)='object'

      UNION

      SELECT T
        .ID,
        t.user_id,
        A.question_id,
        x.id as answer_id,
        x.value,
        x."userInput"

      FROM
        submission
        AS T LEFT JOIN answer AS A ON A.submission_id = T.ID,
        jsonb_to_recordset(a.data) as  x(id VARCHAR, value VARCHAR,"isUserInput" bool, "userInput" VARCHAR)

      WHERE
      T.assessment_id = $1
      AND T.course_id = $2
      AND T.lecture_id = $3
      AND A.question_id = $4
      AND x.id=$5
      AND T.status='graded'
      AND A.DATA IS NOT NULL
      AND T.deleted IS NULL

        and jsonb_typeof(a.data)='array') as k



        GROUP BY k.question_id, k.answer_id, k."value", k."userInput"
        ) as n
      `;

    const [results, total] = await Promise.all([
      this.submissionRepository.query(sql1, [
        id,
        query.courseId,
        query.lectureId,
        query.questionId,
        query.answerId,
        (paging.page - 1) * paging.itemsPerPage,
        paging.itemsPerPage,
      ]),
      this.submissionRepository.query(sql2, [id, query.courseId, query.lectureId, query.questionId, query.answerId]),
    ]);

    return {
      results,
      meta: { ...paging, total: +total[0].total },
    };
  }

  async findAllSurveyList(query: QuerySurveyCourseListDto, paging: PaginationDto): Promise<Paginated<QuerySurveyTableResultDTO[]>> {
    let queryBuilder = this.contentRepository
      .createQueryBuilder('content')
      .innerJoin('content.assessment', 'assessment')
      .select([
        'content.id',
        'assessment.id',
        'assessment.commonId',
        'assessment.title',
        'assessment.isGlobal',
        'course.id',
        'course.title',
        'course.version',
        'lecture.id',
        'lecture.title',
      ])
      .innerJoin('content.lecture', 'lecture')
      .innerJoin('lecture.course', 'course')
      .loadRelationCountAndMap('assessment.questionsCount', 'assessment.questions')
      .groupBy(
        'content.id, assessment.id, assessment.commonId, assessment.title, assessment.isGlobal, course.id, course.title, course.version, lecture.id, lecture.title',
      )
      .andWhere('assessment.isWebinarSurvey = FALSE')
      .andWhere("assessment.type = 'survey'")
      .andWhere('assessment.current_version = TRUE');

    if (query.course) {
      queryBuilder = queryBuilder.andWhere('course.title ILIKE :course', {
        course: `%${query.course}%`,
      });
    }

    if (query.lecture) {
      queryBuilder = queryBuilder.andWhere('lecture.title ILIKE :lecture', {
        lecture: `%${query.lecture}%`,
      });
    }

    if (query.survey) {
      queryBuilder = queryBuilder.andWhere('assessment.title ILIKE :survey', {
        survey: `%${query.survey}%`,
      });
    }
    // if (query.globalSurvey && query.webinarSurvey) {
    //   queryBuilder = queryBuilder.andWhere('assessment.isGlobal = TRUE');
    // } else if (query.globalSurvey) {
    //   queryBuilder = queryBuilder.andWhere(
    //     'assessment.isGlobal = TRUE AND assessment.isWebinarSurvey = FALSE',
    //   );
    // } else if (query.webinarSurvey) {
    //   queryBuilder = queryBuilder.andWhere(
    //     'assessment.isGlobal = TRUE AND assessment.isWebinarSurvey = TRUE',
    //   );
    // }

    queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
    queryBuilder.take(paging.itemsPerPage);

    if (query.sortField) {
      queryBuilder.orderBy(query.sortField, query.sortDirection);
    }

    const [resultsORM, total] = await Promise.all([queryBuilder.getMany(), queryBuilder.getCount()]);

    const results = <QuerySurveyTableResultDTO[]>(<unknown>resultsORM);

    if (!results.length) {
      return {
        results: [],
        meta: { ...paging, total },
      };
    }

    const sub = this.assessmentsRepository
      .createQueryBuilder('assessment')
      .select([
        'count(DISTINCT(submission.id)) as submission_count',
        'assessment.commonId as commonId',
        'submission.course_id as courseId',
        'submission.lecture_id as lectureId',
      ])
      .leftJoin(Submission, 'submission', 'submission.assessment_id = assessment.id')
      .where('assessment.commonId IN (:...ids)', {
        ids: results.map(e => e.assessment.commonId),
      })
      .groupBy('assessment.commonId')
      .addGroupBy('submission.course_id')
      .addGroupBy('submission.lecture_id');
    const results2 = await sub.getRawMany();
    const submissions = results2.reduce((a, e) => ((a[e.commonid + e.courseid + e.lectureid] = e.submission_count), a), {});
    const r = results.map(e => {
      e.submissionsCount = submissions[e.assessment.commonId + e.lecture.course.id + e.lecture.id];
      return e;
    });
    return {
      results: r,
      meta: { ...paging, total },
    };
  }

  async findAllWebinarSurveyList(
    query: QuerySurveyWebinarListDto,
    paging: PaginationDto,
  ): Promise<Paginated<QueryWebinarSurveyTableResultDTO[]>> {
    let queryBuilder = this.assessmentsRepository
      .createQueryBuilder('assessment')
      .select(['assessment.id', 'assessment.commonId', 'assessment.title'])
      .loadRelationCountAndMap('assessment.questionsCount', 'assessment.questions')
      .groupBy('assessment.id, assessment.commonId, assessment.title, assessment.isGlobal')
      .andWhere('assessment.isWebinarSurvey = TRUE')
      .andWhere("assessment.type = 'survey'")
      .andWhere('assessment.current_version = TRUE');

    if (query.survey) {
      queryBuilder = queryBuilder.andWhere('assessment.title ILIKE :survey', {
        survey: `%${query.survey}%`,
      });
    }

    const [resultsORM, total2] = await Promise.all([queryBuilder.getMany(), queryBuilder.getCount()]);

    const results = <QueryWebinarSurveyTableResultDTO[]>(<unknown>resultsORM);

    if (!results.length) {
      return {
        results: [],
        meta: { ...paging, total: total2 },
      };
    }

    let sub = this.submissionRepository
      .createQueryBuilder('submission')
      .leftJoin(Assessment, 'assessment', 'assessment.id = submission.assessment_id')
      .select([
        'count(DISTINCT(submission.id)) as submission_count',
        `count(DISTINCT(submission.id)) FILTER (WHERE submission.status = :status) as submission_graded_count`,
        'assessment.commonId as commonId',
        'submission.webinarId as webinarId',
        'webinar.name as webinarTitle',
      ])
      .leftJoin(Webinar, 'webinar', 'webinar.id = submission.webinarId')
      .where('assessment.commonId IN (:...ids)', {
        ids: results.map(e => e.commonId),
        status: SubmissionStatus.Graded,
      })
      .andWhere('submission.webinarId is not null')
      .groupBy('assessment.commonId')
      .addGroupBy('submission.webinarId')
      .addGroupBy('webinar.name');

    if (query.webinar) {
      sub = sub.andWhere('webinar.name ILIKE :webinar', {
        webinar: `%${query.webinar}%`,
      });
    }

    sub.skip((paging.page - 1) * paging.itemsPerPage);
    sub.take(paging.itemsPerPage);

    if (query.sortField) {
      sub.orderBy(query.sortField, query.sortDirection);
    }

    //const results2 = await sub.getRawMany();
    const [results2, total] = await Promise.all([sub.getRawMany(), sub.getCount()]);

    const assessments = results.reduce((a, e) => ((a[e.commonId] = e), a), {});
    const r = results2.map(e => {
      return {
        ...e,
        ...assessments[e.commonid],
      };
    });
    return {
      results: r,
      meta: { ...paging, total },
    };
  }

  async getList(query: QuerySurveyListDto, paging?: PaginationDto): Promise<Paginated<QuerySurveyListResult[]>> {
    let queryBuilder = this.assessmentsRepository
      .createQueryBuilder('assessment')
      .select([
        'assessment.id',
        'assessment.title',
        'assessment.isGlobal',
        'assessment.commonId',
        'assessment.isWebinarSurvey',
        'course.id',
        'course.title',
        'lecture.id',
        'lecture.title',
        'module.id',
        'module.title',
      ])
      .leftJoinAndSelect('assessment.lecture', 'lecture')
      .leftJoinAndSelect('lecture.course', 'course')
      .leftJoinAndSelect('lecture.module', 'module');

    if (query.questionCount) {
      queryBuilder = queryBuilder.loadRelationCountAndMap('assessment.questionsCount', 'assessment.questions');
    }

    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('course.id = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.moduleId) {
      queryBuilder = queryBuilder.andWhere('module.id = :moduleId', {
        moduleId: query.moduleId,
      });
    }

    if (query.survey) {
      queryBuilder = queryBuilder.andWhere('assessment.title ILIKE :survey', {
        survey: `%${query.survey}%`,
      });
    }

    if (query.webinarSurvey && query.webinarId) {
      // page: webinars/{id}, tab: Survey. Select assessment [global] and [local by webinar_id]
      queryBuilder = queryBuilder.andWhere(
        '( (assessment.isGlobal = TRUE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = TRUE)' +
          ' OR (assessment.isGlobal = FALSE AND assessment.current_version = TRUE AND assessment.webinar_id = :webinarId) )',
        {
          webinarId: query.webinarId,
        },
      );
    } else if (query.globalSurvey && query.webinarSurvey) {
      // page: surveys/webinars, Selected [global] type of surveys
      queryBuilder = queryBuilder.andWhere(
        'assessment.isGlobal = TRUE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = TRUE',
      );
    } else if (query.globalSurvey !== undefined && !query.globalSurvey && query.webinarSurvey) {
      // page: surveys/webinars, Selected [local] type of surveys
      queryBuilder = queryBuilder.andWhere(
        'assessment.isGlobal = FALSE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = TRUE',
      );
    } else if (query.globalSurvey) {
      // page: /surveys/global
      queryBuilder = queryBuilder.andWhere(
        'assessment.isGlobal = TRUE AND assessment.current_version = TRUE AND assessment.isWebinarSurvey = FALSE',
      );
    } else if (query.webinarSurvey) {
      // page: surveys/webinars, [not selected] any type of surveys
      queryBuilder = queryBuilder.andWhere('assessment.current_version = TRUE AND assessment.isWebinarSurvey = TRUE');
    }

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (query.sortField) {
      queryBuilder.orderBy(query.sortField, query.sortDirection);
    }

    const [resultsORM, total] = await queryBuilder.getManyAndCount();

    const results = <QuerySurveyListResult[]>(<unknown>resultsORM);

    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findVersions(id: string, query: QueryAssessmentVersionListDto): Promise<QuerySurveyVersionsDTO[]> {
    const assessment = await this.findOne(id);
    const assessments = await this.assessmentsRepository.find({
      where: {
        commonId: assessment.commonId,
      },
      order: {
        currentVersion: 'DESC',
        created: 'ASC',
      },
    });
    const assessmentIds = assessments.map(a => a.id);

    let queryBuilder = this.submissionRepository
      .createQueryBuilder('submission')
      .select(['count(1) as count', 'assessment.id'])
      .innerJoin('submission.assessment', 'assessment')
      .groupBy('assessment.id')
      .where('assessment.id = ANY (:assessmentIds)', { assessmentIds })
      .orderBy('assessment.currentVersion', 'DESC')
      .addOrderBy('assessment.created', 'DESC');

    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('submission.course_id = :courseId', {
        courseId: query.courseId,
      });
    }

    if (query.lectureId) {
      queryBuilder = queryBuilder.andWhere('submission.lecture_id = :lectureId', {
        lectureId: query.lectureId,
      });
    }

    const count = await queryBuilder.getRawMany();

    return assessments.map((a: Assessment) => {
      const c = count.find(c => c.assessment_id === a.id) || { count: 0 };
      return {
        id: a.id,
        title: a.title,
        commonId: a.commonId,
        isGlobal: a.isGlobal,
        isWebinarSurvey: a.isWebinarSurvey,
        submissionsCount: c.count,
        created: a.created,
        modified: a.modified,
      } as QuerySurveyVersionsDTO;
    });
  }
}
