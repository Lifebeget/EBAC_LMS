import { CRUD } from 'src/lib/interfaces/CRUD';
import { Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe, Req, ForbiddenException, Query } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { QuestionsService } from './questions.service';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';
import { PgConstraints } from '../pg.decorators';
import { Question } from '../lib/entities/question.entity';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { AssessmentsService } from './assessments.service';
import { QuerySurveyAnswersDto } from './dto/query-survey-answers.dto';

@ApiTags('questions')
@ApiBearerAuth()
@Controller('')
export class QuestionsController implements CRUD {
  constructor(private readonly questionsService: QuestionsService, private readonly assessmentsService: AssessmentsService) {}

  @Post('assessments/:assessmentId/questions')
  @RequirePermissions(Permission.COURSE_CREATE)
  @ApiOperation({ summary: 'Create a new question for assessment' })
  @ApiResponse({
    status: 201,
    description: 'Created question',
    type: Question,
  })
  @PgConstraints()
  create(@Param('assessmentId', new ParseUUIDPipe()) assessmentId: string, @Body() createQuestionDto: CreateQuestionDto) {
    return this.questionsService.create({ assessmentId, ...createQuestionDto });
  }

  @Get('assessments/:assessmentId/questions')
  @ApiOperation({ summary: 'List all questions for assessment' })
  @ApiResponse({
    status: 200,
    type: [Question],
  })
  async findAll(@Param('assessmentId', new ParseUUIDPipe()) assessmentId: string, @Req() req: Express.Request) {
    const hasAccess = await this.userHasAccessToTheCourse(req.user, assessmentId);
    if (!hasAccess) {
      throw new ForbiddenException('ERRORS.ASSESSMENTS_QUESTIONS_FORBIDDEN');
    }
    const result = await this.questionsService.findForAssessment(assessmentId);
    if (!req.user.hasPermission(Permission.COURSE_EDIT)) {
      for (const question of result) {
        if (<any>question?.data && (<any>question?.data)?.answers) {
          for (const answer of (<any>question?.data)?.answers) {
            delete answer.isCorrect;
          }
        }
      }
    }
    return result;
  }

  async userHasAccessToTheCourse(user: Express.User, assessmentId: string) {
    if (user.permissions.includes(Permission.COURSE_VIEW)) {
      return true;
    }
    const assessment = await this.assessmentsService.findOne(assessmentId);
    return (
      user.enrollments.some(e => e.courseId == assessment.lecture.course.id) ||
      (assessment.lecture.free &&
        assessment.lecture.active &&
        assessment.lecture.published &&
        assessment.lecture.module.active &&
        assessment.lecture.module.published &&
        assessment.lecture.course.active &&
        assessment.lecture.course.published)
    );
  }

  @Get('questions/:id')
  @ApiOperation({ summary: 'Get one question by id' })
  @ApiResponse({
    status: 200,
    description: 'Get a question by id',
    type: Question,
  })
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Express.Request) {
    const result = await this.questionsService.findOne(id);
    const hasAccess = await this.userHasAccessToTheCourse(req.user, result.assessment.id);
    if (!hasAccess) {
      throw new ForbiddenException('ERRORS.QUESTION_FORBIDDEN');
    }
    return result;
  }

  @Patch('questions/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Updates a question' })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateQuestionDto: UpdateQuestionDto) {
    return this.questionsService.update(id, updateQuestionDto);
  }

  @Delete('questions/:id')
  @RequirePermissions(Permission.COURSE_EDIT)
  @ApiOperation({ summary: 'Deletes a question' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.questionsService.remove(id);
  }

  @Get('surveys/RateQuestionResults')
  surveyRateQuestionResults(@Query() query: QuerySurveyAnswersDto) {
    return this.questionsService.surveyRateQuestionResults(query);
  }
}
