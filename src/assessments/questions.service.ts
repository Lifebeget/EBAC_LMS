import { CRUD } from 'src/lib/interfaces/CRUD';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Question } from '@entities/question.entity';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';
import { QuerySurveyAnswersDto } from './dto/query-survey-answers.dto';
import { AssessmentCriterion } from '@entities/assessment-criterion.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateLectureContentCbaDto } from '../criteria-based-assessments/dto/create-lecture-content-cba.dto';

@Injectable()
export class QuestionsService implements CRUD {
  constructor(
    @InjectRepository(Question)
    private questionsRepository: Repository<Question>,
    @InjectRepository(AssessmentCriterion)
    private cbaRepository: Repository<AssessmentCriterion>,
  ) {}
  findAll(query: any, ...params: any): Promise<any> {
    throw new Error('Method not implemented.');
  }

  @Transactional()
  async create(createQuestionDto: CreateQuestionDto) {
    const createdQuestion = await this.questionsRepository.save({
      ...createQuestionDto,
      assessment: { id: createQuestionDto.assessmentId },
    });
    if (!createdQuestion) {
      throw new BadRequestException('Question not created');
    }
    if (createQuestionDto.assessmentCriteria) {
      const cbaEntity = createQuestionDto.assessmentCriteria.map(criterion => {
        return {
          questionId: createdQuestion.id,
          ...criterion,
        };
      });
      await this.cbaRepository.save(cbaEntity);
    }
    return createdQuestion;
  }

  findForAssessment(assessmentId: string): Promise<Question[]> {
    return this.questionsRepository
      .createQueryBuilder('question')
      .leftJoinAndSelect('question.assessmentCriteria', 'assessmentCriteria', 'question.id = assessmentCriteria.questionId')
      .where('assessment_id = :assessmentId', { assessmentId })
      .getMany();
  }

  @Transactional()
  findOne(id: string): Promise<Question> {
    return this.questionsRepository.findOne(id, {
      relations: ['assessment', 'assessmentCriteria'],
    });
  }

  @Transactional()
  async update(id: string, updateQuestionDto: UpdateQuestionDto) {
    if ('assessmentCriteria' in updateQuestionDto) {
      await this.updateQuestionCBA(id, updateQuestionDto.assessmentCriteria);
      delete updateQuestionDto.assessmentCriteria;
    }
    const updateFields = { ...updateQuestionDto, id };
    return this.questionsRepository.save(updateFields);
  }

  async updateQuestionCBA(questionId: string, cba: CreateLectureContentCbaDto[]) {
    const cbaEntity = cba?.map(criterion => {
      return {
        questionId,
        ...criterion,
      };
    });
    const cbaIds = cbaEntity.filter(cba => cba.id).map(cba => cba.id);
    // Удалить лишние CBA, которые не входят в новый список
    const qb = this.cbaRepository
      .createQueryBuilder('assessment_criterion')
      .delete()
      .where('assessment_criterion.question_id = :questionId', { questionId });

    if (cbaIds.length > 0) {
      qb.andWhere('assessment_criterion.id NOT IN (:...cbaIds)', { cbaIds });
    }
    await qb.execute();

    await this.cbaRepository.save(cbaEntity);
  }

  remove(id: string) {
    return this.questionsRepository.delete(id);
  }

  async clear() {
    const questions = await this.questionsRepository.find();

    await Promise.all(questions.map(q => this.remove(q.id)));
  }

  purgeAll() {
    return this.questionsRepository.clear();
  }

  surveyRateQuestionResults(query: QuerySurveyAnswersDto) {
    return this.questionsRepository
      .createQueryBuilder('question')
      .where('question.id = :questionId', { questionId: query.questionId })
      .getOne();
  }
}
