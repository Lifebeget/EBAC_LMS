import { QueryAttemptsDto } from './dto/query-attempts.dto';
import { Attempt } from '@entities/attempt.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAttemptDto, CreateOneAdditionalAttemptDto } from './dto/create-attempt.dto';
import { HaveAttemptsDto } from './dto/have-attempts.dto';
import { Assessment } from '@entities/assessment.entity';
import { Submission } from '@entities/submission.entity';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AttemptsService {
  constructor(
    @InjectRepository(Attempt)
    private attemptsRepository: Repository<Attempt>,
    @InjectRepository(Assessment)
    private assessmentsRepository: Repository<Assessment>,
    @InjectRepository(Submission)
    private submissionsRepository: Repository<Submission>,
    private usersService: UsersService,
  ) {}

  async create(createAttemptDto: CreateAttemptDto) {
    const { userId, courseId, lectureId, assessmentId, additionalAttempts } = createAttemptDto;

    const result = await this.attemptsRepository.save({
      user: { id: userId },
      course: { id: courseId },
      lecture: { id: lectureId },
      assessment: { id: assessmentId },
      additionalAttempts,
    });

    return result;
  }

  async createOneAdditionalAttempt(createOneAdditionalAttemptDto: CreateOneAdditionalAttemptDto) {
    await this.checkConsistent(createOneAdditionalAttemptDto)
    
    const { userId, assessmentId } = createOneAdditionalAttemptDto;

    const haveAttempts = await this.haveAttempts({ userId, assessmentId });
    if (haveAttempts) {
      throw new BadRequestException('The user still have attempts');
    }

    return await this.create({ ...createOneAdditionalAttemptDto, additionalAttempts: 1 });
  }

  async haveAttempts(dto: HaveAttemptsDto): Promise<boolean> {
    const submissionsCount = await this.submissionsRepository.count({
      assessment: { id: dto.assessmentId },
      user: { id: dto.userId },
      isQuestion: false,
    });

    const { maxAttempts } = await this.assessmentsRepository.findOne(dto.assessmentId);

    const additionalAttempts = await this.attemptsRepository
      .createQueryBuilder()
      .select('sum(additional_attempts)')
      .where('assessment_id = :assessmentId', { assessmentId: dto.assessmentId })
      .groupBy('assessment_id')
      .getRawOne();

    const additionalAttemptsSum = additionalAttempts?.sum || 0;

    return submissionsCount < maxAttempts + parseInt(additionalAttemptsSum);
  }

  async findAll(query: QueryAttemptsDto) {
    const qb = this.attemptsRepository.createQueryBuilder();

    if (query.userId) {
      qb.andWhere('user_id = :userId', { userId: query.userId });
    }

    if (query.assessmentId) {
      qb.andWhere('assessment_id = :assessmentId', { assessmentId: query.assessmentId });
    }

    return await qb.getMany();
  }

  async remove(id: string) {
    return await this.attemptsRepository.delete(id);
  }

  async checkConsistent(createOneAdditionalAttemptDto: CreateOneAdditionalAttemptDto) {
    const { userId, assessmentId, lectureId, courseId } = createOneAdditionalAttemptDto;

    const user = await this.usersService.findOne(userId);

    if (!user) {
      throw new BadRequestException('User is not found');
    }

    const assessment = await this.assessmentsRepository.findOne(assessmentId, {
      relations: ['lecture'],
    });

    if (!assessment) {
      throw new BadRequestException('Assessment is not found');
    }

    if (assessment.lecture.id !== lectureId) {
      throw new BadRequestException('Assessment don\'t belong to the lecture');
    }

    if (assessment.lecture.courseId !== courseId) {
      throw new BadRequestException('Lecture don\'t belong to the course');
    }

    if (!user.enrollments.some(e => e.courseId === courseId)) {
      throw new BadRequestException('The user is not enrolled for the course');
    }
  }
}
