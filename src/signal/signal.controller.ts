import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { SubmissionsService } from 'src/assessments/submissions.service';
import { Public } from 'src/auth/public.decorator';
import { InterappGuard } from 'src/auth/interapp.guard';
import * as ffs from 'fs';
import * as assert from 'assert';
const fs = ffs.promises;

@Controller('')
export class SignalController {
  constructor(private readonly submissionsService: SubmissionsService) {}

  @Public()
  @Post('signal/import-finish')
  @UseGuards(InterappGuard)
  async importFinish(@Body() _body: { challenge: string }) {
    await this.submissionsService.tutorsPauseIfIdle();
    return this.submissionsService.distribute();
  }

  @Public()
  @Post('signal/log-population-finish')
  @UseGuards(InterappGuard)
  async logPopulationFinished(@Body() _body: { challenge: string }) {
    assert(process.env.LOG_FILE && process.env.LOG_FILE_POPULATION, 'Population is disabled on this server');
    await fs.unlink(process.env.LOG_FILE_POPULATION).catch(() => undefined);
    return true;
  }

  // route for application logging
  @Public()
  @Post('signal/save-log')
  @UseGuards(InterappGuard)
  async saveLog() {
    return true;
  }
}
