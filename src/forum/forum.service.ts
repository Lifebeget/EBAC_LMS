import { ForumComplaints } from '@entities/forum-complaints.entity';
import { ForumLikes } from '@entities/forum-likes.entity';
import { ForumMessagesLock } from '@entities/forum-messages-lock.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { Course } from '@entities/course.entity';
import { ForumSubscribe } from '@entities/forum-subscribe.entity';
import { ForumViewed } from '@entities/forum-viewed.entity';
import { User } from '@entities/user.entity';
import { NotificationType } from '@enums/notification-type.enum';
import { ResolveState } from '@enums/resolve-type.enum';
import { CourseRole } from '@lib/types/enums/course-role.enum';
import { SystemRole } from '@lib/types/enums';
import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { convert } from 'html-to-text';
import { LecturesService } from 'src/courses/lectures.service';
import { FilesService } from 'src/files/files.service';
import { NotificationsService } from 'src/notifications/notifications.service';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { UsersService } from 'src/users/users.service';
import { Connection, getConnection, getManager, Repository, SelectQueryBuilder } from 'typeorm';
import { IsolationLevel, Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateComplaintDto } from './dto/create-forum-complaint.dto';
import { CreateMessageImportDto } from './dto/create-forum-message-import.dto';
import { CreateMessageDto } from './dto/create-forum-message.dto';
import { ForumMessageSummaryDto } from './dto/forum-summary.dto';
import { LockMessageDto } from './dto/lock-forum-message.dto';
import { QueryForumComplaintsDto } from './dto/query-forum-complaints.dto';
import { QueryForumMessagesDto, QueryForumMessagesForAdminPanelDto } from './dto/query-forum-messages.dto';
import { QueryForumThreadsDto } from './dto/query-forum-threads.dto';
import { QueryUnresolvedForumMessagesDto } from './dto/query-unresolved-forum-messages.dto';
import { ResolveMessageDto } from './dto/resolve-forum-message.dto';
import { UpdateMessageDto } from './dto/update-forum-message.dto';
import { SettingsService } from 'src/settings/settings.service';
import {
  selectForumMessage,
  selectUser,
  selectAvatar,
  selectLecture,
  selectCourse,
  filterCreatePost,
  replaceSubstituteUser,
  filterSticky,
} from './forum.helpers';
import { DateService } from 'src/utils/date.service';
import { TariffsService } from 'src/directory/service-level/tariffs.service';
import { ResponsibleTag } from '@lib/types/enums/responsible-option.enum';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { ForumTriggerService } from 'src/trigger/services/forum-trigger.service';
import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { ViewForumThread } from 'src/lib/views/view-forum-thread.entity';
import { Brackets } from 'typeorm';

const forumAdminRoles = [CourseRole.Support, CourseRole.Tutor, CourseRole.Moderator, CourseRole.Teamlead];

@Injectable()
export class ForumService {
  private userEBACOnline: User;
  private userEBACOnlineUpdateTime: number;
  private readonly urgentHours: number;
  private readonly outdatedHours: number;
  constructor(
    @InjectConnection()
    private readonly connection: Connection,

    @InjectRepository(ForumMessages)
    private readonly forumMessagesRepository: Repository<ForumMessages>,

    @InjectRepository(ForumMessagesLock)
    private readonly forumMessagesLockRepository: Repository<ForumMessagesLock>,

    @InjectRepository(ForumLikes)
    private readonly forumLikesRepository: Repository<ForumLikes>,

    @InjectRepository(ForumSubscribe)
    private readonly forumSubscribeRepository: Repository<ForumSubscribe>,

    @InjectRepository(ForumViewed)
    private readonly forumViewedRepository: Repository<ForumViewed>,

    @InjectRepository(ForumComplaints)
    private readonly forumComplaintsRepository: Repository<ForumComplaints>,

    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,

    private readonly notificationsService: NotificationsService,
    private readonly filesService: FilesService,
    private readonly usersService: UsersService,
    private readonly lecturesService: LecturesService,
    private readonly tariffService: TariffsService,
    private readonly dateService: DateService,
    configService: ConfigService,
    private socketsGateway: SocketsGateway,
    private userService: UsersService,
    private enrollmentsService: EnrollmentsService,
    private readonly settingsService: SettingsService,
    private readonly forumTrigger: ForumTriggerService,
  ) {
    this.urgentHours = configService.get<number>('forumResolve.urgentHours');
    this.outdatedHours = configService.get<number>('forumResolve.outdatedHours');
  }

  resolvedUser(user, lecture) {
    const hasEnrollment =
      user.isFrontendManager() ||
      user.enrolledAsSupport(lecture.courseId) ||
      user.enrolledAsTutor(lecture.courseId) ||
      user.enrolledAsTeamlead(lecture.courseId);

    return hasEnrollment;
  }

  async sendSocket(message) {
    const users = await this.usersService.findByRole(SystemRole.Support, message.courseId);
    for (let index = 0; index < users.length; index++) {
      const user = users[index];
      this.socketsGateway.dataUpdateForUser('newUnresolvedForumMessage', user.id);
    }
  }

  async getUserEBACOnline() {
    if (this.userEBACOnlineUpdateTime && this.userEBACOnline && new Date().getTime() - this.userEBACOnlineUpdateTime < 5 * 60000) {
      return this.userEBACOnline;
    }
    const settings = await this.settingsService.getSettings();
    if (!settings.mainSettings.forumEbacUserEmail) {
      console.error('forum user email EMPTY at settings!');
      return null;
    }

    this.userEBACOnline = await this.usersService.findOneByEmail(settings.mainSettings.forumEbacUserEmail);
    this.userEBACOnlineUpdateTime = new Date().getTime();

    return this.userEBACOnline;
  }

  @Transactional()
  async create(dto: CreateMessageDto, user: User) {
    const [text, author, lecture, course] = await Promise.all([
      await this.filesService.replaceAndUploadBase64ImgS3(dto.body),
      await this.usersService.findOne(user.id),
      await this.lecturesService.findOne(dto.lectureId, true),
      await this.coursesRepository.findOne(dto.courseId),
    ]);

    const entity = {
      id: dto.id,
      externalId: dto.externalId,
      title: dto.title,
      body: text,
      score: dto.score,
      parent: dto.parentId && { id: dto.parentId },
      threadParent: null,
      hidden: false,
      sticky: dto.sticky,
      lecture: lecture || (dto.lectureId && { id: dto.lectureId, title: '--hidden' }),
      course,
      user: { id: user.id, name: user.name },
    } as ForumMessages;

    entity.skipResolve = dto.isModerator;

    if (dto.substituteUser) {
      entity.substituteUser = await this.getUserEBACOnline();
    }

    let messageParent, subscribes;

    if (dto.parentId) {
      [messageParent, subscribes] = await Promise.all([
        this.forumMessagesRepository
          .createQueryBuilder('forumMessage')
          .leftJoinAndSelect('forumMessage.threadParent', 'threadParent')
          .where('forumMessage.id = :messageId', {
            messageId: dto.parentId,
          })
          .getOne(),
        this.forumSubscribeRepository
          .createQueryBuilder('forumSubscribe')
          .select(['forumSubscribe.userId as userId'])
          .distinctOn(['user_id'])
          .where('forumSubscribe.thread = :id', {
            id: dto.parentId,
          })
          .andWhere('forumSubscribe.userId <> :user', {
            user: user.id,
          })
          .execute(),
      ]);
      entity.threadParentId = messageParent?.threadParent || messageParent?.id;
    }

    const createdAt = new Date();
    const createTariff = await this.tariffService.getTariffForForum(createdAt);

    const tariff = {
      createTariff,
      resolveTariff: createTariff,
      urgentDate: await this.dateService.addBusinessHours(createdAt, createTariff.urgentHours),
      outdatedDate: await this.dateService.addBusinessHours(createdAt, createTariff.outdatedHours),
    };

    const message = await this.forumMessagesRepository.save({
      ...entity,
      tariff,
    });

    this.forumTrigger.messageSent(message);

    if (!entity.skipResolve) {
      this.sendSocket(message);
    }

    const values = subscribes?.map(e => ({
      title: '',
      body: '',
      notificationType: NotificationType.ForumNewReply,
      notificationData: {
        forumNewReply: {
          message,
          messageParent,
          author: author.id,
          authorName: author.name,
          lecture: lecture.id,
          lectureTitle: lecture.title,
          course: course.id,
          courseTitle: course.title,
        },
      },
      recipient: { id: e.userid },
    }));

    const promises: Promise<any>[] = [
      this.forumSubscribeRepository.save({
        message: { id: message.id },
        user: { id: user.id },
        thread: { id: entity.threadParent?.id || message.id },
      }),
      this.forumViewedRepository.save({
        message: { id: message.id },
        user: { id: user.id },
      }),
    ];
    if (values) {
      promises.push(this.notificationsService.createBulk(values));
    }

    await Promise.all(promises);

    return filterCreatePost(message);
  }

  @Transactional()
  updateViewDate(res, userId: string): Promise<any[]> {
    const promises = [];
    res.forEach(q => {
      promises.push(
        this.forumViewedRepository.save({
          message: { id: q.id },
          user: { id: userId },
          modified: dayjs(),
        }),
      );
    });
    return Promise.all(promises);
  }

  async findAll(paging: PaginationDto, query: QueryForumMessagesDto, userId: string): Promise<Paginated<ForumMessages[]>> {
    let queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .leftJoinAndSelect('forumMessage.lecture', 'lecture')
      .leftJoinAndSelect('forumMessage.course', 'course')
      .leftJoinAndSelect('forumMessage.user', 'users')
      .leftJoinAndSelect('forumMessage.substituteUser', 'substituteUser')
      .leftJoinAndSelect('substituteUser.avatar', 'substituteUserAvatar')
      .leftJoinAndSelect('forumMessage.tariff', 'tariff')
      .leftJoinAndSelect('forum_messages', 'thread', 'thread.id = forumMessage.threadParentId')
      .leftJoinAndSelect('users.avatar', 'avatar')
      .setParameters({ userId })
      .leftJoinAndMapOne(
        'forumMessage.viewed',
        ForumViewed,
        'forumViewed',
        'forumViewed.message_id = forumMessage.id and forumViewed.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.like',
        ForumLikes,
        'forumLikes',
        'forumLikes.message_id = forumMessage.id and forumLikes.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.subscribe',
        ForumSubscribe,
        'forumSubscribe',
        'forumSubscribe.message_id = forumMessage.id and forumSubscribe.user=:userId',
      )
      .leftJoinAndSelect(
        'users.enrollments',
        'enrollments',
        'enrollments.courseId = forumMessage.courseId AND enrollments.user = forumMessage.user AND enrollments.active=TRUE AND enrollments.role IN (:...roles) AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW()) AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())',
      )
      .setParameter('roles', forumAdminRoles);
    if (query.ids) {
      queryBuilder.whereInIds(query.ids);
    }

    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('forumMessage.course = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.lectureId) {
      queryBuilder = queryBuilder.andWhere('forumMessage.lecture = :lectureId', { lectureId: query.lectureId });
    }
    if (query.userId) {
      queryBuilder = queryBuilder.andWhere('forumMessage.user = :userIdFilter', {
        userIdFilter: query.userId,
      });
    }
    if (query.sticky) {
      queryBuilder = queryBuilder.andWhere('forumMessage.sticky = :sticky', {
        sticky: query.sticky,
      });
    }
    if (query.parent) {
      queryBuilder = queryBuilder.andWhere('forumMessage.parent = :parent', {
        parent: query.parent,
      });
    }
    if (query.threadParent) {
      queryBuilder = queryBuilder.andWhere('forumMessage.threadParent = :threadParent', { threadParent: query.threadParent });
    }

    if (query.loadAllFromCurrentPostion) {
      queryBuilder.offset(paging.page * paging.itemsPerPage);
    } else if (!paging.showAll) {
      queryBuilder.offset((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.limit(paging.itemsPerPage);
    }

    queryBuilder.addOrderBy('forumMessage.sticky', 'DESC');
    queryBuilder.addOrderBy('forumMessage.created', 'ASC');

    const [res, total] = await queryBuilder.getManyAndCount();

    const threads = Array.from(new Set(res.map(e => e.threadParentId)));

    let authorThreadMap;
    if (query.loadAuthor) {
      const authorThread = await this.forumMessagesRepository
        .createQueryBuilder('forumMessage')
        .leftJoin('forumMessage.user', 'user')
        .select(['forumMessage.id as id', 'user.name as name', 'user.email as email'])
        .whereInIds(threads)
        .execute();

      authorThreadMap = new Map(authorThread.map(item => [item.id, item]));
    }

    const [usersCount, ...users3avatar] = await Promise.all([
      this.forumMessagesRepository
        .createQueryBuilder('forumMessage')
        .select(['forumMessage.threadParent', 'count( distinct forumMessage.user)'])
        .groupBy('forumMessage.threadParent')
        .whereInIds(threads)
        .execute(),

      ...threads.map(e =>
        getConnection()
          .createQueryBuilder()
          .setParameter('id', e)
          .from(
            qb =>
              qb
                .select(['forumMessage2.userId as userId'])
                .from(ForumMessages, 'forumMessage2')
                .distinct(true)
                .where('forumMessage2.threadParent = :id', { id: e }),
            'x',
          )
          .leftJoinAndSelect(User, 'user1', 'user1.id=x.userid')
          .leftJoinAndSelect('user1.avatar', 'file')
          .select(['user1.id as id', 'file.url as url', 'user1.name as name'])
          .orderBy('RANDOM()')
          .limit(3)
          .execute(),
      ),
    ]);

    const usersCountMap = new Map(usersCount.map(e => [e.thread_parent_id, e.count]));
    const users3avatarMap = new Map(users3avatar.map((e, i) => [threads[i], e]));

    this.updateViewDate(res, userId);

    const results = res
      .map(e => ({
        ...e,
        userAnsweredCount: +usersCountMap.get(e.threadParentId),
        userParticipants: users3avatarMap.get((<any>e).threadParentId),
        isUrgent: dayjs().isBetween(dayjs(e.tariff.urgentDate), dayjs(e.tariff.outdatedDate)),
        isOutdated: dayjs().isAfter(dayjs(e.tariff.outdatedDate)),
        threadOutdatedDate: e.tariff.outdatedDate,
        authorThread: authorThreadMap ? authorThreadMap.get(e.threadParentId) : null,
      }))
      .map(e => {
        try {
          e.body = convert((e.body || '').replace(/<img src="(.*?)">/gi, '<IMAGE>').replace(/\n/gi, '. ')).slice(0, 1000);
        } catch (error) {
          console.error(error);
        }
        return e;
      });

    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findAllThreads(
    paging: PaginationDto,
    query: QueryForumThreadsDto,
    userId: string,
    isOnlyStudent: boolean,
  ): Promise<Paginated<any>> {
    // Query for Thread
    const subThreadIds = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .select('forumMessage.id as id')
      .where('forumMessage.threadParent is null');
    if (query.courseId) {
      subThreadIds.andWhere('forumMessage.course = :courseId');
    }
    if (query.lectureId) {
      subThreadIds.andWhere('forumMessage.lecture = :lectureId');
    }
    if (query.threadId) {
      subThreadIds.andWhere('forumMessage.id = :threadId');
    }
    if (query.pipeline) {
      subThreadIds.andWhere('forumMessage.pipeline = :pipeline');
    }
    subThreadIds.addOrderBy('forumMessage.sticky', 'DESC');


    if (!query.sort || query.sort === 1 || query.sort === 2) {
      // sort by thread date
      subThreadIds.addOrderBy('forumMessage.created', 'DESC');
    } else if (query.sort === 3) {
      // sort by like amount
      subThreadIds.addOrderBy('forumMessage.score', 'DESC');
    } else if (query.sort === 4) {
      // sort by answer date
      subThreadIds.leftJoin(
        qb => {
          qb.addSelect('max(forumMessagesTime.created) as time55')
            .addSelect('forumMessagesTime.threadParent as parent55')
            .from(ForumMessages, 'forumMessagesTime')
            .groupBy('forumMessagesTime.threadParent')
            .where('forumMessagesTime.threadParent is not null');
          if (query.courseId) {
            qb.andWhere('forumMessagesTime.course = :courseId');
          }
          if (query.lectureId) {
            qb.andWhere('forumMessagesTime.lecture = :lectureId');
          }
          if (query.threadId) {
            qb.andWhere('forumMessagesTime.id = :threadId');
          }
          return qb;
        },
        'answertime',
        'forumMessage.id=answertime.parent55',
      );
      subThreadIds.addSelect('COALESCE(time55, "forumMessage".created) as sort55');

      subThreadIds.addOrderBy('sort55', 'DESC');
    }

    if (!paging.showAll) {
      subThreadIds.offset((paging.page - 1) * paging.itemsPerPage);
      subThreadIds.limit(paging.itemsPerPage);
    }

    subThreadIds
      .setParameter('lectureId', query.lectureId)
      .setParameter('threadId', query.threadId)
      .setParameter('courseId', query.courseId)
      .setParameter('pipeline', query.pipeline);

    const total = await subThreadIds.getCount();
    if (!total) {
      return {
        results: [],
        meta: { ...paging, total },
      };
    }

    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .select([
        ...selectForumMessage(),
        ...selectUser(),
        ...selectAvatar(),
        ...selectUser('substituteUser'),
        ...selectAvatar('substituteUserAvatar'),
        ...selectLecture(),
        ...selectCourse(),
      ])
      .leftJoin('forumMessage.lecture', 'lecture')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoin('forumMessage.course', 'course')
      .leftJoin('forumMessage.user', 'users')
      .leftJoin('users.avatar', 'avatar')
      .leftJoin('forumMessage.substituteUser', 'substituteUser')
      .leftJoin('substituteUser.avatar', 'substituteUserAvatar')
      .leftJoinAndMapOne(
        'forumMessage.like',
        ForumLikes,
        'forumLikes',
        'forumLikes.message_id = forumMessage.id and forumLikes.user=:userId',
        { userId },
      )
      .leftJoinAndMapOne(
        'forumMessage.subscribe',
        ForumSubscribe,
        'forumSubscribe',
        'forumSubscribe.message_id = forumMessage.id and forumSubscribe.user=:userId',
        { userId },
      )
      .leftJoinAndMapOne(
        'forumMessage.viewed',
        ForumViewed,
        'forumViewed',
        'forumViewed.message_id = forumMessage.id and forumViewed.user=:userId',
        { userId },
      )
      .innerJoin('(' + subThreadIds.getQuery() + ')', 'sorted', 'sorted.id = "forumMessage".id')
      .leftJoinAndSelect(
        'users.enrollments',
        'enrollments',
        'enrollments.courseId = forumMessage.courseId AND enrollments.user = forumMessage.user AND enrollments.active=TRUE AND enrollments.role IN (:...roles) AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW()) AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())',
      )
      .leftJoinAndSelect('forumMessage.tariff', 'tariff')
      .setParameter('roles', forumAdminRoles)
      .setParameter('lectureId', query.lectureId)
      .setParameter('threadId', query.threadId)
      .setParameter('courseId', query.courseId)
      .addOrderBy('forumMessage.sticky', 'DESC');

    if (!query.sort || query.sort === 1 || query.sort === 2) {
      queryBuilder.addOrderBy('forumMessage.created', 'DESC');
    } else if (query.sort === 3) {
      queryBuilder.addOrderBy('forumMessage.score', 'DESC');
    } else if (query.sort === 4) {
      queryBuilder.addOrderBy('sorted.sort55', 'DESC');
    }

    const res = await queryBuilder.getMany();
    const threads = res.map(e => e.id);

    const ranks = await this.forumMessagesRepository.query(
      `
        select id, thread_parent_id as thread, rank  from
        (SELECT forum_messages.*,
          rank() OVER (PARTITION BY thread_parent_id ORDER BY sticky DESC, created ASC)
          FROM forum_messages
          WHERE forum_messages.thread_parent_id in ('${threads.join("','")}')
          AND forum_messages.deleted IS NULL
          ) ranked_scores
        where rank <=$1;
      `,
      [+query.answerCount || 3],
    );
    // Query for Answer
    const answerRecords = await this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .select([
        ...selectForumMessage(),
        ...selectUser(),
        ...selectAvatar(),
        ...selectUser('substituteUser'),
        ...selectAvatar('substituteUserAvatar'),
      ])
      .whereInIds(ranks.map(e => e.id))
      .leftJoin('forumMessage.user', 'users')
      .leftJoin('users.avatar', 'avatar')
      .leftJoin('forumMessage.substituteUser', 'substituteUser')
      .leftJoin('substituteUser.avatar', 'substituteUserAvatar')
      .setParameter('userId', userId)
      .leftJoinAndMapOne(
        'forumMessage.like',
        ForumLikes,
        'forumLikes',
        'forumLikes.message_id = forumMessage.id and forumLikes.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.subscribe',
        ForumSubscribe,
        'forumSubscribe',
        'forumSubscribe.message_id = forumMessage.id and forumSubscribe.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.viewed',
        ForumViewed,
        'forumViewed',
        'forumViewed.message_id = forumMessage.id and forumViewed.user=:userId',
      )
      .leftJoinAndSelect(
        'users.enrollments',
        'enrollments',
        'enrollments.courseId = forumMessage.courseId AND enrollments.user = forumMessage.user AND enrollments.active=TRUE AND enrollments.role IN (:...roles) AND (enrollments.date_to IS NULL OR enrollments.date_to > NOW()) AND (enrollments.date_from IS NULL OR enrollments.date_from <= NOW())',
      )
      .setParameter('roles', forumAdminRoles)
      .getMany();

    const answerRecordsMap = new Map(answerRecords.map(e => [e.id, e]));
    const answers = ranks.reduce((a, e) => {
      if (!a.has(e.thread)) a.set(e.thread, []);
      const answer = answerRecordsMap.get(e.id);
      if (answer) a.get(e.thread).splice(+e.rank - 1, 0, answer);
      return a;
    }, new Map());

    const [counters, ...users3avatar] = await Promise.all([
      this.forumMessagesRepository
        .createQueryBuilder('forumMessage')
        .select(['forumMessage.threadParent', 'count( distinct forumMessage.user) as users', 'count( forumMessage.id) as answers'])
        .groupBy('forumMessage.threadParent')
        .where('forumMessage.threadParent in (:...threads)', { threads })
        .execute(),

      ...threads.map(e =>
        getConnection()
          .createQueryBuilder()
          .setParameter('id', e)
          .from(
            qb =>
              qb
                .select(['forumMessage2.userId as userId'])
                .from(ForumMessages, 'forumMessage2')
                .distinct(true)
                .where('forumMessage2.threadParent = :id'),
            'x',
          )
          .leftJoinAndSelect(User, 'user1', 'user1.id=x.userid')
          .leftJoinAndSelect('user1.avatar', 'file')
          .select(['user1.id as id', 'file.url as url', 'user1.name as name'])
          .orderBy('RANDOM()')
          .limit(3)
          .execute(),
      ),
    ]);

    const countersMap: Map<string, any> = new Map(
      counters.map(e => [
        e.thread_parent_id,
        {
          usersCount: e.users || 0,
          total: +e.answers || 0,
          page: 1,
          itemsPerPage: +query.answerCount || 3,
        },
      ]),
    );
    const users3avatarMap = new Map(users3avatar.map((e, i) => [threads[i], e]));

    this.updateViewDate(res, userId);

    let results = res.map(e => {
      const result = {
        ...e,
        userAnsweredCount: 0,
        answerPagination: {
          total: 0,
          page: 1,
          itemsPerPage: +query.answerCount || 3,
        },
        userParticipants: users3avatarMap.get((<any>e).id) || [],
        answers: answers.get(e.id) || [],
      };
      if (countersMap.has(e.id)) {
        const total = countersMap.get(e.id);
        result.userAnsweredCount = total.usersCount;
        result.answerPagination.total = total.total;
      }
      return result;
    });
    if (isOnlyStudent) {
      results = replaceSubstituteUser(results);
    }
    if (query.sort === 1) {
      results = filterSticky(results);
    }
    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findOneAndMarkRead(id: string, userId: string, isOnlyStudent: boolean): Promise<any> {
    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .select([
        ...selectForumMessage(),
        ...selectUser(),
        ...selectAvatar(),
        ...selectUser('substituteUser'),
        ...selectAvatar('substituteUserAvatar'),
        ...selectLecture(),
        ...selectCourse(),
        ...selectForumMessage('parent'),
        ...selectForumMessage('threadParent'),
      ])
      .leftJoin('forumMessage.lecture', 'lecture')
      .leftJoin('forumMessage.course', 'course')
      .leftJoin('forumMessage.user', 'users')
      .leftJoin('users.avatar', 'avatar')
      .leftJoin('forumMessage.substituteUser', 'substituteUser')
      .leftJoin('substituteUser.avatar', 'substituteUserAvatar')
      .leftJoin('forumMessage.parent', 'parent')
      .leftJoin('forumMessage.threadParent', 'threadParent')
      .setParameters({ userId })
      .leftJoinAndMapOne(
        'forumMessage.viewed',
        ForumViewed,
        'forumViewed',
        'forumViewed.message_id = forumMessage.id and forumViewed.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.like',
        ForumLikes,
        'forumLikes',
        'forumLikes.message_id = forumMessage.id and forumLikes.user=:userId',
      )
      .leftJoinAndMapOne(
        'forumMessage.subscribe',
        ForumSubscribe,
        'forumSubscribe',
        'forumSubscribe.message_id = forumMessage.id and forumSubscribe.user=:userId',
      )
      .andWhere('forumMessage.id = :id', {
        id,
      });

    let results = await queryBuilder.getOne();

    this.updateViewDate([results], userId);

    if (isOnlyStudent) {
      results = replaceSubstituteUser(results);
    }

    return results;
  }

  @Transactional()
  async findOne(id: string, includeLock = false, isOnlyStudent: boolean): Promise<ForumMessages> {
    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .select([
        ...selectForumMessage(),
        ...selectUser(),
        ...selectAvatar(),
        ...selectUser('substituteUser'),
        ...selectAvatar('substituteUserAvatar'),
        ...selectLecture(),
        ...selectCourse(),
        ...selectForumMessage('parent'),
        ...selectForumMessage('threadParent'),
      ])
      .leftJoin('forumMessage.lecture', 'lecture')
      .leftJoin('forumMessage.course', 'course')
      .leftJoin('forumMessage.user', 'users')
      .leftJoin('users.avatar', 'avatar')
      .leftJoin('forumMessage.substituteUser', 'substituteUser')
      .leftJoin('substituteUser.avatar', 'substituteUserAvatar')
      .leftJoin('forumMessage.parent', 'parent')
      .leftJoin('forumMessage.threadParent', 'threadParent')
      .andWhere('forumMessage.id = :id', {
        id,
      });

    if (includeLock) {
      queryBuilder.leftJoinAndSelect('forumMessage.lock', 'lock').leftJoinAndSelect('lock.user', 'responsibleUser');
    }

    let results = await queryBuilder.getOne();

    if (isOnlyStudent) {
      results = replaceSubstituteUser(results);
    }

    return results;
  }

  findOneByExternalId(externalId: string): Promise<ForumMessages> {
    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .leftJoinAndSelect('forumMessage.lecture', 'lecture')
      .leftJoinAndSelect('forumMessage.course', 'course')
      .leftJoinAndSelect('forumMessage.user', 'user')
      .leftJoinAndSelect('forumMessage.parent', 'parent')
      .leftJoinAndSelect('forumMessage.threadParent', 'threadParent')
      .andWhere('forumMessage.externalId = :externalId', {
        externalId,
      });

    return queryBuilder.getOne();
  }

  @Transactional()
  async update(id: string, updateMessageDto: UpdateMessageDto) {
    const updateFields: any = { ...updateMessageDto, id };
    if (updateFields.body) updateFields.body = await this.filesService.replaceAndUploadBase64ImgS3(updateFields.body);
    updateFields.edited = new Date();
    return this.forumMessagesRepository.save(updateFields);
  }

  remove(id: string) {
    return this.forumMessagesRepository.softDelete(id);
  }

  removePermanent(id: string) {
    return this.forumMessagesRepository.delete(id);
  }

  restore(id: string) {
    return this.forumMessagesRepository.restore(id);
  }

  @Transactional()
  async setLike(id: string, userId: string, like: number): Promise<any> {
    const before: any[] = await Promise.all([
      this.forumLikesRepository.findOne(
        { message: { id: id }, user: { id: userId } },
        {
          select: ['like'],
        },
      ),
      this.forumMessagesRepository.findOne({ id: id }, { select: ['score'] }),
    ]);

    if (before[0]?.like === like) return { score: before[1].score };

    const res = await Promise.all([
      this.forumLikesRepository
        .createQueryBuilder()
        .insert()
        .into(ForumLikes)
        .values({
          message: { id },
          like,
          user: { id: userId },
        })
        .onConflict(`("message_id", "user_id") DO UPDATE SET "like" = :like`)
        .setParameter('like', like)
        .execute(),
      this.forumMessagesRepository.save({
        id,
        score: before[1].score + like - (before[0]?.like || 0),
      }),
    ]);

    return { score: res[1].score, like: res[0] };
  }

  setHide(id: string, userId: string, hide: boolean): Promise<any> {
    const updateFields: Partial<ForumMessages> = { hidden: hide, id };
    return this.forumMessagesRepository.save(updateFields);
  }

  setSticky(id: string, sticky: boolean): Promise<any> {
    const updateFields: Partial<ForumMessages> = { sticky, id };
    return this.forumMessagesRepository.save(updateFields);
  }

  @Transactional()
  async setResolveThread(id: string, body: ResolveMessageDto, userId: string, message: ForumMessages): Promise<any> {
    if (!body.resolveThreadBeforeDate) {
      throw new BadRequestException('resolveThreadBeforeDate required');
    }
    const updateFields: Partial<ForumMessages> = {
      resolved: new Date(),
      resolvedUserId: userId,
      lock: null,
    };

    const messageId = message.id === id ? id : message.threadParentId;
    const result = await this.forumMessagesRepository
      .createQueryBuilder()
      .update(ForumMessages)
      .set(updateFields)
      .where(
        `(id = :id or thread_parent_id=:id)
        AND skipResolve = FALSE
        AND resolved IS NULL
        AND date_trunc('second', created) <= :beforeDate`,
        { id: messageId, beforeDate: body.resolveThreadBeforeDate },
      )
      .execute();

    if (message.lock) {
      await this.forumMessagesRepository.save({
        id: message.threadParentId || message.id,
        lock: null,
      });
      await this.forumMessagesLockRepository.delete(message.lock);
    }

    return result;
  }

  @Transactional()
  async setResolveOne(id: string, userId: string, message: ForumMessages): Promise<any> {
    const updateFields: Partial<ForumMessages> = {
      resolved: new Date(),
      resolvedUserId: userId,
      lock: null,
      id,
    };

    const result = await this.forumMessagesRepository.save(updateFields);
    if (message.lock) {
      await Promise.all([
        this.forumMessagesRepository.save({
          id: message.threadParentId || message.id,
          lock: null,
        }),
        this.forumMessagesLockRepository.delete(message.lock),
      ]);
    }
    return result;
  }

  @Transactional({ isolationLevel: IsolationLevel.READ_UNCOMMITTED })
  async unlockIfAllResolved(thread: ForumMessages): Promise<any> {
    const count = await this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .where('forumMessage.id = :threadId', {
        threadId: thread.id,
      })
      .andWhere(
        `
        forumMessage.skipResolve = FALSE
        AND forumMessage.resolved IS NULL
      `,
      )
      .getCount();

    if (!count) {
      await this.unlockThread(thread);
    }
  }

  async setResolve(id: string, body: ResolveMessageDto, userId: string, message: ForumMessages, thread: ForumMessages): Promise<any> {
    let result;
    if (!body.resolveThreadBeforeDate) {
      result = await this.setResolveOne(id, userId, message);
    } else {
      result = await this.setResolveThread(id, body, userId, message);
    }

    // снимаем лок если все прочитано в треде
    if (thread.lock) {
      await this.unlockIfAllResolved(thread);
    }
    return result;
  }

  subscribe(id: string, userId: string, threadParentId: string) {
    return this.forumSubscribeRepository
      .createQueryBuilder()
      .insert()
      .into(ForumSubscribe)
      .values({
        message: { id },
        user: { id: userId },
        thread: { id: threadParentId },
      })
      .onConflict(`("message_id", "user_id") DO NOTHING`)
      .execute();
  }

  unsubscribe(id: string, userId: string) {
    return this.forumSubscribeRepository.delete({
      message: {
        id,
      },
      user: {
        id: userId,
      },
    });
  }

  @Transactional()
  async createComplaint(id: string, dto: CreateComplaintDto, userId: string) {
    const complaint = await this.forumComplaintsRepository.save({
      ...dto,
      message: { id },
      user: { id: userId },
    });

    //TODO: add notification to moderator?

    return complaint;
  }

  async findAllComplaints(paging: PaginationDto, query: QueryForumComplaintsDto): Promise<Paginated<ForumComplaints[]>> {
    let queryBuilder = this.forumComplaintsRepository
      .createQueryBuilder('forumComplaints')
      .leftJoinAndSelect('forumComplaints.message', 'message')
      .leftJoinAndSelect('forumComplaints.user', 'user')
      .leftJoinAndSelect('message.lecture', 'lecture1')
      .leftJoinAndSelect('message.course', 'course')
      .leftJoinAndSelect('message.user', 'user2');

    if (query.courseId) {
      queryBuilder = queryBuilder.andWhere('message.course = :courseId', {
        courseId: query.courseId,
      });
    }
    if (query.active) {
      queryBuilder = queryBuilder.andWhere('forumComplaints.active = :active', {
        active: query.active,
      });
    }
    if (query.userId) {
      queryBuilder = queryBuilder.andWhere('forumComplaints.user = :userIdFilter', {
        userIdFilter: query.userId,
      });
    }
    if (query.messageId) {
      queryBuilder = queryBuilder.andWhere('forumComplaints.message = :messageId', {
        messageId: query.messageId,
      });
    }
    if (query.type) {
      queryBuilder = queryBuilder.andWhere('forumComplaints.complaint_type = :type', {
        type: query.type,
      });
    }

    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    queryBuilder.addOrderBy('forumComplaints.created', 'DESC');

    const [res, total] = await queryBuilder.getManyAndCount();

    return {
      results: res,
      meta: { ...paging, total },
    };
  }

  findOneComplaint(id: string): Promise<any> {
    const queryBuilder = this.forumComplaintsRepository
      .createQueryBuilder('forumComplaints')
      .leftJoinAndSelect('forumComplaints.message', 'message')
      .leftJoinAndSelect('forumComplaints.user', 'user')
      .leftJoinAndSelect('message.lecture', 'lecture1')
      .leftJoinAndSelect('message.course', 'course')
      .leftJoinAndSelect('message.user', 'user2')
      .andWhere('forumComplaints.id = :id', {
        id,
      });

    return queryBuilder.getOne();
  }

  setStatusComplaint(id: string, active: boolean): Promise<any> {
    return this.forumComplaintsRepository.save({ id, active });
  }

  removeComplaint(id: string) {
    return this.forumComplaintsRepository.delete(id);
  }

  findManyByExternalId(externalIds: string[]): Promise<ForumMessages[]> {
    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .leftJoinAndSelect('forumMessage.lecture', 'lecture')
      .leftJoinAndSelect('forumMessage.course', 'course')
      .leftJoinAndSelect('forumMessage.user', 'user')
      .leftJoinAndSelect('forumMessage.parent', 'parent')
      .leftJoinAndSelect('forumMessage.threadParent', 'threadParent')
      .where('forumMessage.externalId in (:...externalIds)', { externalIds });

    return queryBuilder.getMany();
  }

  @Transactional()
  async createImport(dto: CreateMessageImportDto, userId: string) {
    const entity = {
      id: dto.id,
      externalId: dto.externalId,
      title: dto.title,
      body: dto.body,
      score: dto.score,
      parent: dto.parentId && { id: dto.parentId },
      threadParent: null,
      hidden: false,
      sticky: dto.sticky,
      lecture: dto.lectureId && { id: dto.lectureId },
      course: { id: dto.courseId },
      user: { id: userId },
      created: dto.created,
      modified: dto.modified,
    } as unknown as ForumMessages;

    if (dto.parentId) {
      const messageParent = await this.forumMessagesRepository
        .createQueryBuilder('forumMessage')
        .leftJoinAndSelect('forumMessage.threadParent', 'threadParent')
        .where('forumMessage.id = :messageId', {
          messageId: dto.parentId,
        })
        .getOne();

      entity.threadParentId = messageParent?.threadParentId || messageParent?.id;
    }

    const message = await this.forumMessagesRepository.save(entity);

    const promises: Promise<any>[] = [
      this.forumSubscribeRepository.save({
        message: { id: message.id },
        user: { id: userId },
        thread: { id: entity.threadParent?.id || message.id },
      }),
      this.forumViewedRepository.save({
        message: { id: message.id },
        user: { id: userId },
      }),
    ];
    await Promise.all(promises);

    return message;
  }

  getMutexDate(): Date {
    const MUTEX_TIME = process.env.SUBMISSION_LOCK_SECONDS ?? '3600';
    const mutexTimeInt = parseInt(MUTEX_TIME, 10) * 1000;
    return new Date(Date.now() + mutexTimeInt);
  }

  findAllWithBase64Img() {
    return this.forumMessagesRepository
      .createQueryBuilder('forumMessages')
      .where('forumMessages.body like :b64', {
        b64: `%src="data:%`,
      })
      .select(['forumMessages.id', 'forumMessages.body'])
      .getMany();
  }

  async lockThread(id: string, body: LockMessageDto, userId: string, message: ForumMessages) {
    const mutex = body.user ? dayjs().add(72, 'hours') : this.getMutexDate();
    if (message.lock) {
      if (body.pipeline) {
        await this.forumMessagesRepository.save({
          id,
          pipeline: body.pipeline,
        });
      }
      const lock = await this.forumMessagesLockRepository.save({
        id: message.lock.id,
        user: { id: body.user || userId },
        mutex,
      });
      return {
        id,
        lock,
        pipeline: body.pipeline || message.pipeline,
      };
    }
    const lock = await this.forumMessagesLockRepository.save({
      user: { id: body.user || userId },
      mutex,
    });
    await this.sendSocket(message);

    const updateFields: Partial<ForumMessages> = {
      id,
      lock,
    };

    if (body.pipeline) {
      updateFields.pipeline = body.pipeline;
    }

    return this.forumMessagesRepository.save(updateFields);
  }

  async unlockThread(message: ForumMessages, pipeline?: ForumThreadPipeline) {
    const updateFields: Partial<ForumMessages> = {
      lock: null,
    };
    if (pipeline) {
      updateFields.pipeline = pipeline;
    }
    await this.forumMessagesRepository.update(
      {
        id: message.id,
      },
      updateFields,
    );
    await this.sendSocket(message);
    if (message.lock?.id) {
      return this.forumMessagesLockRepository.delete(message.lock.id);
    }
  }

  async addUnresolvedFilterByRole(
    user: User,
    asRole: SystemRole,
    resolveState: ResolveState,
    subquery: SelectQueryBuilder<ViewForumThread>,
    parameters: any,
    hasResponsibleFilter: boolean,
  ) {
    switch (asRole) {
      // курсы на которые он записан как саппорт
      case SystemRole.Support:
        const coursesEnrolledSupport = user.enrollments.filter(e => e.role == CourseRole.Support).map(e => e.courseId);

        if (coursesEnrolledSupport.length) {
          subquery.andWhere('thread.courseId IN (:...coursesEnrolledSupport)');
          parameters.coursesEnrolledSupport = coursesEnrolledSupport;
        } else {
          subquery.andWhere('thread.courseId IN (NULL)');
        }

        if (!hasResponsibleFilter) {
          subquery.andWhere('(lock IS NULL OR lock.mutex <= NOW() OR (lock.userId = :userId and lock.mutex > NOW()))');
          subquery.andWhere('(thread.pipeline = :pipeline OR (lock.userId = :userId and lock.mutex > NOW()))');
        }
        parameters.userId = user.id;
        parameters.pipeline = ForumThreadPipeline.support;

        break;

      // только личные
      case SystemRole.Tutor:
        const coursesEnrolledTutor = user.enrollments.filter(e => e.role == CourseRole.Tutor).map(e => e.courseId);

        if (resolveState == ResolveState.Today) {
          subquery.andWhere('thread.resolvedUserId = :userId'); // PROBLEM HERE
        } else {
          subquery.andWhere(`(
            (lock.userId = :userId AND lock.mutex > NOW())
            OR (thread.pipeline = :pipeline AND (lock IS NULL OR lock.mutex < NOW()))
          )`);
        }
        if (coursesEnrolledTutor.length) {
          subquery.andWhere('thread.courseId IN (:...coursesEnrolledTutor)');
          parameters.coursesEnrolledTutor = coursesEnrolledTutor;
        } else {
          subquery.andWhere('thread.courseId IN (NULL)');
        }
        parameters.userId = user.id;
        parameters.pipeline = ForumThreadPipeline.tutor;

        break;

      // все, что в воронке тьюторов
      case SystemRole.Teamlead:
        const coursesEnrolledTeamlead = user.enrollments.filter(e => e.role == CourseRole.Teamlead).map(e => e.courseId);

        if (coursesEnrolledTeamlead.length) {
          subquery.andWhere('thread.courseId IN (:...coursesEnrolledTeamlead)');
        } else {
          subquery.andWhere('thread.courseId IN (NULL)');
        }
        subquery.andWhere('thread.pipeline = :pipeline');
        parameters.pipeline = ForumThreadPipeline.tutor;
        parameters.coursesEnrolledTeamlead = coursesEnrolledTeamlead;
        break;

      // все
      case SystemRole.Manager:
        break;
    }
  }

  addSubqueryForResponsibles(query: QueryUnresolvedForumMessagesDto, subquery: SelectQueryBuilder<ViewForumThread>, parameters: any) {
    if (query.responsibleTag) {
      switch (query.responsibleTag) {
        case ResponsibleTag.NoResponsible:
          subquery.andWhere('(lock IS NULL OR lock.mutex <= NOW())');

          break;
        case ResponsibleTag.AnyResponsible:
          subquery.andWhere(`(
            (lock.mutex > NOW() OR pipeline = :responsiblePipeline)
          )`);
          parameters.responsiblePipeline = ForumThreadPipeline.tutor;

          break;
        case ResponsibleTag.AllTutors:
          subquery.andWhere('pipeline = :responsiblePipeline');
          parameters.responsiblePipeline = ForumThreadPipeline.tutor;

          break;
        case ResponsibleTag.AllSupportManagers:
          subquery.andWhere('pipeline = :responsiblePipeline').andWhere('lock.mutex > NOW()');
          parameters.responsiblePipeline = ForumThreadPipeline.support;

          break;
      }
    } else if (query.responsibleId) {
      subquery.andWhere('responsibleUser.id = :responsibleId').andWhere('lock.mutex > NOW()');

      parameters.responsibleId = query.responsibleId;
    }
    return [subquery, parameters];
  }

  async findAllUnresolvedThreads(paging: PaginationDto, query: QueryUnresolvedForumMessagesDto, user: User): Promise<Paginated<any>> {
    const [subquery, parameters] = await this.createQueryTemplate(query, paging, user);

    const newPosts = await subquery.getRawMany();

    const newPostsCount = await getConnection()
      .createQueryBuilder()
      .select(['count(1) as count'])
      .from('(' + subquery.getQuery() + ')', 'newpost')
      .setParameters(parameters)
      .getRawOne();

    if (newPosts.length === 0) {
      return {
        results: [],
        meta: { ...paging, total: 0 },
      };
    }

    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessage')
      .leftJoinAndSelect('forumMessage.lecture', 'lecture')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('forumMessage.course', 'course')
      .leftJoinAndSelect('forumMessage.user', 'users')
      .leftJoinAndSelect('users.avatar', 'avatar')
      .leftJoinAndSelect('forumMessage.substituteUser', 'substituteUser')
      .leftJoinAndSelect('substituteUser.avatar', 'substituteUserAvatar')
      .leftJoinAndSelect('forumMessage.lock', 'lock')
      .leftJoinAndSelect('lock.user', 'responsibleUser')
      .leftJoinAndSelect('responsibleUser.avatar', 'responsibleUserAvatar')
      .leftJoinAndSelect('forumMessage.tariff', 'tariff')
      .andWhere('forumMessage.id IN (:...ids)', {
        ids: newPosts.map(t => t.id),
      });

    const [res, total] = await queryBuilder.getManyAndCount();
    if (!total) {
      return {
        results: [],
        meta: { ...paging, total },
      };
    }

    const resMap = new Map(res.map(m => [m.id, m]));

    const results = newPosts
      .map(p => {
        const forumMessage = resMap.get(p.id);

        return {
          ...forumMessage,
          unresolvedDate: p.unresolved_date,
          replyCount: +p.replies,
          unresolvedCount: +p.unresolved_count,
          isUrgent: dayjs().isBetween(dayjs(p.urgent_date), dayjs(p.outdated_date)),
          isOutdated: dayjs().isAfter(dayjs(p.outdated_date)),
          threadOutdatedDate: p.outdated_date,
        };
      })
      .map(e => {
        try {
          e.body = convert((e.body || '').replace(/<img src="(.*?)">/gi, '<IMAGE>').replace(/\n/gi, '. ')).slice(0, 1000);
        } catch (error) {
          console.error(error);
        }
        return e;
      })
      .map(e => {
        try {
          if (e.lock && e.lock.mutex <= new Date()) {
            e.lock = null;
          }
        } catch (error) {
          console.error(error);
        }
        return e;
      })
      .sort((a, b) => dayjs(a.unresolvedDate).unix() - dayjs(b.unresolvedDate).unix());

    return {
      results,
      meta: { ...paging, total: +newPostsCount.count },
    };
  }

  async summaryPart1(query: QueryUnresolvedForumMessagesDto, user: User): Promise<any> {
    delete query.resolveState;

    const [subquery, parameters] = await this.createQueryTemplate(query, null, user);

    const queryBuilder = await getConnection()
      .createQueryBuilder()
      .select(`count(1) as allCount`)
      .addSelect(
        `SUM(
          CASE WHEN '${dayjs().toISOString()}' BETWEEN newpost.urgent_date AND newpost.outdated_date THEN 1
            ELSE 0
          END) as urgentCount
        `,
      )
      .addSelect(
        `SUM(
        CASE WHEN '${dayjs().toISOString()}' > newpost.outdated_date THEN 1
          ELSE 0
        END) as outdatedCount
      `,
      )
      .from('(' + subquery.getQuery() + ')', 'newpost')
      .setParameters(parameters);

    return queryBuilder.getRawOne();
  }

  async summaryPart2(query: QueryUnresolvedForumMessagesDto, user: User): Promise<any> {
    query.showResolved = true;
    query.resolveState = ResolveState.Today;

    const [queryBuilder, _parameters] = await this.createQueryTemplate(query, null, user);

    queryBuilder.select(`count(1) as todayCount`).groupBy();

    return queryBuilder.getRawOne();
  }

  async summary(query: QueryUnresolvedForumMessagesDto, user: User): Promise<ForumMessageSummaryDto> {
    const results = await Promise.all([this.summaryPart1(query, user), this.summaryPart2(query, user)]);

    return {
      allCount: +results[0].allcount,
      urgentCount: +results[0].urgentcount,
      outdatedCount: +results[0].outdatedcount,
      todayCount: +results[1].todaycount,
    };
  }

  async createQueryTemplate(
    query: QueryUnresolvedForumMessagesDto,
    paging?: PaginationDto,
    user?: User,
    forFilter = false,
    // eslint-disable-next-line @typescript-eslint/ban-types
  ): Promise<[SelectQueryBuilder<ViewForumThread>, any]> {
    const entityManager = getManager();
    const subquery = entityManager
      .createQueryBuilder()
      .addFrom(ViewForumThread, 'thread')
      .select([
        'thread.id as id',
        'thread.created as created',
        'thread.pipeline as pipeline',
        'thread.replies as replies',
        'thread.unresolved_count as unresolved_count',
        'thread.unresolved_date as unresolved_date',
        'thread.urgent_date as urgent_date',
        'thread.outdated_date as outdated_date',
      ])
      .leftJoin('thread.lock', 'lock')
      .leftJoin('lock.user', 'responsibleUser')
      .leftJoin('thread.course', 'course')
      .leftJoin('course.tags', 'tags') // Can return multiple results
      .leftJoin('thread.lecture', 'lecture')
      .leftJoin('lecture.module', 'module')
      .leftJoin('thread.user', 'user')
      .leftJoin('thread.resolvedUser', 'resolvedUser')
      .leftJoin('thread.substituteUser', 'substituteUser')
      .addGroupBy('thread.id')
      .addGroupBy('thread.created')
      .addGroupBy('thread.pipeline')
      .addGroupBy('thread.replies')
      .addGroupBy('thread.unresolved_count')
      .addGroupBy('thread.unresolved_date')
      .addGroupBy('thread.urgent_date')
      .addGroupBy('thread.outdated_date');

    const parameters: any = {
      userId: user.id,
    };

    if (!forFilter) {
      this.addSubqueryForResponsibles(query, subquery, parameters);
    }

    if (query.threadId) {
      query.showResolved = true;
      subquery.andWhere('thread.id = :threadId');
      parameters.threadId = query.threadId;
    }

    if (query.courseId) {
      subquery.andWhere('thread.courseId = :courseId');
      parameters.courseId = query.courseId;
    }

    if (query.courseIds && query.courseIds.length) {
      subquery.andWhere('thread.courseId IN (:...courseIds)');
      parameters.courseIds = query.courseIds;
    }

    if (query.moduleId) {
      subquery.andWhere('lecture.module = :moduleId');
      parameters.moduleId = query.moduleId;
    }

    if (query.lectureId) {
      subquery.andWhere('thread.lectureId = :lectureId');
      parameters.lectureId = query.lectureId;
    }

    if (query.userId) {
      subquery.andWhere('thread.userId = :userId');
      parameters.userId = query.userId;
    }

    if (query.userIds && query.userIds.length) {
      subquery.andWhere('thread.userId IN (:...userIds)');
      parameters.userIds = query.userIds;
    }

    if (query.tagIds && query.tagIds.length) {
      subquery.andWhere('tags.id IN (:...tagIds)');
      parameters.tagIds = query.tagIds;
    }

    switch (query.resolveState) {
      case ResolveState.Urgent:
        subquery.andWhere(`'${dayjs().toISOString()}' BETWEEN thread.urgent_date AND thread.outdated_date`);
        break;
      case ResolveState.Outdated:
        subquery.andWhere(`'${dayjs().toISOString()}' > thread.outdated_date`);
        break;

      case ResolveState.Today:
        subquery.andWhere('thread.lastResolved BETWEEN :dayStart AND :dayEnd');
        parameters.dayStart = dayjs().startOf('day').toDate();
        parameters.dayEnd = dayjs().endOf('day').toDate();
        query.showResolved = true;
        break;
    }

    if (query.createdStartDate) {
      subquery.andWhere('thread.lastCreated >= :createdStartDate');
      parameters.createdStartDate = query.createdStartDate;
    }

    if (query.createdEndDate) {
      subquery.andWhere('thread.created <= :createdEndDate');
      parameters.createdEndDate = query.createdEndDate;
    }

    if (query.resolvedStartDate) {
      query.showResolved = true;
      subquery.andWhere('thread.lastResolved >= :resolvedStartDate');
      parameters.resolvedStartDate = query.resolvedStartDate;
    }

    if (query.resolvedEndDate) {
      query.showResolved = true;
      subquery.andWhere('thread.resolved <= :resolvedEndDate');
      parameters.resolvedEndDate = query.resolvedEndDate;
    }

    if (!query.showResolved) {
      subquery.andWhere('thread.unresolved_count > 0');
    }

    if (query.searchString) {
      subquery.andWhere(`LOWER(CONCAT(user.name, ' ', course.title, ' ', module.title)) like :like`);
      parameters.like = `%${query.searchString}%`;
    }

    const hasResponsibleFilter = !!query.responsibleTag;

    await this.addUnresolvedFilterByRole(user, query.asRole, query.resolveState, subquery, parameters, hasResponsibleFilter);

    subquery.setParameters(parameters);

    return [subquery, parameters];
  }

  async getResponsiblesForFilter(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    const [queryBuilder] = await this.createQueryTemplate(query, null, user, true);
    queryBuilder.select('distinct responsibleUser.id as id, responsibleUser.name as name ').groupBy().orderBy('responsibleUser.name');

    const responsibleUsers = await queryBuilder.getRawMany();
    return responsibleUsers.filter(responsible => responsible.id);
  }

  async getCoursesForFilter(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    delete query.courseId;

    const [queryBuilder] = await this.createQueryTemplate(query, null, user);
    queryBuilder.select('distinct course.id as id, course.title as name ').groupBy().orderBy('course.title');

    return queryBuilder.getRawMany();
  }

  async getModulesForFilter(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    delete query.moduleId;

    const [queryBuilder] = await this.createQueryTemplate(query, null, user);
    queryBuilder.select('distinct module.id as id, module.title as name ').groupBy().orderBy('module.title');

    return queryBuilder.getRawMany();
  }

  async getStudentsForFilter(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    delete query.userIds;

    const [queryBuilder] = await this.createQueryTemplate(query, null, user);
    queryBuilder.select('distinct user.id as id, user.name as name ').groupBy().orderBy('user.name');

    return queryBuilder.getRawMany();
  }

  async getTagsForFilter(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    delete query.tagIds;

    const [queryBuilder] = await this.createQueryTemplate(query, null, user);
    queryBuilder.select('distinct tags.id as id, tags.title as name ').groupBy().andWhere('tags.id IS NOT NULL').orderBy('tags.title');

    return queryBuilder.getRawMany();
  }

  async getFilterData(query: QueryUnresolvedForumMessagesDto, user: Express.User) {
    const [responsibles, courses, modules, students, tags] = await Promise.all([
      this.getResponsiblesForFilter(query, user),
      this.getCoursesForFilter(query, user),
      this.getModulesForFilter(query, user),
      this.getStudentsForFilter(query, user),
      this.getTagsForFilter(query, user),
    ]);

    return {
      responsibles,
      courses,
      modules,
      students,
      tags,
    };
  }

  async getRedirectUsers(message: ForumMessages, _user: Express.User) {
    const users = await Promise.all([
      this.usersService.findByRole(SystemRole.Support, message.courseId),
      this.usersService.findByRole(SystemRole.Tutor, message.courseId),
      this.usersService.findByRole(SystemRole.Teamlead, message.courseId),
    ]);
    // Support must have 'support' role enrollments
    users[0] = users[0].filter(u => u.enrollments.find(e => e.role === CourseRole.Support));
    // Tutor must have 'tutor' role enrollments
    users[1] = users[1].filter(u => u.enrollments.find(e => e.role === CourseRole.Tutor));
    // Teamlead must have 'teamlead' role enrollments
    users[2] = users[2].filter(u => u.enrollments.find(e => e.role === CourseRole.Teamlead));
    const usersObj = {};
    users.forEach(e => e.forEach(k => (!usersObj[k.id] ? (usersObj[k.id] = k) : usersObj[k.id].roles.push(...k.roles))));

    return Object.values(usersObj);
  }

  async findAllForAdminPanel(paging: PaginationDto, query: QueryForumMessagesForAdminPanelDto): Promise<Paginated<any[]>> {
    const selectWithoutResponsible = query.responsibles?.some(el => !el);
    const selectedResponsibles = query.responsibles?.filter(Boolean);
    const selectedCourses = query.checkedCourseElements?.filter((el: any) => el.type === 'course').map((el: any) => el.id);
    const selectedSections = query.checkedCourseElements?.filter((el: any) => el.type === 'section').map((el: any) => el.id);
    const selectedModules = query.checkedCourseElements?.filter((el: any) => el.type === 'module').map((el: any) => el.id);
    const selectedLectures = query.checkedCourseElements?.filter((el: any) => el.type === 'lecture').map((el: any) => el.id);

    const queryBuilder = this.forumMessagesRepository
      .createQueryBuilder('forumMessages')
      .leftJoinAndSelect('forumMessages.tariff', 'tariff')
      .leftJoinAndSelect('forumMessages.course', 'course')
      .leftJoinAndSelect('forumMessages.lecture', 'lecture')
      .leftJoinAndSelect('forumMessages.threadParent', 'threadParent')
      .leftJoinAndSelect('threadParent.resolvedUser', 'threadParentResolvedUser')
      .leftJoinAndSelect('threadParent.lock', 'threadParentLock')
      .leftJoinAndSelect('threadParentLock.user', 'threadParentLockUser')
      .leftJoinAndSelect('forumMessages.user', 'user')
      .leftJoinAndSelect('forumMessages.resolvedUser', 'resolvedUser')
      .leftJoinAndSelect('lecture.module', 'module')
      .leftJoinAndSelect('module.section', 'section')
      .leftJoinAndSelect('forumMessages.lock', 'lock', 'forumMessages.lock_id is not null')
      .leftJoinAndSelect('lock.user', 'lockUser');

    if (query.responsibles?.length) {
      queryBuilder.andWhere(
        new Brackets(qb => {
          if (selectWithoutResponsible) {
            qb.orWhere(
              `(
                (forumMessages.resolved is not null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.resolved_user_id is null)
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = false
                  and threadParent.lock is not null
                  and threadParent.resolved is not null
                  and threadParent.resolved_user_id is null)
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = true
                  and threadParent.user_id is null)
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = false
                  and threadParentLock.user_id is null)
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is null
                  and lock is not null
                  and lock.user_id is null)
                or (forumMessages.skip_resolve = true
                  and forumMessages.user_id is null)
              )`,
            );
          }
          if (selectedResponsibles?.length) {
            qb.orWhere(
              `(
                (forumMessages.resolved is not null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.resolved_user_id in (:...responsibles))
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = false
                  and threadParent.lock is not null
                  and threadParent.resolved is not null
                  and threadParent.resolved_user_id in (:...responsibles))
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = true
                  and threadParent.user_id in (:...responsibles))
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is not null
                  and threadParent.skip_resolve = false
                  and threadParentLock.user_id in (:...responsibles))
                or (forumMessages.resolved is null 
                  and forumMessages.skip_resolve = false
                  and forumMessages.thread_parent_id is null
                  and lock is not null
                  and lock.user_id in (:...responsibles))
                or (forumMessages.skip_resolve = true
                  and forumMessages.user_id in (:...responsibles))
              )`,
              {
                responsibles: selectedResponsibles,
              },
            );
          }
        }),
      );
    }
    if (query.pipeline) {
      queryBuilder.andWhere('forumMessages.pipeline = :pipeline', {
        pipeline: query.pipeline,
      });
    }
    if (query.createdFrom) {
      queryBuilder.andWhere('forumMessages.created >= :createdFrom', {
        createdFrom: query.createdFrom,
      });
    }
    if (query.createdTo) {
      queryBuilder.andWhere('forumMessages.created <= :createdTo', {
        createdTo: query.createdTo,
      });
    }
    if (query.resolvedFrom) {
      queryBuilder.andWhere('(forumMessages.resolved >= :resolvedFrom and forumMessages.resolved is not null)', {
        resolvedFrom: query.resolvedFrom,
      });
    }
    if (query.resolvedTo) {
      queryBuilder.andWhere('(forumMessages.resolved <= :resolvedTo and forumMessages.resolved is not null)', {
        resolvedTo: query.resolvedTo,
      });
    }
    if (selectedCourses?.length || selectedSections?.length || selectedModules?.length || selectedLectures?.length) {
      queryBuilder.andWhere(
        new Brackets(qb => {
          if (selectedCourses.length) {
            qb.orWhere('course.id IN (:...selectedCourses)', {
              selectedCourses,
            });
          }
          if (selectedSections.length) {
            qb.orWhere('section.id IN (:...selectedSections)', {
              selectedSections,
            });
          }
          if (selectedModules.length) {
            qb.orWhere('module.id IN (:...selectedModules)', {
              selectedModules,
            });
          }
          if (selectedLectures.length) {
            qb.orWhere('lecture.id IN (:...selectedLectures)', {
              selectedLectures,
            });
          }
        }),
      );
    }
    if (query.isResolvedAfterOutdated) {
      queryBuilder.andWhere('forumMessages.resolved is not NULL');
      queryBuilder.andWhere('tariff.outdated_date is not NULL');
      queryBuilder.andWhere('forumMessages.resolved > tariff.outdated_date');
    }
    if (query.isResolvedAfterOutdated === false) {
      queryBuilder.andWhere('forumMessages.resolved is not NULL');
      queryBuilder.andWhere('tariff.outdated_date is not NULL');
      queryBuilder.andWhere('forumMessages.resolved <= tariff.outdated_date');
    }
    if (typeof query.isSkipResolve === 'boolean') {
      queryBuilder.andWhere('forumMessages.skip_resolve = :isSkipResolve', {
        isSkipResolve: query.isSkipResolve,
      });
    }

    if (paging && !paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }

    if (query.sortField) {
      let sortField;
      if (query.sortField === 'created') {
        sortField = 'forumMessages.created';
      } else if (query.sortField === 'resolved') {
        sortField = 'forumMessages.resolved';
      } else if (query.sortField === 'deadline') {
        sortField = 'tariff.outdatedDate';
      } else if (query.sortField === 'course') {
        sortField = 'course.title';
      } else if (query.sortField === 'section') {
        sortField = 'section.title';
      } else if (query.sortField === 'module') {
        sortField = 'module.sort';
      } else if (query.sortField === 'lecture') {
        sortField = 'lecture.sort';
      } else if (query.sortField === 'pipeline') {
        sortField = 'forumMessages.pipeline';
      } else {
        sortField = 'forumMessages.created';
      }
      queryBuilder.orderBy(sortField, query.sortDirection);
    } else {
      queryBuilder.orderBy('forumMessages.created', 'ASC');
    }

    const [forumMessages, total] = await queryBuilder.getManyAndCount();

    const results = forumMessages.map(el => {
      const convertHoursToDaysAndHours = (hours: number | string, zeroDiff?: string): string => {
        if (!hours) return zeroDiff;
        const days = Math.floor(Number(hours) / 24);
        hours = Number(hours) - days * 24;
        return days && hours ? `${days}d ${hours}h` : days && !hours ? `${days}d` : `${hours}h`;
      };

      const overallCorrection =
        el.created && el.resolved ? convertHoursToDaysAndHours(dayjs(el.resolved).diff(dayjs(el.created), 'hours'), '< 1h') : '';

      const outdatedDiff =
        el.resolved && el.tariff?.outdatedDate && dayjs(el.resolved).isAfter(el.tariff.outdatedDate)
          ? convertHoursToDaysAndHours(dayjs(el.resolved).diff(dayjs(el.tariff.outdatedDate), 'hours'), '< 1h')
          : !el.resolved && el.tariff?.outdatedDate && dayjs().isAfter(el.tariff.outdatedDate)
          ? convertHoursToDaysAndHours(dayjs().diff(dayjs(el.tariff.outdatedDate), 'hours'), '< 1h')
          : '';

      const link = `${process.env.TUTOR_LMS_URL}/messages/${el.threadParentId || el.id}/?courseId=${el.courseId}`;
      const resolved = el.resolved ? dayjs(el.resolved).format('YYYY-MM-DD HH:mm') : '';
      const deadline = el.tariff?.outdatedDate ? dayjs(el.tariff.outdatedDate).format('YYYY-MM-DD HH:mm') : '';
      const created = el.created ? dayjs(el.created).format('YYYY-MM-DD HH:mm') : '';
      const courseTitle = `${el.course?.title}${el.course?.version > 1 ? ' v' + el.course.version : ''}`;
      const moduleNumber = el.lecture?.module?.customNumber || el.lecture?.module?.number;
      const moduleTitle = moduleNumber !== null ? `${moduleNumber} - ${el.lecture?.module?.title}` : `${el.lecture?.module?.title}`;
      const lectureNumber = el.lecture?.customNumber || el.lecture?.number;
      const lectureTitle = lectureNumber !== null ? `${lectureNumber} - ${el.lecture?.title}` : `${el.lecture?.title}`;
      const responsible =
        el.resolved && !el.skipResolve
          ? el.resolvedUser?.name
          : !el.resolved && !el.skipResolve && el.threadParent?.skipResolve === false
          ? el.threadParent.lock?.user?.name
          : !el.resolved && !el.skipResolve && el.threadParent?.skipResolve === true
          ? el.threadParent.user?.name
          : !el.resolved && !el.skipResolve && !el.threadParent
          ? el.lock?.user?.name
          : el.skipResolve
          ? el.user?.name
          : '';

      return {
        id: el.id,
        responsible,
        course: courseTitle,
        section: el.lecture?.module?.section?.title,
        module: moduleTitle,
        lecture: lectureTitle,
        created,
        resolved,
        deadline,
        overallCorrection,
        outdatedDiff,
        pipeline: el.pipeline,
        link,
      };
    });

    return {
      results,
      meta: {
        ...paging,
        total,
        sortField: query.sortField,
        sortDirection: query.sortDirection,
      },
    };
  }
}
