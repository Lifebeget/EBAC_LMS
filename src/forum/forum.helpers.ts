import * as omit from 'lodash.omit';
import { ForumMessages } from '@entities/forum-messages.entity';
import { CourseRole } from '@lib/types/enums/course-role.enum';

function selectForumMessage(tableName = 'forumMessage', fieldNames: string[] = []) {
  return [
    'body',
    'created',
    'hidden',
    'id',
    'modified',
    'edited',
    'parentId',
    'courseId',
    'lectureId',
    'score',
    'sticky',
    'substituteUser',
    'threadParentId',
    'title',
    'user',
    'resolved',
    'pipeline',
    'skip_resolve',
  ]
    .concat(fieldNames || [])
    .map(f => `${tableName}.${f}`);
}

function selectUser(tableName = 'users', fieldNames: string[] = []) {
  return ['id', 'name'].concat(fieldNames || []).map(f => `${tableName}.${f}`);
}

function selectAvatar(tableName = 'avatar', fieldNames: string[] = []) {
  return ['id', 'eadboxUrl', 's3Key', 'url'].concat(fieldNames || []).map(f => `${tableName}.${f}`);
}

function selectLecture(tableName = 'lecture', fieldNames: string[] = []) {
  return ['id', 'title'].concat(fieldNames || []).map(f => `${tableName}.${f}`);
}

function selectCourse(tableName = 'course', fieldNames: string[] = []) {
  return ['id', 'title'].concat(fieldNames || []).map(f => `${tableName}.${f}`);
}

function filterCreatePost(message: ForumMessages) {
  return omit(message, ['deleted', 'externalId', 'resolved', 'resolvedUserId', 'skipResolve', 'substituteUserId']);
}

function replaceSubstituteUser(results: ForumMessages & any) {
  if (Array.isArray(results)) {
    return results.map(e => {
      if (e.substituteUser) {
        e.user = e.substituteUser;
        delete e.substituteUser;
      }
      if (e.answers && e.answers.length) {
        e.answers.forEach((answer: any) => {
          if (answer.substituteUser) {
            answer.user = {
              ...answer.substituteUser,
              enrollments: [
                {
                  active: true,
                  courseId: answer.courseId,
                  dateFrom: null,
                  dateTo: null,
                  role: CourseRole.Support,
                },
              ],
            };
            delete answer.substituteUser;
          }
        });
      }
      return e;
    });
  }
  if (results.substituteUser) {
    results.user = results.substituteUser;
    delete results.substituteUser;
  }

  return results;
}
function filterSticky(threads: ForumMessages & any) {
  const results = [];
  threads.forEach((thread: any) => {
    if (thread.sticky) {
      results.push(thread);
    } else {
      thread.answers.forEach((answer: any) => {
        if (answer.sticky) {
          answer.answers = [];
          results.push(answer);
        }
      });
    }
  });
  return results;
}
export { selectForumMessage, selectUser, selectAvatar, selectLecture, selectCourse, filterCreatePost, replaceSubstituteUser, filterSticky };
