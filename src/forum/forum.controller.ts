import { ForumComplaints } from '@entities/forum-complaints.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { ComplaintType } from '@enums/complaint-type.enum';
import { Permission } from '@enums/permission.enum';
import { ForumThreadPipeline } from '@lib/api/types';
import { SystemRole } from '@lib/types/enums';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  Req,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as dayjs from 'dayjs';
import { Request } from 'express';
import { CoursesService } from 'src/courses/courses.service';
import { EnrollmentsService } from 'src/courses/enrollments.service';
import { LecturesService } from 'src/courses/lectures.service';
import { Pagination } from 'src/pagination.decorator';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { HierarchysService } from 'src/users/hierarchy.service';
import { UsersService } from 'src/users/users.service';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateComplaintDto } from './dto/create-forum-complaint.dto';
import { CreateMessageDto } from './dto/create-forum-message.dto';
import { LockMessageDto } from './dto/lock-forum-message.dto';
import { QueryForumComplaintsDto } from './dto/query-forum-complaints.dto';
import { QueryForumMessagesDto, QueryForumMessagesForAdminPanelDto } from './dto/query-forum-messages.dto';
import { QueryForumThreadsDto } from './dto/query-forum-threads.dto';
import { QueryUnresolvedForumMessagesDto } from './dto/query-unresolved-forum-messages.dto';
import { ResolveMessageDto } from './dto/resolve-forum-message.dto';
import { UpdateMessageDto } from './dto/update-forum-message.dto';
import { ForumService } from './forum.service';

@ApiTags('forum')
@ApiBearerAuth()
@Controller()
export class ForumController {
  constructor(
    private readonly forumService: ForumService,
    private lectureService: LecturesService,
    private coursesService: CoursesService,
    private userService: UsersService,
    private hierarchysService: HierarchysService,
    private enrollmentsService: EnrollmentsService,
  ) {}

  isEnrollment(courseId: string, user: Express.User, onlyManagers = false) {
    return (
      user.enrolledAsSupport(courseId) ||
      (!onlyManagers && user.enrolledAsStudent(courseId) && user.coursePermissions[courseId].canReadForum) ||
      user.enrolledAsTutor(courseId) ||
      user.enrolledAsTeamlead(courseId) ||
      user.enrolledAsModerator(courseId)
    );
  }

  isEnrollmentWrite(courseId: string, user: Express.User, onlyManagers = false) {
    return (
      user.enrolledAsSupport(courseId) ||
      (!onlyManagers && user.enrolledAsStudent(courseId) && user.coursePermissions[courseId].canWriteForum) ||
      user.enrolledAsTutor(courseId) ||
      user.enrolledAsTeamlead(courseId) ||
      user.enrolledAsModerator(courseId)
    );
  }

  private async checkTeamLeadCoursesAccess(courseId: string, user: Express.User) {
    if (!user.isTeamlead()) return false;

    const courses = (await this.userService.getCoursesForTutorsTeamlead(user.id)) || [];
    if (courses.filter(e => e === courseId).length) {
      return true;
    }
  }

  isFrontManagerRoles(user: Express.User) {
    // эти роли любой курс
    if (user.isSuperadmin() || user.isAdmin() || user.isManager() || user.isObserver()) {
      return true;
    }
    return false;
  }

  async isCourseAccessByRole(courseId: string, user: Express.User) {
    if (this.isFrontManagerRoles(user)) {
      return true;
    }

    // курсы подчиненных
    const teamleadCheck = await this.checkTeamLeadCoursesAccess(courseId, user);
    if (teamleadCheck) {
      return true;
    }
  }

  async isTeamLeadTutor(message: ForumMessages, userId: string) {
    const teamleadTutorIds = await this.enrollmentsService.findTeamleadTutors(userId);

    return teamleadTutorIds.includes(message.lock.userId);
  }

  async isTeamLeadTutorMessage(message: ForumMessages, userId: string) {
    const teamleadTutorIds = await this.enrollmentsService.findTeamleadTutors(userId);
    return teamleadTutorIds.includes(message.user.id);
  }

  async isCourseAccess(courseId: string, user: Express.User, onlyManagers = false) {
    return this.isEnrollment(courseId, user, onlyManagers) || (await this.isCourseAccessByRole(courseId, user));
  }

  async isCourseAccessWrite(courseId: string, user: Express.User, onlyManagers = false) {
    return this.isEnrollmentWrite(courseId, user, onlyManagers) || (await this.isCourseAccessByRole(courseId, user));
  }

  async isOnlyStudent(courseId: string, user: Express.User) {
    return !this.isEnrollment(courseId, user, true) && !(await this.isCourseAccessByRole(courseId, user));
  }

  async isCourseAccessException(courseId: string, user: Express.User, onlyManagers = false) {
    if (this.isEnrollment(courseId, user, onlyManagers) || (await this.isCourseAccess(courseId, user))) {
      return;
    }

    throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
  }

  async isMessagesAccessException(ids: string[], user: Express.User, onlyManagers = false) {
    const promises = [];
    ids.forEach(id => {
      const promise = this.isOneMessageAccessException(id, user, true);
      promises.push(promise);
    });
    await Promise.all(promises);
  }

  async isOneMessageAccessException(id: string, user: Express.User, onlyManagers = false) {
    const message = await this.forumService.findOne(id, true, false);
    if (!message) throw new NotFoundException('Message not found');

    if (this.isEnrollment(message.courseId, user, onlyManagers) || (await this.isCourseAccess(message.courseId, user))) {
      return message;
    }

    throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
  }

  async isMessageAccessExceptionWrite(id: string, user: Express.User, onlyManagers = false) {
    const message = await this.forumService.findOne(id, true, false);
    if (!message) throw new NotFoundException('Message not found');

    if (this.isEnrollmentWrite(message.courseId, user, onlyManagers) || (await this.isCourseAccess(message.courseId, user))) {
      return message;
    }

    throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
  }

  // MESSAGES
  @Transactional()
  @Post('forum/messages')
  @ApiOperation({ summary: 'Create a new forum message' })
  @ApiBody({ type: CreateMessageDto })
  @ApiResponse({
    status: 201,
    description: 'Created forum message',
    type: ForumMessages,
  })
  @PgConstraints()
  async create(@Body() createForumDto: CreateMessageDto, @Req() req: Request) {
    if (!(await this.isCourseAccessWrite(createForumDto.courseId, req.user))) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }

    if (createForumDto.isModerator && !this.isFrontManagerRoles(req.user) && !this.isEnrollment(createForumDto.courseId, req.user, true)) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }

    if (
      createForumDto.substituteUser &&
      !req.user.enrolledAsSupport(createForumDto.courseId) &&
      !this.isCourseAccessByRole(createForumDto.courseId, req.user)
    ) {
      throw new ForbiddenException('only support can substitute user!');
    }

    return this.forumService.create(createForumDto, req.user);
  }

  @Get('forum/messages')
  @ApiOperation({ summary: 'List messages by forum' })
  @ApiQuery({
    name: 'courseId',
    description: 'Course',
    required: true,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Lecture',
    required: false,
  })
  @ApiQuery({
    name: 'userId',
    description: 'User',
    required: false,
  })
  @ApiQuery({
    name: 'sticky',
    description: 'Sticky',
    required: false,
  })
  @ApiQuery({
    name: 'parent',
    description: 'Parent message id',
    required: false,
  })
  @ApiQuery({
    name: 'threadParent',
    description: 'Thread parent message id',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Array of messages',
    type: [ForumMessages],
  })
  @ApiQuery({
    name: 'includeLock',
    description: 'includeLock',
    required: false,
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async findAll(
    @Query() query: QueryForumMessagesDto,
    @Pagination() paging: PaginationDto,
    @Req() req: Request,
  ): Promise<Paginated<ForumMessages[]>> {
    if (query.ids) {
      if (!(query.ids.length > 0)) {
        await this.isCourseAccessException(query.courseId, req.user);
      }
      this.isMessagesAccessException(query.ids, req.user, true);
    }

    return this.forumService.findAll(paging, query, req.user?.id);
  }

  @Post('forum/messages/for-admin-panel')
  @ApiOperation({ summary: 'List messages by forum' })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async findAllForAdminPanel(
    @Body() body: QueryForumMessagesForAdminPanelDto,
    @Pagination() paging: PaginationDto,
    @Req() req: Request,
  ): Promise<Paginated<ForumMessages[]>> {
    return this.forumService.findAllForAdminPanel(paging, body);
  }

  @Get('forum/threads')
  @ApiOperation({ summary: 'List messages by forum(question and N answers)' })
  @ApiQuery({
    name: 'courseId',
    description: 'Course',
    required: true,
  })
  @ApiQuery({
    name: 'lectureId',
    description: 'Lecture',
    required: false,
  })
  @ApiQuery({
    name: 'threadId',
    description: 'Thread',
    required: false,
  })
  @ApiQuery({
    name: 'answerCount',
    description: 'Answer count',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: 'Sort thread',
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Array of messages',
    type: [ForumMessages],
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async findAllThreads(
    @Query() query: QueryForumThreadsDto,
    @Pagination() paging: PaginationDto,
    @Req() req: Request,
  ): Promise<Paginated<ForumMessages[]>> {
    if (!(await this.isCourseAccess(query.courseId, req.user))) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }

    const isOnlyStudent = await this.isOnlyStudent(query.courseId, req.user);

    return this.forumService.findAllThreads(paging, query, req.user?.id, isOnlyStudent);
  }

  @Get('forum/messages/:id')
  @ApiOperation({ summary: 'Get one message from forum' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Message',
    type: ForumMessages,
  })
  @PgConstraints()
  async findOne(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const message = await this.isOneMessageAccessException(id, req.user);

    const isOnlyStudent = await this.isOnlyStudent(message.courseId, req.user);

    return this.forumService.findOneAndMarkRead(id, req.user?.id, isOnlyStudent);
  }

  @Transactional()
  @Patch('forum/messages/:id')
  @ApiOperation({ summary: 'Updates a message' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiBody({ type: UpdateMessageDto })
  @ApiResponse({
    status: 200,
    description: 'Returns updated fields',
  })
  @PgConstraints()
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateForumDto: UpdateMessageDto, @Req() req: Request) {
    const message = await this.isMessageAccessExceptionWrite(id, req.user);
    if (
      (message.user.id != req.user.id ||
        (message.user.id == req.user.id &&
          req.user.enrolledAsStudent(message.courseId) &&
          !req.user.enrolledAsTutor(message.courseId) &&
          dayjs().diff(message.created, 'hour') > 1)) &&
      !this.isFrontManagerRoles(req.user) &&
      !(await this.isTeamLeadTutorMessage(message, req.user.id)) &&
      !req.user.enrolledAsSupport(message.courseId)
    ) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }

    return this.forumService.update(id, updateForumDto);
  }

  @Transactional()
  @Delete('forum/messages/:id')
  @ApiOperation({ summary: 'Deletes a message' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async remove(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const message = await this.isMessageAccessExceptionWrite(id, req.user);
    if (
      message.user.id != req.user.id &&
      !this.isFrontManagerRoles(req.user) &&
      !(await this.isTeamLeadTutorMessage(message, req.user.id)) &&
      !req.user.enrolledAsSupport(message.courseId)
    ) {
      throw new ForbiddenException('ERRORS.MESSAGE_ACCESS_FORBIDDEN');
    }

    return this.forumService.remove(id);
  }

  @Transactional()
  @Delete('forum/messages/:id/permanent')
  @ApiOperation({ summary: 'Deletes a message(permanent)' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async removePermanent(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user, true);

    return this.forumService.removePermanent(id);
  }

  @Transactional()
  @Post('forum/messages/:id/restore')
  @ApiOperation({ summary: 'Restore a message' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async restore(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isMessageAccessExceptionWrite(id, req.user);

    return this.forumService.restore(id);
  }

  // LIKES

  @Transactional()
  @Post('forum/messages/:id/like')
  @ApiOperation({ summary: 'Set Like' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns likes count of message',
  })
  @PgConstraints()
  async setLike(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user);

    return this.forumService.setLike(id, req.user?.id, 1);
  }

  @Transactional()
  @Post('forum/messages/:id/neutral')
  @ApiOperation({ summary: 'Unset Like' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns likes count of message',
  })
  @PgConstraints()
  async setNeutral(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user);

    return this.forumService.setLike(id, req.user?.id, 0);
  }

  @Transactional()
  @Post('forum/messages/:id/dislike')
  @ApiOperation({ summary: 'Set Dislike', deprecated: true })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns likes count of message',
  })
  @PgConstraints()
  async setDislike(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user);

    return this.forumService.setLike(id, req.user?.id, -1);
  }

  // SUBSCRIBE FOR REPLIES

  @Transactional()
  @Post('forum/messages/:id/subscribe')
  @ApiOperation({ summary: 'Subscribe for replies' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async subscribe(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const messsage = await this.isOneMessageAccessException(id, req.user);
    return this.forumService.subscribe(id, req.user?.id, messsage.threadParentId || messsage.id);
  }

  @Transactional()
  @Post('forum/messages/:id/unsubscribe')
  @ApiOperation({ summary: 'Unsubscribe for replies' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
    deprecated: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async unsubscribe(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user);

    return this.forumService.unsubscribe(id, req.user?.id);
  }

  // HIDE(BAN) MESSAGES

  @Transactional()
  @Post('forum/messages/:id/hide')
  @RequirePermissions(Permission.FORUM_MANAGE)
  @ApiOperation({ summary: 'Hide message content' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async hide(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user, true);

    return this.forumService.setHide(id, req.user?.id, true);
  }

  @Transactional()
  @Post('forum/messages/:id/unhide')
  @RequirePermissions(Permission.FORUM_MANAGE)
  @ApiOperation({ summary: 'Unhide message content' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async unhide(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user, true);

    return this.forumService.setHide(id, req.user?.id, false);
  }

  // STICKY MESSAGES

  @Transactional()
  @Post('forum/messages/:id/sticky')
  @RequirePermissions(Permission.FORUM_MANAGE)
  @ApiOperation({ summary: 'Sticky message content' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async sticky(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user, true);

    return this.forumService.setSticky(id, true);
  }

  @Transactional()
  @Post('forum/messages/:id/unsticky')
  @RequirePermissions(Permission.FORUM_MANAGE)
  @ApiOperation({ summary: 'Unsticky message content' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @PgConstraints()
  async unsticky(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.isOneMessageAccessException(id, req.user, true);

    return this.forumService.setSticky(id, false);
  }

  // COMPLAINTS

  @Transactional()
  @Post('forum/messages/:id/complaint')
  @ApiOperation({ summary: 'Create complaint on message' })
  @ApiBody({ type: CreateComplaintDto })
  @ApiResponse({
    status: 201,
    description: 'Created complaint on message',
    type: ForumComplaints,
  })
  @PgConstraints()
  async createComplaint(@Param('id', new ParseUUIDPipe()) id: string, @Body() createComplaintDto: CreateComplaintDto, @Req() req: Request) {
    await this.isCourseAccessException(id, req.user);

    return this.forumService.createComplaint(id, createComplaintDto, req.user?.id);
  }

  @Get('forum/complaints')
  @ApiOperation({ summary: 'List complaints' })
  @ApiQuery({
    name: 'courseId',
    description: 'Course',
    required: true,
  })
  @ApiQuery({
    name: 'active',
    description: 'Active',
    enum: ['true', 'false'],
    required: false,
  })
  @ApiQuery({
    name: 'messageId',
    description: 'Message',
    required: false,
  })
  @ApiQuery({
    name: 'userId',
    description: 'User',
    required: false,
  })
  @ApiQuery({
    name: 'type',
    description: 'Complaint type',
    enum: Object.values(ComplaintType),
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: 'Array of messages',
    type: ForumMessages,
  })
  @RequirePermissions(Permission.FORUM_MANAGE)
  @PgConstraints()
  async findAllComplaints(
    @Query() query: QueryForumComplaintsDto,
    @Pagination() paging: PaginationDto,
    @Req() req: Request,
  ): Promise<Paginated<ForumComplaints[]>> {
    if (query.courseId) {
      await this.isCourseAccessException(query.courseId, req.user);
    }
    return this.forumService.findAllComplaints(paging, query);
  }

  @Get('forum/complaints/:id')
  @ApiOperation({ summary: 'Get complaint' })
  @ApiParam({
    name: 'id',
    description: 'Complaint id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Get a complaint by id',
    type: ForumMessages,
  })
  @RequirePermissions(Permission.FORUM_MANAGE)
  @PgConstraints()
  async findOneComplaint(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkComplaintRightsById(id, req.user);
    return this.forumService.findOneComplaint(id);
  }

  @Transactional()
  @Post('forum/complaints/:id/complete')
  @ApiOperation({ summary: 'Close complaint' })
  @ApiParam({
    name: 'id',
    description: 'Complaint id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @RequirePermissions(Permission.FORUM_MANAGE)
  @PgConstraints()
  async setCompleteComplaint(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkComplaintRightsById(id, req.user);

    return this.forumService.setStatusComplaint(id, false);
  }

  @Transactional()
  @Post('forum/complaints/:id/reopen')
  @ApiOperation({ summary: 'Reopen complaint' })
  @ApiParam({
    name: 'id',
    description: 'Complaint id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @RequirePermissions(Permission.FORUM_MANAGE)
  @PgConstraints()
  async setReopenComplaint(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkComplaintRightsById(id, req.user);
    return this.forumService.setStatusComplaint(id, true);
  }

  @Transactional()
  @Delete('forum/complaints/:id')
  @ApiOperation({ summary: 'Delete complaint' })
  @ApiParam({
    name: 'id',
    description: 'Complaint id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @RequirePermissions(Permission.FORUM_MANAGE)
  @PgConstraints()
  async removeComplaint(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    await this.checkComplaintRightsById(id, req.user);
    return this.forumService.removeComplaint(id);
  }

  private async checkComplaintRightsById(id: string, user: Express.User) {
    const complaint = await this.forumService.findOneComplaint(id);
    if (!complaint) throw new NotFoundException('Complaint not found');

    const hasEnrollment =
      user.enrolledAsTutor(complaint.message.course?.id) ||
      user.enrolledAsTeamlead(complaint.message.course?.id) ||
      user.enrolledAsSupport(complaint.message.course?.id);

    if (!hasEnrollment) {
      throw new ForbiddenException();
    }
  }

  // RESOLVE

  @Transactional()
  @Post('forum/messages/:id/resolve')
  @RequirePermissions(Permission.FORUM_VIEW)
  @ApiOperation({ summary: 'Resolve message' })
  @ApiParam({
    name: 'id',
    description: 'Message id',
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async resolve(@Param('id', new ParseUUIDPipe()) id: string, @Body() body: ResolveMessageDto, @Req() req: Request) {
    const message = await this.isOneMessageAccessException(id, req.user, true);

    // проверяем лок треда
    let thread;
    if (message.threadParentId) {
      thread = await this.forumService.findOne(message.threadParentId, false, false);
    } else {
      thread = message;
    }

    await this.checkAccessLockMessage(thread, req.user);

    if (!body.resolveThreadBeforeDate) {
      if (message.skipResolve) {
        throw new BadRequestException('Message is skipResolve');
      }
      if (message.resolved) {
        return 'OK';
      }
    }

    return this.forumService.setResolve(id, body, req.user.id, message, thread);
  }

  async checkAccessLockMessage(message: ForumMessages, user: Express.User) {
    if (message.lock && message.lock.userId && message.lock.userId != user.id && message.lock.mutex.getTime() > new Date().getTime()) {
      const roleOk = await this.isFrontManagerRoles(user);
      if (roleOk) {
        return true;
      }
      if (message.pipeline === ForumThreadPipeline.tutor && user.parsedRoles.includes(SystemRole.Teamlead)) {
        return true;
      }
      const result = await this.isTeamLeadTutor(message, user.id);
      if (!result) {
        throw new BadRequestException(`This message is locked for another user: ${message.lock.userId}`);
      }
    }
  }

  @Transactional()
  @Post('forum/messages/:id/lock-thread')
  @RequirePermissions(Permission.FORUM_VIEW)
  @ApiOperation({ summary: 'Lock/redirect thread to user' })
  @ApiResponse({
    status: 201,
    description: 'Created lock',
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async lock(@Param('id', new ParseUUIDPipe()) id: string, @Body() body: LockMessageDto, @Req() req: Request) {
    const message = await this.isOneMessageAccessException(id, req.user);

    if (message.threadParentId) {
      throw new BadRequestException(`Cant lock message, only thread`);
    }

    await this.checkAccessLockMessage(message, req.user);

    return this.forumService.lockThread(id, body, req.user.id, message);
  }

  @Transactional()
  @Post('forum/messages/:id/unlock-thread')
  @RequirePermissions(Permission.FORUM_VIEW)
  @ApiOperation({ summary: 'unlock thread' })
  @ApiResponse({
    status: 201,
    description: 'Remove lock',
  })
  @PgConstraints()
  async unlock(@Param('id', new ParseUUIDPipe()) id: string, @Body() body: LockMessageDto, @Req() req: Request) {
    const message = await this.isOneMessageAccessException(id, req.user);

    if (message.threadParentId) {
      throw new UnauthorizedException(`Cant unlock message, only thread`);
    }
    await this.checkAccessLockMessage(message, req.user);
    const pipeline = body?.pipeline || undefined;
    return this.forumService.unlockThread(message, pipeline);
  }

  @Get('forum/unresolved/threads')
  @RequirePermissions(Permission.FORUM_VIEW)
  @ApiOperation({
    summary: 'List unresolved thread by forum',
  })
  @ApiResponse({
    status: 200,
    description: 'Array of messages',
    type: [ForumMessages],
  })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @PgConstraints()
  async findAllUnresolvedThreads(
    @Query() query: QueryUnresolvedForumMessagesDto,
    @Pagination() paging: PaginationDto,
    @Req() req: Request,
  ): Promise<Paginated<ForumMessages[]>> {
    return this.forumService.findAllUnresolvedThreads(paging, query, req.user);
  }

  @Get('forum/unresolved/summary')
  @RequirePermissions(Permission.FORUM_VIEW)
  @ApiOperation({ summary: 'Unresolved stats' })
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  async summary(@Query() query: QueryUnresolvedForumMessagesDto, @Req() req: Request) {
    return this.forumService.summary(query, req.user);
  }

  @Get('forum/unresolved/filterData')
  @RequirePermissions(Permission.FORUM_VIEW)
  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  getFilterData(@Query() query: QueryUnresolvedForumMessagesDto, @Req() req: Request) {
    return this.forumService.getFilterData(query, req.user);
  }

  @Get('forum/messages/:id/redirect-users')
  @ApiOperation({ summary: 'Users list for redirect for message' })
  @RequirePermissions(Permission.FORUM_VIEW)
  async getRedirectUsers(@Param('id', new ParseUUIDPipe()) id: string, @Req() req: Request) {
    const message = await this.isOneMessageAccessException(id, req.user, true);
    return this.forumService.getRedirectUsers(message, req.user);
  }
}
