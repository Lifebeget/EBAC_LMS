import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsUUID, ValidateIf } from 'class-validator';
import { QuerySorted } from './query-sorted';
import { ForumThreadPipeline } from '@lib/api/types';

export class QueryForumMessagesDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ValidateIf(o => o.hasOwnProperty('sticky'))
  @IsBooleanString()
  sticky?: boolean;

  // @ValidateIf((o) => o.hasOwnProperty('tutorsGroup'))
  // @IsBooleanString()
  // tutorsGroup?: boolean;

  @ValidateIf(o => o.hasOwnProperty('threadParent'))
  @IsUUID()
  threadParent?: string;

  @ValidateIf(o => o.hasOwnProperty('parent'))
  @IsUUID()
  parent?: string;

  @ApiProperty({
    description: 'Load all from current position',
    default: false,
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('loadAllFromCurrentPostion'))
  @IsBooleanString()
  loadAllFromCurrentPostion?: boolean;

  @ValidateIf(o => o.hasOwnProperty('loadAuthor'))
  @IsBooleanString()
  loadAuthor?: boolean;

  @ApiProperty({ description: 'Messages identifiers to load' })
  ids: string[];
}

export class QueryForumMessagesForAdminPanelDto extends QuerySorted {
  @ApiProperty({ description: 'Find where resolved date after outdated date', required: false })
  @ValidateIf(o => o.hasOwnProperty('isResolvedAfterOutdated'))
  isResolvedAfterOutdated?: boolean;

  @ApiProperty({ description: 'Filter by responsibles', required: false })
  @ValidateIf(o => o.hasOwnProperty('responsibles'))
  responsibles?: string[];

  @ApiProperty({ description: 'Filter by courses, sections, modules or lectures', required: false })
  @ValidateIf(o => o.hasOwnProperty('checkedCourseElements'))
  checkedCourseElements?: string[];

  @ApiProperty({
    description: 'Filter by create period start date',
    required: false,
  })
  createdFrom?: Date;

  @ApiProperty({
    description: 'Filter by create period end date',
    required: false,
  })
  createdTo?: Date;

  @ApiProperty({
    description: 'Filter by resolve period start date',
    required: false,
  })
  resolvedFrom?: Date;

  @ApiProperty({
    description: 'Filter by resolve period end date',
    required: false,
  })
  resolvedTo?: Date;

  @ApiProperty({
    description: 'Filter by pipeline',
    required: false,
  })
  pipeline?: ForumThreadPipeline;

  @ApiProperty({ description: 'Filter by skip resolve', required: false })
  @ValidateIf(o => o.hasOwnProperty('isSkipResolve'))
  isSkipResolve?: boolean;
}
