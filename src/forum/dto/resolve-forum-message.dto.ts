import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, ValidateIf } from 'class-validator';

export class ResolveMessageDto {
  @ApiProperty({ description: 'Resolve all thread before date' })
  @ValidateIf(o => o.hasOwnProperty('resolveThreadBeforeDate'))
  @IsDateString()
  resolveThreadBeforeDate?: Date;
}
