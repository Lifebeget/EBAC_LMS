import { ComplaintType } from '@enums/complaint-type.enum';
import { IsBooleanString, IsIn, IsUUID, ValidateIf } from 'class-validator';

export class QueryForumComplaintsDto {
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ValidateIf(o => o.hasOwnProperty('active'))
  @IsBooleanString()
  active?: boolean;

  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ValidateIf(o => o.hasOwnProperty('messageId'))
  @IsUUID()
  messageId?: string;

  @ValidateIf(o => o.hasOwnProperty('type'))
  @IsIn(Object.values(ComplaintType))
  type?: ComplaintType;
}
