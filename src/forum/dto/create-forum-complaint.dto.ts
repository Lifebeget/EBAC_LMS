import { ComplaintType } from '@enums/complaint-type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsNotEmpty, IsString, ValidateIf } from 'class-validator';

export class CreateComplaintDto {
  @ApiProperty({ description: 'Complaint type' })
  @ValidateIf(o => o.hasOwnProperty('complaintType'))
  @IsIn(Object.values(ComplaintType))
  complaintType?: ComplaintType;

  @ApiProperty({ description: 'Complaint text' })
  @ValidateIf(o => o.hasOwnProperty('text'))
  @IsNotEmpty()
  @IsString()
  text?: string;
}
