import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsEnum, IsNumber, IsNumberString, IsUUID, ValidateIf } from 'class-validator';

export class QueryForumThreadsDto {
  @IsUUID()
  courseId: string;

  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ValidateIf(o => o.hasOwnProperty('threadId'))
  @IsUUID()
  threadId?: string;

  @ApiProperty({
    description: 'Filter by pipeline',
    required: false,
    enum: ForumThreadPipeline,
  })
  @ValidateIf(o => o.hasOwnProperty('pipeline'))
  @IsEnum(ForumThreadPipeline)
  pipeline?: ForumThreadPipeline;

  @ValidateIf(o => o.hasOwnProperty('answerCount'))
  @IsNumberString()
  answerCount?: number;

  @ValidateIf(o => o.hasOwnProperty('sort'))
  @Type(() => Number)
  @IsNumber()
  sort?: number;

  @ApiProperty({ description: 'Sticky' })
  @ValidateIf(o => o.hasOwnProperty('sticky'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  sticky?: boolean;
}
