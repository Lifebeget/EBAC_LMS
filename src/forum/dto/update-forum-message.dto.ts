import { PickType } from '@nestjs/mapped-types';
import { CreateMessageDto } from './create-forum-message.dto';

export class UpdateMessageDto extends PickType(CreateMessageDto, ['title', 'body', 'sticky'] as const) {}
