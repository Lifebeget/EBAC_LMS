import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { ResolveState } from '@enums/resolve-type.enum';
import { SystemRole } from '@lib/types/enums';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsUUID, ValidateIf } from 'class-validator';

export class QueryUnresolvedForumMessagesDto {
  @ApiProperty({ description: 'Find by id', required: false })
  @ValidateIf(o => o.hasOwnProperty('threadId'))
  @IsUUID()
  threadId?: string;

  @ApiProperty({ description: 'Filter by course', required: false })
  @ValidateIf(o => o.hasOwnProperty('courseId'))
  @IsUUID()
  courseId?: string;

  @ApiProperty({ description: 'Filter by courses', required: false })
  courseIds?: string[];

  @ApiProperty({ description: 'Filter by module', required: false })
  @ValidateIf(o => o.hasOwnProperty('moduleId'))
  @IsUUID()
  moduleId?: string;

  @ApiProperty({ description: 'Filter by lecture', required: false })
  @ValidateIf(o => o.hasOwnProperty('lectureId'))
  @IsUUID()
  lectureId?: string;

  @ApiProperty({
    description: 'Show resolved',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('showResolved'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  showResolved?: boolean;

  @ApiProperty({
    description: 'Filter by submission type list',
    required: false,
  })
  resolveState?: ResolveState;

  @ApiProperty({ description: 'Filter by user', required: false })
  @ValidateIf(o => o.hasOwnProperty('userId'))
  @IsUUID()
  userId?: string;

  @ApiProperty({ description: 'Filter by users list', required: false })
  userIds?: string[];

  @ApiProperty({ description: 'Filter by responsible user', required: false })
  @ValidateIf(o => o.hasOwnProperty('responsibleId'))
  @IsUUID()
  responsibleId?: string;

  @ApiProperty({
    description: 'Filter by responsible users tag (ResponsibleOption enum)',
    required: false,
  })
  @ValidateIf(o => o.hasOwnProperty('responsibleTag'))
  responsibleTag?: string;

  @ApiProperty({
    description: 'Text to search',
    required: false,
  })
  searchString?: string;

  @ApiProperty({
    description: 'Filter by course tag(vertical)',
    required: false,
  })
  tagIds?: string[];

  @ApiProperty({
    description: 'Filter by created period start date',
    required: false,
  })
  createdStartDate?: Date;

  @ApiProperty({
    description: 'Filter by created period end date',
    required: false,
  })
  createdEndDate?: Date;

  @ApiProperty({
    description: 'Filter by resolved period start date',
    required: false,
  })
  resolvedStartDate?: Date;

  @ApiProperty({
    description: 'Filter by resolved period end date',
    required: false,
  })
  resolvedEndDate?: Date;

  @ApiProperty({
    description: 'Filter by pipeline',
    required: false,
    enum: ForumThreadPipeline,
  })
  @ValidateIf(o => o.hasOwnProperty('pipeline'))
  @IsEnum(ForumThreadPipeline)
  pipeline?: ForumThreadPipeline;

  @ApiProperty({
    description: 'Get submissions for role',
    required: false,
  })
  asRole?: SystemRole;
}
