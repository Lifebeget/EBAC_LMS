import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsUUID, ValidateIf } from 'class-validator';

export class LockMessageDto {
  @ApiProperty({ description: 'User lock' })
  @ValidateIf(o => o.hasOwnProperty('user'))
  @IsUUID()
  user?: string;

  @ApiProperty({ description: 'Pipeline switch' })
  @ValidateIf(o => o.hasOwnProperty('pipeline'))
  @IsEnum(ForumThreadPipeline)
  pipeline?: ForumThreadPipeline;
}
