import { ApiProperty } from '@nestjs/swagger';

export class ForumMessageSummaryDto {
  @ApiProperty({ description: 'Count of all forum message' })
  allCount: number;

  @ApiProperty({ description: 'Count of urgent forum message' })
  urgentCount: number;

  @ApiProperty({ description: 'Count of outdated forum message' })
  outdatedCount: number;

  @ApiProperty({ description: 'Count of forum message resolved today' })
  todayCount: number;
}
