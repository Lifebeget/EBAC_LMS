import { ForumThreadPipeline } from '@enums/forum-thread-pipeline.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { ValidateIf, IsNotEmpty, IsInt, IsUUID, IsBoolean, IsOptional, IsEnum } from 'class-validator';
import { ExcludeBase64FromLog } from 'src/logger';

export class CreateMessageDto {
  @ApiProperty({ description: 'id' })
  @ValidateIf(o => o.hasOwnProperty('id'))
  @IsUUID()
  id?: string; // TODO: field for develop

  @ApiProperty({ description: 'External ID (EADBOX)' })
  externalId?: string;

  @ApiProperty({ description: 'Message title' })
  title?: string;

  @ApiProperty({ description: 'Message text' })
  @IsNotEmpty()
  @ExcludeBase64FromLog()
  body: string;

  @ApiProperty({ description: 'Like count' })
  @ValidateIf(o => o.hasOwnProperty('score'))
  @IsInt()
  score?: number;

  @ApiProperty({ description: 'Parent message' })
  @ValidateIf(o => o.hasOwnProperty('parent'))
  @IsUUID()
  @IsOptional()
  parentId?: string;

  @ApiProperty({ description: 'Sticky' })
  @ValidateIf(o => o.hasOwnProperty('sticky'))
  @IsBoolean()
  sticky?: boolean;

  @ApiProperty({ description: 'Lecture' })
  @ValidateIf(o => o.hasOwnProperty('lecture'))
  @IsUUID()
  @IsOptional()
  lectureId?: string;

  @ApiProperty({ description: 'Course' })
  @IsUUID()
  courseId: string;

  @ApiProperty({ description: 'Substitute user' })
  @ValidateIf(o => o.hasOwnProperty('substituteUser'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  substituteUser?: boolean;

  @ApiProperty({ description: 'Thread pipeline (support/tutor)' })
  @ValidateIf(o => o.hasOwnProperty('pipeline'))
  @IsEnum(ForumThreadPipeline)
  pipeline?: ForumThreadPipeline;

  @ApiProperty({ description: 'Flag that identifies moderator' })
  @ValidateIf(o => o.hasOwnProperty('isModerator'))
  @Transform(flag => flag.value === 'true', {
    toClassOnly: true,
  })
  @IsBoolean()
  isModerator: boolean;
}
