import { IsDateString, IsUUID } from 'class-validator';
import { CreateMessageDto } from './create-forum-message.dto';

export class CreateMessageImportDto extends CreateMessageDto {
  @IsDateString()
  created?: string;

  @IsDateString()
  modified?: string;

  @IsUUID()
  userId?: string;
}
