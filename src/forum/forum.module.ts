import { ForumComplaints } from '@entities/forum-complaints.entity';
import { ForumLikes } from '@entities/forum-likes.entity';
import { ForumMessagesLock } from '@entities/forum-messages-lock.entity';
import { ForumMessages } from '@entities/forum-messages.entity';
import { Course } from '@entities/course.entity';
import { ForumSubscribe } from '@entities/forum-subscribe.entity';
import { ForumViewed } from '@entities/forum-viewed.entity';
import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoursesModule } from 'src/courses/courses.module';
import { DirectoryModule } from 'src/directory/directory.module';
import { FilesModule } from 'src/files/files.module';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { SettingsModule } from 'src/settings/settings.module';
import { SocketsModule } from 'src/sockets/sockets.module';
import { TriggerModule } from 'src/trigger/trigger.module';
import { UsersModule } from 'src/users/users.module';
import { ForumController } from './forum.controller';
import { ForumService } from './forum.service';

@Module({
  imports: [
    forwardRef(() => NotificationsModule),
    FilesModule,
    forwardRef(() => CoursesModule),
    forwardRef(() => UsersModule),
    ConfigModule,
    forwardRef(() => SocketsModule),
    SettingsModule,
    DirectoryModule,
    forwardRef(() => TriggerModule),
    TypeOrmModule.forFeature([ForumComplaints, ForumLikes, ForumMessages, ForumMessagesLock, ForumSubscribe, ForumViewed, Course]),
  ],
  controllers: [ForumController],
  providers: [ForumService],
})
export class ForumModule {}
