import { Topic } from '@entities/topic.entity';
import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsModule } from 'src/settings/settings.module';
import { TopicService } from './topic.service';
import { TopicsController } from './topics.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Topic]), forwardRef(() => SettingsModule)],
  controllers: [TopicsController],
  providers: [TopicService],
  exports: [TopicService],
})
export class TopicModule {}
