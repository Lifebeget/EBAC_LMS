import { ApiProperty } from '@nestjs/swagger';

export class QueryTopicDto {
  @ApiProperty({
    description: 'Filter by external IDs',
    type: [Number],
    required: false,
  })
  externalIds?: number[];

  @ApiProperty({ description: 'Include nomenclatures', required: false })
  includeNomenclatures?: boolean;

  @ApiProperty({ description: 'Nomenclature schedule ID', required: false })
  scheduleId: string;
}
