import { Topic } from '@entities/topic.entity';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { SettingsService } from 'src/settings/settings.service';
import { Connection, FindManyOptions, getConnection, Repository } from 'typeorm';
import { PaginationDto } from '../pagination.dto';
import { QueryTopicDto } from './dto/query-topic.dto';
import { camelizeKeys } from 'humps';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class TopicService {
  constructor(
    @Inject(forwardRef(() => SettingsService))
    private settingsService: SettingsService,

    @InjectConnection()
    private readonly connection: Connection,

    @InjectRepository(Topic)
    private readonly topicRepository: Repository<Topic>,
  ) {}

  save(dto: Topic) {
    return this.topicRepository.save(dto);
  }

  async find(values: FindManyOptions<Topic>) {
    return this.topicRepository.find({
      ...values,
      relations: ['schedule'],
    });
  }

  async findAll(paging: PaginationDto, query: QueryTopicDto) {
    const queryBuilder = getConnection().createQueryBuilder().select(['topic.*']).from('topic', 'topic');
    if (query.externalIds) {
      if (!Array.isArray(query.externalIds)) query.externalIds = [query.externalIds];
      queryBuilder.where('external_id = ANY(:external_ids)', { external_ids: query.externalIds });
    }
    if (query.includeNomenclatures) {
      queryBuilder.addSelect('topic_nomenclatures.nomenclatures');
      queryBuilder.leftJoin(
        qb => {
          return qb
            .select([
              'topic.id as id',
              "COALESCE(jsonb_agg(nomenclature) FILTER (WHERE nomenclature.id IS NOT NULL), '[]') as nomenclatures",
            ])
            .from('topic', 'topic')
            .leftJoin('topic_nomenclature_link', 'topic_nomenclature_link', 'topic_nomenclature_link.topic_id = topic.id')
            .leftJoin('nomenclature', 'nomenclature', 'topic_nomenclature_link.nomenclature_id = nomenclature.id')
            .groupBy('topic.id');
        },
        'topic_nomenclatures',
        'topic_nomenclatures.id = topic.id',
      );
    }

    const total = await queryBuilder.getCount();

    if (!paging.showAll) {
      queryBuilder.offset((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.limit(paging.itemsPerPage);
    }

    const results = camelizeKeys(await queryBuilder.getRawMany());

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  async findOne(id: string) {
    return this.topicRepository.findOne(id);
  }

  async upsert(values) {
    let columns = getConnection()
      .getMetadata(Topic)
      .ownColumns.map(column => {
        return column.databaseName;
      });
    columns = columns.filter(col => {
      return col !== 'id';
    });
    const res = await getConnection().createQueryBuilder().insert().into(Topic).values(values).orUpdate(columns, ['external_id']).execute();
    return res;
  }
  async upsertRelations(topic: Topic, relationName: string, relationsToAdd: Array<any>, relationsToRemove: Array<any>) {
    const _res = await getConnection()
      .createQueryBuilder()
      .relation(Topic, relationName)
      .of(topic)
      .addAndRemove(relationsToAdd, relationsToRemove);
  }

  async delete(values) {
    return this.topicRepository.softDelete(values);
  }
}
