import { Topic } from '@entities/topic.entity';
import { Permission } from '@enums/permission.enum';
import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { Interapp } from 'src/auth/interapp.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { FindManyOptions, Repository } from 'typeorm';
import { Pagination } from '../pagination.decorator';
import { PaginationDto } from '../pagination.dto';
import { TopicService } from './topic.service';
import { QueryTopicDto } from './dto/query-topic.dto';

@Controller('topics')
@ApiTags('topics')
export class TopicsController {
  constructor(
    @InjectRepository(Topic)
    private topicsRepository: Repository<Topic>,
    private topicsService: TopicService,
  ) {}

  @Get()
  @Interapp()
  @RequirePermissions(Permission.TOPIC_VIEW)
  async findTopics(@Query('offset') offset: number, @Query('limit') limit: number, @Query('scheduleId') scheduleId?: string) {
    const where: FindManyOptions<Topic>['where'] = {};
    if (scheduleId) where.scheduleId = scheduleId;

    const [data, count] = await this.topicsRepository.findAndCount({
      where: where,
      order: { id: 'ASC' },
      skip: offset,
      take: limit,
      withDeleted: true,
    });
    return { data, count };
  }
  @Get('find-many')
  @Interapp()
  @RequirePermissions(Permission.TOPIC_VIEW)
  async findMany(@Query() query: QueryTopicDto, @Pagination() paging: PaginationDto) {
    return await this.topicsService.findAll(paging, query);
  }
}
