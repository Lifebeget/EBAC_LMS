import { Topic } from '@entities/topic.entity';
import { Webinar } from '@entities/webinars.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import * as dayjs from 'dayjs';
import { Repository } from 'typeorm';
import { CmsTopic } from '../lib/types/CmsTopic';
import { ImportOptions } from '../lib/types/ImportOptions';

export class ImportTopics {
  private topicsRepository: Repository<Topic>;
  private webinarsRepository: Repository<Webinar>;
  private apiKey: string;
  private url: string;

  constructor(context: INestApplicationContext) {
    this.topicsRepository = context.get(getRepositoryToken(Topic));
    this.webinarsRepository = context.get(getRepositoryToken(Webinar));
    this.apiKey = process.env.CMS_API_KEY;
    this.url = process.env.CMS_TOPICS_URL;
    strict.ok(this.apiKey);
    strict.ok(this.url);
  }

  async import({ pageSize = 100 }: ImportOptions) {
    let page = 0;
    let created = 0;
    let updated = 0;
    const missingSchedules = [];

    console.log(`➡️ Importing topics`, { pageSize });

    while (true) {
      // Fetch CMS topics
      const res = await axios.get<{ data: { fields: CmsTopic[] } }>(this.url, {
        params: {
          offset: page * pageSize,
          limit: pageSize,
          apiKey: this.apiKey,
        },
      });

      // Create or update each LMS topic
      for (const cmsTopic of res.data.data.fields) {
        const schedule = await this.webinarsRepository.findOne(
          { externalId: cmsTopic.event_schedule_id },
          { select: ['id'], withDeleted: true },
        );
        if (!schedule) {
          if (missingSchedules.indexOf(cmsTopic.event_schedule_id) === -1) {
            missingSchedules.push(cmsTopic.event_schedule_id);
          }
        }

        const lmsTopic: Partial<Topic> = {
          schedule,
          externalId: cmsTopic.id,
          title: cmsTopic.title,
          description: cmsTopic.description,
          webinarLink: cmsTopic.webinar_link,
          recordingLink: cmsTopic.webinar_recording,
          regularDiscountPromocode: cmsTopic.discounter_course_link_regular,
          killerDiscountPromocode: cmsTopic.discounter_course_link_killer,
          speakerName: cmsTopic.speaker_name,
          speakerDescription: cmsTopic.speaker_text,
          speakerSubtitle: cmsTopic.speaker_subtitle,
          speakerFromFirstDay: cmsTopic.speaker_from_day_one,
          speakerFromSecondDay: cmsTopic.speaker_from_day_two,
          dayNumber: cmsTopic.num + 1,
          created: new Date(cmsTopic.created),
          modified: new Date(cmsTopic.modified),
          deleted: cmsTopic.deleted ? new Date(cmsTopic.deleted) : null,
          regularDiscountDescription: cmsTopic.discounter_course_link_regular_description,
          killerDiscountDescription: cmsTopic.discounter_course_link_killer_description,
          topicDateUtc: dayjs(cmsTopic.topic_date_utc).utc().toDate(),
          topicDateBrazil: cmsTopic.topic_date_utc
            ? dayjs(cmsTopic.topic_date_utc).utc().add(new Date().getTimezoneOffset(), 'minutes').toDate()
            : null,
          topicActive: cmsTopic.active,
          speakerImageUrl: cmsTopic?.file_speaker_image?.url,
          imageUrl: cmsTopic?.file_preview_image?.url,
        };

        const existingLmsTopic = await this.topicsRepository.findOne({ externalId: lmsTopic.externalId }, { withDeleted: true });
        if (existingLmsTopic) {
          await this.topicsRepository.save({
            ...existingLmsTopic,
            ...lmsTopic,
          });
          updated += 1;
        } else {
          await this.topicsRepository.save(lmsTopic);
          created += 1;
        }
      }

      // Quit or go to the next page
      if (res.data.data.fields.length < pageSize) {
        console.log('✔️ done', {
          created,
          updated,
          total: created + updated,
          missingSchedules: missingSchedules.length,
        });
        break;
      } else {
        page += 1;
        console.log({ page, created, updated });
      }
    }
  }
}
/*
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { FilesService } from 'src/files/files.service';
import { counterTemplate } from '../lib/types/ImportCounters';
import { TopicService } from './topic.service';
import { Topic } from '@entities/topic.entity';
import * as dayjs from 'dayjs';
import { NomenclatureService } from '../nomenclature/nomenclature.service';
import { Nomenclature } from '@entities/nomenclature.entity';
import { In } from 'typeorm';

export class ImportTopics {
  private filesService: FilesService;
  private topicsService: TopicService;
  private nomenclaturesService: NomenclatureService;
  private debug: boolean;
  constructor(
    context: INestApplicationContext,
    private counter = { ...counterTemplate },
  ) {
    this.filesService = context.get(FilesService);
    this.topicsService = context.get(TopicService);
    this.nomenclaturesService = context.get(NomenclatureService);
    this.debug = true;
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(
      `Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`,
    );
  }

  async import(concurrency = 3, debug = true) {
    console.log('Starting import topics');
    this.debug = debug;

    await this.importData();

    this.printStats(this.counter);
  }

  async importData() {
    const start = Date.now();
    const cmsTopics: Array<any> = await this.downloadTopics();
    const cmsTopicsNomenclaturesRelations: Array<any> =
      await this.downloadRelations();

    if (this.debug)
      console.log(`Load ${Date.now() - start}ms count = ${cmsTopics.length}`);

    if (!cmsTopics.length) return;
    const cmsTopicLength = cmsTopics.length;
    for (const [cmsTopicIndex, cmsTopic] of Object.entries(cmsTopics)) {
      try {
        const topicNomenclaturesRelations =
          cmsTopicsNomenclaturesRelations.filter(
            (relation) => relation.event_schedule_topic_id == cmsTopic.id,
          );
        const nomenclatureIds: Array<string> = topicNomenclaturesRelations.map(
          (rel) => rel['nomenclature_id'],
        );
        const nomenclatures = await this.nomenclaturesService.find({
          where: { externalId: In(nomenclatureIds) },
        });
        await this.upsert(cmsTopic, nomenclatures);
      } catch (e) {
        if (this.debug) {
          console.log(
            `Topic create/update Error. Topic external id ${cmsTopic.id}.`,
          );
          console.log(`Error:`, e);
        }
      }
      if (this.debug)
        console.log(
          `${+cmsTopicIndex + 1}/${cmsTopicLength}`,
          'topic id',
          cmsTopic.id,
        );
    }
    if (this.debug) console.log('Save time', Date.now() - start, 'ms');
  }

  async downloadTopics(): Promise<any> {
    const result: Array<any> = [];
    let pageCounter = 1;
    const itemsPerPage = 300;
    while (true) {
      try {
        const res = await axios
          .get(process.env.CMS_TOPICS_URL, {
            params: {
              limit: itemsPerPage,
              offset: itemsPerPage * (pageCounter - 1),
              apiKey: process.env.CMS_API_KEY,
            },
          })
          .catch((err) => {
            console.error('import topics', err);
          });
        if (!res || !(<any>res)?.data?.data?.fields?.length) {
          break;
        }
        // return res?.data.data.fields;

        result.push(...res?.data.data.fields);
        pageCounter++;
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }
  async downloadRelations(): Promise<any> {
    const result: Array<any> = [];
    let pageCounter = 1;
    const itemsPerPage = 300;
    while (true) {
      try {
        const res = await axios
          .get(process.env.CMS_NOMENCLATURE_ID_WITH_DISCOUNTS_URL, {
            params: {
              limit: itemsPerPage,
              offset: itemsPerPage * (pageCounter - 1),
              apiKey: process.env.CMS_API_KEY,
            },
          })
          .catch((err) => {
            console.error('import topics', err);
          });
        if (!res || !(<any>res)?.data?.data?.fields?.length) {
          break;
        }
        // return res?.data.data.fields;

        result.push(...res?.data.data.fields);
        pageCounter++;
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }
  async upsert(cmsTopic: any, nomenclatures: Nomenclature[] = []) {
    const topic = new Topic();
    topic.externalId = cmsTopic.id;
    // topic.scheduleId = cmsTopic.event_schedule_id;
    topic.title = cmsTopic.title;
    topic.description = cmsTopic.description;
    topic.webinarLink = cmsTopic.webinar_link;
    topic.recordingLink = cmsTopic.webinar_recording;
    topic.regularDiscountPromocode = cmsTopic.discounter_course_link_regular;
    topic.killerDiscountPromocode = cmsTopic.discounter_course_link_killer;
    topic.speakerName = cmsTopic.speaker_name;
    topic.speakerDescription = cmsTopic.speaker_text;
    topic.speakerSubtitle = cmsTopic.speaker_subtitle;
    topic.speakerFromFirstDay = cmsTopic.speaker_from_day_one;
    topic.speakerFromSecondDay = cmsTopic.speaker_from_day_two;
    topic.dayNumber = cmsTopic.num + 1;
    topic.created = cmsTopic.created;
    topic.modified = cmsTopic.modified;
    topic.deleted = cmsTopic.deleted;
    topic.regularDiscountDescription =
      cmsTopic.discounter_course_link_regular_description;
    topic.killerDiscountDescription =
      cmsTopic.discounter_course_link_killer_description;
    topic.topicDateUtc = cmsTopic.topic_date_utc;
    if (cmsTopic.topic_date_utc) {
      topic.topicDateBrazil = dayjs(cmsTopic.topic_date_utc)
        .add(3, 'hours')
        .toDate();
    }
    topic.topicActive = cmsTopic.active;
    topic.speakerImageUrl = cmsTopic?.file_speaker_image?.url;
    topic.imageUrl = cmsTopic?.file_preview_image?.url;

    const dbTopicsPromise = this.topicsService.find({
      select: ['externalId'],
      where: { externalId: cmsTopic.id },
      withDeleted: true,
    });
    const dbTopics = await dbTopicsPromise;
    let dbTopic;
    if (dbTopics.length > 0) {
      dbTopic = dbTopics[0];
      this.counter.updated++;
    } else {
      this.counter.created++;
    }

    const topicRes = await this.topicsService.upsert(topic);
    const topicRelationsRes = await this.topicsService.upsertRelations(
      topic,
      'nomenclatures',
      nomenclatures,
      dbTopic?.nomenclatures?.length ? dbTopic?.nomenclatures : nomenclatures,
    );
  }
}
*/
