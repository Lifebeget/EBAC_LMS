import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImportInfo } from 'src/lib/entities/import-info.entity';
import { ImportInfoService } from './import-info.service';
import { ImportInfoController } from './import-info.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ImportInfo])],
  providers: [ImportInfoService],
  controllers: [ImportInfoController],
})
export class ImportInfoModule {}
