import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { ImportInfo } from 'src/lib/entities/import-info.entity';
import { ImportState } from 'src/lib/enums/import-state.enum';
import { ImportType } from 'src/lib/enums/import-type.enum';
import { StartPermission } from 'src/lib/enums/start-permission.enum';
import { Repository } from 'typeorm';
import { Paginated, PaginationDto } from '../pagination.dto';
import { ImportsQueryDto } from './DTO/ImportsQuery.dto';

@Injectable()
export class ImportInfoService {
  constructor(
    @InjectRepository(ImportInfo)
    private readonly importInfoRepository: Repository<ImportInfo>,
  ) {}
  async saveStart(importType: ImportType) {
    const importInfo: ImportInfo = {
      importType,
      state: ImportState.Running,
      startedAt: new Date(),
    };

    const expiredDuration = this.getExpirationDuration(importType);

    importInfo.expiredAt = dayjs(importInfo.startedAt).add(expiredDuration).toDate();

    return this.importInfoRepository.save(importInfo);
  }
  async getRunning(): Promise<ImportInfo[]> {
    return this.importInfoRepository
      .createQueryBuilder('importInfo')
      .where('importInfo.state = :importState', {
        importState: ImportState.Running,
      })
      .andWhere('importInfo.expiredAt > :currentDate', {
        currentDate: new Date(),
      })
      .getMany();
  }

  getExpirationDuration(importType: ImportType) {
    if (importType === ImportType.Mutex) {
      return dayjs.duration(15, 'minutes');
    }

    if (importType === ImportType.Fast) {
      return dayjs.duration(3, 'hour');
    }

    if (importType === ImportType.Full) {
      return dayjs.duration(6, 'hour');
    }

    if (importType === ImportType.Users) {
      return dayjs.duration(3, 'hour');
    }

    if (importType === ImportType.FastCourse) {
      return dayjs.duration(5, 'minutes');
    }

    throw new Error('Undefined ImportType in getExpirationDuration');
  }

  async getStartPermission(importType: ImportType) {
    const running = await this.getRunning();

    console.log(running);

    if (running.length > 0) {
      if (this.includesImportType(running, importType)) {
        return StartPermission.Deny;
      }

      if (importType === ImportType.Full) {
        return StartPermission.Wait;
      }

      if ([ImportType.Mutex, ImportType.FastCourse].includes(importType)) {
        return StartPermission.Allow;
      }

      if ([ImportType.Fast, ImportType.Users].includes(importType)) {
        if (this.includesImportType(running, ImportType.Full)) {
          return StartPermission.Deny;
        }

        return StartPermission.Wait;
      }
    }

    return StartPermission.Allow;
  }

  private includesImportType(list: ImportInfo[], importType: ImportType): boolean {
    return !!list.find(importInfo => importInfo.importType === importType);
  }

  async saveFinish(importInfo: ImportInfo, importState: ImportState) {
    importInfo.finishedAt = new Date();
    importInfo.state = importState;
    return this.importInfoRepository.save(importInfo);
  }

  async saveCompletion(importInfo: ImportInfo) {
    return this.saveFinish(importInfo, ImportState.Completed);
  }

  async saveFail(importInfo: ImportInfo) {
    return this.saveFinish(importInfo, ImportState.Fail);
  }

  async saveInterrupt(importInfo: ImportInfo) {
    return this.saveFinish(importInfo, ImportState.Interrupt);
  }

  async findAll(paging: PaginationDto, query: ImportsQueryDto): Promise<Paginated<ImportInfo[]>> {
    let queryBuilder = this.importInfoRepository.createQueryBuilder('import_info');
    if (query.state) {
      queryBuilder = queryBuilder.andWhere('import_info.state LIKE :state', {
        state: `%${query.state}%`,
      });
    }
    if (query.import_type) {
      queryBuilder = queryBuilder.andWhere('import_info.import_type LIKE :import_type', {
        import_type: `%${query.import_type}%`,
      });
    }
    if (!paging.showAll) {
      queryBuilder.skip((paging.page - 1) * paging.itemsPerPage);
      queryBuilder.take(paging.itemsPerPage);
    }
    queryBuilder.addOrderBy('import_info.started_at', 'DESC');

    const [res, total] = await queryBuilder.getManyAndCount();

    return {
      results: res,
      meta: { ...paging, total },
    };
  }
}
