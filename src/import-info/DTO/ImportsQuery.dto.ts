import { ImportState } from '../../lib/enums/import-state.enum';
import { ImportType } from '../../lib/enums/import-type.enum';

export class ImportsQueryDto {
  state?: ImportState;
  import_type?: ImportType;
}
