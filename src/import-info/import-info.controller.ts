import { ApiBearerAuth, ApiOperation, ApiQuery, ApiTags, ApiExtraModels } from '@nestjs/swagger';
import { Controller, Get, Query } from '@nestjs/common';
import { RequirePermissions } from '../require-permissions.decorator';
import { Permission } from '../lib/enums/permission.enum';
import { ApiPaginatedResponse, PaginationDto } from '../pagination.dto';
import { Pagination } from '../pagination.decorator';
import { ImportInfoService } from './import-info.service';
import { ImportsQueryDto } from './DTO/ImportsQuery.dto';
import { ImportInfo } from '../lib/entities/import-info.entity';

@ApiTags('import-info')
@ApiBearerAuth()
@Controller('imports')
@ApiExtraModels(ImportInfo)
export class ImportInfoController {
  constructor(private readonly importService: ImportInfoService) {}

  @Get()
  @RequirePermissions(Permission.IMPORTS_VIEW)
  @ApiOperation({ summary: 'List all import-info' })
  @ApiQuery({
    name: 'state',
    description: 'Filter by state',
    required: false,
  })
  @ApiQuery({
    name: 'import_type',
    description: 'Filter by import type',
    required: false,
  })
  @ApiPaginatedResponse(ImportInfo)
  findAll(@Pagination() paging: PaginationDto, @Query() query: ImportsQueryDto) {
    return this.importService.findAll(paging, query);
  }
}
