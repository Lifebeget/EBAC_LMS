import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsModule } from 'src/settings/settings.module';
import { NomenclatureService } from './nomenclature.service';
import { Nomenclature } from '@entities/nomenclature.entity';
import { NomenclatureController } from './nomenclature.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Nomenclature]), forwardRef(() => SettingsModule)],
  controllers: [NomenclatureController],
  providers: [NomenclatureService],
  exports: [NomenclatureService],
})
export class NomenclatureModule {}
