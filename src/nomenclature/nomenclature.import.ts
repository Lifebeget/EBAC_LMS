import { Nomenclature } from '@entities/nomenclature.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import { Repository } from 'typeorm';
import { CmsNomenclature } from '../lib/types/CmsNomenclature';
import { ImportOptions } from '../lib/types/ImportOptions';
import { NomenclatureService } from './nomenclature.service';

export class ImportNomenclatures {
  private nomenclaturesRepository: Repository<Nomenclature>;
  private nomenclaturesService: NomenclatureService;
  private apiKey: string;
  private url: string;

  constructor(context: INestApplicationContext) {
    this.nomenclaturesRepository = context.get(getRepositoryToken(Nomenclature));
    this.nomenclaturesService = context.get(NomenclatureService);
    this.apiKey = process.env.CMS_API_KEY;
    this.url = process.env.CMS_NOMENCLATURES_URL;
    strict.ok(this.apiKey);
    strict.ok(this.url);
  }

  async import({ pageSize = 100 }: ImportOptions) {
    let page = 0;
    let created = 0;
    let updated = 0;

    console.log(`➡️ Importing nomenclatures`, { pageSize });

    while (true) {
      // Fetch CMS nomenclatures
      const res = await axios.get<{ data: { fields: CmsNomenclature[] } }>(this.url, {
        params: {
          offset: page * pageSize,
          limit: pageSize,
          apiKey: this.apiKey,
        },
      });

      // Create or update each LMS nomenclature
      for (const cmsNomenclature of res.data.data.fields) {
        const lmsNomenclature: Partial<Nomenclature> = {
          externalId: cmsNomenclature.id,
          category: cmsNomenclature.course_program_file,
          type: this.nomenclaturesService.cmsToLmsType(cmsNomenclature.type),
          shortName: cmsNomenclature.short_name,
          name: cmsNomenclature.name,
          pageUrl: cmsNomenclature.page_url,
          productIdRd: cmsNomenclature.rd_crm_product_id,
          programUrl: cmsNomenclature.file_course_program_file?.url,
          color: cmsNomenclature.background_color,
          campaignIdRd: cmsNomenclature.rd_campaign_id,
          created: new Date(cmsNomenclature.created),
          modified: new Date(cmsNomenclature.modified),
        };

        const existingLmsNomenclature = await this.nomenclaturesRepository.findOne(
          { externalId: lmsNomenclature.externalId },
          { withDeleted: true },
        );
        if (existingLmsNomenclature) {
          await this.nomenclaturesRepository.save({
            ...existingLmsNomenclature,
            ...lmsNomenclature,
          });
          updated += 1;
        } else {
          await this.nomenclaturesRepository.save(lmsNomenclature);
          created += 1;
        }
      }

      // Quit or go to the next page
      if (res.data.data.fields.length < pageSize) {
        console.log('✔️ done', { created, updated, total: created + updated });
        break;
      } else {
        page += 1;
        console.log({ page, created, updated });
      }
    }
  }
}

/*
import { INestApplicationContext } from '@nestjs/common';
import axios from 'axios';
import { FilesService } from 'src/files/files.service';
import { counterTemplate } from '../lib/types/ImportCounters';
import { NomenclatureService } from './nomenclature.service';
import { Nomenclature } from '@entities/nomenclature.entity';
import * as dayjs from 'dayjs';
import { In } from 'typeorm';
import { TopicService } from '../topic/topic.service';
import { Topic } from '@entities/topic.entity';

export class ImportNomenclatures {
  private filesService: FilesService;
  private nomenclaturesService: NomenclatureService;
  private topicsService: TopicService;
  private debug: boolean;
  constructor(
    context: INestApplicationContext,
    private counter = { ...counterTemplate },
  ) {
    this.filesService = context.get(FilesService);
    this.nomenclaturesService = context.get(NomenclatureService);
    this.topicsService = context.get(TopicService);
    this.debug = true;
  }

  printStats(stats: any, title = '') {
    if (title) {
      console.log(`=== ${title} ===`);
    }
    console.log(
      `Created: ${stats.created} | Updated: ${stats.updated} | Skipped: ${stats.skipped}`,
    );
  }

  async import(concurrency = 3, debug = true) {
    console.log('Starting import nomenclatures');
    this.debug = debug;

    await this.importData();

    this.printStats(this.counter);
  }

  async importData() {
    const start = Date.now();
    const cmsNomenclatures: Array<any> = await this.downloadNomenclatures();
    const cmsTopicsNomenclaturesRelations: Array<any> =
      await this.downloadRelations();

    if (this.debug)
      console.log(
        `Load ${Date.now() - start}ms count = ${cmsNomenclatures.length}`,
      );

    if (!cmsNomenclatures.length) return;
    const cmsNomenclatureLength = cmsNomenclatures.length;
    for (const [cmsNomenclatureIndex, cmsNomenclature] of Object.entries(
      cmsNomenclatures,
    )) {
      try {
        const topicNomenclaturesRelations =
          cmsTopicsNomenclaturesRelations.filter(
            (relation) => relation.nomenclature_id == cmsNomenclature.id,
          );
        const topicIds: Array<string> = topicNomenclaturesRelations.map(
          (rel) => rel['event_schedule_topic_id'],
        );
        const topics = await this.topicsService.find({
          where: { externalId: In(topicIds) },
        });

        await this.upsert(cmsNomenclature, topics);
      } catch (e) {
        if (this.debug) {
          console.log(
            `Nomenclature create/update Error. Nomenclature external id ${cmsNomenclature.id}.`,
          );
          console.log(`Error:`, e);
        }
      }
      if (this.debug)
        console.log(
          `${+cmsNomenclatureIndex + 1}/${cmsNomenclatureLength}`,
          'nomenclature id',
          cmsNomenclature.id,
        );
    }
    if (this.debug) console.log('Save time', Date.now() - start, 'ms');
  }

  async downloadNomenclatures(): Promise<any> {
    const result: Array<any> = [];
    let pageCounter = 1;
    const itemsPerPage = 300;
    while (true) {
      try {
        const res = await axios
          .get(process.env.CMS_NOMENCLATURES_URL, {
            params: {
              limit: itemsPerPage,
              offset: itemsPerPage * (pageCounter - 1),
              apiKey: process.env.CMS_API_KEY,
            },
          })
          .catch((err) => {
            console.error('import nomenclatures', err);
          });
        if (!res || !(<any>res)?.data?.data?.fields?.length) {
          break;
        }
        // return res?.data.data.fields;

        result.push(...res?.data.data.fields);
        pageCounter++;
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }
  async downloadRelations(): Promise<any> {
    const result: Array<any> = [];
    let pageCounter = 1;
    const itemsPerPage = 300;
    while (true) {
      try {
        const res = await axios
          .get(process.env.CMS_NOMENCLATURE_ID_WITH_DISCOUNTS_URL, {
            params: {
              limit: itemsPerPage,
              offset: itemsPerPage * (pageCounter - 1),
              apiKey: process.env.CMS_API_KEY,
            },
          })
          .catch((err) => {
            console.error('import topics', err);
          });
        if (!res || !(<any>res)?.data?.data?.fields?.length) {
          break;
        }
        // return res?.data.data.fields;

        result.push(...res?.data.data.fields);
        pageCounter++;
      } catch (error) {
        console.error(error);
      }
    }
    return result;
  }
  async upsert(cmsNomenclature: any, topics: Topic[] = []) {
    const nomenclature = new Nomenclature();
    nomenclature.externalId = cmsNomenclature.id;
    nomenclature.category = cmsNomenclature.course_program_file;
    nomenclature.type = this.nomenclaturesService.cmsToLmsType(
      cmsNomenclature.type,
    );
    nomenclature.shortName = cmsNomenclature.short_name;
    nomenclature.name = cmsNomenclature.name;
    nomenclature.pageUrl = cmsNomenclature.page_url;
    nomenclature.productIdRd = cmsNomenclature.rd_crm_product_id;
    nomenclature.programUrl = cmsNomenclature.file_course_program_file?.url;
    nomenclature.color = cmsNomenclature.background_color;
    nomenclature.campaignIdRd = cmsNomenclature.rd_campaign_id;
    nomenclature.created = cmsNomenclature.created;
    nomenclature.modified = cmsNomenclature.modified;

    const dbNomenclatures = await this.nomenclaturesService.find({
      select: ['externalId'],
      where: { externalId: cmsNomenclature.id },
      withDeleted: true,
    });
    let dbNomenclature;
    if (dbNomenclatures.length > 0) {
      dbNomenclature = dbNomenclatures[0];
      this.counter.updated++;
    } else {
      this.counter.created++;
    }

    const nomenclatureRes = await this.nomenclaturesService.upsert(
      nomenclature,
    );
    const nomenclatureRelationsRes =
      await this.nomenclaturesService.upsertRelations(
        nomenclature,
        'topics',
        topics,
        dbNomenclature?.topics.length ? dbNomenclature?.topics : topics,
      );
  }
}
*/
