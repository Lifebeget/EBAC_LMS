import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { SettingsService } from 'src/settings/settings.service';
import { Connection, getConnection, Repository } from 'typeorm';
import { Nomenclature } from '@entities/nomenclature.entity';
import { NomenclatureType } from '@enums/nomenclature-type';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class NomenclatureService {
  constructor(
    @Inject(forwardRef(() => SettingsService))
    private settingsService: SettingsService,

    @InjectConnection()
    private readonly connection: Connection,

    @InjectRepository(Nomenclature)
    private readonly nomenclatureRepository: Repository<Nomenclature>,
  ) {}

  save(dto: Nomenclature) {
    return this.nomenclatureRepository.save(dto);
  }

  async find(values) {
    return this.nomenclatureRepository.find({
      ...values,
      relations: ['topics'],
    });
  }

  @Transactional()
  async findOne(id: string) {
    return this.nomenclatureRepository.findOne(id);
  }

  async findOneByExternalId(externalId: string) {
    const res = this.nomenclatureRepository.find({
      where: { externalId },
    });
    return res[0];
  }

  async upsert(values) {
    let columns = getConnection()
      .getMetadata(Nomenclature)
      .ownColumns.map(column => {
        return column.databaseName;
      });
    columns = columns.filter(col => {
      return col !== 'id';
    });
    const res = await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Nomenclature)
      .values(values)
      .orUpdate(columns, ['external_id'])
      .execute();
    return res;
  }
  async upsertRelations(nomenclature, relationName: string, relationsToAdd: Array<any>, relationsToRemove: Array<any>) {
    const _res = await getConnection()
      .createQueryBuilder()
      .relation(Nomenclature, relationName)
      .of(nomenclature)
      .addAndRemove(relationsToAdd, relationsToRemove);
  }
  async delete(values) {
    return this.nomenclatureRepository.softDelete(values);
  }
  cmsToLmsType(cmsType) {
    switch (+cmsType) {
      case 1:
        return NomenclatureType.PROFESSION;
      default:
        return NomenclatureType.COURSE;
    }
  }
}
