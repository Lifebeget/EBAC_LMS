import { Nomenclature } from '@entities/nomenclature.entity';
import { Permission } from '@enums/permission.enum';
import { Controller, Get, NotFoundException, Param, Query } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiProperty, ApiTags } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { strict } from 'assert';
import { Interapp } from 'src/auth/interapp.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Repository } from 'typeorm';
import { NomenclatureService } from './nomenclature.service';

export class GetVariablesDto {
  @ApiProperty({ required: false })
  variableGroupId?: string;
}

@ApiTags('nomenclatures')
@Controller('nomenclatures')
export class NomenclatureController {
  constructor(
    @InjectRepository(Nomenclature)
    private nomenclaturesRepository: Repository<Nomenclature>,
    private readonly nomenclatureService: NomenclatureService,
  ) {}

  @Get()
  @Interapp()
  @RequirePermissions(Permission.NOMENCLATURE_VIEW)
  async findNomenclatures(@Query('offset') offset: number, @Query('limit') limit: number) {
    const [data, count] = await this.nomenclaturesRepository.findAndCount({
      order: { id: 'ASC' },
      skip: offset,
      take: limit,
      withDeleted: true,
    });
    return { data, count };
  }

  @Get(':id')
  @ApiOkResponse({ type: Nomenclature })
  @ApiNotFoundResponse()
  async getNomenclatureById(@Param('id') nomenclatureId: string) {
    const nomenclature = await this.nomenclatureService.findOne(nomenclatureId);
    strict.ok(nomenclature, new NotFoundException());
    return nomenclature;
  }

  @Get(':external-id')
  @ApiOkResponse({ type: Nomenclature })
  @ApiNotFoundResponse()
  async getNomenclatureByExternalId(@Param('id') nomenclatureExternalId: string) {
    const nomenclature = await this.nomenclatureService.findOneByExternalId(nomenclatureExternalId);
    strict.ok(nomenclature, new NotFoundException());
    return nomenclature;
  }
}
