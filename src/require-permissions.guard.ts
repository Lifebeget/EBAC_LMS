import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Permission } from './lib/enums/permission.enum';
import { PERMISSIONS_KEY } from './require-permissions.decorator';

@Injectable()
export class RequirePermissionsGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredPermissions = this.reflector.getAllAndOverride<Permission[]>(PERMISSIONS_KEY, [context.getHandler(), context.getClass()]);
    if (!requiredPermissions) {
      return true;
    }

    let req = null;
    // @ts-ignore TODO: need description
    if (context.getType() === 'graphql') {
      req = GqlExecutionContext.create(context).getContext().req;
    } else {
      req = context.switchToHttp().getRequest();
    }

    const { user } = req;

    return requiredPermissions.some(permission => user.permissions?.includes(permission));
  }
}
