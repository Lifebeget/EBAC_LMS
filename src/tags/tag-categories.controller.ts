import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Permission } from 'src/lib/enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { TagCategoriesService } from './tag-categories.service';
import { CreateTagCategoryDto } from './dto/create-tag-category.dto';
import { UpdateTagCategoryDto } from './dto/update-tag-category.dto';
import { QueryTagCategoryDto } from './dto/query-tag-category.dto';
import { ApiPaginatedResponse, PaginationDto } from '../pagination.dto';
import { Pagination } from '../pagination.decorator';
import { TagCategory } from '@entities/tag-category.entity';

@ApiTags('tags')
@ApiBearerAuth()
@Controller('tag-categories')
export class TagCategoriesController {
  constructor(private readonly tagCategoriesService: TagCategoriesService) {}

  @Post()
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Create tag category' })
  @ApiResponse({ status: 200, type: TagCategory, description: 'Created tag' })
  findOrCreate(@Body() createTagCategoryDto: CreateTagCategoryDto) {
    return this.tagCategoriesService.create(createTagCategoryDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all tags' })
  @ApiPaginatedResponse(TagCategory)
  @ApiResponse({
    status: 200,
    type: [TagCategory],
    description: 'Find all tags',
  })
  findAll(@Pagination() paging: PaginationDto, @Query() query: QueryTagCategoryDto) {
    return this.tagCategoriesService.findAll(paging, query);
  }

  @Patch(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Update tag category' })
  @ApiResponse({
    status: 200,
    type: TagCategory,
    description: 'Updated tag category',
  })
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateTagCategoryDto: UpdateTagCategoryDto) {
    return this.tagCategoriesService.update(id, updateTagCategoryDto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Delete tag category' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  delete(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.tagCategoriesService.remove(id);
  }
}
