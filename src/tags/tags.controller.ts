import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Tag } from 'src/lib/entities/tag.entity';
import { Permission } from 'src/lib/enums/permission.enum';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { TagsService } from './tags.service';
import { ApiPaginatedResponse, PaginationDto } from '../pagination.dto';
import { Pagination } from '../pagination.decorator';
import { QueryTagDto } from './dto/query-tag.dto';

@ApiTags('tags')
@ApiBearerAuth()
@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @Post()
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Create tag' })
  @ApiResponse({ status: 200, type: Tag, description: 'Created tag' })
  findOrCreate(@Body() createTagDto: CreateTagDto) {
    return this.tagsService.create(createTagDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all tags' })
  @ApiPaginatedResponse(Tag)
  @ApiResponse({ status: 200, type: [Tag], description: 'Find all tags' })
  findAll(@Pagination() paging: PaginationDto, @Query() query: QueryTagDto) {
    return this.tagsService.findAll(paging, query);
  }

  @Get('by-group')
  @ApiOperation({ summary: 'Get tags by group' })
  @ApiResponse({ status: 200, description: 'Find tags by group' })
  findAllByGroup(@Query() query: QueryTagDto) {
    return this.tagsService.findAllByGroup(query);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get one tag' })
  @ApiResponse({ status: 200, type: Tag, description: 'One tag' })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.tagsService.findOne(id);
  }

  @Patch(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Update tag' })
  @ApiResponse({ status: 200, type: Tag, description: 'Updated tag' })
  update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateTagDto: UpdateTagDto) {
    return this.tagsService.update(id, updateTagDto);
  }

  @Delete(':id')
  @RequirePermissions(Permission.COURSE_MANAGE)
  @ApiOperation({ summary: 'Delete tag' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  delete(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.tagsService.remove(id);
  }
}
