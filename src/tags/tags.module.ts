import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tag } from 'src/lib/entities/tag.entity';
import { TagCategory } from '@entities/tag-category.entity';
import { TagsService } from './tags.service';
import { TagsController } from './tags.controller';
import { TagCategoriesService } from './tag-categories.service';
import { TagCategoriesController } from './tag-categories.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Tag]), TypeOrmModule.forFeature([TagCategory])],
  providers: [TagsService, TagCategoriesService],
  exports: [TagsService, TagCategoriesService],
  controllers: [TagsController, TagCategoriesController],
})
export class TagsModule {}
