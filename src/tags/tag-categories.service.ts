import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Paginated, PaginationDto } from '../pagination.dto';
import { TagCategory } from '@entities/tag-category.entity';
import { CreateTagCategoryDto } from './dto/create-tag-category.dto';
import { UpdateTagCategoryDto } from './dto/update-tag-category.dto';
import { QueryTagCategoryDto } from './dto/query-tag-category.dto';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class TagCategoriesService {
  constructor(
    @InjectRepository(TagCategory)
    private readonly tagCategoriesRepository: Repository<TagCategory>,
  ) {}

  findTitles(): Promise<TagCategory[]> {
    return this.tagCategoriesRepository.createQueryBuilder('tagCategory').select('tagCategory.title').getMany();
  }

  async findAll(paging: PaginationDto, query: QueryTagCategoryDto): Promise<Paginated<TagCategory[]>> {
    const qb = this.tagCategoriesRepository.createQueryBuilder('tagCategory');

    if (query.title) {
      qb.andWhere('LOWER(tagCategory.title) LIKE LOWER(:title)', {
        title: `%${query.title}%`,
      });
    }

    if (query.type) {
      qb.andWhere('tagCategory.type = :type', {
        type: query.type,
      });
    }

    if (!paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    const [results, total] = await qb.getManyAndCount();

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  findOne(id: string) {
    return this.tagCategoriesRepository.findOne(id);
  }

  async create(createTagCategoryDto: CreateTagCategoryDto) {
    return this.tagCategoriesRepository.save(createTagCategoryDto);
  }

  update(id: string, updateTagCategoryDto: UpdateTagCategoryDto) {
    return this.tagCategoriesRepository.save({ ...updateTagCategoryDto, id });
  }

  remove(id: string) {
    return this.tagCategoriesRepository.delete(id);
  }
}
