import { ApiProperty } from '@nestjs/swagger';
import { TagType } from '@lib/types/enums/tag-type.enum';

export class QueryTagDto {
  @ApiProperty({ description: 'Filter by title', required: false })
  title?: string;

  @ApiProperty({
    description: 'Filter by category type',
    enum: TagType,
  })
  categoryType?: TagType;
}
