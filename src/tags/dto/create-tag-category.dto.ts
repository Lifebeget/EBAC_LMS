import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateTagCategoryDto {
  @ApiProperty({ description: 'Tag category title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Sorting' })
  sort: number;
}
