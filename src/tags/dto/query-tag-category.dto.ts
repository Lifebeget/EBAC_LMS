import { ApiProperty } from '@nestjs/swagger';
import { TagType } from '@lib/types/enums/tag-type.enum';

export class QueryTagCategoryDto {
  @ApiProperty({ description: 'Filter by title', required: false })
  title?: string;

  @ApiProperty({
    description: 'Filter by type',
    enum: TagType,
  })
  type?: TagType;
}
