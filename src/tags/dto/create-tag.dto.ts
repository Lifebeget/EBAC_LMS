import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateTagDto {
  @ApiProperty({ description: 'Tag title' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'Sorting' })
  @IsOptional()
  sort?: number;

  @ApiProperty({
    description: 'Tag category Id',
  })
  @IsNotEmpty()
  categoryId: string;
}
