import { TagCategory } from '@entities/tag-category.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateTagDto {
  @ApiProperty({ description: 'Tag title' })
  @IsOptional()
  title?: string;

  @ApiProperty({ description: 'Sorting' })
  @IsOptional()
  sort?: number;

  @ApiProperty({
    required: false,
    description: 'Tag category Id',
  })
  @IsOptional()
  categoryId?: string;

  @ApiProperty({
    required: false,
    description: 'Tag category',
  })
  @IsOptional()
  category?: TagCategory;
}
