import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Tag } from 'src/lib/entities/tag.entity';
import { QueryFailedError, Repository } from 'typeorm';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { Paginated, PaginationDto } from '../pagination.dto';
import { QueryTagDto } from './dto/query-tag.dto';
import { TagCategory } from '@entities/tag-category.entity';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private readonly tagsRepository: Repository<Tag>,
    @InjectRepository(TagCategory)
    private readonly tagCategoriesRepository: Repository<TagCategory>,
  ) {}

  async findAll(paging: PaginationDto, query: QueryTagDto): Promise<Paginated<Tag[]>> {
    const qb = this.tagsRepository
      .createQueryBuilder('tag')
      .leftJoinAndSelect('tag.category', 'tagCategory')
      .where('tag.category is not null');

    if (query.title) {
      qb.andWhere('LOWER(tag.title) LIKE LOWER(:title)', {
        title: `%${query.title}%`,
      });
    }

    if (query.categoryType) {
      qb.andWhere('tagCategory.type = :type', {
        type: `${query.categoryType}`,
      });
    }

    if (!paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    const [results, total] = await qb.getManyAndCount();

    return {
      results,
      meta: { ...paging, total },
    };
  }

  async findAllByGroup(query: QueryTagDto) {
    const qb = this.tagCategoriesRepository
      .createQueryBuilder('tagCategory')
      .leftJoin('tagCategory.tags', 'tag')
      .select('tagCategory.title as label')
      .addSelect(`json_agg(json_build_object('label', tag.title, 'value', tag.id)) as childrens`)
      .where('tag.id is not null')
      .groupBy('tagCategory.id');

    if (query.title) {
      qb.andWhere('LOWER(tag.title) LIKE LOWER(:title)', {
        title: `%${query.title}%`,
      });
    }

    if (query.categoryType) {
      qb.andWhere('tagCategory.type = :type', {
        type: `${query.categoryType}`,
      });
    }

    const result = await qb.getRawMany();

    return result;
  }

  @Transactional()
  async findOne(id: string, withCourses = false) {
    const qb = await this.tagsRepository.createQueryBuilder('tag').leftJoinAndSelect('tag.category', 'tagCategory');
    if (withCourses) {
      qb.leftJoinAndSelect('tag.courses', 'course');
    }
    qb.andWhere('tag.id = :id', { id });
    return qb.getOne();
  }

  async create(createTagDto: CreateTagDto) {
    const createFields = {
      ...createTagDto,
      category: { id: createTagDto.categoryId },
    };

    return this.tagsRepository.save(createFields).catch((err: Error) => {
      if (err instanceof QueryFailedError) {
        throw new BadRequestException('A tag with the same title and category already exists');
      } else {
        throw new BadRequestException(err.message);
      }
    });
  }

  async update(id: string, updateTagDto: UpdateTagDto) {
    if (updateTagDto.categoryId) {
      updateTagDto = Object.assign(updateTagDto, { category: { id: updateTagDto.categoryId } });
    }

    return this.tagsRepository.save({ ...updateTagDto, id }).catch((err: Error) => {
      if (err instanceof QueryFailedError) {
        throw new BadRequestException('A tag with the same title and category already exists');
      } else {
        throw new BadRequestException(err.message);
      }
    });
  }

  remove(id: string) {
    return this.tagsRepository.delete(id);
  }

  findOneByTitle(tagTitle: string, relations: string[] = []) {
    return this.tagsRepository.findOne({ title: tagTitle }, { relations });
  }
}
