import { TopicNomenclatureLink } from '@entities/topic-nomenclature-link.entity';
import { Permission } from '@enums/permission.enum';
import { Controller, Get, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Interapp } from 'src/auth/interapp.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { Repository } from 'typeorm';

@Controller('topic-nomenclature-links')
export class TopicNomenclatureLinksController {
  constructor(
    @InjectRepository(TopicNomenclatureLink)
    private topicNomenclatureLinksRepository: Repository<TopicNomenclatureLink>,
  ) {}

  @Get()
  @Interapp()
  @RequirePermissions(Permission.TOPIC_NOMENCLATURE_LINK_VIEW)
  async findTopicNomenclatureLinks(@Query('offset') offset: number, @Query('limit') limit: number) {
    const [data, count] = await this.topicNomenclatureLinksRepository.findAndCount({
      order: { id: 'ASC' },
      skip: offset,
      take: limit,
      withDeleted: true,
    });
    return { data, count };
  }
}
