import { Nomenclature } from '@entities/nomenclature.entity';
import { TopicNomenclatureLink } from '@entities/topic-nomenclature-link.entity';
import { Topic } from '@entities/topic.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import { Repository } from 'typeorm';
import { CmsTopicNomenclatureLink } from '../lib/types/CmsTopicNomenclatureLink';
import { ImportOptions } from '../lib/types/ImportOptions';

export class ImportTopicsNomenclaturesLinks {
  private topicsRepository: Repository<Topic>;
  private nomenclaturesRepository: Repository<Nomenclature>;
  private topicNomenclatureLinksRepository: Repository<TopicNomenclatureLink>;
  private apiKey: string;
  private url: string;

  constructor(context: INestApplicationContext) {
    this.topicsRepository = context.get(getRepositoryToken(Topic));
    this.nomenclaturesRepository = context.get(getRepositoryToken(Nomenclature));
    this.topicNomenclatureLinksRepository = context.get(getRepositoryToken(TopicNomenclatureLink));
    this.apiKey = process.env.CMS_API_KEY;
    this.url = process.env.CMS_NOMENCLATURE_ID_WITH_DISCOUNTS_URL;
    strict.ok(this.apiKey);
    strict.ok(this.url);
  }

  async import({ pageSize = 2 }: ImportOptions) {
    let page = 0;
    let created = 0;
    let updated = 0;

    console.log(`➡️ Importing topics/nomenclatures links`, { pageSize });

    while (true) {
      // Fetch CMS links
      const res = await axios.get<{
        data: { fields: CmsTopicNomenclatureLink[] };
      }>(this.url, {
        params: {
          offset: page * pageSize,
          limit: pageSize,
          apiKey: this.apiKey,
        },
      });

      // Create or update each LMS link
      for (const cmsLink of res.data.data.fields) {
        const topic = await this.topicsRepository.findOne(
          { externalId: cmsLink.event_schedule_topic_id },
          { select: ['id'], withDeleted: true },
        );
        strict.ok(topic, `Missing topic ${cmsLink.event_schedule_topic_id}`);
        const nomenclature = await this.nomenclaturesRepository.findOne(
          { externalId: cmsLink.nomenclature_id },
          { select: ['id'], withDeleted: true },
        );
        strict.ok(nomenclature, `Missing nomenclature ${cmsLink.nomenclature_id}`);

        const lmsLink: Partial<TopicNomenclatureLink> = {
          topic,
          nomenclature,
        };

        const existingLmsLink = await this.topicNomenclatureLinksRepository.findOne({ topic, nomenclature }, { withDeleted: true });
        if (existingLmsLink) {
          await this.topicNomenclatureLinksRepository.save({
            ...existingLmsLink,
            ...lmsLink,
          });
          updated += 1;
        } else {
          await this.topicNomenclatureLinksRepository.save(lmsLink);
          created += 1;
        }
      }

      // Quit or go to the next page
      if (res.data.data.fields.length < pageSize) {
        console.log('✔️ done', {
          created,
          updated,
          total: created + updated,
        });
        break;
      } else {
        page += 1;
        console.log({ page, created, updated });
      }
    }
  }
}
