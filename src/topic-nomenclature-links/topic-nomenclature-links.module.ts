import { TopicNomenclatureLink } from '@entities/topic-nomenclature-link.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TopicNomenclatureLinksController } from './topic-nomenclature-links.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TopicNomenclatureLink])],
  controllers: [TopicNomenclatureLinksController],
})
export class TopicNomenclatureLinksModule {}
