import { File } from '@entities/file.entity';
import { User } from '@entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Exception } from 'handlebars';
import { Repository } from 'typeorm';
const fetch = require('node-fetch');

@Injectable()
export class PdfService {
  constructor(
    @InjectRepository(File)
    private filesRepository: Repository<File>,
  ) {}

  async printPage(url: URL, params: any, fileFolder: string, fileTitle: string, owner: User) {
    url.searchParams.set('INTERAPP', process.env.INTERAPP_SECRET);

    const fetchLink =
      process.env.LAMBDA_PDF_MAKER +
      '?' +
      new URLSearchParams({
        url: url.toString() + '&',
        params: JSON.stringify(params),
        fileFolder,
      });

    console.log('PDF print request: ' + fetchLink);

    const pdfLink = await fetch(fetchLink);

    if (pdfLink.status === 500) {
      throw new Exception('Certificate generating error');
    }

    const fileUrl = await pdfLink.text();

    if (fileUrl.message) {
      throw new Exception(fileUrl.message);
    }

    return await this.filesRepository.save({
      url: fileUrl,
      title: fileTitle,
      extension: '.pdf',
      // filename: pdfLink.url.split('/').pop(),
      filename: fileUrl.split('/').pop(),
      size: 0,
      owner,
    });
  }
}
