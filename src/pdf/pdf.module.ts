import { File } from '@entities/file.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PdfService } from './pdf.service';

@Module({
  imports: [TypeOrmModule.forFeature([File])],
  providers: [PdfService],
  exports: [PdfService],
})
export class PdfModule {}
