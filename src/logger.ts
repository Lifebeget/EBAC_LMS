import {
  ArgumentsHost,
  CACHE_MANAGER,
  CallHandler,
  Catch,
  ExceptionFilter,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NestInterceptor,
  Optional,
  SetMetadata,
  forwardRef,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ClientKafka } from '@nestjs/microservices';
import * as assert from 'assert';
import { plainToClass, Transform } from 'class-transformer';
import { Request, Response } from 'express';
import * as ffs from 'fs';
import { metatypeSymb } from './metatype.pipe';
import { logTagKey } from './service/log/logging';
const fs = ffs.promises;
import { tap } from 'rxjs/operators';
import * as path from 'path';
import { Cache } from 'cache-manager';
import { KAFKA_ENABLED, KAFKA_PREFIX } from './helpers';
import { KafkaClientService } from './kafka/kafka-client.service';

type MethodT = 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELETE';

const optOutLogggingSymb = Symbol('optOutLoggging');
const magicKey = Symbol('makeTransformation');

const santize = (obj: Record<string, any>) => {
  return plainToClass(Reflect.getMetadata(metatypeSymb, obj), obj, {
    magicKey,
  } as Record<string, any>);
};

export interface LogEntry<T = Record<string, any>> {
  traceId: string;
  time: Date;
  ip: string;
  method: MethodT;
  url: string;
  userId?: string;
  body?: T;
  handlerTag: string;
}

export interface ResonseLogEntry {
  traceId: string;
  time: Date;
  ip: string;
  method: MethodT;
  url: string;
  userId?: string;
  responseBody?: string;
  handlerTag: string;
  requestTime: Date;
  statusCode: number;
}

export function ExcludeBase64FromLog() {
  return Transform(({ value, options }) => {
    if ((options as Record<string, any>).magicKey !== magicKey || !value) {
      return value;
    }

    if (!value) {
      return value;
    }

    if (!value.replaceAll) {
      return value;
    }

    try {
      // просто файл
      if (value.startsWith('data:image')) {
        return value.replaceAll(/data:image\/(.*?);base64,(.*?)$/gi, 'data:image/$1;base64,***excluded***');
      }
      // текст с картинками
      return value.replaceAll(/src="data:image\/(.*?);base64,(.*?)">/gi, 'src="data:image/$1;base64,***excluded***">');
    } catch (error) {
      console.error('ExcludeBase64FromLog:', error);
      return value;
    }
  });
}

/**
 * Makes controller responsible for providing it's own logs
 * @see {log}
 */
export const NoLog = () => SetMetadata(optOutLogggingSymb, true);

export const popLogFile = async () => {
  assert(process.env.LOG_FILE);

  const file = (await fs.readFile(process.env.LOG_FILE)).toString();

  if (!file) {
    return false;
  }

  const lines = file.split('\n');
  await fs.writeFile(process.env.LOG_FILE, lines.slice(1).join('\n'));
  return lines[0];
};

export const logRequest = async (req: Optional<LogEntry, 'time'>, kafkaClient: null | KafkaClientService) => {
  const defaults = {
    time: new Date(),
  };

  const data = Object.assign(defaults, req, {
    body: req.body && Object.values(req.body).length > 0 ? JSON.stringify(santize(req.body)) : '',
  });

  data.body = data.body.replaceAll(/("*(password|token)"\s*:\s*")[^"]*/gi, '$1****');

  if (![undefined, '****', null].includes(/"[^"]*password[^"]*":"([^"]+)"/gi.exec(data.body)?.[1])) {
    console.warn(
      `\n\nLooks like logged message body containes password key.
      Please ensure this is the expected behavior and not misplaced decorator
      Route that leaked password:
      ${data.url}
      ${data.handlerTag}\n\n`,
    );
  }
  const logMsg = `${data.time.getTime()}\t${data.traceId}\t${data.ip || ''}\t${data.method || ''}\t${data.url || ''}\t${data.handlerTag}\t${
    data.userId || ''
  }\t${data.body}`;

  const logObj = {
    localtime: data.time.getTime(),
    traceId: data.traceId,
    ip: data.ip,
    method: data.method,
    url: data.url,
    userId: data.userId,
    body: data.body,
    handlerTag: data.handlerTag,
  };

  if (process.env.LOG_FILE) {
    fs.appendFile(path.resolve(process.env.LOG_FILE), `[REQUEST_LOG] ${logMsg}\n`);
  }

  if (KAFKA_ENABLED && kafkaClient) {
    kafkaClient.emit(`log.requests.0`, logObj);
  }

  console.log(`[REQUEST_LOG] ${logMsg}`);
};

export const logResponse = async (data: ResonseLogEntry, kafkaClient: null | KafkaClientService) => {
  const responseDuration = data.time.getTime() - data.requestTime.getTime() === 0 ? 1 : data.time.getTime() - data.requestTime.getTime();
  const reponseLength = data.responseBody?.length || 0;
  const responseBody = data.responseBody
    ? data.responseBody?.substring(0, 1024).replace(/("*(password|token)"\s*:\s*")[^"]*/gi, '$1****')
    : '';

  const logMsg = `${data.time.getTime()}\t${data.traceId}\t${data.ip || ''}\t${data.method || ''}\t${data.url || ''}\t${data.handlerTag}\t${
    data.userId || ''
  }\t${responseDuration}ms\t${data.statusCode}\t${responseBody}\t${reponseLength}`;

  const logObj = {
    localtime: data.time.getTime(),
    traceId: data.traceId,
    ip: data.ip,
    method: data.method,
    url: data.url,
    userId: data.userId,
    responseDuration,
    body: responseBody,
    bodyLength: reponseLength,
    handlerTag: data.handlerTag,
    statusCode: data.statusCode,
  };

  if (process.env.LOG_FILE) {
    fs.appendFile(path.resolve(process.env.LOG_FILE), `[RESPONSE_LOG] ${logMsg}\n`);
  }

  if (KAFKA_ENABLED && kafkaClient) {
    kafkaClient.emit(`log.responses.0`, logObj);
  }

  console.log(`[RESPONSE_LOG] ${logMsg}`);
};

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(
    private reflector: Reflector,
    @Inject(CACHE_MANAGER)
    private cacheManager: Cache,
    @Inject(forwardRef(() => KafkaClientService))
    private readonly kafkaClient: KafkaClientService,
  ) {}

  intercept(context: ExecutionContext, next: CallHandler) {
    if (context.getType() !== 'http') {
      return next.handle();
    }
    const req: Request = context.switchToHttp().getRequest();
    const res: Response = context.switchToHttp().getResponse();
    const traceId = req.get('X-Request-ID');

    const user = req.user;
    const handlerTag = this.reflector.get(logTagKey, context.getHandler()) || context.getClass().name + '.' + context.getHandler().name;
    const reqLogDate = new Date();
    if (this.reflector.getAllAndOverride<boolean>(optOutLogggingSymb, [context.getHandler(), context.getClass()]) || !req) {
      return next.handle();
    }

    logRequest(
      {
        traceId: traceId || '',
        time: reqLogDate,
        ip: req.get('X-Real-IP') || req.ip,
        method: req.method as MethodT,
        url: req.url,
        userId: user?.id,
        body: req.body,
        handlerTag,
      },
      this.kafkaClient,
    ).catch(err => console.error('Failed to save log\n', err));

    const response_obj = {
      traceId: traceId || '',
      time: new Date(),
      ip: req.get('X-Real-IP') || req.ip,
      method: req.method as MethodT,
      url: req.url,
      userId: user?.id,
      responseBody: '',
      handlerTag,
      requestTime: reqLogDate,
      statusCode: 0,
    };

    return next.handle().pipe(
      tap({
        next: data => {
          response_obj.time = new Date();
          response_obj.statusCode = res.statusCode;
          response_obj.responseBody = data ? JSON.stringify(data) : '';
          logResponse(response_obj, this.kafkaClient).catch(err => console.error('Failed to save log\n', err));
        },
        error: async err => {
          response_obj.time = new Date();
          response_obj.statusCode = err?.statusCode || err?.response?.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
          response_obj.responseBody = err ? JSON.stringify(err.response || err.message || err) : '';
          await this.cacheManager.set(req.url, true);
          logResponse(response_obj, this.kafkaClient).catch(err => console.error('Failed to save log\n', err));
        },
      }),
    );
  }
}

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(
    @Inject(CACHE_MANAGER)
    private cacheManager: Cache,
    @Inject(forwardRef(() => KafkaClientService))
    private readonly kafkaClient: KafkaClientService,
  ) {}

  async catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<Response>();
    const req = ctx.getRequest<Request>();
    const status = exception.getStatus();

    const traceId = req.get('X-Request-ID');
    const responseBody = exception.getResponse() ? JSON.stringify(exception.getResponse()) : '';
    const handlerTag = '';
    const reqLogDate = new Date();

    if (!(await this.cacheManager.get(req.url))) {
      logRequest(
        {
          traceId: traceId || '',
          time: reqLogDate,
          ip: req.get('X-Real-IP') || req.ip,
          method: req.method as MethodT,
          url: req.url,
          userId: req.user?.id,
          body: req.body,
          handlerTag,
        },
        this.kafkaClient,
      ).catch(err => console.error('Failed to save log\n', err));

      logResponse(
        {
          traceId: traceId || '',
          time: new Date(),
          ip: req.get('X-Real-IP') || req.ip,
          method: req.method as MethodT,
          url: req.url,
          userId: req.user?.id,
          responseBody,
          handlerTag,
          requestTime: reqLogDate,
          statusCode: status,
        },
        this.kafkaClient,
      ).catch(err => console.error('Failed to save log\n', err));
    }
    await this.cacheManager.del(req.url);
    res.status(status).json(exception.getResponse());
  }
}
