import { IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateClientDto {
  @ApiProperty({ description: 'Client name' })
  @IsNotEmpty()
  name: string;

  @ApiProperty({ description: 'Client last name' })
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ description: 'Client full name' })
  @IsNotEmpty()
  fullName: string;

  @ApiProperty({ description: 'Client phone' })
  @IsPhoneNumber()
  phone: string;

  @ApiProperty({ description: 'Client email' })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty()
  @IsOptional()
  country?: string;
}
