import { Permission } from '@enums/permission.enum';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Interapp } from 'src/auth/interapp.decorator';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { FindOrCreateClientDto } from 'src/subscriptions/dto/find-or-create-client.dto';
import { ClientsService } from './clients.service';

@Controller('clients')
@ApiTags('clients')
export class ClientsController {
  constructor(private clientsService: ClientsService) {}

  @Post()
  @Interapp()
  @RequirePermissions(Permission.CLIENT_MANAGE)
  async findOrCreateClient(@Body() body: FindOrCreateClientDto) {
    return this.clientsService.findOrCreateClient(body);
  }
}
