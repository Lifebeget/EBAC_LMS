import { Client } from '@entities/client.entity';
import { INestApplicationContext } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { strict } from 'assert';
import axios from 'axios';
import { Repository } from 'typeorm';
import { GddClient } from '../lib/types/GddClient';
import { ImportOptions } from '../lib/types/ImportOptions';

export class ImportClients {
  private clientsRepository: Repository<Client>;
  private token: string;
  private url: string;

  constructor(context: INestApplicationContext) {
    this.clientsRepository = context.get(getRepositoryToken(Client));
    this.token = process.env.GDD_TOKEN;
    this.url = process.env.GDD_CLIENTS_URL;
    strict.ok(this.token);
    strict.ok(this.url);
  }

  async import({ pageSize = 100 }: ImportOptions) {
    let page = 0;
    let created = 0;
    let updated = 0;

    console.log(`➡️ Importing clients`, { pageSize });

    while (true) {
      // Fetch GDD clients
      const res = await axios.get<{ clients: GddClient[] }>(this.url, {
        params: { offset: page * pageSize, limit: pageSize, token: this.token },
      });

      // Map GDD clients to LMS clients
      const lmsClients = res.data.clients.map<Partial<Client>>(gddClient => ({
        externalId: gddClient.id,
        email: gddClient.email,
        phone: gddClient.phone,
        fullName: gddClient.full_name,
        name: gddClient.name,
        lastName: gddClient.last_name,
        created: new Date(gddClient.created),
        modified: new Date(gddClient.modified),
        options: gddClient.options,
      }));

      // Create or update each LMS client
      for (const lmsClient of lmsClients) {
        const existingLmsClient = await this.clientsRepository.findOne({ externalId: lmsClient.externalId }, { withDeleted: true });
        if (existingLmsClient) {
          await this.clientsRepository.save({
            ...existingLmsClient,
            ...lmsClient,
          });
          updated += 1;
        } else {
          await this.clientsRepository.save(lmsClient);
          created += 1;
        }
      }

      // Quit or go to the next page
      if (lmsClients.length < pageSize) {
        console.log('✔️ done', { created, updated, total: created + updated });
        break;
      } else {
        page += 1;
        console.log({ page, created, updated });
      }
    }
  }
}
