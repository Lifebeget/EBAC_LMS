import { Client } from '@entities/client.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientContacts } from 'src/lib/types/ClientContacts';
import { FindOrCreateClientDto } from 'src/subscriptions/dto/find-or-create-client.dto';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { CreateClientDto } from './dto/create-client.dto';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
  ) {}

  async createClient(createClientDto: CreateClientDto): Promise<Client> {
    const client = this.clientRepository.create(createClientDto);
    return this.clientRepository.save(client);
  }

  async findClient({ phone, email }: Partial<ClientContacts>): Promise<Client> {
    return this.clientRepository.findOne({ phone, email });
  }

  @Transactional()
  async findClientById(clientId: string) {
    return this.clientRepository.findOne(clientId);
  }

  async findClientByEmail({ email }: Partial<ClientContacts>): Promise<Client> {
    return this.clientRepository.findOne({ email });
  }

  async findClientByEmailAndEmptyPhone({ phone, email }: Partial<ClientContacts>): Promise<Client> {
    const client = await this.clientRepository.findOne({ email, phone: null });

    if (!client) {
      return null;
    }

    const updatedClient = await this.clientRepository
      .createQueryBuilder()
      .update(Client)
      .set({ phone })
      .where('email = :email', { email })
      .returning('*')
      .updateEntity(true)
      .execute()
      .then(response => response.raw[0]);

    return updatedClient;
  }

  async findClientByPhone({ phone }: Partial<ClientContacts>): Promise<Client> {
    return this.clientRepository.findOne({ phone });
  }

  async findClientByPhoneAndEmptyEmail({ phone, email }: Partial<ClientContacts>): Promise<Client> {
    const client = await this.clientRepository.findOne({ phone, email: null });

    if (!client) {
      return null;
    }

    const updatedClient = await this.clientRepository
      .createQueryBuilder()
      .update(Client)
      .set({ email })
      .where('phone = :phone', { phone })
      .returning('*')
      .updateEntity(true)
      .execute()
      .then(response => response.raw[0]);

    return updatedClient;
  }

  async findOrCreateClient({ name, phone, email, country }: FindOrCreateClientDto): Promise<Client> {
    const nameSpaceIndex = name.indexOf(' ');
    const firstName = name.substr(0, nameSpaceIndex) || name;
    const lastName = firstName !== name ? name.substr(firstName.length + 1) : null;

    const clientObject = {
      phone,
      email,
      fullName: name,
      name: firstName,
      lastName,
      country,
    };
    let newClient: Client;

    if (phone && email) {
      const existingClientWithFullContacts = await this.findClient({
        phone,
        email,
      });

      if (existingClientWithFullContacts) {
        console.log('Subscription: client found', existingClientWithFullContacts);
        return existingClientWithFullContacts;
      }

      const existingClientWithEmail = await this.findClientByEmailAndEmptyPhone({
        phone,
        email,
      });

      if (existingClientWithEmail) {
        console.log('Subscription: client found by email', existingClientWithEmail);
        return existingClientWithEmail;
      }

      const existingClientWithPhone = await this.findClientByPhoneAndEmptyEmail({
        phone,
        email,
      });

      if (existingClientWithPhone) {
        console.log('Subscription: client found by phone', existingClientWithPhone);
        return existingClientWithPhone;
      }
      newClient = await this.createClient(clientObject);
    } else if (email) {
      const existingClientWithEmail = await this.findClientByEmail({ email });

      if (existingClientWithEmail) {
        console.log('Subscription: client found by email', existingClientWithEmail);
        return existingClientWithEmail;
      }
      newClient = await this.createClient(clientObject);
    } else if (phone) {
      const existingClientWithPhone = await this.findClientByPhone({ phone });

      if (existingClientWithPhone) {
        console.log('Subscription: client found by phone', existingClientWithPhone);
        return existingClientWithPhone;
      }
      newClient = await this.createClient(clientObject);
    }

    console.log('Subscription: new client created', newClient);
    return newClient;
  }
}
