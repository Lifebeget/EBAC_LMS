import { Injectable } from '@nestjs/common';
import { Maintenance, maintenanceList } from './scripts';
import { RunScriptDTO } from './dto/run-script.dto';

@Injectable()
export class MaintenanceService {
  async getMaintenances(): Promise<Maintenance[]> {
    return maintenanceList.get();
  }

  async runStep(dto: RunScriptDTO): Promise<any> {
    const { script, step, params } = dto;
    return await maintenanceList.script(script)?.runStep(step, params);
  }
}
