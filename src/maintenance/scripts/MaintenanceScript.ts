import { BadRequestException } from '@nestjs/common';
import { getConnection } from 'typeorm';

export interface Maintenance {
  title: string;
  description?: string;
  steps: Array<{
    title: string;
    description?: string;
    sql: string[];
    params?: Array<{
      title: string;
      description?: string;
      required?: boolean;
    }>;
  }>;
}

export class MaintenanceScript {
  private readonly maintenance: Maintenance;

  constructor(maintenance: Maintenance) {
    this.maintenance = maintenance;
  }

  public async runStep(step: number, params?: any[]) {
    if (this.maintenance.steps[step] && this.maintenance.steps[step].sql) {
      const checkParams = this.maintenance.steps[step].params
        ? this.maintenance.steps[step].params.every((param, index) => {
            if (param.required) {
              return !!params[index];
            } else {
              return true;
            }
          })
        : true;

      if (!checkParams) {
        throw new BadRequestException(`Fill in all required fields`);
      }

      const result = [],
        sqls = this.maintenance.steps[step].sql,
        length = sqls.length;
      for (let i = 0; i < length; i++) {
        const temp = await getConnection()
          .query(sqls[i], params)
          .catch(error => {
            throw new BadRequestException(error.message);
          });
        result.push(temp);
      }
      return result;
    } else {
      throw new BadRequestException(`Not found ${step} step`);
    }
  }

  public get() {
    return this.maintenance;
  }

  public title() {
    return this.maintenance.title;
  }
}

export class MaintenanceScriptsList {
  private maintenanceList: MaintenanceScript[] = [];

  public push(maintenance: MaintenanceScript) {
    this.maintenanceList.push(maintenance);
  }

  public get(): Maintenance[] {
    return this.maintenanceList.map((maintenance: MaintenanceScript) => maintenance.get());
  }

  public script(index?: number): MaintenanceScript {
    return this.maintenanceList[index];
  }
}
