import { MaintenanceScript } from './MaintenanceScript';

export const MoveSubmissionsBeetwenCourses = new MaintenanceScript({
  title: 'Change course for submissions by module',
  description: 'При переносе модуля между курсами домашки остаются привязаны к старому курсу, скрипт переносит их на новый курс',
  steps: [
    {
      title: 'Step 1',
      description: 'Step 1 description',
      sql: ['UPDATE submission set course_id = $1, module_id = $2, lecture_id = $3, assessment_id = $4 WHERE module_id = $5 AND lecture_id = $6;'],
      params: [
        {
          title: 'To course ID',
          description: 'Insert course id',
        },
        {
          title: 'To module ID',
          description: 'Insert module id',
        },
        {
          title: 'To lecture ID',
          description: 'Insert lecture id',
        },
        {
          title: 'To assessment ID',
          description: 'Insert assessment id',
        },
        {
          title: 'From module ID',
          description: 'Where module id',
        },
        {
          title: 'From lecture ID',
          description: 'Where lecture id',
        },
      ],
    },
    {
      title: 'Step 2',
      description: 'Step 2 description',
      sql: ['UPDATE answer set question_id = $1 WHERE question_id = $2 AND submission_id = $3;'],
      params: [
        {
          title: 'To question ID',
          description: 'Insert question id',
        },
        {
          title: 'From question ID',
          description: 'Where question id',
        },
        {
          title: 'From submission ID',
          description: 'Where submission id',
        },
      ],
    },
  ],
});
