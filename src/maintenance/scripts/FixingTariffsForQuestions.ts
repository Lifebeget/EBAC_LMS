import { MaintenanceScript } from './MaintenanceScript';

export const fixingTariffsForQuestions = new MaintenanceScript({
  title: 'Admin interface problems',
  description: `Can't open the course in admin interface
You should check a response from the server when opening Admin page. Usually the problem is in graphql query that reports that "tariff is missing".
Someone somehow creates an exercise without tariff attached and it breaks the query. Usually it is an empty exercise that was somehow duplicated. The fix is to remove such exercise or assign a tariff to it.`,
  steps: [
    {
      title: 'Step 1 pt.1',
      description: `Get all assessments with type 'default' (homework exercise) without tariff
It should have questions`,
      sql: [
        `select distinct a.id as assessment_id, c.id as content_id from assessment a
inner join content c on a.id = c.assessment_id
left join question q on a.id = q.assessment_id
where a.type = 'default' and a.level_id is null and q.id is not null;`,
      ],
    },
    {
      title: 'Step 1 pt.2',
      description: `Get default service level for submissions`,
      sql: [`select id from assessment_level where target = 'submission' and is_default;`],
    },
    {
      title: 'Step 2 v.1',
      description: `Set assessments with type 'default' without tarrifs. To default tariff`,
      sql: [
        `update assessment
set level_id = $1
where id in (
    select distinct a.id from assessment a
    left join question q on a.id = q.assessment_id
    where a.type = 'default' and a.level_id is null and q.id is not null
    );`,
      ],
      params: [
        {
          title: 'levelId',
          description: 'Insert levelId from previous request',
          required: true,
        },
      ],
    },
    {
      title: 'Step 2 v.2',
      description: `Deleting exercises without questions and tariffs
1. Delete a content block with such assessment
2. Delete all assessments without levels and questions`,
      sql: [
        `delete from content where type = 'task' and assessment_id in (
select distinct a.id from assessment a
inner join content c on a.id = c.assessment_id
left join question q on a.id = q.assessment_id
where a.type = 'default' and a.level_id is null and q.id is null
);`,
        `delete from assessment where id in (
select distinct a.id from assessment a
left join question q on a.id = q.assessment_id
where a.type = 'default' and a.level_id is null and q.id is null
);`,
      ],
    },
  ],
});
