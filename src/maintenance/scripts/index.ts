import { emptyAnswerInSubmission } from './EmptyAnswerInSubmission';
import { exampleScript } from './ExmapleScript';
import { fixingTariffsForQuestions } from './FixingTariffsForQuestions';
import { invalidDateInSubmission } from './InvalidDateInSubmission';
import { MaintenanceScriptsList } from './MaintenanceScript';
import { MoveSubmissionsBeetwenCourses } from './MoveSubmissionsBeetwenCourses';
import { MoveStudentsBeetwenCourses } from './MoveStudentsBeetwenCourses';

const list = new MaintenanceScriptsList();
list.push(exampleScript);
list.push(invalidDateInSubmission);
list.push(emptyAnswerInSubmission);
list.push(fixingTariffsForQuestions);
list.push(MoveSubmissionsBeetwenCourses);
list.push(MoveStudentsBeetwenCourses);

export * from './MaintenanceScript';
export const maintenanceList = list;
