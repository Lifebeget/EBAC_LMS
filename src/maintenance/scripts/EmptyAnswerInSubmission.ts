import { MaintenanceScript } from './MaintenanceScript';

export const emptyAnswerInSubmission = new MaintenanceScript({
  title: 'EADBOX Error in submission',
  description: `This happens when submission doesn’t have any answers attached. Has nothing to do with EADBOX.`,
  steps: [
    {
      title: 'Step 1',
      description: `Check if there are submissions without answers;`,
      sql: [
        `select * from submission s
left join answer a on s.id = a.submission_id
where s.status = 'submitted' and a.id is null;`,
      ],
    },
    {
      title: 'Step 2',
      description: `1. Delete submission_tariffs of such submissions
2. Delete such submissions`,
      sql: [
        `delete from submission_tariff where submission_id in (
    select s.id from submission s
    left join answer a on s.id = a.submission_id
    where s.status = 'submitted' and a.id is null
);`,
        `delete from submission where id in (
    select s.id from submission s
    left join answer a on s.id = a.submission_id
    where s.status = 'submitted' and a.id is null
);`,
      ],
    },
  ],
});
