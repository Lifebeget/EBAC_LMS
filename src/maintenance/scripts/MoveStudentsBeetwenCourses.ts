import { MaintenanceScript } from './MaintenanceScript';

export const MoveStudentsBeetwenCourses = new MaintenanceScript({
  title: 'Move user to course',
  description: 'Переместить студента на другой курс',
  steps: [
    {
      title: 'By course ID',
      description: 'Переместить студента по его ID',
      sql: [`UPDATE enrollment set course_id = $1 WHERE course_id = $2 AND type = 'buy';`],
      params: [
        {
          title: 'To course ID',
          description: 'Course id',
        },
        {
          title: 'From course ID',
          description: 'Course id',
        },
      ],
    },
    {
      title: 'By course ID and created date',
      description: 'Переместить студента по его ID',
      sql: [`UPDATE enrollment set course_id = $1 WHERE course_id = $2 AND created BETWEEN $3 AND $4 AND type = 'buy';`],
      params: [
        {
          title: 'To course ID',
          description: 'Course id',
        },
        {
          title: 'From course ID',
          description: 'Course id',
        },
        {
          title: 'Created from',
          description: 'YYYY-MM-DD',
        },
        {
          title: 'Created to',
          description: 'YYYY-MM-DD',
        },
      ],
    },
    {
      title: 'By user ID',
      description: 'Переместить студента по его ID',
      sql: [`UPDATE enrollment set course_id = $1 WHERE course_id = $2 AND user_id=$3 AND type = 'buy';`],
      params: [
        {
          title: 'To course ID',
          description: 'Course id',
        },
        {
          title: 'From course ID',
          description: 'Course id',
        },
        {
          title: 'Student ID',
          description: 'Student id',
        },
      ],
    },
  ],
});
