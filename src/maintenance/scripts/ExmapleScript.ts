import { MaintenanceScript } from './MaintenanceScript';

export const exampleScript = new MaintenanceScript({
  title: 'Example Script',
  description: 'Example script',
  steps: [
    {
      title: 'Step 1',
      description: 'Step 1 description',
      sql: ['SELECT * FROM public.user LIMIT 20'],
    },
    {
      title: 'Step 2',
      description: 'Step 2 description',
      sql: ['SELECT * FROM public.user WHERE name = $1 LIMIT 20'],
      params: [
        {
          title: 'Username',
          description: 'Insert username',
        },
      ],
    },
    {
      title: 'Step 3',
      description: 'Step 3 description',
      sql: ['SELECT * FROM public.user WHERE name = $1 AND active = $2 LIMIT 20'],
      params: [
        {
          title: 'Username',
          description: 'Insert username',
        },
        {
          title: 'Active',
          description: 'Insert active',
        },
      ],
    },
  ],
});
