import { MaintenanceScript } from './MaintenanceScript';

export const invalidDateInSubmission = new MaintenanceScript({
  title: 'INVALID DATE in submission',
  description: `Это поведение связано с тем, что у Submission проставлен флаг current_attempt = false — это значит, что есть более свежая версия задания, а эта версия, по логике, должна быть уже проверена, и интерфейс пытается отобразить комментарий тьютора, которого нет.

Каким-то образом домашки от студента задублировались.
  
Так же в редких случаях бывает, что более свежей версии нет, но почему-то всё равно проставился флаг current_attempt = false.`,
  steps: [
    {
      title: 'Step 1',
      description: `First check if there are such submissions
All submissions that are in submitted status that has more recent attempt`,
      sql: [
        `select s.id as id, s2.id as duplicate_id, s.user_id from submission s
left join submission s2 on s2.user_id = s.user_id and s2.assessment_id = s.assessment_id and s2.current_attempt = true
where s.status = 'submitted' and s.current_attempt = false and
      s2.id is not null;`,
      ],
    },
    {
      title: 'Step 2 v.1',
      description: `1. Delete answers from such submissions
2. Delete tarrifs from such submissions
3. Delete such submissions`,
      sql: [
        `delete from answer where submission_id in (
select s.id from submission s
left join submission s2 on s2.user_id = s.user_id and s2.assessment_id = s.assessment_id and s2.current_attempt = true
where s.status = 'submitted' and s.current_attempt = false and
      s2.id is not null);`,
        `delete from submission_tariff where submission_id in (
select s.id from submission s
left join submission s2 on s2.user_id = s.user_id and s2.assessment_id = s.assessment_id and s2.current_attempt = true
where s.status = 'submitted' and s.current_attempt = false and
      s2.id is not null);`,
        `delete from submission where id in (
select s.id from submission s
left join submission s2 on s2.user_id = s.user_id and s2.assessment_id = s.assessment_id and s2.current_attempt = true
where s.status = 'submitted' and s.current_attempt = false and
      s2.id is not null);`,
      ],
    },
    {
      title: 'Step 2 v.2',
      description: `Set current attempt to true of such submissions`,
      sql: [
        `update submission set current_attempt = true where id in (
  select s.id from submission s
  left join submission s2 on s2.user_id = s.user_id and s2.assessment_id = s.assessment_id and s2.current_attempt = true
  where s.status = 'submitted' and s.current_attempt = false and
        s2.id is null
);`,
      ],
    },
  ],
});
