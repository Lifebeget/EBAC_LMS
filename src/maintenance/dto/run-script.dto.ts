import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber } from 'class-validator';

export class RunScriptDTO {
  @ApiProperty({ description: 'script index' })
  @IsNumber()
  script: number;

  @ApiProperty({ description: 'step index' })
  @IsNumber()
  step: number;

  @ApiProperty({ description: 'Sql params' })
  @IsArray()
  params?: any[];
}
