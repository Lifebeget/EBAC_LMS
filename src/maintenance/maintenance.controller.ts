import { Permission } from '@lib/types/enums';
import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { RunScriptDTO } from './dto/run-script.dto';
import { MaintenanceService } from './maintenance.service';
import { Maintenance } from './scripts';

@ApiTags('maintenance')
@ApiBearerAuth()
@Controller('maintenance')
export class MaintenanceController {
  constructor(private readonly maintenanceService: MaintenanceService) {}

  @Get()
  @RequirePermissions(Permission.MAINTENANCE_MANAGE)
  getMaintenance(): Promise<Maintenance[]> {
    return this.maintenanceService.getMaintenances();
  }

  @Post()
  @RequirePermissions(Permission.MAINTENANCE_MANAGE)
  runStep(@Body() dto: RunScriptDTO): Promise<Maintenance[]> {
    return this.maintenanceService.runStep(dto);
  }
}
