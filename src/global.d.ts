import { CoursePermissionsType, SystemRole } from '@lib/api/types';
import { User as UserModel } from './lib/entities/user.entity';
import { Permission } from './lib/enums/permission.enum';

declare global {
  type Await<T> = T extends PromiseLike<infer U> ? U : T;

  type Optional<T extends Record<string, any>, K extends keyof T = keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

  type MethodT = 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELETE';

  // #BuildPerfomance> beware of recursive types
  // https://github.com/microsoft/TypeScript/issues/35729#issuecomment-567033431
  type DeepPartial<T> = {
    [P in keyof T]?: DeepPartial<T[P]>;
  };
  namespace Express {
    export interface User extends UserModel {
      parsedRoles: SystemRole[];
      permissions: Permission[];
      coursePermissions: CoursePermissionsType;
      isSuperadmin(): boolean;
      isAdmin(): boolean;
      isObserver(): boolean;
      isDesigner(): boolean;
      isManager(): boolean;
      isTeamlead(): boolean;
      isTutor(): boolean;
      isSupport(): boolean;
      isFrontendManager(): boolean;
      hasSomeRoles(roles: SystemRole | SystemRole[]): boolean;
      hasPermission(permission: Permission | Permission[]): boolean;
      hasSomePermission(permission: Permission | Permission[]): boolean;
      enrolledAsTutor(courseId: string): boolean;
      enrolledAsTeamlead(courseId: string): boolean;
      enrolledAsStudent(courseId: string): boolean;
      enrolledAsModerator(courseId: string): boolean;
      enrolledAsSupport(courseId: string): boolean;
    }
  }

  namespace NodeJS {
    interface ProcessEnv {
      IMPORTS_LOG_DIR: string;
      DB_USER: string;
      DB_PASSWORD: string;
      DB_NAME: string;
      DB_HOST: string;
      DB_PORT: string;
      PASSWORD_SALT: string;
      JWT_TOKEN_SECRET: string;
      JWT_EXPIRATION_TIME: string;
      DEFAULT_ADMIN_EMAIL: string;
      DEFAULT_ADMIN_PASSWORD: string;
      INTERAPP_SECRET: string;
      CRYPTO_KEY: string;
      FILES_STORAGE_TYPE: string;
      FILES_SERVER_URL: string;
      API_URL: string;
      LOG_DIR_PERSIST: string;
      CLICKHOUSE_DB_HOST: string;
      CLICKHOUSE_DB_PORT: string;
      CLICKHOUSE_DB_NAME: string;
      CLICKHOUSE_DB_USER: string;
      CLICKHOUSE_DB_PASSWORD: string;
      LOG_FILE: string;
      LOG_FILE_POPULATION: string;
      EADBOX_API_URL: string;
      EADBOX_API_KEY: string;
      EADBOX_ADMIN_LOGIN: string;
      EADBOX_ADMIN_PASSWORD_BASE64: string;
      SUBMISSION_LOCK_SECONDS: string;
      MAIL_USER: string;
      MAIL_PASSWORD: string;
      MAIL_SERVICE: string;
      MAIL_HOST: string;
      MAIL_FROM: string;
      MAIL_BCC: string;
      MAIL_FORCE_TO: string;
      TOKEN_TTL: string;
      TUTOR_LMS_URL: string;
      STUDENT_LMS_URL: string;
      WEBINAR_ACCESS_KEY: string;
      WEBINAR_GDD_SUBSCRIPTIONS_URL: string;
      PLATFORM_NAME: string;
      EXPERTSENDER_API_KEY: string;
      EXPERTSENDER_API_URL: string;
      EXPERTSENDER_MAIL_LIST: string;
      LAMBDA_NEGATIVE_FORUM_MSG_URL: string;
      LAMBDA_NEGATIVE_FORUM_MSG_MX_URL: string;
      LAMBDA_NEGATIVE_FORUM_MSG_AUTH: string;
    }
  }
}
