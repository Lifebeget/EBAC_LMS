import { ReadStatus } from '@entities/read-status.entity';
import { SystemNotification } from '@entities/system-notification.entity';
import { User } from '@entities/user.entity';
import { Permission } from '@enums/permission.enum';
import { ReadStatusResult } from '@enums/read-status-result.enum';
import { BadRequestException, Body, Controller, Delete, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags, ApiExtraModels } from '@nestjs/swagger';
import { CurrentUser } from 'src/current-user.decorator';
import { Pagination } from 'src/pagination.decorator';
import { ApiPaginatedResponse, PaginationDto } from 'src/pagination.dto';
import { PgConstraints } from 'src/pg.decorators';
import { RequirePermissions } from 'src/require-permissions.decorator';
import { CreateSystemNotificationDto } from './dto/create-system-notification.dto';
import { QuerySystemNotificationDto } from './dto/query-system-notification.dto';
import { QueryUserSystemNotificationsDto } from './dto/query-user-system-notifications.dto';
import { UpdateSystemNotificationDto } from './dto/update-system-notification-dto';
import { SystemNotificationsService } from './system-notifications.service';
import { Errors } from './types/errors';

@ApiTags('system-notifications')
@ApiBearerAuth()
@Controller()
@ApiExtraModels(ReadStatus)
export class SystemNotificationsController {
  constructor(private readonly systemNotificationsService: SystemNotificationsService) {}

  @Post('system-notifications')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_MANAGE)
  @ApiBody({ type: CreateSystemNotificationDto })
  @ApiOperation({ summary: 'Create a new system notification' })
  @ApiResponse({
    status: 201,
    description: 'Created notification',
    type: SystemNotification,
  })
  @PgConstraints()
  async create(
    @Body()
    createSystemNotificationDto: CreateSystemNotificationDto,
    @CurrentUser() user: User,
  ) {
    try {
      return await this.systemNotificationsService.create({
        ...createSystemNotificationDto,
        authorId: user.id,
      });
    } catch (err) {
      this.processError(err);
    }
  }

  @Get('system-notifications')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_VIEW)
  @ApiOperation({ summary: 'List all system notifications' })
  @ApiPaginatedResponse(SystemNotification)
  findAll(@Query() query: QuerySystemNotificationDto, @Pagination() paging: PaginationDto) {
    return this.systemNotificationsService.findAll(query, paging);
  }

  @Get('system-notifications/:id')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_VIEW)
  @ApiOperation({ summary: 'Get one system notification by id' })
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.systemNotificationsService.findOne(id);
  }

  @Patch('system-notifications/:id')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_MANAGE)
  @ApiOperation({ summary: 'Updates a system notification' })
  @ApiResponse({
    status: 200,
    description: 'Updated system notification',
  })
  async update(@Param('id', new ParseUUIDPipe()) id: string, @Body() updateSystemNotificationDto: UpdateSystemNotificationDto) {
    try {
      return await this.systemNotificationsService.update(id, updateSystemNotificationDto);
    } catch (err) {
      this.processError(err);
    }
  }

  @Delete('system-notifications/:id')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_MANAGE)
  @ApiOperation({ summary: 'Deletes a system notification' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of affected fields',
  })
  remove(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.systemNotificationsService.remove(id);
  }

  @Get('system-notifications/:id/read-statuses')
  @RequirePermissions(Permission.SYSTEM_NOTIFICATIONS_VIEW)
  @ApiOperation({ summary: 'List all read statuses by system notification id' })
  @ApiPaginatedResponse(ReadStatus)
  findAllReadStatuses(@Param('id', new ParseUUIDPipe()) id: string, @Pagination() paging: PaginationDto) {
    return this.systemNotificationsService.findAllReadStatuses(id, paging);
  }

  @Get('user/system-notifications')
  @ApiOperation({
    summary: 'Gets list of user system notifications',
  })
  @ApiResponse({
    status: 200,
    description: 'Returns list of user system notifications',
  })
  getUserSystemNotifications(@CurrentUser() user: User, @Query() query: QueryUserSystemNotificationsDto): Promise<SystemNotification[]> {
    return this.systemNotificationsService.getUserSystemNotifications(user, query);
  }

  @Patch('user/system-notifications/:id')
  @ApiOperation({
    summary: 'Updates a read status of user system notification',
  })
  @ApiResponse({
    status: 200,
    description: 'Returns true in any case',
  })
  async markUserSystemNotification(
    @CurrentUser() user: User,
    @Param('id') systemNotificationId: string,
    @Body('result') result: ReadStatusResult,
  ): Promise<true> {
    await this.systemNotificationsService.readSystemNotification(
      {
        userId: user.id,
        systemNotificationId,
      },
      result,
    );

    return true;
  }

  processError(err: Error & { type?: Errors }) {
    if (err.type === Errors.ValidationError) {
      throw new BadRequestException(err.message);
    }

    throw err;
  }
}
