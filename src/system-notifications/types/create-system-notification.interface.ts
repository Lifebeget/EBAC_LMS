import { CreateSystemNotificationDto } from '../dto/create-system-notification.dto';

export interface CreateSystemNotificationInterface extends CreateSystemNotificationDto {
  authorId: string;
}
