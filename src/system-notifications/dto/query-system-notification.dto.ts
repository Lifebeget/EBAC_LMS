import { SystemNotificationType } from '@enums/system-notification-type.enum';
import { ApiProperty } from '@nestjs/swagger';

export class QuerySystemNotificationDto {
  @ApiProperty({
    description: 'Set filter by system notification ids',
    required: false,
  })
  ids?: string[];

  @ApiProperty({ description: 'Set filter by author', required: false })
  authorId?: string;

  @ApiProperty({
    description: 'Set filter by content (title/description)',
    required: false,
  })
  content?: string;

  @ApiProperty({
    description: 'Set filter by system notification type',
    required: false,
  })
  type?: SystemNotificationType;

  @ApiProperty({ description: 'Filter by "forTutor" status', required: false })
  forTutor?: boolean;

  @ApiProperty({
    description: 'Filter by "forStudent" status',
    required: false,
  })
  forStudent?: boolean;

  @ApiProperty({ description: 'Filter by "forAdmin" status', required: false })
  forAdmin?: boolean;

  @ApiProperty({ description: 'Filter by "deleted" status', required: false })
  deleted?: boolean;

  @ApiProperty({ description: 'Minimal date of creation', required: false })
  createdMinDate?: Date;
}
