import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { CreateSystemNotificationDto } from './create-system-notification.dto';

export class UpdateSystemNotificationDto extends PartialType(CreateSystemNotificationDto) {
  @ApiProperty({
    description: 'System notification has been deleted',
    required: false,
    default: false,
  })
  @IsOptional()
  deleted?: boolean;
}
