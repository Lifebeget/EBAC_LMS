import { IsBoolean, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { SystemNotificationButtonType } from '@enums/system-notification-button-type.enum';
import { SystemNotificationCondition } from '../../lib/types/SystemNotificationCondition';
import { SystemNotificationType } from '@enums/system-notification-type.enum';
import { SystemRole } from '@lib/types/enums';

export class CreateSystemNotificationDto {
  @ApiProperty({ description: 'System notification title', required: true })
  @IsNotEmpty()
  title: string;

  @ApiProperty({
    description: 'System notification description',
    required: false,
  })
  @IsOptional()
  description?: string;

  @ApiProperty({
    description: 'Is system notification for tutor interface',
    default: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  forTutor?: boolean;

  @ApiProperty({
    description: 'Is system notification for student interface',
    default: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  forStudent?: boolean;

  @ApiProperty({
    description: 'Is system notification for admin interface',
    default: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  forAdmin?: boolean;

  @ApiProperty({
    description: 'If set, notification must be read in its entirety before it can be closed',
    default: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  mustRead?: boolean;

  @ApiProperty({
    description: "Don't mark as read until the flag is lifted or notification is deleted",
    default: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  sticky?: boolean;

  @ApiProperty({
    enum: SystemNotificationType,
    description: 'Type of system notification',
    required: true,
  })
  @IsNotEmpty()
  type: SystemNotificationType;

  @ApiProperty({
    enum: SystemNotificationButtonType,
    description: 'Type of system notification button',
    required: false,
  })
  @IsOptional()
  buttonType?: SystemNotificationButtonType;

  @ApiProperty({
    description: 'System notification view conditions',
    required: false,
  })
  @IsOptional()
  conditions?: SystemNotificationCondition;

  @ApiProperty({
    description: 'System notification lifetime in hours',
    default: 720,
    required: false,
  })
  @IsOptional()
  @IsNumber()
  lifetimeHours?: number;

  @ApiProperty({ description: 'Route to follow after notification click' })
  @IsOptional()
  to?: string;

  @ApiProperty({ description: 'Need to translate message', enum: SystemRole })
  @IsOptional()
  @IsBoolean()
  translate?: boolean;

  @ApiProperty({ description: 'Show notification only for this role', enum: SystemRole })
  @IsOptional()
  forRole?: SystemRole;
}
