import { PickType } from '@nestjs/swagger';
import { QuerySystemNotificationDto } from './query-system-notification.dto';

export class QueryUserSystemNotificationsDto extends PickType(QuerySystemNotificationDto, ['forTutor', 'forAdmin', 'forStudent']) {}
