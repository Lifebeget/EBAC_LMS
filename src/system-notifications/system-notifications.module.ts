import { SystemNotification } from '@entities/system-notification.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SystemNotificationsController } from './system-notifications.controller';
import { SystemNotificationsService } from './system-notifications.service';
import { ReadStatus } from '@entities/read-status.entity';
import { SocketsModule } from 'src/sockets/sockets.module';

@Module({
  imports: [TypeOrmModule.forFeature([SystemNotification, ReadStatus]), SocketsModule],
  controllers: [SystemNotificationsController],
  providers: [SystemNotificationsService],
})
export class SystemNotificationsModule {}
