import { ReadStatus } from '@entities/read-status.entity';
import { SystemNotification } from '@entities/system-notification.entity';
import { User } from '@entities/user.entity';
import { ReadStatusResult } from '@enums/read-status-result.enum';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Paginated, PaginationDto } from 'src/pagination.dto';
import { SocketsGateway } from 'src/sockets/sockets.gateway';
import { Brackets, Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';
import { QuerySystemNotificationDto } from './dto/query-system-notification.dto';
import { QueryUserSystemNotificationsDto } from './dto/query-user-system-notifications.dto';
import { UpdateSystemNotificationDto } from './dto/update-system-notification-dto';
import { CreateSystemNotificationInterface } from './types/create-system-notification.interface';
import { Errors } from './types/errors';

@Injectable()
export class SystemNotificationsService {
  constructor(
    @InjectRepository(SystemNotification)
    private readonly systemNotificationRepository: Repository<SystemNotification>,
    @InjectRepository(ReadStatus)
    private readonly readStatusRepository: Repository<ReadStatus>,
    private readonly socketsGateway: SocketsGateway,
  ) {}

  async create({ authorId, ...createSystemNotificationDto }: CreateSystemNotificationInterface): Promise<SystemNotification> {
    this.validateFORProp(createSystemNotificationDto);

    const createFields = {
      ...createSystemNotificationDto,
      author: { id: authorId },
    };

    const entity = await this.systemNotificationRepository.save(createFields);
    this.notifySockets('createSystemNotification', entity, entity.id);
    return entity;
  }

  async findAll(query: QuerySystemNotificationDto, paging: PaginationDto): Promise<Paginated<SystemNotification[]>> {
    const qb = this.getQuery(query).leftJoinAndSelect('systemNotification.author', 'author');

    const readStatusQb = this.readStatusRepository
      .createQueryBuilder('readStatus')
      .select('readStatus.systemNotification.id', 'systemNotificationId')
      .addSelect('COUNT(readStatus.systemNotification.id)', 'count')
      .groupBy('readStatus.systemNotification.id');

    qb.leftJoin(
      `(${readStatusQb.clone().where(`readStatus.result IS NOT NULL`).getQuery()})`,
      'readedRows',
      '"readedRows"."systemNotificationId" = systemNotification.id',
    ).addSelect('"readedRows"."count"', 'read');

    qb.leftJoin(
      `(${readStatusQb.clone().getQuery()})`,
      'openedRows',
      '"openedRows"."systemNotificationId" = systemNotification.id',
    ).addSelect('"openedRows"."count"', 'open');

    if (!paging.showAll) {
      qb.skip((paging.page - 1) * paging.itemsPerPage);
      qb.take(paging.itemsPerPage);
    }

    const { entities, raw } = await qb.getRawAndEntities();
    const total = await qb.getCount();

    const results = entities.map((entity, idx) => ({
      ...entity,
      read: +raw[idx].read,
      open: +raw[idx].open,
    }));

    return {
      results,
      meta: { ...paging, total },
    };
  }

  @Transactional()
  findOne(id: string): Promise<SystemNotification> {
    return this.systemNotificationRepository.findOne(id, {
      relations: ['author'],
    });
  }

  async update(id: string, updateSystemNotificationDto: UpdateSystemNotificationDto): Promise<SystemNotification> {
    const entity = await this.findOne(id);

    const entityFields = {
      ...entity,
      ...updateSystemNotificationDto,
    };

    this.validateFORProp(entityFields);

    const updatedEntity = await this.systemNotificationRepository.save(entityFields);

    this.notifySockets('updateSystemNotification', entity, updatedEntity);

    return updatedEntity;
  }

  async remove(id: string) {
    const removed = await this.update(id, { deleted: true });
    this.notifySockets('deleteSystemNotification', removed, id);
    return removed;
  }

  async getUserSystemNotifications(user: User, query: QueryUserSystemNotificationsDto): Promise<SystemNotification[]> {
    const userRoles = user.roles.map(r => r.role);

    const qb = this.getQuery({ ...query, deleted: false })
      .andWhere(`systemNotification.created + make_interval(hours => systemNotification.lifetimeHours) >= NOW()`)
      .leftJoinAndMapOne('systemNotification.readStatus', 'systemNotification.readStatuses', 'readStatus', 'readStatus.user = :userId', {
        userId: user.id,
      })
      .andWhere('readStatus.result IS NULL')
      .andWhere('(systemNotification.forRole in (:...userRoles) or systemNotification.forRole is null)', { userRoles });

    const systemNotifications = await qb.getMany();
    const createEntities: ReadStatus[] = systemNotifications
      .filter(systemNotification => !systemNotification.readStatus)
      .map(systemNotification => {
        return ReadStatus.of({
          user,
          systemNotification,
        });
      });

    if (createEntities.length) {
      await this.readStatusRepository.save(createEntities);
    }

    return systemNotifications;
  }

  async readSystemNotification(
    { userId, systemNotificationId }: { userId: string; systemNotificationId: string },
    result: ReadStatusResult,
  ) {
    const user = { id: userId };
    const systemNotification = { id: systemNotificationId };
    const readStatus = await this.readStatusRepository.findOne(
      {
        user,
        systemNotification,
      },
      {
        relations: ['systemNotification'],
      },
    );

    return (
      !readStatus.systemNotification.sticky &&
      (await this.readStatusRepository.save({
        user,
        systemNotification,
        result,
      }))
    );
  }

  async findAllReadStatuses(id: string, paging: PaginationDto): Promise<Paginated<ReadStatus[]>> {
    const [results, total] = await this.readStatusRepository.findAndCount({
      where: {
        systemNotification: { id },
      },
      relations: ['user'],
      ...(!paging.showAll && {
        skip: (paging.page - 1) * paging.itemsPerPage,
        take: paging.itemsPerPage,
      }),
    });

    return {
      results,
      meta: { ...paging, total },
    };
  }

  private getQuery(query: QuerySystemNotificationDto) {
    const qb = this.systemNotificationRepository.createQueryBuilder('systemNotification').orderBy('systemNotification.created', 'DESC');

    qb.where('systemNotification.deleted = :deleted', {
      deleted: !!query.deleted,
    });

    if (query.ids?.length) {
      qb.andWhere('systemNotification.id IN (:...ids)', {
        ids: query.ids,
      });
    }

    if (query.authorId) {
      qb.andWhere('systemNotification.author.id = :authorId', {
        authorId: query.authorId,
      });
    }

    if (query.content) {
      qb.andWhere(
        new Brackets(qb => {
          qb.where('systemNotification.title ILIKE :title', {
            title: `%${query.content}%`,
          }).orWhere('systemNotification.description ILIKE :description', {
            description: `%${query.content}%`,
          });
        }),
      );
    }

    if (query.forAdmin) {
      qb.andWhere('systemNotification.forAdmin = :forAdmin', {
        forAdmin: query.forAdmin,
      });
    }

    if (query.forStudent) {
      qb.andWhere('systemNotification.forStudent = :forStudent', {
        forStudent: query.forStudent,
      });
    }

    if (query.forTutor) {
      qb.andWhere('systemNotification.forTutor = :forTutor', {
        forTutor: query.forTutor,
      });
    }

    if (query.type) {
      qb.andWhere('systemNotification.type = :type', {
        type: query.type,
      });
    }

    if (query.createdMinDate) {
      qb.andWhere('systemNotification.created >= :createdMinDate', {
        createdMinDate: query.createdMinDate,
      });
    }

    return qb;
  }

  private validateFORProp(systemNotificationDto: { forAdmin?: boolean; forStudent?: boolean; forTutor?: boolean }) {
    const { forAdmin, forStudent, forTutor } = systemNotificationDto;
    if (!forAdmin && !forStudent && !forTutor) {
      throw {
        message: 'forAdmin/forStudent/forTutor — one or more must be selected',
        type: Errors.ValidationError,
      };
    }
  }

  private notifySockets(
    event: string,
    settings: { forAdmin: boolean; forStudent: boolean; forTutor: boolean },
    payload: SystemNotification | string,
  ) {
    const { forAdmin, forStudent, forTutor } = settings;
    const rooms = [forAdmin && 'admin', forTutor && 'tutor', forStudent && 'student'].filter(Boolean);

    this.socketsGateway.sendSystemNotification(rooms, event, payload);
  }
}
