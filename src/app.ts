import { INestApplicationContext } from '@nestjs/common';

export let resolveApp = undefined;
export const appPromise = new Promise<INestApplicationContext>(res => (resolveApp = res));
