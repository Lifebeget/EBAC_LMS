import { CronJob } from 'cron';
import { spawn } from 'child_process';
import * as path from 'path';
import * as fs from 'fs';
import { CronExpression } from '@nestjs/schedule';

const jobs = [];

function createCronJob(cronTime: CronExpression, command = 'echo nothing') {
  jobs.push(
    new CronJob(
      cronTime,
      function () {
        console.log(`START CRON JOB - "${command}"`);
        const child = spawn(command, [], { shell: true });

        child.stdout.on('data', (data) => {
          console.log(`STDOUT: ${data}`);
        });

        child.stdout.on('error', (err) => {
          console.log(`ERROR: ${err}`);
        });

        child.on('close', function (code) {
          console.log(`"${command}" CLOSE WITH CODE = ${code}`);
          return code;
        });
      },
      null,
      true,
      'America/Sao_Paulo',
    ).start(),
  );
}

createCronJob(
  CronExpression.EVERY_HOUR,
  `node bin/manage populate ${process.env.API_URL}`,
);
