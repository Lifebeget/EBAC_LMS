import { CronJob } from 'cron';
import { spawn } from 'child_process';
import * as path from 'path';
import * as fs from 'fs';
import config from '../config';

const jobs = [];

function logger(msg, logFile) {
  const message = `${new Date().toISOString()}: ${msg}`;
  console.log(message);
  fs.appendFile(
    process.env.IMPORTS_LOG_DIR ? `${path.resolve(process.env.IMPORTS_LOG_DIR)}/${logFile}` : logFile,
    `${message}`,
    function (err) {
      if (err) throw err;
    },
  );
}

function runJob(command, logFile) {
  logger(`START CRON JOB - "${command}"`, logFile);
  const child = spawn(command, [], { shell: true });

  child.stdout.on('data', data => {
    logger(`STDOUT: ${data}`, logFile);
  });

  child.stdout.on('error', err => {
    logger(`ERROR: ${err}`, logFile);
  });

  child.stderr.on('data', err => {
    logger(`ERROR: ${err}`, logFile);
  });

  child.on('close', function (code) {
    logger(`"${command}" CLOSE WITH CODE = ${code}`, logFile);
    return code;
  });
}

function createCronJob(cronTime, command = 'echo nothing', logFile = 'debug.log') {
  jobs.push(
    new CronJob(
      cronTime,
      function () {
        runJob(command, logFile);
      },
      null,
      true,
      config.timeZone,
    ).start(),
  );
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
for (const [key, value] of Object.entries(config.cronJobs)) {
  if (value.active) {
    createCronJob(value.time, value.command, value.log);
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
for (const [key, value] of Object.entries(config.launchJobs)) {
  if (value.active) {
    logger(`AUTOLAUNCH JOB - "${value.command}"`, value.log);
    runJob(value.command, value.log);
  }
}
