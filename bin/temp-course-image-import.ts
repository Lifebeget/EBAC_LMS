import axios, { AxiosResponse } from 'axios';

const email = 'admin@example.com';
const password = 'admin';
const apiUrl = 'http://localhost:3000';

import * as links from './temp-course-image-import.json';

const headers: Record<string, string> = {
  'Content-Type': 'application/json',
};

(async () => {
  let response: AxiosResponse;

  response = await axios.post(
    `${apiUrl}/auth/login`,
    {
      email,
      password,
    },
    { headers },
  );

  const token: string = response.data.access_token;
  headers.Authorization = 'Bearer ' + token;

  response = await axios.post(
    'https://cms.ebaconline.com.br/api/public/showcase/',
  );
  // CCCCCCOMBO!
  const ebacCourses = response.data.data.data;

  response = await axios.get(`${apiUrl}/courses`, { headers });
  const lmsCourses = response.data;

  ebacCourses.forEach(async (eCourse) => {
    const link = links.find((l) => l.nomenclature_id == eCourse.id);
    if (!link) return console.log('Link not found. Id: ' + eCourse.id);
    const lCourse = lmsCourses.find(
      (lCourse) => link.eb_course_id == lCourse.externalId,
    );
    if (!lCourse)
      return console.log('Course not found. ExternalId: ' + link.eb_course_id);

    const updateFields: Record<string, string> = {
      id: eCourse.id,
      duration: eCourse.duration,
      color: eCourse.backgroundColor,
    };

    const tryImage = async (url: string) => {
      if (!url) return false;
      try {
        response = await axios.head(url);
        return true;
      } catch (e) {
        return false;
      }
    };

    const setImage = async (url: string) => {
      try {
        response = await axios.post(
          `${apiUrl}/files/create-from-url`,
          { url },
          { headers },
        );

        if (response.data?.id) {
          updateFields.imageId = response.data?.id;
        }

        return true;
      } catch (e) {
        return false;
      }
    };

    const bySlugUrl = `https://ebaconline.com.br/images/nomenclatures/${eCourse.slug}.svg`;

    if (
      eCourse.images?.showcaseImage?.url &&
      tryImage(eCourse.images?.showcaseImage?.url)
    ) {
      await setImage(eCourse.images.showcaseImage.url);
    } else if (tryImage(bySlugUrl)) {
      await setImage(
        `https://ebaconline.com.br/images/nomenclatures/${eCourse.slug}.svg`,
      );
    }

    try {
      response = await axios.patch(
        `${apiUrl}/courses/${lCourse.id}`,
        updateFields,
        { headers },
      );

      if (response.data?.id) {
        console.log(`Course ${lCourse.id} updated.`);
      } else {
        console.error(`Course update error ${lCourse.id}.`);
        console.dir(updateFields);
      }
    } catch (e) {
      console.error(e);
    }
  });
})();
